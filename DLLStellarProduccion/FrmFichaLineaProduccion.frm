VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmFichaLineaProduccion 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11010
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   7560
   ControlBox      =   0   'False
   Icon            =   "FrmFichaLineaProduccion.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11010
   ScaleWidth      =   7560
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdGrabar 
      Caption         =   "Guardar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   4320
      Picture         =   "FrmFichaLineaProduccion.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   27
      Top             =   9540
      Width           =   1455
   End
   Begin VB.CommandButton CmdSalir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   6000
      Picture         =   "FrmFichaLineaProduccion.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   9540
      Width           =   1335
   End
   Begin VB.Frame FrameOtros 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   2580
      TabIndex        =   17
      Top             =   660
      Visible         =   0   'False
      Width           =   2295
      Begin VB.Label lblOtros 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Materia Prima"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   18
         Top             =   30
         Width           =   1995
      End
   End
   Begin VB.Frame FrameHistorial 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   5040
      TabIndex        =   12
      Top             =   660
      Visible         =   0   'False
      Width           =   2295
      Begin VB.Label lblHistorial 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Historial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   19
         Top             =   30
         Visible         =   0   'False
         Width           =   1995
      End
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   2175
      Left            =   120
      TabIndex        =   10
      Top             =   1200
      Width           =   7335
      Begin VB.TextBox txtUnidadMedida 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         TabIndex        =   1
         Top             =   1680
         Width           =   3495
      End
      Begin VB.TextBox txtDescri 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   0
         TabStop         =   0   'False
         Text            =   "Ejemplo"
         Top             =   840
         Width           =   6615
      End
      Begin VB.Label lblUnidadMedida 
         BackStyle       =   0  'Transparent
         Caption         =   "Unidad de Medida o Procesamiento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   1320
         Width           =   3855
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2040
         X2              =   7000
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label lblLineaPrd 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "L�nea de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   1710
      End
      Begin VB.Label lblDescLinea 
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   4455
      End
   End
   Begin VB.Frame FramePlan 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   660
      Width           =   2295
      Begin VB.Label lblPlanificacion 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "General"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   9
         Top             =   30
         Width           =   1995
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   7680
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5115
         TabIndex        =   6
         Top             =   75
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   7080
         Picture         =   "FrmFichaLineaProduccion.frx":9D8E
         Top             =   -30
         Width           =   480
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   7305
      Left            =   0
      TabIndex        =   14
      Top             =   1920
      Width           =   7335
      Begin VB.TextBox txtUsuarioUpd 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   31
         TabStop         =   0   'False
         Text            =   "SUPERVISOR"
         Top             =   2880
         Width           =   2895
      End
      Begin VB.TextBox txtUltimaActualizacion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   4320
         Locked          =   -1  'True
         TabIndex        =   29
         TabStop         =   0   'False
         Text            =   "2000/01/01 23:59 AM"
         Top             =   1920
         Width           =   2895
      End
      Begin VB.TextBox txtObs 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1875
         Left            =   360
         MaxLength       =   1024
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   25
         Top             =   5040
         Width           =   6750
      End
      Begin VB.ComboBox CboEstatusFuncional 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   3840
         Width           =   3480
      End
      Begin VB.TextBox txtCapacidadOperativaActual 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   360
         TabIndex        =   3
         Top             =   2880
         Width           =   3495
      End
      Begin VB.TextBox txtCapacidadInstalada 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   360
         TabIndex        =   2
         Top             =   1920
         Width           =   3495
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         DragMode        =   1  'Automatic
         Height          =   975
         Left            =   120
         TabIndex        =   16
         Top             =   480
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   1720
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Usuario:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   4320
         TabIndex        =   30
         Top             =   2520
         Width           =   2655
      End
      Begin VB.Label lblUltActualizacion 
         BackStyle       =   0  'Transparent
         Caption         =   "�ltima Actualizaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   4320
         TabIndex        =   28
         Top             =   1560
         Width           =   2655
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Observaciones o Informaci�n Adicional"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   4560
         Width           =   5295
      End
      Begin VB.Label lblEstadoFuncional 
         BackStyle       =   0  'Transparent
         Caption         =   "Estado de Funcionamiento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   24
         Top             =   3480
         Width           =   3495
      End
      Begin VB.Label lblCapacidadOperativaActual 
         BackStyle       =   0  'Transparent
         Caption         =   "Capacidad Operativa Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   23
         Top             =   2520
         Width           =   3495
      End
      Begin VB.Label lblCapacidadInstalada 
         BackStyle       =   0  'Transparent
         Caption         =   "Capacidad Instalada (Max)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   1560
         Width           =   3495
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00AE5B00&
         X1              =   960
         X2              =   7200
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Historial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   15
         Top             =   120
         Width           =   690
      End
   End
End
Attribute VB_Name = "FrmFichaLineaProduccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada        As Boolean

Public Descripcion_PK       As String
Public mGrabo               As Boolean

Private mClsGrupos As New ClsGrupos
Private FechaModif          As Date

Private Sub cmdGrabar_Click()
    If GuardarDatosLinea() Then
        mGrabo = True
        Unload Me
        Exit Sub
    End If
End Sub

Private Sub CmdSalir_Click()
    mGrabo = False
    Unload Me
End Sub

Private Sub Exit_Click()
    CmdSalir_Click
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        mGrabo = False
        
        mClsGrupos.cTipoGrupo = "PRD_EST_LN"
        mClsGrupos.CargarComboGrupos FrmAppLink.CnADM, CboEstatusFuncional
        
        Tecla_Pulsada = True
        ListRemoveItems CboEstatusFuncional, "Ninguno"
        Tecla_Pulsada = False
        
        If CargarDatosLinea(Descripcion_PK) Then
            'Exit Sub
        End If
        
    End If
    
End Sub

Private Function CargarDatosLinea(ByVal pDescripcion As String) As Boolean
    
    On Error GoTo Error
    
    Dim mSql As String, mRsDatos As ADODB.Recordset
    
    mSql = "SELECT TOP 1 * FROM MA_LINEAPRODUCCION MA " & _
    "WHERE (MA.Descripcion = '" & QuitarComillasSimples(pDescripcion) & "') " & _
    Empty
    
    Set mRsDatos = FrmAppLink.CnADM.Execute(mSql)
    
    If Not mRsDatos.EOF Then
        txtDescri = pDescripcion
        txtUnidadMedida = mRsDatos!UnidadDeMedida
        txtCapacidadInstalada = mRsDatos!CapacidadInstalada
        txtCapacidadOperativaActual = mRsDatos!CapacidadOperativaActual
        ListSafeItemSelection CboEstatusFuncional, mRsDatos!EstadoFuncional
        txtObs = mRsDatos!Observacion
        FechaModif = IIf(mRsDatos!Fecha_Upd >= mRsDatos!Fecha_Add, mRsDatos!Fecha_Upd, mRsDatos!Fecha_Add)
        txtUltimaActualizacion = SDate(FechaModif) & " " & STime(FechaModif)
        txtUsuarioUpd = Buscar_Usuario(mRsDatos!Cod_Usuario_Upd)
        CargarDatosLinea = True
    Else
        txtDescri = pDescripcion
        txtUnidadMedida = Empty
        txtCapacidadInstalada = Empty
        txtCapacidadOperativaActual = Empty
        'ListSafeItemSelection CboEstatusFuncional, Empty
        txtObs = Empty
        txtUltimaActualizacion = "N/A"
        txtUsuarioUpd = "N/A"
    End If
    
    Exit Function
    
Error:
    
    CargarDatosLinea = False
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Function

Private Sub CboEstatusFuncional_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntL As Long
    
    mClsGrupos.cTipoGrupo = "PRD_EST_LN"
    
    If PosAntL <> CboEstatusFuncional.ListIndex Or CboEstatusFuncional.ListCount = 1 Then
        PosAntL = CboEstatusFuncional.ListIndex
        If CboEstatusFuncional.ListIndex = CboEstatusFuncional.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo FrmAppLink.CnADM, CboEstatusFuncional
            mClsGrupos.CargarComboGrupos FrmAppLink.CnADM, CboEstatusFuncional
            Tecla_Pulsada = True
            ListRemoveItems CboEstatusFuncional, "Ninguno"
            Tecla_Pulsada = False
            PosAntL = 0
        Else
            If Trim(CboEstatusFuncional.Text) <> Empty Then
                
            Else
                'CboEstatusFuncional.Visible = False
            End If
        End If
    End If
    
End Sub

Public Function Buscar_Usuario(Valor_Campo As String) As String
    
    Dim Rec_User As New ADODB.Recordset
    
    Call Apertura_Recordset(Rec_User)
    
    Rec_User.Open "SELECT Descripcion FROM MA_USUARIOS WHERE CodUsuario = '" & Valor_Campo & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not Rec_User.EOF Then
        Buscar_Usuario = Rec_User!Descripcion
    Else
        Buscar_Usuario = "N/A"
    End If
    
    Call Cerrar_Recordset(Rec_User)
    
End Function

Private Function GuardarDatosLinea() As Boolean
    
    On Error GoTo Error
    
    Dim mSql As String, mRsDatos As ADODB.Recordset
    
    Set mRsDatos = New ADODB.Recordset
    
    With mRsDatos
        
        Apertura_RecordsetC mRsDatos
        
        mSql = "SELECT TOP 1 * FROM MA_LINEAPRODUCCION MA " & _
        "WHERE (MA.Descripcion = '" & QuitarComillasSimples(Descripcion_PK) & "') " & _
        Empty
        
        .Open mSql, FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If .EOF Then
            .AddNew
            !Descripcion = Descripcion_PK
            '!Fecha_Add = FechaBD(Now, FBD_FULL, True) ' GetDate()
            '!Fecha_Upd = FechaBD(Now, FBD_FULL, True) ' GetDate()
            !Cod_Usuario_Add = FrmAppLink.GetCodUsuario
            !Cod_Usuario_Upd = !Cod_Usuario_Add
        Else
            .Update
            !Fecha_Upd = FechaBD(Now, FBD_FULL, True) ' GetDate()
            !Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
        End If
        
        !UnidadDeMedida = Left(txtUnidadMedida, .Fields("UnidadDeMedida").DefinedSize)
        !CapacidadInstalada = Left(txtCapacidadInstalada, .Fields("CapacidadInstalada").DefinedSize)
        !CapacidadOperativaActual = Left(txtCapacidadOperativaActual, .Fields("CapacidadOperativaActual").DefinedSize)
        !EstadoFuncional = Left(CboEstatusFuncional.Text, .Fields("EstadoFuncional").DefinedSize)
        !Observacion = txtObs 'Left(txtObs, .Fields("Observacion").DefinedSize)
        
        .UpdateBatch
        
        GuardarDatosLinea = True
        
    End With
    
    Exit Function
    
Error:
    
    GuardarDatosLinea = False
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Function

Private Sub Form_Load()
    lbl_Organizacion = "Ficha de L�nea de Producci�n"
    AjustarPantalla Me
End Sub
