Attribute VB_Name = "ModAppLink"
Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    FrmAppLink.AjustarPantalla Forma, False
End Sub

Public Function sGetIni(ByVal SIniFile As String, ByVal SSection As String, _
ByVal sKey As String, ByVal SDefault As String) As String
    sGetIni = FrmAppLink.sGetIni(SIniFile, SSection, sKey, SDefault)
End Function

Public Sub sWriteIni(ByVal SIniFile As String, ByVal SSection As String, _
ByVal sKey As String, ByVal sData As String)
    FrmAppLink.sWriteIni SIniFile, SSection, sKey, sData
End Sub

Public Function GetEnvironmentVariable(ByVal Expression) As String
    GetEnvironmentVariable = FrmAppLink.GetEnvironmentVariable(Expression)
End Function

Public Function ExisteTabla(pNombreTabla As String, pConexion As Object) As Boolean
    ExisteTabla = FrmAppLink.ExisteTabla(pNombreTabla, pConexion)
End Function

Public Function ExisteTablaV3(ByVal pTabla As String, _
pCn As Variant, _
Optional ByVal pBD As String = "") As Boolean
    ExisteTablaV3 = FrmAppLink.ExisteTablaV3(pTabla, pCn, pBD)
End Function

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, Optional pSql As String = "", Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    If Not IsMissing(pRs) Then
        ExisteCampoTabla = FrmAppLink.ExisteCampoTabla(pCampo, pRs)
    ElseIf Not IsMissing(pCn) Then
        ExisteCampoTabla = FrmAppLink.ExisteCampoTabla(pCampo, , pSql, pValidarFecha, pCn)
    Else
        ExisteCampoTabla = FrmAppLink.ExisteCampoTabla(pCampo, , pSql, pValidarFecha)
    End If
End Function

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    FrmAppLink.Apertura_Recordset Rec
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    FrmAppLink.Cerrar_Recordset Rec
End Sub

Public Sub Apertura_RecordsetC(ByRef Rec As ADODB.Recordset)
    FrmAppLink.Apertura_RecordsetC Rec
End Sub

Public Sub CambiarFuenteMensaje( _
Optional ByVal TextProperties As StdFont = Nothing, _
Optional ByVal pColorID = -1)
    FrmAppLink.CambiarFuenteMensaje TextProperties, pColorID
End Sub

Public Sub ActivarMensajeGrande(Optional ByVal pPorcentaje As Integer = 20)
    FrmAppLink.ActivarMensajeGrande pPorcentaje
End Sub

Public Function Mensaje(Activo As Boolean, Texto As String, Optional pVbmodal = True, _
Optional txtBoton1 As Variant, Optional txtBoton2 As Variant) As Boolean
    Mensaje = FrmAppLink.Mensaje(Activo, Texto, pVbmodal, txtBoton1, txtBoton2)
End Function

Public Function RetornoMensaje() As Boolean
    RetornoMensaje = FrmAppLink.RetornoMensaje
End Function

Public Function MsjErrorRapido(ByVal pDescripcion As String, _
Optional ByVal pMsjIntro As String = "[StellarMensaje]")
    MsjErrorRapido = FrmAppLink.MsjErrorRapido(pDescripcion, pMsjIntro)
End Function

Public Function Truncar(Numero, Optional ByVal Decimales As Integer) As Double
    Truncar = FrmAppLink.Truncar(Numero, Decimales)
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    BuscarValorBD = FrmAppLink.BuscarValorBD(Campo, SQL, pDefault, pCn)
End Function

Public Function ExecuteSafeSQL(ByVal SQL As String, _
pCn As ADODB.Connection, _
Optional ByRef Records = 0, _
Optional ByVal ReturnsResultSet As Boolean = False) As ADODB.Recordset
    Set ExecuteSafeSQL = FrmAppLink.ExecuteSafeSQL(SQL, pCn, Records, ReturnsResultSet)
End Function

Public Function Buscar_ProductosPrecios(ByVal pDeposito As String, Optional ByVal pPrecioCliente) As Variant
    Buscar_ProductosPrecios = FrmAppLink.Buscar_ProductosPrecios(pDeposito, pPrecioCliente)
End Function

Public Function BuscarInfoProducto_Basica(Optional pConex, _
Optional ByVal pPrecioCliente, Optional ByVal pGlobal As Boolean = False, _
Optional ByVal pDeposito, Optional pTextoABuscar, _
Optional ByVal pVista As FrmBuscarProducto_Vista = VistaPrecioCliente, _
Optional ByVal pDescontarComprometidos As Boolean = False, _
Optional ByVal pAccesoFichaProductos As Boolean = True) As Variant
    BuscarInfoProducto_Basica = FrmAppLink.BuscarInfoProducto_Basica(pConex, pPrecioCliente, _
    pGlobal, pDeposito, pTextoABuscar, pVista, pDescontarComprometidos, pAccesoFichaProductos)
End Function

Public Function BuscarInfo_Usuario( _
Optional pConex, _
Optional ByVal pTextoABuscar, _
Optional ByVal isQuery As Boolean = False, _
Optional ByVal AdministrarOpciones As Boolean = False) As Variant
    BuscarInfo_Usuario = FrmAppLink.BuscarInfo_Usuario(pConex, pTextoABuscar, isQuery, AdministrarOpciones)
End Function

Public Function BuscarReglaNegocioBoolean(ByVal pCampo, Optional pIgualA) As Boolean
    BuscarReglaNegocioBoolean = FrmAppLink.BuscarReglaNegocioBoolean(pCampo, pIgualA)
End Function

Public Function BuscarReglaNegocioStr(ByVal pCampo, _
Optional ByVal pDefault As String = "") As String
    BuscarReglaNegocioStr = FrmAppLink.BuscarReglaNegocioStr(pCampo, pDefault)
End Function

Public Function GetServerName(pCn As ADODB.Connection) As String
    GetServerName = FrmAppLink.GetServerName(pCn)
End Function

Public Function BuscarCodigoEdiProducto(ByVal pCodNasa, _
Optional ByVal pRetornarCodNasaporDefecto As Boolean = False) As String
    BuscarCodigoEdiProducto = FrmAppLink.BuscarCodigoEdiProducto(pCodNasa, pRetornarCodNasaporDefecto)
End Function

Public Sub Retraso(ByVal Segundos As Long)
    FrmAppLink.Retraso Segundos
End Sub

Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function LimitarComillasSimples(ByVal pCadena As String) As String
    LimitarComillasSimples = FrmAppLink.LimitarComillasSimples(pCadena)
End Function

Public Function CampoLength(ByVal Campo As String, ByVal Tabla As String, _
Server_BD As ADODB.Connection, FieldType As Enum_FieldType, _
Optional MinLength As Integer = 1) As Integer
    CampoLength = FrmAppLink.CampoLength(Campo, Tabla, Server_BD, FieldType, MinLength)
End Function

Public Function StringContains(Source As String, Search As String, _
tipo As StringContains_TipoBusqueda, _
Optional pConexion As ADODB.Connection) As Boolean
    StringContains = FrmAppLink.StringContains(Source, Search, tipo, pConexion)
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    SafeEquals = FrmAppLink.SafeEquals(pVal1, pVal2)
End Function

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = FrmAppLink.Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' Changed AS a Pointer to V2.

Public Function Collection_EncontrarValor(pCollection As Collection, ByVal pValor, _
Optional ByVal pIndiceInicial As Long = 1) As Long
    Collection_EncontrarValor = FrmAppLink.Collection_FindValueIndex(pCollection, pValor, pIndiceInicial)
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteKey(pCollection As Collection, ByVal pKey As String) As Boolean
    Collection_ExisteKey = FrmAppLink.Collection_HasKey(pCollection, pKey)
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteIndex(pCollection As Collection, ByVal pIndex As Long) As Boolean
    Collection_ExisteIndex = FrmAppLink.Collection_HasIndex(pCollection, pIndex)
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean
    Collection_RemoveKey = FrmAppLink.Collection_SafeRemoveKey(pCollection, pKey)
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    Collection_RemoveIndex = FrmAppLink.Collection_SafeRemoveIndex(pCollection, pIndex)
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|") As Collection
    Set ConvertirCadenaDeAsociacion = FrmAppLink.ConvertirCadenaDeAsociacion(pCadena, pSeparador)
End Function

Public Function ConvertirCadenaDeAsociacionAvanzado(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":") As Dictionary
    Set ConvertirCadenaDeAsociacionAvanzado = FrmAppLink.ConvertirCadenaDeAsociacionAvanzado(pCadena, pSeparador, pSeparadorInterno)
End Function

Public Function ImageList_ExisteItem(pImgList As ImageList, pIndex As Variant) As Boolean
    ImageList_ExisteItem = FrmAppLink.ImageList_ExisteItem(pImgList, pIndex)
End Function

Public Function PuedeImprimirDataReportVertical(mRep, Optional pTamañoPapel As String = "Carta / Letter") As Boolean
    PuedeImprimirDataReportVertical = FrmAppLink.PuedeImprimirDataReportVertical(mRep, pTamañoPapel)
End Function

Public Function PuedeImprimirDataReportHorizontal(mRep, Optional pTamañoPapel As String = "Oficio / Legal") As Boolean
    PuedeImprimirDataReportHorizontal = FrmAppLink.PuedeImprimirDataReportHorizontal(mRep, pTamañoPapel)
End Function

Public Function NombreDiadelaSemana(pDiaID As Byte, Optional pLang As String = "Esp", _
Optional ByVal pOmitirCaracteresEspeciales As Boolean = True) As String
    NombreDiadelaSemana = FrmAppLink.NombreDiadelaSemana(pDiaID, pLang, pOmitirCaracteresEspeciales)
End Function

Public Function RemoverAcentos(ByVal Cadena As String) As String
    RemoverAcentos = FrmAppLink.RemoverAcentos(Cadena)
End Function

Public Function ReplaceMultiple(ByVal OrigString As String, _
ByVal ReplaceString As String, ParamArray FindChars()) As String
    Dim PArray As Variant
    PArray = FindChars
    ReplaceMultiple = FrmAppLink.ReplaceMultiple(OrigString, ReplaceString, PArray)
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant
    ValidarNumeroIntervalo = FrmAppLink.ValidarNumeroIntervalo(pValor, pMax, pMin)
End Function

Public Function HexToString(ByVal HexToStr As String) As String
    HexToString = FrmAppLink.HexToString(HexToStr)
End Function

Public Function StringToHex(ByVal StrToHex As String) As String
    StringToHex = FrmAppLink.StringToHex(StrToHex)
End Function

Public Function ReadFileIntoString(ByVal StrFilePath As String) As String
    ReadFileIntoString = FrmAppLink.ReadFileIntoString(StrFilePath)
End Function

Public Function LoadFile(ByVal StrFileName As String) As String
    LoadFile = FrmAppLink.LoadFile(StrFileName)
End Function

Public Function WriteStringIntoFile(ByVal StrFilePath As String, ByVal pText As String) As Boolean
    FrmAppLink.WriteStringIntoFile StrFilePath, pText
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    KillSecure = FrmAppLink.KillSecure(pFile)
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = FrmAppLink.KillSecure(pFile)
End Function

Public Function PuedeObtenerFoco(Objeto As Object, _
Optional ByVal OrMode As Boolean = False) As Boolean
    PuedeObtenerFoco = FrmAppLink.PuedeObtenerFoco(Objeto, OrMode)
End Function

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = FrmAppLink.Stellar_Mensaje(pResourceID, True)
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    Stellar_Mensaje = FrmAppLink.Stellar_Mensaje(Mensaje, pDevolver)
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    SafeCreateObject = FrmAppLink.SafeCreateObject(pClass, pServerName)
End Function

Public Function PathExists(ByVal pPath As String) As Boolean
    PathExists = FrmAppLink.PathExists(pPath)
End Function

Public Function GetDirectoryRoot(ByVal pPath As String) As String
    GetDirectoryRoot = FrmAppLink.GetDirectoryRoot(pPath)
End Function

Public Function GetDirParent(ByVal pPath As String, _
Optional ByVal pURL As Boolean) As String
    GetDirParent = FrmAppLink.GetDirParent(pPath, pURL)
End Function

Public Function CopyPath(ByVal Source As String, ByVal Destination As String) As Boolean
    CopyPath = FrmAppLink.CopyPath(Source, Destination)
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String
    FindPath = FrmAppLink.FindPath(FileName, FindFileInUpperLevels, BaseFilePath)
End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean
    CreateFullDirectoryPath = FrmAppLink.CreateFullDirectoryPath(pDirectoryPath)
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    GetLines = FrmAppLink.GetLines(HowMany)
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    GetTab = FrmAppLink.GetTab(HowMany)
End Function

Public Function GetSysTime(ByVal bReturnUTC As Boolean) As Date
    GetSysTime = FrmAppLink.GetSysTime(bReturnUTC)
End Function
 
Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    FechaBD = FrmAppLink.FechaBD(Expression, pTipo, pGrabarRecordset)
End Function

Public Function Control_TopReal(pControl As Object) As Long
    Control_TopReal = FrmAppLink.Control_TopReal(pControl)
End Function

Public Function Control_LeftReal(pControl As Object) As Long
    Control_LeftReal = FrmAppLink.Control_LeftReal(pControl)
End Function

Public Function HideCtl(ByVal pCtl, ByVal TimeInMillis, _
Optional ByVal TagProgress As Boolean = True)
    HideCtl = FrmAppLink.HideCtl(pCtl, TimeInMillis, TagProgress)
End Function

Public Function CollapseCtl(ByVal pCtl, ByVal TimeInMillis, _
Optional ByVal TagProgress As Boolean = True)
    CollapseCtl = FrmAppLink.CollapseCtl(pCtl, TimeInMillis, TagProgress)
End Function

Public Sub TecladoAvanzado(pControl As Object, Optional ByVal Backup As Boolean = False, _
Optional ByVal Backup_ReestablecerControlOriginal As Boolean = True, Optional pTipoEspecifico As TipoTecladoStellar = -1)
    FrmAppLink.TecladoAvanzado pControl, Backup, Backup_ReestablecerControlOriginal, pTipoEspecifico
End Sub

Public Function NombreTemp() As String
    NombreTemp = FrmAppLink.NombreTemp
End Function

Public Function SepararCadenas(pCantCaracteres As Long, _
ByVal pCadenaIni As String, ByVal pCadenaFin As String, _
Optional pRelleno = " ") As String
    SepararCadenas = FrmAppLink.SepararCadenas(pCantCaracteres, pCadenaIni, pCadenaFin, pRelleno)
End Function

Public Function ReadIniFull(ByVal pFilePath As String) As Collection
    Set ReadIniFull = FrmAppLink.ReadIniFull(pFilePath)
End Function

Public Function RegenerarIni(pFilePath As String, pCollection As Collection) As Boolean
    RegenerarIni = FrmAppLink.RegenerarIni(pFilePath, pCollection)
End Function

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant
    isDBNull = FrmAppLink.isDBNull(pValue, pDefaultValueReturned)
End Function

Public Function QuickInputRequest(ByVal pTituloSolicitud As String, Optional pMostrarBotonSalir As Boolean = True, _
Optional ByVal pDefaultAlCancelar As String = "", Optional ByVal pDefaultAlIniciar As String = "", _
Optional pPlaceHolder As String = "", Optional pTituloBarra As String = "Stellar BUSINESS", _
Optional pAlineamientoTituloSolicitud As AlignmentConstants = AlignmentConstants.vbLeftJustify, _
Optional pAlineamientoInput As AlignmentConstants = AlignmentConstants.vbLeftJustify, _
Optional pAlineamientoPlaceHolder As AlignmentConstants = AlignmentConstants.vbCenter, _
Optional pAlineamientoTituloBarra As AlignmentConstants = AlignmentConstants.vbCenter, _
Optional pTecladoAutomatico As Boolean = False, _
Optional pAllowEmptyResp As Boolean = False, _
Optional pPwd As Boolean = False) As String
    QuickInputRequest = FrmAppLink.QuickInputRequest(pTituloSolicitud, pMostrarBotonSalir, _
    pDefaultAlCancelar, pDefaultAlIniciar, _
    pPlaceHolder, pTituloBarra, _
    pAlineamientoTituloSolicitud, _
    pAlineamientoInput, _
    pAlineamientoPlaceHolder, _
    pAlineamientoTituloBarra, _
    pTecladoAutomatico, _
    pAllowEmptyResp, _
    pPwd)
End Function

Public Sub SeleccionarTexto(pControl As Object)
    FrmAppLink.SeleccionarTexto pControl
End Sub

Public Function LoadTextFile(ByVal pFilePath As String) As String
    LoadTextFile = FrmAppLink.LoadFile(pFilePath)
End Function

Public Function SaveTextFile(ByVal pContents As String, ByVal pFilePath As String, _
Optional pUseUnicode As Boolean = False) As Boolean
    SaveTextFile = FrmAppLink.SaveTextFile(pContents, pFilePath, pUseUnicode)
End Function

Public Function PrinterSecureKillDoc() As Boolean
    PrinterSecureKillDoc = FrmAppLink.PrinterSecureKillDoc
End Function

Public Function SplitStringIntoFixedLengthLines(ByVal MaxLineChars As Long, _
ByVal pFullString As String, _
Optional ByVal RemoveLf As Boolean = True)
    SplitStringIntoFixedLengthLines = FrmAppLink.SplitStringIntoFixedLengthLines(MaxLineChars, pFullString, RemoveLf)
End Function

Public Function WordWrapLineBySpace(ByRef LineStr As String, MaxLineChars As Long, ByRef FoundLine As Boolean)
    WordWrapLineBySpace = FrmAppLink.WordWrapLineBySpace(LineStr, MaxLineChars, FoundLine)
End Function

Public Function NumericFirst(ByVal pSqlField As String, _
Optional ByVal Desc As Boolean = False) As String
    NumericFirst = FrmAppLink.NumericFirst(pSqlField, Desc)
End Function

Public Function FixAmpersandString(ByVal pString As String) As String
    FixAmpersandString = FrmAppLink.FixAmpersandString(pString)
End Function

Function RemoverCaracter(ByVal pString As String, ByVal pPosicion As Long) As String
    RemoverCaracter = FrmAppLink.RemoverCaracter(pString, pPosicion)
End Function

Public Function ChangeDataReportZoom(Optional Initialize As Boolean = False)
    ChangeDataReportZoom = FrmAppLink.ChangeDataReportZoom(Initialize)
End Function

Public Function EndOfDay(Optional ByVal pFecha) As Date
    EndOfDay = FrmAppLink.EndOfDay(pFecha)
End Function

Public Sub ShowPicturePreviewOnForm(ActiveForm As Object, PicObj)
    FrmAppLink.ShowPicturePreviewOnForm ActiveForm, PicObj
End Sub

Public Sub ListSafeItemSelection(pCmb As Object, ByVal pStrVal As String)
    FrmAppLink.ListSafeItemSelection pCmb, pStrVal
End Sub

Public Sub ListSafeIndexSelection(pCmb As Object, ByVal pIndex As Integer)
    FrmAppLink.ListSafeIndexSelection pCmb, pIndex
End Sub

Public Function ListContainsAny(pCmb As Object, _
ParamArray pItems() As Variant) As Boolean
    Dim PArray As Variant
    PArray = pItems
    ListContainsAny = FrmAppLink.ListContainsAny(pCmb, PArray)
End Function

Public Function ListItemDataContainsAny(pCmb As Object, _
ParamArray pItems() As Variant) As Boolean
    Dim PArray As Variant
    PArray = pItems
    ListItemDataContainsAny = FrmAppLink.ListItemDataContainsAny(pCmb, PArray)
End Function

Public Function ListRemoveItems(pCmb As Object, ParamArray pItems() As Variant) As Boolean
    Dim PArray As Variant
    PArray = pItems
    ListRemoveItems = FrmAppLink.ListRemoveItems(pCmb, PArray)
End Function

Public Function ListRemoveItemsByItemData(pCmb As Object, _
ParamArray pItems() As Variant) As Boolean
    Dim PArray As Variant
    PArray = pItems
    ListRemoveItemsByItemData = FrmAppLink.ListRemoveItemsByItemData(pCmb, PArray)
End Function

Public Function DebugColumnViewInfo(pObjTabular As Object, _
Optional ByVal pViewDataRow As Long = 0) As String
    DebugColumnViewInfo = FrmAppLink.DebugColumnViewInfo(pObjTabular, pViewDataRow)
End Function

Public Function ValidarConexion(pConexion As Object, _
Optional pNewConnectionString As String, _
Optional pEsConexGlobal As Boolean = False) As Boolean
    ValidarConexion = FrmAppLink.ValidarConexion(pConexion, pNewConnectionString, pEsConexGlobal)
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    Set AsEnumerable = FrmAppLink.AsEnumerable(ArrayList)
End Function

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    FrmAppLink.SafeVarAssign pVar, pValue
End Sub

Public Function SafeItem(pObj, pItem, pDefaultValue)
    SafeItem = FrmAppLink.SafeItem(pObj, pItem, pDefaultValue)
End Function

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    SafeProp = FrmAppLink.SafeProp(pObj, pItem, pDefaultValue)
End Function

Public Sub SafeItemAssign(pObj, pItem, pValue)
    FrmAppLink.SafeItemAssign pObj, pItem, pValue
End Sub

Public Sub SafePropAssign(pObj, pProperty, pValue)
    FrmAppLink.SafePropAssign pObj, pProperty, pValue
End Sub

Public Function QuickPrintRecordset(ByVal pRs As ADODB.Recordset, _
Optional OnlyCurrentRow As Boolean = False) As String
    QuickPrintRecordset = FrmAppLink.QuickPrintRecordset(pRs, OnlyCurrentRow)
End Function

Public Function QuickPrintRecord(ByVal pRs As ADODB.Recordset) As String
    QuickPrintRecord = FrmAppLink.QuickPrintRecordset(pRs, True)
End Function

Public Function PrintRecordset(ByVal pRs As ADODB.Recordset, Optional ByVal pRowNumAlias As String = vbNullString, _
Optional OnlyCurrentRow As Boolean = False, Optional EncloseHeaders As Boolean = True, _
Optional LineBeforeData As Boolean = False, Optional ByVal pOnValueError As String = "Error|N/A", _
Optional NestLevel As Byte = 0) As String
    PrintRecordset = FrmAppLink.PrintRecordset(pRs, pRowNumAlias, OnlyCurrentRow, _
    EncloseHeaders, LineBeforeData, pOnValueError, NestLevel)
End Function

Public Function PrintRecord(ByVal pRs As ADODB.Recordset, _
Optional EncloseHeaders As Boolean = True, Optional LineBeforeData As Boolean = False, _
Optional ByVal pOnValueError As String = "Error|N/A") As String
    PrintRecord = FrmAppLink.PrintRecordset(pRs, , True, EncloseHeaders, LineBeforeData, pOnValueError)
End Function

Public Function ExecuteSQLProcedure(pCn As ADODB.Connection, _
Optional ByVal pFilePath As String, _
Optional ByVal pSQLText As String, _
Optional ByVal pIfSQLIsStoredProcedure_Name As String, _
Optional pIfSQLIsStoredProcedure_Parameters As Variant = Empty, _
Optional ByVal GetResultSetInsteadOfCommand As Boolean = False, _
Optional ByVal CallerErrorHandling As Boolean = False _
) As Object
    'If CallerErrorHandling Then On Error GoTo 0
    Set ExecuteSQLProcedure = FrmAppLink.ExecuteSQLProcedure(pCn, pFilePath, pSQLText, _
    pIfSQLIsStoredProcedure_Name, pIfSQLIsStoredProcedure_Parameters, _
    GetResultSetInsteadOfCommand, CallerErrorHandling)
End Function

' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

' Formatea Fecha Corta o Larga si incluye hora, según configuración regional. _
Solo debe usarse en capa de presentación, no en consultas a bases de datos !

Public Function GDate(ByVal pDate) As String: On Error Resume Next: GDate = FormatDateTime(pDate, vbGeneralDate): End Function

' Formatea Fecha Corta según configuración regional. _
Solo debe usarse en capa de presentación, no en consultas a bases de datos !

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

' Formatea Hora Corta o Larga si incluye hora, según configuración regional. _
Solo debe usarse en capa de presentación, no en consultas a bases de datos !

Public Function GTime(ByVal pDate) As String: On Error Resume Next: GTime = FormatDateTime(pDate, vbLongTime): End Function

' Formatea Fecha Corta según configuración regional. _
Solo debe usarse en capa de presentación, no en consultas a bases de datos !

Public Function STime(ByVal pDate) As String: On Error Resume Next: STime = FormatDateTime(pDate, vbShortTime): End Function

Public Function ValorNumericoOpcion(ByVal pBit As Double) As Double: ValorNumericoOpcion = (2 ^ pBit): End Function

'Private Function ConvertirBinario(ByVal pNumeroCombinacion As Double, _
'Optional ByVal pTopeCantidadOpciones As Double = 7) As String
    'ConvertirBinario = FrmAppLink.ConvertirBinario(pNumeroCombinacion, pTopeCantidadOpciones)
'End Function

Public Function ArrayBitsSeleccionados(ByVal pNumeroCombinacion As Double, _
Optional ByVal pTopeCantidadOpciones As Double = 7) As Variant
    ArrayBitsSeleccionados = FrmAppLink.ArrayBitsSeleccionados(pNumeroCombinacion, pTopeCantidadOpciones)
End Function

Public Function BitActivo(ByVal pNumeroOpcion As Double, Optional ByVal BitArrayCargado As Variant, _
Optional ByVal pNumeroCombinacion As Double, Optional ByVal pTopeCantidadOpciones As Double = 7) As Boolean
    BitActivo = FrmAppLink.BitActivo(pNumeroOpcion, BitArrayCargado, pNumeroCombinacion, pTopeCantidadOpciones)
End Function

Public Function GetFont(Optional ByVal Name, Optional ByVal Size, _
Optional ByVal Bold, Optional ByVal Italic, _
Optional ByVal Charset, Optional ByVal Weight, _
Optional ByVal StrikeThrough, Optional ByVal Underline) As StdFont
    Set GetFont = FrmAppLink.GetFont(Name, Size, Bold, Italic, Charset, Weight, StrikeThrough, Underline)
End Function

Public Sub ShowTooltip(ByVal pText, ByVal pWidth, ByVal pTime, Optional pControl, _
Optional ByVal pAlignment As Integer = 0, _
Optional ByVal pBackColor, _
Optional ByVal pForeColor, _
Optional ByVal pBorderColor, _
Optional pFontObj As StdFont, _
Optional ByVal TooltipID As String)
    FrmAppLink.ShowTooltip pText, pWidth, pTime, pControl, pAlignment, _
    pBackColor, pForeColor, pBorderColor, pFontObj, TooltipID
End Sub

Public Sub ShowToast(ByVal pText, ByVal pWidth, ByVal pTime, Optional pControl, _
Optional ByVal pAlignment As Integer = 0, _
Optional ByVal pBackColor, _
Optional ByVal pForeColor, _
Optional ByVal pBorderColor, _
Optional pFontObj As StdFont, _
Optional ByVal TooltipID As String)
    FrmAppLink.ShowToast pText, pWidth, pTime, pControl, pAlignment, _
    pBackColor, pForeColor, pBorderColor, pFontObj, TooltipID
End Sub

Public Sub ReinicioAlternoBUSINESS()
    FrmAppLink.ReinicioAlternoBUSINESS
End Sub

Public Sub OrdenarOperaciones(ByVal OperatorChar As String, _
ByRef FieldItem, ByRef NumOperations As Long)
    FrmAppLink.OrdenarOperaciones OperatorChar, FieldItem, NumOperations
End Sub

Public Sub ReemplazarVariableRecordset(ByRef TmpItem, pRs As ADODB.Recordset, _
CurrentOperationTag, VarRsTag)
    FrmAppLink.ReemplazarVariableRecordset TmpItem, pRs, CurrentOperationTag, VarRsTag
End Sub

Public Sub AplicarFormatoVariable(ByRef TmpItem, CurrentOperationTag, _
FormatInitTag, FormatTag, FormatEndTag)
    FrmAppLink.AplicarFormatoVariable TmpItem, CurrentOperationTag, FormatInitTag, FormatTag, FormatEndTag
End Sub

Public Sub ReemplazarSecuenciaVariable(ByRef TmpItem, CurrentOperationTag, _
ReplaceInitTag, ReplaceTargetTag, ReplaceSecuenceTag, ReplaceEndTag)
    FrmAppLink.ReemplazarSecuenciaVariable TmpItem, CurrentOperationTag, _
    ReplaceInitTag, ReplaceTargetTag, ReplaceSecuenceTag, ReplaceEndTag
End Sub

Public Sub AlinearVariable(ByRef TmpItem, CurrentOperationTag, _
PadInitTag, PadNumTag, PadEndTag, PadLeftTag, PadRightTag)
    FrmAppLink.AlinearVariable TmpItem, CurrentOperationTag, _
    PadInitTag, PadNumTag, PadEndTag, PadLeftTag, PadRightTag
End Sub

Public Sub TrimText(ByRef TmpItem, _
CurrentOperationTag, TrimSpacesTag)
    FrmAppLink.TrimText TmpItem, CurrentOperationTag, TrimSpacesTag
End Sub

Public Sub LeftText(ByRef TmpItem, CurrentOperationTag, _
StrLeftInitTag, StrLeftTag, StrLeftEndTag)
    FrmAppLink.LeftText TmpItem, CurrentOperationTag, _
    StrLeftInitTag, StrLeftTag, StrLeftEndTag
End Sub

Public Sub RightText(ByRef TmpItem, CurrentOperationTag, _
StrRightInitTag, StrRightTag, StrRightEndTag)
    FrmAppLink.RightText TmpItem, CurrentOperationTag, _
    StrRightInitTag, StrRightTag, StrRightEndTag
End Sub

Public Function RellenarCadenasSeparadas(ByVal pCadenaIzq As String, ByVal pCadenaDer As String, _
ByVal pLongitud As Long, _
Optional ByVal pChar As String = " ", _
Optional ByVal TruncarSiNoHayEspacioRestante As Boolean = True) As String
    RellenarCadenasSeparadas = FrmAppLink.RellenarCadenasSeparadas(pCadenaIzq, pCadenaDer, _
    pLongitud, pChar, TruncarSiNoHayEspacioRestante)
End Function

Public Function RellenarCadena_ALaIzq(ByVal pCadena As String, ByVal pLongitud As Long, _
Optional pChar As String = " ") As String
    RellenarCadena_ALaIzq = FrmAppLink.RellenarCadena_ALaIzq(pCadena, pLongitud, pChar)
End Function

Public Function RellenarCadena_ALaDer(ByVal pCadena As String, ByVal pLongitud As Long, _
Optional pChar As String = " ") As String
    RellenarCadena_ALaDer = FrmAppLink.RellenarCadena_ALaDer(pCadena, pLongitud, pChar)
End Function

Public Function CambiarImpresora(ByVal pDeviceNameOrPort As String, _
Optional ByVal pReestablecerOriginal As Boolean = False, _
Optional ByVal pPorPuerto As Boolean = False) As Boolean
    CambiarImpresora = FrmAppLink.CambiarImpresora(pDeviceNameOrPort, _
    pReestablecerOriginal, pPorPuerto)
End Function

Public Function ReturnNonAlpha(ByVal sString As String) As String
   ReturnNonAlpha = FrmAppLink.ReturnNonAlpha(sString)
End Function

Public Function GetWordTemplateApplication(ByVal pRuta As String) As Object
    GetWordTemplateApplication = FrmAppLink.GetWordTemplateApplication(pRuta)
End Function

Public Sub ReemplazarTextoWord(ByVal pDocumento As Object, _
ByVal pTextoBuscar As String, ByVal pTextoNuevo As String, _
Optional ByVal IncludeTextBoxes As Boolean = True)
    FrmAppLink.ReemplazarTextoWord pDocumento, _
    pTextoBuscar, pTextoNuevo, IncludeTextBoxes
End Sub

Public Sub DGS(pForma As Object, ByVal pTitulo As String, ByVal pTabla As String)
    
    Set Forma = pForma
    Titulo = pTitulo
    Tabla = pTabla
    
    Call FrmAppLink.DGS(Forma, Titulo, Tabla)
    
End Sub

Public Sub Carga_DGS(Campo As Object, ByVal Tabla_Busq As String, ByVal Quien As Integer)
    FrmAppLink.Carga_DGS Campo, Tabla_Busq, Quien
End Sub

Public Sub MAKE_VIEW(ByRef nTabla, nCampo1, nCampo2, nTitulo, Name_Form As Form, COMENTARIO As String, _
Optional ByVal BusquedaRapida As Boolean = False, _
Optional pCampo_Txt As Object, _
Optional pCampo_Lbl As Object)
    
    Set Campo_Txt = pCampo_Txt
    Set Campo_Lbl = pCampo_Lbl
    
    FrmAppLink.MAKE_VIEW nTabla, nCampo1, nCampo2, nTitulo, Name_Form, COMENTARIO, BusquedaRapida, pCampo_Txt, pCampo_Lbl
    
End Sub

Public Function No_Consecutivo(ByVal Campo_Cons As String, _
Optional ByVal pAvance As Boolean = True, _
Optional pConexion As Object = Nothing, _
Optional ByVal pBD As String = "")
    No_Consecutivo = FrmAppLink.No_Consecutivo(Campo_Cons, pAvance, pConexion, pBD)
End Function

Public Sub DatePicker_FechaHora(pTxtObj As Object, _
Optional ByVal pTitulo As String, _
Optional ByVal pDiaInicioSemana As MSComCtl2.DayConstants = mvwMonday)
    FrmAppLink.DatePicker_FechaHora pTxtObj, pTitulo, pDiaInicioSemana
End Sub

Public Sub DatePicker_Fecha(pTxtObj As Object, _
Optional ByVal pTitulo As String, _
Optional ByVal pDiaInicioSemana As MSComCtl2.DayConstants = mvwMonday)
    FrmAppLink.DatePicker_Fecha pTxtObj, pTitulo, pDiaInicioSemana
End Sub

Public Sub DatePicker_Hora(pTxtObj As Object)
    FrmAppLink.DatePicker_Hora pTxtObj
End Sub

Public Function ManejaSucursales(pConnection) As Boolean
    ManejaSucursales = FrmAppLink.ManejaSucursales(pConnection)
End Function

Public Function ManejaPOS(pConnection) As Boolean
    ManejaPOS = FrmAppLink.ManejaPOS(pConnection)
End Function

Public Function InsertarAuditoria(ByVal TipoAuditoria As Long, _
ByVal DescripcionAuditoria As String, _
ByVal DescripcionEvento As String, _
ByVal Ventana As String, _
ByVal TipoObjeto As String, _
ByVal CodigoAfectado As String, _
ByRef Conexion As Object) As Boolean
    
    InsertarAuditoria = FrmAppLink.InsertarAuditoria(TipoAuditoria, DescripcionAuditoria, _
    DescripcionEvento, Ventana, TipoObjeto, CodigoAfectado, Conexion)
    
End Function

Public Sub SafeFocus(ByVal pObj As Object)
    FrmAppLink.SafeFocus pObj
End Sub

Public Sub ReimprimirFormulaProduccion(tipo As String, Formula As String, _
Optional Numero As Integer = 0, Optional TipoImpresion As String)
    FrmAppLink.ReimprimirFormulaProduccion tipo, Formula, Numero, TipoImpresion
End Sub

Public Function MonedaProducto(ByVal pCodigo As String, pCn As Object) As String
    MonedaProducto = FrmAppLink.MonedaProducto(pCodigo, pCn)
End Function

Public Function FactorMonedaProducto(ByVal pCodigo As String, pCn As Object) As Double
    FactorMonedaProducto = FrmAppLink.FactorMonedaProducto(pCodigo, pCn)
End Function

Public Sub MostrarDocumentoStellar(ByVal pConcepto As String, _
ByVal pStatus As String, ByVal pDocumento As String, _
Optional BackToDLL As Object = Nothing)
    FrmAppLink.MostrarDocumentoStellar pConcepto, pStatus, pDocumento, BackToDLL
End Sub

Public Sub RefreshForm(pForm As Object)
    FrmAppLink.RefreshForm pForm
End Sub

Public Function SpaceToUnderscore(ByVal pText) As String
    SpaceToUnderscore = FrmAppLink.SpaceToUnderscore(pText)
End Function

Public Function NoSpace(ByVal pText) As String
    NoSpace = FrmAppLink.NoSpace(pText)
End Function

Function Buscar_Moneda(Optional Campo2 As Control, _
Optional Campo3 As Control, _
Optional Campo4 As Control, _
Optional Prede As Boolean, _
Optional val_moneda As String, _
Optional Asignar As Boolean = False, _
Optional OBJETOS As Boolean = False) As Boolean
    Buscar_Moneda = FrmAppLink.Buscar_Moneda(Campo2, Campo3, Campo4, Prede, val_moneda, Asignar, OBJETOS)
End Function

Public Sub Consulta_F2(Name_Form As Form, COMENTARIO As String, Quien As String, _
Optional pCampo_Txt As Object, _
Optional pCampo_Lbl As Object)
    FrmAppLink.Consulta_F2 Name_Form, COMENTARIO, Quien, pCampo_Txt, pCampo_Lbl
End Sub

Public Sub MAKE_DOC(ByRef nTabla, nCampo1, nCampo2, NCAMPO3, nTitulo, Name_Form As Form, COMENTARIO As String, Concepto As String, Estatus As String, ANULAR_DOC As Boolean, IMPRIMIR_DOC As Boolean, Optional pPrechequeo As Boolean, Optional pBkOrderOdc As Boolean = False, Optional NCAMPO4 As String = "")
    FrmAppLink.MAKE_DOC nTabla, nCampo1, nCampo2, NCAMPO3, nTitulo, Name_Form, COMENTARIO, Concepto, Estatus, ANULAR_DOC, IMPRIMIR_DOC, pPrechequeo, pBkOrderOdc, NCAMPO4
End Sub
                        
Public Sub MAKE_DOC2(ByRef nTabla, nCampo1, nCampo2, NCAMPO3, nTitulo, Name_Form As Form, COMENTARIO As String, Concepto As String, Estatus As String, ANULAR_DOC As Boolean, IMPRIMIR_DOC As Boolean, Optional pPrechequeo As Boolean, Optional pBkOrderOdc As Boolean = False, Optional NCAMPO4 As String = "")
    FrmAppLink.MAKE_DOC2 nTabla, nCampo1, nCampo2, NCAMPO3, nTitulo, Name_Form, COMENTARIO, Concepto, Estatus, ANULAR_DOC, IMPRIMIR_DOC, pPrechequeo, pBkOrderOdc, NCAMPO4
End Sub

Public Function CheckCad(Texto1 As Object, Optional CantDec As Integer = 2, Optional Mensaje As String, Optional Porcent As Boolean = True) As Boolean
    CheckCad = FrmAppLink.CheckCad(Texto1, CantDec, Mensaje, Porcent)
End Function

Public Sub Produccion_Reimprimir_Formula(tipo As String, Formula As String, _
Optional Numero As Integer = 0, Optional TipoImpresion As String)
    FrmAppLink.Produccion_Reimprimir_Formula tipo, Formula, Numero, TipoImpresion
End Sub

Public Sub PasarTrPend(ByVal pTabla, ByRef pRsOringen As ADODB.Recordset, ByVal pCnnDestino As ADODB.Connection, _
ByVal pCnnVeriPosSuc As ADODB.Connection, _
Optional ByVal pCamposExepto = "", _
Optional ByVal pCamposAdicionales = "", _
Optional ByVal pEliminarOringen As Boolean = False)
    FrmAppLink.PasarTrPend pTabla, pRsOringen, pCnnDestino, pCnnVeriPosSuc, pCamposExepto, pCamposAdicionales, pEliminarOringen
End Sub

Public Sub CambiarColorFila(pGrid As MSFlexGrid, pFila As Long, pColor As Long)
    FrmAppLink.CambiarColorFila pGrid, pFila, pColor
End Sub

Public Function ShapeStrAdv(SQLStr1 As String, Alias1 As String, _
SQLStr2() As String, Alias2() As String, Campo1() As String, _
Campo2() As String, AliasRel() As String) As String
    ShapeStrAdv = FrmAppLink.ShapeStrAdv(SQLStr1, Alias1, _
    SQLStr2(), Alias2(), Campo1(), Campo2(), AliasRel())
End Function

Public Function ShapeStrAdvGC(SQLStr1 As String, Alias1 As String, SQLStr2 As String, Alias2 As String, _
Campo1 As String, Campo2 As String, AliasRel As String, _
Optional OpcionesAdicionales As String = "") As String
    ShapeStrAdvGC = FrmAppLink.ShapeStrAdvGC(SQLStr1, Alias1, SQLStr2, Alias2, _
    Campo1, Campo2, AliasRel, OpcionesAdicionales)
End Function

Public Function ShapeStrAdvGCR(SQLStr1 As String, Alias1 As String, SQLStr2 As String, Alias2 As String, _
pRelacion As String, AliasRel As String, Optional OpcionesAdicionales As String = "") As String
    ShapeStrAdvGCR = FrmAppLink.ShapeStrAdvGC(SQLStr1, Alias1, SQLStr2, Alias2, _
    pRelacion, AliasRel, OpcionesAdicionales)
End Function

Public Sub REPO_CABE(ByRef Reporte, Titulo, Optional pMayuscula As Boolean = True)
    FrmAppLink.REPO_CABE Reporte, Titulo, pMayuscula
End Sub

Public Sub Crear_Xml_Definido(Rs1, Ruta As String, ByVal Campos As Variant, _
Optional ByVal MantenerOrdenCampos As Boolean = False)
    FrmAppLink.Crear_Xml_Definido Rs1, Ruta, Campos, MantenerOrdenCampos
End Sub

Public Sub Crear_Xml_Definido_V2(Rs1, Ruta As String, ByVal Campos As Variant, _
Optional ByVal MantenerOrdenCampos As Boolean = False)
    FrmAppLink.Crear_Xml_Definido_V2 Rs1, Ruta, Campos, MantenerOrdenCampos
End Sub

Public Function PedirConceptoUnidadProceso( _
Optional ByVal pIDProceso As String, _
Optional ByVal pNombreProceso As String _
) As Variant
    PedirConceptoUnidadProceso = FrmAppLink.PedirConceptoUnidadProceso(pIDProceso, pNombreProceso)
End Function

' -------------------- FUNCIONES API --------------------

Function FindWindowLike(ByVal WindowText As String, ByVal ClassName As String, _
ByRef alMatchingHwnds() As Long, _
Optional ByVal lStartHwnd As Long = 0, _
Optional ByVal lChildID As Long = -1) As Integer
    FindWindowLike = FrmAppLink.FindWindowLike(WindowText, ClassName, alMatchingHwnds, lStartHwnd, lChildID)
End Function
 
Public Function ShellEx(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    ShellEx = FrmAppLink.ShellEx(hWnd, lpOperation, lpFile, lpParameters, lpDirectory, nShowCmd)
End Function

Public Function ShellExInfo(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    ShellExInfo = FrmAppLink.ShellExInfo(hWnd, lpOperation, _
    lpFile, lpParameters, lpDirectory, nShowCmd)
End Function

'Private Function WindowTitle(ByVal hWnd As Long) As String
    'WindowTitle = FrmAppLink.WindowTitle(hWnd)
'End Function

Public Function ClassName(ByVal Title As String) As String
    ClassName = FrmAppLink.ClassName(Title)
End Function

Public Function FindHandle(pClassName As String, _
Optional ByVal pTitle As String = "") As Long
    FindHandle = FrmAppLink.FindHandle(pClassName, pTitle)
End Function

Public Function IsAdmin() As Long
    IsAdmin = FrmAppLink.IsAdmin
End Function

Public Sub PosicionarVentana(ByVal hWnd As Long, ByVal pLeft As Long, ByVal pTop As Long)
    FrmAppLink.PosicionarVentana hWnd, pLeft, pTop
End Sub

Public Sub MostrarCalendario(pDatePicker As DTPicker)
    FrmAppLink.MostrarCalendario (pDatePicker)
End Sub

Public Function Reinicio_Aplicacion(ByVal pProceso As String, _
ByVal pAplicacion As String, _
Optional ByVal pControlID As Boolean = False, _
Optional ByVal pIDProceso As Long, _
Optional ByVal pTiempoEsperaenMilisegundos As Long = 2000) As Boolean
    Reinicio_Aplicacion = FrmAppLink.Reinicio_Aplicacion(pProceso, _
    pAplicacion, pControlID, pIDProceso, pTiempoEsperaenMilisegundos)
End Function

Public Function ChangePriority(dwPriorityClass As Long) As Boolean
    ChangePriority = FrmAppLink.ChangePriority(dwPriorityClass)
End Function

Public Function ShellAndWait(ByVal PathName As String, _
Optional ByVal WindowStyle As VbAppWinStyle = vbMinimizedFocus) As Double
    ShellAndWait = FrmAppLink.ShellAndWait(PathName, WindowStyle)
End Function

'Private Sub CBSelectExact(ByVal hwndCombo As Long, ByVal strSearch As String)
    'FrmAppLink.CBSelectExact hwndCombo, strSearch
'End Sub

Public Sub SafeSendKeys(ByVal Keys As String, Optional ByVal Wait)
    FrmAppLink.SafeSendKeys Keys, Wait
End Sub

Public Sub MoverVentana(hWnd As Long)
    FrmAppLink.MoverVentana hWnd
End Sub

Public Function GetProcessInfo(ByVal pID As Long) As Boolean
    GetProcessInfo = FrmAppLink.GetProcessInfo(pID)
End Function

Public Sub Sleep(ByVal dwMilliseconds As Long)
    FrmAppLink.Sleep dwMilliseconds
End Sub
