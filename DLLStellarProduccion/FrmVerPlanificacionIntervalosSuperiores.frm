VERSION 5.00
Begin VB.Form FrmVerPlanificacionIntervalosSuperiores 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10980
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   13860
   ControlBox      =   0   'False
   Icon            =   "FrmVerPlanificacionIntervalosSuperiores.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10980
   ScaleWidth      =   13860
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   5175
      Left            =   120
      TabIndex        =   32
      Top             =   1320
      Width           =   6135
      Begin VB.TextBox txtPresentacion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   60
         Text            =   "2000/01/01 23:59 AM"
         Top             =   3960
         Width           =   3135
      End
      Begin VB.TextBox txtCantibul 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   58
         Text            =   "2000/01/01 23:59 AM"
         Top             =   4560
         Width           =   3135
      End
      Begin VB.TextBox txtPlanificadoIntO 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   45
         Text            =   "2000/01/01 23:59 AM"
         Top             =   3360
         Width           =   4455
      End
      Begin VB.TextBox txtRequeridoIntO 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   43
         Text            =   "2000/01/01 23:59 AM"
         Top             =   2880
         Width           =   4455
      End
      Begin VB.TextBox txtCapacidadIntO 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   41
         Text            =   "2000/01/01 23:59 AM"
         Top             =   2400
         Width           =   4455
      End
      Begin VB.TextBox txtIntervaloOrigen 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   38
         Text            =   "2000/01/01 23:59 AM"
         Top             =   1680
         Width           =   5655
      End
      Begin VB.TextBox txtDescProd 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   240
         TabIndex        =   34
         Text            =   "Ejemplo"
         Top             =   840
         Width           =   5655
      End
      Begin VB.TextBox txtUnidadMedida 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   9600
         Width           =   3495
      End
      Begin VB.Label lblPresentacion 
         BackStyle       =   0  'Transparent
         Caption         =   "Unidad de Medida"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   59
         Top             =   3960
         Width           =   2415
      End
      Begin VB.Label lblCantibul 
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad por Empaque"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   57
         Top             =   4560
         Width           =   2415
      End
      Begin VB.Label lblPlanificadoIntO 
         BackStyle       =   0  'Transparent
         Caption         =   "Planificado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   44
         Top             =   3360
         Width           =   1215
      End
      Begin VB.Label lblRequeridoIntO 
         BackStyle       =   0  'Transparent
         Caption         =   "Requerido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label lblIntervaloOrigen 
         BackStyle       =   0  'Transparent
         Caption         =   "Intervalo Seleccionado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   40
         Top             =   1320
         Width           =   2655
      End
      Begin VB.Label lblCapacidadIntO 
         BackStyle       =   0  'Transparent
         Caption         =   "Capacidad:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   39
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label lblDescProd 
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   480
         Width           =   4455
      End
      Begin VB.Label lblLineaPrd 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   240
         TabIndex        =   36
         Top             =   120
         Width           =   750
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   1320
         X2              =   5750
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label lblUnidadMedida 
         BackStyle       =   0  'Transparent
         Caption         =   "Unidad de Medida o Procesamiento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   9240
         Width           =   3855
      End
   End
   Begin VB.Frame FrameOtros 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   2580
      TabIndex        =   7
      Top             =   660
      Visible         =   0   'False
      Width           =   2295
      Begin VB.Label lblOtros 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Materia Prima"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   8
         Top             =   30
         Width           =   1995
      End
   End
   Begin VB.Frame FrameHistorial 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   5040
      TabIndex        =   5
      Top             =   660
      Visible         =   0   'False
      Width           =   2295
      Begin VB.Label lblHistorial 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Historial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   9
         Top             =   30
         Visible         =   0   'False
         Width           =   1995
      End
   End
   Begin VB.Frame FramePlan 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   660
      Width           =   2295
      Begin VB.Label lblPlanificacion 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "General"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   4
         Top             =   30
         Width           =   1995
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   540
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13920
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   10215
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   11235
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   13200
         Picture         =   "FrmVerPlanificacionIntervalosSuperiores.frx":628A
         Top             =   -30
         Width           =   480
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   9345
      Left            =   6360
      TabIndex        =   6
      Top             =   1320
      Width           =   7335
      Begin VB.Frame FrameInt3 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Height          =   2895
         Left            =   120
         TabIndex        =   46
         Top             =   6240
         Visible         =   0   'False
         Width           =   6975
         Begin VB.TextBox txtPlanificadoInt3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   51
            Text            =   "2000/01/01 23:59 AM"
            Top             =   2280
            Width           =   5415
         End
         Begin VB.TextBox txtRequeridoInt3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   50
            Text            =   "2000/01/01 23:59 AM"
            Top             =   1800
            Width           =   5415
         End
         Begin VB.TextBox txtCapacidadInt3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   49
            Text            =   "2000/01/01 23:59 AM"
            Top             =   1320
            Width           =   5415
         End
         Begin VB.TextBox lblInt3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   48
            Text            =   "2000/01/01 23:59 AM"
            Top             =   600
            Width           =   6615
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   47
            Top             =   9600
            Width           =   3495
         End
         Begin VB.Label lblPlanificadoInt3 
            BackStyle       =   0  'Transparent
            Caption         =   "Planificado:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   56
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblRequeridoInt3 
            BackStyle       =   0  'Transparent
            Caption         =   "Requerido:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   55
            Top             =   1800
            Width           =   1215
         End
         Begin VB.Label lblCapacidadInt3 
            BackStyle       =   0  'Transparent
            Caption         =   "Capacidad:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   54
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label lblDescInt3 
            BackStyle       =   0  'Transparent
            Caption         =   "A�o"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   53
            Top             =   120
            Width           =   2655
         End
         Begin VB.Label Label4 
            BackStyle       =   0  'Transparent
            Caption         =   "Unidad de Medida o Procesamiento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   120
            TabIndex        =   52
            Top             =   9240
            Width           =   3855
         End
      End
      Begin VB.Frame FrameInt2 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Height          =   2895
         Left            =   120
         TabIndex        =   21
         Top             =   3240
         Visible         =   0   'False
         Width           =   6975
         Begin VB.TextBox Text9 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   26
            Top             =   9600
            Width           =   3495
         End
         Begin VB.TextBox lblInt2 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   25
            Text            =   "2000/01/01 23:59 AM"
            Top             =   600
            Width           =   6615
         End
         Begin VB.TextBox txtCapacidadInt2 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   24
            Text            =   "2000/01/01 23:59 AM"
            Top             =   1320
            Width           =   5415
         End
         Begin VB.TextBox txtRequeridoInt2 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   23
            Text            =   "2000/01/01 23:59 AM"
            Top             =   1800
            Width           =   5415
         End
         Begin VB.TextBox txtPlanificadoInt2 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   22
            Text            =   "2000/01/01 23:59 AM"
            Top             =   2280
            Width           =   5415
         End
         Begin VB.Label Label10 
            BackStyle       =   0  'Transparent
            Caption         =   "Unidad de Medida o Procesamiento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   120
            TabIndex        =   31
            Top             =   9240
            Width           =   3855
         End
         Begin VB.Label lblDescInt2 
            BackStyle       =   0  'Transparent
            Caption         =   "Mes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   30
            Top             =   120
            Width           =   2655
         End
         Begin VB.Label lblCapacidadInt2 
            BackStyle       =   0  'Transparent
            Caption         =   "Capacidad:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label lblRequeridoInt2 
            BackStyle       =   0  'Transparent
            Caption         =   "Requerido:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   28
            Top             =   1800
            Width           =   1215
         End
         Begin VB.Label lblPlanificadoInt2 
            BackStyle       =   0  'Transparent
            Caption         =   "Planificado:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   27
            Top             =   2280
            Width           =   1215
         End
      End
      Begin VB.Frame FrameInt1 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Height          =   2895
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Visible         =   0   'False
         Width           =   6975
         Begin VB.TextBox txtPlanificadoInt1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   20
            Text            =   "2000/01/01 23:59 AM"
            Top             =   2280
            Width           =   5415
         End
         Begin VB.TextBox txtRequeridoInt1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   18
            Text            =   "2000/01/01 23:59 AM"
            Top             =   1800
            Width           =   5415
         End
         Begin VB.TextBox txtCapacidadInt1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   16
            Text            =   "2000/01/01 23:59 AM"
            Top             =   1320
            Width           =   5415
         End
         Begin VB.TextBox lblInt1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   14
            Text            =   "2000/01/01 23:59 AM"
            Top             =   600
            Width           =   6615
         End
         Begin VB.TextBox Text8 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   11
            Top             =   9600
            Width           =   3495
         End
         Begin VB.Label lblPlanificadoInt1 
            BackStyle       =   0  'Transparent
            Caption         =   "Planificado:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblRequeridoInt1 
            BackStyle       =   0  'Transparent
            Caption         =   "Requerido:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   17
            Top             =   1800
            Width           =   1215
         End
         Begin VB.Label lblCapacidadInt1 
            BackStyle       =   0  'Transparent
            Caption         =   "Capacidad:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label lblDescInt1 
            BackStyle       =   0  'Transparent
            Caption         =   "Semana"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   13
            Top             =   120
            Width           =   2655
         End
         Begin VB.Label Label9 
            BackStyle       =   0  'Transparent
            Caption         =   "Unidad de Medida o Procesamiento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   9240
            Width           =   3855
         End
      End
   End
End
Attribute VB_Name = "FrmVerPlanificacionIntervalosSuperiores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada        As Boolean

Public CodProducto          As String
Public Descripcion          As String
Public Cantibul             As Double
Public Presentacion         As String
Public CantDec              As Integer
Public CapacidadEmpBase     As Double
Public CapacidadUniBase     As Double

Public CapacidadPeriodo     As String
Public CapacidadEmp         As String
Public CapacidadUni         As String

Public RequeridosPeriodo    As String
Public PlanificadosPeriodo  As String

Public TipoPeriodo          As String
Public Periodo              As String
Public DatosPeriodo         As Dictionary

Public FormaDeTrabajo       As Integer
Public MostrarCantibul      As Boolean

Dim nDesde As Double, nHasta As Double
Dim fDesde As Double, fHasta As Double

Private Sub CmdSalir_Click()
    mGrabo = False
    Unload Me
End Sub

Private Sub Exit_Click()
    CmdSalir_Click
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        txtDescProd = Descripcion
        txtPresentacion = Presentacion
        txtCantibul = Cantibul
        txtCantibul.Visible = MostrarCantibul
        
        Select Case TipoPeriodo
            
            Case "D"
                
                txtIntervaloOrigen = Periodo
                
                mEmpaques = CapacidadEmp
                mUnidades = CapacidadUni
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadIntO.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadIntO.Text = "N/A"
                End If
                
                txtRequeridoIntO.Text = RequeridosPeriodo
                txtPlanificadoIntO = PlanificadosPeriodo
                
                FrameInt1.Visible = True
                
                lblDescInt1 = "Semana"
                
                If FrmPlanificacionProduccion.OptDiaInicioSemana(0).Value Then
                    DiaSemanaVB = vbSunday
                    DiaSemanaSQL = 7
                Else
                    DiaSemanaVB = vbMonday
                    DiaSemanaSQL = 1
                End If
                
                nDesde = DatosPeriodo("nPeriodoIni")
                fDesde = DatosPeriodo("fPeriodoIni")
                NumWDay = Weekday(fDesde, DiaSemanaVB)
                If NumWDay > 1 Then nDesde = CDbl(DateAdd("d", -1 * (NumWDay - 1), fDesde))
                fDesde = CDate(nDesde)
                nHasta = nDesde + 6
                fHasta = CDate(nHasta)
                lblInt1.Tag = nDesde
                lblInt1.Text = Replace(SDate(fDesde), Year(fDesde), Format(fDesde, "yy")) & _
                " a " & _
                Replace(SDate(fHasta), Year(fHasta), Format(fHasta, "yy")) & _
                "  (" & DatePart("ww", fDesde, DiaSemanaVB, vbFirstJan1) & ")"
                lblDescInt1.Tag = nHasta
                
                FrameInt2.Visible = True
                
                lblDescInt2 = "Mes"
                
                fDesde = DatosPeriodo("fPeriodoIni")
                nDesde = CDbl(DateSerial(Year(fDesde), Month(fDesde), 1))
                fDesde = CDate(nDesde)
                lblInt2.Tag = nDesde
                nHasta = CDbl(DateAdd("d", -1, DateAdd("M", 1, DateSerial(Year(fDesde), Month(fDesde), 1))))
                lblDescInt2.Tag = nHasta
                lblInt2.Text = MonthName(Month(fDesde)) & " " & Year(fDesde)
                
                FrameInt3.Visible = True
                
                lblDescInt3 = "A�o"
                
                fDesde = DatosPeriodo("fPeriodoIni")
                nDesde = CDbl(DateSerial(Year(fDesde), 1, 1))
                fDesde = CDate(nDesde)
                lblInt3.Tag = nDesde
                nHasta = CDbl(DateAdd("d", -1, DateAdd("yyyy", 1, DateSerial(Year(fDesde), 1, 1))))
                lblDescInt3.Tag = nHasta
                lblInt3.Text = Year(fDesde)
                
            Case "W"
                
                txtIntervaloOrigen = Periodo
                
                mEmpaques = CapacidadEmp
                mUnidades = CapacidadUni
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadIntO.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadIntO.Text = "N/A"
                End If
                
                txtRequeridoIntO.Text = RequeridosPeriodo
                txtPlanificadoIntO = PlanificadosPeriodo
                
                FrameInt1.Visible = True
                
                lblDescInt1 = "Mes"
                
                fDesde = DatosPeriodo("fPeriodoIni")
                nDesde = CDbl(DateSerial(Year(fDesde), Month(fDesde), 1))
                fDesde = CDate(nDesde)
                lblInt1.Tag = nDesde
                nHasta = CDbl(DateAdd("d", -1, DateAdd("M", 1, DateSerial(Year(fDesde), Month(fDesde), 1))))
                lblDescInt1.Tag = nHasta
                lblInt1.Text = MonthName(Month(fDesde)) & " " & Year(fDesde)
                
                FrameInt2.Visible = True
                
                lblDescInt2 = "A�o"
                
                fDesde = DatosPeriodo("fPeriodoIni")
                nDesde = CDbl(DateSerial(Year(fDesde), 1, 1))
                fDesde = CDate(nDesde)
                lblInt2.Tag = nDesde
                nHasta = CDbl(DateAdd("d", -1, DateAdd("yyyy", 1, DateSerial(Year(fDesde), 1, 1))))
                lblDescInt2.Tag = nHasta
                lblInt2.Text = Year(fDesde)
                
                
            Case "M"
                
                txtIntervaloOrigen = Periodo
                
                mEmpaques = CapacidadEmp
                mUnidades = CapacidadUni
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadIntO.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadIntO.Text = "N/A"
                End If
                
                txtRequeridoIntO.Text = RequeridosPeriodo
                txtPlanificadoIntO = PlanificadosPeriodo
                
                FrameInt1.Visible = True
                
                lblDescInt1 = "A�o"
                
                fDesde = DatosPeriodo("fPeriodoIni")
                nDesde = CDbl(DateSerial(Year(fDesde), 1, 1))
                fDesde = CDate(nDesde)
                lblInt1.Tag = nDesde
                nHasta = CDbl(DateAdd("d", -1, DateAdd("yyyy", 1, DateSerial(Year(fDesde), 1, 1))))
                lblDescInt1.Tag = nHasta
                lblInt1.Text = Year(fDesde)
                
            'Case "Y"
            
        End Select
        
        If CargarDatosPeriodo() Then
            'Exit Sub
        End If
        
    End If
    
End Sub

Private Function CargarDatosPeriodo() As Boolean
    
    On Error GoTo Error
    
        Select Case TipoPeriodo
            
            Case "D"
                
                mEmpaques = CapacidadEmpBase * FrmPlanificacionProduccion.DiasHabilesW
                mUnidades = CapacidadUniBase * FrmPlanificacionProduccion.DiasHabilesW
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadInt1.Text = "N/A"
                End If
                
                mCampos = IIf(False _
                Or False, _
                "*", "isNULL(SUM(nEmpaquesRequerido), 0) AS nEmpaquesRequerido, " & _
                "isNULL(SUM(nUnidadesRequerido), 0) AS nUnidadesRequerido")
                
                mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS MA " & _
                "WHERE c_CodArticulo = '" & CodProducto & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, "AND c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                mSql = mSql & " AND dFechaInicio BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' AND '" & FechaBD(EndOfDay(CDate(CDbl(lblDescInt1.Tag))), FBD_FULL) & "' AND dFechaFin BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' AND '" & FechaBD(CDate(CDbl(lblDescInt1.Tag)), FBD_FULL) & "' "
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mEmpaques = mRs!nEmpaquesRequerido
                    mUnidades = mRs!nUnidadesRequerido
                    mTotal = (mEmpaques * Cantibul) + mUnidades
                    mEmpaques = Fix(mTotal / Cantibul)
                    mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                    txtRequeridoInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtRequeridoInt1.Text = "N/A"
                End If
                
                mRs.Close
                
                mSql = "SELECT isNULL(SUM(n_Cantidad), 0) AS CantPeriodo " & _
                "FROM TR_ORDEN_PRODUCCION TR " & _
                "INNER JOIN MA_ORDEN_PRODUCCION MA " & _
                "ON TR.c_CodLocalidad = MA.c_CodLocalidad " & _
                "AND TR.c_Documento = MA.c_Documento " & _
                "WHERE b_Producir = 1 " & _
                "AND c_CodArticulo = '" & CodProducto & "' " & _
                "AND MA.c_Status IN ('DPE', 'DCO') " & _
                "AND MA.d_Fecha_Produccion BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' " & _
                "AND '" & FechaBD(CDate(CDbl(lblDescInt1.Tag)), FBD_FULL) & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, _
                "AND MA.c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mTotal = Round(mRs!CantPeriodo, CantDec)
                    mEmpRaw = (mTotal / Cantibul)
                    mEmpaques = Fix(mEmpRaw)
                    mUnidades = Round(Round(mEmpRaw - mEmpaques, 8) * Cantibul, CantDec)
                    txtPlanificadoInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtPlanificadoInt1.Text = "N/A"
                End If
                
                mRs.Close
                
                '
                
                mEmpaques = CapacidadEmpBase * FrmPlanificacionProduccion.DiasHabilesM
                mUnidades = CapacidadUniBase * FrmPlanificacionProduccion.DiasHabilesM
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadInt2.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadInt2.Text = "N/A"
                End If
                
                mCampos = IIf(False _
                Or False, _
                "*", "isNULL(SUM(nEmpaquesRequerido), 0) AS nEmpaquesRequerido, " & _
                "isNULL(SUM(nUnidadesRequerido), 0) AS nUnidadesRequerido")
                
                mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS MA " & _
                "WHERE c_CodArticulo = '" & CodProducto & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, "AND c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                mSql = mSql & " AND dFechaInicio BETWEEN '" & FechaBD(CDate(CDbl(lblInt2.Tag))) & "' AND '" & FechaBD(EndOfDay(CDate(CDbl(lblDescInt2.Tag))), FBD_FULL) & "' AND dFechaFin BETWEEN '" & FechaBD(CDate(CDbl(lblInt2.Tag))) & "' AND '" & FechaBD(CDate(CDbl(lblDescInt2.Tag)), FBD_FULL) & "' "
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mEmpaques = mRs!nEmpaquesRequerido
                    mUnidades = mRs!nUnidadesRequerido
                    mTotal = (mEmpaques * Cantibul) + mUnidades
                    mEmpaques = Fix(mTotal / Cantibul)
                    mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                    txtRequeridoInt2.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtRequeridoInt2.Text = "N/A"
                End If
                
                mRs.Close
                
                mSql = "SELECT isNULL(SUM(n_Cantidad), 0) AS CantPeriodo " & _
                "FROM TR_ORDEN_PRODUCCION TR " & _
                "INNER JOIN MA_ORDEN_PRODUCCION MA " & _
                "ON TR.c_CodLocalidad = MA.c_CodLocalidad " & _
                "AND TR.c_Documento = MA.c_Documento " & _
                "WHERE b_Producir = 1 " & _
                "AND c_CodArticulo = '" & CodProducto & "' " & _
                "AND MA.c_Status IN ('DPE', 'DCO') " & _
                "AND MA.d_Fecha_Produccion BETWEEN '" & FechaBD(CDate(CDbl(lblInt2.Tag))) & "' " & _
                "AND '" & FechaBD(CDate(CDbl(lblDescInt2.Tag)), FBD_FULL) & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, _
                "AND MA.c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mTotal = Round(mRs!CantPeriodo, CantDec)
                    mEmpRaw = (mTotal / Cantibul)
                    mEmpaques = Fix(mEmpRaw)
                    mUnidades = Round(Round(mEmpRaw - mEmpaques, 8) * Cantibul, CantDec)
                    txtPlanificadoInt2.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtPlanificadoInt2.Text = "N/A"
                End If
                
                mRs.Close
                
                '
                
                mEmpaques = CapacidadEmpBase * FrmPlanificacionProduccion.DiasHabilesY
                mUnidades = CapacidadUniBase * FrmPlanificacionProduccion.DiasHabilesY
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadInt3.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadInt3.Text = "N/A"
                End If
                
                mCampos = IIf(False _
                Or False, _
                "*", "isNULL(SUM(nEmpaquesRequerido), 0) AS nEmpaquesRequerido, " & _
                "isNULL(SUM(nUnidadesRequerido), 0) AS nUnidadesRequerido")
                
                mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS MA " & _
                "WHERE c_CodArticulo = '" & CodProducto & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, "AND c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                mSql = mSql & " AND dFechaInicio BETWEEN '" & FechaBD(CDate(CDbl(lblInt3.Tag))) & "' AND '" & FechaBD(EndOfDay(CDate(CDbl(lblDescInt3.Tag))), FBD_FULL) & "' AND dFechaFin BETWEEN '" & FechaBD(CDate(CDbl(lblInt3.Tag))) & "' AND '" & FechaBD(CDate(CDbl(lblDescInt3.Tag)), FBD_FULL) & "' "
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mEmpaques = mRs!nEmpaquesRequerido
                    mUnidades = mRs!nUnidadesRequerido
                    mTotal = (mEmpaques * Cantibul) + mUnidades
                    mEmpaques = Fix(mTotal / Cantibul)
                    mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                    txtRequeridoInt3.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtRequeridoInt3.Text = "N/A"
                End If
                
                mRs.Close
                
                mSql = "SELECT isNULL(SUM(n_Cantidad), 0) AS CantPeriodo " & _
                "FROM TR_ORDEN_PRODUCCION TR " & _
                "INNER JOIN MA_ORDEN_PRODUCCION MA " & _
                "ON TR.c_CodLocalidad = MA.c_CodLocalidad " & _
                "AND TR.c_Documento = MA.c_Documento " & _
                "WHERE b_Producir = 1 " & _
                "AND c_CodArticulo = '" & CodProducto & "' " & _
                "AND MA.c_Status IN ('DPE', 'DCO') " & _
                "AND MA.d_Fecha_Produccion BETWEEN '" & FechaBD(CDate(CDbl(lblInt2.Tag))) & "' " & _
                "AND '" & FechaBD(CDate(CDbl(lblDescInt2.Tag)), FBD_FULL) & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, _
                "AND MA.c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mTotal = Round(mRs!CantPeriodo, CantDec)
                    mEmpRaw = (mTotal / Cantibul)
                    mEmpaques = Fix(mEmpRaw)
                    mUnidades = Round(Round(mEmpRaw - mEmpaques, 8) * Cantibul, CantDec)
                    txtPlanificadoInt3.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtPlanificadoInt3.Text = "N/A"
                End If
                
                mRs.Close
                
            Case "W"
                
                mEmpaques = CapacidadEmpBase * FrmPlanificacionProduccion.DiasHabilesM
                mUnidades = CapacidadUniBase * FrmPlanificacionProduccion.DiasHabilesM
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadInt1.Text = "N/A"
                End If
                
                mCampos = IIf(False _
                Or False, _
                "*", "isNULL(SUM(nEmpaquesRequerido), 0) AS nEmpaquesRequerido, " & _
                "isNULL(SUM(nUnidadesRequerido), 0) AS nUnidadesRequerido")
                
                mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS MA " & _
                "WHERE c_CodArticulo = '" & CodProducto & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, "AND c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                mSql = mSql & " AND dFechaInicio BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' AND '" & FechaBD(EndOfDay(CDate(CDbl(lblDescInt1.Tag))), FBD_FULL) & "' AND dFechaFin BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' AND '" & FechaBD(CDate(CDbl(lblDescInt1.Tag)), FBD_FULL) & "' "
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mEmpaques = mRs!nEmpaquesRequerido
                    mUnidades = mRs!nUnidadesRequerido
                    mTotal = (mEmpaques * Cantibul) + mUnidades
                    mEmpaques = Fix(mTotal / Cantibul)
                    mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                    txtRequeridoInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtRequeridoInt1.Text = "N/A"
                End If
                
                mRs.Close
                
                mSql = "SELECT isNULL(SUM(n_Cantidad), 0) AS CantPeriodo " & _
                "FROM TR_ORDEN_PRODUCCION TR " & _
                "INNER JOIN MA_ORDEN_PRODUCCION MA " & _
                "ON TR.c_CodLocalidad = MA.c_CodLocalidad " & _
                "AND TR.c_Documento = MA.c_Documento " & _
                "WHERE b_Producir = 1 " & _
                "AND c_CodArticulo = '" & CodProducto & "' " & _
                "AND MA.c_Status IN ('DPE', 'DCO') " & _
                "AND MA.d_Fecha_Produccion BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' " & _
                "AND '" & FechaBD(CDate(CDbl(lblDescInt1.Tag)), FBD_FULL) & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, _
                "AND MA.c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mTotal = Round(mRs!CantPeriodo, CantDec)
                    mEmpRaw = (mTotal / Cantibul)
                    mEmpaques = Fix(mEmpRaw)
                    mUnidades = Round(Round(mEmpRaw - mEmpaques, 8) * Cantibul, CantDec)
                    txtPlanificadoInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtPlanificadoInt1.Text = "N/A"
                End If
                
                mRs.Close
                
                '
                
                mEmpaques = CapacidadEmpBase * FrmPlanificacionProduccion.DiasHabilesY
                mUnidades = CapacidadUniBase * FrmPlanificacionProduccion.DiasHabilesY
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadInt2.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadInt2.Text = "N/A"
                End If
                
                mCampos = IIf(False _
                Or False, _
                "*", "isNULL(SUM(nEmpaquesRequerido), 0) AS nEmpaquesRequerido, " & _
                "isNULL(SUM(nUnidadesRequerido), 0) AS nUnidadesRequerido")
                
                mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS MA " & _
                "WHERE c_CodArticulo = '" & CodProducto & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, "AND c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                mSql = mSql & " AND dFechaInicio BETWEEN '" & FechaBD(CDate(CDbl(lblInt2.Tag))) & "' AND '" & FechaBD(EndOfDay(CDate(CDbl(lblDescInt2.Tag))), FBD_FULL) & "' AND dFechaFin BETWEEN '" & FechaBD(CDate(CDbl(lblInt2.Tag))) & "' AND '" & FechaBD(CDate(CDbl(lblDescInt2.Tag)), FBD_FULL) & "' "
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mEmpaques = mRs!nEmpaquesRequerido
                    mUnidades = mRs!nUnidadesRequerido
                    mTotal = (mEmpaques * Cantibul) + mUnidades
                    mEmpaques = Fix(mTotal / Cantibul)
                    mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                    txtRequeridoInt2.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtRequeridoInt2.Text = "N/A"
                End If
                
                mRs.Close
                
                mSql = "SELECT isNULL(SUM(n_Cantidad), 0) AS CantPeriodo " & _
                "FROM TR_ORDEN_PRODUCCION TR " & _
                "INNER JOIN MA_ORDEN_PRODUCCION MA " & _
                "ON TR.c_CodLocalidad = MA.c_CodLocalidad " & _
                "AND TR.c_Documento = MA.c_Documento " & _
                "WHERE b_Producir = 1 " & _
                "AND c_CodArticulo = '" & CodProducto & "' " & _
                "AND MA.c_Status IN ('DPE', 'DCO') " & _
                "AND MA.d_Fecha_Produccion BETWEEN '" & FechaBD(CDate(CDbl(lblInt2.Tag))) & "' " & _
                "AND '" & FechaBD(CDate(CDbl(lblDescInt2.Tag)), FBD_FULL) & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, _
                "AND MA.c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mTotal = Round(mRs!CantPeriodo, CantDec)
                    mEmpRaw = (mTotal / Cantibul)
                    mEmpaques = Fix(mEmpRaw)
                    mUnidades = Round(Round(mEmpRaw - mEmpaques, 8) * Cantibul, CantDec)
                    txtPlanificadoInt2.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtPlanificadoInt2.Text = "N/A"
                End If
                
                mRs.Close
                
                
            Case "M"
                
                mEmpaques = CapacidadEmpBase * FrmPlanificacionProduccion.DiasHabilesY
                mUnidades = CapacidadUniBase * FrmPlanificacionProduccion.DiasHabilesY
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                If mTotal > 0 Then
                    txtCapacidadInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtCapacidadInt1.Text = "N/A"
                End If
                
                mCampos = IIf(False _
                Or False, _
                "*", "isNULL(SUM(nEmpaquesRequerido), 0) AS nEmpaquesRequerido, " & _
                "isNULL(SUM(nUnidadesRequerido), 0) AS nUnidadesRequerido")
                
                mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS MA " & _
                "WHERE c_CodArticulo = '" & CodProducto & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, "AND c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                mSql = mSql & " AND dFechaInicio BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' AND '" & FechaBD(EndOfDay(CDate(CDbl(lblDescInt1.Tag))), FBD_FULL) & "' AND dFechaFin BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' AND '" & FechaBD(CDate(CDbl(lblDescInt1.Tag)), FBD_FULL) & "' "
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mEmpaques = mRs!nEmpaquesRequerido
                    mUnidades = mRs!nUnidadesRequerido
                    mTotal = (mEmpaques * Cantibul) + mUnidades
                    mEmpaques = Fix(mTotal / Cantibul)
                    mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                    txtRequeridoInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtRequeridoInt1.Text = "N/A"
                End If
                
                mRs.Close
                
                mSql = "SELECT isNULL(SUM(n_Cantidad), 0) AS CantPeriodo " & _
                "FROM TR_ORDEN_PRODUCCION TR " & _
                "INNER JOIN MA_ORDEN_PRODUCCION MA " & _
                "ON TR.c_CodLocalidad = MA.c_CodLocalidad " & _
                "AND TR.c_Documento = MA.c_Documento " & _
                "WHERE b_Producir = 1 " & _
                "AND c_CodArticulo = '" & CodProducto & "' " & _
                "AND MA.c_Status IN ('DPE', 'DCO') " & _
                "AND MA.d_Fecha_Produccion BETWEEN '" & FechaBD(CDate(CDbl(lblInt1.Tag))) & "' " & _
                "AND '" & FechaBD(CDate(CDbl(lblDescInt1.Tag)), FBD_FULL) & "' " & _
                IIf(Trim(FrmPlanificacionProduccion.txt_sucursal.Text) <> Empty, _
                "AND MA.c_CodLocalidad = '" & FrmPlanificacionProduccion.txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & FrmPlanificacionProduccion.CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(FrmPlanificacionProduccion.CboTurno.Text) <> Empty, "AND (c_Turno = '" & FrmPlanificacionProduccion.CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    mTotal = Round(mRs!CantPeriodo, CantDec)
                    mEmpRaw = (mTotal / Cantibul)
                    mEmpaques = Fix(mEmpRaw)
                    mUnidades = Round(Round(mEmpRaw - mEmpaques, 8) * Cantibul, CantDec)
                    txtPlanificadoInt1.Text = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                Else
                    txtPlanificadoInt1.Text = "N/A"
                End If
                
                mRs.Close
                
            'Case "Y"
            
        End Select
    
    Exit Function
    
Error:
    
    CargarDatosLinea = False
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Function

Private Function GuardarDatosLinea() As Boolean
    
    On Error GoTo Error
    
    Dim mSql As String, mRsDatos As ADODB.Recordset
    
    With mRsDatos
        
        Apertura_RecordsetC mRsDatos
        
        mSql = "SELECT TOP 1 * FROM MA_LINEAPRODUCCION MA " & _
        "WHERE (MA.Descripcion = '" & QuitarComillasSimples(pDescripcion) & "' " & _
        Empty
        
        .Open mSql, FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not .EOF Then
            .AddNew
            !Descripcion = Descripcion_PK
            '!Fecha_Add = FechaBD(Now, FBD_FULL, True) ' GetDate()
            '!Fecha_Upd = FechaBD(Now, FBD_FULL, True) ' GetDate()
            !Cod_Usuario_Add = FrmAppLink.GetCodUsuario
            !Cod_Usuario_Upd = !Cod_Usuario_Add
            .UpdateBatch
            
        Else
            .Update
            !Fecha_Upd = FechaBD(Now, FBD_FULL, True) ' GetDate()
            !Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
        End If
        
        !UnidadDeMedida = Left(txtUnidadMedida, .Fields("UnidadDeMedida").DefinedSize)
        !CapacidadInstalada = Left(txtCapacidadInstalada, .Fields("CapacidadInstalada").DefinedSize)
        !CapacidadOperativaActual = Left(txtCapacidadOperativaActual, .Fields("CapacidadOperativaActual").DefinedSize)
        !EstadoFuncional = Left(CboEstatusFuncional.Text, .Fields("EstadoFuncional").DefinedSize)
        !Observacion = txtObs 'Left(txtObs, .Fields("Observacion").DefinedSize)
        
        .UpdateBatch
        
        GuardarDatosLinea = True
        
    End With
    
    Exit Function
    
Error:
    
    GuardarDatosLinea = False
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Function

Private Sub Form_Load()
    
    lbl_Organizacion = "Resumen de Fecha en intervalos superiores"
    
    AjustarPantalla Me
    
End Sub
