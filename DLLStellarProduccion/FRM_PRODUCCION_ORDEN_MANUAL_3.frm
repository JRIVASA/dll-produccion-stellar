VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FRM_PRODUCCION_ORDEN_MANUAL_3 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   405
   ClientWidth     =   15330
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   Tag             =   "FORMULAS"
   Begin VB.Frame FrameTabRegistroVSOrden 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   9000
      TabIndex        =   104
      Top             =   2520
      Width           =   4095
      Begin VB.Label lblDatosRegistroVSOrden 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Reporte de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   105
         Top             =   30
         Width           =   3855
      End
   End
   Begin VB.Frame FrameTabSumario 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   240
      TabIndex        =   42
      Top             =   2520
      Width           =   4095
      Begin VB.Label lblSumario 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Resumen de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   43
         Top             =   30
         Width           =   3855
      End
   End
   Begin VB.Frame FrameTabProceso 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   4800
      TabIndex        =   40
      Top             =   2520
      Width           =   3735
      Begin VB.Label lblProceso 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Detalle"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   41
         Top             =   30
         Width           =   3495
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   29
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.TextBox MSK_FACTOR 
      Alignment       =   1  'Right Justify
      CausesValidation=   0   'False
      Enabled         =   0   'False
      Height          =   315
      Left            =   5355
      TabIndex        =   25
      Text            =   "0"
      Top             =   1425
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox dbmoneda 
      Enabled         =   0   'False
      Height          =   315
      Left            =   315
      MaxLength       =   10
      TabIndex        =   24
      Top             =   1395
      Visible         =   0   'False
      Width           =   1365
   End
   Begin VB.Frame frame_datos 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ClipControls    =   0   'False
      Height          =   720
      Left            =   240
      TabIndex        =   22
      Top             =   1635
      Width           =   14775
      Begin VB.TextBox txtFormula 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   139
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   200
         Width           =   1275
      End
      Begin VB.CheckBox ModifFlag 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Trabajar sin formula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   11880
         TabIndex        =   2
         Top             =   240
         Width           =   2655
      End
      Begin VB.CommandButton CmdFormula 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   435
         Left            =   2640
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   180
         Width           =   555
      End
      Begin VB.TextBox txt_descripcion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4620
         MaxLength       =   50
         TabIndex        =   1
         Top             =   195
         Width           =   5865
      End
      Begin VB.Label lblFormula 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Formula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   34
         Top             =   240
         Width           =   750
      End
      Begin VB.Label lblDescFormula 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3405
         TabIndex        =   23
         Top             =   240
         Width           =   1170
      End
   End
   Begin VB.PictureBox CoolBar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1080
      ScaleWidth      =   21270
      TabIndex        =   20
      Top             =   421
      Width           =   21270
      Begin MSComctlLib.Toolbar BarraO 
         Height          =   810
         Left            =   120
         TabIndex        =   103
         Top             =   120
         Width           =   10140
         _ExtentX        =   17886
         _ExtentY        =   1429
         ButtonWidth     =   1746
         ButtonHeight    =   1429
         ToolTips        =   0   'False
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   9
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "F2 Buscar"
               ImageIndex      =   1
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   3
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "BORD"
                     Text            =   "Orden de Producci�n"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Object.Visible         =   0   'False
                     Key             =   "BPRD"
                     Text            =   "Produccion"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "BPRO"
                     Text            =   "Buscar Producto"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "F4 Grabar"
               ImageIndex      =   2
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   1
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "GORD"
                     Text            =   "Grabar Producci�n"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Anular"
               Key             =   "Anular"
               Object.ToolTipText     =   "F6 Anular"
               ImageIndex      =   3
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   1
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "AORD"
                     Text            =   "Producci�n"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Cancelar"
               Key             =   "Cancelar"
               Object.ToolTipText     =   "F7 Cancelar"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Reimprimir"
               Key             =   "Reimprimir"
               Object.ToolTipText     =   "F8 Reimprimir"
               ImageIndex      =   5
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   1
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "RORD"
                     Text            =   "Producci�n"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "F12 Salir"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Opciones"
               Key             =   "Opciones"
               Object.ToolTipText     =   "F1 Ayuda"
               ImageIndex      =   7
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Ayuda"
                     Text            =   "Ayuda"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Moneda"
                     Text            =   "Cambiar Moneda"
                  EndProperty
               EndProperty
            EndProperty
         EndProperty
      End
      Begin VB.Label lbl_fecha 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "01/01/2015"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   11040
         TabIndex        =   33
         Top             =   675
         Width           =   3495
      End
      Begin VB.Label lbl_consecutivo 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "000000000"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   12840
         TabIndex        =   32
         Top             =   195
         Width           =   1815
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   31
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lbl_concepto 
         BackStyle       =   0  'Transparent
         Caption         =   "Producci�n No:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   30
         Top             =   195
         Width           =   2415
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   1095
         Index           =   3
         Left            =   10440
         Top             =   0
         Width           =   4575
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":6A8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":881E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":A5B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":C342
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":E0D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":FE66
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":11BF8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtedit 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1980
      TabIndex        =   16
      Top             =   4320
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.Frame FrameSumario 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7695
      Left            =   240
      TabIndex        =   45
      Top             =   3000
      Visible         =   0   'False
      Width           =   14895
      Begin VB.TextBox txtMotivoParadaNoProgramada 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   9960
         TabIndex        =   185
         Text            =   "0.00"
         Top             =   7300
         Width           =   4455
      End
      Begin VB.TextBox txtHorasParadaNoProgramada 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   13320
         Locked          =   -1  'True
         TabIndex        =   183
         Text            =   "0.00"
         Top             =   6840
         Width           =   1095
      End
      Begin VB.TextBox txtHorasParadaProgramada 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   9960
         Locked          =   -1  'True
         TabIndex        =   181
         Text            =   "0.00"
         Top             =   6840
         Width           =   1095
      End
      Begin VB.TextBox txt_destino 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2190
         MaxLength       =   10
         TabIndex        =   169
         Top             =   1290
         Width           =   1500
      End
      Begin VB.TextBox txt_origen 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2190
         MaxLength       =   10
         TabIndex        =   168
         Top             =   720
         Width           =   1500
      End
      Begin VB.CommandButton cmd_origen 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3825
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":1398A
         Style           =   1  'Graphical
         TabIndex        =   167
         Top             =   720
         Width           =   360
      End
      Begin VB.CommandButton cmd_destino 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3825
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":1418C
         Style           =   1  'Graphical
         TabIndex        =   166
         Top             =   1290
         Width           =   360
      End
      Begin VB.ComboBox CboLnP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10335
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   720
         Visible         =   0   'False
         Width           =   3000
      End
      Begin VB.ComboBox CboTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10320
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1320
         Visible         =   0   'False
         Width           =   3000
      End
      Begin VB.TextBox txtPesoTotalMezcla 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         Locked          =   -1  'True
         TabIndex        =   99
         Text            =   "0.00"
         Top             =   4800
         Width           =   1695
      End
      Begin VB.TextBox txtPesoTotalProductos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         Locked          =   -1  'True
         TabIndex        =   95
         Text            =   "0.00"
         Top             =   4200
         Width           =   1695
      End
      Begin VB.TextBox txtCostosDirUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2880
         Locked          =   -1  'True
         TabIndex        =   93
         Text            =   "0.00"
         Top             =   3585
         Width           =   2235
      End
      Begin VB.TextBox txtCostoUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2880
         Locked          =   -1  'True
         TabIndex        =   89
         Text            =   "0.00"
         Top             =   3000
         Width           =   2235
      End
      Begin VB.Frame FrameCargarLotes 
         Appearance      =   0  'Flat
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   6800
         TabIndex        =   87
         Top             =   1920
         Width           =   1695
         Begin VB.Label lblCargarLotes 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Batches"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   88
            Top             =   30
            Width           =   1335
         End
      End
      Begin VB.Frame FrameMultiplicarFormula 
         Appearance      =   0  'Flat
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   4800
         TabIndex        =   85
         Top             =   1920
         Width           =   1695
         Begin VB.Label lblMultiplicarFormula 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Veces"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   86
            Top             =   30
            Width           =   1335
         End
      End
      Begin VB.TextBox txtMulti 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2880
         TabIndex        =   8
         Text            =   "1"
         Top             =   1920
         Width           =   1455
      End
      Begin VB.TextBox txtDescProducto 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   5
         Top             =   120
         Width           =   9520
      End
      Begin VB.TextBox txtProducir 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1440
         Locked          =   -1  'True
         MaxLength       =   20
         TabIndex        =   3
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   120
         Width           =   1275
      End
      Begin VB.CommandButton CmdProducir 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   435
         Left            =   3000
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_3.frx":1498E
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   90
         Width           =   555
      End
      Begin VB.TextBox txtCostoTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   78
         Text            =   "0.00"
         Top             =   1875
         Width           =   2235
      End
      Begin VB.TextBox txtHorasHombreXUnidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         TabIndex        =   11
         Text            =   "0.00"
         Top             =   6360
         Width           =   1695
      End
      Begin VB.TextBox txtCostoHoraCargaFabril 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         TabIndex        =   10
         Text            =   "0.00"
         Top             =   5880
         Width           =   1695
      End
      Begin VB.TextBox txtCostoHoraHombre 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         TabIndex        =   9
         Text            =   "0.00"
         Top             =   5400
         Width           =   1695
      End
      Begin VB.TextBox txtCantiLote 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   69
         Text            =   "0"
         Top             =   7200
         Width           =   1575
      End
      Begin VB.TextBox txtCostosDir 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   64
         Text            =   "0.00"
         Top             =   2520
         Width           =   2235
      End
      Begin VB.TextBox txtCapacidadLote 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   60
         Text            =   "0.00"
         Top             =   6600
         Width           =   1575
      End
      Begin VB.TextBox txtCapacidadMaximaLote 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         TabIndex        =   58
         Text            =   "0.00"
         Top             =   6000
         Width           =   1575
      End
      Begin VB.TextBox txtPesoEmp 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   56
         Text            =   "0.00"
         Top             =   5400
         Width           =   1575
      End
      Begin VB.TextBox txtCantibul 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   54
         Text            =   "1"
         Top             =   4800
         Width           =   1575
      End
      Begin VB.TextBox txtPesoUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   52
         Text            =   "0.00"
         Top             =   4200
         Width           =   1575
      End
      Begin VB.TextBox txtCantProducto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2880
         Locked          =   -1  'True
         TabIndex        =   48
         Text            =   "1.00"
         Top             =   2475
         Width           =   1515
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Motivo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   8400
         TabIndex        =   184
         Top             =   7320
         Width           =   795
      End
      Begin VB.Label lblHorasParadaNoProgramada 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "No Programada:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   11400
         TabIndex        =   182
         Top             =   6840
         Width           =   1755
      End
      Begin VB.Label lblHorasParadaProgramada 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Programada:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   8400
         TabIndex        =   180
         Top             =   6840
         Width           =   1380
      End
      Begin VB.Label lblParadas 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Paradas (Horas)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   6480
         TabIndex        =   179
         Top             =   6840
         Width           =   1695
      End
      Begin VB.Label lbl_origen 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4320
         TabIndex        =   173
         Top             =   720
         Width           =   3495
      End
      Begin VB.Label lbl_destino 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4320
         TabIndex        =   172
         Top             =   1290
         Width           =   3495
      End
      Begin VB.Label lblDestino 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito Destino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   360
         TabIndex        =   171
         Top             =   1350
         Width           =   1890
      End
      Begin VB.Label lblOrigen 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito Origen"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   360
         TabIndex        =   170
         Top             =   780
         Width           =   1905
      End
      Begin VB.Label lblLineaProduccion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "L�nea de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   8040
         TabIndex        =   102
         Top             =   750
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.Label lblTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Turno de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   8040
         TabIndex        =   101
         Top             =   1320
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.Label lblNomenclaturaPesoTotalMezcla 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   100
         Top             =   4800
         Width           =   345
      End
      Begin VB.Label lblPesoTotalMezcla 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Total de Mezcla: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   98
         Top             =   4800
         Width           =   2370
      End
      Begin VB.Label lblPesoTotalProductos 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Total de Productos: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   97
         Top             =   4200
         Width           =   2730
      End
      Begin VB.Label lblNomenclaturaPesoTotalProductos 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   96
         Top             =   4200
         Width           =   345
      End
      Begin VB.Label lblNomenclaturaCDirUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5280
         TabIndex        =   94
         Top             =   3630
         Width           =   1065
      End
      Begin VB.Label lblCostosDirUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costos Dir. por Und.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   92
         Top             =   3630
         Width           =   2280
      End
      Begin VB.Label lblCostoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo por Und.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   91
         Top             =   3045
         Width           =   1710
      End
      Begin VB.Label lblNomenclaturaCostoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5280
         TabIndex        =   90
         Top             =   3045
         Width           =   1065
      End
      Begin VB.Label lblAplicarFormula 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Aplicar Formula: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   84
         Top             =   1920
         Width           =   1845
      End
      Begin VB.Label lblDescProducto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3765
         TabIndex        =   83
         Top             =   180
         Visible         =   0   'False
         Width           =   1470
      End
      Begin VB.Label lblProducto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   360
         TabIndex        =   82
         Top             =   180
         Width           =   885
      End
      Begin VB.Label lblNomenclaturaCostoTotal 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13440
         TabIndex        =   79
         Top             =   1920
         Width           =   315
      End
      Begin VB.Label lblCostoTotal 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Total: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   9120
         TabIndex        =   77
         Top             =   1920
         Width           =   1380
      End
      Begin VB.Label lblNomenclaturaHorasHombreXUnidad 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Horas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   76
         Top             =   6360
         Width           =   615
      End
      Begin VB.Label lblHorasHombreXUnidad 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cant. Hora Hombre / Und.: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   75
         Top             =   6360
         Width           =   2985
      End
      Begin VB.Label lblNomenclaturaCostoHoraCargaFabril 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Hora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   74
         Top             =   5880
         Width           =   1065
      End
      Begin VB.Label lblCostoHoraCargaFabril 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Hora Carga Fabril: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   73
         Top             =   5880
         Width           =   2700
      End
      Begin VB.Label lblNomenclaturaCostoHoraHombre 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Hora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   72
         Top             =   5400
         Width           =   1065
      End
      Begin VB.Label lblCostoHoraHombre 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Hora Hombre: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   71
         Top             =   5400
         Width           =   2280
      End
      Begin VB.Label lblNomenclaturaUnidadesXLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   70
         Top             =   7200
         Width           =   510
      End
      Begin VB.Label lblUnidadesXLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cant. / Batch: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   68
         Top             =   7200
         Width           =   1530
      End
      Begin VB.Label lblNomenclaturaCantibul 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   67
         Top             =   4800
         Width           =   510
      End
      Begin VB.Label lblNomenclaturaPesoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   66
         Top             =   4200
         Width           =   345
      End
      Begin VB.Label lblNomenclaturaPesoEmp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   65
         Top             =   5400
         Width           =   345
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Unidad de Medida: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   4800
         TabIndex        =   63
         Top             =   2520
         Width           =   2070
      End
      Begin VB.Label lblNomenclaturaCapMaxLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   62
         Top             =   6000
         Width           =   345
      End
      Begin VB.Label lblNomenclaturaCapLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   61
         Top             =   6600
         Width           =   345
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Capacidad Batch: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   59
         Top             =   6600
         Width           =   1890
      End
      Begin VB.Label lblCapacidadMaximaLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cap. Max. Batch: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   57
         Top             =   6000
         Width           =   1860
      End
      Begin VB.Label lblPesoEmp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso por Empaque: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   55
         Top             =   5400
         Width           =   2160
      End
      Begin VB.Label lblCantibul 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und. por Empaque: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   53
         Top             =   4800
         Width           =   2175
      End
      Begin VB.Label lblPesoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso por Und: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   51
         Top             =   4200
         Width           =   1605
      End
      Begin VB.Label lblNomenclaturaCant 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   6960
         TabIndex        =   50
         Top             =   2520
         Width           =   1515
      End
      Begin VB.Label lblNomenclaturaCDir 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13440
         TabIndex        =   49
         Top             =   2520
         Width           =   315
      End
      Begin VB.Label lblCantidadProducto 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cantidad Producto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   47
         Top             =   2520
         Width           =   2040
      End
      Begin VB.Label lblTotalCostosDir 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costos Directos: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   9120
         TabIndex        =   46
         Top             =   2520
         Width           =   1800
      End
   End
   Begin VB.Frame FrameProceso 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7695
      Left            =   240
      TabIndex        =   35
      Top             =   3000
      Width           =   14895
      Begin VB.TextBox txtEdit2 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   1680
         TabIndex        =   81
         Top             =   3840
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.ComboBox CboPCD 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         Style           =   2  'Dropdown List
         TabIndex        =   80
         Top             =   6720
         Visible         =   0   'False
         Width           =   2760
      End
      Begin MSFlexGridLib.MSFlexGrid Ingredientes 
         CausesValidation=   0   'False
         Height          =   2505
         Left            =   0
         TabIndex        =   12
         Top             =   360
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   4419
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid TotalesIngredientes 
         CausesValidation=   0   'False
         Height          =   420
         Left            =   0
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   2925
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   741
         _Version        =   393216
         Rows            =   1
         Cols            =   8
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   0   'False
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid Empaques 
         CausesValidation=   0   'False
         Height          =   1305
         Left            =   0
         TabIndex        =   13
         Top             =   3430
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   2302
         _Version        =   393216
         Rows            =   1
         Cols            =   8
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid TotalesEmpaques 
         CausesValidation=   0   'False
         Height          =   420
         Left            =   0
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   4800
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   741
         _Version        =   393216
         Rows            =   1
         Cols            =   8
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   0   'False
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid CostosDirectos 
         CausesValidation=   0   'False
         Height          =   1785
         Left            =   120
         TabIndex        =   14
         Top             =   5760
         Width           =   6975
         _ExtentX        =   12303
         _ExtentY        =   3149
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid GrdTotales 
         CausesValidation=   0   'False
         Height          =   1785
         Left            =   7320
         TabIndex        =   15
         Top             =   5760
         Width           =   7335
         _ExtentX        =   12938
         _ExtentY        =   3149
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line LnProductosUtilizar 
         BorderColor     =   &H00AE5B00&
         X1              =   2505
         X2              =   14500
         Y1              =   165
         Y2              =   165
      End
      Begin VB.Label lblTotales 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Totales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7320
         TabIndex        =   44
         Top             =   5400
         Width           =   1005
      End
      Begin VB.Line LnTotales 
         BorderColor     =   &H00AE5B00&
         X1              =   8505
         X2              =   14500
         Y1              =   5565
         Y2              =   5565
      End
      Begin VB.Label lblCostosDir 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costos Directos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   120
         TabIndex        =   39
         Top             =   5400
         Width           =   1635
      End
      Begin VB.Line LnCostoDir 
         BorderColor     =   &H00AE5B00&
         X1              =   1905
         X2              =   7000
         Y1              =   5565
         Y2              =   5565
      End
      Begin VB.Label lblProductosUtilizar 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Productos a Utilizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   120
         TabIndex        =   36
         Top             =   0
         Width           =   2055
      End
   End
   Begin VB.Frame FrameRegistroVSOrden 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7695
      Left            =   240
      TabIndex        =   106
      Top             =   3000
      Visible         =   0   'False
      Width           =   14895
      Begin VB.TextBox txtDifBatch 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   9120
         Locked          =   -1  'True
         TabIndex        =   178
         Text            =   "0"
         Top             =   2520
         Width           =   975
      End
      Begin VB.TextBox txtDifCant 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   12840
         Locked          =   -1  'True
         TabIndex        =   176
         Text            =   "0"
         Top             =   2520
         Width           =   975
      End
      Begin VB.TextBox txtDiferenciaPesosTotalesReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   174
         Text            =   "0.00"
         Top             =   4200
         Width           =   1575
      End
      Begin VB.TextBox txtCostoUniReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   162
         Text            =   "0.00"
         Top             =   7200
         Width           =   1575
      End
      Begin VB.TextBox txtTotalHorasTrabajadas 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   161
         Text            =   "0"
         Top             =   6600
         Width           =   1575
      End
      Begin VB.TextBox txtNumHorasTrabajadas 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   159
         Text            =   "0"
         Top             =   6000
         Width           =   1575
      End
      Begin VB.TextBox txtNumTrabajadores 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   157
         Text            =   "0"
         Top             =   5400
         Width           =   1575
      End
      Begin VB.TextBox txtPorcVariacionPesosTotalesReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   155
         Text            =   "0"
         Top             =   4800
         Width           =   1575
      End
      Begin VB.TextBox txtPesoTotalProductoRepReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   150
         Text            =   "0.00"
         Top             =   3600
         Width           =   1575
      End
      Begin VB.TextBox txtPesoMezclaTotalRepReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   146
         Text            =   "0.00"
         Top             =   3000
         Width           =   1575
      End
      Begin VB.TextBox txtCantProdReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   12840
         Locked          =   -1  'True
         TabIndex        =   145
         Text            =   "0"
         Top             =   2040
         Width           =   975
      End
      Begin VB.TextBox txtLotesRegistro 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   9120
         Locked          =   -1  'True
         TabIndex        =   143
         Text            =   "0"
         Top             =   2040
         Width           =   975
      End
      Begin VB.TextBox txtCantProdOrden 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   137
         Text            =   "0"
         Top             =   3600
         Width           =   1575
      End
      Begin VB.TextBox txtPesoPromedioEmp 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   134
         Text            =   "0.00"
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox txtPesoPromedioUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11280
         Locked          =   -1  'True
         TabIndex        =   131
         Text            =   "0.00"
         Top             =   840
         Width           =   1575
      End
      Begin VB.TextBox txtPesoMezclaEstandar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   128
         Text            =   "0.00"
         Top             =   5400
         Width           =   1575
      End
      Begin VB.TextBox txtPesoUniReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   125
         Text            =   "0.00"
         Top             =   840
         Width           =   1575
      End
      Begin VB.TextBox txtCantibulReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   112
         Text            =   "1"
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox txtPesoEmpReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   111
         Text            =   "0.00"
         Top             =   2040
         Width           =   1575
      End
      Begin VB.TextBox txtCostoUniProyectado 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   110
         Text            =   "0.00"
         Top             =   7200
         Width           =   1575
      End
      Begin VB.TextBox txtCantLoteOrden 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   109
         Text            =   "0"
         Top             =   3000
         Width           =   1575
      End
      Begin VB.TextBox txtPesoTotalProductosReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   108
         Text            =   "0.00"
         Top             =   4200
         Width           =   1575
      End
      Begin VB.TextBox txtPesoTotalMezclaReg 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3360
         Locked          =   -1  'True
         TabIndex        =   107
         Text            =   "0.00"
         Top             =   4800
         Width           =   1575
      End
      Begin VB.Label lblDifBatch 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Dif. Batch: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   177
         Top             =   2520
         Width           =   1185
      End
      Begin VB.Label lblDifCant 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Dif. Cant: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   11640
         TabIndex        =   175
         Top             =   2520
         Width           =   1095
      End
      Begin VB.Label lblNomenclaturaCantLoteOrden 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Batch"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   165
         Top             =   3000
         Width           =   570
      End
      Begin VB.Label lblCostoUniReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Unitario Real: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   164
         Top             =   7200
         Width           =   2205
      End
      Begin VB.Label lblNomenclaturaCostoUniReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Und"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13440
         TabIndex        =   163
         Top             =   7200
         Width           =   990
      End
      Begin VB.Label lblTotalHorasTrabajadas 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Total de Horas trabajadas: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   160
         Top             =   6600
         Width           =   2895
      End
      Begin VB.Label lblNumHorasTrabajadas 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "N�mero de Horas trabajadas: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   158
         Top             =   6000
         Width           =   3210
      End
      Begin VB.Label lblNumTrabajadores 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "N�mero de Trabajadores: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   156
         Top             =   5400
         Width           =   2820
      End
      Begin VB.Label lblPorcVariacionPesosTotalesReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "% Variaci�n / Merma: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   154
         Top             =   4800
         Width           =   2415
      End
      Begin VB.Label lblNomenclaturaDiferenciaPesosTotalesReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13320
         TabIndex        =   153
         Top             =   4200
         Width           =   345
      End
      Begin VB.Label lblDiferenciaPesosTotalesReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Variacion vs Peso Mezcla: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   152
         Top             =   4200
         Width           =   2775
      End
      Begin VB.Label lblNomenclaturaPesoTotalProductoRepReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13320
         TabIndex        =   151
         Top             =   3600
         Width           =   345
      End
      Begin VB.Label lblPesoTotalProductoRepReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Te�rico Prod. Term.: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   149
         Top             =   3600
         Width           =   2895
      End
      Begin VB.Label lblNomenclaturaPesoTotalMezclaRepReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13320
         TabIndex        =   148
         Top             =   3000
         Width           =   345
      End
      Begin VB.Label lblPesoTotalMezclaRepReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso de Mezcla Reportado: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   147
         Top             =   3000
         Width           =   2925
      End
      Begin VB.Label lblCantProdReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cant. Prod. Terminado: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   10200
         TabIndex        =   144
         Top             =   2040
         Width           =   2595
      End
      Begin VB.Label lblLotesRegistro 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Batches: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   142
         Top             =   2040
         Width           =   960
      End
      Begin VB.Label lblRegistroReal 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Registro Real de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   9960
         TabIndex        =   141
         Top             =   240
         Width           =   2970
      End
      Begin VB.Line LnDivReg 
         BorderColor     =   &H00AE5B00&
         X1              =   4605
         X2              =   9355
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label lblDatosOrigen 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Datos seg�n Orden / Formula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   140
         Top             =   240
         Width           =   3150
      End
      Begin VB.Label lblNomenclaturaCantProdOrden 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   138
         Top             =   3600
         Width           =   510
      End
      Begin VB.Label lblPesoPromedioEmp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Promedio por Emp.: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   136
         Top             =   1440
         Width           =   2820
      End
      Begin VB.Label lblNomenclaturaPesoPromedioEmp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13320
         TabIndex        =   135
         Top             =   1440
         Width           =   345
      End
      Begin VB.Label lblPesoPromedioUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Promedio por Und: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7920
         TabIndex        =   133
         Top             =   840
         Width           =   2700
      End
      Begin VB.Label lblNomenclaturaPesoPromedioUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13320
         TabIndex        =   132
         Top             =   840
         Width           =   345
      End
      Begin VB.Label lblPesoMezclaEstandar 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso de Mezcla Estandar: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   130
         Top             =   5400
         Width           =   2745
      End
      Begin VB.Label lblNomenclaturaPesoMezclaEstandar 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg. / Und"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   129
         Top             =   5400
         Width           =   1020
      End
      Begin VB.Label lblPesoUniReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso por Und: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   127
         Top             =   840
         Width           =   1605
      End
      Begin VB.Label lblNomenclaturaPesoUniReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   126
         Top             =   840
         Width           =   345
      End
      Begin VB.Label lblCantibulReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und. por Empaque: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   124
         Top             =   1440
         Width           =   2175
      End
      Begin VB.Label lblPesoEmpReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso por Empaque: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   123
         Top             =   2040
         Width           =   2160
      End
      Begin VB.Label lblCantProdOrden 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cant. Prod. en Orden: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   122
         Top             =   3600
         Width           =   2430
      End
      Begin VB.Label lblCostoUnitarioProyectado 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Unitario Estandar: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   121
         Top             =   7200
         Width           =   2670
      End
      Begin VB.Label lblNomenclaturaCostoUniProyectado 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Und"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   120
         Top             =   7200
         Width           =   990
      End
      Begin VB.Label lblNomenclaturaPesoEmpReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   119
         Top             =   2040
         Width           =   345
      End
      Begin VB.Label lblNomenclaturaCantibulReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   118
         Top             =   1440
         Width           =   510
      End
      Begin VB.Label lblCantLoteOrden 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cant. Batch en Orden: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   117
         Top             =   3000
         Width           =   2430
      End
      Begin VB.Label lblNomenclaturaPesoTotalProductosReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   116
         Top             =   4200
         Width           =   345
      End
      Begin VB.Label lblPesoTotalProductosReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Total de Productos: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   115
         Top             =   4200
         Width           =   2730
      End
      Begin VB.Label lblPesoTotalMezclaReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Total de Mezcla: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   114
         Top             =   4800
         Width           =   2370
      End
      Begin VB.Label lblNomenclaturaPesoTotalMezclaReg 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   113
         Top             =   4800
         Width           =   345
      End
   End
   Begin VB.Label lbl_moneda 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1755
      TabIndex        =   26
      Top             =   1425
      Visible         =   0   'False
      Width           =   3525
   End
   Begin VB.Label LBL_TEMP 
      BackStyle       =   0  'Transparent
      Height          =   210
      Left            =   10950
      TabIndex        =   21
      Top             =   9840
      Visible         =   0   'False
      Width           =   840
   End
   Begin VB.Label lbl_simbolo2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   5445
      TabIndex        =   19
      Top             =   7020
      Width           =   615
   End
   Begin VB.Label lbl_simbolo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   10740
      TabIndex        =   18
      Top             =   5280
      Width           =   615
   End
   Begin VB.Label QUIEN_SOY 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "F�RMULAS DE PRODUCCI�N"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   -570
      TabIndex        =   17
      Top             =   1110
      Visible         =   0   'False
      Width           =   4170
   End
End
Attribute VB_Name = "FRM_PRODUCCION_ORDEN_MANUAL_3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Tecla_Pulsada As Boolean, Who_Ami As Integer
Dim cproducto As String, cEureka As String, cRecetas As String, Criterio As String, cinventario As String
Dim RsProducto As New ADODB.Recordset, RsRecetas As New ADODB.Recordset, rsPmoneda As New ADODB.Recordset
Dim RsInventario As New ADODB.Recordset, RsCodigos As New ADODB.Recordset
Dim Valor_Temp As Variant, Lines As Integer, Cols As Integer
Dim CuantosDel As Integer, CostoActivo As String, DecGrid(0) As Integer
Dim RsEureka As New ADODB.Recordset, FlgAdd As Boolean, FlgUpd As Boolean
Dim Cambio As Double, Cod_Moneda As String, GridLleno As Boolean, LastConsecutivo As String
Dim mCostoOrig As Double, mDecimales As Double

Private mClsGrupos As New ClsGrupos

Private TmpDisableEvent As Boolean
Private TmpNoCalcular   As Boolean

Private mRsProducto As ADODB.Recordset

Private ManejaMermaExplicita            As Boolean
Private FormaDeTrabajoRes               As Integer
Private MultiplicadorCantibul           As Double
Private mMultiplicar                    As Double
Private CantidadBaseFormula             As Double
Private TotalMerma                      As Double

Private NomenclaturaFDT                 As String

Private Enum ColPre
    LDeci
    Producto
    Descripci�n
    Presentaci�n
    CantBaseFormula
    CantLote
    PorcLoteReal
    PorcLote
    CantOrden
    CantProyectada
    Canti
    PorcMerma
    NetCantOrden
    NetCantProyectada
    NetCant
    DifNetCant
    PorcVariacionNetCant
    CostoPreXMerma
    CostoPre
    CostoProd
    CostoProdNetoProyectado
    CostoProdNeto
    DifCostoNeto
    PorcVariacionCostoNeto
    PorcTipoCosto
    PorcCostoGlobal
    nCantibul
    Info
    [ColCount]
End Enum

Private Enum ColDir
    TipoCosto
    ProyectadoUnitarioDecimal
    ProyectadoUnitario
    MontoProyectado
    RealUnitarioDecimal
    RealUnitario
    MontoReal
    DifMontos
    PorcCostoGlobal
    [ColCount]
End Enum

Private Enum ColTot
    TipoCosto
    MontoProyectado
    MontoReal
    DifMontos
    PorcCostoGlobal
    [ColCount]
End Enum

Private FormulaCantBase     As Double
Private FormulaOrigenOPR    As String
Private ObservacionInicial  As String
Private DocumentoRelacion   As String
Private CantidadTotalOrden  As Double
Private esBackOrder         As Boolean
Private FechaEstimadaPRD    As Date
Private FechaVcto           As Double
Private EstatusOPR          As String

Private Sub BarraO_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case "BUSCAR"
            Tecla_Pulsada = True
            'Call Form_KeyDown(vbKeyF2, 0)
            BarraO_ButtonMenuClick BarraO.Buttons(1).ButtonMenus("BORD")
            
        Case "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
            
        Case "ANULAR"
            Call Form_KeyDown(vbKeyF6, 0)
        
        Case "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        
        Case "REIMPRIMIR"
            Call Form_KeyDown(vbKeyF8, 0)
        
        Case "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        
        Case "AYUDA"
            'Ayuda
            
    End Select
    
End Sub

Private Sub BarraO_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    
    Select Case UCase(ButtonMenu.Key)
        
        Case "BPRO"
            'Call Buscar_Producto(Codigo)
            Select Case Who_Ami
                Case 1
                    If Not txtedit.Visible Then
                        Producto_Imp = Empty
                        Tecla_Pulsada = True
                        If Ingredientes.Col = ColPre.Producto Then
                            mArrProducto = BuscarInfoProducto_Basica(Ent.BDD, , , , , FrmBuscarProducto_Vista.VistaMarca, True)
                            
                            If Not IsEmpty(mArrProducto) Then
                                txtedit = mArrProducto(0)
                                Tecla_Pulsada = False
                                Call txtEdit_LostFocus
                            Else
                                txtedit = Empty
                            End If
                        End If
                        Tecla_Pulsada = False
                    End If
                Case 2
                    If Not txtEdit2.Visible Then
                        Producto_Imp = Empty
                        Tecla_Pulsada = True
                        If Empaques.Col = ColPre.Producto Then
                            mArrProducto = BuscarInfoProducto_Basica(Ent.BDD, , , , , FrmBuscarProducto_Vista.VistaMarca, True)
                            
                            If Not IsEmpty(mArrProducto) Then
                                txtEdit2 = mArrProducto(0)
                                Tecla_Pulsada = False
                                Call txtedit2_LostFocus
                            Else
                                txtEdit2 = Empty
                            End If
                        End If
                        Tecla_Pulsada = False
                    End If
            End Select
            
        Case "GORD", "GESP"
            'GRABAR
            Call Grabar_Produccion(UCase(ButtonMenu.Key))
        
        Case "BPRD", "BPRDESP"
            'BUSCAR
            Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "BPRD", "DCO", "DWT"), False, False)
            
        Case "BORD", "BESP", "BDCO"
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, _
            IIf(UCase(ButtonMenu.Key) = "BORD", "DPE", _
            IIf(UCase(ButtonMenu.Key) = "BDCO", "DCO", "DWT")), _
            False, False, , , "c_CodLocalidad")
            
        Case "AORD", "AESP"
            
            'ANULAR
            Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "AORD", "DCO", "DWT"), True, False)
            
        Case "RESP", "RORD"
            
            FrmAppLink.EnableTmpDLLProduccion1 = gCls
            'REIMPRIMIR
            Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "RORD", "DCO", "DWT"), False, True)
            FrmAppLink.EnableTmpDLLProduccion1 = Nothing
            
        Case "RDCO"
            FrmAppLink.EnableTmpDLLProduccion1 = gCls
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, "DCO", False, True, , , "c_CodLocalidad")
            FrmAppLink.EnableTmpDLLProduccion1 = Nothing
            
        Case "MONEDA"
            
            Call Cambiar_Moneda
            
    End Select
    
End Sub

Private Sub cmd_destino_Click()
    Tecla_Pulsada = True
    Call txt_destino_KeyDown(vbKeyF2, 0)
End Sub

Private Sub cmd_origen_Click()
    Tecla_Pulsada = True
    Call txt_ORIGEN_KeyDown(vbKeyF2, 0)
End Sub

Private Sub CmdFormula_Click()
    txtFormula_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdProducir_Click()
    mArr = BuscarInfoProducto_Basica(Ent.BDD, , False, , , VistaMarca, , True)
    If Not IsEmpty(mArr) Then
        txtProducir.Text = mArr(0)
        txtProducir_LostFocus
    Else
        txtProducir.Text = Empty
        txt_descripcion.Text = Empty
    End If
End Sub

Private Sub CargarGrid( _
Optional ByVal Reset As Boolean = True)
    
    ' Para Ingredientes
    
    NomenclaturaFDT = IIf(FormaDeTrabajoRes = 1, "Emp.", _
    IIf(lblNomenclaturaCant <> Empty, StrConv(lblNomenclaturaCant, vbProperCase), Empty))
    
    If Reset Then
        Ingredientes.Rows = 1
        Ingredientes.Rows = 2
    End If
    
    Ingredientes.AllowUserResizing = flexResizeColumns
    Ingredientes.Cols = ColPre.ColCount
    Ingredientes.WordWrap = True
    Ingredientes.ScrollTrack = True
    Ingredientes.ScrollBars = flexScrollBarBoth
    Ingredientes.Row = 0
    Ingredientes.RowHeight(0) = 720
    
    Call MSGridAsign(Ingredientes, 0, ColPre.LDeci, "LDec", 0, flexAlignRightCenter)
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Producto, Stellar_Mensaje(5010), 1290, flexAlignCenterCenter) 'producto
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignLeftCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Descripci�n, Stellar_Mensaje(143), 2500, flexAlignCenterCenter) 'descripcion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignLeftCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Presentaci�n, FrmAppLink.PRDLabelCampoPresenta, 1000, flexAlignCenterCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignLeftCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CantLote, "Cantidad Batch", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcLoteReal, "% Batch Real", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcLote, "% Batch", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CantBaseFormula, "CantBaseFormula", 0, flexAlignRightCenter) ' CANTIDAD BASE FORMULA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CantOrden, "Cant. Restante en Orden", 0, flexAlignRightCenter) 'CANTIDAD ORDEN PRODUCCION SIN MERMA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CantProyectada, "Cant. Estandar Proyectada", 0, flexAlignRightCenter) ' CANTIDAD FORMULA SIN MERMA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Canti, "Cantidad Real Utilizada", 0, flexAlignRightCenter) ' CANTIDAD UTILIZADA SIN MERMA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    If ManejaMermaExplicita Then
        
        Call MSGridAsign(Ingredientes, 0, ColPre.PorcMerma, Stellar_Mensaje(5013), _
        IIf(ModifFlag, 585, 0), IIf(ModifFlag, flexAlignCenterCenter, flexAlignRightCenter)) '% merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
        Call MSGridAsign(Ingredientes, 0, ColPre.CostoPreXMerma, Stellar_Mensaje(5011), 0, flexAlignRightCenter) 'costo x %merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
    Else
        
        Call MSGridAsign(Ingredientes, 0, ColPre.PorcMerma, Stellar_Mensaje(5013), 0, flexAlignRightCenter) '% merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
        Call MSGridAsign(Ingredientes, 0, ColPre.CostoPreXMerma, Stellar_Mensaje(5011), 0, flexAlignRightCenter) 'costo x %merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
    End If
    
    Call MSGridAsign(Ingredientes, 0, ColPre.NetCantOrden, "Cant. Restante en Orden", _
    IIf(Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE", 930, 0), _
    IIf(Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE", flexAlignCenterCenter, flexAlignRightCenter)) 'CANTIDAD ORDEN PRODUCCION INCLUYENDO MERMA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.NetCantProyectada, "Cant. Estandar Proyectada", _
    IIf(ModifFlag <> 1, 930, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignRightCenter)) ' CANTIDAD FORMULA INCLUYENDO MERMA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.NetCant, "Cantidad Real Utilizada", 930, flexAlignRightCenter) ' CANTIDAD UTILIZADA INCLUYENDO MERMA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.DifNetCant, "Diferencia de Cantidad", _
    IIf(ModifFlag <> 1, 930, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignRightCenter)) ' DIFERENCIA DE CANTIDAD NETA PROYECTADA - REAL
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcVariacionNetCant, "% Variacion Cantidad", _
    IIf(ModifFlag <> 1, 750, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignRightCenter)) ' % VARIACION DE LA DIFERENCIA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CostoPre, Replace(Stellar_Mensaje(151), ":", ""), 1200, flexAlignCenterCenter)   'costo
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CostoProd, Replace(Stellar_Mensaje(151), ":", ""), 0, flexAlignRightCenter)   'costo
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CostoProdNetoProyectado, "Costo Total Proyectado", _
    IIf(ModifFlag <> 1, 1350, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignRightCenter)) ' COSTO DE FORMULA INCLUYENDO MERMA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CostoProdNeto, Replace(Stellar_Mensaje(2938), ":", "") & _
    " " & "Real", 1350, flexAlignCenterCenter)
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.DifCostoNeto, "Diferencia de Costo", _
    IIf(ModifFlag <> 1, 1230, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignRightCenter)) ' DIFERENCIA DE COSTOS NETOS PROYECTADO - REAL
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcVariacionCostoNeto, "% Variacion Costo", _
    IIf(ModifFlag <> 1, 0, 0), IIf(ModifFlag <> 1, flexAlignRightCenter, flexAlignRightCenter)) ' % VARIACION DE LA DIFERENCIA
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcTipoCosto, "% Costo / Tipo Material", 0, flexAlignCenterCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcCostoGlobal, "% Costo Total", 0, flexAlignCenterCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.nCantibul, "Cant. por Emp.", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Info, "Info", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call SetDefMSGrid(Ingredientes, 1, ColPre.Producto)
    
    '
    
    With TotalesIngredientes
        
        If Reset Then
            .Rows = 0
            .Rows = 1
        End If
        
        .AllowUserResizing = flexResizeColumns
        .Cols = ColPre.ColCount
        .WordWrap = True
        .ScrollTrack = True
        .ScrollBars = flexScrollBarBoth
        .Row = 0
        .RowHeightMin = Ingredientes.RowHeightMin
        
        For I = 0 To .Cols - 1
            .Col = I
            .ColWidth(I) = Ingredientes.ColWidth(I)
            .ColAlignment(I) = Ingredientes.ColAlignment(I)
            .CellBackColor = 11857337
        Next I
        
    End With
    
    With Empaques
        
        If Reset Then
            .Rows = 0
            .Rows = 1
        End If
        
        .AllowUserResizing = flexResizeColumns
        .Cols = ColPre.ColCount
        .WordWrap = True
        .ScrollTrack = True
        .ScrollBars = flexScrollBarBoth
        .Row = 0
        .RowHeightMin = Ingredientes.RowHeightMin
        
        For I = 0 To .Cols - 1
            .ColWidth(I) = Ingredientes.ColWidth(I)
            .ColAlignment(I) = Ingredientes.ColAlignment(I)
        Next I
        
    End With
    
    With TotalesEmpaques
        
        If Reset Then
            .Rows = 0
            .Rows = 1
        End If
        
        .AllowUserResizing = flexResizeColumns
        .Cols = ColPre.ColCount
        .WordWrap = True
        .ScrollTrack = True
        .ScrollBars = flexScrollBarBoth
        .Row = 0
        .RowHeightMin = Ingredientes.RowHeightMin
        
        For I = 0 To .Cols - 1
            .Col = I
            .ColWidth(I) = Ingredientes.ColWidth(I)
            .ColAlignment(I) = Ingredientes.ColAlignment(I)
            .CellBackColor = 11857337
        Next I
        
    End With
    
    ' Para Costos Directos
    
    If Reset Then
        CostosDirectos.Rows = 1
        CostosDirectos.Rows = 2
    End If
    
    CostosDirectos.AllowUserResizing = flexResizeColumns
    CostosDirectos.Cols = ColDir.ColCount
    CostosDirectos.WordWrap = True
    CostosDirectos.ScrollTrack = True
    CostosDirectos.ScrollBars = flexScrollBarBoth
    CostosDirectos.Row = 0
    CostosDirectos.RowHeight(0) = 360
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.TipoCosto, "Tipo Costo", _
    IIf(ModifFlag <> 1, 2150, 2500), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignCenterCenter))
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignLeftCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.ProyectadoUnitarioDecimal, "Proy. Unit.", 0, flexAlignRightCenter)
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.ProyectadoUnitario, "Proy. Unit.", _
    IIf(ModifFlag <> 1, 0, 0), IIf(ModifFlag <> 1, flexAlignRightCenter, flexAlignRightCenter))
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.MontoProyectado, "Monto Proyec.", _
    IIf(ModifFlag <> 1, 1500, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignRightCenter))
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.RealUnitario, "Unitario", _
    IIf(ModifFlag, 1500, 0), IIf(ModifFlag, flexAlignCenterCenter, flexAlignCenterCenter))
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.RealUnitarioDecimal, "Unitario", 0, flexAlignRightCenter)
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.MontoReal, "Total", _
    IIf(ModifFlag <> 1, 1500, 2000), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignCenterCenter))
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.DifMontos, "Diferencia", _
    IIf(ModifFlag <> 1, 1500, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignCenterCenter))
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.PorcCostoGlobal, "% Costo", _
    IIf(ModifFlag <> 1, 0, 0), IIf(ModifFlag <> 1, flexAlignRightCenter, flexAlignRightCenter))
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    ' Para Totalizar
    
    If Reset Then
        GrdTotales.Rows = 1
        GrdTotales.Rows = 6
    End If
    
    GrdTotales.AllowUserResizing = flexResizeColumns
    GrdTotales.Cols = ColTot.ColCount
    GrdTotales.WordWrap = True
    GrdTotales.ScrollTrack = True
    GrdTotales.ScrollBars = flexScrollBarBoth
    GrdTotales.Row = 0
    GrdTotales.RowHeight(0) = 360
    
    Call MSGridAsign(GrdTotales, 0, ColTot.TipoCosto, "Tipo Costo", _
    IIf(ModifFlag <> 1, 2730, 3400), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignCenterCenter))
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignLeftCenter
    
    Call MSGridAsign(GrdTotales, 0, ColTot.MontoProyectado, "Monto Proyec.", _
    IIf(ModifFlag <> 1, 1425, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignRightCenter))
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignRightCenter
    
    Call MSGridAsign(GrdTotales, 0, ColTot.MontoReal, "Monto Real", _
    IIf(ModifFlag <> 1, 1425, 3100), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignCenterCenter))
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignRightCenter
    
    Call MSGridAsign(GrdTotales, 0, ColTot.DifMontos, "Diferencia", _
    IIf(ModifFlag <> 1, 1425, 0), IIf(ModifFlag <> 1, flexAlignCenterCenter, flexAlignCenterCenter))
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignRightCenter
    
    Call MSGridAsign(GrdTotales, 0, ColTot.PorcCostoGlobal, "% Costo", _
    IIf(ModifFlag <> 1, 0, 0), IIf(ModifFlag <> 1, flexAlignRightCenter, flexAlignRightCenter))
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignRightCenter
    
    If Reset Then
        
        GrdTotales.Row = 1
        GrdTotales.TextMatrix(1, ColTot.TipoCosto) = "MATERIA PRIMA Y EMPAQUE"
        GrdTotales.TextMatrix(1, ColTot.MontoProyectado) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(1, ColTot.MontoReal) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(1, ColTot.DifMontos) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(1, ColTot.PorcCostoGlobal) = FormatNumber(0, Std_Decm) & "%"
        CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
        
        GrdTotales.Row = 2
        GrdTotales.TextMatrix(2, ColTot.TipoCosto) = "COSTOS DIRECTOS"
        GrdTotales.TextMatrix(2, ColTot.MontoProyectado) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(2, ColTot.MontoReal) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(2, ColTot.DifMontos) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(2, ColTot.PorcCostoGlobal) = FormatNumber(0, Std_Decm) & "%"
        CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
        
        If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelVerCostosDirectos _
        Or FrmAppLink.PRDNivelVerCostosDirectos = 0 Then
            
            GrdTotales.RowHeight(2) = 0
            
            CostosDirectos.Visible = False
            lblCostosDir.Visible = False
            LnCostoDir.Visible = False
            
            lblTotales.Left = 4080
            LnTotales.X1 = 5265
            LnTotales.X2 = 11260
            GrdTotales.Left = lblTotales.Left
            
            lblTotalCostosDir.Visible = False
            txtCostosDir.Visible = False
            lblNomenclaturaCDir.Visible = False
            
            lblCostosDirUni.Visible = False
            txtCostosDirUni.Visible = False
            lblNomenclaturaCDirUni.Visible = False
            
            lblCostoHoraHombre.Visible = False
            lblCostoHoraCargaFabril.Visible = False
            lblHorasHombreXUnidad.Visible = False
            
            txtCostoHoraHombre.Visible = False
            txtCostoHoraCargaFabril.Visible = False
            txtHorasHombreXUnidad.Visible = False
            
            lblNomenclaturaCostoHoraHombre.Visible = False
            lblNomenclaturaCostoHoraCargaFabril.Visible = False
            lblNomenclaturaHorasHombreXUnidad.Visible = False
            
        End If
        
        GrdTotales.Row = 3
        GrdTotales.TextMatrix(3, ColTot.TipoCosto) = "GENERAL"
        GrdTotales.TextMatrix(3, ColTot.MontoProyectado) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(3, ColTot.MontoReal) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(3, ColTot.DifMontos) = FormatNumber(0, Std_Decm)
        GrdTotales.TextMatrix(3, ColTot.PorcCostoGlobal) = FormatNumber(0, Std_Decm) & "%"
        CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
        
        GrdTotales.Row = 4
        GrdTotales.TextMatrix(4, ColTot.TipoCosto) = "% VARIACION MATERIALES"
        GrdTotales.TextMatrix(4, ColTot.MontoProyectado) = Empty
        GrdTotales.TextMatrix(4, ColTot.MontoReal) = Empty
        GrdTotales.TextMatrix(4, ColTot.DifMontos) = FormatNumber(0, Std_Decm) & "%"
        GrdTotales.TextMatrix(4, ColTot.PorcCostoGlobal) = Empty
        CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
        
        GrdTotales.Row = 5
        GrdTotales.TextMatrix(5, ColTot.TipoCosto) = "% VARIACION TOTAL"
        GrdTotales.TextMatrix(5, ColTot.MontoProyectado) = Empty
        GrdTotales.TextMatrix(5, ColTot.MontoReal) = Empty
        GrdTotales.TextMatrix(5, ColTot.DifMontos) = FormatNumber(0, Std_Decm) & "%"
        GrdTotales.TextMatrix(5, ColTot.PorcCostoGlobal) = Empty
        CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
        
        If ModifFlag <> 1 Then
            
            GrdTotales.RowHeight(4) = 0
            GrdTotales.RowHeight(5) = 0
            
        End If
        
    End If
    
    '
    
    lblNomenclaturaCostoTotal = lbl_simbolo
    lblNomenclaturaCDir = lbl_simbolo
    lblNomenclaturaCostoUni = lbl_simbolo & " / " & NomenclaturaFDT
    lblNomenclaturaCDirUni = lbl_simbolo & " / " & NomenclaturaFDT
    
    If FormaDeTrabajoRes = 1 Then
        lblNomenclaturaCant = "Emp."
        lblNomenclaturaUnidadesXLote = "Emp."
        lblHorasHombreXUnidad = "Cant. Hora Hombre / Emp.: "
    End If
    
    FrameSumario.Height = FrameProceso.Height
    FrameRegistroVSOrden.Height = FrameProceso.Height
    
    If Reset Then
        CargarCostosDirectos
    End If
    
End Sub

Private Sub CostosDirectos_Click()
    With CostosDirectos
        If .Col = ColDir.TipoCosto Then
            If .TextMatrix(.Row, .Col) = Empty Then
                mClsGrupos.cTipoGrupo = "PCD"
                mClsGrupos.CargarComboGrupos Ent.BDD, CboPCD
                Tecla_Pulsada = True
                ListRemoveItems CboPCD, " ", "Ninguno"
                Tecla_Pulsada = False
                CboPCD.Move .CellLeft + .Left, .CellTop + .Top
                CboPCD.Visible = True
            End If
        ElseIf .Col = ColDir.MontoReal And .TextMatrix(.Row, ColDir.TipoCosto) <> Empty Then
            mValorAnt = .Text
            mValorNew = QuickInputRequest("Ingrese el Monto relacionado al Costo Directo.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
            If IsNumeric(mValorNew) Then
                If Not IsNumeric(mValorAnt) Then mValorAnt = 0
                If CDbl(mValorNew) <> CDbl(mValorAnt) Then
                    .TextMatrix(.Row, ColDir.RealUnitarioDecimal) = (mValorNew / mMultiplicar)
                    .TextMatrix(.Row, ColDir.RealUnitario) = FormatNumber(CDbl(.TextMatrix(.Row, ColDir.RealUnitarioDecimal)), Std_Decm)
                    RecalcularTransaccion
                End If
                If .Row = .Rows - 1 Then
                    .Rows = .Rows + 1
                End If
            End If
        ElseIf .Col = ColDir.RealUnitario And .TextMatrix(.Row, ColDir.TipoCosto) <> Empty Then
            mValorAnt = .Text
            mValorNew = QuickInputRequest("Ingrese el Costo Directo por unidad.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
            If IsNumeric(mValorNew) Then
                If Not IsNumeric(mValorAnt) Then mValorAnt = 0
                If CDbl(mValorNew) <> CDbl(mValorAnt) Then
                    .TextMatrix(.Row, ColDir.RealUnitarioDecimal) = Abs(mValorNew)
                    .Text = FormatNumber(CDbl(.TextMatrix(.Row, ColDir.RealUnitarioDecimal)), Std_Decm)
                    RecalcularTransaccion
                End If
                If .Row = .Rows - 1 Then
                    .Rows = .Rows + 1
                End If
            End If
        End If
    End With
End Sub

Private Sub CostosDirectos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If CostosDirectos.Rows = 2 And CostosDirectos.Row = 1 Then
            CostosDirectos.Rows = CostosDirectos.Rows + 1
            CostosDirectos.RemoveItem CostosDirectos.Row
        Else
            CostosDirectos.RemoveItem CostosDirectos.Row
        End If
        RecalcularTransaccion
    End If
End Sub

Private Sub Form_Load()
    
    Call AjustarPantalla(Me)
    
    BarraO.Buttons(1).Caption = Stellar_Mensaje(102) 'buscar
    
    BarraO.Buttons(1).ButtonMenus(1).Text = Stellar_Mensaje(5014) 'Orden de Producci�n
    
    BarraO.Buttons(1).ButtonMenus(2).Text = Stellar_Mensaje(1010) 'Producci�n
    BarraO.Buttons(1).ButtonMenus(2).Visible = False
    BarraO.Buttons(1).ButtonMenus(3).Text = Stellar_Mensaje(5002)
    
    BarraO.Buttons(3).Caption = Stellar_Mensaje(103) 'grabar
    BarraO.Buttons(3).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(2).Text
    
    BarraO.Buttons(4).Caption = Stellar_Mensaje(104) 'anular
    BarraO.Buttons(4).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(2).Text
    
    BarraO.Buttons(5).Caption = Stellar_Mensaje(105) 'cancelar
    BarraO.Buttons(6).Caption = Stellar_Mensaje(106) 'reimprimir
    
    BarraO.Buttons(6).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(2).Text
    
    BarraO.Buttons(8).Caption = Stellar_Mensaje(107) 'salir
    BarraO.Buttons(9).Caption = Stellar_Mensaje(108) 'opciones
    BarraO.Buttons(9).ButtonMenus(1).Text = Stellar_Mensaje(7) 'Ayuda
    BarraO.Buttons(9).ButtonMenus(2).Text = Stellar_Mensaje(120) 'Cambiar Moneda
    
    lbl_concepto.Caption = Stellar_Mensaje(2953) 'produccion n�
    
    Label5.Caption = Stellar_Mensaje(128) 'fecha
    
    lblDescFormula.Caption = Stellar_Mensaje(143) 'descripcion
    lblTotalCostosDir.Caption = Stellar_Mensaje(5004) 'costos directos
    
    lblProductosUtilizar.Caption = Stellar_Mensaje(5006) 'productos a utilizar
    lblCostosDir.Caption = lblTotalCostosDir.Caption
    
    lbl_Organizacion.Caption = FrmAppLink.GetLblOrganizacion
    
    ManejaMermaExplicita = FrmAppLink.PRDManejaMermaExplicita
    FormaDeTrabajoRes = FrmAppLink.PRDAvanzadaResultadosFormaDeTrabajo
    
    lbl_fecha.Caption = SDate(Now)
    
    ObservacionInicial = StellarMensaje(2948) ' EJECUCION DE FORMULA DE PRODUCCI�N
    
    If FrmAppLink.InventarioCampoLineaProduccion Then
        lblLineaProduccion.Visible = True
        CboLnP.Visible = True
    End If
    
    If FrmAppLink.InventarioCampoTurnoProduccion Then
        lblTurno.Visible = True
        CboTurno.Visible = True
    End If
    
    mClsGrupos.cTipoGrupo = "LNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
    Tecla_Pulsada = True
    ListRemoveItems CboLnP, "Ninguno"
    Tecla_Pulsada = False
            
    mClsGrupos.cTipoGrupo = "TNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
    Tecla_Pulsada = True
    ListRemoveItems CboTurno, "Ninguno"
    Tecla_Pulsada = False
    
    ' Carga de Reglas Comerciales para el c�lculo del costo de producci�n.
    
    Ent.RsReglasComerciales.Requery
    
    Select Case Ent.RsReglasComerciales!Estimacion_Inv
        Case 0
            CostoActivo = "n_CostoAct"
        Case 1
            CostoActivo = "n_CostoAnt"
        Case 2
            CostoActivo = "n_CostoPro"
        Case 3
            CostoActivo = "n_CostoRep"
    End Select
    
    If FrmAppLink.PRDCostoActivo <> Empty Then CostoActivo = FrmAppLink.PRDCostoActivo
    
    Cancelar
    
    If FrmAppLink.PRDAvanzadaOcultarCantidadFormula Then
        FrameCargarLotes.Left = FrameMultiplicarFormula.Left
        FrameMultiplicarFormula.Visible = False
    End If
    
End Sub

Private Sub Form_Activate()
    Screen.MousePointer = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbAltMask Then
        
        Select Case KeyCode
            
            Case vbKeyB
            
            Case vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
                
            Case vbKeyN
                Call Form_KeyDown(vbKeyF6, 0)
            
            Case vbKeyC
                Call Form_KeyDown(vbKeyF7, 0)
            
            Case vbKeyR
                Call Form_KeyDown(vbKeyF8, 0)
            
            Case vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
                
            Case vbKeyA
            
        End Select
        
    Else
        
        Select Case KeyCode
            Case vbKeyF1
                'AYUDA
            
            Case vbKeyF2
                'BUSCAR
                If txtFormula.Enabled Then
                    SafeFocus txtFormula
                    Call txtFormula_KeyDown(vbKeyF2, 0)
                End If
            
            Case vbKeyF3
                'AGREGAR
                
            Case vbKeyF4
                'GRABAR
                BarraO_ButtonMenuClick BarraO.Buttons(3).ButtonMenus("GORD")
                
            Case vbKeyF5
                'MODIFICAR
                
            Case vbKeyF6
                'ANULAR
                BarraO_ButtonMenuClick BarraO.Buttons(4).ButtonMenus("AORD")
                
            Case vbKeyF7
                'CANCELAR
                Call Cancelar
                
            Case vbKeyF8
                'REIMPRIMIR
                BarraO_ButtonMenuClick BarraO.Buttons(6).ButtonMenus("RORD")
                
            Case vbKeyF9
                'RESERVADO
                
            Case vbKeyF10
                'RESERVADO
            
            Case vbKeyF12
                'SALIR
                Call Salir
                
        End Select
        
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set FRM_PRODUCCION_ORDEN_MANUAL_3 = Nothing
End Sub

Private Sub FrameCargarLotes_Click()
    lblCargarLotes_Click
End Sub

Private Sub FrameMultiplicarFormula_Click()
    lblMultiplicarFormula_Click
End Sub

Private Sub Ingredientes_DblClick()
    Call Ingredientes_KeyPress(vbKeyReturn)
End Sub

Private Sub Ingredientes_GotFocus()
    If Ingredientes.Col = ColPre.Producto And ModifFlag Then
        oTeclado.Key_Down
        Who_Ami = 1
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub Ingredientes_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
    Else
        Select Case KeyCode
            Case Is = vbKeyF2
                If ModifFlag Then
                    Who_Ami = 1
                    BarraO_ButtonMenuClick BarraO.Buttons("Buscar").ButtonMenus("BPRO")
                End If
            Case Is = vbKeyDelete
                Call Ingredientes_KeyPress(vbKeyDelete)
        End Select
    End If
End Sub

Private Sub Ingredientes_KeyPress(KeyAscii As Integer)
    'If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                With Ingredientes
                    If (.Col = ColPre.Producto Or .Col = ColPre.PorcMerma And ModifFlag) Or .Col = ColPre.NetCant Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtedit.MaxLength = 0
                                
                            Case ColPre.NetCant
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Ingredientes, .Row, ColPre.NetCant) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.NetCant)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                        End Select
                        
                        '   ACA SE COMENTA PARA DEJAR POR FUERA A LA COLUMNA DE MERMA
                        If (.Col = ColPre.Producto And .Row = .Rows - 1) _
                        Or .Col = ColPre.NetCant _
                        Or IIf(ManejaMermaExplicita, .Col = ColPre.PorcMerma, False) Then
                            If .Text <> "" Then
                                Valor_Temp = .Text
                                txtedit.Text = .Text
                            End If
                            txtedit.Left = FrameProceso.Left + .CellLeft + .Left
                            txtedit.Top = FrameProceso.Top + .CellTop + .Top
                            txtedit.Height = .CellHeight
                            txtedit.Width = .CellWidth
                            SeleccionarTexto txtedit
                            txtedit.Visible = True
                            txtedit.Enabled = True
                            SafeFocus txtedit
                            .Enabled = False
                        Else
                            .Enabled = True
                            .Col = ColPre.Producto
                            .Row = .Rows - 1
                            SafeFocus Ingredientes
                        End If
                    End If
                
                End With
                
            Case vbKeyEscape
                With Ingredientes
                    .Enabled = True
                    txtedit.Visible = False
                    If .Enabled And .Visible Then .SetFocus
                End With
                
            Case 48 To 57, 106 To 255, vbKeyDecimal
                With Ingredientes
                    If (.Col = ColPre.Producto Or .Col = ColPre.PorcMerma And ModifFlag) Or .Col = ColPre.NetCant Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtedit.MaxLength = 0
                                
                            Case ColPre.NetCant, ColPre.CantLote
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Ingredientes, .Row, ColPre.NetCant) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.NetCant)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                        End Select
                        
                        Valor_Temp = .Text
                        txtedit.Left = FrameProceso.Left + .CellLeft + .Left
                        txtedit.Top = FrameProceso.Top + .CellTop + .Top
                        txtedit.Height = .CellHeight
                        txtedit.Width = .CellWidth
                        txtedit.Visible = True
                        txtedit.Text = Chr(KeyAscii)
                        txtedit.SelStart = Len(txtedit.Text)
                        txtedit.SelLength = 0
                        SafeFocus txtedit
                        .Enabled = False
                    End If
                    
                End With
                
            Case vbKeyDelete
                With Ingredientes
                    If ModifFlag Then
                        If .Row = 1 And .Rows = 2 Then
                            .Rows = 1
                            .Rows = 2
                            RecalcularTransaccion
                        Else
                            .RemoveItem .Row
                            RecalcularTransaccion
                        End If
                    End If
                End With
        End Select
    'End If
End Sub

Private Sub Ingredientes_LostFocus()
    If Empaques.Enabled Then
        Empaques.Row = Empaques.Rows - 1
        Empaques.Col = ColPre.Producto
    End If
End Sub

Private Sub Ingredientes_SelChange()
    If Ingredientes.Col = ColPre.Producto And ModifFlag Then
        Who_Ami = 1
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

'

Private Sub Empaques_DblClick()
    Call Empaques_KeyPress(vbKeyReturn)
End Sub

Private Sub Empaques_GotFocus()
    If Empaques.Col = ColPre.Producto And ModifFlag Then
        Who_Ami = 2
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub Empaques_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
    Else
        Select Case KeyCode
            Case Is = vbKeyF2
                If ModifFlag Then
                    Who_Ami = 2
                    BarraO_ButtonMenuClick BarraO.Buttons("Buscar").ButtonMenus("BPRO")
                End If
            Case Is = vbKeyDelete
                Call Empaques_KeyPress(vbKeyDelete)
        End Select
    End If
End Sub

Private Sub Empaques_KeyPress(KeyAscii As Integer)
    'If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                With Empaques
                    If (.Col = ColPre.Producto Or .Col = ColPre.PorcMerma And ModifFlag) Or .Col = ColPre.NetCant Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtEdit2.MaxLength = 0
                                
                            Case ColPre.NetCant
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                                
                            Case ColPre.PorcMerma
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Empaques, .Row, ColPre.NetCant) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.NetCant)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                        End Select
                        
                        '   ACA SE COMENTA PARA DEJAR POR FUERA A LA COLUMNA DE MERMA
                        If (.Col = ColPre.Producto And .Row = .Rows - 1) _
                        Or .Col = ColPre.NetCant _
                        Or IIf(ManejaMermaExplicita, .Col = ColPre.PorcMerma, False) Then
                            If .Text <> "" Then
                                Valor_Temp = .Text
                                txtEdit2.Text = .Text
                            End If
                            txtEdit2.Left = .CellLeft + .Left
                            txtEdit2.Top = .CellTop + .Top
                            txtEdit2.Height = .CellHeight
                            txtEdit2.Width = .CellWidth
                            SeleccionarTexto txtEdit2
                            txtEdit2.Visible = True
                            txtEdit2.Enabled = True
                            SafeFocus txtEdit2
                            .Enabled = False
                        Else
                            .Enabled = True
                            .Col = ColPre.Producto
                            .Row = .Rows - 1
                            SafeFocus Empaques
                        End If
                    End If
                
                End With
                
            Case vbKeyEscape
                With Empaques
                    .Enabled = True
                    txtEdit2.Visible = False
                    If .Enabled And .Visible Then .SetFocus
                End With
                
            Case 48 To 57, 106 To 255, vbKeyDecimal
                With Empaques
                    If (.Col = ColPre.Producto Or .Col = ColPre.PorcMerma And ModifFlag) Or .Col = ColPre.NetCant Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtEdit2.MaxLength = 0
                                
                            Case ColPre.NetCant, ColPre.CantLote
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Empaques, .Row, ColPre.NetCant) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.NetCant)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                        End Select
                        
                        Valor_Temp = .Text
                        txtEdit2.Left = .CellLeft + .Left
                        txtEdit2.Top = .CellTop + .Top
                        txtEdit2.Height = .CellHeight
                        txtEdit2.Width = .CellWidth
                        txtEdit2.Visible = True
                        txtEdit2.Enabled = True
                        txtEdit2.Text = Chr(KeyAscii)
                        txtEdit2.SelStart = Len(txtEdit2.Text)
                        txtEdit2.SelLength = 0
                        SafeFocus txtEdit2
                        .Enabled = False
                    End If
                    
                End With
                
            Case vbKeyDelete
                With Empaques
                    If ModifFlag Then
                        If .Row = 0 And .Rows = 1 Then
                            .Rows = 0
                            .Rows = 1
                            RecalcularTransaccion
                        Else
                            .RemoveItem .Row
                            RecalcularTransaccion
                        End If
                    End If
                End With
        End Select
    'End If
End Sub

Private Sub Empaques_LostFocus()
    If CostosDirectos.Enabled Then
        CostosDirectos.Row = CostosDirectos.Rows - 1
        CostosDirectos.Col = ColDir.MontoReal
    End If
End Sub

Private Sub Empaques_SelChange()
    If Empaques.Col = ColPre.Producto And ModifFlag Then
        Who_Ami = 2
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

'

Private Sub RecalcularTransaccion()
    
    On Error GoTo Error
    
    Ingredientes.Visible = False
    Empaques.Visible = False
    CostosDirectos.Visible = False
    GrdTotales.Visible = False
    
    ' Primero Calcular Detalle
    
    With Ingredientes
        
        For I = 1 To .Rows - 1
            
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.NetCant)) Then
                'If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    
                    .TextMatrix(I, ColPre.CostoPreXMerma) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.PorcMerma) / 100) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    ' Esta no se recalcula. Es el Input.
                    '.TextMatrix(I, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * (1# + (.TextMatrix(I, ColPre.PorcMerma) / 100)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    .TextMatrix(I, ColPre.Canti) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCant)) / (1# + (.TextMatrix(I, ColPre.PorcMerma) / 100)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    
                    If (CantidadBaseFormula > 0) And ModifFlag = vbUnchecked Then
                        .TextMatrix(I, ColPre.CantProyectada) = FormatNumber(mMultiplicar * _
                        (CDbl(.TextMatrix(I, ColPre.CantBaseFormula)) / CantidadBaseFormula), .TextMatrix(I, ColPre.LDeci))
                        .TextMatrix(I, ColPre.NetCantProyectada) = FormatNumber( _
                        CDbl(.TextMatrix(I, ColPre.CantProyectada)) * (1# + (CDbl(.TextMatrix(I, ColPre.PorcMerma)) / 100)), .TextMatrix(I, ColPre.LDeci))
                    Else
                        .TextMatrix(I, ColPre.CantProyectada) = 0
                        .TextMatrix(I, ColPre.NetCantProyectada) = 0
                    End If
                    
                    .TextMatrix(I, ColPre.DifNetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) - CDbl(.TextMatrix(I, ColPre.NetCant)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    If (CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) <> 0) Then
                        .TextMatrix(I, ColPre.PorcVariacionNetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.DifNetCant)) / CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcVariacionNetCant) = FormatNumber(0, 2) & "%"
                    End If
                    
                    .TextMatrix(I, ColPre.CostoProd) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.CostoProdNetoProyectado) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCant)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    
                    .TextMatrix(I, ColPre.DifCostoNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.CostoProdNetoProyectado)) - CDbl(.TextMatrix(I, ColPre.CostoProdNeto)), Std_Decm)
                    If (CDbl(.TextMatrix(I, ColPre.CostoProdNetoProyectado)) <> 0) Then
                        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.DifCostoNeto)) / CDbl(.TextMatrix(I, ColPre.CostoProdNetoProyectado)) * 100, 2)
                    Else
                        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(0, 2)
                    End If
                    
                'End If
            End If
            
        Next
        
    End With
    
    With Empaques
        For I = 0 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.NetCant)) Then
                'If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    
                    .TextMatrix(I, ColPre.CostoPreXMerma) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.PorcMerma) / 100) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    ' Esta no se recalcula. Es el Input.
                    '.TextMatrix(I, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * (1# + (.TextMatrix(I, ColPre.PorcMerma) / 100)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    .TextMatrix(I, ColPre.Canti) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCant)) / (1# + (.TextMatrix(I, ColPre.PorcMerma) / 100)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    
                    If (CantidadBaseFormula > 0) And ModifFlag = vbUnchecked Then
                        .TextMatrix(I, ColPre.CantProyectada) = FormatNumber(mMultiplicar * _
                        (CDbl(.TextMatrix(I, ColPre.CantBaseFormula)) / CantidadBaseFormula), .TextMatrix(I, ColPre.LDeci))
                        .TextMatrix(I, ColPre.NetCantProyectada) = FormatNumber( _
                        CDbl(.TextMatrix(I, ColPre.CantProyectada)) * (1# + (CDbl(.TextMatrix(I, ColPre.PorcMerma)) / 100)), .TextMatrix(I, ColPre.LDeci))
                    Else
                        .TextMatrix(I, ColPre.CantProyectada) = 0
                        .TextMatrix(I, ColPre.NetCantProyectada) = 0
                    End If
                    
                    .TextMatrix(I, ColPre.DifNetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) - CDbl(.TextMatrix(I, ColPre.NetCant)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    If (CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) <> 0) Then
                        .TextMatrix(I, ColPre.PorcVariacionNetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.DifNetCant)) / CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcVariacionNetCant) = FormatNumber(0, 2) & "%"
                    End If
                    
                    .TextMatrix(I, ColPre.CostoProd) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.CostoProdNetoProyectado) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCant)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    
                    .TextMatrix(I, ColPre.DifCostoNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.CostoProdNetoProyectado)) - CDbl(.TextMatrix(I, ColPre.CostoProdNeto)), Std_Decm)
                    If (CDbl(.TextMatrix(I, ColPre.CostoProdNetoProyectado)) <> 0) Then
                        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.DifCostoNeto)) / CDbl(.TextMatrix(I, ColPre.CostoProdNetoProyectado)) * 100, 2)
                    Else
                        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(0, 2)
                    End If
                    
                'End If
            End If
        Next
    End With
    
    With CostosDirectos
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColDir.TipoCosto) <> Empty Then
                
                If Not IsNumeric(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal)) Then .TextMatrix(I, ColDir.ProyectadoUnitarioDecimal) = 0
                If Not IsNumeric(.TextMatrix(I, ColDir.ProyectadoUnitario)) Then .TextMatrix(I, ColDir.ProyectadoUnitario) = FormatNumber(0, Std_Decm)
                If Not IsNumeric(.TextMatrix(I, ColDir.RealUnitarioDecimal)) Then .TextMatrix(I, ColDir.RealUnitarioDecimal) = 0
                If Not IsNumeric(.TextMatrix(I, ColDir.RealUnitario)) Then .TextMatrix(I, ColDir.RealUnitario) = FormatNumber(0, Std_Decm)
                
                .TextMatrix(I, ColDir.MontoProyectado) = FormatNumber(CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal)) * mMultiplicar, Std_Decm)
                .TextMatrix(I, ColDir.MontoReal) = FormatNumber(CDbl(.TextMatrix(I, ColDir.RealUnitarioDecimal)) * mMultiplicar, Std_Decm)
                
                .TextMatrix(I, ColDir.DifMontos) = FormatNumber(CDbl(.TextMatrix(I, ColDir.MontoProyectado)) - CDbl(.TextMatrix(I, ColDir.MontoReal)), Std_Decm)
                
            End If
        Next
    End With
    
    ' Luego Sumar Totales
    
    Dim SumaCostoIngredientes As Double, SumaCostoEmpaques As Double, _
    SumaCostoMateriales As Double, SumaCostosDirectos As Double, SumaGlobal As Double
    
    Dim SumaCostoIngredientesProyeccion As Double, SumaCostoEmpaquesProyeccion As Double, _
    SumaCostoMaterialesProyeccion As Double, SumaCostosDirectosProyeccion As Double, SumaGlobalProyeccion As Double
    
    Dim SumaCostosDirectosUnitario As Double, SumaVariacionMateriales As Double, SumaVariacionCostosDirectos As Double, _
    SumaProyeccionMateriales As Double, SumaProyeccionCostosDirectos As Double
    
    SumaCostoIngredientes = SumaGrid(Ingredientes, ColPre.CostoProdNeto)
    SumaCostoEmpaques = SumaGrid(Empaques, ColPre.CostoProdNeto)
    SumaCostoMateriales = Round(SumaCostoIngredientes + SumaCostoEmpaques, 8)
    SumaCostosDirectos = SumaGrid(CostosDirectos, ColDir.MontoReal)
    SumaCostosDirectosUnitario = SumaGrid(CostosDirectos, ColDir.RealUnitarioDecimal)
    SumaGlobal = Round(SumaCostoMateriales + SumaCostosDirectos, 8)
    
    SumaCostoIngredientesProyeccion = SumaGrid(Ingredientes, ColPre.CostoProdNetoProyectado)
    SumaCostoEmpaquesProyeccion = SumaGrid(Empaques, ColPre.CostoProdNetoProyectado)
    SumaCostoMaterialesProyeccion = Round(SumaCostoIngredientesProyeccion + SumaCostoEmpaquesProyeccion, 8)
    SumaCostosDirectosProyeccion = SumaGrid(CostosDirectos, ColDir.MontoProyectado)
    SumaGlobalProyeccion = Round(SumaCostoMaterialesProyeccion + SumaCostosDirectosProyeccion, 8)
    
    txtCantProducto.Tag = (txtCantProducto.Text * MultiplicadorCantibul)
    
    If CDbl(txtCantProducto.Tag) > 1 Then
        ' Para evitar que el monto unitario quede con mas decimales que los que maneja la moneda.
        SumaGlobal = Round(Round(SumaGlobal / CDbl(txtCantProducto.Tag), Std_Decm) * CDbl(txtCantProducto.Tag), Std_Decm)
        SumaGlobalProyeccion = Round(Round(SumaGlobalProyeccion / CDbl(txtCantProducto.Tag), Std_Decm) * CDbl(txtCantProducto.Tag), Std_Decm)
    End If
    
    txtCostosDirUni = FormatNumber(SumaCostosDirectosUnitario, Std_Decm)
    txtCostoUni = FormatNumber(SumaGlobal / mMultiplicar, Std_Decm)
    txtCostoUniProyectado = FormatNumber(SumaGlobalProyeccion / mMultiplicar, Std_Decm)
    
    With Ingredientes
        
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.NetCant)) Then
                'If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    If SumaCostoIngredientes <> 0 Then
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaCostoIngredientes * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(0, 2) & "%"
                    End If
                    If SumaGlobal <> 0 Then
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaGlobal * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
                    End If
                'End If
            End If
        Next
        
    End With
    
    With TotalesIngredientes
        
        I = 0
        
        .TextMatrix(I, ColPre.Descripci�n) = "TOTAL POR INGREDIENTES"
        .TextMatrix(I, ColPre.CantLote) = FormatNumber(SumaGrid(Ingredientes, ColPre.CantLote), 3)
        .TextMatrix(I, ColPre.PorcLote) = "100%"
        
        .TextMatrix(I, ColPre.CantOrden) = FormatNumber(SumaGrid(Ingredientes, ColPre.CantOrden), 3)
        .TextMatrix(I, ColPre.CantProyectada) = FormatNumber(SumaGrid(Ingredientes, ColPre.CantProyectada), 3)
        .TextMatrix(I, ColPre.Canti) = FormatNumber(SumaGrid(Ingredientes, ColPre.Canti), 3)
        
        .TextMatrix(I, ColPre.NetCantOrden) = FormatNumber(SumaGrid(Ingredientes, ColPre.CantOrden), 3)
        .TextMatrix(I, ColPre.NetCantProyectada) = FormatNumber(SumaGrid(Ingredientes, ColPre.CantProyectada), 3)
        .TextMatrix(I, ColPre.NetCant) = FormatNumber(SumaGrid(Ingredientes, ColPre.NetCant), 3)
        
        .TextMatrix(I, ColPre.DifNetCant) = FormatNumber(SumaGrid(Ingredientes, ColPre.DifNetCant), 3)
        If CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) <> 0 Then
            .TextMatrix(I, ColPre.PorcVariacionNetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.DifNetCant)) / CDbl(.TextMatrix(I, ColPre.NetCantProyectada)) * 100, 2) & "%"
        Else
            .TextMatrix(I, ColPre.PorcVariacionNetCant) = FormatNumber(0, 2) & "%"
        End If
        
        txtPesoTotalMezcla.Text = .TextMatrix(I, ColPre.NetCant)
        
        .TextMatrix(I, ColPre.CostoPreXMerma) = FormatNumber(SumaGrid(Ingredientes, ColPre.CostoPreXMerma), Std_Decm)
        .TextMatrix(I, ColPre.CostoProd) = FormatNumber(SumaGrid(Ingredientes, ColPre.CostoProd), Std_Decm)
        .TextMatrix(I, ColPre.CostoProdNetoProyectado) = FormatNumber(SumaGrid(Ingredientes, ColPre.CostoProdNetoProyectado), Std_Decm)
        .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(SumaCostoIngredientes, Std_Decm)
        
        .TextMatrix(I, ColPre.DifCostoNeto) = FormatNumber(SumaGrid(Ingredientes, ColPre.DifCostoNeto), Std_Decm)
        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(SumaGrid(Ingredientes, ColPre.PorcVariacionCostoNeto), 2)
        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.PorcVariacionCostoNeto)) / Ingredientes.Rows - 1, 2) & "%"
        
        .TextMatrix(I, ColPre.PorcTipoCosto) = "100%"
        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(SumaGrid(Ingredientes, ColPre.PorcCostoGlobal), 2) & "%"
        
    End With
    
    With Empaques
        For I = 0 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.NetCant)) Then
                'If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    If SumaCostoEmpaques <> 0 Then
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaCostoEmpaques * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(0, 2) & "%"
                    End If
                    If SumaGlobal <> 0 Then
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaGlobal * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
                    End If
                'End If
            End If
        Next
    End With
    
    With TotalesEmpaques
        
        I = 0
        
        .TextMatrix(I, ColPre.Descripci�n) = "TOTAL POR EMPAQUES"
        
        .TextMatrix(I, ColPre.CostoPreXMerma) = FormatNumber(SumaGrid(Empaques, ColPre.CostoPreXMerma), Std_Decm)
        .TextMatrix(I, ColPre.CostoProd) = FormatNumber(SumaGrid(Empaques, ColPre.CostoProd), Std_Decm)
        .TextMatrix(I, ColPre.CostoProdNetoProyectado) = FormatNumber(SumaGrid(Empaques, ColPre.CostoProdNetoProyectado), Std_Decm)
        .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(SumaCostoEmpaques, Std_Decm)
        
        .TextMatrix(I, ColPre.DifCostoNeto) = FormatNumber(SumaGrid(Empaques, ColPre.DifCostoNeto), Std_Decm)
        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(SumaGrid(Empaques, ColPre.PorcVariacionCostoNeto), 2)
        .TextMatrix(I, ColPre.PorcVariacionCostoNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.PorcVariacionCostoNeto)) / Empaques.Rows - 1, 2) & "%"
        
        .TextMatrix(I, ColPre.PorcTipoCosto) = "100%"
        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(SumaGrid(Empaques, ColPre.PorcCostoGlobal), 2) & "%"
        
    End With
    
    SumaProyeccionMateriales = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.CostoProdNetoProyectado)) + _
    CDbl(TotalesEmpaques.TextMatrix(0, ColPre.CostoProdNetoProyectado))
    
    With CostosDirectos
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColDir.TipoCosto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColDir.MontoReal)) Then
                If SumaGlobal <> 0 Then
                    .TextMatrix(I, ColDir.PorcCostoGlobal) = FormatNumber(.TextMatrix(I, ColDir.MontoReal) / SumaGlobal * 100, 2) & "%"
                Else
                    .TextMatrix(I, ColDir.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
                End If
            End If
        Next
    End With
    
    SumaProyeccionCostosDirectos = SumaGrid(CostosDirectos, ColDir.MontoProyectado)
    
    SumaVariacionMateriales = SumaGrid(Ingredientes, ColPre.DifCostoNeto) + _
    SumaGrid(Empaques, ColPre.DifCostoNeto)
    
    SumaVariacionCostosDirectos = SumaGrid(CostosDirectos, ColDir.DifMontos)
    
    With GrdTotales
        
        .TextMatrix(1, ColTot.MontoProyectado) = FormatNumber(SumaProyeccionMateriales, Std_Decm)
        .TextMatrix(2, ColTot.MontoProyectado) = FormatNumber(SumaProyeccionCostosDirectos, Std_Decm)
        .TextMatrix(3, ColTot.MontoProyectado) = FormatNumber(SumaProyeccionMateriales + SumaProyeccionCostosDirectos, Std_Decm)
        
        If SumaGlobal <> 0 Then
            
            .TextMatrix(1, ColTot.MontoReal) = FormatNumber(SumaCostoMateriales, Std_Decm)
            .TextMatrix(1, ColTot.PorcCostoGlobal) = FormatNumber( _
            SumaCostoMateriales / SumaGlobal * 100, 2) & "%"
            
            .TextMatrix(2, ColTot.MontoReal) = FormatNumber(SumaCostosDirectos, Std_Decm)
            .TextMatrix(2, ColTot.PorcCostoGlobal) = FormatNumber(SumaCostosDirectos / SumaGlobal * 100, 2) & "%"
            
            .TextMatrix(3, ColTot.MontoReal) = FormatNumber(SumaGlobal, Std_Decm)
            .TextMatrix(3, ColTot.PorcCostoGlobal) = "100%"
            
        Else
            
            .TextMatrix(1, ColTot.MontoReal) = FormatNumber(SumaCostoMateriales, Std_Decm)
            .TextMatrix(1, ColTot.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
            
            .TextMatrix(2, ColTot.MontoReal) = FormatNumber(SumaCostosDirectos, Std_Decm)
            .TextMatrix(2, ColTot.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
            
            .TextMatrix(3, ColTot.MontoReal) = FormatNumber(SumaGlobal, Std_Decm)
            .TextMatrix(3, ColTot.PorcCostoGlobal) = "100%"
            
        End If
        
        .TextMatrix(1, ColTot.DifMontos) = FormatNumber(CDbl(.TextMatrix(1, ColTot.MontoProyectado)) - CDbl(.TextMatrix(1, ColTot.MontoReal)), Std_Decm)
        .TextMatrix(2, ColTot.DifMontos) = FormatNumber(CDbl(.TextMatrix(2, ColTot.MontoProyectado)) - CDbl(.TextMatrix(2, ColTot.MontoReal)), Std_Decm)
        .TextMatrix(3, ColTot.DifMontos) = FormatNumber(CDbl(.TextMatrix(3, ColTot.MontoProyectado)) - CDbl(.TextMatrix(3, ColTot.MontoReal)), Std_Decm)
        
        If SumaProyeccionMateriales <> 0 Then
            .TextMatrix(4, ColTot.MontoReal) = FormatNumber(SumaVariacionMateriales / SumaProyeccionMateriales * 100, 2) & "%"
        Else
            .TextMatrix(4, ColTot.MontoReal) = FormatNumber(0, Std_Decm) & "%"
        End If
        
        If SumaVariacionCostosDirectos <> 0 Then
            .TextMatrix(5, ColTot.MontoReal) = FormatNumber(SumaVariacionCostosDirectos / SumaVariacionCostosDirectos * 100, 2) & "%"
        Else
            .TextMatrix(5, ColTot.MontoReal) = FormatNumber(0, Std_Decm) & "%"
        End If
        
    End With
    
Finally:
    
    Ingredientes.Visible = True
    Empaques.Visible = True
    CostosDirectos.Visible = FrmAppLink.GetNivelUsuario >= FrmAppLink.PRDNivelVerCostosDirectos And FrmAppLink.PRDNivelVerCostosDirectos > 0
    GrdTotales.Visible = True
    
    Exit Sub
    
Error:
    
    'Resume ' Debugging
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    GoTo Finally
    
End Sub

Private Sub RecalcularSumario()
    txtCostosDir = GrdTotales.TextMatrix(2, ColTot.MontoReal)
    txtCostoTotal = GrdTotales.TextMatrix(3, ColTot.MontoReal)
End Sub

Private Sub FrameTabProceso_Click()
    lblProceso_Click
End Sub

Private Sub FrameTabSumario_Click()
    lblSumario_Click
End Sub

Private Sub lblCargarLotes_Click()
    
    If Not IsNumeric(txtMulti) Then
        Mensaje True, "Cantidad inv�lida."
        Exit Sub
    End If
    
    mMultiplicarAnt = CDbl(txtCantProducto)
    mMultiplicar = Fix(CDbl(txtMulti)) * CDbl(txtCantiLote)
    
    If CDbl(mMultiplicar) > 0 Then
        'txtProducir.Tag = Empty
        'Buscar_Formula txtFormula.Text
        txtMulti.Tag = FormatNumber(mMultiplicar, mDecimales)
        EstablecerCantidadProducto
    Else
        mMultiplicar = mMultiplicarAnt
    End If
    
End Sub

Private Sub lblDatosRegistroVSOrden_Click()
    FrameProceso.Visible = False
    FrameSumario.Visible = False
    FrameRegistroVSOrden.Visible = True
    RecogerDatosRegistroVSOrden
End Sub

Private Sub RecogerDatosRegistroVSOrden()
    
    FrameRegistroVSOrden.Enabled = True
    txtPesoPromedioUni.Enabled = True
    txtNumHorasTrabajadas.Enabled = True
    txtNumTrabajadores.Enabled = True
    
    txtPesoUniReg = txtPesoUni
    txtCantibulReg = txtCantibul
    txtPesoEmpReg = txtPesoEmp
    mCantMax = IIf(CantidadTotalOrden <> 0, CantidadTotalOrden, CDbl(txtCantProdReg))
    txtCantProdOrden = mCantMax
    txtPesoTotalProductosReg = txtPesoTotalProductos
    txtPesoTotalMezclaReg = txtPesoTotalMezcla
    
    txtCantProdReg = FormatNumber(txtCantProducto.Text, mDecimales)
    
    If CDbl(txtCantiLote) <> 0 Then
        txtCantLoteOrden = FormatNumber(CDbl(txtCantProdOrden) / CDbl(txtCantiLote), 0)
        txtLotesRegistro = FormatNumber(CDbl(txtCantProdReg) / CDbl(txtCantiLote), 0)
    Else
        txtCantLoteOrden = FormatNumber(0, 0)
        txtLotesRegistro = FormatNumber(0, 0)
    End If
    
    If CDbl(txtCantProdOrden) <> 0 Then
        txtPesoMezclaEstandar = FormatNumber((CDbl(txtPesoTotalMezcla) / CDbl(txtCantProdOrden)) * (CDbl(txtCantProdOrden) / CDbl(txtCantProducto)), 3)
    Else
        txtPesoMezclaEstandar = FormatNumber(0, 0)
    End If
    
    txtPesoMezclaTotalRepReg = TotalesIngredientes.TextMatrix(0, ColPre.NetCant)
    txtPesoTotalProductoRepReg = FormatNumber(CDbl(txtCantProdReg) * CDbl(txtPesoPromedioEmp), 3)
    If Not IsNumeric(txtPesoMezclaTotalRepReg) Then txtPesoMezclaTotalRepReg = 0
    txtDiferenciaPesosTotalesReg = FormatNumber(CDbl(txtPesoMezclaTotalRepReg) - CDbl(txtPesoTotalProductoRepReg), 3)
    
    If CDbl(txtPesoMezclaTotalRepReg) <> 0 Then
        txtPorcVariacionPesosTotalesReg = FormatNumber(CDbl(txtDiferenciaPesosTotalesReg) / CDbl(txtPesoMezclaTotalRepReg) * 100, 2) & "%"
    Else
        txtPorcVariacionPesosTotalesReg = FormatNumber(0, 2) & "%"
    End If
    
    txtCostoUniReg = txtCostoUni
    
    txtDifBatch = FormatNumber(CDbl(txtLotesRegistro) - CDbl(txtCantLoteOrden), 2)
    txtDifCant = FormatNumber(CDbl(txtCantProdReg) - CDbl(txtCantProdOrden), 2)
    
End Sub

Private Sub lblMultiplicarFormula_Click()
    
    If Not IsNumeric(txtMulti) Then
        Mensaje True, "Cantidad inv�lida."
        Exit Sub
    End If
    
'    If CDbl(txtCantiLote) > 0 And CDbl(txtCapacidadMaximaLote) > 0 Then
'        mLotes = Round(CDbl(txtMulti) / CDbl(txtCantiLote), 8)
'        mRes = Round(mLotes - Fix(mLotes), 8)
'        If mRes >= 0.001 Then
'            mSugMinLotes = Fix(mLotes)
'            mSugMaxLotes = mSugMinLotes + 1
'            mSugMin = Fix(mSugMinLotes * CDbl(txtCantiLote))
'            mSugMax = Fix(mSugMaxLotes * CDbl(txtCantiLote))
'            ActivarMensajeGrande 80
'            If Mensaje(False, "Atenci�n, la cantidad que est� asignando es menor o mayor " & _
'            "a una cantidad de Batches Completos. Se sugiere colocar " & mSugMinLotes & _
'            " o " & mSugMaxLotes & " Batches (" & mSugMin & " o " & mSugMax & ") . Presione Aceptar para continuar si desea omitir " & _
'            "esta alerta o Cancelar para aplicar la cantidad sugerida.") Then
'                txtMulti = Fix(CDbl(txtMulti))
'            Else
'                If Mensaje(False, "Presione Aceptar para cargar " & mSugMinLotes & " Batches (" & mSugMin & ")" & _
'                " o Cancelar para cargar " & mSugMaxLotes & " Batches (" & mSugMax & ")") Then
'                    txtMulti = mSugMin
'                Else
'                    txtMulti = mSugMax
'                End If
'            End If
'        End If
'    Else
'        txtMulti = Fix(CDbl(txtMulti))
'    End If
    
    If CDbl(txtMulti) > 0 Then
        txtMulti.Tag = FormatNumber(CDbl(CantidadBaseFormula) * txtMulti.Text, mDecimales)
        EstablecerCantidadProducto
        'txtProducir.Tag = Empty
        'mMultiplicar = CDbl(txtMulti)
        'Buscar_Formula txtFormula.Text
    Else
        txtMulti = 1
    End If
    
End Sub

Private Sub EstablecerCantidadProducto()
    
    If Not IsNumeric(txtMulti.Tag) Then
        Mensaje True, "Cantidad inv�lida."
        Exit Sub
    End If
    
    'If CDbl(txtCantiLote) > 0 And CDbl(txtCapacidadMaximaLote) > 0 Then
        'mLotes = Round(CDbl(txtMulti.Tag) / CDbl(txtCantiLote), 8)
        'mRes = Round(mLotes - Fix(mLotes), 8)
        'If mRes >= 0.001 Then
            'mSugMinLotes = Fix(mLotes)
            'mSugMaxLotes = mSugMinLotes + 1
            'mSugMin = Fix(mSugMinLotes * CDbl(txtCantiLote))
            'mSugMax = Fix(mSugMaxLotes * CDbl(txtCantiLote))
            'ActivarMensajeGrande 80
            'If Mensaje(False, "Atenci�n, la cantidad que est� produciendo es menor o mayor " & _
            "a una cantidad de Batches Completos. Se sugiere colocar " & mSugMinLotes & _
            " o " & mSugMaxLotes & " Batches (" & mSugMin & " o " & mSugMax & ") . Presione Aceptar para continuar si desea omitir " & _
            "esta alerta o Cancelar para producir la cantidad sugerida.") Then
                
            'Else
                'If Mensaje(False, "Presione Aceptar para cargar " & mSugMinLotes & " Batches (" & mSugMin & ")" & _
                " o Cancelar para cargar " & mSugMaxLotes & " Batches (" & mSugMax & ")") Then
                    'txtMulti.Tag = mSugMin
                'Else
                    'txtMulti.Tag = mSugMax
                'End If
            'End If
        'End If
    'Else
        'txtMulti = Fix(CDbl(txtMulti))
    'End If
    
    If CDbl(txtMulti.Tag) > 0 Then
        txtCantProducto.Text = txtMulti.Tag
        'txtProducir.Tag = Empty
        'If txtFormula <> Empty Then
            'mMultiplicar = CDbl(txtMulti.Tag)
            'Buscar_Formula txtFormula.Text
        'End If
    Else
        txtMulti.Tag = FormatNumber(1, mDecimales)
        txtCantProducto.Text = txtMulti.Tag
    End If
    
    mMultiplicar = CDbl(txtMulti.Tag)
    txtPesoTotalProductos.Text = FormatNumber(CDbl(txtPesoEmp.Tag) * CDbl(txtMulti.Tag), 3)
    
    'If (CantidadBaseFormula > 0) Then
        
'        With Ingredientes
'        For I = 1 To .Rows - 1
'            If .TextMatrix(I, ColPre.Producto) <> Empty Then
'                .TextMatrix(I, ColPre.CantProyectada) = FormatNumber(mMultiplicar * _
'                (CDbl(.TextMatrix(I, ColPre.CantBaseFormula)) / CantidadBaseFormula), .TextMatrix(I, ColPre.LDeci))
'                .TextMatrix(I, ColPre.NetCantProyectada) = FormatNumber( _
'                CDbl(.TextMatrix(I, ColPre.CantProyectada)) * (1# + (CDbl(.TextMatrix(I, ColPre.PorcMerma)) / 100)), .TextMatrix(I, ColPre.LDeci))
'            End If
'        Next
'        End With
'
'        With Empaques
'        For I = 1 To .Rows - 1
'            If .TextMatrix(I, ColPre.Producto) <> Empty Then
'                .TextMatrix(I, ColPre.CantProyectada) = FormatNumber(CDbl(txtMulti.Tag) * _
'                (CDbl(.TextMatrix(I, ColPre.CantBaseFormula)) / CantidadBaseFormula), .TextMatrix(I, ColPre.LDeci))
'                .TextMatrix(I, ColPre.NetCantProyectada) = FormatNumber( _
'                CDbl(.TextMatrix(I, ColPre.CantProyectada)) * (1# + (CDbl(.TextMatrix(I, ColPre.PorcMerma)) / 100)), .TextMatrix(I, ColPre.LDeci))
'            End If
'        Next
'        End With
        
        RecalcularTransaccion
        
    'End If
    
End Sub

Private Sub lblProceso_Click()
    FrameSumario.Visible = False
    FrameRegistroVSOrden.Visible = False
    FrameProceso.Visible = True
End Sub

Private Sub lblSumario_Click()
    FrameProceso.Visible = False
    FrameRegistroVSOrden.Visible = False
    FrameSumario.Visible = True
    RecalcularSumario
End Sub

Private Sub ModifFlag_Click()
    
    If TmpDisableEvent Then
        TmpDisableEvent = False
        Exit Sub
    End If
    
    If ModifFlag Then
        
        'PRDNivelTrabajarSinFormula = 11 ' Debug
        
        If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelTrabajarSinFormula Then
            
            'PRDNivelTrabajarSinFormula = 6 ' Debug
            
            Dim FrmAutorizacion: Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
            
            FrmAutorizacion.mNivel = FrmAppLink.PRDNivelTrabajarSinFormula
            FrmAutorizacion.Titulo = StellarMensaje(2967) '"Introduzca las credenciales " & _
            "de un usuario que autorice a trabajar sin " & _
            "base a una formula."
            
            Set FrmAutorizacion.mConexion = Ent.BDD
            
            FrmAutorizacion.Show vbModal
            
            If FrmAutorizacion.mAceptada Then
                
                Autorizante = FrmAutorizacion.mUsuario
                Set FrmAutorizacion = Nothing
                ' Continuar
                
            Else
                
                Set FrmAutorizacion = Nothing
                TmpDisableEvent = True
                ModifFlag.Value = vbUnchecked
                Exit Sub
                
            End If
            
        End If
        
    End If
    
    If ModifFlag Then
        txtFormula.Enabled = False
        CmdFormula.Enabled = False
        txtFormula.Text = Empty
        txtFormula.BackColor = FrameProceso.BackColor
        txt_descripcion.Enabled = False
        txt_descripcion.BackColor = FrameProceso.BackColor
        txtProducir.Enabled = True
        txtProducir.BackColor = txtCapacidadMaximaLote.BackColor
        txtDescProducto.BackColor = txtProducir.BackColor
        Ingredientes.Row = 1
        lblAplicarFormula.Visible = False
        txtMulti.Visible = False
        FrameMultiplicarFormula.Visible = False
        FrameCargarLotes.Visible = False
        MenuAccess True, False
        CargarGrid False
    Else
        Cancelar
    End If
    
End Sub

Private Sub txt_descripcion_GotFocus()
    txt_descripcion.SelStart = 0
    txt_descripcion.SelLength = Len(txt_descripcion.Text)
End Sub

Private Sub txt_descripcion_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub txt_destino_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            lastdestino = txt_destino.Text
            SafeFocus txt_destino
            TmpDisableEvent = True
            
            Set Campo_Txt = txt_destino
            Set Campo_Lbl = lbl_destino
            
            Call Consulta_F2(Me, "GENERICO", Stellar_Mensaje(65), Campo_Txt, Campo_Lbl) 'DEPOSITOS
            
            Tecla_Pulsada = False
            
            If lastdestino <> txt_destino.Text Then
                txt_destino_LostFocus
            End If
            
            TS = Now
            TS1 = DateAdd("s", 1, TS)
            
            While TS < TS1
                TS = Now
                DoEvents
            Wend
            
            Tecla_Pulsada = False
            TmpDisableEvent = False
            
        Case Is = vbKeyReturn
            
            oTeclado.Key_Tab
            
    End Select
    
End Sub

Private Sub txt_destino_LostFocus()
    If txt_destino.Text <> "" And Tecla_Pulsada = False Then
        Call Apertura_Recordset(RsEureka)
        RsEureka.Open "select * from ma_deposito " & _
        "where c_CodDeposito = '" & txt_destino & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsEureka.EOF Then
            lbl_destino.Caption = RsEureka!c_Descripcion
            SafeFocus Ingredientes
        Else
            'Call Mensaje(True, "El c�digo no esta asignado a ning�n dep�sito.")
            Mensaje True, StellarMensaje(16314)
            lcDeposito = ""
            lbl_destino.Caption = ""
            txt_destino.Text = ""
        End If
        Call Cerrar_Recordset(RsEureka)
    Else
        lbl_destino.Caption = ""
    End If
End Sub

Private Sub txt_ORIGEN_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            lastdeposito = txt_origen.Text
            SafeFocus txt_origen
            TmpDisableEvent = True
            Set Campo_Txt = txt_origen
            Set Campo_Lbl = lbl_origen
            Call Consulta_F2(Me, "GENERICO", Stellar_Mensaje(65), Campo_Txt, Campo_Lbl) 'DEPOSITOS
            Tecla_Pulsada = False
            If lastdeposito <> txt_origen.Text Then
                txt_origen_LostFocus
            End If
            TS = Now
            TS1 = DateAdd("s", 1, TS)
            While TS < TS1
                TS = Now
                DoEvents
            Wend
            Tecla_Pulsada = False
            TmpDisableEvent = False
        Case Is = vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub txt_origen_LostFocus()
    If txt_origen.Text <> "" And Tecla_Pulsada = False Then
        Call Apertura_Recordset(RsEureka)
        RsEureka.Open "select * from ma_deposito " & _
        "where c_CodDeposito = '" & txt_origen & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsEureka.EOF Then
            lbl_origen.Caption = RsEureka!c_Descripcion
        Else
            'Call Mensaje(True, "El c�digo no esta asignado a ning�n dep�sito.")
            Mensaje True, StellarMensaje(16314)
            lcDeposito = ""
            lbl_origen.Caption = ""
            txt_origen.Text = ""
        End If
        Call Cerrar_Recordset(RsEureka)
    Else
        lbl_origen.Caption = ""
    End If
End Sub

Private Sub txtCantProducto_Click()
    mValorAnt = txtCantProducto
    mValorNew = QuickInputRequest("Ingrese la Cantidad a producir.", True, , mValorAnt, "Escriba la Cantidad", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 1
        If CDbl(mValorNew) <> CDbl(mValorAnt) And CDbl(mValorNew) >= 1 Then
            txtMulti.Tag = FormatNumber(Abs(mValorNew), mDecimales)
            EstablecerCantidadProducto
        End If
    End If
End Sub

Private Sub txtCapacidadMaximaLote_Click()
    mValorAnt = txtCapacidadMaximaLote
    mValorNew = QuickInputRequest("Ingrese el Peso Maximo / Capacidad del Batch.", True, , mValorAnt, "Escriba la Cantidad", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtCapacidadMaximaLote = FormatNumber(mValorNew, Std_Decm)
        End If
    End If
End Sub

Private Sub txtCostoHoraCargaFabril_Click()
    mValorAnt = txtCostoHoraCargaFabril
    mValorNew = QuickInputRequest("Ingrese el Costo por Hora de Carga Fabril.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtCostoHoraCargaFabril = FormatNumber(Abs(mValorNew), Std_Decm)
        End If
    End If
End Sub

Private Sub txtCostoHoraHombre_Click()
    mValorAnt = txtCostoHoraHombre
    mValorNew = QuickInputRequest("Ingrese el Costo por Hora Hombre.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtCostoHoraHombre = FormatNumber(Abs(mValorNew), Std_Decm)
        End If
    End If
End Sub

Private Sub TxtEdit_GotFocus()
    If Ingredientes.Col = ColPre.Producto And Empaques.TextMatrix(Empaques.Row, ColPre.Producto) = Empty Then
        Who_Ami = 1
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    ElseIf Ingredientes.Col = ColPre.Producto And Empaques.TextMatrix(Empaques.Row, ColPre.Producto) <> Empty Then
        Who_Ami = 1
        txtEdit2.Enabled = False
        txtEdit2.Text = Empty
    End If
End Sub

Private Sub txtEdit_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            'oTeclado.Key_Tab
            Call txtEdit_LostFocus
        Case vbKeyEscape
            Call Ingredientes_KeyPress(vbKeyEscape)
        Case Else
            If Ingredientes.Col = ColPre.PorcMerma Then ' MERMA
                Select Case KeyAscii
                    Case 48 To 57
                    Case 8
                    Case 46
                    Case Else
                        KeyAscii = 0
                End Select
            End If
    End Select
End Sub

Private Sub txtEdit_LostFocus()
    
    Dim Mensajes As String, RsEureka As New ADODB.Recordset, ColAct As Integer, FilAct As Integer, Merma As Double, Monto As Double
    
    On Error GoTo Errores
    
    With Ingredientes
        
        Select Case .Col
            
            Case ColPre.Producto
                
                'Producto
                
                If Tecla_Pulsada = True Then Exit Sub
                
                If Trim(txtedit.Text) <> "" Then
                    
                    txtedit.Text = AlternateCode(Trim(txtedit.Text))
                    
                    If Not (Validar_Repeticiones(Ingredientes, txtedit) _
                    Or Validar_Repeticiones2(Empaques, txtedit)) _
                    And txtedit.Text <> txtProducir.Text Then
                        
                        Set RsProducto = ScanData("SELECT * FROM MA_PRODUCTOS INNER JOIN MA_MONEDAS " & _
                        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
                        "WHERE c_Codigo = '" & Trim(txtedit.Text) & "' ")
                        
                        If Not RsProducto.EOF Then
                            
                            If RsProducto!n_Activo = 1 Then
                                
                                .Text = txtedit.Text
                                
                                .TextMatrix(.Row, ColPre.LDeci) = RsProducto!Cant_Decimales
                                .TextMatrix(.Row, ColPre.Descripci�n) = RsProducto!c_Descri
                                .TextMatrix(.Row, ColPre.Presentaci�n) = isDBNull(RsProducto!c_Presenta, Empty)
                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, RsProducto!Cant_Decimales)
                                .TextMatrix(.Row, ColPre.CostoPre) = FormatNumber((RsProducto.Fields(CostoActivo).Value * (RsProducto!n_Factor / CDbl(MSK_FACTOR.Text))), Std_Decm)
                                .TextMatrix(.Row, ColPre.CostoProd) = .TextMatrix(.Row, ColPre.CostoPre)
                                .TextMatrix(.Row, ColPre.nCantibul) = RsProducto!n_Cantibul
                                
                                'If CDbl(txtCantiLote.Text) > 0 Then
                                    '.Col = ColPre.CantLote
                                'Else
                                    .Col = ColPre.NetCant
                                'End If
                                
                                oTeclado.Key_Return
                                
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Producto no esta activo.")
                                Mensaje True, StellarMensaje(16101)
                                Tecla_Pulsada = False
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = ColPre.Producto
                        End If
                        
                        RsProducto.Close
                        
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista.")
                        Mensaje True, StellarMensaje(2816)
                        Tecla_Pulsada = False
                        .Col = ColPre.Producto
                        .Text = ""
                    End If
                    
                Else
                    .Col = ColPre.Producto
                End If
                
            Case ColPre.NetCant 'Cantidad
                
                .Enabled = True
                
                If Trim(txtedit.Text) <> "" Then
                    
                    If CheckCad(txtedit, CDbl(.TextMatrix(.Row, ColPre.LDeci)), , False) Then
                        
                        If CDbl(txtedit.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Text = FormatNumber(txtedit.Text, CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.Canti) = FormatNumber(CDbl(.Text) / (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            '.TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                oTeclado.Key_Down
                            End If
                            
                            Call Apertura_Recordset(RsRecetas)
                            texto_cod = .TextMatrix(.Row, ColPre.Producto)
                            
                            Criterio = "select *, ma_productos." & CostoActivo & " AS Costo from ma_productos " & _
                            "where c_Codigo = '" & texto_cod & "'"
                            
                            RsRecetas.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            Call Apertura_Recordset(rsPmoneda)
                            
                            rsPmoneda.Open "select * from ma_monedas where c_CodMoneda = '" & RsRecetas!c_CodMoneda & "'", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                            
'                            If CDbl(.TextMatrix(.Row, ColPre.PorcMerma)) = 0 Then
'                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, 2)
'                                .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber(0, Std_Decm)
'                                If Not ManejaMermaExplicita Then
'                                    .Rows = .Rows + 1
'                                Else
'                                    .Col = ColPre.PorcMerma
'                                End If
'                                oTeclado.Key_Return
'                            Else
'                                .Col = ColPre.PorcMerma
'                                txtedit = .TextMatrix(.Row, .Col)
'                                TmpNoCalcular = True
'                                txtEdit_LostFocus
'                                TmpNoCalcular = False
'                            End If
                            
                            RecalcularTransaccion
                            
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        
                        If txtedit.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            Exit Sub
                        Else
                            .Col = ColPre.NetCant
                            .Text = 1
                            oTeclado.Key_Return
                        End If
                        
                    End If
                    
                End If
                
            Case ColPre.PorcMerma 'Merma
                
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If CheckCad(txtedit, 3, , False) Then
                        
                        If CDbl(txtedit.Text) >= 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Enabled = True
                            .Text = FormatNumber(CDbl(txtedit.Text), 2)
                            
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                oTeclado.Key_Down
                            End If
                            
                            .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber((CDbl(txtedit.Text) * CDbl(.TextMatrix(.Row, ColPre.CostoPre))) / 100, Std_Decm)
                            
                            '.TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.Canti)) * (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            ' Primero calcular la base sin la merma en base a la cantidad introducida
                            .TextMatrix(.Row, ColPre.Canti) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) / (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            ' Luego recalcular la cantidad con merma asegurando que no haya perdida por exceso de decimales, _
                            tomar en cuenta los del producto
                            .TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.Canti)) * (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            If Not TmpNoCalcular Then
                                RecalcularTransaccion
                                Call SetDefMSGrid(Ingredientes, .Row + 1, ColPre.Producto)
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor es negativo")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        If txtedit.Text <> "" Then
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                    End If
                    
                End If
            
        End Select
        
    End With
    
    txtedit.Visible = False
    txtedit.Text = ""
    
    If Tecla_Pulsada = False Then
        Ingredientes.Enabled = True
        SafeFocus Ingredientes
    End If
    
    If Ingredientes.Col = ColPre.Producto Then
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Private Sub txtedit2_GotFocus()
    If Empaques.Col = ColPre.Producto And Ingredientes.TextMatrix(Ingredientes.Row, ColPre.Producto) = Empty Then
        Who_Ami = 2
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    ElseIf Empaques.Col = ColPre.Producto And Ingredientes.TextMatrix(Ingredientes.Row, ColPre.Producto) <> Empty Then
        Who_Ami = 2
        txtEdit2.Enabled = False
        txtEdit2.Text = Empty
    End If
End Sub

Private Sub txtedit2_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            Call txtedit2_LostFocus
        Case vbKeyEscape
            Call Empaques_KeyPress(vbKeyEscape)
        Case Else
            If Empaques.Col = ColPre.PorcMerma Then ' MERMA
                Select Case KeyAscii
                    Case 48 To 57
                    Case 8
                    Case 46
                    Case Else
                        KeyAscii = 0
                End Select
            End If
    End Select
End Sub

Private Sub txtedit2_LostFocus()
    
    Dim Mensajes As String, RsEureka As New ADODB.Recordset, ColAct As Integer, FilAct As Integer, Merma As Double, Monto As Double
    
    On Error GoTo Errores
    
    With Empaques
        
        Select Case .Col
            
            Case ColPre.Producto
                
                'Producto
                
                If Tecla_Pulsada = True Then Exit Sub
                
                If Trim(txtEdit2.Text) <> "" Then
                    
                    txtEdit2.Text = AlternateCode(Trim(txtEdit2.Text))
                    
                    If Not (Validar_Repeticiones(Ingredientes, txtEdit2) _
                    Or Validar_Repeticiones2(Empaques, txtEdit2)) _
                    And txtEdit2.Text <> txtProducir.Text Then
                        
                        Set RsProducto = ScanData("SELECT * FROM MA_PRODUCTOS INNER JOIN MA_MONEDAS " & _
                        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
                        "WHERE c_Codigo = '" & Trim(txtEdit2.Text) & "' ")
                        
                        If Not RsProducto.EOF Then
                            
                            If RsProducto!n_Activo = 1 Then
                                
                                .Text = txtEdit2.Text
                                
                                .TextMatrix(.Row, ColPre.LDeci) = RsProducto!Cant_Decimales
                                .TextMatrix(.Row, ColPre.Descripci�n) = RsProducto!c_Descri
                                .TextMatrix(.Row, ColPre.Presentaci�n) = isDBNull(RsProducto!c_Presenta, Empty)
                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, RsProducto!Cant_Decimales)
                                .TextMatrix(.Row, ColPre.CostoPre) = FormatNumber((RsProducto.Fields(CostoActivo).Value * (RsProducto!n_Factor / CDbl(MSK_FACTOR.Text))), Std_Decm)
                                .TextMatrix(.Row, ColPre.CostoProd) = .TextMatrix(.Row, ColPre.CostoPre)
                                .TextMatrix(.Row, ColPre.nCantibul) = RsProducto!n_Cantibul
                                
                                .Col = ColPre.NetCant
                                
                                oTeclado.Key_Return
                                
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Producto no esta activo.")
                                Mensaje True, StellarMensaje(16101)
                                Tecla_Pulsada = False
                            End If
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = ColPre.Producto
                        End If
                        RsProducto.Close
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista.")
                        Mensaje True, StellarMensaje(2816)
                        Tecla_Pulsada = False
                        .Col = ColPre.Producto
                        .Text = ""
                    End If
                Else
                    .Col = ColPre.Producto
                End If
                
            Case ColPre.NetCant 'Cantidad
                
                .Enabled = True
                
                If Trim(txtEdit2.Text) <> "" Then
                    
                    If CheckCad(txtEdit2, CDbl(.TextMatrix(.Row, ColPre.LDeci)), , False) Then
                        
                        If CDbl(txtEdit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Text = FormatNumber(txtEdit2.Text, CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.Canti) = FormatNumber(CDbl(.Text) / (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            '.TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                oTeclado.Key_Down
                            End If
                            
                            Call Apertura_Recordset(RsRecetas)
                            texto_cod = .TextMatrix(.Row, ColPre.Producto)
                            
                            Criterio = "select *, ma_productos." & CostoActivo & " AS Costo from ma_productos " & _
                            "where c_Codigo = '" & texto_cod & "'"
                            
                            RsRecetas.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            Call Apertura_Recordset(rsPmoneda)
                            
                            rsPmoneda.Open "select * from ma_monedas where c_CodMoneda = '" & RsRecetas!c_CodMoneda & "'", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                            
'                            If CDbl(.TextMatrix(.Row, ColPre.PorcMerma)) = 0 Then
'                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, 2)
'                                .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber(0, Std_Decm)
'                                If Not ManejaMermaExplicita Then
'                                    .Rows = .Rows + 1
'                                Else
'                                    .Col = ColPre.PorcMerma
'                                End If
'                                oTeclado.Key_Return
'                            Else
'                                .Col = ColPre.PorcMerma
'                                txtedit = .TextMatrix(.Row, .Col)
'                                TmpNoCalcular = True
'                                txtEdit_LostFocus
'                                TmpNoCalcular = False
'                            End If
                            
                            RecalcularTransaccion
                            
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        
                        If txtEdit2.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            Exit Sub
                        Else
                            .Col = ColPre.NetCant
                            .Text = 1
                            oTeclado.Key_Return
                        End If
                        
                    End If
                    
                End If
                
            Case ColPre.PorcMerma 'Merma
                
                If Trim(txtEdit2.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If CheckCad(txtEdit2, 3, , False) Then
                        
                        If CDbl(txtEdit2.Text) >= 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Enabled = True
                            .Text = FormatNumber(CDbl(txtEdit2.Text), 2)
                            
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                'oTeclado.Key_Down
                            End If
                            
                            .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber((CDbl(txtEdit2.Text) * CDbl(.TextMatrix(.Row, ColPre.CostoPre))) / 100, Std_Decm)
                            
                            '.TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.Canti)) * (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            ' Primero calcular la base sin la merma en base a la cantidad introducida
                            .TextMatrix(.Row, ColPre.Canti) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) / (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            ' Luego recalcular la cantidad con merma asegurando que no haya perdida por exceso de decimales, _
                            tomar en cuenta los del producto
                            .TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.Canti)) * (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            If Not TmpNoCalcular Then
                                RecalcularTransaccion
                                Call SetDefMSGrid(Empaques, .Rows - 1, ColPre.Producto)
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor es negativo")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        If txtEdit2.Text <> "" Then
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                    End If
                    
                End If

        End Select
        
    End With
    
    txtEdit2.Visible = False
    txtEdit2.Text = ""
    
    If Tecla_Pulsada = False Then
        Empaques.Enabled = True
        SafeFocus Empaques
    End If
    
    If Empaques.Col = ColPre.Producto Then
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Sub Salir()
    Set FRM_PRODUCCION_ORDEN_MANUAL_3 = Nothing
    Unload Me
End Sub

Sub Cancelar( _
Optional ByVal MantenerFormula As Boolean = False _
)
    
    Call Apertura_RecordsetC(RsProducto)
    Call Apertura_RecordsetC(RsRecetas)
    Call Apertura_RecordsetC(rsPmoneda)
    Call Apertura_RecordsetC(RsInventario)
    Call Apertura_RecordsetC(RsCodigos)
    Call Apertura_RecordsetC(RsEureka)
    
    FlgUpd = False
    FlgAdd = False
    ModifFlag.Value = vbUnchecked
    
    txtFormula.Enabled = True
    CmdFormula.Enabled = True
    txtFormula.BackColor = txtCapacidadMaximaLote.BackColor
    txtFormula = Empty
    
    txt_descripcion.Enabled = False
    txt_descripcion.Text = Empty
    txt_descripcion.BackColor = txtCapacidadMaximaLote.BackColor
    
    txtProducir.Enabled = False
    txtProducir.BackColor = FrameProceso.BackColor
    txtProducir.Text = Empty
    txtProducir.Tag = Empty
    
    CmdProducir.Enabled = True
    
    txtDescProducto.Enabled = False
    txtDescProducto.Text = Empty
    txtDescProducto.BackColor = FrameProceso.BackColor
    
    txt_origen.Text = Empty
    txt_destino.Text = Empty
    
    lbl_origen = Empty
    lbl_destino = Empty
    
    lblAplicarFormula.Visible = True
    txtMulti.Visible = True
    FrameMultiplicarFormula.Visible = True
    FrameCargarLotes.Visible = True
    
    txtMulti = 1
    txtMulti.Tag = 1
    mMultiplicar = 1
    
    CargarGrid
    
    lbl_fecha.Caption = Format(Now, "short date")
    
    ModifFlag.Value = vbUnchecked
    
    GridLleno = False
    Tecla_Pulsada = False
    
    txtedit.Visible = False
    txtedit.Text = Empty
    txtEdit2.Visible = False
    txtEdit2.Text = Empty
    
    txtCantProducto.Text = FormatNumber(1, 2)
    lblNomenclaturaCant.Caption = Empty
    txtPesoUni.Text = FormatNumber(0, 2)
    txtCantibul.Text = FormatNumber(1, 2)
    txtPesoEmp.Text = FormatNumber(0, 2)
    txtPesoEmp.Tag = 0
    txtCapacidadMaximaLote.Text = FormatNumber(0, 2)
    txtCapacidadLote.Text = FormatNumber(0, 2)
    txtCantiLote.Text = FormatNumber(0, 0)
    txtCantiLote.Tag = txtCantiLote.Text
    
    txtCostoTotal.Text = FormatNumber(0, Std_Decm)
    txtCostosDir.Text = FormatNumber(0, Std_Decm)
    txtCostoHoraHombre.Text = FormatNumber(0, Std_Decm)
    txtCostoHoraCargaFabril.Text = FormatNumber(0, Std_Decm)
    txtHorasHombreXUnidad.Text = FormatNumber(0, 2)
    txtHorasParadaProgramada.Text = FormatNumber(0, 2)
    txtHorasParadaNoProgramada.Text = FormatNumber(0, 2)
    txtMotivoParadaNoProgramada.Text = Empty
    
    txtPesoPromedioUni = 0
    txtNumTrabajadores = 0
    txtNumHorasTrabajadas = 0
    
    EstatusOPR = Empty
    FormulaOrigenOPR = Empty
    CantidadBaseFormula = 0
    CantidadTotalOrden = 0
    DocumentoRelacion = Empty
    esBackOrder = False
    FechaEstimadaPRD = Date
    FechaVcto = Date
    
    ListSafeIndexSelection CboLnP, 0
    ListSafeIndexSelection CboTurno, 0
    
    Call Nuevo_Consecutivo
    Call Ini_Moneda
    
End Sub

Sub Nuevo_Consecutivo()
    lbl_consecutivo.Caption = Format(No_Consecutivo("Orden_Produccion_Nuevo", False), "000000000")
End Sub

Sub CargarCostosDirectos()
    
    If FrmAppLink.PRDAvanzadaPrecargarCostosDirectos Then
        
        With CostosDirectos
            
            Dim mRsCD As ADODB.Recordset
            
            Set mRsCD = Ent.BDD.Execute("SELECT DISTINCT cs_Grupo FROM MA_AUX_GRUPO WHERE cs_Tipo = 'PCD'")
            
            .Rows = 1
            .Rows = 2
            .Row = 1
            
            I = 0
            
            While Not mRsCD.EOF
                I = I + 1
                .TextMatrix(I, ColDir.TipoCosto) = mRsCD!cs_Grupo
                .TextMatrix(I, ColDir.RealUnitarioDecimal) = 0
                .TextMatrix(I, ColDir.RealUnitario) = FormatNumber(0, Std_Decm)
                .TextMatrix(I, ColDir.MontoReal) = FormatNumber(0, Std_Decm)
                .Rows = .Rows + 1
                mRsCD.MoveNext
            Wend
            
            .Row = .Rows - 1
            .Col = ColDir.TipoCosto
            
            mRsCD.Close
            
        End With
        
    End If
    
End Sub

Sub Cambiar_Moneda()
    
    Dim MActual As String, MActDec As Integer, mDesc As String, MSimbolo As String, mFACTOR As Double
    Dim RsMoneda As New ADODB.Recordset
    
    MActual = dbmoneda.Text
    mDesc = lbl_moneda.Caption
    MActDec = Std_Decm
    MSimbolo = lbl_simbolo.Caption
    mFACTOR = CDbl(MSK_FACTOR.Text)
    
    Call Consulta_F2(Me, "MONEDA_GENERICA", "MONEDAS")
    CodMoneda = FrmAppLink.GetCodMonedaSel
    
    If CodMoneda <> "" Then
        If Buscar_Moneda(dbmoneda, lbl_moneda, MSK_FACTOR, False, CodMoneda, True, True) = False Then
            'Call Mensaje(True, "No existe una moneda predeterminada en el sistema.")
            Mensaje True, StellarMensaje(16289)
            Unload Me
            Exit Sub
        End If
    End If
    
    If MActual <> dbmoneda.Text Then
        'MsgBox "Cambio la moneda, recuerde colocar el cambio de moneda."
        '
        'ESCRIBIR PROCEDIMIENTOS DE RECALCULAR GRIDS
        '
        Call Apertura_RecordsetC(RsMoneda)
        RsMoneda.Open "select * from ma_monedas where c_CodMoneda = '" & dbmoneda & "' ", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsMoneda.EOF Then
            lbl_simbolo.Caption = RsMoneda!c_Simbolo
        Else
            lbl_simbolo.Caption = "Unk"
        End If
        Std_Decm = RsMoneda!n_Decimales
        RsMoneda.Close
        
    End If
    
End Sub

Sub Ini_Moneda()
    Dim RsMoneda As New ADODB.Recordset
    RsMoneda.Open "select * from ma_monedas where b_Activa = 1 AND b_Preferencia = 1", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsMoneda.EOF Then
        dbmoneda.Text = RsMoneda!c_CodMoneda
        lbl_moneda.Caption = RsMoneda!c_Descripcion
        MSK_FACTOR.Text = RsMoneda!n_Factor
        Std_Decm = RsMoneda!n_Decimales
        lbl_simbolo.Caption = RsMoneda!c_Simbolo
        CodMoneda = dbmoneda.Text
    Else
        If Me.Visible Then Unload Me
    End If
End Sub

Function Validar_Repeticiones(Grid As MSFlexGrid, Txt As TextBox) As Boolean
    'Dim FilAct As Integer, ColAct As Integer
    Validar_Repeticiones = False
    With Grid
        'ColAct = Grid.Col
        'FilAct = Grid.Row
        .Enabled = True
        For Cont = 1 To Grid.Rows - 1
            If Grid.TextMatrix(Cont, ColPre.Producto) = Txt.Text Then
                .Enabled = False
                Validar_Repeticiones = True
                Exit Function
            End If
        Next Cont
    End With
End Function

Function Validar_Repeticiones2(Grid As MSFlexGrid, Txt As TextBox) As Boolean
    'Dim FilAct As Integer, ColAct As Integer
    Validar_Repeticiones2 = False
    With Grid
        'ColAct = Grid.Col
        'FilAct = Grid.Row
        .Enabled = True
        For Cont = 0 To Grid.Rows - 1
            If Grid.TextMatrix(Cont, ColPre.Producto) = Txt.Text Then
                .Enabled = False
                Validar_Repeticiones2 = True
                Exit Function
            End If
        Next Cont
    End With
End Function

Function AlternateCode(Codigo As Variant) As Variant
    Dim RsCodigos As New ADODB.Recordset
    Call Apertura_RecordsetC(RsCodigos)
    RsCodigos.Open "select * from ma_codigos where c_Codigo = '" & Codigo & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsCodigos.EOF Then
        AlternateCode = RsCodigos!c_codnasa
    Else
        AlternateCode = Codigo
    End If
    RsCodigos.Close
End Function

Function ScanData(SQLI As String) As ADODB.Recordset
    Dim RsTemp As New ADODB.Recordset
    Call Apertura_RecordsetC(RsTemp)
    RsTemp.Open SQLI, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsTemp.EOF Then
        Set ScanData = RsTemp
    Else
        Set ScanData = RsTemp
    End If
End Function

Function SumaGrid(Grid As MSFlexGrid, Columna As Integer) As Double
    Dim total As Double, Cont As Integer
    total = 0
    For Cont = Grid.FixedRows To Grid.Rows - 1
        If IsNumeric(Replace(Grid.TextMatrix(Cont, Columna), "%", Empty)) Then
            total = total + CDbl(Replace(Grid.TextMatrix(Cont, Columna), "%", Empty))
        End If
    Next Cont
    SumaGrid = Round(total, 8)
End Function

Sub Buscar_Formula(Formula As String)
    
    'BUSCAR PRODUCCION
    Dim RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset, _
    RsCostoDir As New ADODB.Recordset, RsProducir As New ADODB.Recordset, rsMonedas As New ADODB.Recordset
    
    Call Cancelar
    Call Apertura_RecordsetC(RsMaProduccion)
    
    RsMaProduccion.Open "SELECT * FROM MA_PRODUCCION WHERE C_FORMULA = '" & Formula & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    GridLleno = False
    If mMultiplicar <= 0 Then mMultiplicar = 1
    
    If Not RsMaProduccion.EOF Then
        
        lbl_consecutivo.Caption = RsMaProduccion!c_Formula
        
        If RsMaProduccion!TipoFormula <> 1 Then
            Mensaje True, "El tipo de f�rmula es incompatible con el modulo de producci�n avanzada."
            Cancelar
            Exit Sub
        End If
        
        CantidadBaseFormula = RsMaProduccion!CantidadBaseFormula
        
        'BUSCAR DETALLES Y CARGAR
        
        'A PRODUCIR
        Call Apertura_RecordsetC(RsProducir)
        RsProducir.Open "SELECT TOP 1 * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = '1'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsProducir.EOF Then
            
            'For Cont = 1 To Producir.Rows - 2
                
                If RsProducir!n_Activo = 1 Then
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsProducir!c_CodMoneda
                    
                    txtProducir.Text = RsProducir!c_Codigo
                    txtProducir_LostFocus
                    
                Else
                    'Call Mensaje(True, "El producto '" & RsProducir!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    Exit Sub
                End If
                
            'Next Cont
        
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
        
        txtFormula = Formula
        txt_descripcion.Text = RsMaProduccion!c_Descripcion
        
        'MONEDA
        Call Apertura_Recordset(rsMonedas)
        rsMonedas.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda = '" & RsMaProduccion!c_CodMoneda & "' ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not rsMonedas.EOF Then
            lbl_moneda.Caption = rsMonedas!c_Descripcion
            dbmoneda.Text = rsMonedas!c_CodMoneda
            Std_Decm = rsMonedas!n_Decimales
            MSK_FACTOR.Text = FormatNumber(rsMonedas!n_Factor, Std_Decm)
            lbl_simbolo.Caption = rsMonedas!c_Simbolo
        Else
            'Call Mensaje(True, "No se encontr� la moneda de la f�rmula.")
            Mensaje True, StellarMensaje(2806)
            Call Cancelar
            Exit Sub
        End If
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = 0 AND TipoItem = 1", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With Ingredientes
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 2
                
                For Cont = 1 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(RsTrProduccion!CantidadEnLote, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = RsTrProduccion!PorcVsLote
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(RsTrProduccion!PorcVsLote * 100, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        
                        If (FormulaOrigenOPR <> Empty Or txtFormula <> Empty) And CantidadBaseFormula <> 0 Then
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = (RsTrProduccion!n_Cantidad / CantidadBaseFormula)
                        Else
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = 0
                        End If
                        
                        .TextMatrix(Cont, ColPre.CantOrden) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(RsTrProduccion!n_Cantidad * mMultiplicar, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.NetCantOrden) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.NetCant) = FormatNumber((CDbl(.TextMatrix(Cont, ColPre.Canti)) * (1# + (RsTrProduccion!n_Merma) / 100)) * mMultiplicar, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_Merma, 2)
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Exit Sub
                    End If
                Next Cont
                
            Else
                'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                Mensaje True, StellarMensaje(359)
                Call Cancelar
                Exit Sub
            End If
        End With
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = 0 AND TipoItem = 2", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With Empaques
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 1
                
                For Cont = 0 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = 0
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(0, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        
                        If FormulaOrigenOPR <> Empty And CantidadBaseFormula <> 0 Then
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = (RsTrProduccion!n_Cantidad / CantidadBaseFormula)
                        Else
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = 0
                        End If
                        
                        .TextMatrix(Cont, ColPre.CantOrden) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(RsTrProduccion!n_Cantidad * mMultiplicar, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.NetCantOrden) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.NetCant) = FormatNumber((CDbl(.TextMatrix(Cont, ColPre.Canti)) * (1# + (RsTrProduccion!n_Merma) / 100)) * mMultiplicar, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_Merma, 2)
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Exit Sub
                    End If
                Next Cont
                
            Else
                
                'Estos no deben ser obligatorios. Es opcional manejar empaques.
                ''Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                'Mensaje True, StellarMensaje(359)
                'Call Cancelar
                'Exit Sub
                
            End If
            
        End With
        
        CargarCostosDirectos
        
        ' COSTOS DIRECTOS
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM MA_PRODUCCION_COSTOS_DIRECTOS WHERE CodFormula = '" & Formula & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With CostosDirectos
            
            I = 0
            
            Do While Not RsTrProduccion.EOF
                
                If FrmAppLink.PRDAvanzadaPrecargarCostosDirectos Then
                    
                    For I = 0 To .Rows - 1
                        If UCase(RsTrProduccion!TipoCosto) = UCase(.TextMatrix(I, ColDir.TipoCosto)) Then
                            .TextMatrix(I, ColDir.ProyectadoUnitarioDecimal) = (RsTrProduccion!Monto / CantidadBaseFormula)
                            .TextMatrix(I, ColDir.ProyectadoUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal)), Std_Decm)
                            .TextMatrix(I, ColDir.RealUnitarioDecimal) = CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal))
                            .TextMatrix(I, ColDir.RealUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.RealUnitarioDecimal)), Std_Decm)
                            Exit For
                        End If
                    Next
                    
                Else
                    
                    I = I + 1
                    .TextMatrix(I, ColDir.TipoCosto) = RsTrProduccion!TipoCosto
                    .TextMatrix(I, ColDir.ProyectadoUnitarioDecimal) = (RsTrProduccion!Monto / CantidadBaseFormula)
                    .TextMatrix(I, ColDir.ProyectadoUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal)), Std_Decm)
                    .TextMatrix(I, ColDir.RealUnitarioDecimal) = CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal))
                    .TextMatrix(I, ColDir.RealUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.RealUnitarioDecimal)), Std_Decm)
                    .Rows = .Rows + 1
                    
                End If
                
                RsTrProduccion.MoveNext
                
            Loop
            
        End With
        
        GridLleno = True
        
        txtCantProducto = FormatNumber(mMultiplicar, 0)
        EstablecerCantidadProducto
        
        RecalcularTransaccion ' Primero calcular las columnas faltantes
        RecalcularTransaccion ' Luego calcular totales
        lblSumario_Click
        
        txtCapacidadMaximaLote = FormatNumber(RsMaProduccion!CapacidadMaxLote, 3)
        txtCapacidadLote.Text = FormatNumber(SumaGrid(Ingredientes, ColPre.CantLote), 2)
        
        If CDbl(txtPesoEmp.Tag) > 0 Then
            txtCantiLote.Tag = Round(CDbl(txtCapacidadLote) / CDbl(txtPesoEmp), 8)
            txtCantiLote.Text = FormatNumber(txtCantiLote.Tag, 0)
        Else
            txtCantiLote.Text = FormatNumber(0, 0)
            txtCantiLote.Tag = 0
        End If
        
        txtCostoHoraHombre = FormatNumber(RsMaProduccion!CostoHoraHombre, Std_Decm)
        txtCostoHoraCargaFabril = FormatNumber(RsMaProduccion!CostoHoraCargaFabril, Std_Decm)
        txtHorasHombreXUnidad = FormatNumber(RsMaProduccion!HorasHombrePorUnidad, Std_Decm)
        
        RsTrProduccion.Close
        RsProducir.Close
        rsMonedas.Close
        
        Ingredientes.Row = Ingredientes.Rows - 1
        Ingredientes.Col = ColPre.Producto
        Empaques.Row = Empaques.Rows - 1
        Empaques.Col = ColPre.Producto
        CostosDirectos.Row = CostosDirectos.Rows - 1
        CostosDirectos.Col = ColDir.MontoReal
        MenuAccess True, False
        
    Else
        'Call Mensaje(True, "No se encontr� la Formula N� " & Formula)
        Mensaje True, Replace(StellarMensaje(2808), "$(Code)", Formula)
    End If
    
    RsMaProduccion.Close
    
End Sub

Sub Buscar_Orden_Produccion(ByVal tipo As String, _
ByVal orden As String, ByVal CodLocalidad As String)
    
    'BUSCAR PRODUCCION
    Dim RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset, _
    RsCostoDir As New ADODB.Recordset, RsProducir As New ADODB.Recordset, rsMonedas As New ADODB.Recordset
    
    Call Cancelar
    Call Apertura_RecordsetC(RsMaProduccion)
    
    RsMaProduccion.Open "SELECT * FROM MA_ORDEN_PRODUCCION " & _
    "WHERE c_Documento = '" & orden & "' " & _
    "AND c_CodLocalidad = '" & CodLocalidad & "' " & _
    "AND c_Status = '" & FrmAppLink.GetFindStatus & "' ", _
    Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    GridLleno = False
    mMultiplicar = 1
    
    If Not RsMaProduccion.EOF Then
        
        If RsMaProduccion!TipoOrdenProduccion <> 1 Then
            Mensaje True, "El tipo de orden de producci�n es incompatible con el modulo de producci�n avanzada."
            Cancelar
            Exit Sub
        End If
        
        EstatusOPR = FrmAppLink.GetFindStatus
        FormulaOrigenOPR = RsMaProduccion!c_Formula
        CantidadBaseFormula = RsMaProduccion!CantidadBaseFormula
        DocumentoRelacion = "OPR " & RsMaProduccion!c_Documento
        esBackOrder = RsMaProduccion!b_BackOrder
        FechaEstimadaPRD = RsMaProduccion!d_Fecha_Produccion
        If Not isDBNull(RsMaProduccion!d_FechaVencimiento) Then
            FechaVcto = RsMaProduccion!d_FechaVencimiento
            DiasRestantes = DateDiff("d", Date, FechaVcto)
            If EstatusOPR = "DPE" And DiasRestantes >= 0 And DiasRestantes <= 1 Then
                Mensaje True, Replace(StellarMensaje(2956), "$(FV)", SDate(FechaVcto))
            End If
        Else
            FechaVcto = 0
        End If
        Call ListSafeItemSelection(CboLnP, RsMaProduccion!c_LineaProduccion)
        Call ListSafeItemSelection(CboTurno, RsMaProduccion!c_Turno)
        
        'BUSCAR DETALLES Y CARGAR
        
        'A PRODUCIR
        Call Apertura_RecordsetC(RsProducir)
        RsProducir.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
        "LEFT JOIN MA_MONEDAS " & _
        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
        "WHERE c_Documento = '" & orden & "' AND b_Producir = 1 ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsProducir.EOF Then
            
            'For Cont = 1 To Producir.Rows - 2
                
                If RsProducir!n_Activo = 1 Then
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsProducir!c_CodMoneda
                    
                    txtProducir.Text = RsProducir!c_Codigo
                    txtProducir_LostFocus
                    
                    ' Despues de verificar la forma de trabajo
                    
                    CantidadTotalOrden = (RsProducir!n_Cantidad / MultiplicadorCantibul)
                    txtCantProducto.Tag = Round(RsProducir!n_Cantidad - RsProducir!n_Cant_Realizada, 8) / MultiplicadorCantibul
                    txtCantProducto.Text = FormatNumber(txtCantProducto.Tag, mDecimales)
                    txtMulti.Tag = FormatNumber(txtCantProducto.Tag, mDecimales)
                    'mMultiplicar = CantidadBaseFormula
                    'txtMulti = mMultiplicar
                    
                Else
                    'Call Mensaje(True, "El producto '" & RsProducir!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    Exit Sub
                End If
                
            'Next Cont
        
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
        
        txtFormula.Enabled = False
        CmdFormula.Enabled = True
        txtFormula = FormulaOrigenOPR
        txt_descripcion.Text = RsMaProduccion!c_Descripcion
        
        'MONEDA
        Call Apertura_Recordset(rsMonedas)
        rsMonedas.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda = '" & RsMaProduccion!c_CodMoneda & "' ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not rsMonedas.EOF Then
            lbl_moneda.Caption = rsMonedas!c_Descripcion
            dbmoneda.Text = rsMonedas!c_CodMoneda
            Std_Decm = rsMonedas!n_Decimales
            MSK_FACTOR.Text = FormatNumber(rsMonedas!n_Factor, Std_Decm)
            lbl_simbolo.Caption = rsMonedas!c_Simbolo
        Else
            'Call Mensaje(True, "No se encontr� la moneda de la f�rmula.")
            Mensaje True, StellarMensaje(2806)
            Call Cancelar
            Exit Sub
        End If
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open _
        "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
        "WHERE c_Documento = '" & orden & "' AND b_Producir = 0 AND TipoItem = 1", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Dim mCantidadDisponible As Double
        
        With Ingredientes
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 2
                
                For Cont = 1 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(RsTrProduccion!CantidadEnLote, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = RsTrProduccion!PorcVsLote
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(RsTrProduccion!PorcVsLote * 100, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        
                        If FormulaOrigenOPR <> Empty And CantidadBaseFormula <> 0 Then
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = (RsTrProduccion!CantidadBaseFormula / CantidadBaseFormula)
                        Else
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = 0
                        End If
                        
                        mCantidadDisponible = Round(RsTrProduccion!n_Cantidad - RsTrProduccion!n_Cant_Utilizada, RsTrProduccion!Cant_Decimales)
                        If mCantidadDisponible < 0 Then mCantidadDisponible = 0
                        
                        .TextMatrix(Cont, ColPre.CantOrden) = FormatNumber(mCantidadDisponible, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.CantProyectada) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_PorcMerma, 2)
                        
                        .TextMatrix(Cont, ColPre.NetCantOrden) = FormatNumber(.TextMatrix(Cont, ColPre.CantOrden) * (1# + (RsTrProduccion!n_PorcMerma / 100)), RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.NetCantProyectada) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.NetCant) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Exit Sub
                    End If
                Next Cont
                
            Else
                'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                Mensaje True, StellarMensaje(359)
                Call Cancelar
                Exit Sub
            End If
        End With
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open _
        "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
        "WHERE c_Documento = '" & orden & "' AND b_Producir = 0 AND TipoItem = 2", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With Empaques
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 1
                
                For Cont = 0 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = 0
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(0, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        
                        If FormulaOrigenOPR <> Empty And CantidadBaseFormula <> 0 Then
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = (RsTrProduccion!CantidadBaseFormula / CantidadBaseFormula)
                        Else
                            .TextMatrix(Cont, ColPre.CantBaseFormula) = 0
                        End If
                        
                        mCantidadDisponible = Round(RsTrProduccion!n_Cantidad - RsTrProduccion!n_Cant_Utilizada, RsTrProduccion!Cant_Decimales)
                        If mCantidadDisponible < 0 Then mCantidadDisponible = 0
                        
                        .TextMatrix(Cont, ColPre.CantOrden) = FormatNumber(mCantidadDisponible, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.CantProyectada) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(RsTrProduccion!n_Cantidad, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_PorcMerma, 2)
                        
                        .TextMatrix(Cont, ColPre.NetCantOrden) = FormatNumber(.TextMatrix(Cont, ColPre.CantOrden) * (1# + (RsTrProduccion!n_PorcMerma / 100)), RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.NetCantProyectada) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.NetCant) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Exit Sub
                    End If
                Next Cont
                
            Else
                
                'Estos no deben ser obligatorios. Es opcional manejar empaques.
                ''Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                'Mensaje True, StellarMensaje(359)
                'Call Cancelar
                'Exit Sub
                
            End If
            
        End With
        
        CargarCostosDirectos
        
        ' COSTOS DIRECTOS
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM MA_ORDEN_PRODUCCION_COSTOS_DIRECTOS WHERE CodOrden = '" & orden & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With CostosDirectos
            
            I = 0
            
            Do While Not RsTrProduccion.EOF
                
                If FrmAppLink.PRDAvanzadaPrecargarCostosDirectos Then
                    
                    For I = 0 To .Rows - 1
                        If UCase(RsTrProduccion!TipoCosto) = UCase(.TextMatrix(I, ColDir.TipoCosto)) Then
                            .TextMatrix(I, ColDir.ProyectadoUnitarioDecimal) = (RsTrProduccion!Monto / CantidadTotalOrden)
                            .TextMatrix(I, ColDir.ProyectadoUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal)), Std_Decm)
                            .TextMatrix(I, ColDir.RealUnitarioDecimal) = CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal))
                            .TextMatrix(I, ColDir.RealUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.RealUnitarioDecimal)), Std_Decm)
                            Exit For
                        End If
                    Next
                    
                Else
                    
                    I = I + 1
                    .TextMatrix(I, ColDir.TipoCosto) = RsTrProduccion!TipoCosto
                    .TextMatrix(I, ColDir.ProyectadoUnitarioDecimal) = (RsTrProduccion!Monto / CantidadTotalOrden)
                    .TextMatrix(I, ColDir.ProyectadoUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal)), Std_Decm)
                    .TextMatrix(I, ColDir.RealUnitarioDecimal) = CDbl(.TextMatrix(I, ColDir.ProyectadoUnitarioDecimal))
                    .TextMatrix(I, ColDir.RealUnitario) = FormatNumber(CDbl(.TextMatrix(I, ColDir.RealUnitarioDecimal)), Std_Decm)
                    .Rows = .Rows + 1
                    
                End If
                
                RsTrProduccion.MoveNext
                
            Loop
            
        End With
        
        GridLleno = True
        
        RecalcularTransaccion ' Primero calcular las columnas faltantes
        RecalcularTransaccion ' Luego calcular totales
        lblSumario_Click
        
        txtCapacidadMaximaLote = FormatNumber(RsMaProduccion!CapacidadMaxLote, 3)
        txtCapacidadLote.Text = FormatNumber(SumaGrid(Ingredientes, ColPre.CantLote), 2)
        
        If CDbl(txtPesoEmp.Tag) > 0 Then
            txtCantiLote.Tag = Round(CDbl(txtCapacidadLote) / CDbl(txtPesoEmp), 8)
            txtCantiLote.Text = FormatNumber(txtCantiLote.Tag, 0)
        Else
            txtCantiLote.Text = FormatNumber(0, 0)
            txtCantiLote.Tag = 0
        End If
        
        txtCostoHoraHombre = FormatNumber(RsMaProduccion!CostoHoraHombre, Std_Decm)
        txtCostoHoraCargaFabril = FormatNumber(RsMaProduccion!CostoHoraCargaFabril, Std_Decm)
        txtHorasHombreXUnidad = FormatNumber(RsMaProduccion!HorasHombrePorUnidad, Std_Decm)
        
        txtPesoTotalProductos.Text = FormatNumber(RsMaProduccion!PesoTotal, 3)
        txtPesoTotalMezcla.Text = FormatNumber(RsMaProduccion!PesoTotalNeto, 3)
        
        RsTrProduccion.Close
        RsProducir.Close
        rsMonedas.Close
        
        Ingredientes.Row = Ingredientes.Rows - 1
        Ingredientes.Col = ColPre.Producto
        Empaques.Row = Empaques.Rows - 1
        Empaques.Col = ColPre.Producto
        CostosDirectos.Row = CostosDirectos.Rows - 1
        CostosDirectos.Col = ColDir.MontoReal
        MenuAccess True, False
        
        'txtMulti.Visible = False
        'FrameCargarLotes.Visible = False
        'FrameMultiplicarFormula.Visible = False
        
        EstablecerCantidadProducto
        
    Else
        'Call Mensaje(True, "No se encontr� la Formula N� " & Formula)
        Mensaje True, Replace(StellarMensaje(2808), "$(Code)", Formula)
    End If
    
    RsMaProduccion.Close
    
End Sub

Sub MenuAccess(EnblGRD As Boolean, EnblTXT As Boolean)
    txtedit.Visible = EnblTXT
    txtedit.Text = Empty
    txtEdit2.Visible = EnblTXT
    txtEdit2.Text = Empty
    Ingredientes.Enabled = EnblGRD
    Empaques.Enabled = EnblGRD
    CostosDirectos.Enabled = EnblGRD
End Sub

Private Sub txtFormula_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            If txtFormula.Enabled Then
                If Trim(txtFormula.Text) <> Empty Then
                    Buscar_Formula txtFormula.Text
                    
                    Who_Ami = -1
                    
                    DocumentoRelacion = txtFormula.Text
                    ObservacionInicial = StellarMensaje(2948)
                    esBackOrder = False
                    FechaEstimadaPRD = Date
                    FechaVcto = 0
                    Call ListSafeIndexSelection(CboLnP, 0)
                    Call ListSafeIndexSelection(CboTurno, 0)
                        
                Else
                    txt_descripcion.Text = Empty
                End If
                oTeclado.Key_Tab
            End If
        Case vbKeyF2
            Tecla_Pulsada = True
            TmpDisableEvent = True
            Set Campo_Txt = txtFormula
            SafeFocus Campo_Txt
            Set Campo_Lbl = txt_descripcion
            Call MAKE_VIEW("MA_PRODUCCION", "c_Formula", "c_Descripcion", StellarMensaje(2803), Me, "GENERICO", , Campo_Txt, Campo_Lbl)
            TS = Now
            TS1 = DateAdd("s", 1, TS)
            While TS < TS1
                TS = Now
                DoEvents
            Wend
            oTeclado.Key_Return
            Tecla_Pulsada = False
            TmpDisableEvent = False
    End Select
End Sub

Private Sub txtHorasHombreXUnidad_Click()
    mValorAnt = txtHorasHombreXUnidad
    mValorNew = QuickInputRequest("Ingrese el Numero de Horas hombre por unidad.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtHorasHombreXUnidad = FormatNumber(Abs(mValorNew), Std_Decm)
        End If
    End If
End Sub

Private Sub txtHorasParadaNoProgramada_Click()
    mValorAnt = txtHorasParadaNoProgramada
    mValorNew = QuickInputRequest("Ingrese el Numero de Horas que el proceso estuvo detenido bajo circunstancias imprevistas.", True, , mValorAnt, "Escriba el n�mero.", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtHorasParadaNoProgramada = FormatNumber(Abs(mValorNew), 2)
        End If
    End If
End Sub

Private Sub txtHorasParadaProgramada_Click()
    mValorAnt = txtHorasParadaProgramada
    mValorNew = QuickInputRequest("Ingrese el Numero de Horas que el proceso estuvo detenido de manera planificada.", True, , mValorAnt, "Escriba el n�mero.", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtHorasParadaProgramada = FormatNumber(Abs(mValorNew), 2)
        End If
    End If
End Sub

Private Sub txtMotivoParadaNoProgramada_Click()
    mValorAnt = txtMotivoParadaNoProgramada
    mValorNew = QuickInputRequest("Ingrese el motivo por el cual el proceso productivo estuvo detenido (en caso de que aplique, si no hubo interrupciones dejarlo en blanco).", True, , mValorAnt, "Escriba el motivo.", , , , , , , True)
    txtMotivoParadaNoProgramada = mValorNew
End Sub

Private Sub txtMulti_Click()
    mValorAnt = txtMulti
    mValorNew = QuickInputRequest("Ingrese la Cantidad de veces a aplicar la f�rmula.", True, , mValorAnt, "Escriba la Cantidad", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 1
        If CDbl(mValorNew) <> CDbl(mValorAnt) And CDbl(mValorNew) >= 1 Then
            txtMulti = FormatNumber(Abs(mValorNew), 0)
        End If
    End If
End Sub

Private Sub txtNumHorasTrabajadas_Click()
    mValorAnt = txtNumHorasTrabajadas
    mValorNew = QuickInputRequest("Ingrese el N�mero de horas trabajadas en la jornada." _
    , True, , mValorAnt, "Escriba el N�mero", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtNumHorasTrabajadas = FormatNumber(mValorNew, 2)
            CalcularTotalHoras
        End If
    End If
End Sub

Private Sub txtNumTrabajadores_Click()
    mValorAnt = txtNumTrabajadores
    mValorNew = QuickInputRequest("Ingrese el N�mero de Trabajadores involucrados en la producci�n." _
    , True, , mValorAnt, "Escriba el N�mero", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtNumTrabajadores = FormatNumber(mValorNew, 0)
            CalcularTotalHoras
        End If
    End If
End Sub

Private Sub CalcularTotalHoras()
    
    If IsNumeric(txtNumTrabajadores) And IsNumeric(txtNumHorasTrabajadas) Then
        txtTotalHorasTrabajadas.Text = FormatNumber(CDbl(txtNumTrabajadores) * CDbl(txtNumHorasTrabajadas), 2)
    Else
        txtTotalHorasTrabajadas.Text = FormatNumber(0, 0)
    End If
    
    Dim mCambio As Boolean
    
    If FrmAppLink.PRDAvanzadaCostoDirectoManoDeObra <> Empty Then
        For I = 1 To CostosDirectos.Rows - 1
            If UCase(CostosDirectos.TextMatrix(I, ColDir.TipoCosto)) = FrmAppLink.PRDAvanzadaCostoDirectoManoDeObra Then
                CostosDirectos.TextMatrix(I, ColDir.MontoReal) = CDbl(txtTotalHorasTrabajadas.Text) * _
                CDbl(txtCostoHoraHombre)
                CostosDirectos.TextMatrix(I, ColDir.RealUnitarioDecimal) = _
                (CDbl(CostosDirectos.TextMatrix(I, ColDir.MontoReal)) / CDbl(txtCantProdReg))
                CostosDirectos.TextMatrix(I, ColDir.RealUnitario) = FormatNumber(CDbl( _
                CostosDirectos.TextMatrix(I, ColDir.RealUnitarioDecimal)), Std_Decm)
                mCambio = True
                Exit For
            End If
        Next
    End If
    
    If FrmAppLink.PRDAvanzadaCostoDirectoCargaFabril <> Empty Then
        For I = 1 To CostosDirectos.Rows - 1
            If UCase(CostosDirectos.TextMatrix(I, ColDir.TipoCosto)) = FrmAppLink.PRDAvanzadaCostoDirectoCargaFabril Then
                CostosDirectos.TextMatrix(I, ColDir.MontoReal) = CDbl(txtTotalHorasTrabajadas.Text) * _
                CDbl(txtCostoHoraCargaFabril)
                CostosDirectos.TextMatrix(I, ColDir.RealUnitarioDecimal) = _
                (CDbl(CostosDirectos.TextMatrix(I, ColDir.MontoReal)) / CDbl(txtCantProdReg))
                CostosDirectos.TextMatrix(I, ColDir.RealUnitario) = FormatNumber(CDbl( _
                CostosDirectos.TextMatrix(I, ColDir.RealUnitarioDecimal)), Std_Decm)
                mCambio = True
                Exit For
            End If
        Next
    End If
    
    If mCambio Then
        RecalcularTransaccion
    End If
    
End Sub

Private Sub txtPesoPromedioUni_Click()
    mValorAnt = txtPesoPromedioUni
    mValorNew = QuickInputRequest("Ingrese el Peso Promedio por " & NomenclaturaFDT & "." _
    , True, , mValorAnt, "Escriba el Peso", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtPesoPromedioUni = FormatNumber(Abs(mValorNew), 3)
            txtPesoPromedioEmp = FormatNumber(mValorNew * CDbl(txtCantibul), 3)
            RecogerDatosRegistroVSOrden
        End If
    End If
End Sub

Private Sub txtProducir_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtProducir_LostFocus
    ElseIf KeyCode = vbKeyF2 Then
        CmdProducir_Click
    End If
End Sub

Private Sub txtProducir_LostFocus()
    
    Set mRsProducto = New ADODB.Recordset
    mRsProducto.CursorLocation = adUseClient
    
    If txtProducir.Text <> Empty Then
        
        mRsProducto.Open "SELECT P.* FROM MA_PRODUCTOS P INNER JOIN MA_CODIGOS C " & _
        "ON P.c_Codigo = C.c_CodNasa " & _
        "WHERE C.c_Codigo = '" & txtProducir.Text & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not mRsProducto.EOF Then
            txtProducir.Text = mRsProducto!c_Codigo
            If txtProducir.Text <> txtProducir.Tag Then
                txtProducir.Tag = mRsProducto!c_Codigo
                txtDescProducto.Text = mRsProducto!c_Descri
                lblNomenclaturaCant = mRsProducto!c_Presenta
                If FrmAppLink.FichaProd_CamposPesoNeto Then
                    txtPesoUni = FormatNumber(mRsProducto!n_PesoNetoUni, 3)
                Else
                    txtPesoUni = FormatNumber(mRsProducto!n_Peso, 3)
                End If
                txtCantibul = FormatNumber(mRsProducto!n_Cantibul, 0)
                txtPesoEmp.Tag = Round(CDbl(txtPesoUni) * CDbl(txtCantibul), 8)
                txtPesoEmp.Text = FormatNumber(txtPesoEmp.Tag, 3)
                mCostoOrig = mRsProducto.Fields(CostoActivo).Value
                mDecimales = mRsProducto!Cant_Decimales
                If FormaDeTrabajoRes = 1 Then
                    MultiplicadorCantibul = Abs(txtCantibul)
                    txtPesoTotalProductos.Text = FormatNumber(CDbl(txtPesoEmp.Tag) * CDbl(txtMulti), 3)
                Else
                    MultiplicadorCantibul = 1
                    txtPesoTotalProductos.Text = FormatNumber(CDbl(txtPesoUni.Text) * CDbl(txtMulti), 3)
                End If
                CargarGrid
                RecalcularTransaccion
            End If
        Else
            GoTo NotFound
        End If
        
        mRsProducto.ActiveConnection = Nothing
        
    Else
NotFound:
        txtProducir.Text = Empty
        txtProducir.Tag = Empty
        txt_descripcion.Text = Empty
    End If
    
End Sub

Private Sub CboPCD_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntL As Long
    
    mClsGrupos.cTipoGrupo = "PCD"
    
    If PosAntL <> CboPCD.ListIndex Or CboPCD.ListCount = 1 Then
        PosAntL = CboPCD.ListIndex
        If CboPCD.ListIndex = CboPCD.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboPCD
            mClsGrupos.CargarComboGrupos Ent.BDD, CboPCD
            Tecla_Pulsada = True
            ListRemoveItems CboPCD, " ", "Ninguno"
            Tecla_Pulsada = False
        Else
            If Trim(CboPCD.Text) <> Empty Then
                Found = False
                For I = 1 To CostosDirectos.Rows - 1
                    If UCase(CostosDirectos.TextMatrix(I, ColDir.TipoCosto)) = UCase(CboPCD.Text) Then
                        CostosDirectos.Row = I
                        CostosDirectos.Col = ColDir.MontoReal
                        Found = True
                        Exit For
                    End If
                Next
                CboPCD.Visible = False
                If Not Found Then
                    CostosDirectos.Row = CostosDirectos.Rows - 1
                    CostosDirectos.TextMatrix(CostosDirectos.Row, ColDir.TipoCosto) = CboPCD.Text
                    CostosDirectos.TextMatrix(CostosDirectos.Row, ColDir.MontoReal) = FormatNumber(0, Std_Decm)
                    CostosDirectos.Rows = CostosDirectos.Rows + 1
                    RecalcularTransaccion
                End If
            Else
                CboPCD.Visible = False
            End If
        End If
    End If
    
End Sub

Private Sub CboLnP_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntL As Long
    
    mClsGrupos.cTipoGrupo = "LNP"
    
    If PosAntL <> CboLnP.ListIndex Or CboLnP.ListCount = 1 Then
        PosAntL = CboLnP.ListIndex
        If CboLnP.ListIndex = CboLnP.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboLnP
            mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
            Tecla_Pulsada = True
            ListRemoveItems CboLnP, "Ninguno"
            Tecla_Pulsada = False
        End If
    End If
    
End Sub

Private Sub CboTurno_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntT As Long
    
    mClsGrupos.cTipoGrupo = "TNP"
    
    If PosAntT <> CboTurno.ListIndex Or CboTurno.ListCount = 1 Then
        PosAntT = CboTurno.ListIndex
        If CboTurno.ListIndex = CboTurno.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboTurno
            mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
            Tecla_Pulsada = True
            ListRemoveItems CboTurno, "Ninguno"
            Tecla_Pulsada = False
        End If
    End If
    
End Sub

Sub Grabar_Produccion(tipo As String)
    
    Dim RsEureka As New ADODB.Recordset, RsInventarioCargar As New ADODB.Recordset, _
    RsInventarioDescargar As New ADODB.Recordset
    
    Dim RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset, _
    RsTrProduccionCDir As New ADODB.Recordset
    
    Dim RsDepositoCargo As New ADODB.Recordset, RsDepositoDescargo As New ADODB.Recordset, _
    RsMerma As New ADODB.Recordset, ConsecutivoAjuste As String
    
    RecalcularTransaccion
    RecalcularSumario
    RecogerDatosRegistroVSOrden
    
    Dim ActiveTrans As Boolean
    
    'On Error GoTo RecordFail
    'VALIDAR DATOS
    If CDbl(txtCostoTotal) < 0 Then
        'Call Mensaje(True, "El monto de producci�n no puede ser negativo.")
        Mensaje True, StellarMensaje(2809)
        Exit Sub
    ElseIf CDbl(txtCostoTotal) = 0 Then
        'Call Mensaje(True, "No hay datos suficientes para grabar." & vbNewLine & "Verifique que existen productos para la realizar la producci�n.")
        Mensaje True, Replace(StellarMensaje(2810), "$(Line)", vbNewLine)
        Exit Sub
    End If
    
    If Trim(MSGridRecover(Ingredientes, 1, ColPre.Producto)) = Empty Then
        CanRecord = False
        'Call Mensaje(True, "Especifique el detalle de productos a utilizar.")
        Mensaje True, StellarMensaje(2821)
        SafeFocus Ingredientes
        Ingredientes.Col = ColPre.Producto
        Ingredientes.Row = Ingredientes.Rows - 1
        Exit Sub
    End If
    
    If txtProducir.Tag = Empty Then
        CanRecord = False
        'Call Mensaje(True, "Especifique el detalle de productos a producir.")
        Mensaje True, StellarMensaje(2821)
        SafeFocus txtProducir
        Exit Sub
    End If
      
    If Trim(txt_origen.Text) = Empty Then
        'Call Mensaje(True, "Debe colocar el dep�sito Origen, de donde se tomar�n los productos a utilizar.")
        Mensaje True, StellarMensaje(11009)
        lblSumario_Click
        Exit Sub
    End If
    
    If Trim(txt_destino.Text) = Empty Then
        'Call Mensaje(True, "Debe colocar el dep�sito Destino, donde se almacenar�n los productos a producir.")
        Mensaje True, StellarMensaje(16554)
        lblSumario_Click
        Exit Sub
    End If
    
    If FrmAppLink.PRDRequiereLineaDeProduccion Then
        If Trim(CboLnP.Text) = Empty _
        Or UCase(Trim(CboLnP.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2964)
            lblSumario_Click
            SafeFocus CboLnP
            Exit Sub
        End If
    End If
    
    If FrmAppLink.PRDRequiereTurno Then
        If Trim(CboTurno.Text) = Empty _
        Or UCase(Trim(CboTurno.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2965)
            lblSumario_Click
            SafeFocus CboTurno
            Exit Sub
        End If
    End If
    
    Dim Cont As Integer
    
    For Cont = 1 To Ingredientes.Rows - 1
        If Ingredientes.TextMatrix(Cont, ColPre.Producto) = Empty _
        And Ingredientes.TextMatrix(Cont, ColPre.NetCant) <> Empty Then
            Call Mensaje(True, Stellar_Mensaje(16165)) '"El C�digo del Producto no Existe.")
            Exit Sub
        End If
    Next
    
    If txt_origen.Text <> "" Then
        
        Call Apertura_Recordset(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_DEPOSITO " & _
        "WHERE c_CodDeposito = '" & QuitarComillasSimples(txt_origen) & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsEureka.EOF Then
            lbl_origen.Caption = RsEureka!c_Descripcion
            'txtEdit.SetFocus
        Else
            'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
            Mensaje True, StellarMensaje(16189)
            lcDeposito = ""
            lbl_origen.Caption = ""
            txt_origen.Text = ""
            Exit Sub
        End If
        
        Call Cerrar_Recordset(RsEureka)
        
    Else
        'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
        Mensaje True, StellarMensaje(16189)
        lbl_origen.Caption = ""
        Exit Sub
    End If
    
    If txt_destino.Text <> "" Then
        
        Call Apertura_Recordset(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_DEPOSITO " & _
        "WHERE c_CodDeposito = '" & QuitarComillasSimples(txt_destino) & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsEureka.EOF Then
            lbl_destino.Caption = RsEureka!c_Descripcion
        Else
            'Call Mensaje(True, "El c�digo del dep�sito Destino no es Valido.")
            Mensaje True, StellarMensaje(16189)
            lcDeposito = ""
            lbl_destino.Caption = ""
            txt_destino.Text = ""
            Exit Sub
        End If
        
        Call Cerrar_Recordset(RsEureka)
        
    Else
        'Call Mensaje(True, "El c�digo del dep�sito Destino no es Valido.")
        Mensaje True, StellarMensaje(16189)
        lbl_destino.Caption = ""
        Exit Sub
    End If
    
    If Not IsNumeric(txtTotalHorasTrabajadas) Then txtTotalHorasTrabajadas = 0
    If CDbl(txtTotalHorasTrabajadas) < 0 Then
        txtNumHorasTrabajadas = 0
        txtNumTrabajadores = 0
        txtTotalHorasTrabajadas = 0
    End If
    
    If CDbl(txtTotalHorasTrabajadas) = 0 Then
        ActivarMensajeGrande 40
        If Not Mensaje(False, "Las horas trabajadas en la jornada no estan establecidas (0). " & _
        "�Est� seguro de continuar?, Presione Aceptar para omitir el registro o Cancelar " & _
        "para ajustar el valor de los mismos.") Then
            lblDatosRegistroVSOrden_Click
            SafeFocus txtNumTrabajadores
            Exit Sub
        End If
    End If
    
    If Not IsNumeric(txtPesoUni) Then txtPesoUni = 0
    If CDbl(txtPesoUni) < 0 Then txtPesoUni = 0
    If Not IsNumeric(txtPesoPromedioUni) Then txtPesoPromedioUni = 0
    If CDbl(txtPesoPromedioUni) < 0 Then txtPesoPromedioUni = 0
    
    If CDbl(txtPesoUni) > 0 And CDbl(txtPesoPromedioUni) = 0 Then
        ActivarMensajeGrande 40
        If Not Mensaje(False, "No ha establecido el peso promedio del producto y sus datos relacionados. " & _
        "�Est� seguro de continuar?, Presione Aceptar para omitir el registro o Cancelar " & _
        "para ajustar el valor de los mismos.") Then
            lblDatosRegistroVSOrden_Click
            SafeFocus txtPesoUni
            Exit Sub
        End If
    End If
    
    Dim FrmAutorizacion
    
    ' Nueva Validaci�n de Existencia
    
    If FrmAppLink.PRDNivelValidarExistenciaIngredientes <> -1 Then
        
        Dim mMsjExistencia As String, mExistenciaTmp As Double, mGrids, mGrid
        Dim mValidarNivelExistencia As Boolean
        
        mMsjExistencia = Empty
        
        mGrids = Array(Ingredientes, Empaques)
        
        For Each mGrid In AsEnumerable(mGrids)
            
            For Cont = 1 To mGrid.Rows - 1
                
                mCantTmp = Val(mGrid.TextMatrix(Cont, ColPre.Canti))
                
                If Trim(mGrid.TextMatrix(Cont, ColPre.Producto)) <> Empty And mCantTmp > 0 Then
                    
                    mPorcMerma = IIf(FrmAppLink.PRDAjusteAutomaticoPorMerma, CDbl(mGrid.TextMatrix(Cont, ColPre.PorcMerma)), 0)
                    mCantTmp = CDbl(mGrid.TextMatrix(Cont, ColPre.Canti)) * (1 + (mPorcMerma / 100))
                    
                    mExistenciaTmp = BuscarValorBD("Existencia", _
                    "SELECT SUM(n_Cantidad - CASE WHEN n_Cant_Comprometida >= 0 " & _
                    "THEN n_Cant_Comprometida ELSE 0 END) AS Existencia " & _
                    "FROM MA_DEPOPROD " & _
                    "WHERE c_CodArticulo = '" & mGrid.TextMatrix(Cont, ColPre.Producto) & "' " & _
                    "AND c_CodDeposito = '" & QuitarComillasSimples(txt_origen.Text) & "' " & _
                    "GROUP BY c_CodArticulo ", 0)
                    
                    If mCantTmp > mExistenciaTmp Then
                        'mMsjExistencia = mMsjExistencia & vbNewLine & _
                        "[" & mGrid.TextMatrix(Cont, ColPre.Producto) & "]  " & _
                        "[" & mGrid.TextMatrix(Cont, ColPre.Descripci�n) & "]   " & _
                        "" & _
                        "Utilizada: (" & mCantTmp & ")  Existencia: (" & FormatNumber(mExistenciaTmp, mGrid.TextMatrix(Cont, ColPre.LDeci)) & ")"
                        mMsjExistencia = mMsjExistencia & vbNewLine & Replace(Replace(Replace(Replace(Replace( _
                        StellarMensaje(2983), "$(Cod)", "[" & mGrid.TextMatrix(Cont, ColPre.Producto) & "]  "), _
                        "$(Desc)", "[" & mGrid.TextMatrix(Cont, ColPre.Descripci�n) & "]   "), _
                        "$(Cant)", "(" & mCantTmp & ")"), _
                        "$(Exis)", "(" & FormatNumber(mExistenciaTmp, mGrid.TextMatrix(Cont, ColPre.LDeci)) & ")"), _
                        "$(Tab)", "  ")
                    End If
                    
                End If
                
            Next
            
        Next
        
        If mMsjExistencia <> Empty Then
            
            'mMsjExistencia = "Los siguientes productos no poseen suficiente existencia en el dep�sito origen para efectuar la producci�n: " & _
            vbNewLine & mMsjExistencia
            mMsjExistencia = Replace(StellarMensaje(2984), "$(Ln)", vbNewLine) & mMsjExistencia
            
            If FrmAppLink.PRDNivelValidarExistenciaIngredientes > 0 Then
                mValidarNivelExistencia = True
            End If
            
            If mValidarNivelExistencia Then
                
                mValidarNivelExistencia = (FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelValidarExistenciaIngredientes)
                
                If mValidarNivelExistencia Then
                    
                    'mMsjExistencia = mMsjExistencia & vbNewLine & vbNewLine & _
                    "Actualmente no posee los permisos para realizar esta acci�n. " & _
                    "Presione Aceptar si desea solicitar autorizaci�n para continuar. " & _
                    "En caso contrario presione Cancelar. "
                    mMsjExistencia = mMsjExistencia & vbNewLine & vbNewLine & _
                    StellarMensaje(2985)
                    
                    ActivarMensajeGrande 100
                    
                    If Not Mensaje(False, mMsjExistencia) Then
                        
                        Exit Sub
                        
                    Else
                        
                        Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
                        
                        FrmAutorizacion.mNivel = FrmAppLink.PRDNivelValidarExistenciaIngredientes
                        FrmAutorizacion.Titulo = StellarMensaje(298) ' "Indique el usuario..."
                        
                        Set FrmAutorizacion.mConexion = Ent.BDD
                        
                        FrmAutorizacion.Show vbModal
                        
                        If FrmAutorizacion.mAceptada Then
                            
                            Autorizante = FrmAutorizacion.mUsuario
                            Set FrmAutorizacion = Nothing
                            ' Continuar
                            
                        Else
                            
                            Set FrmAutorizacion = Nothing
                            Mensaje True, StellarMensaje(16050)
                            Exit Sub
                            
                        End If
                        
                    End If
                    
                End If
                
            Else
                
                ActivarMensajeGrande 100
                Mensaje True, mMsjExistencia
                Exit Sub
                
            End If
            
        End If
        
    End If
    
    '
    
    If Not (Left(DocumentoRelacion, 4) = "OPR " _
    And Find_Concept = "PRD" _
    And EstatusOPR = "DPE") _
    Then
        
        'PRDNivelProduccionSinOPR = 11 ' Debug
        
        If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelProduccionSinOPR Then
            
            Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
            
            'PRDNivelProduccionSinOPR = 6 ' Debug
            
            FrmAutorizacion.mNivel = FrmAppLink.PRDNivelProduccionSinOPR
            FrmAutorizacion.Titulo = StellarMensaje(2968) ' "Introduzca las credenciales " & _
            "de un usuario que autorice la producci�n " & _
            "sin orden de producci�n asociada."
            
            Set FrmAutorizacion.mConexion = Ent.BDD
            
            FrmAutorizacion.Show vbModal
            
            If FrmAutorizacion.mAceptada Then
                
                Autorizante = FrmAutorizacion.mUsuario
                Set FrmAutorizacion = Nothing
                ' Continuar
                
            Else
                
                Set FrmAutorizacion = Nothing
                Mensaje True, StellarMensaje(16050)
                Exit Sub
                
            End If
            
        End If
        
    End If
    
    If tipo = "GORD" Then
        
        If FrmAppLink.PRDCambioPrecioAutomatico = 0 _
        Or FrmAppLink.PRDCambioPrecioAutomatico = 2 Then
            
            'For Cont = 1 To Producir.Rows - 1
                
                Dim mCostoNew As Double, _
                mVerPrecios As Boolean, Visualizar As Integer
                
                mCostoNew = CDbl(txtCostoUni) 'Round((( _
                CDbl(txtCostoTotal) / _
                CDbl(txtCantProducto)) _
                ), Std_Decm)
                
                If mCostoOrig <> mCostoNew Then
                    
                    Visualizar = 0
                    
                    '"Existe una variaci�n en el costo del Producto [$(Code)][$(Desc)]" & _
                    "�Desea validar los precios de venta?"
                    Mensaje False, Replace(Replace(StellarMensaje(2939), "$(Code)", txtProducir.Text), _
                    "$(Desc)", txtDescProducto.Text)
                    
                    If FrmAppLink.RetornoMensaje Then
                        Visualizar = 1
                    Else
                        Visualizar = -1
                    End If
                    
                    If Visualizar = 1 Then
                        
                        Dim Newfrm_Orden_Reposicion: Set Newfrm_Orden_Reposicion = FrmAppLink.GetNewFrmOrdenReposicion
                        
                        FrmAppLink.MonedaDoc.BuscarMonedas , dbmoneda.Text
                        Newfrm_Orden_Reposicion.ReadOnly = True
                        Newfrm_Orden_Reposicion.CampoCostoComparar = CostoActivo
                        Newfrm_Orden_Reposicion.Codigo = txtProducir
                        
                        FrmAppLink.SetUnitario = mCostoNew
                        
                        If FrmAppLink.PRDCambioPrecioAutomatico = 2 Then
                            Newfrm_Orden_Reposicion.SoloAceptar = True
                        End If
                        
                        Newfrm_Orden_Reposicion.Reload
                        Newfrm_Orden_Reposicion.Show vbModal
                        
                        If FrmAppLink.PRDCambioPrecioAutomatico = 0 Then
                            If Not FrmAppLink.GetIngresar Then Exit Sub
                        End If
                        
                    End If
                    
                End If
                
            'Next
            
        End If
        
        Dim Frm_Totalizar: Set Frm_Totalizar = FrmAppLink.GetFrmTotalizar
        
        Frm_Totalizar.lbl_impuesto.Caption = FormatNumber(0)
        Frm_Totalizar.lbl_subtotal.Caption = CDbl(GrdTotales.TextMatrix(1, ColTot.MontoReal))
        Frm_Totalizar.msk_bsdesc.Text = FormatNumber(0)
        Frm_Totalizar.lbl_total.Caption = CDbl(txtCostoTotal)
        Frm_Totalizar.txtImpuesto = FormatNumber(0)
        
        Frm_Totalizar.ManejaObservacion = True
        Frm_Totalizar.Obsevacion_SoloLectura = False
        Frm_Totalizar.ObservacionInicial = ObservacionInicial
        Frm_Totalizar.ManejaOtrosCargos = True
        Frm_Totalizar.RecalcularOtrosCargos = False
        Frm_Totalizar.MostrarPorcentajeOtrosCargos = False
        Frm_Totalizar.txtOtrosCargos.Text = FormatNumber(CDbl(txtCostosDir.Text), Std_Decm)
        Frm_Totalizar.Show vbModal
        
        If FrmAppLink.GetLcGrabar = False Then
            Set Frm_Totalizar = Nothing
            Exit Sub
        End If
        
        Dim mFechaProduccion As Date
        mFechaProduccion = Frm_Totalizar.fecha_recepcion
        
        If Frm_Totalizar.chk_vencimiento = vbChecked Then
            mFechaVcto = CDbl(Frm_Totalizar.dtp_odc_vence)
        Else
            mFechaVcto = 0
        End If
        
        ObservacionInicial = Frm_Totalizar.txtObs.Text
        
        Set Frm_Totalizar = Nothing
        
    End If
    
    Ent.BDD.BeginTrans: ActiveTrans = True
    
    ConsecutivoAjuste = ""
    
    Dim RowIsClean As Boolean
    
    For I = 1 To Ingredientes.Rows - 1
        RowIsClean = True
        For K = 0 To Ingredientes.Cols - 1
            If Ingredientes.TextMatrix(I, K) <> "" Then
                RowIsClean = False
                Exit For
            End If
        Next K
        If (RowIsClean And I > 1) Then Ingredientes.RemoveItem I
    Next I
    
    For I = 0 To Empaques.Rows - 1
        RowIsClean = True
        For K = 0 To Empaques.Cols - 1
            If Empaques.TextMatrix(I, K) <> "" Then
                RowIsClean = False
                Exit For
            End If
        Next K
        If (RowIsClean And I > 1) Then Empaques.RemoveItem I
    Next I
    
    If tipo = "GORD" Then
        
        'GRABAR ORDEN
        
        lbl_consecutivo.Caption = Format(No_Consecutivo("PRODUCCION_ORDEN"), "000000000")
        
        TotalMerma = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.CostoPreXMerma)) + _
        CDbl(TotalesEmpaques.TextMatrix(0, ColPre.CostoPreXMerma))
        
        If (TotalMerma > 0) Then
            ConsecutivoAjuste = Format(No_Consecutivo("AJUSTES"), "000000000")
        End If
        
        Call Apertura_RecordsetC(RsEureka)
        RsEureka.Open "SELECT * FROM MA_INVENTARIO " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Concepto = 'PRD' AND c_Status = 'DCO' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsEureka.EOF Then
            
            RsEureka.AddNew
            
            'ACTUALIZAR EL CABECERO DE INVENTARIO CON LOS DATOS DE LA FORMA
            Call UpdateHeader(RsEureka, "DCO", mFechaProduccion, ConsecutivoAjuste)
            
            Call Apertura_RecordsetC(RsMaProduccion)
            RsMaProduccion.Open "SELECT * FROM MA_PRODUCCION_REGISTRO " & _
            "WHERE 1 = 2", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            Call UpdateHeader_RegistroAvanzado(RsMaProduccion, "DCO", mFechaProduccion, ConsecutivoAjuste)
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A UTILIZAR (DESCARGO)
            Call Apertura_RecordsetC(RsInventarioDescargar)
            
            RsInventarioDescargar.Open "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND c_Concepto = 'PRD' AND c_TipoMov = 'Descargo'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioDescargar.EOF Then
                
                Call UpdateUtilizar(RsInventarioDescargar, "Descargo", tipo, mFechaProduccion)
                
                Call Apertura_RecordsetC(RsTrProduccion)
                
                RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION_REGISTRO " & _
                "WHERE 1 = 2", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                Call UpdateUtilizar_RegistroAvanzado(RsTrProduccion, "Descargo", tipo)
                
            Else
                Call Mensaje(True, "Existe un detalle de Descargo con el documento N� '" & lbl_consecutivo.Caption & "' ")
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
            'ACTUALIZAR EL DEPOSITO CON LOS PRODUCTOS A UTILIZAR
            Call UpDateDepositoUtilizar(RsDepositoDescargo)
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A PRODUCIR (CARGO)
            Call Apertura_RecordsetC(RsInventarioCargar)
            
            RsInventarioCargar.Open "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND c_Concepto = 'PRD' AND c_TipoMov = 'Cargo'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioCargar.EOF Then
                
                'Call Apertura_RecordsetC(RsTrProduccion)
                
                'RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION_REGISTRO " & _
                "WHERE 1 = 2", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                Call UpdateProducir(RsInventarioCargar, RsTrProduccion, "Cargo", mFechaProduccion, tipo)
                
            Else
                Call Mensaje(True, "Existe un detalle de Cargo con el documento N� '" & lbl_consecutivo.Caption & "' ")
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
            'ACTUALIZAR EL DEPOSITO CON LOS PRODUCTOS A PRODUCIR
            Call UpDateDepositoProducir(RsDepositoCargo)
            
            'ACTUALIZAR MERMA
            
            If FrmAppLink.PRDAjusteAutomaticoPorMerma And (TotalMerma > 0) Then
                If Not UpdateMerma("DCO", ConsecutivoAjuste, RsEureka!c_Documento) Then
                    Call Mensaje(True, "No se pudo procesar un ajuste por merma.")
                    Ent.BDD.RollbackTrans: ActiveTrans = False
                    Exit Sub
                End If
            End If
            
            If Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
                
                If esBackOrder Then
                    
                    ' Cerrar Documento cuando se haya producido la cantidad total de cada producto de la orden.
                    
                    TmpValue = ";WITH Cantidades_OPR AS ( " & vbNewLine & _
                    "SELECT c_Documento AS Documento, ID AS RowID, CASE WHEN MAX(n_Decimales) > 3 THEN MAX(n_Decimales) ELSE 3 END AS Decimales, SUM(CASE WHEN n_Cant_Realizada > n_Cantidad THEN n_Cantidad ELSE n_Cant_Realizada END) AS CantDPE, " & vbNewLine & _
                    "SUM(n_Cantidad) As CantDCO " & vbNewLine & _
                    "FROM TR_ORDEN_PRODUCCION " & vbNewLine & _
                    "WHERE b_Producir = 1 " & vbNewLine & _
                    "AND c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & vbNewLine & _
                    "GROUP BY c_Documento, ID " & vbNewLine & _
                    "), TMP_CANT AS ( " & vbNewLine & _
                    "SELECT ROUND(TBL.CantDPE - TBL.CantDCO, TBL.Decimales) AS CantRestante, ((-1.0) * (1.0 / POWER(10, TBL.Decimales))) AS DiferenciaMinimaParaCerrarDocumento, * FROM Cantidades_OPR TBL " & vbNewLine & _
                    ") UPDATE MA_ORDEN_PRODUCCION " & vbNewLine & _
                    "SET c_Status = 'DCO' " & vbNewLine & _
                    "FROM MA_ORDEN_PRODUCCION OPR INNER JOIN TMP_CANT " & vbNewLine & _
                    "ON OPR.c_Documento = TMP_CANT.Documento " & vbNewLine & _
                    "WHERE OPR.c_Documento = TMP_CANT.Documento " & vbNewLine & _
                    "AND TMP_CANT.CantRestante >= TMP_CANT.DiferenciaMinimaParaCerrarDocumento; " & vbNewLine
                    
                Else
                    
                    ' Cerrar Orden independientemente de las cantidades utilizadas / realizadas.
                    
                    TmpValue = "UPDATE MA_ORDEN_PRODUCCION " & vbNewLine & _
                    "SET c_Status = 'DCO' " & vbNewLine & _
                    "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & vbNewLine
                    
                End If
                
                Ent.BDD.Execute TmpValue
                
            End If
            
            Call Apertura_RecordsetC(RsTrProduccionCDir)
            Ent.BDD.Execute "DELETE FROM MA_PRODUCCION_REGISTRO_COSTOS_DIRECTOS WHERE CodRegistro = '" & lbl_consecutivo & "' AND CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' "
            RsTrProduccionCDir.Open "SELECT * FROM MA_PRODUCCION_REGISTRO_COSTOS_DIRECTOS WHERE 1 = 2 ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            With CostosDirectos
                For Cont = 1 To .Rows - 1
                    If Trim(.TextMatrix(Cont, ColDir.TipoCosto)) <> Empty Then
                        If IsNumeric(.TextMatrix(Cont, ColDir.MontoReal)) Then
                            If CDbl(.TextMatrix(Cont, ColDir.MontoReal)) > 0 Then
                                RsTrProduccionCDir.AddNew
                                RsTrProduccionCDir!CodRegistro = lbl_consecutivo
                                RsTrProduccionCDir!CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                                RsTrProduccionCDir!IDGrupo = 0
                                RsTrProduccionCDir!TipoCosto = .TextMatrix(Cont, ColDir.TipoCosto)
                                RsTrProduccionCDir!MontoProyectado = CDbl(.TextMatrix(Cont, ColDir.MontoProyectado))
                                RsTrProduccionCDir!MontoReal = CDbl(.TextMatrix(Cont, ColDir.MontoReal))
                                RsTrProduccionCDir!CodMoneda = dbmoneda.Text
                                RsTrProduccionCDir!nFactorTemp = CDbl(MSK_FACTOR.Text)
                                RsTrProduccionCDir.UpdateBatch
                            End If
                        End If
                    End If
                Next Cont
            End With
            
        Else
            Call Mensaje(True, "La orden N� '" & lbl_consecutivo.Caption & "' existe en el Sistema.")
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
    End If
    
    Ent.BDD.CommitTrans: ActiveTrans = False
    
    LcConsecu = lbl_consecutivo.Caption
    Find_Concept = "PRD"
    
    MostrarDocumentoStellar Find_Concept, Find_Status, LcConsecu, gCls
    
    Call Nuevo_Consecutivo
    Call Cancelar
    
    Exit Sub
    
RecordFail:
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans: ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Sub UpdateHeader(ByRef RsTemp As ADODB.Recordset, Status As String, _
ByVal pFechaProduccion As Date, Optional Ajuste As String = "")
    With RsTemp
        !c_Documento = lbl_consecutivo.Caption
        !d_Fecha = FechaBD(pFechaProduccion, , True)
        !c_Descripcion = txt_descripcion.Text
        !c_Status = Status
        Find_Status = Status
        !c_Concepto = "PRD"
        !c_CodProveedOR = Ajuste
        !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
        !c_CodMoneda = dbmoneda.Text
        !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
        !n_Descuento = 0
        !c_Observacion = ObservacionInicial '"EJECUCION DE FORMULA DE PRODUCCI�N"
        If Left(DocumentoRelacion, 4) = "OPR " Then
            !c_Relacion = DocumentoRelacion
        Else
            !c_Relacion = txtFormula.Text
        End If
        !c_CodComprador = FrmAppLink.GetCodUsuario
        !c_Dep_Orig = txt_origen.Text
        !c_Dep_Dest = txt_destino.Text
        !c_Motivo = "PRODUCCION"
        !c_CodTransporte = ""
        !c_Ejecutor = FrmAppLink.GetNomUsuario
        If FrmAppLink.InventarioCampoLineaProduccion Then
            !c_LineaProduccion = CboLnP.Text
        End If
        If FrmAppLink.InventarioCampoTurnoProduccion Then
            !c_Turno = CboTurno.Text
        End If
        !c_Factura = ""
        !c_Transportista = ""
        !n_Cantidad_compra = CDbl(txtCostosDir.Text)
        If FrmAppLink.PRDManejaConceptosContabilidad And Status <> "DWT" Then
            TmpCon = PedirConceptoUnidadProceso("47")
            If TmpCon(0) <> -1 Then
                !CodConcepto = TmpCon(0)
                If ExisteCampoTabla("cs_CodUnidad", RsTemp) Then
                    !cs_CodUnidad = TmpCon(1)
                End If
            Else
                !CodConcepto = 65
            End If
        End If
    End With
    RsTemp.UpdateBatch
End Sub

Sub UpdateHeader_RegistroAvanzado(ByRef RsTemp As ADODB.Recordset, Status As String, _
ByVal pFechaProduccion As Date, Optional Ajuste As String = "")
    With RsTemp
        .AddNew
        
        !CodRegistro = lbl_consecutivo.Caption
        !CodLocalidad = FrmAppLink.GetCodLocalidadSistema
        !CodFormula = txtFormula
        !CodOrdenProduccion = Mid(DocumentoRelacion, 5)
        
        !FechaProduccion = FechaBD(pFechaProduccion, , True)
        !CostoProductos = CDbl(GrdTotales.TextMatrix(1, ColTot.MontoReal))
        !CostoMerma = TotalMerma
        !CostosDirectos = CDbl(txtCostosDir)
        !CostoTotal = CDbl(txtCostoTotal)
        !CostoTotalProyectado = CDbl(GrdTotales.TextMatrix(3, ColTot.MontoProyectado))
        
        !LineaProduccion = CboLnP.Text
        !TurnoProduccion = CboTurno.Text
        
        !PesoUnitarioFormula = CDbl(txtPesoUni)
        !CantibulFormula = CDbl(txtCantibul)
        !PesoEmpaqueFormula = CDbl(txtPesoEmp)
        !CapacidadMaxLote = CDbl(txtCapacidadMaximaLote)
        !CapacidadLote = CDbl(txtCapacidadLote)
        !UnidadesPorLote = CDbl(txtCantiLote) * MultiplicadorCantibul
        !CostoHoraHombre = CDbl(txtCostoHoraHombre)
        !CostoHoraCargaFabril = CDbl(txtCostoHoraCargaFabril)
        !HorasHombrePorUnidad = CDbl(txtHorasHombreXUnidad) / MultiplicadorCantibul
        !PesoUnitarioNetoFormula = CDbl(txtPesoMezclaEstandar)
        !PesoPromedioUnidad = CDbl(txtPesoPromedioUni)
        !PesoPromedioEmpaque = CDbl(txtPesoPromedioEmp)
        !CantidadProduccion = CDbl(txtCantProducto.Text) * MultiplicadorCantibul
        !NumeroDeLotes = CDbl(txtLotesRegistro)
        !PesoTotalUtilizado = CDbl(txtPesoMezclaTotalRepReg)
        !PesoTotalTeorico = CDbl(txtPesoTotalProductoRepReg)
        !VariacionPesoTotal = CDbl(txtDiferenciaPesosTotalesReg)
        !PorcVariacionPesoTotal = CDbl(Replace(txtPorcVariacionPesosTotalesReg, "%", Empty))
        !NumeroTrabajadores = CDbl(txtNumTrabajadores)
        !NumeroHorasTrabajadas = CDbl(txtNumHorasTrabajadas)
        !TotalHorasTrabajadas = CDbl(txtTotalHorasTrabajadas)
        !CostoUnitarioFormula = CDbl(txtCostoUniProyectado)
        !CostoUnitarioTeorico = CDbl(txtCostoUni)
        !CantidadBaseFormula = CantidadBaseFormula
        !TiempoParadaProgramada = CDbl(txtHorasParadaProgramada)
        !TiempoParadaImprevista = CDbl(txtHorasParadaNoProgramada)
        !MotivoParadaProduccion = txtMotivoParadaNoProgramada
    End With
    RsTemp.UpdateBatch
End Sub

Sub UpdateUtilizar(ByRef RsTemp As ADODB.Recordset, TipoMov As String, _
tipo As String, ByVal pFechaProduccion As Date)
    
    Dim Cont As Integer, NewCont As Long, TmpValue
    
    For Cont = 1 To Ingredientes.Rows - 1
        
        mCant = Ingredientes.TextMatrix(Cont, ColPre.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Linea = NewCont
            !c_Concepto = "PRD"
            !c_Documento = lbl_consecutivo.Caption
            !c_Deposito = txt_origen.Text
            !c_CodArticulo = Ingredientes.TextMatrix(Cont, ColPre.Producto)
            !n_Cantidad = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.Canti)), _
            CLng(Ingredientes.TextMatrix(Cont, ColPre.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.CostoPre)), Std_Decm))
            !n_Subtotal = CDbl(FormatNumber(!n_Costo * !n_Cantidad, Std_Decm))
            !n_Impuesto = 0
            !n_Total = !n_Subtotal
            !c_TipoMov = TipoMov
            !n_Cant_Teorica = 0
            !n_Cant_Diferencia = 0
            !n_Precio = !n_Costo
            !n_Precio_Original = !n_Costo
            !f_Fecha = FechaBD(pFechaProduccion, , True) 'Date
            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
            
            If ExisteCampoTabla("n_FactorMonedaProd", RsTemp) Then 'CMM1N
                '!n_FactorMonedaProd = mFactorLn
                !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            End If
            
            !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !ns_CantidadEmpaque = CDbl(Ingredientes.TextMatrix(Cont, ColPre.nCantibul))
            
            ''' Nuevo. Lote. Si algun dia hay que agregar algo mas en este campo, lo separamos por ,
            ''!c_Descripcion = StellarMensaje(2937) & " |" & Preparado.TextMatrix(Cont, ColumnaPreparado.Lote) & "|"
            'If FrmAppLink.InventarioCampoIDLote Then
                '!c_IDLote = Ingredientes.TextMatrix(Cont, ColPre.Lote)
            'End If
            
        End With
        
        RsTemp.UpdateBatch
        
        If (tipo = "GORD") And RsTemp!n_Cantidad > 0 And Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
            Ent.BDD.Execute "UPDATE TR_ORDEN_PRODUCCION " & _
            "SET n_Cant_Utilizada = ROUND(n_Cant_Utilizada + (" & RsTemp!n_Cantidad & "), 8) " & _
            "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & _
            "AND c_CodArticulo = '" & RsTemp!c_CodArticulo & "' " & _
            "AND b_Producir = 0 ", TmpValue
        End If
        
        End If
        End If
        
    Next Cont
    
    For Cont = 0 To Empaques.Rows - 1
        
        mCant = Empaques.TextMatrix(Cont, ColPre.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Linea = NewCont
            !c_Concepto = "PRD"
            !c_Documento = lbl_consecutivo.Caption
            !c_Deposito = txt_origen.Text
            !c_CodArticulo = Empaques.TextMatrix(Cont, ColPre.Producto)
            !n_Cantidad = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.Canti)), _
            CLng(Empaques.TextMatrix(Cont, ColPre.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.CostoPre)), Std_Decm))
            !n_Subtotal = CDbl(FormatNumber(!n_Costo * !n_Cantidad, Std_Decm))
            !n_Impuesto = 0
            !n_Total = !n_Subtotal
            !c_TipoMov = TipoMov
            !n_Cant_Teorica = 0
            !n_Cant_Diferencia = 0
            !n_Precio = !n_Costo
            !n_Precio_Original = !n_Costo
            !f_Fecha = Date
            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
            
            If ExisteCampoTabla("n_FactorMonedaProd", RsTemp) Then 'CMM1N
                '!n_FactorMonedaProd = mFactorLn
                !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            End If
            
            !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !ns_CantidadEmpaque = CDbl(Empaques.TextMatrix(Cont, ColPre.nCantibul))
            
            ''' Nuevo. Lote. Si algun dia hay que agregar algo mas en este campo, lo separamos por ,
            ''!c_Descripcion = StellarMensaje(2937) & " |" & Preparado.TextMatrix(Cont, ColumnaPreparado.Lote) & "|"
            'If FrmAppLink.InventarioCampoIDLote Then
                '!c_IDLote = Ingredientes.TextMatrix(Cont, ColPre.Lote)
            'End If
            
        End With
        
        RsTemp.UpdateBatch
        
        If (tipo = "GORD") And RsTemp!n_Cantidad > 0 And Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
            Ent.BDD.Execute "UPDATE TR_ORDEN_PRODUCCION " & _
            "SET n_Cant_Utilizada = ROUND(n_Cant_Utilizada + (" & RsTemp!n_Cantidad & "), 8) " & _
            "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & _
            "AND c_CodArticulo = '" & RsTemp!c_CodArticulo & "' " & _
            "AND b_Producir = 0 ", TmpValue
        End If
        
        End If
        End If
        
    Next Cont
    
End Sub

Sub UpdateUtilizar_RegistroAvanzado(ByRef RsTemp As ADODB.Recordset, TipoMov As String, tipo As String)
    
    Dim Cont As Integer, NewCont As Long, TmpValue
    
    For Cont = 1 To Ingredientes.Rows - 1
        
        mCant = Ingredientes.TextMatrix(Cont, ColPre.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Documento = lbl_consecutivo.Caption
            !c_Linea = NewCont
            !c_CodigoInput = Ingredientes.TextMatrix(Cont, ColPre.Producto)
            !c_CodArticulo = !c_CodigoInput
            !c_Descripcion = Ingredientes.TextMatrix(Cont, ColPre.Descripci�n)
            !c_Presenta = Ingredientes.TextMatrix(Cont, ColPre.Presentaci�n)
            
            !n_Cantidad = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.Canti)), _
            CLng(Ingredientes.TextMatrix(Cont, ColPre.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.CostoPre)), Std_Decm))
            
            !n_PorcMerma = CDbl(Ingredientes.TextMatrix(Cont, ColPre.PorcMerma))
            !n_CantidadNeta = CDbl(Ingredientes.TextMatrix(Cont, ColPre.NetCant))
            !n_MontoMerma = CDbl(Ingredientes.TextMatrix(Cont, ColPre.CostoPreXMerma))
            !n_Subtotal = CDbl(FormatNumber(!n_Costo * !n_Cantidad, Std_Decm))
            !n_Descuento = 0
            !n_Impuesto = 0
            !n_Total = !n_Subtotal
            !n_CantidadProyectada = CDbl(Ingredientes.TextMatrix(Cont, ColPre.NetCantProyectada))
            
            !b_Producir = 0
            !c_IDLote = vbNullString
            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            !ns_CantidadEmpaque = CDbl(Ingredientes.TextMatrix(Cont, ColPre.nCantibul))
            !nu_FactorCosto = 1
            
            !TipoItem = 1
            If Not IsNumeric(Ingredientes.TextMatrix(Cont, ColPre.CantLote)) Then Ingredientes.TextMatrix(Cont, ColPre.CantLote) = 0
            !CantidadEnLote = CDbl(Ingredientes.TextMatrix(Cont, ColPre.CantLote))
            If Not IsNumeric(Ingredientes.TextMatrix(Cont, ColPre.PorcLoteReal)) Then Ingredientes.TextMatrix(Cont, ColPre.PorcLoteReal) = 0
            !PorcVsLote = CDbl(Ingredientes.TextMatrix(Cont, ColPre.PorcLoteReal))
            !TotalCantidadPorTipoItem = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.Canti))
            !TotalCantidadNetaPorTipoItem = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.NetCant))
            !TotalCostoPorTipoItem = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.CostoProdNeto))
            !TotalCostoEnItems = CDbl(GrdTotales.TextMatrix(1, ColTot.MontoReal))
            If Not IsNumeric(Ingredientes.TextMatrix(Cont, ColPre.CantBaseFormula)) Then Ingredientes.TextMatrix(Cont, ColPre.CantBaseFormula) = 0
            !CantidadBaseFormula = CDbl(Ingredientes.TextMatrix(Cont, ColPre.CantBaseFormula))
            
        End With
        
        RsTemp.UpdateBatch
        
        If (tipo = "GORD") And RsTemp!n_Cantidad > 0 And Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
            Ent.BDD.Execute "UPDATE TR_ORDEN_PRODUCCION " & _
            "SET n_Cant_Utilizada = ROUND(n_Cant_Utilizada + (" & RsTemp!n_Cantidad & "), 8) " & _
            "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & _
            "AND c_CodArticulo = '" & RsTemp!c_CodArticulo & "' " & _
            "AND b_Producir = 0 ", TmpValue
        End If
        
        End If
        End If
        
    Next Cont
    
    For Cont = 0 To Empaques.Rows - 1
        
        mCant = Empaques.TextMatrix(Cont, ColPre.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Documento = lbl_consecutivo.Caption
            !c_Linea = NewCont
            !c_CodigoInput = Empaques.TextMatrix(Cont, ColPre.Producto)
            !c_CodArticulo = !c_CodigoInput
            !c_Descripcion = Empaques.TextMatrix(Cont, ColPre.Descripci�n)
            !c_Presenta = Empaques.TextMatrix(Cont, ColPre.Presentaci�n)
            
            !n_Cantidad = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.Canti)), _
            CLng(Empaques.TextMatrix(Cont, ColPre.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.CostoPre)), Std_Decm))
            
            !n_PorcMerma = CDbl(Empaques.TextMatrix(Cont, ColPre.PorcMerma))
            !n_CantidadNeta = CDbl(Empaques.TextMatrix(Cont, ColPre.NetCant))
            !n_MontoMerma = CDbl(Empaques.TextMatrix(Cont, ColPre.CostoPreXMerma))
            !n_Subtotal = CDbl(FormatNumber(!n_Costo * !n_Cantidad, Std_Decm))
            !n_Descuento = 0
            !n_Impuesto = 0
            !n_Total = !n_Subtotal
            !n_CantidadProyectada = CDbl(Empaques.TextMatrix(Cont, ColPre.NetCantProyectada))
            
            !b_Producir = 0
            !c_IDLote = vbNullString
            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            !ns_CantidadEmpaque = CDbl(Empaques.TextMatrix(Cont, ColPre.nCantibul))
            !nu_FactorCosto = 1
            
            !TipoItem = 2
            If Not IsNumeric(Empaques.TextMatrix(Cont, ColPre.CantLote)) Then Empaques.TextMatrix(Cont, ColPre.CantLote) = 0
            !CantidadEnLote = CDbl(Empaques.TextMatrix(Cont, ColPre.CantLote))
            !PorcVsLote = 0
            !TotalCantidadPorTipoItem = 0
            !TotalCantidadNetaPorTipoItem = 0
            !TotalCostoPorTipoItem = CDbl(TotalesEmpaques.TextMatrix(0, ColPre.CostoProdNeto))
            !TotalCostoEnItems = CDbl(GrdTotales.TextMatrix(1, ColTot.MontoReal))
            If Not IsNumeric(Empaques.TextMatrix(Cont, ColPre.CantBaseFormula)) Then Empaques.TextMatrix(Cont, ColPre.CantBaseFormula) = 0
            !CantidadBaseFormula = CDbl(Empaques.TextMatrix(Cont, ColPre.CantBaseFormula))
            
        End With
        
        RsTemp.UpdateBatch
        
        If (tipo = "GORD") And RsTemp!n_Cantidad > 0 And Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
            Ent.BDD.Execute "UPDATE TR_ORDEN_PRODUCCION " & _
            "SET n_Cant_Utilizada = ROUND(n_Cant_Utilizada + (" & RsTemp!n_Cantidad & "), 8) " & _
            "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & _
            "AND c_CodArticulo = '" & RsTemp!c_CodArticulo & "' " & _
            "AND b_Producir = 0 ", TmpValue
        End If
        
        End If
        End If
        
    Next Cont
    
End Sub

Sub UpdateProducir(ByRef RsTemp As ADODB.Recordset, _
ByRef RsProduccion As ADODB.Recordset, TipoMov As String, _
ByVal pFechaProduccion As Date, tipo As String)
    
    Dim Cont As Integer, RsProductos As New ADODB.Recordset, Moneda_Prod As Integer
    Dim RsMonedaProd As New ADODB.Recordset
    Dim FactorMonedaProd As Double
    
    Dim Actualizar As Boolean
    Dim ActualizarCosto As Boolean
    Dim TmpValue
    
    If Not (tipo = "GESP") Then
        
        If FrmAppLink.PRDValidarCambioDeCosto Then
            
            'Call Mensaje(False, "�Desea actualizar el costo de los productos?" & vbNewLine & "Si es as�, presione aceptar para continuar.")
            Mensaje False, Replace(StellarMensaje(2812), "$(Line)", vbNewLine)
            
            If RetornoMensaje Then
                ActualizarCosto = True
            End If
            
        Else
            ActualizarCosto = True ' Siempre actualizar.
        End If
        
        If FrmAppLink.PRDCambioPrecioAutomatico > 0 Then
            Actualizar = True
        Else
            
            'Call Mensaje(False, "�Desea actualizar el precio de los productos?" & vbNewLine & "Si es as�, presione aceptar para continuar.")
            Mensaje False, Replace(StellarMensaje(2811), "$(Line)", vbNewLine)
            
            If FrmAppLink.RetornoMensaje Then
                Actualizar = True
            End If
            
        End If
        
    End If
    
    'For Cont = 1 To Producir.Rows - 1
        
        'If Producir.TextMatrix(Cont, ColumnaProducir.Producto) <> "" Then
            
            RsTemp.AddNew
            
            With RsTemp
                
                !c_Linea = Cont
                !c_Concepto = "PRD"
                !c_Documento = lbl_consecutivo.Caption
                !c_Deposito = txt_destino.Text
                !c_CodArticulo = txtProducir.Text
                
                !n_Cantidad = Round(CDbl(txtCantProducto), mDecimales)
                
                !n_Costo = CDbl(FormatNumber(CDbl(txtCostoUni), Std_Decm))
                !n_Subtotal = CDbl(FormatNumber(CDbl(txtCostoTotal), Std_Decm))
                
                !n_Total = !n_Subtotal
                
                !n_Impuesto = 0
                !c_TipoMov = TipoMov
                !n_Cant_Teorica = 0
                !n_Cant_Diferencia = 0
                
                !ns_CantidadEmpaque = CDbl(txtCantibul)
                
                !n_Precio = !n_Costo
                !n_Precio_Original = !n_Costo
                
                !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                
                !f_Fecha = FechaBD(pFechaProduccion, , True) 'Date
                !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
                
                If ExisteCampoTabla("n_FactorMonedaProd", RsTemp) Then 'CMM1N
                    '!n_FactorMonedaProd = mFactorLn
                    !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
                End If
                
                ''' Nuevo. Lote. Si algun dia hay que agregar algo mas en este campo, lo separamos por ,
                ''!c_Descripcion = StellarMensaje(2937) & " |" & Producir.TextMatrix(Cont, ColumnaProducir.Lote) & "|"
                'If FrmAppLink.InventarioCampoIDLote Then
                    '!c_IDLote = Producir.TextMatrix(Cont, ColumnaProducir.Lote)
                'End If
                
                ' Actualizar Precios!
                Call Apertura_RecordsetC(RsProductos)
                
                RsProductos.Open "SELECT * FROM MA_PRODUCTOS " & _
                "WHERE c_Codigo = '" & txtProducir & "'", _
                Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                If Not RsProductos.EOF Then
                    
                    'ACA AGREGO LAS VARIABLES NECESARIAS PARA EL AGREGADO EN HISTORICO
                    
                    Dim CostoAnterior As Double
                    Dim CostoRepAnterior As Double
                    Dim CostoAntAnterior As Double
                    Dim CostoProAnterior As Double
                    Dim Precio1Anterior As Double
                    Dim Precio2Anterior As Double
                    Dim Precio3Anterior As Double
                    
                    CostoAnterior = RsProductos!n_CostoAct
                    CostoRepAnterior = RsProductos!n_Costorep
                    CostoAntAnterior = RsProductos!n_CostoAnt
                    CostoProAnterior = RsProductos!n_CostoPro
                    Precio1Anterior = RsProductos!n_Precio1
                    Precio2Anterior = RsProductos!n_Precio2
                    Precio3Anterior = RsProductos!n_Precio3
                    
                    Call Apertura_RecordsetC(RsMonedaProd)
                    
                    RsMonedaProd.Open "SELECT * FROM MA_MONEDAS " & _
                    "WHERE c_CodMoneda = '" & RsProductos!c_CodMoneda & "'", _
                    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    If Not RsMonedaProd.EOF Then
                        Moneda_Prod = RsMonedaProd!n_Decimales
                        FactorMonedaProd = RsMonedaProd!n_Factor
                    Else
                        Moneda_Prod = 2
                        FactorMonedaProd = 1
                    End If
                    
                    'ACA SE ACTUALIZAN LOS COSTOS
                    
                    If ActualizarCosto Then
                        RsProductos!n_CostoAnt = RsProductos!n_CostoAct
                        RsProductos!n_CostoAct = CDbl(txtCostoUni)
                        RsProductos!n_CostoPro = CDbl(FormatNumber((RsProductos!n_CostoAnt + RsProductos!n_CostoAct) / 2, Moneda_Prod))
                        RsProductos!n_Costorep = RsProductos!n_CostoAct
                    End If
                    
                    'ACA SE AGREGAN PARA DEFINIRLOS Y GUARDAN LOS NUEVOS PRECIOS
                    Precio1 = 0
                    Precio2 = 0
                    Precio3 = 0
                    
                    If Actualizar Then
                        
                        Dim Porcentaje As Double
                        
                        If RsProductos!n_CostoAnt <> 0 Then
                            Porcentaje = (RsProductos!n_CostoAct / RsProductos!n_CostoAnt)
                        Else ' Evitar error. No Modificar precios.
                            'Mensaje True, "El producto [" & RsProductos!c_Descri & "] tiene el Costo Anterior en 0. No se pueden recalcular los precios para este producto. Debe verificar los precios manualmente."
                            Mensaje True, Replace(StellarMensaje(2813), "$(Desc)", RsProductos!c_Descri)
                            Porcentaje = 1
                        End If
                        
                        RsProductos!n_Precio1 = FormatNumber(RsProductos!n_Precio1 * Porcentaje, Moneda_Prod)
                        RsProductos!n_Precio2 = FormatNumber(RsProductos!n_Precio2 * Porcentaje, Moneda_Prod)
                        RsProductos!n_Precio3 = FormatNumber(RsProductos!n_Precio3 * Porcentaje, Moneda_Prod)
                        
                        'ACA SE GUARDAN LOS NUEVOS PRECIOS
                        Precio1 = CDbl(FormatNumber(RsProductos!n_Precio1 * Porcentaje, Moneda_Prod))
                        Precio2 = CDbl(FormatNumber(RsProductos!n_Precio2 * Porcentaje, Moneda_Prod))
                        Precio3 = CDbl(FormatNumber(RsProductos!n_Precio3 * Porcentaje, Moneda_Prod))
                        
                    End If
                    
                    RsProductos.UpdateBatch
                    
                    'ACA SE GUARDA LOS CAMBIOS EN LA TABLA MA_HISTORICO_COSTO_PRECIO
                    
                    If (CostoAnterior <> CDbl(RsProductos!n_CostoAct)) Or (CostoAntAnterior <> CDbl(RsProductos!n_CostoAnt)) Or (CostoProAnterior <> CDbl(RsProductos!n_CostoPro)) Or (CostoRepAnterior <> CDbl(RsProductos!n_Costorep)) Or (Precio1Anterior <> CDbl(Precio1)) Or (Precio2Anterior <> CDbl(Precio2)) Or (Precio3Anterior <> CDbl(Precio3)) Then
                        
                        If ExisteTablaV3("MA_HISTORICO_COSTO_PRECIO", Ent.BDD) Then
                            Dim Rs As New ADODB.Recordset
                            CorrelativoHistorico = No_Consecutivo("HISTORICO_COSTO_PRECIO", True)
                            Cadena = "insert into MA_HISTORICO_COSTO_PRECIO (c_Historico,d_FechaCambio,c_ProcesoOrigen,c_Documento,c_Usuario,c_CodArticulo,Precio1_Ant,Precio1_Nuevo,Precio2_Ant,Precio2_Nuevo,Precio3_Ant,Precio3_Nuevo,CostoAct_Ant,CostoAct_Nuevo,CostoRep_Ant,CostoRep_Nuevo,CostoAnt_Ant,CostoAnt_Nuevo,CostoPro_Ant,CostoPro_Nuevo,CodMoneda_Act,FacMoneda_Act)" _
                            & " values('" & CorrelativoHistorico & "', GETDATE(), 'PRODUCCION MANUAL', '" & LcConsecu & "','" & lcCodUsuario & "','" & RsProductos!c_Codigo & "', " & Precio1Anterior & "," & RsProductos!n_Precio1 & "," & Precio2Anterior & "," & RsProductos!n_Precio2 & "," & Precio3Anterior & "," & RsProductos!n_Precio3 & "," & CostoAnterior & "," & RsProductos!n_CostoAct & "," & CostoRepAnterior & "," & RsProductos!n_Costorep & "," & CostoAntAnterior & "," & RsProductos!n_CostoAnt & "," & CostoProAnterior & "," & RsProductos!n_CostoPro & ",'" & RsProductos!c_CodMoneda & "'," & CDec(FactorMonedaProd) & ")"
                            'Debug.Print Cadena
                            Rs.Open Cadena, Ent.BDD, adOpenDynamic, adLockBatchOptimistic
                            'Debug.Print cadena
                        End If
                        
                        If Not (tipo = "GESP") Then
                            PasarTrPend "TR_PENDIENTE_PROD", RsProductos, Ent.BDD, Ent.BDD, Array("id")
                        End If
                        
                    End If
                    
                    'HASTA AQUI EL AGREGADO
                    
                End If
                
            End With
            
            If (tipo = "GORD") And RsTemp!n_Cantidad > 0 And Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
                Ent.BDD.Execute "UPDATE TR_ORDEN_PRODUCCION " & _
                "SET n_Cant_Realizada = ROUND(n_Cant_Realizada + (" & RsTemp!n_Cantidad & "), 8) " & _
                "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & _
                "AND c_CodArticulo = '" & RsTemp!c_CodArticulo & "' " & _
                "AND b_Producir = 1 ", TmpValue
            End If
            
            RsTemp.UpdateBatch
            
            RsProduccion.AddNew
            
            With RsProduccion
                
                NewCont = NewCont + 1
                
                !c_Documento = lbl_consecutivo.Caption
                !c_Linea = NewCont
                !c_CodigoInput = txtProducir
                !c_CodArticulo = !c_CodigoInput
                !c_Descripcion = txtDescProducto
                !c_Presenta = lblNomenclaturaCant
                
                !n_Cantidad = Round(CDbl(txtCantProducto), mDecimales)
                !n_Costo = CDbl(FormatNumber(CDbl(txtCostoUni), Std_Decm))
                
                !n_PorcMerma = 0
                !n_CantidadNeta = !n_Cantidad
                !n_MontoMerma = 0
                !n_Subtotal = CDbl(FormatNumber(!n_Costo * !n_Cantidad, Std_Decm))
                !n_Descuento = 0
                !n_Impuesto = 0
                !n_Total = !n_Subtotal
                !n_CantidadProyectada = !n_CantidadNeta
                
                !b_Producir = 1
                !c_IDLote = vbNullString
                !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
                !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
                !ns_CantidadEmpaque = CDbl(txtCantibul)
                !nu_FactorCosto = 1
                
                !CantidadBaseFormula = CantidadBaseFormula
                
            End With
            
            RsProduccion.UpdateBatch
            
        'End If
        
    'Next Cont
    
End Sub

Sub UpDateDepositoUtilizar(ByRef RsTemp As ADODB.Recordset)
    
    Dim Cont As Integer
    
    For Cont = 1 To Ingredientes.Rows - 1
        
        If Ingredientes.TextMatrix(Cont, ColPre.Producto) <> "" Then
            
            Call Apertura_RecordsetC(RsTemp)
            
            RsTemp.Open "SELECT * FROM MA_DEPOPROD " & _
            "WHERE c_CodDeposito = '" & Trim(txt_origen.Text) & "' " & _
            "AND c_CodArticulo = '" & Ingredientes.TextMatrix(Cont, ColPre.Producto) & "' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsTemp.EOF Then
                RsTemp.AddNew
                RsTemp!n_Cantidad = 0
                RsTemp!n_Cant_Comprometida = 0
                RsTemp!n_Cant_Ordenada = 0
            End If
            
            With RsTemp
                !c_CodDeposito = Trim(txt_origen.Text)
                !c_CodArticulo = Ingredientes.TextMatrix(Cont, ColPre.Producto)
                !c_Descripcion = Ingredientes.TextMatrix(Cont, ColPre.Descripci�n)
                !n_Cantidad = CDbl(FormatNumber(!n_Cantidad - _
                CDbl(Ingredientes.TextMatrix(Cont, ColPre.Canti)), _
                CDbl(Ingredientes.TextMatrix(Cont, ColPre.LDeci))))
            End With
            
            RsTemp.UpdateBatch
            
        End If
        
    Next Cont
    
    For Cont = 0 To Empaques.Rows - 1
        
        If Empaques.TextMatrix(Cont, ColPre.Producto) <> "" Then
            
            Call Apertura_RecordsetC(RsTemp)
            
            RsTemp.Open "SELECT * FROM MA_DEPOPROD " & _
            "WHERE c_CodDeposito = '" & Trim(txt_origen.Text) & "' " & _
            "AND c_CodArticulo = '" & Empaques.TextMatrix(Cont, ColPre.Producto) & "' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsTemp.EOF Then
                RsTemp.AddNew
                RsTemp!n_Cantidad = 0
                RsTemp!n_Cant_Comprometida = 0
                RsTemp!n_Cant_Ordenada = 0
            End If
            
            With RsTemp
                !c_CodDeposito = Trim(txt_origen.Text)
                !c_CodArticulo = Empaques.TextMatrix(Cont, ColPre.Producto)
                !c_Descripcion = Empaques.TextMatrix(Cont, ColPre.Descripci�n)
                !n_Cantidad = CDbl(FormatNumber(!n_Cantidad - _
                CDbl(Empaques.TextMatrix(Cont, ColPre.Canti)), _
                CDbl(Empaques.TextMatrix(Cont, ColPre.LDeci))))
            End With
            
            RsTemp.UpdateBatch
            
        End If
        
    Next Cont
    
End Sub

Sub UpDateDepositoProducir(ByRef RsTemp As ADODB.Recordset)
    
    Dim Cont As Integer
    
    'For Cont = 1 To Producir.Rows - 1
        
        If txtProducir <> "" Then
            
            Call Apertura_RecordsetC(RsTemp)
            
            RsTemp.Open "SELECT * FROM MA_DEPOPROD " & _
            "WHERE c_CodDeposito = '" & Trim(txt_destino.Text) & "' " & _
            "AND c_CodArticulo = '" & txtProducir & "' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsTemp.EOF Then
                RsTemp.AddNew
                RsTemp!n_Cantidad = 0
                RsTemp!n_Cant_Comprometida = 0
                RsTemp!n_Cant_Ordenada = 0
            End If
            
            With RsTemp
                !c_CodDeposito = Trim(txt_destino.Text)
                !c_CodArticulo = txtProducir
                !c_Descripcion = txtDescProducto
                !n_Cantidad = CDbl(FormatNumber(!n_Cantidad + _
                CDbl(txtCantProducto), mDecimales))
            End With
            
            RsTemp.UpdateBatch
            
        End If
        
    'Next Cont
    
End Sub

Function UpdateMerma(Status As String, ConsecutivoAjuste As String, _
Optional orden As String) As Boolean
    
    Dim Cont As Integer, RsAjuste As New ADODB.Recordset, RsTrAjuste As New ADODB.Recordset
    UpdateMerma = False
    
    Call Apertura_RecordsetC(RsAjuste)
    
    If ConsecutivoAjuste = "" Then
        UpdateMerma = True
    Else
        RsAjuste.Open "SELECT * FROM MA_INVENTARIO " & _
        "WHERE c_Documento = '" & ConsecutivoAjuste & "' " & _
        "AND c_Concepto = 'AJU' AND c_Status = 'DCO' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If RsAjuste.EOF Then
            RsAjuste.AddNew
            With RsAjuste
                !c_Documento = ConsecutivoAjuste
                !c_Concepto = "AJU"
                !d_Fecha = Date
                !c_Descripcion = "Ajuste por Producci�n N� '" & orden & "', merma de productos"
                !c_Status = Status
                !c_CodProveedOR = ""
                !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                !c_CodMoneda = dbmoneda.Text
                !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
                'If ExisteCampoTabla("n_FactorMonedaProd", RsAjuste) Then 'CMM1N
                    ''!n_FactorMonedaProd = mFactorLn
                    '!n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
                'End If
                !n_Descuento = CDbl(FormatNumber(TotalMerma, Std_Decm))
                !c_Observacion = "Ajuste Autom�tico por Produccion N� '" & orden & "', por merma de productos"
                !c_Relacion = orden
                !c_CodComprador = FrmAppLink.GetCodUsuario
                !c_Dep_Orig = txt_origen.Text
                !c_Dep_Dest = ""
                !c_Motivo = "Producci�n"
                !c_CodTransporte = ""
                !c_Ejecutor = FrmAppLink.GetCodUsuario
                !c_Factura = ""
                !c_Transportista = ""
                !n_Cantidad_compra = 0
                !CodConcepto = ConceptoAjuste
            End With
            
            RsAjuste.UpdateBatch
            Call Apertura_RecordsetC(RsTrAjuste)
            RsTrAjuste.Open "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Documento = '" & ConsecutivoAjuste & "' " & _
            "AND c_Concepto = 'AJU' AND c_TipoMov = 'Descargo' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            Dim NewCont As Long
            
            If RsTrAjuste.EOF Then
                With RsTrAjuste
                    
                    For Cont = 1 To Ingredientes.Rows - 1
                        If IsNumeric(Ingredientes.TextMatrix(Cont, ColPre.PorcMerma)) Then
                        If CDbl(Ingredientes.TextMatrix(Cont, ColPre.PorcMerma)) > 0 Then
                            .AddNew
                            NewCont = NewCont + 1
                            !c_Linea = NewCont
                            !c_Concepto = "AJU"
                            !c_Documento = ConsecutivoAjuste
                            !c_Deposito = txt_origen.Text
                            !c_CodArticulo = Trim(Ingredientes.TextMatrix(Cont, ColPre.Producto))
                            
                            !n_Cantidad = CDbl(FormatNumber(Ingredientes.TextMatrix(Cont, ColPre.Canti), _
                            CDbl(Ingredientes.TextMatrix(Cont, ColPre.LDeci)))) _
                            * Ingredientes.TextMatrix(Cont, ColPre.PorcMerma) / 100
                            
                            !n_Costo = FormatNumber(Ingredientes.TextMatrix(Cont, ColPre.CostoPre), Std_Decm)
                            !n_Subtotal = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.CostoPreXMerma)), Std_Decm))
                            
                            !n_Impuesto = 0
                            !n_Total = !n_Subtotal
                            !c_TipoMov = "Descargo"
                            !n_Cant_Teorica = 0
                            !n_Cant_Diferencia = 0
                            !n_Precio = CDbl(FormatNumber(Ingredientes.TextMatrix(Cont, ColPre.CostoPre), Std_Decm))
                            !n_Precio_Original = CDbl(FormatNumber(Ingredientes.TextMatrix(Cont, ColPre.CostoPreXMerma), Std_Decm))
                            !f_Fecha = Date
                            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                            !n_FactorCambio = CDbl(FormatNumber(CDbl(MSK_FACTOR.Text), Std_Decm))
                            !CodConcepto = ConceptoAjuste
                            !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                            !ns_CantidadEmpaque = CDbl(Ingredientes.TextMatrix(Cont, ColPre.nCantibul))
                            RsTrAjuste.UpdateBatch
                        End If
                        End If
                    Next Cont
                    
                    For Cont = 0 To Empaques.Rows - 1
                        If IsNumeric(Empaques.TextMatrix(Cont, ColPre.PorcMerma)) Then
                        If CDbl(Empaques.TextMatrix(Cont, ColPre.PorcMerma)) > 0 Then
                            .AddNew
                            NewCont = NewCont + 1
                            !c_Linea = NewCont
                            !c_Concepto = "AJU"
                            !c_Documento = ConsecutivoAjuste
                            !c_Deposito = txt_origen.Text
                            !c_CodArticulo = Trim(Empaques.TextMatrix(Cont, ColPre.Producto))
                            
                            !n_Cantidad = CDbl(FormatNumber(Empaques.TextMatrix(Cont, ColPre.Canti), _
                            CDbl(Empaques.TextMatrix(Cont, ColPre.LDeci)))) _
                            * Empaques.TextMatrix(Cont, ColPre.PorcMerma) / 100
                            
                            !n_Costo = FormatNumber(Empaques.TextMatrix(Cont, ColPre.CostoPre), Std_Decm)
                            !n_Subtotal = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.CostoPreXMerma)), Std_Decm))
                            
                            !n_Impuesto = 0
                            !n_Total = !n_Subtotal
                            !c_TipoMov = "Descargo"
                            !n_Cant_Teorica = 0
                            !n_Cant_Diferencia = 0
                            !n_Precio = CDbl(FormatNumber(Empaques.TextMatrix(Cont, ColPre.CostoPre), Std_Decm))
                            !n_Precio_Original = CDbl(FormatNumber(Empaques.TextMatrix(Cont, ColPre.CostoPreXMerma), Std_Decm))
                            !f_Fecha = Date
                            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                            !n_FactorCambio = CDbl(FormatNumber(CDbl(MSK_FACTOR.Text), Std_Decm))
                            !CodConcepto = ConceptoAjuste
                            !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                            !ns_CantidadEmpaque = CDbl(Empaques.TextMatrix(Cont, ColPre.nCantibul))
                            RsTrAjuste.UpdateBatch
                        End If
                        End If
                    Next Cont
                    
                End With
                
                UpdateMerma = True
                
            Else
                UpdateMerma = False
            End If
            
        Else
            UpdateMerma = False
        End If
        
    End If
    
End Function

