Attribute VB_Name = "EventoTeclado"
Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

Private Enum KeyConst
    KEYEVENTF_EXTENDEDKEY = &H1
    KEYEVENTF_KEYUP = &H2
    KF_ALTDOWN = &H2000
    KF_DLGMODE = &H800
    KF_EXTENDED = &H100
    KF_MENUMODE = &H1000
    KF_REPEAT = &H4000
    KF_UP = &H8000
    KF_NORMAL = &H0
End Enum

Public Enum KeyAltCtrlConst
    LEFT_ALT_PRESSED = &H2
    LEFT_CTRL_PRESSED = &H8
    RIGHT_ALT_PRESSED = &H1
    RIGHT_CTRL_PRESSED = &H4
End Enum

Public Const VK_ADD = &H6B
Public Const VK_ATTN = &HF6
Public Const VK_BACK = &H8
Public Const VK_CANCEL = &H3
Public Const VK_CAPITAL = &H14
Public Const VK_CLEAR = &HC
Public Const VK_CRSEL = &HF7
Public Const VK_DECIMAL = &H6E
Public Const VK_DELETE = &H2E
Public Const VK_DIVIDE = &H6F
Public Const VK_DOWN = &H28
Public Const VK_END = &H23
Public Const VK_EREOF = &HF9
Public Const VK_ESCAPE = &H1B
Public Const VK_EXECUTE = &H2B
Public Const VK_EXSEL = &HF8
Public Const VK_F1 = &H70
Public Const VK_F10 = &H79
Public Const VK_F11 = &H7A
Public Const VK_F12 = &H7B
Public Const VK_F13 = &H7C
Public Const VK_F14 = &H7D
Public Const VK_F15 = &H7E
Public Const VK_F16 = &H7F
Public Const VK_F17 = &H80
Public Const VK_F18 = &H81
Public Const VK_F19 = &H82
Public Const VK_F2 = &H71
Public Const VK_F20 = &H83
Public Const VK_F21 = &H84
Public Const VK_F22 = &H85
Public Const VK_F23 = &H86
Public Const VK_F24 = &H87
Public Const VK_F3 = &H72
Public Const VK_F4 = &H73
Public Const VK_F5 = &H74
Public Const VK_F6 = &H75
Public Const VK_F7 = &H76
Public Const VK_F8 = &H77
Public Const VK_F9 = &H78
Public Const VK_HELP = &H2F
Public Const VK_HOME = &H24
Public Const VK_INSERT = &H2D
Public Const VK_LBUTTON = &H1
Public Const VK_LCONTROL = &HA2
Public Const VK_LEFT = &H25
Public Const VK_LMENU = &HA4
Public Const VK_LSHIFT = &HA0
Public Const VK_MBUTTON = &H4             '  NO es contiguo a L RBUTTON
Public Const VK_MULTIPLY = &H6A
Public Const VK_NEXT = &H22
Public Const VK_NONAME = &HFC
Public Const VK_NUMLOCK = &H90
Public Const VK_NUMPAD0 = &H60
Public Const VK_NUMPAD1 = &H61
Public Const VK_NUMPAD2 = &H62
Public Const VK_NUMPAD3 = &H63
Public Const VK_NUMPAD4 = &H64
Public Const VK_NUMPAD5 = &H65
Public Const VK_NUMPAD6 = &H66
Public Const VK_NUMPAD7 = &H67
Public Const VK_NUMPAD8 = &H68
Public Const VK_NUMPAD9 = &H69
Public Const VK_OEM_CLEAR = &HFE
Public Const VK_PA1 = &HFD
Public Const VK_PAUSE = &H13
Public Const VK_PLAY = &HFA
Public Const VK_PRINT = &H2A
Public Const VK_PRIOR = &H21
Public Const VK_PROCESSKEY = &HE5
Public Const VK_RBUTTON = &H2
Public Const VK_RCONTROL = &HA3
Public Const VK_TAB = &H9
Public Const VK_RETURN = &HD
Public Const VK_RIGHT = &H27
Public Const VK_RMENU = &HA5
Public Const VK_RSHIFT = &HA1
Public Const VK_SCROLL = &H91
Public Const VK_SELECT = &H29
Public Const VK_SEPARATOR = &H6C
Public Const VK_SHIFT = &H10
Public Const VK_CONTROL = &H11
Public Const VK_MENU = &H12
Public Const VK_SNAPSHOT = &H2C
Public Const VK_SPACE = &H20
Public Const VK_SUBTRACT = &H6D
Public Const VK_UP = &H26
Public Const VK_ZOOM = &HFB

Public oTeclado As New ClsTeclado

Public Sub SendKeys(Caracteres As String, Optional opcion As Boolean = True, Optional Repetir As Boolean = False)
    
    On Error GoTo Errores
    
    Dim CCont As Integer, CtrlPend As Boolean, AltPend As Boolean, ShiftPend As Boolean
    Dim SubCommand As String
    
    'Caracteres = EjecutarAccion(Caracteres)
    CtrlPend = False
    AltPend = False
    ShiftPend = False
    
    Do While Len(Caracteres) > 0
        
        If Mid(Caracteres, 1, 1) = "{" And InStr(2, Caracteres, "}") > 0 Then
            
            SubCommand = ExtraerIntervalo(Caracteres, True)
            
            Select Case SubCommand
                Case "{^}"
                    If CtrlPend Then
                        CtrlPend = False
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    Else
                        CtrlPend = True
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                    End If
                Case "{%}"
                    If AltPend Then
                        AltPend = False
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_UP, 0)
                    Else
                        AltPend = True
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_ALTDOWN, 0)
                    End If
                Case "{+}"
                    If ShiftPend Then
                        ShiftPend = False
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    Else
                        ShiftPend = True
                        Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                    End If
                Case "{WINDOWS}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{MENU}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{SCROLLLOCK}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{PAUSE}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Case "{INSERT}"
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
'                Case "{BACKSPACE}", "{BS}", "{BKSP}", "{DELETE}", "{DEL}"
'                    Call keybd_event(ExecAction(SubCommand), IIf(Opcion, 1, 0), KeyConst.KF_REPEAT, 0)
                Case Else
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                    Call keybd_event(ExecAction(SubCommand), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
            End Select
        Else
            If Mid(Caracteres, 1, 1) = "~" Then
                Call keybd_event(vbKeyReturn, IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                Call keybd_event(vbKeyReturn, IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                Caracteres = Mid(Caracteres, 2, Len(Caracteres))
            Else
                If NecesitaDoble(CByte(Asc(Mid(Caracteres, 1, 1)))) Then
                    If CByte(Asc(Mid(Caracteres, 1, 1))) = 18 Then
                        If AltPend Then
                            AltPend = False
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_UP, 0)
                        Else
                            AltPend = True
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_ALTDOWN, 0)
                        End If
                    ElseIf CByte(Asc(Mid(Caracteres, 1, 1))) = vbKeyControl Then
                        If CtrlPend Then
                            CtrlPend = False
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        Else
                            CtrlPend = True
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                        End If
                    ElseIf CByte(Asc(Mid(Caracteres, 1, 1))) = vbKeyShift Then
                        If ShiftPend Then
                            ShiftPend = False
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        Else
                            ShiftPend = True
                            Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_NORMAL, 0)
                        End If
                    Else
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        If CtrlPend Then
                            CtrlPend = False
                            Call keybd_event(CByte(vbKeyControl), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        End If
                        If ShiftPend Then
                            ShiftPend = False
                            Call keybd_event(CByte(vbKeyShift), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        End If
                        If AltPend Then
                            AltPend = False
                            Call keybd_event(CByte(18), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                        End If
                    End If
                    Caracteres = Mid(Caracteres, 2, Len(Caracteres))
                Else
                    If Repetir Then
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KF_REPEAT, 0)
                    Else
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_EXTENDEDKEY, 0)
                        Call keybd_event(CByte(Asc(Mid(Caracteres, 1, 1))), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                    
                    Caracteres = Mid(Caracteres, 2, Len(Caracteres))
                    
                    If CtrlPend Then
                        CtrlPend = False
                        Call keybd_event(CByte(vbKeyControl), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                    
                    If ShiftPend Then
                        ShiftPend = False
                        Call keybd_event(CByte(vbKeyShift), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                    
                    If AltPend Then
                        AltPend = False
                        Call keybd_event(CByte(18), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
                    End If
                End If
            End If
        End If
    Loop
    
    If CtrlPend Then
        CtrlPend = False
        Call keybd_event(CByte(vbKeyControl), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
    End If
    
    If ShiftPend Then
        ShiftPend = False
        Call keybd_event(CByte(vbKeyShift), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
    End If
    
    If AltPend Then
        AltPend = False
        Call keybd_event(CByte(18), IIf(opcion, 1, 0), KeyConst.KEYEVENTF_KEYUP, 0)
    End If
    
    Exit Sub
    
Errores:
    
    'Debug.Print Err.Description
    Err.Clear
    
End Sub

Private Function ExecAction(ByRef Accion As String) As Byte
    Select Case UCase(Accion)
        Case "{BACKSPACE}", "{BS}", "{BKSP}"
            ExecAction = CByte(vbKeyBack)
        Case "{SPACE}"
            ExecAction = CByte(vbKeySpace)
        Case "{BREAK}"
            ExecAction = CByte(vbKeyCancel)
        Case "{PAUSE}"
            ExecAction = CByte(vbKeyPause)
        Case "{CAPSLOCK}"
            ExecAction = CByte(vbKeyCapital)
        Case "{DELETE}", "{DEL}"
            ExecAction = CByte(vbKeyDelete)
        Case "{DOWN}"
            ExecAction = CByte(vbKeyDown)
        Case "{END}"
            ExecAction = CByte(vbKeyEnd)
        Case "{ENTER}", "~"
            ExecAction = CByte(vbKeyReturn)
        Case "{ESC}"
            ExecAction = CByte(vbKeyEscape)
        Case "{HELP}"
            ExecAction = CByte(vbKeyHelp)
        Case "{HOME}"
            ExecAction = CByte(vbKeyHome)
        Case "{INSERT}", "{INS}"
            ExecAction = CByte(vbKeyInsert)
        Case "{LEFT}"
            ExecAction = CByte(vbKeyLeft)
        Case "{NUMLOCK}"
            ExecAction = CByte(vbKeyNumlock)
        Case "{PGDN}"
            ExecAction = CByte(vbKeyPageDown)
        Case "{PGUP}"
            ExecAction = CByte(vbKeyPageUp)
        Case "{PRTSC}"
            ExecAction = CByte(vbKeyPrint)
        Case "{RIGHT}"
            ExecAction = CByte(vbKeyRight)
        Case "{SCROLLLOCK}"
            ExecAction = CByte(145)
        Case "{TAB}"
            ExecAction = CByte(vbKeyTab)
        Case "{UP}"
            ExecAction = CByte(vbKeyUp)
        Case "{F1}"
            ExecAction = CByte(vbKeyF1)
        Case "{F2}"
            ExecAction = CByte(vbKeyF2)
        Case "{F3}"
            ExecAction = CByte(vbKeyF3)
        Case "{F4}"
            ExecAction = CByte(vbKeyF4)
        Case "{F5}"
            ExecAction = CByte(vbKeyF5)
        Case "{F6}"
            ExecAction = CByte(vbKeyF6)
        Case "{F7}"
            ExecAction = CByte(vbKeyF7)
        Case "{F8}"
            ExecAction = CByte(vbKeyF8)
        Case "{F9}"
            ExecAction = CByte(vbKeyF9)
        Case "{F10}"
            ExecAction = CByte(vbKeyF10)
        Case "{F11}"
            ExecAction = CByte(vbKeyF11)
        Case "{F12}"
            ExecAction = CByte(vbKeyF12)
        Case "{F13}"
            ExecAction = CByte(vbKeyF13)
        Case "{F14}"
            ExecAction = CByte(vbKeyF14)
        Case "{F15}"
            ExecAction = CByte(vbKeyF15)
        Case "{F16}"
            ExecAction = CByte(vbKeyF16)
        Case "{+}"
            ExecAction = CByte(vbKeyShift)
        Case "{^}"
            ExecAction = CByte(vbKeyControl)
        Case "{%}"
            ExecAction = CByte(18)
        Case "{WINDOWS}"
            ExecAction = CByte(91)
        Case "{MENU}"
            ExecAction = CByte(93)
        Case "{.}"
            ExecAction = CByte(vbKeyDecimal)
    End Select
End Function

Private Function ExtraerIntervalo(ByRef Cadena As String, Optional eliminar As Boolean = False) As String
    Dim ILLave As Integer, FLlave As Integer, SubCaracter As String, Caracteres As String
    Const ValidCad = "{BACKSPACE}{BS}{BKSP}{BREAK}{CAPSLOCK}{DELETE}{DEL}{DOWN}{END}{ENTER}~{ESC}{HELP}{HOME}{INSERT}{INS}{LEFT}{NUMLOCK}{PGDN}{PGUP}{PRTSC}{RIGHT}{SCROLLLOCK}{TAB}{UP}{F1}{F2}{F3}{F4}{F5}{F6}{F7}{F8}{F9}{F10}{F11}{F12}{F13}{F14}{F15}{F16}"
    Const ControlsCad = "{+}{^}{%}"
    Caracteres = Cadena
    ILLave = 1
    If Len(Caracteres) <= 0 Then Exit Function
    ILLave = InStr(ILLave, Caracteres, "{")
    If ILLave = 0 Then Exit Function
    FLlave = InStr(ILLave, Caracteres, "}")
    SubCaracter = UCase(Mid(Caracteres, ILLave, FLlave - ILLave + 1))
    Caracteres = Replace(UCase(Caracteres), SubCaracter, "", ILLave, 1)
    ExtraerIntervalo = SubCaracter
    If eliminar Then
        Cadena = Caracteres
    End If
End Function

Private Function NecesitaDoble(ByRef Accion As Byte) As Boolean
    NecesitaDoble = False
    Select Case Accion
        Case vbKeyBack
            NecesitaDoble = True
        Case vbKeySpace
            NecesitaDoble = False
        Case vbKeyCancel
            NecesitaDoble = True
        Case vbKeyPause
            NecesitaDoble = True
        Case vbKeyCapital
            NecesitaDoble = True
        Case vbKeyDelete
            NecesitaDoble = True
        Case vbKeyDecimal
            NecesitaDoble = True
        Case vbKeyDown
            NecesitaDoble = True
        Case vbKeyEnd
            NecesitaDoble = True
        Case vbKeyReturn
            NecesitaDoble = True
        Case vbKeyEscape
            NecesitaDoble = True
        Case vbKeyHelp
            NecesitaDoble = True
        Case vbKeyHome
            NecesitaDoble = True
        Case vbKeyInsert
            NecesitaDoble = True
        Case vbKeyLeft
            NecesitaDoble = True
        Case vbKeyNumlock
            NecesitaDoble = True
        Case vbKeyPageDown
            NecesitaDoble = True
        Case vbKeyPageUp
            NecesitaDoble = True
        Case vbKeyPrint
            NecesitaDoble = True
        Case vbKeyRight
            NecesitaDoble = True
        Case 145
            NecesitaDoble = True
        Case vbKeyTab
            NecesitaDoble = False
        Case vbKeyUp
            NecesitaDoble = True
        Case vbKeyF1
            NecesitaDoble = True
        Case vbKeyF2
            NecesitaDoble = True
        Case vbKeyF3
            NecesitaDoble = True
        Case vbKeyF4
            NecesitaDoble = True
        Case vbKeyF5
            NecesitaDoble = True
        Case vbKeyF6
            NecesitaDoble = True
        Case vbKeyF7
            NecesitaDoble = True
        Case vbKeyF8
            NecesitaDoble = True
        Case vbKeyF9
            NecesitaDoble = True
        Case vbKeyF10
            NecesitaDoble = True
        Case vbKeyF11
            NecesitaDoble = True
        Case vbKeyF12
            NecesitaDoble = True
        Case vbKeyF13
            NecesitaDoble = True
        Case vbKeyF14
            NecesitaDoble = True
        Case vbKeyF15
            NecesitaDoble = True
        Case vbKeyF16
            NecesitaDoble = True
        Case vbKeyShift
            NecesitaDoble = True
        Case vbKeyControl
            NecesitaDoble = True
        Case 18
            NecesitaDoble = True
        Case 91
            NecesitaDoble = True
        Case 93
            NecesitaDoble = True
    End Select
End Function

