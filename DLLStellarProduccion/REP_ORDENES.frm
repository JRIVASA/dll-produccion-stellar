VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form REP_ORDENES 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8655
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   9915
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8655
   ScaleWidth      =   9915
   Begin VB.CheckBox ChkPrdAdv 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Ver Datos Prod. Avanzada"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   5520
      TabIndex        =   76
      Top             =   6960
      Value           =   1  'Checked
      Width           =   4200
   End
   Begin VB.CommandButton CmdOpenXML 
      Height          =   495
      Left            =   5460
      Picture         =   "REP_ORDENES.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   47
      Top             =   7500
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&XML"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   2
      Left            =   5160
      Picture         =   "REP_ORDENES.frx":3738
      Style           =   1  'Graphical
      TabIndex        =   46
      Top             =   7440
      Width           =   1100
   End
   Begin VB.CheckBox chk_ResumenDia 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Resumido por D�a"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   300
      TabIndex        =   12
      Top             =   6300
      Width           =   3000
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   40
      Top             =   0
      Width           =   10800
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7395
         TabIndex        =   42
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label Label14 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Ordenes de Produccion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   41
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Pantalla"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   0
      Left            =   6315
      Picture         =   "REP_ORDENES.frx":54BA
      Style           =   1  'Graphical
      TabIndex        =   18
      ToolTipText     =   "Vista Preliminar"
      Top             =   7425
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Impresora"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   1
      Left            =   7455
      Picture         =   "REP_ORDENES.frx":723C
      Style           =   1  'Graphical
      TabIndex        =   19
      ToolTipText     =   "Imprimir Reporte"
      Top             =   7425
      Width           =   1095
   End
   Begin VB.CommandButton cmd_salir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   8610
      Picture         =   "REP_ORDENES.frx":8FBE
      Style           =   1  'Graphical
      TabIndex        =   20
      ToolTipText     =   "Salir del Reporte"
      Top             =   7425
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1500
      Left            =   105
      TabIndex        =   26
      Top             =   6915
      Width           =   4530
      Begin VB.ListBox Ordenar 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   990
         Left            =   90
         Sorted          =   -1  'True
         TabIndex        =   13
         ToolTipText     =   "Campos de clasificaci�n"
         Top             =   345
         Width           =   1590
      End
      Begin VB.CommandButton add 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   330
         Left            =   1740
         Picture         =   "REP_ORDENES.frx":AD40
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "A�adir a la lista de selecci�n"
         Top             =   350
         Width           =   720
      End
      Begin VB.CommandButton eliminar 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1740
         Picture         =   "REP_ORDENES.frx":B542
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Eliminar de la lista de selecci�n"
         Top             =   665
         Width           =   720
      End
      Begin VB.CommandButton Borrar 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1740
         Picture         =   "REP_ORDENES.frx":BD44
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Eliminar todos"
         Top             =   980
         Width           =   720
      End
      Begin VB.ListBox seleccionados 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   990
         Left            =   2820
         TabIndex        =   17
         ToolTipText     =   "Campos seleccionados"
         Top             =   330
         Width           =   1590
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00AE5B00&
         X1              =   2415
         X2              =   4400
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de Ordenamiento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   3
         Left            =   0
         TabIndex        =   43
         Top             =   0
         Width           =   2265
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   3
         Left            =   2685
         TabIndex        =   30
         ToolTipText     =   "Ordenar Primero"
         Top             =   360
         Width           =   105
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   4
         Left            =   2685
         TabIndex        =   29
         ToolTipText     =   "Ordenar Segundo"
         Top             =   540
         Width           =   105
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   5
         Left            =   2685
         TabIndex        =   28
         ToolTipText     =   "Ordenar Tercero"
         Top             =   735
         Width           =   105
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   6
         Left            =   2595
         TabIndex        =   27
         ToolTipText     =   "Ordenar ..."
         Top             =   900
         Width           =   180
      End
   End
   Begin VB.Frame FrameCriterios 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   6165
      Left            =   120
      TabIndex        =   22
      Top             =   585
      Width           =   9675
      Begin VB.ComboBox CboTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   5355
         Style           =   2  'Dropdown List
         TabIndex        =   75
         Top             =   5220
         Width           =   3120
      End
      Begin VB.CommandButton bDpto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3180
         Picture         =   "REP_ORDENES.frx":C546
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   3120
         Width           =   435
      End
      Begin VB.TextBox departamento 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1680
         TabIndex        =   66
         Top             =   3120
         Width           =   1440
      End
      Begin VB.CommandButton bGrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3180
         Picture         =   "REP_ORDENES.frx":CD48
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   3600
         Width           =   435
      End
      Begin VB.TextBox grupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1680
         TabIndex        =   64
         Top             =   3600
         Width           =   1440
      End
      Begin VB.CommandButton bSubgrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3180
         Picture         =   "REP_ORDENES.frx":D54A
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   4080
         Width           =   435
      End
      Begin VB.TextBox subgrupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1680
         TabIndex        =   62
         Top             =   4080
         Width           =   1440
      End
      Begin VB.ComboBox CmbStatus 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   7665
         Style           =   2  'Dropdown List
         TabIndex        =   60
         ToolTipText     =   "Seleccione el Tipo de Impresi�n."
         Top             =   5040
         Visible         =   0   'False
         Width           =   2145
      End
      Begin VB.CheckBox ChkFechaEstimada 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Fecha de Produccion"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3600
         TabIndex        =   59
         Top             =   5220
         Visible         =   0   'False
         Width           =   2145
      End
      Begin VB.CommandButton CmdFormula 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6600
         Picture         =   "REP_ORDENES.frx":DD4C
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   900
         Width           =   435
      End
      Begin VB.TextBox txtFormula 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4800
         MaxLength       =   20
         TabIndex        =   53
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   885
         Width           =   1755
      End
      Begin VB.ComboBox CboLnP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   5355
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   4680
         Width           =   3120
      End
      Begin VB.TextBox txtLote 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1380
         MaxLength       =   20
         TabIndex        =   48
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   4680
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton CmdDocumentos 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3180
         Picture         =   "REP_ORDENES.frx":E54E
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   900
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.TextBox txt_orden 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1380
         MaxLength       =   20
         TabIndex        =   2
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   885
         Width           =   1755
      End
      Begin VB.TextBox txt_comprador 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1380
         MaxLength       =   10
         TabIndex        =   10
         ToolTipText     =   "Ingrese el c�digo del comprador o usuario"
         Top             =   2505
         Width           =   1755
      End
      Begin VB.CommandButton cmd_comprador 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3165
         Picture         =   "REP_ORDENES.frx":ED50
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   2505
         Width           =   435
      End
      Begin VB.TextBox txt_producto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1380
         TabIndex        =   8
         ToolTipText     =   "Indique el c�digo del producto"
         Top             =   2095
         Width           =   1755
      End
      Begin VB.CommandButton cmd_producto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3165
         Picture         =   "REP_ORDENES.frx":F552
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2095
         Width           =   435
      End
      Begin VB.TextBox txt_sucursal 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1380
         TabIndex        =   4
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   1285
         Width           =   1755
      End
      Begin VB.CommandButton cmd_sucursal 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3165
         Picture         =   "REP_ORDENES.frx":FD54
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1285
         Width           =   435
      End
      Begin VB.ComboBox Modalidad 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10020
         Style           =   2  'Dropdown List
         TabIndex        =   21
         ToolTipText     =   "Seleccione el Tipo de Impresi�n."
         Top             =   2505
         Visible         =   0   'False
         Width           =   2145
      End
      Begin VB.CommandButton cmd_Deporig 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3165
         Picture         =   "REP_ORDENES.frx":10556
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1695
         Width           =   435
      End
      Begin VB.TextBox origen 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1380
         TabIndex        =   6
         ToolTipText     =   "Indique el deposito origen"
         Top             =   1695
         Width           =   1755
      End
      Begin MSComCtl2.DTPicker Fechalow 
         CausesValidation=   0   'False
         Height          =   330
         Left            =   1380
         TabIndex        =   0
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   345
         Width           =   2145
         _ExtentX        =   3784
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   135725057
         CurrentDate     =   36415
      End
      Begin MSComCtl2.DTPicker Fechahigh 
         CausesValidation=   0   'False
         Height          =   330
         Left            =   7050
         TabIndex        =   1
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   330
         Width           =   2145
         _ExtentX        =   3784
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   135725057
         CurrentDate     =   36415
      End
      Begin MSComCtl2.DTPicker DTFechaEstimadaIni 
         CausesValidation=   0   'False
         Height          =   330
         Left            =   4200
         TabIndex        =   55
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   5400
         Visible         =   0   'False
         Width           =   2145
         _ExtentX        =   3784
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   135725057
         CurrentDate     =   36415
      End
      Begin MSComCtl2.DTPicker DTFechaEstimadaFin 
         CausesValidation=   0   'False
         Height          =   330
         Left            =   6960
         TabIndex        =   58
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   5400
         Visible         =   0   'False
         Width           =   2145
         _ExtentX        =   3784
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   135725057
         CurrentDate     =   36415
      End
      Begin VB.Label lblTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Turno de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3360
         TabIndex        =   74
         Top             =   5220
         Width           =   1875
      End
      Begin VB.Label lblDpto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   73
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label lblGrupo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   72
         Top             =   3600
         Width           =   510
      End
      Begin VB.Label LBL_DEPARTAMENTO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3660
         TabIndex        =   71
         Top             =   3120
         UseMnemonic     =   0   'False
         Width           =   5790
      End
      Begin VB.Label LBL_GRUPO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3660
         TabIndex        =   70
         Top             =   3600
         UseMnemonic     =   0   'False
         Width           =   5790
      End
      Begin VB.Label LBL_SUBGRUPO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3660
         TabIndex        =   69
         Top             =   4080
         UseMnemonic     =   0   'False
         Width           =   5790
      End
      Begin VB.Label lblSubgrupo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Subgrupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   68
         Top             =   4080
         Width           =   825
      End
      Begin VB.Label lblStatus 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8220
         TabIndex        =   61
         Top             =   4740
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblFechaEstimadaFin 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   6600
         TabIndex        =   57
         Top             =   5460
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label lblFechaEstimadaIni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3420
         TabIndex        =   56
         Top             =   5460
         Visible         =   0   'False
         Width           =   525
      End
      Begin VB.Label lblFormula 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Formula N�"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3720
         TabIndex        =   52
         Top             =   900
         Width           =   975
      End
      Begin VB.Label lblLineaProduccion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "L�nea de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3360
         TabIndex        =   51
         Top             =   4710
         Width           =   1875
      End
      Begin VB.Label lblLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Lote"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   49
         Top             =   4740
         Visible         =   0   'False
         Width           =   360
      End
      Begin VB.Label lblDoc 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   7680
         TabIndex        =   45
         Top             =   885
         Visible         =   0   'False
         Width           =   1725
      End
      Begin VB.Label lbl_fecha2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Y"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   6240
         TabIndex        =   44
         Top             =   345
         Width           =   105
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   " Criterios de Busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   71
         Left            =   0
         TabIndex        =   39
         Top             =   0
         Width           =   1935
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2040
         X2              =   9150
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Orden N�"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   38
         Top             =   885
         Width           =   1155
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Usuario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   37
         Top             =   2505
         Width           =   1005
      End
      Begin VB.Label lbl_comprador 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3645
         TabIndex        =   36
         Top             =   2505
         Width           =   5805
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   35
         Top             =   2100
         Width           =   1110
      End
      Begin VB.Label lbl_producto 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3645
         TabIndex        =   34
         Top             =   2100
         Width           =   5805
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Sucursal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   33
         Top             =   1290
         Width           =   1095
      End
      Begin VB.Label lbl_sucursal 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3645
         TabIndex        =   32
         Top             =   1290
         Width           =   5805
      End
      Begin VB.Label lbl_dep_orig 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3645
         TabIndex        =   31
         Top             =   1695
         Width           =   5805
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Modo"
         ForeColor       =   &H80000002&
         Height          =   195
         Left            =   9900
         TabIndex        =   25
         Top             =   2535
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   24
         Top             =   1695
         Width           =   1095
      End
      Begin VB.Label lbl_fecha1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Entre"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   120
         TabIndex        =   23
         Top             =   405
         Width           =   810
      End
   End
   Begin MSComDlg.CommonDialog rutaXml 
      Left            =   4740
      Top             =   6960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "REP_ORDENES"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim FormaCargada As Boolean
Dim Criterio As String, Def_Moneda As String, Cot_Moneda As Double, std_monmod As Double
Dim CostoActivo As String

Private ManejaLote                              As Boolean

Private mListaFormulas                  As Boolean
Private mListaOrdenes                   As Boolean
Private mDocReimprimir                  As String
Private mInternalDocPreview             As Boolean
Private mLocReimprimir                  As String
Private mOpcionNumero                   As Integer

Dim mClsGrupos As New ClsGrupos

Public Property Let ListadoDeFormulas(ByVal pValor As Boolean)
    mListaFormulas = pValor
End Property

Public Property Let ListadoDeOrdenes(ByVal pValor As Boolean)
    mListaOrdenes = pValor
End Property

Public Property Let ReimpresionDocumento(ByVal pValor As String)
    mDocReimprimir = pValor
End Property

Public Property Let ReimpresionLocalidad(ByVal pValor As String)
    mLocReimprimir = pValor
End Property

Public Property Let OpcionNumero(ByVal pValor As Integer)
    mOpcionNumero = pValor
End Property

Public Sub PreviewDoc()
    FormaCargada = False
    mInternalDocPreview = True
    Form_Activate
End Sub

Private Sub aceptar_Click(Index As Integer)
    
    If ManejaLote Then
        txtLote.Text = QuitarComillasSimples(txtLote.Text)
    End If
    
    If chk_ResumenDia.Value = vbChecked Then
        Call ResumenDia_Ordenes(Index)
    Else
        If mListaFormulas Then
            Call Reimprimir_Formulas("DCO", Empty, Index)
        ElseIf mListaOrdenes Then
            Call Reimprimir_Ordenes(Index)
        Else
            Call Reimprimir_Producciones("DCO", Empty, Index)
        End If
    End If
    
    RefreshForm Me
    
End Sub

Private Sub Add_Click()
    If Ordenar.ListIndex >= 0 Then
        seleccionados.AddItem Ordenar.List(Ordenar.ListIndex)
        Ordenar.RemoveItem Ordenar.ListIndex
    End If
End Sub

Private Sub Borrar_Click()
    While seleccionados.ListCount > 0
        Resp = seleccionados.ListCount
        Ordenar.AddItem seleccionados.List(seleccionados.ListCount - 1)
        seleccionados.RemoveItem (seleccionados.ListCount - 1)
    Wend
End Sub

Private Sub CboLnP_Click()
    
    Static PosAnt As Long
    
    mClsGrupos.cTipoGrupo = "LNP"
    
    If PosAnt <> CboLnP.ListIndex Or CboLnP.ListCount = 1 Then
        PosAnt = CboLnP.ListIndex
        If CboLnP.ListIndex = CboLnP.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboLnP
            mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
            ListRemoveItems CboLnP, "Ninguno"
        End If
    End If
    
End Sub

Private Sub CboTurno_Click()
    
    Static PosAnt As Long
    
    mClsGrupos.cTipoGrupo = "TNP"
    
    If PosAnt <> CboTurno.ListIndex Or CboTurno.ListCount = 1 Then
        PosAnt = CboTurno.ListIndex
        If CboTurno.ListIndex = CboTurno.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboTurno
            mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
            ListRemoveItems CboTurno, "Ninguno"
        End If
    End If
    
End Sub

Private Sub chk_ResumenDia_Click()
    If chk_ResumenDia.Value = vbChecked Then
        txt_orden.Enabled = False
        txt_orden.Text = Empty
        txt_orden.Tag = Empty
        'CmdDocumentos.Visible = True
        txt_comprador.Enabled = False
        cmd_comprador.Enabled = False
        Frame1.Enabled = False
        Borrar_Click
    Else
        txt_orden.Enabled = True
        'CmdDocumentos.Visible = False
        txt_comprador.Enabled = True
        cmd_comprador.Enabled = True
        Frame1.Visible = True
    End If
End Sub

Private Sub ChkFechaEstimada_Click()
    If ChkFechaEstimada = 1 Then
        Fechalow.Enabled = False
        Fechahigh.Enabled = False
        DTFechaEstimadaIni.Enabled = True
        DTFechaEstimadaFin.Enabled = True
    Else
        Fechalow.Enabled = True
        Fechahigh.Enabled = True
        DTFechaEstimadaIni.Enabled = False
        DTFechaEstimadaFin.Enabled = False
    End If
End Sub

Private Sub cmd_comprador_Click()
    Tecla_Pulsada = True
    txt_comprador.SetFocus
    Call txt_comprador_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub cmd_Deporig_Click()
    Tecla_Pulsada = True
    origen.SetFocus
    Call origen_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub cmd_moneda_Click()
    Tecla_Pulsada = True
    txt_moneda.SetFocus
    Call txt_moneda_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub cmd_producto_Click()
    Tecla_Pulsada = True
    txt_producto.SetFocus
    Call txt_producto_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub cmd_sucursal_Click()
    Tecla_Pulsada = True
    txt_sucursal.SetFocus
    Call txt_sucursal_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub CmdDocumentos_Click()
    
    Dim SQL As String
    
    If mListaFormulas Then
        CmdFormula_Click
        Exit Sub
    End If
    
    If chk_ResumenDia.Value = vbChecked Then
        
        Dim Frm_Super_Consultas
        Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
        
        With Frm_Super_Consultas
            
            SQL = "SELECT * FROM (SELECT (CAST(CAST(d_Fecha AS DATE) AS DATETIME) + CAST(CAST(ds_HoraFinal AS TIME) AS DATETIME)) AS Fecha, " & _
            "c_Documento, c_Dep_Orig, PRD.c_Descripcion, DEP.c_Descripcion AS Deposito " & _
            "FROM MA_INVENTARIO PRD INNER JOIN MA_DEPOSITO DEP " & _
            "ON PRD.c_Dep_Orig = DEP.c_CodDeposito " & _
            "AND PRD.c_CodLocalidad = DEP.c_CodLocalidad " & _
            "WHERE c_Concepto = 'PRD' " & _
            "AND NOT LEFT(c_Documento, 1) IN ('E') " & _
            "AND CAST(d_Fecha AS DATE) BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "') TB WHERE 1 = 1 "
            
            .Inicializar SQL, StellarMensaje(2802), Ent.BDD, , True
            
            .Add_ItemLabels StellarMensaje(15500), "Fecha", 3660, 0, , Array("Fecha", "VbGeneralDate")
            .Add_ItemSearching StellarMensaje(15500), "Fecha"
            
            .Add_ItemLabels StellarMensaje(10016), "c_Documento", 2020, 0
            .Add_ItemSearching StellarMensaje(10016), "c_Documento"
            
            .Add_ItemLabels StellarMensaje(7057), "Deposito", 2900, 0
            .Add_ItemSearching StellarMensaje(7057), "Deposito"
            
            .Add_ItemLabels StellarMensaje(1091), "c_Descripcion", 2525, 0
            .Add_ItemSearching StellarMensaje(1091), "c_Descripcion"
            
            .StrOrderBy = "Fecha"
            
            .BusquedaInstantanea = True
            .DespliegueInstantaneo_RetornarCualquierValor = False
            .ModoDespliegueInstantaneo = True
            
            .Show vbModal
            
            Set Arr_Documentos = .ArrResultado
            
            If Arr_Documentos Is Nothing Then
                txt_orden.Text = 0
                txt_orden.Tag = Empty
            Else
                
                Dim ListaStr As String
                
                For Each Item In Arr_Documentos
                    ListaStr = ListaStr & "," & "'" & Item(1) & "'"
                Next
                
                ListaStr = Mid(ListaStr, 2)
                
                txt_orden.Text = Arr_Documentos.Count
                txt_orden.Tag = ListaStr
                
            End If
            
            Set Frm_Super_Consultas = Nothing
            
        End With
        
    Else
        
        If mListaOrdenes Then
            
            FrmAppLink.EnableTmpDLLProduccion1 = gCls
            Me.Tag = "ORDEN_PRODUCCION"
            'REIMPRIMIR
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, _
            Empty, _
            False, False, , , "c_CodLocalidad")
            FrmAppLink.EnableTmpDLLProduccion1 = Nothing
            
        Else
            
            FrmAppLink.EnableTmpDLLProduccion1 = gCls
            Me.Tag = "PRODUCCION"
            'REIMPRIMIR
            Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", "PRD", "DCO", False, False)
            FrmAppLink.EnableTmpDLLProduccion1 = Nothing
            
        End If
        
    End If
    
End Sub

Private Sub CmdFormula_Click()
    Set Forma = Me
    Set Campo_Txt = txtFormula
    Set Campo_Lbl = lblDoc
    Call MAKE_VIEW("ma_produccion", "c_formula", "c_Descripcion", StellarMensaje(2818), FRM_PRODUCCION_FORMULA_1, "GENERICO", , Campo_Txt, Campo_Lbl)
    If mListaFormulas Then
        txt_orden.Text = txtFormula.Text
    End If
    Exit Sub
End Sub

Private Sub CmdOpenXML_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then ' Abrir con Excel
        On Error Resume Next
        ShellEx Me.hWnd, "Open", "EXCEL", """" & rutaXml.FileName & """", vbNullString, 1
    ElseIf Button = vbMiddleButton Then ' Abrir con la App XML Handler / por defecto.
        On Error Resume Next
        ShellEx Me.hWnd, "Open", """" & rutaXml.FileName & """", vbNullString, vbNullString, 1
    ElseIf Button = vbRightButton Then ' Ocultar
        CmdOpenXML.Visible = False
    End If
End Sub

Private Sub Eliminar_Click()
    If seleccionados.ListIndex >= 0 Then
        Ordenar.AddItem seleccionados.List(seleccionados.ListIndex)
        seleccionados.RemoveItem seleccionados.ListIndex
    End If
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    
    C�digo(71).Caption = Stellar_Mensaje(57) ' criterio de busqueda
    lbl_fecha1.Caption = Stellar_Mensaje(10032) ' entre
    lbl_fecha2.Caption = Stellar_Mensaje(3046) ' hasta
    Label3.Caption = Stellar_Mensaje(2953) ' produccion nro
    Label4.Caption = Stellar_Mensaje(212) 'sucursal
    Label7.Caption = Stellar_Mensaje(2569) 'deposito
    Label9.Caption = Stellar_Mensaje(10017) 'producto
    Label5.Caption = Stellar_Mensaje(11) ' usuario
    C�digo(3).Caption = Stellar_Mensaje(10006) ' criterio de ordenamiento
    Aceptar(0).Caption = Stellar_Mensaje(10013) ' pantalla
    Aceptar(1).Caption = Stellar_Mensaje(10014) ' impresora
    cmd_salir.Caption = Stellar_Mensaje(54) 'salir
    lbl_fecha1.Caption = Stellar_Mensaje(3045) '
    lbl_fecha2.Caption = Stellar_Mensaje(3046) '
    lblFechaEstimadaIni.Caption = Stellar_Mensaje(3045) '
    lblFechaEstimadaFin.Caption = Stellar_Mensaje(3046) '
    
    lblDpto.Caption = StellarMensaje(3028) '"Departamento"
    lblGrupo.Caption = StellarMensaje(161) '"Grupo"
    lblSubgrupo.Caption = StellarMensaje(3029) '"SubGrupo"
    
    'CANT = Me.Width / (Len(Titulo) + Len(Stellar))
    'CANT = CANT / 4
    
    Label14.Caption = Titulo
    Fechalow.Value = Format(Now, "Short date")
    Fechahigh.Value = Format(Now, "Short date")
    
    Ordenar.AddItem "" & StellarMensaje(10016) & " (ASC)"
    Ordenar.AddItem "" & StellarMensaje(10016) & " (DESC)"
    Ordenar.AddItem "" & StellarMensaje(15500) & " (ASC)"
    Ordenar.AddItem "" & StellarMensaje(15500) & " (DESC)"
    
    Modalidad.AddItem "Resumido"
    Modalidad.AddItem "Detallado"
    Modalidad.ListIndex = 0
    
    lblLineaProduccion.Caption = StellarMensaje(2947) ' LineaProduccion
    mClsGrupos.cTipoGrupo = "LNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
    ListRemoveItems CboLnP, "Ninguno"
    
    lblTurno.Caption = StellarMensaje(2966) ' Turno de Produccion
    mClsGrupos.cTipoGrupo = "TNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
    ListRemoveItems CboTurno, "Ninguno"
    
    chk_ResumenDia.Caption = StellarMensaje(2842) ' Consumo por D�a
    ' Carga de Reglas Comerciales para el c�lculo del costo de producci�n.
    
    ManejaLote = FrmAppLink.PRDManejaLote
    
    If ManejaLote And Not mListaFormulas Then
        lblLote.Visible = True
        txtLote.Visible = True
    End If
    
    Ent.RsReglasComerciales.Requery
    
    Select Case Ent.RsReglasComerciales!Estimacion_Inv
        Case 0
            CostoActivo = "n_CostoAct"
        Case 1
            CostoActivo = "n_CostoAnt"
        Case 2
            CostoActivo = "n_CostoPro"
        Case 3
            CostoActivo = "n_CostoRep"
    End Select
    
    If FrmAppLink.PRDCostoActivo <> Empty Then CostoActivo = FrmAppLink.PRDCostoActivo
    
    If Not FrmAppLink.InventarioCampoIDLote Then
        lblLote.Visible = False
        txtLote.Visible = False
    End If
    
    If Not FrmAppLink.InventarioCampoLineaProduccion Then
        lblLineaProduccion.Visible = False
        CboLnP.Visible = False
    End If
    
    If Not FrmAppLink.InventarioCampoTurnoProduccion Then
        lblTurno.Visible = False
        CboTurno.Visible = False
    End If
    
    CmdDocumentos.Visible = True
    
    If mListaFormulas Then
        lblFormula.Visible = False
        txtFormula.Visible = False
        CmdFormula.Visible = False
        Label3.Caption = StellarMensaje(5003) ' Formula
        lbl_fecha1.Visible = False
        lbl_fecha2.Visible = False
        Fechalow.Visible = False
        Fechahigh.Visible = False
        Label5.Enabled = False
        origen.Enabled = False
        cmd_Deporig.Enabled = False
        lbl_dep_orig.Enabled = False
        chk_ResumenDia.Visible = False
        lblLineaProduccion.Visible = False
        CboLnP.Visible = False
        lblTurno.Visible = False
        CboTurno.Visible = False
        ChkPrdAdv.Visible = False
    End If
    
    If mListaOrdenes Then
        
        chk_ResumenDia.Visible = False
        Label7.Enabled = False
        origen.Enabled = False
        cmd_Deporig.Enabled = False
        lbl_dep_orig.Enabled = False
        Label3.Caption = StellarMensaje(10126) ' Orden No
        lblStatus.Caption = StellarMensaje(2019)
        ChkFechaEstimada.Caption = StellarMensaje(2954)
        
        Dim Aumento: Aumento = 300
        
        Me.Height = Me.Height + Aumento
        FrameCriterios.Height = FrameCriterios.Height + Aumento
        Frame1.Top = Frame1.Top + Aumento
        
        'For i = 0 To UBound(Aceptar)
            'Aceptar(i).Top = Aceptar(i).Top + Aumento
        'Next
        
        Aceptar(0).Top = Aceptar(0).Top + Aumento
        Aceptar(1).Top = Aceptar(1).Top + Aumento
        Aceptar(2).Top = Aceptar(2).Top + Aumento
        
        CmdOpenXML.Top = CmdOpenXML.Top + Aumento
        cmd_salir.Top = cmd_salir.Top + Aumento
        
        CmbStatus.Clear
        CmbStatus.AddItem StellarMensaje(2545)
        CmbStatus.AddItem StellarMensaje(2758)
        CmbStatus.AddItem StellarMensaje(10)
        CmbStatus.AddItem StellarMensaje(79)
        CmbStatus.AddItem StellarMensaje(109)
        CmbStatus.ListIndex = 0
        
        lblStatus.Top = 5300
        lblStatus.Left = lblLote.Left
        CmbStatus.Left = txtLote.Left
        CmbStatus.Top = lblStatus.Top
        lblStatus.Visible = True
        CmbStatus.Visible = True
        
        'lblLineaProduccion.Top = lblStatus.Top
        lblLineaProduccion.Left = lblLineaProduccion.Left + 450
        'CboLnP.Top = lblStatus.Top
        CboLnP.Left = CboLnP.Left + 1000
        
        lblTurno.Top = lblStatus.Top
        lblTurno.Left = lblTurno.Left + 450
        CboTurno.Top = lblStatus.Top
        CboTurno.Left = CboTurno.Left + 1000
        
        ChkFechaEstimada.Top = 6000
        ChkFechaEstimada.Left = lblStatus.Left
        ChkFechaEstimada.Visible = True
        
        lblFechaEstimadaIni.Left = 3000
        lblFechaEstimadaIni.Top = ChkFechaEstimada.Top
        lblFechaEstimadaIni.Visible = True
        
        DTFechaEstimadaIni.Left = 3700
        DTFechaEstimadaIni.Top = ChkFechaEstimada.Top - 50
        DTFechaEstimadaIni.Visible = True
        DTFechaEstimadaIni.Value = Date
        
        lblFechaEstimadaFin.Left = 6500
        lblFechaEstimadaFin.Top = ChkFechaEstimada.Top
        lblFechaEstimadaFin.Visible = True
        
        DTFechaEstimadaFin.Left = 7200
        DTFechaEstimadaFin.Top = ChkFechaEstimada.Top - 50
        DTFechaEstimadaFin.Visible = True
        DTFechaEstimadaFin.Value = Date
        
        ChkPrdAdv.Visible = False
        
    End If
    
    AjustarPantalla Me
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        If mLocReimprimir <> Empty Then
            txt_sucursal.Text = mLocReimprimir
            txt_sucursal_LostFocus
        End If
        
        If mDocReimprimir <> Empty Then
            
            txt_orden.Text = mDocReimprimir
            txt_orden.Tag = txt_orden.Text
            txtFormula.Text = Empty
            Fechalow.Value = DateSerial(1900, 1, 1)
            Fechahigh.Value = DateSerial(9999, 1, 1)
            txt_sucursal.Text = Empty
            origen.Text = Empty
            txt_producto.Text = Empty
            txt_comprador.Text = Empty
            departamento.Text = Empty
            grupo.Text = Empty
            subgrupo.Text = Empty
            txtLote.Text = Empty
            ListSafeItemSelection CboLnP, Empty
            ListSafeItemSelection CboTurno, Empty
            
            Call aceptar_Click(mOpcionNumero)
            
            If Not mInternalDocPreview Then
                Unload Me
            Else
                mInternalDocPreview = False
            End If
            
            Exit Sub
            
        End If
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            oTeclado.Key_Tab
            KeyAscii = 0
        Case vbKeyF12
            Call cmd_salir_Click
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set REP_ORDENES = Nothing
End Sub

Private Sub Ordenar_DblClick()
    Call Add_Click
End Sub

Private Sub Ordenar_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyInsert
            Call Add_Click
        Case vbKeyBack
            Call Borrar_Click
    End Select
End Sub

Private Sub origen_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Dim XTITULO As String
            Tabla = "ma_deposito"
            XTITULO = " D E P O S I T O  D E  O R I G E N"
            Set Forma = Me
            Set Campo_Txt = origen
            Set Campo_Lbl = lbl_dep_orig
            Call MAKE_VIEW("ma_deposito", "c_CodDeposito", "c_Descripcion", XTITULO, Me, "GENERICO", , Campo_Txt, Campo_Lbl) 'LST_TRA_TRS
    End Select
End Sub

Private Sub origen_LostFocus()
    Dim RsDeposito As New ADODB.Recordset
    If Trim(origen.Text) <> "" Then
        Call Apertura_Recordset(RsDeposito)
        RsDeposito.Open "select c_Descripcion from ma_deposito where c_CodDeposito = '" & origen.Text & "'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If Not RsDeposito.EOF Then
            lbl_dep_orig.Caption = RsDeposito!c_Descripcion
        Else
            lbl_dep_orig.Caption = ""
            origen.Text = ""
            Call Mensaje(True, "No existe el deposito.")
        End If
        Call Cerrar_Recordset(RsDeposito)
    Else
        lbl_dep_orig.Caption = ""
        origen.Text = ""
    End If
End Sub

Private Sub seleccionados_DblClick()
    Call Eliminar_Click
End Sub

Private Sub seleccionados_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyDelete
            Call Eliminar_Click
        Case vbKeyBack
            Call Borrar_Click
    End Select
End Sub

Private Sub txt_comprador_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Tecla_Pulsada = True
            Set Forma = Me
            Set Campo_Txt = txt_comprador
            Set Campo_Lbl = lbl_comprador
            Call MAKE_VIEW("ma_usuarios", "codusuario", "descripcion", "U S U A R I O S", Me, "GENERICO", , Campo_Txt, Campo_Lbl)
    End Select
End Sub

Private Sub txt_comprador_LostFocus()
    Dim RsUsuario As New ADODB.Recordset
    If Trim(txt_comprador.Text) <> "" Then
        RsUsuario.Open "select * from ma_usuarios where codusuario = '" & Trim(txt_comprador.Text) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If Not RsUsuario.EOF Then
            lbl_comprador.Caption = RsUsuario!Descripcion
        Else
            lbl_comprador.Caption = ""
            txt_comprador.Text = ""
            Call Mensaje(True, "No existe el usuario en el Sistema.")
        End If
    Else
        lbl_comprador.Caption = ""
        txt_comprador.Text = ""
    End If
End Sub

Private Sub txt_moneda_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Tecla_Pulsada = True
            Set Campo_Txt = txt_moneda
            Set Campo_Lbl = lbl_moneda
            Call MAKE_VIEW("MA_MONEDAS", "c_CodMoneda", "c_Descripcion", "M O N E D A S", Me, "GENERICO", , Campo_Txt, Campo_Lbl)
            Tecla_Pulsada = False
    End Select
End Sub

Private Sub txt_moneda_LostFocus()
    Dim rsMonedas As New ADODB.Recordset
    Call Apertura_RecordsetC(rsMonedas)
    rsMonedas.Open "SELECT * FROM MA_MONEDAS WHERE b_Activa = 1 AND c_CodMoneda = '" & Trim(txt_moneda.Text) & "'", _
    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    If Not rsMonedas.EOF Then
        lbl_moneda.Caption = rsMonedas!c_Descripcion
        Cot_Moneda = rsMonedas!n_Factor
        std_monmod = rsMonedas!n_Decimales
    Else
        txt_moneda.Text = Def_Moneda
        Call txt_moneda_LostFocus
    End If
    rsMonedas.Close
End Sub

Private Sub txt_producto_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Tecla_Pulsada = True
            Set Forma = Me
            Set Campo_Txt = txt_producto
            Set Campo_Lbl = lbl_producto
            Call MAKE_VIEW("ma_productos", "c_Codigo", "c_Descri", "P R O D U C T O S", Me, "GENERICO", , Campo_Txt, Campo_Lbl)
    End Select
End Sub

Private Sub txt_producto_LostFocus()
    Dim cproducto As String, RsProducto As New ADODB.Recordset
    Dim cDepGruSub As String
    
    If Trim(txt_producto.Text) = "" Then
        lbl_producto.Caption = ""
        Exit Sub
    End If
    
    cproducto = "select * from ma_productos where ma_productos.c_Codigo = '" & Trim(txt_producto.Text) & "' "
    Call Apertura_Recordset(RsProducto)
    RsProducto.Open cproducto, Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    If Not RsProducto.EOF Then
        lbl_producto.Caption = RsProducto!c_Descri
    Else
        lbl_producto.Caption = ""
        Call Mensaje(True, "Producto no Existe en el Sistema o No tiene ese proveedor asignado .")
        txt_producto.Text = ""
    End If
    Call Cerrar_Recordset(RsProducto)
End Sub

Private Sub txt_sucursal_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Tecla_Pulsada = True
            Set Forma = Me
            Set Campo_Txt = txt_sucursal
            Set Campo_Lbl = lbl_sucursal
            Call MAKE_VIEW("ma_sucursales", "c_Codigo", "c_Descripcion", _
            UCase(Stellar_Mensaje(1254)), Me, "GENERICO", , txt_sucursal, lbl_sucursal) '"S U C U R S A L E S"
    End Select
End Sub

Private Sub txt_sucursal_LostFocus()
    Dim RsSucursal As New ADODB.Recordset
    If Trim(txt_sucursal.Text) <> "" Then
        Call Apertura_Recordset(RsSucursal)
        RsSucursal.Open "Select * from ma_sucursales where c_Codigo = '" & Trim(txt_sucursal.Text) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If Not RsSucursal.EOF Then
            lbl_sucursal.Caption = RsSucursal!c_Descripcion
        Else
            Call Mensaje(True, "No existe la Sucursal en el sistema.")
            lbl_sucursal.Caption = ""
            txt_sucursal.Text = ""
        End If
        RsSucursal.Close
    Else
        lbl_sucursal.Caption = ""
        txt_sucursal.Text = ""
    End If
End Sub

Sub Escribir_DTR_Producciones(RsTemp As ADODB.Recordset, Reporte As Object, Seccion As String, Objeto As String)
    
    Dim RsTrInventarioCargos As New ADODB.Recordset, RsTrInventarioDescargos As New ADODB.Recordset
    Dim CostosMerma As Double, CostosProduccion As Double
    
    Escribir_Txt = False
    
    If Not RsTemp.EOF Then
        
        While Not RsTemp.EOF
            
            Set RsTrInventarioCargos = RsTemp("TRCARGOS").Value
            Set RsTrInventarioDescargos = RsTemp("TRDESCARGOS").Value
            
            'Escribir "PRODUCCI�N No " & Space(10) & rsTemp!c_Documento, Reporte, Seccion, Objeto
            Escribir UCase(StellarMensaje(2953)) & " " & Space(10) & RsTemp!c_Documento, Reporte, Seccion, Objeto
            'Escribir "DEPOSITO: " & rsTemp!c_Dep_Dest & vbNewLine & "DESCRIPCI�N: " & rsTemp!destino, Reporte, Seccion, Objeto
            Escribir "" & UCase(StellarMensaje(7057)) & ": " & RsTemp!c_Dep_Dest & vbNewLine & "" & UCase(StellarMensaje(143)) & ": " & RsTemp!destino, Reporte, Seccion, Objeto
            If Trim(RsTemp!LineaProduccion) <> Empty Then Escribir UCase(StellarMensaje(2947)) & ":" & " " & RsTemp!LineaProduccion, Reporte, Seccion, Objeto
            If Trim(RsTemp!TurnoProduccion) <> Empty Then Escribir UCase(StellarMensaje(2966)) & ":" & " " & RsTemp!TurnoProduccion, Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "PRODUCTOS A UTILIZAR", Reporte, Seccion, Objeto
            Escribir "" & UCase(StellarMensaje(5006)) & "", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir LeftJust("Producto", 18) & " " & LeftJust("Descripci�n", 32) & " " & RightJust("Cantidad", 14) & " " & LeftJust("Presentaci�n", 15) & " " & RightJust("Costo Unit.", 16) & " " & RightJust("Subtotal", 16), Reporte, Seccion, Objeto
            Escribir LeftJust("" & StellarMensaje(5010) & "", 18) & " " & LeftJust("" & StellarMensaje(143) & "", 32) & " " & RightJust("" & StellarMensaje(3001) & "", 14) & " " & LeftJust("" & FrmAppLink.PRDLabelCampoPresenta & "", 15) & " " & RightJust("" & StellarMensaje(151) & "", 16) & " " & RightJust("" & StellarMensaje(139) & "", 16), Reporte, Seccion, Objeto
            
            CostosMerma = 0
            CostosProduccion = 0
            
            While Not RsTrInventarioDescargos.EOF
                Escribir LeftJust(RsTrInventarioDescargos!c_CodArticulo, 18) & " " & LeftJust(Mid(RsTrInventarioDescargos!c_Descri, 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Cantidad, RsTrInventarioDescargos!Cant_Decimales), 14) & " " & LeftJust(RsTrInventarioDescargos!Presentacion, 15) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Costo, Std_Decm), 16) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Subtotal, Std_Decm), 16), Reporte, Seccion, Objeto
                CostosProduccion = CostosProduccion + (RsTrInventarioDescargos!n_Cantidad * RsTrInventarioDescargos!n_Precio)
                RsTrInventarioDescargos.MoveNext
            Wend
            
            'IMPRESION DE LOS MONTO POR MERMA Y PRODUCCION
            
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "." & Space(20) & "  Monto de Producci�n: " & RightJust(FormatNumber(CostosProduccion + rsTemp!n_Cantidad_compra, Std_Decm), 16) & " (No Incluye Costos Directos de Producci�n)", Reporte, Seccion, Objeto
            Escribir "." & Space(20) & "  " & StellarMensaje(2835) & ": " & RightJust(FormatNumber(CostosProduccion + RsTemp!n_Cantidad_compra, Std_Decm), 16) & " (" & StellarMensaje(2836) & ")", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "PRODUCTOS A PRODUCIR", Reporte, Seccion, Objeto
            Escribir "" & UCase(StellarMensaje(5008)) & "", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir LeftJust("Producto", 18) & " " & LeftJust("Descripci�n", 32) & " " & RightJust("Cantidad", 14) & " " & LeftJust("Presentaci�n", 15) & " " & RightJust("Costo Unit.", 16) & " " & RightJust("Subtotal", 16), Reporte, Seccion, Objeto
            Escribir LeftJust("" & StellarMensaje(5010) & "", 18) & " " & LeftJust("" & StellarMensaje(143) & "", 32) & " " & RightJust("" & StellarMensaje(3001) & "", 14) & " " & LeftJust("" & FrmAppLink.PRDLabelCampoPresenta & "", 15) & " " & RightJust("" & StellarMensaje(151) & "", 16) & " " & RightJust("" & StellarMensaje(139) & "", 16), Reporte, Seccion, Objeto
            
            While Not RsTrInventarioCargos.EOF
                'Escribir LeftJust(RsTrInventarioCargos!c_CodArticulo, 18) & " " & LeftJust(Mid(RsTrInventarioCargos!c_Descri, 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Cantidad, 2), 14) & " " & LeftJust(RsTrInventarioCargos!Presentacion, 15) & RightJust(FormatNumber(RsTrInventarioCargos!n_Precio, Std_Decm), 16), Reporte, Seccion, Objeto
                Escribir LeftJust(RsTrInventarioCargos!c_CodArticulo, 18) & " " & LeftJust(Mid(isDBNull(RsTrInventarioCargos!c_Descri, "N/A"), 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Cantidad, isDBNull(RsTrInventarioCargos!Cant_Decimales, 0)), 14) & " " & LeftJust(isDBNull(RsTrInventarioCargos!Presentacion, "N/A"), 15) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Costo, Std_Decm), 16) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Subtotal, Std_Decm), 16), Reporte, Seccion, Objeto
                RsTrInventarioCargos.MoveNext
            Wend
            
            Escribir "", Reporte, Seccion, Objeto
            Escribir StellarMensaje(2611) & ": " & RsTemp!c_Relacion, Reporte, Seccion, Objeto
            Escribir StellarMensaje(137) & " " & RsTemp!c_Observacion, Reporte, Seccion, Objeto
            
            Escribir ".", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            Escribir_Txt = True
            
            RsTemp.MoveNext
            
        Wend
        
        RsTemp.MoveFirst
        
    End If
    
End Sub

Function Center_Text(Texto As String, Ancho As Integer) As String
    Dim Lng As Integer
    Lng = Round((Ancho - Len(Texto)) / 2)
    If Lng <= 0 Then
        Center_Text = Texto
    Else
        Center_Text = Space(Lng) & Texto & Space(Lng)
        If Len(Center_Text) < Ancho Then
            Center_Text = Space(Ancho - Len(Center_Text)) & Center_Text
        End If
    End If
End Function

Function LeftJust(Texto As String, Ancho As Integer)
    Dim Lng As Integer
    Lng = Ancho - Len(Texto)
    If Lng <= 0 Then
        LeftJust = Texto
    Else
        LeftJust = Texto & Space(Lng)
    End If
End Function

Function RightJust(Texto As String, Ancho As Integer)
    Dim Lng As Integer
    Lng = Ancho - Len(Texto)
    If Lng <= 0 Then
        RightJust = Texto
    Else
        RightJust = Space(Lng) & Texto
    End If
End Function

Sub Escribir(Cadena As String, Reporte As Object, Seccion As String, Objeto As String, Optional RC As Boolean = True)
    Reporte.Sections(Seccion).Controls(Objeto).Caption = Reporte.Sections(Seccion).Controls(Objeto).Caption & Cadena & IIf(RC, vbNewLine, "")
End Sub

Sub Reimprimir_Producciones(tipo As String, orden As String, Optional Numero As Integer = 0)
    
    Dim cMaInventario As String, cTrInventarioCargos As String, cTrInventarioDescargos As String
    Dim RsMaInventario As New ADODB.Recordset, RsTrInventarioCargos As New ADODB.Recordset, RsTrInventarioDescargos As New ADODB.Recordset
    Dim Consultas(2) As String, Relacion(2) As String, Alias(2) As String
    Dim RsView As New ADODB.Recordset, OrderBy As String, AuxCriterio As String
    
    If Ent.SHAPE_ADM.State = adStateOpen Then Ent.SHAPE_ADM.Close
    Ent.SHAPE_ADM.ConnectionString = FrmAppLink.CnStrADMShape '"Provider=MSDataShape.1;Persist Security Info=false;Connect Timeout=0;Data Source=" & Srv_Local & ";User ID=sa;Initial Catalog=vad10;Data Provider=" & PROVIDER_LOCAL
    Ent.SHAPE_ADM.Open
    OrderBy = ""
    AuxCriterio = ""
    
    For Cont = 0 To seleccionados.ListCount - 1
        If seleccionados.List(Cont) = "" & StellarMensaje(10016) & " (ASC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "c_Documento"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(10016) & " (DESC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "c_Documento DESC"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(15500) & " (ASC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "d_Fecha"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(15500) & " (DESC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "d_Fecha DESC"
        End If
    Next Cont
    
    Dim mCampoLote As String
    
    mStrLote = StellarMensaje(2937) & " |"
    
    If ManejaLote Then
        
        'mCampoLote = "CASE WHEN (CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & ") > 0 THEN " & _
        "SUBSTRING(c_Descripcion, (CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & "), " & _
        "CASE WHEN CHARINDEX(N'" & mStrLote & "', c_Descripcion) > 0 THEN " & _
        "(CAST(CHARINDEX('|', c_Descripcion, (CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & ")) AS INT) " & _
        "- CAST((CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & ") AS INT)) " & _
        "ELSE 0 END) " & _
        "ELSE '' END"
        
        mCampoLote = "TR_INVENTARIO.c_IDLote"
    Else
        mCampoLote = "''"
    End If
    
    'mLineaProd = "LNP" & " |" ' StellarMensaje()
    
    'mCampoLineaProduccion = "CASE WHEN (CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & ") > 0 THEN " & _
    "SUBSTRING(c_Ejecutor, (CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & "), " & _
    "CASE WHEN CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) > 0 THEN " & _
    "(CAST(CHARINDEX('|', c_Ejecutor, (CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & ")) AS INT) " & _
    "- CAST((CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & ") AS INT)) " & _
    "ELSE 0 END) " & _
    "ELSE '' END"
    
    Dim mCampoLineaProduccion, mCampoTurnoProduccion
    
    If FrmAppLink.InventarioCampoLineaProduccion Then
        mCampoLineaProduccion = "MA_INVENTARIO.c_LineaProduccion"
    Else
        mCampoLineaProduccion = "''"
    End If
    
    If FrmAppLink.InventarioCampoTurnoProduccion Then
        mCampoTurnoProduccion = "MA_INVENTARIO.c_Turno"
    Else
        mCampoTurnoProduccion = "''"
    End If
    
    '& IIf(Trim(OrderBy) = "", "", OrderBy)
    
    Dim mCamposCabPrdAdv, mCamposDetPrdAdv, mCostosDirectos, mJoinCabPrdAdv, mJoinDetPrdAdv
    
    If ChkPrdAdv.Value Then
        
        mJoinCabPrdAdv = " INNER JOIN MA_PRODUCCION_REGISTRO PDREG ON MA_INVENTARIO.c_Documento = PDREG.CodRegistro AND MA_INVENTARIO.c_CodLocalidad = PDREG.CodLocalidad "
        mJoinDetPrdAdv = " INNER JOIN TR_PRODUCCION_REGISTRO TRREG ON TR_INVENTARIO.c_Documento = TRREG.c_Documento AND TR_INVENTARIO.c_CodLocalidad = TRREG.c_CodLocalidad AND TR_INVENTARIO.c_Linea = TRREG.c_Linea AND TR_INVENTARIO.c_CodArticulo = TRREG.c_CodArticulo "
        mCamposCabPrdAdv = ", PDREG.*, CASE WHEN PDREG.CostoTotalProyectado <> 0 THEN ROUND(PDREG.CostoTotalProyectado - PDREG.CostoTotal, 8, 0) ELSE 0 END AS DifTotales, (CAST(CASE WHEN PDREG.CostoTotalProyectado <> 0 THEN ROUND(ROUND(PDREG.CostoTotalProyectado - PDREG.CostoTotal, 8, 0) / PDREG.CostoTotalProyectado, 2, 0) ELSE 0 END AS NVARCHAR(50)) + '%') AS Porc_Variacion_Totales, (CAST(PorcVariacionPesoTotal AS NVARCHAR(50)) + '%') AS Porc_Variacion_Pesos "
        '"CASE WHEN PDREG.PesoTotalTeorico <> 0 THEN ROUND(PDREG.PesoTotalTeorico - PDREG.PesoTotalUtilizado, 8, 0) ELSE 0 END AS DifPesosTotales, CAST(CASE WHEN PDREG.PesoTotalTeorico <> 0 THEN ROUND(ROUND(PDREG.PesoTotalTeorico - PDREG.PesoTotalUtilizado, 8, 0) / PDREG.PesoTotalTeorico, 2, 0) ELSE 0 END) AS NVARCHAR(50) + '%' AS Porc_Variacion_Pesos, " & _
        ""
        mCamposDetPrdAdv = ", n_CantidadNeta, n_MontoMerma, n_CantidadProyectada, ROUND(TRREG.n_Costo * (1 + (n_PorcMerma / 100)), 8, 0) AS CostoUniNeto, ROUND(((TRREG.n_Cantidad * (1 + (n_PorcMerma / 100))) * TRREG.n_Costo), 8, 0) AS CostoProdNeto, CASE WHEN TipoItem = 1 THEN 'INGREDIENTE' WHEN TipoItem = 2 THEN 'EMPAQUE' ELSE 'N/A' END AS DescTipoItem, CASE WHEN TipoItem = 1 THEN CONVERT(NVARCHAR(50), CAST(TotalCantidadNetaPorTipoItem AS MONEY), 1) ELSE '' END AS CantidadTotalIngredientes, CASE WHEN TipoItem = 1 THEN CONVERT(NVARCHAR(50), CAST(TotalCostoPorTipoItem AS MONEY), 1) ELSE '' END AS CostoTotalIngredientes, " & _
        "CASE WHEN TipoItem = 2 THEN CONVERT(NVARCHAR(50), CAST(TotalCantidadNetaPorTipoItem AS MONEY), 1) ELSE '' END AS CantidadTotalEmpaques, CASE WHEN TipoItem = 2 THEN CONVERT(NVARCHAR(50), CAST(TotalCostoPorTipoItem AS MONEY), 1) ELSE '' END AS CostoTotalEmpaques, CONVERT(NVARCHAR(50), CAST(TotalCostoEnItems AS MONEY), 1) AS TotalCostoMateriales, " & _
        "(CAST(CASE WHEN TotalCostoPorTipoItem <> 0 THEN ROUND(ROUND(n_CantidadNeta * TRREG.n_Costo, 8, 0) / TotalCostoPorTipoItem * 100, 2, 0) ELSE '0' END AS NVARCHAR(50)) + '%') AS PorcCostoTipoItem, (CAST(CASE WHEN TotalCostoEnItems <> 0 THEN ROUND(ROUND(n_CantidadNeta * TRREG.n_Costo, 8, 0) / TotalCostoEnItems * 100, 2, 0) ELSE '0' END AS NVARCHAR(50)) + '%') AS PorcCostoTotal"
        
    End If
    
    cMaInventario = "SELECT " & mCampoLineaProduccion & " AS LineaProduccionINV, " & mCampoTurnoProduccion & " AS TurnoProduccionINV, (isNULL((SELECT SUM(n_Subtotal) FROM TR_INVENTARIO WHERE c_Documento = MA_INVENTARIO.c_Documento AND c_Concepto = 'PRD' AND c_TipoMov = 'Descargo'), 0) + n_Cantidad_Compra) AS n_TotalProduccion, isNULL(MA_SUCURSALES.c_Descripcion, 'N/A') AS Localidad, " & _
    "MA_INVENTARIO.*, isNULL(MA_MONEDAS.c_Descripcion, 'N/A') AS C_DESMONEDA, isNULL(ORIGEN.c_Descripcion, 'N/A') AS ORIGEN, " & _
    "isNULL(DESTINO.c_Descripcion, 'N/A') AS DESTINO, isNULL(MA_USUARIOS.Descripcion, 'N/A') AS Usuario" & mCamposCabPrdAdv & " " & _
    "FROM MA_INVENTARIO " & mJoinCabPrdAdv & " LEFT JOIN MA_MONEDAS ON MA_MONEDAS.c_CodMoneda = MA_INVENTARIO.c_CodMoneda LEFT JOIN MA_DEPOSITO AS ORIGEN ON ORIGEN.c_CodDeposito = MA_INVENTARIO.c_Dep_Orig LEFT JOIN MA_DEPOSITO AS DESTINO ON DESTINO.c_CodDeposito = MA_INVENTARIO.c_Dep_Dest LEFT JOIN MA_SUCURSALES ON MA_INVENTARIO.c_CodLocalidad = MA_SUCURSALES.c_Codigo LEFT JOIN MA_USUARIOS ON MA_INVENTARIO.c_CodComprador = MA_USUARIOS.CodUsuario WHERE " & IIf(orden = "", "", "c_Documento = '" & orden & "' AND ") & " c_Concepto = 'PRD' AND c_Status = '" & tipo & "' AND d_Fecha BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' "
    
    cTrInventarioCargos = "SELECT " & mCampoLote & " AS IDLote, TR_INVENTARIO.*, MA_PRODUCTOS.Cant_Decimales, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.c_Presenta AS Presentacion " & _
    "FROM TR_INVENTARIO LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_INVENTARIO.c_CodArticulo WHERE " & IIf(orden = "", "", "c_Documento = '" & orden & "' AND ") & " c_Concepto = 'PRD' AND c_TipoMov = 'Cargo' " & _
    IIf(Trim(txt_producto.Text) = "", "", " AND c_CodArticulo = '" & Trim(txt_producto.Text) & "' ") & _
    IIf(Trim(departamento.Text) = "", "", " AND c_Departamento = '" & Trim(departamento.Text) & "' ") & _
    IIf(Trim(grupo.Text) = "", "", " AND c_Grupo = '" & Trim(grupo.Text) & "' ") & _
    IIf(Trim(subgrupo.Text) = "", "", " AND c_Subgrupo = '" & Trim(subgrupo.Text) & "' ")
    
    cTrInventarioDescargos = "SELECT " & mCampoLote & " AS IDLote, TR_INVENTARIO.*, MA_PRODUCTOS.Cant_Decimales, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.c_Presenta AS Presentacion" & mCamposDetPrdAdv & " " & _
    "FROM TR_INVENTARIO" & mJoinDetPrdAdv & " LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_INVENTARIO.c_CodArticulo " & _
    "WHERE " & IIf(orden = "", "", "c_Documento = '" & orden & "' AND ") & " c_Concepto = 'PRD' AND c_TipoMov = 'Descargo' "
    
    If Trim(txt_orden.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_INVENTARIO.c_Documento = '" & Trim(txt_orden.Text) & "' "
    End If
    
    If Trim(txtFormula.Text) <> Empty Then
        If ChkPrdAdv.Value Then
            AuxCriterio = AuxCriterio & " AND PDREG.CodFormula = '" & Trim(txtFormula.Text) & "' "
        Else
            AuxCriterio = AuxCriterio & " AND CASE WHEN " & _
            "LEFT(MA_INVENTARIO.c_Relacion, 3) = 'OPR' THEN " & _
            "(SELECT TOP 1 c_Formula FROM MA_ORDEN_PRODUCCION " & _
            "WHERE c_Documento = SUBSTRING(MA_INVENTARIO.c_Relacion, 4, 9999)) " & _
            "ELSE MA_INVENTARIO.c_Relacion END = '" & Trim(txtFormula.Text) & "' "
        End If
    End If
    
    If Trim(txt_sucursal.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_INVENTARIO.c_CodLocalidad = '" & Trim(txt_sucursal.Text) & "' "
    End If
    
    If Trim(origen.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_INVENTARIO.c_Dep_Orig = '" & Trim(origen.Text) & "' "
    End If
    
    If Trim(txt_comprador.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_INVENTARIO.c_CodComprador = '" & Trim(txt_comprador.Text) & "' "
    End If
    
    If Trim(txtLote.Text) <> Empty Then
        
        'AuxCriterio = AuxCriterio & " AND (MA_INVENTARIO.c_Concepto + MA_INVENTARIO.c_Documento) IN (" & _
        "SELECT (c_Concepto + c_Documento) FROM TR_INVENTARIO WHERE c_Concepto = 'PRD' " & _
        "AND f_Fecha BETWEEN '" & FechaBD(FechaLow.Value) & "' AND '" & FechaBD(EndOfDay(FechaHigh.Value), FBD_FULL) & "' " & _
        "AND NOT LEFT(c_Documento, 1) IN ('E', 'A') AND c_Descripcion LIKE '%" & txtLote.Text & "%') "
        
        AuxCriterio = AuxCriterio & " AND (MA_INVENTARIO.c_Concepto + MA_INVENTARIO.c_Documento + MA_INVENTARIO.c_CodLocalidad) IN (" & _
        "SELECT (c_Concepto + c_Documento + c_CodLocalidad) FROM TR_INVENTARIO WHERE c_Concepto = 'PRD' " & _
        "AND f_Fecha BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' " & _
        "AND NOT LEFT(c_Documento, 1) IN ('E', 'A') AND (" & mCampoLote & ") LIKE '" & txtLote.Text & "') "
        
    End If
    
    If Trim(CboLnP.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND ((" & mCampoLineaProduccion & ") " & _
        "= '" & QuitarComillasSimples(CboLnP.Text) & "') "
    End If
    
    If Trim(CboTurno.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND ((" & mCampoTurnoProduccion & ") " & _
        "= '" & QuitarComillasSimples(CboTurno.Text) & "') "
    End If
    
    cMaInventario = cMaInventario & AuxCriterio & IIf(Trim(OrderBy) = "", "", "Order by " & OrderBy)
    
    If ChkPrdAdv.Value Then
        cMACostosDir = "SELECT *, ROUND(PCD.MontoProyectado / CantidadProduccion, 4, 0) AS Proyectado_x_Und, " & _
        "ROUND(PCD.MontoReal / CantidadProduccion, 4, 0) AS Monto_x_Und " & _
        "FROM MA_PRODUCCION_REGISTRO_COSTOS_DIRECTOS PCD " & _
        "INNER JOIN MA_INVENTARIO MA " & _
        "ON PCD.CodRegistro = MA.c_Documento " & _
        "AND PCD.CodLocalidad = MA.c_CodLocalidad " & _
        "INNER JOIN MA_PRODUCCION_REGISTRO PRG " & _
        "ON PRG.CodRegistro = MA.c_Documento " & _
        "AND PRG.CodLocalidad = MA.c_CodLocalidad " & _
        "WHERE MA.c_Concepto = 'PRD' " & _
        "AND d_Fecha BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' " & _
        "AND NOT LEFT(c_Documento, 1) IN ('E', 'A')"
    Else
        cMACostosDir = "SELECT '*NULL*' AS c_Documento"
    End If
    
    Consultas(0) = cTrInventarioCargos
    Consultas(1) = cTrInventarioDescargos
    Consultas(2) = cMACostosDir
    
    Relacion(0) = "c_Documento"
    Relacion(1) = "c_Documento"
    Relacion(2) = "c_Documento"
    
    Alias(0) = "TRCARGOS"
    Alias(1) = "TRDESCARGOS"
    Alias(2) = "TRCOSTOSDIR"
    
    Call Apertura_RecordsetC(RsMaInventario)
    
    CCriterio = ShapeStrAdv(cMaInventario, "MA_PRODUCCION", Consultas, Alias, Relacion, Relacion, Alias)
    
    'Debug.Print cTrInventarioCargos
    'Debug.Print cTrInventarioDescargos
    'Debug.Print cMaInventario
    
    RsMaInventario.Open CCriterio, Ent.SHAPE_ADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
    RsView.Open "select top 1 * from ma_productos", Ent.SHAPE_ADM, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not RsMaInventario.EOF
        If RsMaInventario("TRCARGOS").Value.EOF Then
            RsMaInventario.Delete
        End If
        RsMaInventario.MoveNext
    Wend
    
    If Not (RsMaInventario.EOF And RsMaInventario.BOF) Then
        RsMaInventario.MoveFirst
    End If
    
    If Not RsMaInventario.EOF Then
        
        If Not RsMaInventario("TRCARGOS").Value.EOF Then
            
            If Numero = 2 Then
                Call GenerarXML_ListadoProduccion(RsMaInventario)
            Else
                
                Dim Rep_Formulas As Object: Set Rep_Formulas = FrmAppLink.GetDefaultTextDataReport
                
                Call Escribir_DTR_Producciones(RsMaInventario, Rep_Formulas, "MA_PRODUCIR", "LBL_TEXT")
                Call REPO_CABE(Rep_Formulas, "" & StellarMensaje(2949) & "")

                Set Rep_Formulas.DataSource = RsView
                
                If Numero = 0 Then
                    Rep_Formulas.Show vbModal
                Else
                    Rep_Formulas.PrintReport (True)
                End If
                
                Set Rep_Formulas = Nothing
                
            End If
        Else
            Call Mensaje(True, "" & StellarMensaje(16441) & "")
        End If
    Else
        Call Mensaje(True, "" & StellarMensaje(16441) & "")
    End If
    
    RsMaInventario.Close
    Ent.SHAPE_ADM.Close
    
End Sub

Sub ResumenDia_Ordenes(Optional ByVal Numero As Integer = 0)
        
    Dim mSql As String
    
    Dim cMaInventario As String, cTrInventarioCargos As String, cTrInventarioDescargos As String
    Dim RsMaInventario As New ADODB.Recordset, RsTrInventarioCargos As New ADODB.Recordset, RsTrInventarioDescargos As New ADODB.Recordset
    Dim Consultas(1) As String, Relacion(1) As String, Alias(1) As String
    Dim RsView As New ADODB.Recordset, OrderBy As String, AuxCriterio As String
    
    If Ent.SHAPE_ADM.State = adStateOpen Then Ent.SHAPE_ADM.Close
    Ent.SHAPE_ADM.ConnectionString = FrmAppLink.CnStrADMShape '"Provider=MSDataShape.1;Persist Security Info=false;Connect Timeout=0;Data Source=" & Srv_Local & ";User ID=sa;Initial Catalog=vad10;Data Provider=" & PROVIDER_LOCAL
    Ent.SHAPE_ADM.Open
    
    OrderBy = " ORDER BY f_Fecha, 7, Producto"
    
    Dim mInnerMA As String
    
    mInnerMA = " "
    
    AuxCriterio = "WHERE (TR_INVENTARIO.c_Concepto = 'PRD') " & GetLines & _
    "AND TR_INVENTARIO.c_TipoMov = 'Cargo' " & GetLines & _
    "AND NOT LEFT(TR_INVENTARIO.c_Documento, 1) IN ('E'--, 'A'" & GetLines & _
    ")" & GetLines & _
    "AND f_Fecha BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' "
    
    If Trim(txt_sucursal.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND TR_INVENTARIO.c_CodLocalidad = '" & Trim(txt_sucursal.Text) & "' " & GetLines
    End If
    
    If Trim(txtFormula.Text) <> Empty Then
        mInnerMA = " INNER JOIN MA_INVENTARIO ON MA_INVENTARIO.c_Documento = TR_INVENTARIO.c_Documento AND MA_INVENTARIO.c_Concepto = TR_INVENTARIO.c_Concepto AND MA_INVENTARIO.c_CodLocalidad = TR_INVENTARIO.c_CodLocalidad "
        AuxCriterio = AuxCriterio & " AND CASE WHEN " & _
        "LEFT(MA_INVENTARIO.c_Relacion, 3) = 'OPR' THEN " & _
        "(SELECT TOP 1 c_Formula FROM MA_ORDEN_PRODUCCION " & _
        "WHERE c_Documento = SUBSTRING(MA_INVENTARIO.c_Relacion, 4, 9999)) " & _
        "ELSE MA_INVENTARIO.c_Relacion END = '" & Trim(txtFormula.Text) & "' "
    End If
    
    If Trim(origen.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND TR_INVENTARIO.c_Deposito = '" & Trim(origen.Text) & "' " & GetLines
    End If
    
    If Trim(txt_producto.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND TR_INVENTARIO.c_CodArticulo = '" & Trim(txt_producto.Text) & "' " & GetLines
    End If
    
    If Trim(txt_orden.Tag) <> Empty Then
        AuxCriterio = AuxCriterio & " AND TR_INVENTARIO.c_Documento IN (" & txt_orden.Tag & ") " & GetLines
    End If
    
    Dim mCampoLote As String
    
    If Trim(txtLote.Text) <> Empty Then
        
        'mStrLote = StellarMensaje(2937) & " |"
        
        'mCampoLote = "CASE WHEN (CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & ") > 0 THEN " & _
        "SUBSTRING(c_Descripcion, (CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & "), " & _
        "CASE WHEN CHARINDEX(N'" & mStrLote & "', c_Descripcion) > 0 THEN " & _
        "(CAST(CHARINDEX('|', c_Descripcion, (CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & "))) AS INT) " & _
        "- CAST((CHARINDEX(N'" & mStrLote & "', c_Descripcion) + " & Len(mStrLote) & ") AS INT)) " & _
        "ELSE 0 END) " & _
        "ELSE '' END"
        
        mCampoLote = "c_IDLote"
        
        'AuxCriterio = AuxCriterio & " AND (MA_INVENTARIO.c_Concepto + MA_INVENTARIO.c_Documento) IN (" & _
        "SELECT (c_Concepto + c_Documento) FROM TR_INVENTARIO WHERE c_Concepto = 'PRD' " & _
        "AND f_Fecha BETWEEN '" & FechaBD(FechaLow.Value) & "' AND '" & FechaBD(EndOfDay(FechaHigh.Value), FBD_FULL) & "' " & _
        "AND NOT LEFT(c_Documento, 1) IN ('E', 'A') AND c_Descripcion LIKE '%" & txtLote.Text & "%') "
        
        AuxCriterio = AuxCriterio & " AND (MA_INVENTARIO.c_Concepto + MA_INVENTARIO.c_Documento + MA_INVENTARIO.c_CodLocalidad) IN (" & _
        "SELECT (c_Concepto + c_Documento + c_CodLocalidad) FROM TR_INVENTARIO WHERE c_Concepto = 'PRD' " & _
        "AND f_Fecha BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' " & _
        "AND NOT LEFT(c_Documento, 1) IN ('E', 'A') AND (" & mCampoLote & ") LIKE '" & txtLote.Text & "') "
        
    End If
    
    'mLineaProd = "LNP" ' StellarMensaje()
    
    'mCampoLineaProduccion = "CASE WHEN (CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & ") > 0 THEN " & _
    "SUBSTRING(c_Ejecutor, (CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & "), " & _
    "CASE WHEN CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) > 0 THEN " & _
    "(CAST(CHARINDEX('|', c_Ejecutor, (CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & "))) AS INT) " & _
    "- CAST((CHARINDEX(N'" & mLineaProd & "', c_Ejecutor) + " & Len(mLineaProd) & ") AS INT)) " & _
    "ELSE 0 END) " & _
    "ELSE '' END"
    
    Dim mCampoLineaProduccion, mCampoTurnoProduccion
    
    If FrmAppLink.InventarioCampoLineaProduccion Then
        mCampoLineaProduccion = "c_LineaProduccion"
    Else
        mCampoLineaProduccion = "''"
    End If
    
    If FrmAppLink.InventarioCampoTurnoProduccion Then
        mCampoTurnoProduccion = "c_Turno"
    Else
        mCampoTurnoProduccion = "''"
    End If
    
    If Trim(CboLnP.Text) <> Empty Then
        mInnerMA = " INNER JOIN MA_INVENTARIO ON MA_INVENTARIO.c_Documento = TR_INVENTARIO.c_Documento AND MA_INVENTARIO.c_Concepto = TR_INVENTARIO.c_Concepto AND MA_INVENTARIO.c_CodLocalidad = TR_INVENTARIO.c_CodLocalidad "
        AuxCriterio = AuxCriterio & " AND (" & mCampoLineaProduccion & ") " & _
        "= '" & QuitarComillasSimples(CboLnP.Text) & "'"
    End If
    
    If Trim(CboTurno.Text) <> Empty Then
        mInnerMA = " INNER JOIN MA_INVENTARIO ON MA_INVENTARIO.c_Documento = TR_INVENTARIO.c_Documento AND MA_INVENTARIO.c_Concepto = TR_INVENTARIO.c_Concepto AND MA_INVENTARIO.c_CodLocalidad = TR_INVENTARIO.c_CodLocalidad "
        AuxCriterio = AuxCriterio & " AND (" & mCampoTurnoProduccion & ") " & _
        "= '" & QuitarComillasSimples(CboTurno.Text) & "'"
    End If
    
    mSql = "SELECT *, (Produccion - Insumo) AS TotalDia FROM (" & GetLines & _
    "SELECT f_Fecha, c_CodArticulo, Producto, CantDec, SUM(Produccion) AS Produccion, SUM(Insumo) AS Insumo FROM (" & GetLines & _
    "SELECT f_Fecha, c_CodArticulo, MA_PRODUCTOS.c_Descri AS Producto, MA_PRODUCTOS.Cant_Decimales AS CantDec," & GetLines & _
    "SUM(TR_INVENTARIO.n_Cantidad) AS Produccion," & GetLines & _
    "0 AS Insumo FROM" & GetLines & _
    "TR_INVENTARIO" & mInnerMA & "INNER JOIN MA_PRODUCTOS ON TR_INVENTARIO.c_CodArticulo = MA_PRODUCTOS.c_Codigo" & GetLines & _
    AuxCriterio & _
    "GROUP BY TR_INVENTARIO.f_Fecha, c_CodArticulo, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.Cant_Decimales" & GetLines & _
    "UNION ALL" & GetLines & _
    "SELECT f_Fecha, c_CodArticulo, MA_PRODUCTOS.c_Descri AS Producto, MA_PRODUCTOS.Cant_Decimales AS CantDec," & _
    "0 AS Produccion," & GetLines & _
    "SUM(TR_INVENTARIO.n_Cantidad) AS Insumo FROM" & GetLines & _
    "TR_INVENTARIO" & mInnerMA & " INNER JOIN MA_PRODUCTOS ON TR_INVENTARIO.c_CodArticulo = MA_PRODUCTOS.c_Codigo" & GetLines & _
    Replace(AuxCriterio, "Cargo", "Descargo", 1, 1) & _
    "GROUP BY TR_INVENTARIO.f_Fecha, c_CodArticulo, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.Cant_Decimales" & GetLines & _
    ") TB GROUP BY f_Fecha, c_CodArticulo, Producto, CantDec" & GetLines & _
    ") TB2 " & OrderBy & GetLines
    
    Call Apertura_RecordsetC(RsMaInventario)
    
    RsMaInventario.Open mSql, Ent.SHAPE_ADM, adOpenDynamic, adLockReadOnly, adCmdText
    RsView.Open "select top 1 * from ma_productos", Ent.SHAPE_ADM, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsMaInventario.EOF Then
        
        Dim Rep_Formulas As Object: Set Rep_Formulas = FrmAppLink.GetDefaultTextDataReport
        
        Call Escribir_ResumenDia(RsMaInventario, Rep_Formulas, "MA_PRODUCIR", "LBL_TEXT")
        Call REPO_CABE(Rep_Formulas, "" & StellarMensaje(2842) & "")
        Set Rep_Formulas.DataSource = RsView
        
        If Numero = 0 Then
            Rep_Formulas.Show vbModal
        Else
            Rep_Formulas.PrintReport (True)
        End If
        
        Set Rep_Formulas = Nothing
        
    Else
        Call Mensaje(True, "" & StellarMensaje(16441) & "")
    End If
    
    RsMaInventario.Close
    Ent.SHAPE_ADM.Close
    
End Sub

Sub Escribir_ResumenDia(RsTemp As ADODB.Recordset, Reporte As Object, _
Seccion As String, Objeto As String)
    
    Dim mFechaAnt As Date
    
    Escribir Empty, Reporte, Seccion, Objeto
    '"(*) Incluye cargos y descargos por anulaciones seg�n la fecha de realizaci�n."
    Escribir StellarMensaje(2841), Reporte, Seccion, Objeto
    Escribir Empty, Reporte, Seccion, Objeto
    
    While Not RsTemp.EOF
        
        If FechaBD(mFechaAnt) <> FechaBD(RsTemp!f_Fecha) Then
            
            mFechaAnt = RsTemp!f_Fecha
            
            Escribir Empty, Reporte, Seccion, Objeto
            
            Escribir LeftJust(SDate(mFechaAnt), 15) & " " & _
            LeftJust(StellarMensaje(5010), 40) & " " & _
            RightJust(StellarMensaje(1010), 15) & " " & _
            RightJust(StellarMensaje(3024), 15) & " " & _
            RightJust(StellarMensaje(3019), 20) & " ", _
            Reporte, Seccion, Objeto
            
            'Escribir SDate(mFechaAnt), Reporte, Seccion, Objeto
            Escribir Empty, Reporte, Seccion, Objeto
            
        End If
        
        Escribir LeftJust("-" & " " & "" & RsTemp!c_CodArticulo & "", 15) & " " & _
        LeftJust(RsTemp!Producto, 40) & " " & _
        RightJust(FormatNumber(RsTemp!Produccion, RsTemp!CantDec), 15) & " " & _
        RightJust(FormatNumber(RsTemp!Insumo, RsTemp!CantDec), 15) & " " & _
        RightJust(IIf(RsTemp!TotalDia > 0, "+", Empty) & FormatNumber(RsTemp!TotalDia, RsTemp!CantDec), 20) & " ", _
        Reporte, Seccion, Objeto
        
        RsTemp.MoveNext
        
    Wend
    
End Sub

Sub Reimprimir_Formulas(tipo As String, orden As String, Optional Numero As Integer = 0)
    
    Dim cMaInventario As String, cTrInventarioCargos As String, cTrInventarioDescargos As String
    Dim RsMaInventario As New ADODB.Recordset, RsTrInventarioCargos As New ADODB.Recordset, RsTrInventarioDescargos As New ADODB.Recordset
    Dim Consultas(1) As String, Relacion(1) As String, Alias(1) As String
    Dim RsView As New ADODB.Recordset, OrderBy As String, AuxCriterio As String
    
    If Ent.SHAPE_ADM.State = adStateOpen Then Ent.SHAPE_ADM.Close
    Ent.SHAPE_ADM.ConnectionString = FrmAppLink.CnStrADMShape '"Provider=MSDataShape.1;Persist Security Info=false;Connect Timeout=0;Data Source=" & Srv_Local & ";User ID=sa;Initial Catalog=vad10;Data Provider=" & PROVIDER_LOCAL
    Ent.SHAPE_ADM.Open
    OrderBy = ""
    AuxCriterio = ""
    
    For Cont = 0 To seleccionados.ListCount - 1
        If seleccionados.List(Cont) = "" & StellarMensaje(10016) & " (ASC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "c_Formula"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(10016) & " (DESC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "c_Formula DESC"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(15500) & " (ASC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "Update_Date"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(15500) & " (DESC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "Update_Date DESC"
        End If
    Next Cont
    
    '& IIf(Trim(OrderBy) = "", "", OrderBy)
    
    cTotalProd = "ROUND(isNULL((SELECT SUM((COSTOSORIGEN.n_Cantidad * MA_PRODUCTOS." & CostoActivo & " * (1 / MonedaFormula.n_Factor)) + (COSTOSORIGEN.n_Cantidad * MA_PRODUCTOS." & CostoActivo & " * (n_Merma / 100) * (1 / MonedaFormula.n_Factor))) FROM TR_PRODUCCION COSTOSORIGEN INNER JOIN MA_PRODUCCION Doc ON COSTOSORIGEN.c_Formula = Doc.c_Formula INNER JOIN MA_MONEDAS MonedaFormula ON Doc.c_CodMoneda = MonedaFormula.c_CodMoneda INNER JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = COSTOSORIGEN.c_CodProducto INNER JOIN MA_MONEDAS ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda WHERE COSTOSORIGEN.c_Formula = MA_PRODUCCION.c_Formula AND COSTOSORIGEN.b_Producir = 0), 0), 8, 0)"
    cSumaFactor = "ROUND(isNULL((SELECT SUM(nu_FactorCosto) FROM TR_PRODUCCION SUMAFACTOR WHERE SUMAFACTOR.b_Producir = 1 AND SUMAFACTOR.c_Formula = MA_PRODUCCION.c_Formula), 1), 8, 0)"
    
    cMaInventario = "SELECT ROUND((" & cTotalProd & " + n_CostoDir), 8, 0) AS n_TotalFormula, " & _
    "MA_PRODUCCION.*, isNULL(MA_SUCURSALES.c_Descripcion, 'N/A') AS Localidad, isNULL(MA_USUARIOS.Descripcion, 'N/A') AS Usuario, MonedaDoc.c_Descripcion AS c_DesMoneda " & _
    "FROM MA_PRODUCCION LEFT JOIN MA_MONEDAS MonedaDoc ON MonedaDoc.c_CodMoneda = MA_PRODUCCION.c_CodMoneda " & _
    "LEFT JOIN MA_SUCURSALES ON MA_PRODUCCION.c_CodLocalidad = MA_SUCURSALES.c_Codigo " & _
    "LEFT JOIN MA_USUARIOS ON MA_PRODUCCION.c_CodUsuario = MA_USUARIOS.CodUsuario " & _
    "WHERE " & IIf(orden = "", "", "c_Formula = '" & orden & "' AND ") & " MA_PRODUCCION.c_Status = '" & tipo & "' "
    
    cTrInventarioDescargos = "SELECT '" & StellarMensaje(3009) & "' AS c_TipoMov, ROUND((MA_PRODUCTOS." & CostoActivo & " * (1 / MonedaFormula.n_Factor)), 8, 0) AS CostoActivo, ROUND((TR_PRODUCCION.n_Cantidad * MA_PRODUCTOS." & CostoActivo & " * (1 / MonedaFormula.n_Factor)), 8, 0) AS n_Subtotal, ROUND((TR_PRODUCCION.n_Cantidad * MA_PRODUCTOS." & CostoActivo & " * MA_MONEDAS.n_Factor), 8, 0) AS n_SubtotalLocal, TR_PRODUCCION.*, MA_PRODUCTOS.Cant_Decimales, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.c_Presenta AS Presentacion FROM TR_PRODUCCION INNER JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.c_CodProducto INNER JOIN MA_MONEDAS ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda INNER JOIN MA_PRODUCCION Doc ON TR_PRODUCCION.c_Formula = Doc.c_Formula INNER JOIN MA_MONEDAS MonedaFormula ON Doc.c_CodMoneda = MonedaFormula.c_CodMoneda WHERE " & IIf(orden = "", "", "TR_PRODUCCION.c_Formula = '" & orden & "' AND ") & " TR_PRODUCCION.b_Producir = 0 "
    
    cTrInventarioCargos = "SELECT '" & StellarMensaje(3008) & "' AS c_TipoMov, ROUND(((" & cTotalProd & " + n_CostoDir) / TR_PRODUCCION.n_Cantidad * (nu_FactorCosto / " & cSumaFactor & ")), 8, 0) AS CostoActivo, ROUND(((" & cTotalProd & " + n_CostoDir) * (nu_FactorCosto / " & cSumaFactor & ")), 8, 0) AS n_Subtotal, ROUND(((" & cTotalProd & " + n_CostoDir) * (nu_FactorCosto / " & cSumaFactor & ") * MA_MONEDAS.n_Factor), 8, 0) AS n_SubtotalLocal, " & _
    "TR_PRODUCCION.*, MA_PRODUCTOS.Cant_Decimales, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.c_Presenta AS Presentacion FROM TR_PRODUCCION INNER JOIN MA_PRODUCCION ON TR_PRODUCCION.c_Formula = MA_PRODUCCION.c_Formula INNER JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.c_CodProducto INNER JOIN MA_MONEDAS ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda INNER JOIN MA_MONEDAS MonedaFormula ON MA_PRODUCCION.c_CodMoneda = MonedaFormula.c_CodMoneda WHERE " & IIf(orden = "", "", "TR_PRODUCCION.c_Formula = '" & orden & "' AND ") & " TR_PRODUCCION.b_Producir = 1 " & _
    IIf(Trim(txt_producto.Text) = "", "", " AND TR_PRODUCCION.c_CodProducto = '" & Trim(txt_producto.Text) & "' ") & _
    IIf(Trim(departamento.Text) = "", "", " AND c_Departamento = '" & Trim(departamento.Text) & "' ") & _
    IIf(Trim(grupo.Text) = "", "", " AND c_Grupo = '" & Trim(grupo.Text) & "' ") & _
    IIf(Trim(subgrupo.Text) = "", "", " AND c_Subgrupo = '" & Trim(subgrupo.Text) & "' ")
    
    If Trim(txt_orden.Text) <> "" Then
        AuxCriterio = AuxCriterio & " AND MA_PRODUCCION.c_Formula = '" & Trim(txt_orden.Text) & "' "
    End If
    
    If Trim(txt_sucursal.Text) <> "" Then
        AuxCriterio = AuxCriterio & " AND MA_PRODUCCION.c_CodLocalidad = '" & Trim(txt_sucursal.Text) & "' "
    End If
    
    If Trim(txt_comprador.Text) <> "" Then
        AuxCriterio = AuxCriterio & " AND MA_PRODUCCION.c_CodUsuario = '" & Trim(txt_comprador.Text) & "' "
    End If
    
    cMaInventario = cMaInventario & AuxCriterio & IIf(Trim(OrderBy) = "", "", "Order by " & OrderBy)
    
    Consultas(0) = cTrInventarioCargos
    Consultas(1) = cTrInventarioDescargos
    
    Relacion(0) = "c_Formula"
    Relacion(1) = "c_Formula"
    Alias(0) = "TRCARGOS"
    Alias(1) = "TRDESCARGOS"
    
    Call Apertura_RecordsetC(RsMaInventario)
    
    CCriterio = ShapeStrAdv(cMaInventario, "MA_PRODUCCION", Consultas, Alias, Relacion, Relacion, Alias)
    
    'Debug.Print cTrInventarioCargos
    'Debug.Print cTrInventarioDescargos
    'Debug.Print cMaInventario
    
    RsMaInventario.Open CCriterio, Ent.SHAPE_ADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
    RsView.Open "select top 1 * from ma_productos", Ent.SHAPE_ADM, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not RsMaInventario.EOF
        If RsMaInventario("TRCARGOS").Value.EOF Then
            RsMaInventario.Delete
        End If
        RsMaInventario.MoveNext
    Wend
    
    If Not (RsMaInventario.EOF And RsMaInventario.BOF) Then
        RsMaInventario.MoveFirst
    End If
        
    If Not RsMaInventario.EOF Then
        
        If Not RsMaInventario("TRCARGOS").Value.EOF Then
            
            If Numero = 2 Then
                Call GenerarXML_LstadoFormulas(RsMaInventario)
            Else
                
                Dim Rep_Formulas As Object: Set Rep_Formulas = FrmAppLink.GetDefaultTextDataReport
                
                Call Escribir_DTR_Formula(RsMaInventario, Rep_Formulas, "MA_PRODUCIR", "LBL_TEXT")
                Call REPO_CABE(Rep_Formulas, "" & StellarMensaje(1091) & "")
                
                Set Rep_Formulas.DataSource = RsView
                
                If Numero = 0 Then
                    Rep_Formulas.Show vbModal
                Else
                    Rep_Formulas.PrintReport True
                End If
                
                Set Rep_Formulas = Nothing
                
            End If
            
        Else
            Call Mensaje(True, "" & StellarMensaje(16441) & "")
        End If
    Else
        Call Mensaje(True, "" & StellarMensaje(16441) & "")
    End If
    
    RsMaInventario.Close
    Ent.SHAPE_ADM.Close
    
End Sub

Sub Escribir_DTR_Formula(RsTemp As ADODB.Recordset, Reporte As Object, _
Seccion As String, Objeto As String)
    
    Dim RsTrInventarioCargos As New ADODB.Recordset, RsTrInventarioDescargos As New ADODB.Recordset
    Dim CostosMerma As Double, CostosProduccion As Double
    
    Escribir_Txt = False
    
    If Not RsTemp.EOF Then
        
        While Not RsTemp.EOF
            
            Set RsTrInventarioCargos = RsTemp("TRCARGOS").Value
            Set RsTrInventarioDescargos = RsTemp("TRDESCARGOS").Value
            
            'Escribir "PRODUCCI�N No " & Space(10) & rsTemp!c_Documento, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(5003)) & ":", 18) & " " & RsTemp!c_Formula, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(143)) & ":", 18) & " " & RsTemp!c_Descripcion, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(16324)) & ":", 18) & " " & RsTemp!c_DesMoneda, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(5004)) & ":", 18) & " " & FormatNumber(RsTemp!n_CostoDir, Std_Decm), Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "PRODUCTOS A UTILIZAR", Reporte, Seccion, Objeto
            Escribir "" & UCase(StellarMensaje(5006)) & "", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir LeftJust("Producto", 18) & " " & LeftJust("Descripci�n", 32) & " " & RightJust("Cantidad", 14) & " " & LeftJust("Presentaci�n", 15) & " " & RightJust("Costo Unit.", 16) & " " & RightJust("Subtotal", 16), Reporte, Seccion, Objeto
            Escribir LeftJust("" & StellarMensaje(5010) & "", 18) & " " & LeftJust("" & StellarMensaje(143) & "", 32) & " " & RightJust("" & StellarMensaje(3001) & "", 14) & " " & LeftJust("" & FrmAppLink.PRDLabelCampoPresenta & "", 15) & " " & RightJust("" & StellarMensaje(151) & "", 16) & " " & RightJust("" & StellarMensaje(139) & "", 16), Reporte, Seccion, Objeto
            
            CostosMerma = 0
            CostosProduccion = 0
            
            While Not RsTrInventarioDescargos.EOF
                Escribir LeftJust(RsTrInventarioDescargos!c_CodProducto, 18) & " " & LeftJust(Mid(RsTrInventarioDescargos!c_Descri, 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Cantidad, RsTrInventarioDescargos!Cant_Decimales), 14) & " " & LeftJust(RsTrInventarioDescargos!Presentacion, 15) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!CostoActivo, Std_Decm), 16) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Subtotal, Std_Decm), 16), Reporte, Seccion, Objeto
                CostosProduccion = CostosProduccion + (RsTrInventarioDescargos!n_Cantidad * RsTrInventarioDescargos!CostoActivo)
                RsTrInventarioDescargos.MoveNext
            Wend
            
            'IMPRESION DE LOS MONTO POR MERMA Y PRODUCCION
            
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "." & Space(20) & "  Monto de Producci�n: " & RightJust(FormatNumber(CostosProduccion + rsTemp!n_Cantidad_compra, Std_Decm), 16) & " (No Incluye Costos Directos de Producci�n)", Reporte, Seccion, Objeto
            Escribir "." & Space(20) & "  " & StellarMensaje(2835) & ": " & RightJust(FormatNumber(CostosProduccion + RsTemp!n_CostoDir, Std_Decm), 16) & " (" & StellarMensaje(2836) & ")", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "PRODUCTOS A PRODUCIR", Reporte, Seccion, Objeto
            Escribir "" & UCase(StellarMensaje(5008)) & "", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir LeftJust("Producto", 18) & " " & LeftJust("Descripci�n", 32) & " " & RightJust("Cantidad", 14) & " " & LeftJust("Presentaci�n", 15) & " " & RightJust("Costo Unit.", 16) & " " & RightJust("Subtotal", 16), Reporte, Seccion, Objeto
            Escribir LeftJust("" & StellarMensaje(5010) & "", 18) & " " & LeftJust("" & StellarMensaje(143) & "", 32) & " " & RightJust("" & StellarMensaje(3001) & "", 14) & " " & LeftJust("" & FrmAppLink.PRDLabelCampoPresenta & "", 15) & " " & RightJust("" & StellarMensaje(151) & "", 16) & " " & RightJust("" & StellarMensaje(139) & "", 16), Reporte, Seccion, Objeto
            
            While Not RsTrInventarioCargos.EOF
                Escribir LeftJust(RsTrInventarioCargos!c_CodProducto, 18) & " " & LeftJust(Mid(RsTrInventarioCargos!c_Descri, 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Cantidad, RsTrInventarioCargos!Cant_Decimales), 14) & " " & LeftJust(RsTrInventarioCargos!Presentacion, 15) & " " & RightJust(FormatNumber(RsTrInventarioCargos!CostoActivo, Std_Decm), 16) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Subtotal, Std_Decm), 16), Reporte, Seccion, Objeto
                RsTrInventarioCargos.MoveNext
            Wend
            
            Escribir ".", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            Escribir_Txt = True
            
            RsTemp.MoveNext
            
        Wend
        
        RsTemp.MoveFirst
        
    End If
    
End Sub

'Autor:          Luis Arraga
'Fecha Creaci�n: Oct 2019
'Descripcion:    Codigo para generar documento XML
Private Sub GenerarXML_LstadoFormulas(Rs As ADODB.Recordset)
    
    On Error GoTo Error
    
    Dim RutaGenerarXML As String
    Dim Campos As Dictionary: Set Campos = New Dictionary
    Dim TmpCount As Long: TmpCount = -1
    
    rutaXml.Filter = "XML (*.xml)"
    rutaXml.DefaultExt = "xml"
    
    rutaXml.CancelError = True
   
    rutaXml.ShowSave
    
    RutaGenerarXML = rutaXml.FileName
        
    Rs.MoveFirst
    
    'Select Case Modalidad.ListIndex
    
        'Case 0 'Resumida
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Formula", StellarMensaje(10016))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Localidad", StellarMensaje(12))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Update_Date", StellarMensaje(15500))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Descripcion", RemoverAcentos(StellarMensaje(143)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Usuario", StellarMensaje(1229))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_DesMoneda", StellarMensaje(16324))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_CostoDir", SpaceToUnderscore(StellarMensaje(5004)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_TotalFormula", SpaceToUnderscore(StellarMensaje(7019)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_TipoMov_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(10091)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_CodProducto_TRDESCARGOS", "D_" & RemoverAcentos(StellarMensaje(142)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Descri_TRDESCARGOS", "D_" & StellarMensaje(16480))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Presentacion_TRDESCARGOS", "D_" & SpaceToUnderscore(RemoverAcentos(FrmAppLink.PRDLabelCampoPresenta)))
                        
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Cantidad_TRDESCARGOS", "D_" & StellarMensaje(3001))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Merma_TRDESCARGOS", "D_" & "Porc_" & StellarMensaje(3036))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("CostoActivo_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(151)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Subtotal_TRDESCARGOS", "D_" & StellarMensaje(139))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_SubtotalLocal_TRDESCARGOS", "D_" & StellarMensaje(139) & "_Local")
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("nu_FactorCosto_TRDESCARGOS", "D_" & "Factor")
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_TipoMov_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(10091)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_CodProducto_TRCARGOS", "C_" & RemoverAcentos(StellarMensaje(142)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Descri_TRCARGOS", "C_" & StellarMensaje(16480))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Presentacion_TRCARGOS", "C_" & SpaceToUnderscore(RemoverAcentos(FrmAppLink.PRDLabelCampoPresenta)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Cantidad_TRCARGOS", "C_" & StellarMensaje(3001))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Merma_TRCARGOS", "C_" & "Porc_" & StellarMensaje(3036))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("CostoActivo_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(151)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Subtotal_TRCARGOS", "C_" & StellarMensaje(139))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_SubtotalLocal_TRCARGOS", "C_" & StellarMensaje(139) & "_Local")
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("nu_FactorCosto_TRCARGOS", "C_" & "Factor")
            
    'End Select
    
    Call Crear_Xml_Definido(Rs, RutaGenerarXML, Campos, True)
    
    CmdOpenXML.Visible = True
    
    ShowTooltip "Click Izq. -> Abrir con Excel" & GetLines & _
    "Click Med. -> Abrir con aplicaci�n predeterminada para archivos XML" & GetLines & _
    "Click Der. -> Ocultar el bot�n." _
    & GetLines & Space(21) & "|" & GetLines & Space(21) & "V" & _
    GetLines, CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

'Autor:          Luis Arraga
'Fecha Creaci�n: Oct 2019
'Descripcion:    Codigo para generar documento XML
Private Sub GenerarXMLConsumoDiario(Rs As ADODB.Recordset)
    
    On Error GoTo Error
    
    Dim RutaGenerarXML As String
    Dim Campos As Dictionary: Set Campos = New Dictionary
    Dim TmpCount As Long: TmpCount = -1
    
    rutaXml.Filter = "XML (*.xml)"
    rutaXml.DefaultExt = "xml"
    
    rutaXml.CancelError = True
   
    rutaXml.ShowSave
    
    RutaGenerarXML = rutaXml.FileName
        
    Rs.MoveFirst
    
    'Select Case Modalidad.ListIndex
    
        'Case 0 'Resumida
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("f_Fecha", StellarMensaje(15500))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_CodArticulo", RemoverAcentos(StellarMensaje(142)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Producto", StellarMensaje(16480))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Produccion", RemoverAcentos(StellarMensaje(1010)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Insumo", StellarMensaje(3024))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("General", SpaceToUnderscore(StellarMensaje(7019)))
            
    'End Select
    
    Call Crear_Xml_Definido(Rs, RutaGenerarXML, Campos, True)
    
    CmdOpenXML.Visible = True
    
    ShowTooltip "Click Izq. -> Abrir con Excel" & GetLines & _
    "Click Med. -> Abrir con aplicaci�n predeterminada para archivos XML" & GetLines & _
    "Click Der. -> Ocultar el bot�n." _
    & GetLines & Space(21) & "|" & GetLines & Space(21) & "V" & _
    GetLines, CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

'Autor:          Luis Arraga
'Fecha Creaci�n: Oct 2019
'Descripcion:    Codigo para generar documento XML
Private Sub GenerarXML_ListadoProduccion(Rs As ADODB.Recordset)
    
    On Error GoTo Error
    
    Dim RutaGenerarXML As String
    Dim Campos As Dictionary: Set Campos = New Dictionary
    Dim TmpCount As Long: TmpCount = -1
    
    rutaXml.Filter = "XML (*.xml)"
    rutaXml.DefaultExt = "xml"
    
    rutaXml.CancelError = True
   
    rutaXml.ShowSave
    
    RutaGenerarXML = rutaXml.FileName
        
    Rs.MoveFirst
    
    'Select Case Modalidad.ListIndex
    
        'Case 0 'Resumida
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Documento", StellarMensaje(10016))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Localidad", StellarMensaje(12))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Origen", StellarMensaje(2606))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Destino", StellarMensaje(10117))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("d_Fecha", StellarMensaje(15500))
            
            If FrmAppLink.InventarioCampoLineaProduccion Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("LineaProduccionINV", SpaceToUnderscore(RemoverAcentos(StellarMensaje(2947))))
            End If
            
            If FrmAppLink.InventarioCampoTurnoProduccion Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("TurnoProduccionINV", SpaceToUnderscore(RemoverAcentos(StellarMensaje(15513))))
            End If
            
            If ChkPrdAdv.Value Then
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CodOrdenProduccion", "NRO_ORDEN_PROD")
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CodFormula", "NRO_FORMULA")
                
            Else
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_Relacion", SpaceToUnderscore(RemoverAcentos(StellarMensaje(2611))))
                
            End If
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Observacion", RemoverAcentos(StellarMensaje(10122)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Usuario", StellarMensaje(1229))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_DesMoneda", StellarMensaje(16324))
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoProductos", "Costo_Productos")
            End If
            
            If FrmAppLink.GetNivelUsuario >= FrmAppLink.PRDNivelVerCostosDirectos And FrmAppLink.PRDNivelVerCostosDirectos > 0 Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_Cantidad_Compra", SpaceToUnderscore(StellarMensaje(5004)))
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoMerma", "Costo_Merma")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoTotalProyectado", "Total_Proyectado")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoTotal", "Costo_Total_Produccion")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("DifTotales", "Diferencia")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("Porc_Variacion_Totales", "Porc_Variacion_Totales")
            End If
            
            If Not CBool(ChkPrdAdv.Value) Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_TotalProduccion", SpaceToUnderscore(StellarMensaje(7019)))
            End If
            
            If ChkPrdAdv.Value Then
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_CodArticulo_TRCARGOS", "Cod_Prod_Terminado")
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_Descri_TRCARGOS", "Producto_Receta")
                
            End If
            
            If ChkPrdAdv.Value And (FrmAppLink.GetNivelUsuario >= FrmAppLink.PRDNivelVerCostosDirectos And FrmAppLink.PRDNivelVerCostosDirectos > 0) Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoHoraHombre", "Costo_Hora_Hombre")
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoHoraCargaFabril", "Costo_Hora_CargaFabril")
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("HorasHombrePorUnidad", "Horas_Hombre_X_Und")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoUnitarioFormula", "Costo_Proy_x_Und")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoUnitarioTeorico", "Costo_x_Und_Real")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PesoUnitarioFormula", "Peso_Und")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CantibulFormula", "Cant_x_Emp")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PesoEmpaqueFormula", "Peso_Emp")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CapacidadMaxLote", "Cap_Max_Batch_Kg")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CapacidadLote", "Cap_Batch_Kg")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("UnidadesPorLote", "Und_x_Batch")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PesoUnitarioNetoFormula", "Peso_Mezcla_Estandar")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PesoPromedioUnidad", "Peso_Prom_Und")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PesoPromedioEmpaque", "Peso_Prom_Emp")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CantidadProduccion", "Cant_Prod_Final")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("NumeroDeLotes", "Cant_Lotes_Prod")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PesoTotalTeorico", "Peso_Total_Estandar")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PesoTotalUtilizado", "Peso_Total_Mezcla")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("VariacionPesoTotal", "Variacion_Peso")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("Porc_Variacion_Pesos", "Porc_Variacion_Peso")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("NumeroTrabajadores", "Trabajadores")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("NumeroHorasTrabajadas", "Horas_Jornada")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("TotalHorasTrabajadas", "TotalHorasHombre")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("TiempoParadaProgramada", "HorasParadaProgramada")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("TiempoParadaImprevista", "HorasParadaImprevista")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("MotivoParadaProduccion", "Motivo_De_Parada")
            End If
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_TipoMov_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(10091)))
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("DescTipoItem_TRDESCARGOS", "Tipo_Material")
            End If
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_CodArticulo_TRDESCARGOS", "D_" & RemoverAcentos(StellarMensaje(142)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Descri_TRDESCARGOS", "D_" & StellarMensaje(16480))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Presentacion_TRDESCARGOS", "D_" & SpaceToUnderscore(RemoverAcentos(FrmAppLink.PRDLabelCampoPresenta)))
            
            'If ManejaLote Then
                'TmpCount = TmpCount + 1
                'Campos(TmpCount) = Array("IDLote_TRDESCARGOS", "D_" & RemoverAcentos(StellarMensaje(2937)))
            'End If
            
            If FrmAppLink.PRDManejaMermaExplicita And ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_PorcMerma_TRDESCARGOS", "D_Porc_Merma")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_CantidadNeta_TRDESCARGOS", "D_" & StellarMensaje(3001))
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoUniNeto_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(151)))
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("CostoProdNeto_TRDESCARGOS", "D_" & StellarMensaje(139))
            Else
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_Cantidad_TRDESCARGOS", "D_" & StellarMensaje(3001))
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_Costo_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(151)))
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_Subtotal_TRDESCARGOS", "D_" & StellarMensaje(139))
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PorcCostoTipoItem_TRDESCARGOS", "Porc_VS_CostoTotal_TipoItem")
            End If
            
            If ChkPrdAdv.Value Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("PorcCostoTotal_TRDESCARGOS", "Porc_VS_CostoTotalItems")
            End If
            
            'If ChkPrdAdv.Value Then
                'TmpCount = TmpCount + 1
                'Campos(TmpCount) = Array("TotalCostoMateriales_TRDESCARGOS", "TotalCostoMateriales")
            'End If
            
            If Not CBool(ChkPrdAdv.Value) Then
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_TipoMov_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(10091)))
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_CodArticulo_TRCARGOS", "C_" & RemoverAcentos(StellarMensaje(142)))
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_Descri_TRCARGOS", "C_" & StellarMensaje(16480))
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("Presentacion_TRCARGOS", "C_" & SpaceToUnderscore(RemoverAcentos(FrmAppLink.PRDLabelCampoPresenta)))
                
                'If ManejaLote Then
                    'TmpCount = TmpCount + 1
                    'Campos(TmpCount) = Array("IDLote_TRCARGOS", "C_" & RemoverAcentos(StellarMensaje(2937)))
                'End If
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_Cantidad_TRCARGOS", "C_" & StellarMensaje(3001))
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_Costo_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(151)))
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("n_Subtotal_TRCARGOS", "C_" & StellarMensaje(139))
                
            End If
            
            If ChkPrdAdv.Value And (FrmAppLink.GetNivelUsuario >= FrmAppLink.PRDNivelVerCostosDirectos And FrmAppLink.PRDNivelVerCostosDirectos > 0) Then
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("TipoCosto_TRCOSTOSDIR", "CD_TipoCosto")
                
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("MontoProyectado_TRCOSTOSDIR", "CD_Total_Proyectado")
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("MontoReal_TRCOSTOSDIR", "CD_Monto_Real")
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("Proyectado_x_Und_TRCOSTOSDIR", "CD_Proyectado_x_Und")
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("Monto_x_Und_TRCOSTOSDIR", "CD_Monto_x_Und")
                
            End If
            
    'End Select
    
    Call Crear_Xml_Definido(Rs, RutaGenerarXML, Campos, True)
    
    CmdOpenXML.Visible = True
    
    ShowTooltip "Click Izq. -> Abrir con Excel" & GetLines & _
    "Click Med. -> Abrir con aplicaci�n predeterminada para archivos XML" & GetLines & _
    "Click Der. -> Ocultar el bot�n." _
    & GetLines & Space(21) & "|" & GetLines & Space(21) & "V" & _
    GetLines, CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Sub Reimprimir_Ordenes(Optional Numero As Integer = 0)
    
    Dim cMaInventario As String, cTrInventarioCargos As String, cTrInventarioDescargos As String
    Dim RsMaInventario As New ADODB.Recordset, RsTrInventarioCargos As New ADODB.Recordset, RsTrInventarioDescargos As New ADODB.Recordset
    Dim Consultas(1) As String, Relacion(1) As String, Alias(1) As String
    Dim RsView As New ADODB.Recordset, OrderBy As String, _
    AuxCriterio As String, AuxCriterioTR As String
    
    If Ent.SHAPE_ADM.State = adStateOpen Then Ent.SHAPE_ADM.Close
    Ent.SHAPE_ADM.ConnectionString = CnStrADMShape '"Provider=MSDataShape.1;Persist Security Info=false;Connect Timeout=0;Data Source=" & Srv_Local & ";User ID=sa;Initial Catalog=vad10;Data Provider=" & PROVIDER_LOCAL
    Ent.SHAPE_ADM.Open
    OrderBy = ""
    AuxCriterio = ""
    
    For Cont = 0 To seleccionados.ListCount - 1
        If seleccionados.List(Cont) = "" & StellarMensaje(10016) & " (ASC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "c_Documento"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(10016) & " (DESC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "c_Documento DESC"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(15500) & " (ASC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "d_Fecha"
        ElseIf seleccionados.List(Cont) = "" & StellarMensaje(15500) & " (DESC)" Then
            OrderBy = OrderBy & IIf(Trim(OrderBy) = "", "", ",") & "d_Fecha DESC"
        End If
    Next Cont
    
    '& IIf(Trim(OrderBy) = "", "", OrderBy)
    cMaInventario = "SELECT (c_Documento + c_CodLocalidad) AS PK, isNULL(MA_SUCURSALES.c_Descripcion, 'N/A') AS Localidad, " & _
    "MA_ORDEN_PRODUCCION.*, isNULL(MA_MONEDAS.c_Descripcion, 'N/A') AS C_DESMONEDA, " & _
    "isNULL(MA_USUARIOS.Descripcion, 'N/A') AS Usuario " & _
    "FROM MA_ORDEN_PRODUCCION LEFT JOIN MA_MONEDAS ON MA_MONEDAS.c_CodMoneda = MA_ORDEN_PRODUCCION.c_CodMoneda LEFT JOIN MA_SUCURSALES ON MA_ORDEN_PRODUCCION.c_CodLocalidad = MA_SUCURSALES.c_Codigo LEFT JOIN MA_USUARIOS ON MA_ORDEN_PRODUCCION.c_Usuario = MA_USUARIOS.CodUsuario WHERE 1 = 1 "
    
    mCriterioFecha = " AND MA_ORDEN_PRODUCCION.d_Fecha BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' "
    
    If Trim(txt_orden.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_Documento = '" & Trim(txt_orden.Text) & "' "
        AuxCriterioTR = AuxCriterioTR & " AND TR_ORDEN_PRODUCCION.c_Documento = '" & Trim(txt_orden.Text) & "' "
    End If
    
    If Trim(txt_sucursal.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_CodLocalidad = '" & Trim(txt_sucursal.Text) & "' "
        AuxCriterioTR = AuxCriterioTR & " AND TR_ORDEN_PRODUCCION.c_CodLocalidad = '" & Trim(txt_sucursal.Text) & "' "
    End If
    
    If Trim(txtFormula.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_Formula = '" & Trim(txtFormula.Text) & "' "
    End If
    
    If Trim(txt_comprador.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_Usuario = '" & Trim(txt_comprador.Text) & "' "
    End If
    
    If Trim(CboLnP.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_LineaProduccion " & _
        "= '" & QuitarComillasSimples(CboLnP.Text) & "') "
    End If
    
    If ChkFechaEstimada = 1 Then
        mCriterioFecha = " AND MA_ORDEN_PRODUCCION.d_Fecha_Produccion BETWEEN '" & FechaBD(Fechalow.Value) & "' AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' "
    End If
    
    Select Case CmbStatus.ListIndex
        Case 0 ' Todos
        Case 1 ' COMPLETADO
            AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_Status = 'DCO' "
        Case 2 ' PENDIENTE
            AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_Status = 'DPE' "
        Case 3 ' CANCELADO
            AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_Status = 'ANU' "
        Case 4 ' EN ESPERA
            AuxCriterio = AuxCriterio & " AND MA_ORDEN_PRODUCCION.c_Status = 'DWT' "
    End Select
    
    If Trim(txtLote.Text) <> Empty Then
        AuxCriterio = AuxCriterio & " AND (MA_ORDEN_PRODUCCION.c_Documento + MA_ORDEN_PRODUCCION.c_CodLocalidad) IN (" & _
        "SELECT (c_Documento + c_CodLocalidad) FROM TR_ORDEN_PRODUCCION " & _
        "INNER JOIN MA_ORDEN_PRODUCCION ON TR_ORDEN_PRODUCCION.c_Documento = MA_ORDEN_PRODUCCION.c_Documento AND TR_ORDEN_PRODUCCION.c_CodLocalidad = MA_ORDEN_PRODUCCION.c_CodLocalidad " & _
        "WHERE 1 = 1 AND TR_ORDEN_PRODUCCION.c_IDLote LIKE '" & txtLote.Text & "'" & mCriterioFecha & ") "
    End If
    
    AuxCriterio = AuxCriterio & mCriterioFecha
    
    cMaInventario = cMaInventario & AuxCriterio _
    & IIf(Trim(OrderBy) = "", "", " ORDER BY " & OrderBy)
    
    cTrInventarioCargos = "SELECT (c_Documento + c_CodLocalidad) AS PK, '" & StellarMensaje(3008) & "' AS c_TipoMov, TR_ORDEN_PRODUCCION.*, MA_PRODUCTOS.Cant_Decimales, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.c_Presenta AS Presentacion FROM TR_ORDEN_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo WHERE 1 = 1 AND b_Producir = 1 " & _
    IIf(Trim(txt_producto.Text) = "", "", " AND c_CodArticulo = '" & Trim(txt_producto.Text) & "'") & _
    IIf(Trim(departamento.Text) = "", "", " AND c_Departamento = '" & Trim(departamento.Text) & "' ") & _
    IIf(Trim(grupo.Text) = "", "", " AND c_Grupo = '" & Trim(grupo.Text) & "' ") & _
    IIf(Trim(subgrupo.Text) = "", "", " AND c_Subgrupo = '" & Trim(subgrupo.Text) & "' ") & AuxCriterioTR
    
    cTrInventarioDescargos = "SELECT (c_Documento + c_CodLocalidad) AS PK, '" & StellarMensaje(3009) & "' AS c_TipoMov, TR_ORDEN_PRODUCCION.*, MA_PRODUCTOS.Cant_Decimales, MA_PRODUCTOS.c_Descri, MA_PRODUCTOS.c_Presenta AS Presentacion FROM TR_ORDEN_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo WHERE 1 = 1 AND b_Producir = 0 " & AuxCriterioTR
    
    Consultas(0) = cTrInventarioCargos
    Consultas(1) = cTrInventarioDescargos
    
    Relacion(0) = "PK"
    Relacion(1) = "PK"
    
    Alias(0) = "TRCARGOS"
    Alias(1) = "TRDESCARGOS"
    
    Call Apertura_RecordsetC(RsMaInventario)
    
    CCriterio = ShapeStrAdv(cMaInventario, "MAORDENPRODUCCION", Consultas, Alias, Relacion, Relacion, Alias)
    
    'Debug.Print cTrInventarioCargos
    'Debug.Print cTrInventarioDescargos
    'Debug.Print cMaInventario
    
    RsMaInventario.Open CCriterio, Ent.SHAPE_ADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
    RsView.Open "select top 1 * from ma_productos", Ent.SHAPE_ADM, adOpenForwardOnly, adLockReadOnly, adCmdText
        
    While Not RsMaInventario.EOF
        If RsMaInventario("TRCARGOS").Value.EOF Then
            RsMaInventario.Delete
        End If
        RsMaInventario.MoveNext
    Wend
    
    If Not (RsMaInventario.EOF And RsMaInventario.BOF) Then
        RsMaInventario.MoveFirst
    End If
        
    If Not RsMaInventario.EOF Then
        
        If Not RsMaInventario("TRCARGOS").Value.EOF Then
            
            If Numero = 2 Then
                Call GenerarXML_ListadoOrdenes(RsMaInventario)
            Else
                
                Dim Rep_Formulas As Object: Set Rep_Formulas = FrmAppLink.GetDefaultTextDataReport
                
                Call Escribir_DTR_Ordenes(RsMaInventario, Rep_Formulas, "MA_PRODUCIR", "LBL_TEXT")
                Call REPO_CABE(Rep_Formulas, "" & StellarMensaje(5014) & "")
                
                Set Rep_Formulas.DataSource = RsView
                
                If Numero = 0 Then
                    Rep_Formulas.Show vbModal
                Else
                    Rep_Formulas.PrintReport (True)
                End If
                
                Set Rep_Formulas = Nothing
                
            End If
        Else
            Call Mensaje(True, "" & StellarMensaje(16441) & "")
        End If
        
    Else
        Call Mensaje(True, "" & StellarMensaje(16441) & "")
    End If
    
    RsMaInventario.Close
    Ent.SHAPE_ADM.Close
    
End Sub

Sub Escribir_DTR_Ordenes(RsTemp As ADODB.Recordset, _
Reporte As Object, Seccion As String, Objeto As String)
    
    Dim RsTrInventarioCargos As New ADODB.Recordset, RsTrInventarioDescargos As New ADODB.Recordset
    Dim CostosMerma As Double, CostosProduccion As Double
    
    Escribir_Txt = False
    
    If Not RsTemp.EOF Then
        
        While Not RsTemp.EOF
            
            Set RsTrInventarioCargos = RsTemp("TRCARGOS").Value
            Set RsTrInventarioDescargos = RsTemp("TRDESCARGOS").Value
            
            Escribir LeftJust(UCase(StellarMensaje(10126)) & ":", 25) & " " & RsTemp!c_Documento, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(143)) & ":", 25) & " " & RsTemp!c_Descripcion, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(12)) & ":", 25) & " " & RsTemp!localidad, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(15500)) & ":", 25) & " " & SDate(RsTemp!d_Fecha), Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(2954)) & ":", 25) & " " & SDate(RsTemp!d_Fecha_Produccion), Reporte, Seccion, Objeto
            If Trim(RsTemp!c_LineaProduccion) <> Empty Then Escribir LeftJust(UCase(StellarMensaje(2947)) & ":", 25) & " " & RsTemp!c_LineaProduccion, Reporte, Seccion, Objeto
            If Trim(RsTemp!c_Turno) <> Empty Then Escribir LeftJust(UCase(StellarMensaje(2966)) & ":", 25) & " " & RsTemp!c_Turno, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(1229)) & ":", 25) & " " & RsTemp!Usuario, Reporte, Seccion, Objeto
            Escribir LeftJust(UCase(StellarMensaje(16324)) & ":", 25) & " " & RsTemp!c_DesMoneda, Reporte, Seccion, Objeto
            If (FrmAppLink.GetNivelUsuario >= FrmAppLink.PRDNivelVerCostosDirectos And FrmAppLink.PRDNivelVerCostosDirectos > 0) Then
                Escribir LeftJust(UCase(StellarMensaje(5004)) & ":", 25) & " " & FormatNumber(RsTemp!n_CostoDir, Std_Decm), Reporte, Seccion, Objeto
            End If
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "PRODUCTOS A UTILIZAR", Reporte, Seccion, Objeto
            Escribir "" & UCase(StellarMensaje(5006)) & "", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir LeftJust("Producto", 18) & " " & LeftJust("Descripci�n", 32) & " " & RightJust("Cantidad", 14) & " " & LeftJust("Presentaci�n", 15) & " " & RightJust("Costo Unit.", 16) & " " & RightJust("Subtotal", 16), Reporte, Seccion, Objeto
            Escribir LeftJust("" & StellarMensaje(5010) & "", 18) & " " & LeftJust("" & StellarMensaje(143) & "", 32) & " " & RightJust("" & StellarMensaje(3001) & "", 14) & " " & LeftJust("" & FrmAppLink.PRDLabelCampoPresenta & "", 15) & " " & RightJust("" & StellarMensaje(151) & "", 16) & " " & RightJust("" & StellarMensaje(139) & "", 16), Reporte, Seccion, Objeto
            
            CostosMerma = 0
            CostosProduccion = 0
            
            While Not RsTrInventarioDescargos.EOF
                Escribir LeftJust(RsTrInventarioDescargos!c_CodArticulo, 18) & " " & LeftJust(Mid(RsTrInventarioDescargos!c_Descri, 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Cantidad, RsTrInventarioDescargos!Cant_Decimales), 14) & " " & LeftJust(RsTrInventarioDescargos!Presentacion, 15) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Costo, Std_Decm), 16) & " " & RightJust(FormatNumber(RsTrInventarioDescargos!n_Subtotal, Std_Decm), 16), Reporte, Seccion, Objeto
                CostosProduccion = CostosProduccion + (RsTrInventarioDescargos!n_Cantidad * RsTrInventarioDescargos!n_Costo)
                RsTrInventarioDescargos.MoveNext
            Wend
            
            'IMPRESION DE LOS MONTO POR MERMA Y PRODUCCION
            
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "." & Space(20) & "  Monto de Producci�n: " & RightJust(FormatNumber(CostosProduccion + rsTemp!n_Cantidad_compra, Std_Decm), 16) & " (No Incluye Costos Directos de Producci�n)", Reporte, Seccion, Objeto
            Escribir "." & Space(20) & "  " & StellarMensaje(2835) & ": " & RightJust(FormatNumber(CostosProduccion + RsTemp!n_CostoDir, Std_Decm), 16) & " (" & StellarMensaje(2836) & ")", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir "PRODUCTOS A PRODUCIR", Reporte, Seccion, Objeto
            Escribir "" & UCase(StellarMensaje(5008)) & "", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            'Escribir LeftJust("Producto", 18) & " " & LeftJust("Descripci�n", 32) & " " & RightJust("Cantidad", 14) & " " & LeftJust("Presentaci�n", 15) & " " & RightJust("Costo Unit.", 16) & " " & RightJust("Subtotal", 16), Reporte, Seccion, Objeto
            Escribir LeftJust("" & StellarMensaje(5010) & "", 18) & " " & LeftJust("" & StellarMensaje(143) & "", 32) & " " & RightJust("" & StellarMensaje(3001) & "", 14) & " " & LeftJust("" & FrmAppLink.PRDLabelCampoPresenta & "", 15) & " " & RightJust("" & StellarMensaje(151) & "", 16) & " " & RightJust("" & StellarMensaje(139) & "", 16), Reporte, Seccion, Objeto
            
            While Not RsTrInventarioCargos.EOF
                'Escribir LeftJust(RsTrInventarioCargos!c_CodArticulo, 18) & " " & LeftJust(Mid(RsTrInventarioCargos!c_Descri, 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Cantidad, 2), 14) & " " & LeftJust(RsTrInventarioCargos!Presentacion, 15) & RightJust(FormatNumber(RsTrInventarioCargos!n_Precio, Std_Decm), 16), Reporte, Seccion, Objeto
                Escribir LeftJust(RsTrInventarioCargos!c_CodArticulo, 18) & " " & LeftJust(Mid(isDBNull(RsTrInventarioCargos!c_Descri, "N/A"), 1, 30), 32) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Cantidad, isDBNull(RsTrInventarioCargos!Cant_Decimales, 0)), 14) & " " & LeftJust(isDBNull(RsTrInventarioCargos!Presentacion, "N/A"), 15) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Costo, Std_Decm), 16) & " " & RightJust(FormatNumber(RsTrInventarioCargos!n_Subtotal, Std_Decm), 16), Reporte, Seccion, Objeto
                RsTrInventarioCargos.MoveNext
            Wend
            
            Escribir "", Reporte, Seccion, Objeto
            Escribir StellarMensaje(137) & " " & RsTemp!c_Observacion, Reporte, Seccion, Objeto
            
            Escribir ".", Reporte, Seccion, Objeto
            Escribir ".", Reporte, Seccion, Objeto
            Escribir_Txt = True
            
            RsTemp.MoveNext
            
        Wend
        
        RsTemp.MoveFirst
        
    End If
    
End Sub

'Autor:          Luis Arraga
'Fecha Creaci�n: Oct 2019
'Descripcion:    Codigo para generar documento XML
Private Sub GenerarXML_ListadoOrdenes(Rs As ADODB.Recordset)
    
    On Error GoTo Error
    
    Dim RutaGenerarXML As String
    Dim Campos As Dictionary: Set Campos = New Dictionary
    Dim TmpCount As Long: TmpCount = -1
    
    rutaXml.Filter = "XML (*.xml)"
    rutaXml.DefaultExt = "xml"
    
    rutaXml.CancelError = True
   
    rutaXml.ShowSave
    
    RutaGenerarXML = rutaXml.FileName
        
    Rs.MoveFirst
    
    'Select Case Modalidad.ListIndex
    
        'Case 0 'Resumida
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Documento", StellarMensaje(10016))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Localidad", StellarMensaje(12))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Descripcion", RemoverAcentos(StellarMensaje(143)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("d_Fecha", StellarMensaje(15500))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("d_Fecha_Produccion", SpaceToUnderscore(RemoverAcentos(StellarMensaje(2954))))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("LineaProduccion", SpaceToUnderscore(RemoverAcentos(StellarMensaje(2947))))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("TurnoProduccion", SpaceToUnderscore(RemoverAcentos(StellarMensaje(15513))))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("LineaProduccion", SpaceToUnderscore(RemoverAcentos(StellarMensaje(2947))))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Relacion", SpaceToUnderscore(RemoverAcentos(StellarMensaje(2611))))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Observacion", SpaceToUnderscore(RemoverAcentos(StellarMensaje(10122))))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Usuario", StellarMensaje(1229))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_DesMoneda", StellarMensaje(16324))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_CostoDir", SpaceToUnderscore(StellarMensaje(5004)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Total", SpaceToUnderscore(StellarMensaje(7019)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_TipoMov_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(10091)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_CodArticulo_TRDESCARGOS", "D_" & RemoverAcentos(StellarMensaje(142)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Descri_TRDESCARGOS", "D_" & StellarMensaje(16480))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Presentacion_TRDESCARGOS", "D_" & SpaceToUnderscore(RemoverAcentos(FrmAppLink.PRDLabelCampoPresenta)))
            
            If ManejaLote Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_IDLote_TRDESCARGOS", "D_" & RemoverAcentos(StellarMensaje(2937)))
            End If
                        
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Cantidad_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(2957)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Cant_Utilizada_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(2959)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Costo_TRDESCARGOS", "D_" & SpaceToUnderscore(StellarMensaje(151)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Subtotal_TRDESCARGOS", "D_" & StellarMensaje(139))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_PorcMerma_TRDESCARGOS", "D_" & "Porc_" & StellarMensaje(3036))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_MontoMerma_TRDESCARGOS", "D_" & StellarMensaje(3036))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_TipoMov_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(10091)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_CodArticulo_TRCARGOS", "C_" & RemoverAcentos(StellarMensaje(142)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("c_Descri_TRCARGOS", "C_" & StellarMensaje(16480))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("Presentacion_TRCARGOS", "C_" & SpaceToUnderscore(RemoverAcentos(FrmAppLink.PRDLabelCampoPresenta)))
            
            If ManejaLote Then
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array("c_IDLote_TRCARGOS", "C_" & RemoverAcentos(StellarMensaje(2937)))
            End If
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Cantidad_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(2957)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Cant_Realizada_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(2960)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Costo_TRCARGOS", "C_" & SpaceToUnderscore(StellarMensaje(151)))
            
            TmpCount = TmpCount + 1
            Campos(TmpCount) = Array("n_Subtotal_TRCARGOS", "C_" & StellarMensaje(139))
            
    'End Select
    
    Call Crear_Xml_Definido(Rs, RutaGenerarXML, Campos, True)
    
    CmdOpenXML.Visible = True
    
    ShowTooltip "Click Izq. -> Abrir con Excel" & GetLines & _
    "Click Med. -> Abrir con aplicaci�n predeterminada para archivos XML" & GetLines & _
    "Click Der. -> Ocultar el bot�n." _
    & GetLines & Space(21) & "|" & GetLines & Space(21) & "V" & _
    GetLines, CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub departamento_Click()
    If ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub departamento_GotFocus()
    Set CampoT = departamento
End Sub

Private Sub bdpto_Click()
    departamento.SetFocus
    Call departamento_KeyDown(vbKeyF2, 0)
End Sub

Private Sub departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            
            Tabla = "MA_DEPARTAMENTOS"
            Titulo = UCase(Stellar_Mensaje(361)) '"D E P A R T A M E N T O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            departamento.Tag = Empty
            departamento.Text = Empty
            LBL_DEPARTAMENTO.Caption = Empty
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub departamento_LostFocus()
    If departamento.Text <> "" Then
        Set Forma = Me
        Call Carga_DGS(departamento, "MA_DEPARTAMENTOS", 0)
    Else
        Call departamento_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub grupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub grupo_GotFocus()
    Set CampoT = grupo
End Sub

Private Sub bgrupo_Click()
    grupo.SetFocus
    Call grupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub grupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_GRUPOS"
            Titulo = UCase(Stellar_Mensaje(362)) 'G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub grupo_LostFocus()
    If grupo.Text <> "" Then
        Set Forma = Me
        Call Carga_DGS(grupo, "MA_GRUPOS", 1)
    Else
        Call grupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub bsubgrupo_Click()
    subgrupo.SetFocus
    Call subgrupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub subgrupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub subgrupo_GotFocus()
    Set CampoT = subgrupo
End Sub

Private Sub subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_SUBGRUPOS"
            Titulo = UCase(Stellar_Mensaje(363)) 'S U B - G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub subgrupo_LostFocus()
    If subgrupo.Text <> "" Then
        Set Forma = Me
        Call Carga_DGS(subgrupo, "MA_SUBGRUPOS", 2)
    Else
        Call subgrupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Public Sub Buscar_Produccion(tipo As String, orden As String)
    txt_orden.Text = orden
End Sub

Public Sub Buscar_Orden_Produccion(ByVal tipo As String, _
ByVal orden As String, ByVal CodLocalidad As String)
    txt_orden.Text = orden
    txt_sucursal.Text = CodLocalidad
    txt_sucursal_LostFocus
End Sub
