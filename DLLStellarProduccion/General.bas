Attribute VB_Name = "General"
Public Enum Alienacion
    flexDefault = -1
    flexAlignLeftTop = 0
    flexAlignLeftCenter = 1
    flexAlignLeftBottom = 2
    flexAlignCenterTop = 3
    flexAlignCenterCenter = 4
    flexAlignCenterBottom = 5
    flexAlignRightTop = 6
    flexAlignRightCenter = 7
    flexAlignRightBottom = 8
    flexAlignGeneral = 9
End Enum

Public Function StellarMensajeLocal(pResourceID As Long) As String
    StellarMensajeLocal = Stellar_Mensaje_Local(pResourceID, True)
End Function

Public Function Stellar_Mensaje_Local(Mensaje As Long, _
Optional ByVal pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje_Local = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje_Local = "CHK_RES"
    
End Function

Public Sub MSGridAsign(ByRef GridObj As Object, Fila As Integer, Columna As Integer, Texto As Variant, Optional Tamano As Long = -1, Optional Alinear As Alienacion, Optional Formato As String = "")
    GridObj.Row = Fila
    GridObj.Col = Columna
    If Tamano >= 0 Then
        GridObj.ColWidth(GridObj.ColSel) = Tamano
    End If
    If Alinear > -1 Then
        GridObj.CellAlignment = Alinear
    End If
    If Formato <> "" Then
        Texto = Format(Texto, Formato)
    End If
    GridObj.Text = IIf(IsNull(Texto), "", Texto)
End Sub

Sub SetDefMSGrid(ByRef GridObj As Object, Fila As Integer, Columna As Integer)
    GridObj.Col = Columna
    GridObj.Row = Fila
End Sub

Function MSGridRecover(ByRef GridObj As Object, ByVal Fila As Integer, ByVal Columna As Integer) As Variant
    Dim Tcol As Integer, Trow As Integer
    Tcol = GridObj.Col
    Trow = GridObj.Row
    GridObj.Row = Fila
    GridObj.Col = Columna
    MSGridRecover = GridObj.Text
    SetDefMSGrid GridObj, Trow, Tcol
End Function
