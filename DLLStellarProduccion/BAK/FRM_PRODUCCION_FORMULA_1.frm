VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FRM_PRODUCCION_FORMULA_1 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   405
   ClientWidth     =   15330
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FRM_PRODUCCION_FORMULA_1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   Tag             =   "FORMULAS"
   Begin VB.Frame Frame3 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   31
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.TextBox Mtotal 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   390
      Left            =   11820
      Locked          =   -1  'True
      TabIndex        =   27
      Top             =   6585
      Width           =   2160
   End
   Begin VB.TextBox txtedit2 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   2070
      TabIndex        =   26
      Top             =   8595
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.TextBox txtedit 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1980
      TabIndex        =   25
      Top             =   4320
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.TextBox MSK_FACTOR 
      Alignment       =   1  'Right Justify
      CausesValidation=   0   'False
      Enabled         =   0   'False
      Height          =   315
      Left            =   5355
      TabIndex        =   23
      Text            =   "0"
      Top             =   1425
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox dbmoneda 
      Enabled         =   0   'False
      Height          =   315
      Left            =   315
      MaxLength       =   10
      TabIndex        =   22
      Top             =   1395
      Visible         =   0   'False
      Width           =   1365
   End
   Begin VB.Frame frame_datos 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ClipControls    =   0   'False
      Height          =   1080
      Left            =   240
      TabIndex        =   18
      Top             =   1755
      Width           =   14775
      Begin VB.CommandButton CmdLocalidad 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   315
         Left            =   11220
         Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   180
         Width           =   315
      End
      Begin VB.TextBox txt_Sucursal 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10395
         MaxLength       =   20
         TabIndex        =   1
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   180
         Width           =   675
      End
      Begin VB.TextBox txt_costosdirectos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1980
         TabIndex        =   3
         Top             =   540
         Width           =   2820
      End
      Begin VB.TextBox txt_descripcion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1980
         MaxLength       =   50
         TabIndex        =   0
         Top             =   195
         Width           =   6825
      End
      Begin VB.Label lblDescLocalidad 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   600
         Left            =   11715
         TabIndex        =   37
         Top             =   180
         Width           =   2940
      End
      Begin VB.Label lblSucursal 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Localidad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9360
         TabIndex        =   36
         Top             =   240
         Width           =   795
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "(No considerar el costo de los productos a utilizar)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5100
         TabIndex        =   21
         Top             =   585
         Width           =   4320
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Costos Directos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   270
         TabIndex        =   20
         Top             =   585
         Width           =   1530
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   285
         TabIndex        =   19
         Top             =   240
         Width           =   1110
      End
   End
   Begin VB.TextBox txt_merma 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   390
      Left            =   2370
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   6585
      Width           =   2280
   End
   Begin VB.TextBox txt_monto 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   390
      Left            =   6885
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   6585
      Width           =   2445
   End
   Begin MSFlexGridLib.MSFlexGrid Preparado 
      CausesValidation=   0   'False
      Height          =   3105
      Left            =   240
      TabIndex        =   4
      Top             =   3390
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   5477
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      Enabled         =   -1  'True
      FocusRect       =   2
      HighLight       =   0
      GridLinesFixed  =   0
      MergeCells      =   4
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox CoolBar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1080
      ScaleWidth      =   21270
      TabIndex        =   14
      Top             =   421
      Width           =   21270
      Begin MSComctlLib.Toolbar BarraP 
         Height          =   810
         Left            =   180
         TabIndex        =   6
         Top             =   105
         Width           =   10050
         _ExtentX        =   17727
         _ExtentY        =   1429
         ButtonWidth     =   1746
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   11
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar"
               Key             =   "Agregar"
               Object.ToolTipText     =   "Agregar una Nueva Ficha"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "Buscar una Ficha"
               ImageIndex      =   1
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "BFOR"
                     Text            =   "Buscar F�rmula"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Enabled         =   0   'False
                     Key             =   "BPRO"
                     Text            =   "Buscar Producto"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Modificar"
               Key             =   "Modificar"
               Object.ToolTipText     =   "Modificar una Ficha"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Cancelar"
               Key             =   "Cancelar"
               Object.ToolTipText     =   "Cancelar esta Ficha"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Reimprimir"
               Key             =   "Reimprimir"
               Object.Tag             =   "Reimprimir"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Borrar"
               Key             =   "Eliminar"
               Object.ToolTipText     =   "Borrar esta Ficha"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "Grabar esta Ficha"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir del Fichero"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Opciones"
               Key             =   "Opciones"
               Object.ToolTipText     =   "Opciones del Sistema"
               ImageIndex      =   6
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Ayuda"
                     Text            =   "Ayuda"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Moneda"
                     Text            =   "Cambiar Moneda"
                  EndProperty
               EndProperty
            EndProperty
         EndProperty
      End
      Begin VB.Label lbl_fecha 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "01/01/2015"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   11040
         TabIndex        =   35
         Top             =   675
         Width           =   3495
      End
      Begin VB.Label lbl_consecutivo 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "000000000"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   12840
         TabIndex        =   34
         Top             =   195
         Width           =   1815
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   33
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lbl_concepto 
         BackStyle       =   0  'Transparent
         Caption         =   "F�rmula No:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   32
         Top             =   195
         Width           =   2415
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   1095
         Index           =   3
         Left            =   10440
         Top             =   0
         Width           =   4575
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   90
      Top             =   1275
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":6A8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":881E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":A5B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":C342
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":E0D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":FE66
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":11BF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":128D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_FORMULA_1.frx":135AC
            Key             =   "Reimprimir"
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid Producir 
      CausesValidation=   0   'False
      Height          =   2565
      Left            =   240
      TabIndex        =   5
      Top             =   7830
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   4524
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      Enabled         =   -1  'True
      HighLight       =   0
      GridLinesFixed  =   0
      MergeCells      =   4
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00AE5B00&
      X1              =   2745
      X2              =   14675
      Y1              =   7635
      Y2              =   7635
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00AE5B00&
      X1              =   2745
      X2              =   14675
      Y1              =   3200
      Y2              =   3200
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Monto Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   10140
      TabIndex        =   28
      Top             =   6585
      Width           =   1200
   End
   Begin VB.Label lbl_moneda 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1755
      TabIndex        =   24
      Top             =   1425
      Visible         =   0   'False
      Width           =   3525
   End
   Begin VB.Shape Shape1 
      Height          =   2115
      Index           =   0
      Left            =   240
      Top             =   7830
      Width           =   14775
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00E7E8E8&
      Caption         =   "Productos a Producir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Left            =   360
      TabIndex        =   17
      Top             =   7470
      Width           =   2205
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00E7E8E8&
      Caption         =   "Productos a Utilizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Left            =   360
      TabIndex        =   16
      Top             =   3030
      Width           =   2055
   End
   Begin VB.Label LBL_TEMP 
      BackStyle       =   0  'Transparent
      Height          =   210
      Left            =   10950
      TabIndex        =   15
      Top             =   9840
      Visible         =   0   'False
      Width           =   840
   End
   Begin VB.Label lbl_simbolo2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   5445
      TabIndex        =   13
      Top             =   7020
      Width           =   615
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Merma"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   1215
      TabIndex        =   12
      Top             =   6585
      Width           =   690
   End
   Begin VB.Label lbl_simbolo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   10740
      TabIndex        =   10
      Top             =   5280
      Width           =   615
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Subtotal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   5670
      TabIndex        =   9
      Top             =   6585
      Width           =   780
   End
   Begin VB.Label QUIEN_SOY 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "F�RMULAS DE PRODUCCI�N"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   1110
      TabIndex        =   7
      Top             =   2430
      Visible         =   0   'False
      Width           =   4170
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      Height          =   3720
      Index           =   2
      Left            =   240
      Top             =   3390
      Width           =   14775
   End
End
Attribute VB_Name = "FRM_PRODUCCION_FORMULA_1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ModifFlag As Boolean, Tecla_Pulsada As Boolean, Who_Ami As Integer
Dim cproducto As String, cEureka As String, cRecetas As String, Criterio As String, cinventario As String
Dim rsProducto As New ADODB.Recordset, RsRecetas As New ADODB.Recordset, rsPmoneda As New ADODB.Recordset
Dim RsInventario As New ADODB.Recordset, RsCodigos As New ADODB.Recordset
Dim Valor_Temp As Variant, Lines As Integer, Cols As Integer
Dim CuantosDel As Integer, CostoActivo As String, DecGrid(0) As Integer
Dim RsEureka As New ADODB.Recordset, FlgAdd As Boolean, FlgUpd As Boolean
Dim Cambio As Double, Cod_Moneda As String, GridLleno As Boolean, LastConsecutivo As String

Private ManejaMermaExplicita            As Boolean

Private Enum ColPre
    LDeci
    Producto
    Descripci�n
    Canti
    Presentaci�n
    PorcMerma
    CostoPreXMerma
    CostoPre
    CostoProd
    PorcPartCant
    Info
    [ColCount]
End Enum

Private Enum ColPro
    LDeci
    Producto
    Descripci�n
    Canti
    Presentaci�n
    CostoPre
    CostoProd
    Factor
    PorcPartCant
    Info
    [ColCount]
End Enum

Private Sub BarraP_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "AGREGAR"
            Call Form_KeyDown(vbKeyF3, 0)
        Case "BUSCAR"
            Call Form_KeyDown(vbKeyF2, 0)
        Case "MODIFICAR"
            Call Form_KeyDown(vbKeyF5, 0)
        Case "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        Case "REIMPRIMIR"
            Call Form_KeyDown(vbKeyF8, 0)
        Case "ELIMINAR"
            Call Form_KeyDown(vbKeyF6, 0)
        Case "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        Case "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        Case "OPCIONES"
    End Select
End Sub

Private Sub BarraP_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Select Case UCase(ButtonMenu.Key)
        Case "BFOR"
            Call MenuAccess(False, False)
            Tecla_Pulsada = True
            Set Forma = FRM_PRODUCCION_FORMULA_1
            Set Campo_Txt = lbl_consecutivo
            Set Campo_Lbl = txt_descripcion
            LastConsecutivo = lbl_consecutivo.Caption
            Call MAKE_VIEW("ma_produccion", "c_formula", "c_Descripcion", StellarMensaje(2818), FRM_PRODUCCION_FORMULA_1, "GENERICO", , Campo_Txt, Campo_Lbl)
            
            Tecla_Pulsada = False
            If LastConsecutivo <> lbl_consecutivo.Caption Then
                Call Buscar_Formula(lbl_consecutivo.Caption)
            End If
        Case "BPRO"
            'Call Buscar_Producto(Codigo)
            Select Case Who_Ami
                Case 1
                    If Not txtedit.Visible Then
                        Producto_Imp = ""
                        Tecla_Pulsada = True
                        If Preparado.ColSel = ColPre.Producto Then
                            Call MAKE_VIEW("ma_productos", "c_Codigo", "c_Descri", StellarMensaje(254), Me, "PRODUCTOS_GENERAL", , Campo_Txt, Campo_Lbl)
                            If txtedit <> "" Then Preparado_KeyPress (vbKeyReturn)
                            txtedit.MaxLength = 15
                            txtedit.Text = Producto_Imp
                            'Preparado.Text = producto_imp
                            oTeclado.Key_Return
                            Call Preparado_KeyPress(vbKeyReturn)
                        End If
                        Tecla_Pulsada = False
                    End If
                Case 2
                    If Not txtedit2.Visible Then
                        Producto_Imp = ""
                        Tecla_Pulsada = True
                        If Producir.ColSel = ColPro.Producto Then
                            Call MAKE_VIEW("ma_productos", "c_Codigo", "c_Descri", StellarMensaje(254), Me, "PRODUCTOS_GENERAL", , Campo_Txt, Campo_Lbl)
                            If txtedit2 <> "" Then Producir_KeyPress (vbKeyReturn)
                            txtedit2.MaxLength = 15
                            txtedit2.Text = Producto_Imp
                            'Preparado.Text = producto_imp
                            oTeclado.Key_Return
                            Call Producir_KeyPress(vbKeyReturn)
                        End If
                        Tecla_Pulsada = False
                    End If
            End Select
        Case "AYUDA"
            'Call Ayuda
        Case "MONEDA"
            Call MenuAccess(True, False)
            Call Cambiar_Moneda
    End Select
    'Donde Tipo: DWT = Espera, DCO = Completado
    '      Numero: Numero de la Formula
End Sub

Private Sub CmdLocalidad_Click()
    Tecla_Pulsada = True
    SafeFocus txt_Sucursal
    Call txt_sucursal_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub Form_Load()
    
    'Call AbrirTodo
    'Preparaci�n del Grid
    Call AjustarPantalla(Me)
    
    BarraP.Buttons(1).Caption = Stellar_Mensaje(197) 'agregar
    BarraP.Buttons(2).Caption = Stellar_Mensaje(102) 'buscar
    BarraP.Buttons(2).ButtonMenus(1).Text = Stellar_Mensaje(5001) 'Buscar F�rmula
    BarraP.Buttons(2).ButtonMenus(2).Text = Stellar_Mensaje(5002) 'Buscar Producto
    BarraP.Buttons(3).Caption = Stellar_Mensaje(207) 'modificar
    BarraP.Buttons(5).Caption = Stellar_Mensaje(105) 'cancelar
    BarraP.Buttons(6).Caption = Stellar_Mensaje(106) 'reimprimir
    BarraP.Buttons(7).Caption = Stellar_Mensaje(208) 'borrar
    BarraP.Buttons(8).Caption = Stellar_Mensaje(103) 'grabar
    BarraP.Buttons(10).Caption = Stellar_Mensaje(107) 'salir
    BarraP.Buttons(11).Caption = Stellar_Mensaje(108) 'opciones
    BarraP.Buttons(11).ButtonMenus(1).Text = Stellar_Mensaje(7) 'Ayuda
    BarraP.Buttons(11).ButtonMenus(2).Text = Stellar_Mensaje(120) 'Cambiar Moneda
    
    lbl_concepto.Caption = Stellar_Mensaje(5003) 'formula n�
    Label5.Caption = Stellar_Mensaje(128) 'fecha
    
    Label9.Caption = Stellar_Mensaje(143) 'descripcion
    Label6.Caption = Stellar_Mensaje(5004) 'costos directos
    Label4.Caption = Stellar_Mensaje(5005) 'no considerar el costo de los productos a utilizar
    lblSucursal.Caption = StellarMensaje(12) 'Sucursal
    
    Label1.Caption = Stellar_Mensaje(5006) 'productos a utilizar
    Label2.Caption = Stellar_Mensaje(3036) 'merma
    Label8.Caption = Stellar_Mensaje(139) 'subtotal
    Label7.Caption = Stellar_Mensaje(5007) 'monto total
    
    Label3.Caption = Stellar_Mensaje(5008) 'productos a producir
    
    lbl_Organizacion.Caption = FrmAppLink.GetLblOrganizacion
    
    ManejaMermaExplicita = FrmAppLink.PRDManejaMermaExplicita
    
    ' Para Preparado
    Preparado.Cols = ColPre.ColCount
    
    Call MSGridAsign(Preparado, 0, ColPre.LDeci, "LDec", 0, flexAlignRightCenter)
    
    If ManejaMermaExplicita Then
        
        Call MSGridAsign(Preparado, 0, ColPre.Producto, Stellar_Mensaje(5010), 1800, flexAlignCenterCenter) 'producto
        Call MSGridAsign(Preparado, 0, ColPre.Descripci�n, Stellar_Mensaje(143), 3000, flexAlignCenterCenter) 'descripcion
        Call MSGridAsign(Preparado, 0, ColPre.Canti, Stellar_Mensaje(3001), 1100, flexAlignRightCenter) 'cantidad
        Call MSGridAsign(Preparado, 0, ColPre.Presentaci�n, FrmAppLink.PRDLabelCampoPresenta, 1600, flexAlignCenterCenter) 'presentacion
        Call MSGridAsign(Preparado, 0, ColPre.PorcMerma, Stellar_Mensaje(5013), 1100, flexAlignRightCenter) '% merma
        Call MSGridAsign(Preparado, 0, ColPre.CostoPreXMerma, Stellar_Mensaje(5011), 1600, flexAlignRightCenter) 'costo x %merma
        Call MSGridAsign(Preparado, 0, ColPre.CostoPre, Replace(Stellar_Mensaje(151), ":", ""), 1520, flexAlignRightCenter)   'costo
        Call MSGridAsign(Preparado, 0, ColPre.CostoProd, Replace(Stellar_Mensaje(2938), ":", ""), 1520, flexAlignRightCenter)
        Call MSGridAsign(Preparado, 0, ColPre.PorcPartCant, "Part.", 800, flexAlignRightCenter)
        
    Else
        
        Call MSGridAsign(Preparado, 0, ColPre.Producto, Stellar_Mensaje(5010), 1800, flexAlignCenterCenter) 'producto
        Call MSGridAsign(Preparado, 0, ColPre.Descripci�n, Stellar_Mensaje(143), 3800, flexAlignCenterCenter) 'descripcion
        Call MSGridAsign(Preparado, 0, ColPre.Canti, Stellar_Mensaje(3001), 1300, flexAlignRightCenter) 'cantidad
        Call MSGridAsign(Preparado, 0, ColPre.Presentaci�n, FrmAppLink.PRDLabelCampoPresenta, 2000, flexAlignCenterCenter) 'presentacion
        Call MSGridAsign(Preparado, 0, ColPre.PorcMerma, Stellar_Mensaje(5013), 0, flexAlignRightCenter) '% merma
        Call MSGridAsign(Preparado, 0, ColPre.CostoPreXMerma, Stellar_Mensaje(5011), 0, flexAlignRightCenter) 'costo x %merma
        Call MSGridAsign(Preparado, 0, ColPre.CostoPre, Replace(Stellar_Mensaje(151), ":", ""), 2350, flexAlignRightCenter) 'costo
        Call MSGridAsign(Preparado, 0, ColPre.CostoProd, Replace(Stellar_Mensaje(2938), ":", ""), 2350, flexAlignRightCenter)
        Call MSGridAsign(Preparado, 0, ColPre.PorcPartCant, "Part.", 800, flexAlignRightCenter)
        
    End If
    
    Call MSGridAsign(Preparado, 0, ColPre.Info, "Info", _
    IIf(StellarMensaje(555) = "es-VE", 400, 0), IIf(StellarMensaje(555) = "es-VE", flexAlignCenterCenter, flexAlignRightCenter))
    
    Call SetDefMSGrid(Preparado, 1, ColPre.Producto)
    
    'Para Producir
    Producir.Cols = ColPro.ColCount
    
    Call MSGridAsign(Producir, 0, ColPro.LDeci, "LDec", 0, flexAlignRightCenter)
    Call MSGridAsign(Producir, 0, ColPro.Producto, Stellar_Mensaje(5010), 1800, flexAlignCenterCenter) 'producto
    
    If PRDManejaFactor Then
        Call MSGridAsign(Producir, 0, ColPro.Descripci�n, Stellar_Mensaje(143), 4180, flexAlignCenterCenter) 'descripcion
        Call MSGridAsign(Producir, 0, ColPro.Canti, Stellar_Mensaje(3001), 1100, flexAlignCenterCenter) 'cantidad
        Call MSGridAsign(Producir, 0, ColPro.Presentaci�n, FrmAppLink.PRDLabelCampoPresenta, 1800, flexAlignCenterCenter) 'presentacion
        Call MSGridAsign(Producir, 0, ColPro.CostoPre, Stellar_Mensaje(151), 1750, flexAlignRightCenter)
        Call MSGridAsign(Producir, 0, ColPro.CostoProd, Stellar_Mensaje(2938), 1750, flexAlignRightCenter) 'costo prod.
        Call MSGridAsign(Producir, 0, ColPro.Factor, Replace(Stellar_Mensaje(221), ":", ""), 900, flexAlignCenterCenter) 'factor
        Call MSGridAsign(Producir, 0, ColPro.PorcPartCant, "Part.", 800, flexAlignRightCenter)
    Else
        Call MSGridAsign(Producir, 0, ColPro.Descripci�n, Stellar_Mensaje(143), 3800, flexAlignCenterCenter) 'descripcion
        Call MSGridAsign(Producir, 0, ColPro.Canti, Stellar_Mensaje(3001), 1300, flexAlignCenterCenter) 'cantidad
        Call MSGridAsign(Producir, 0, ColPro.Presentaci�n, FrmAppLink.PRDLabelCampoPresenta, 2000, flexAlignCenterCenter) 'presentacion
        Call MSGridAsign(Producir, 0, ColPro.CostoPre, Stellar_Mensaje(151), 2150, flexAlignRightCenter)
        Call MSGridAsign(Producir, 0, ColPro.CostoProd, Stellar_Mensaje(2938), 2150, flexAlignRightCenter) 'costo prod.
        Call MSGridAsign(Producir, 0, ColPro.Factor, "", 0, flexAlignCenterCenter) 'factor
        Call MSGridAsign(Producir, 0, ColPro.PorcPartCant, "Part.", 800, flexAlignRightCenter)
    End If
    
    Call MSGridAsign(Producir, 0, ColPro.Info, "Info", _
    IIf(StellarMensaje(555) = "es-VE", 400, 0), IIf(StellarMensaje(555) = "es-VE", flexAlignCenterCenter, flexAlignRightCenter))
    
    Call SetDefMSGrid(Producir, 1, ColPro.Producto)
    
    lbl_fecha.Caption = Format(Now, "short date")
    
    'Leer Consecutivo
    Call Nuevo_Consecutivo
    Call Ini_Moneda
    
    GridLleno = False
    txt_monto.Text = FormatNumber(0, Std_Decm)
    txt_merma.Text = FormatNumber(0, Std_Decm)
    txt_costosdirectos.Text = FormatNumber(0, Std_Decm)
    
    ' Carga de Reglas Comerciales para el c�lculo del costo de producci�n.
    
    Ent.RsReglasComerciales.Requery
    
    Select Case Ent.RsReglasComerciales!Estimacion_Inv
        Case 0
            CostoActivo = "n_CostoAct"
        Case 1
            CostoActivo = "n_CostoAnt"
        Case 2
            CostoActivo = "n_CostoPro"
        Case 3
            CostoActivo = "n_CostoRep"
    End Select
    
    If FrmAppLink.PRDCostoActivo <> Empty Then CostoActivo = FrmAppLink.PRDCostoActivo
    
    If Not FrmAppLink.GetTipoUsuario Then
        lblSucursal.Visible = False
        txt_Sucursal.Visible = False
        CmdLocalidad.Visible = False
        lblDescLocalidad.Visible = False
    End If
    
    txt_Sucursal.Text = FrmAppLink.GetCodLocalidadSistema
    txt_sucursal_LostFocus
    
End Sub

Private Sub Form_Activate()
    Screen.MousePointer = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF3
            If BarraP.Buttons("Agregar").Enabled Then
                Call MenuAccess(True, False)
                Call Agregar
            End If
        Case vbKeyF2
            If BarraP.Buttons("Buscar").Enabled And Not txt_descripcion.Enabled Then
                Call MenuAccess(True, False)
                Tecla_Pulsada = True
                Set Forma = FRM_PRODUCCION_FORMULA_1
                Set Campo_Txt = lbl_consecutivo
                Set Campo_Lbl = txt_descripcion
                LastConsecutivo = lbl_consecutivo.Caption
                Call MAKE_VIEW("ma_produccion", "c_formula", "c_Descripcion", StellarMensaje(2818), FRM_PRODUCCION_FORMULA_1, "GENERICO", , Campo_Txt, Campo_Lbl)
                Tecla_Pulsada = False
                If LastConsecutivo <> lbl_consecutivo.Caption Then
                    Call Buscar_Formula(lbl_consecutivo.Caption)
                End If
                Tecla_Pulsada = False
            End If
        Case vbKeyF5
            If BarraP.Buttons("Modificar").Enabled Then
                Call MenuAccess(True, False)
                Call Modificar
            End If
        Case vbKeyF7
            If BarraP.Buttons("Cancelar").Enabled Then
                Call MenuAccess(True, False)
                Call Cancelar
            End If
        Case vbKeyF8
            Call MAKE_DOC("MA_PRODUCCION", "MA_PRODUCCION.c_Formula", "MA_PRODUCCION.c_Descripcion", "MA_PRODUCCION.Update_Date", StellarMensaje(2818), Me, "PRODUCCION_FORMULA", "PRD", "DCO", False, True, , , "MA_USUARIOS.Descripcion")
        Case vbKeyF6
            If BarraP.Buttons("Eliminar").Enabled Then
                Call eliminar(lbl_consecutivo.Caption)
                Call MenuAccess(True, False)
            End If
        Case vbKeyF4
            If BarraP.Buttons("Grabar").Enabled Then
                Call MenuAccess(True, False)
                Call Grabar_Formula
            End If
        Case vbKeyF12
            If BarraP.Buttons("Salir").Enabled Then
                Call Salir
            End If
        Case vbKeyF1
            'Ayuda
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set FRM_PRODUCCION_FORMULA_1 = Nothing
End Sub

Private Sub Preparado_Click()
    With Preparado
    If .Col = ColPre.Info And .TextMatrix(.Row, ColPre.Producto) <> Empty And .TextMatrix(.Row, ColPre.Canti) <> Empty Then
        
        Dim FrmMostrarXml: Set FrmMostrarXml = FrmAppLink.GetFrmMostrarXML
        
        FrmMostrarXml.Caption = "Detalles Adicionales."
        
        Dim mTexto, mTotalLn, TotalCant, TotalCantMerma, TotalUnitario
        Dim ContItems
        
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty And .TextMatrix(I, ColPre.Canti) <> Empty Then
                ContItems = ContItems + 1
                TotalCant = TotalCant + Round(CDbl(.TextMatrix(I, ColPre.Canti)), 8)
                TotalCantMerma = TotalCantMerma + Round((CDbl(.TextMatrix(I, ColPre.Canti)) * (CDbl(.TextMatrix(I, ColPre.PorcMerma)) / 100#)), IIf(CLng(.TextMatrix(I, ColPre.LDeci)) > 3, CLng(.TextMatrix(I, ColPre.LDeci)), 3))
                TotalUnitario = TotalUnitario + CDbl(.TextMatrix(I, ColPre.CostoPre))
            End If
        Next
        
        mTotalLn = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.CostoPreXMerma)) + CDbl(.TextMatrix(.Row, ColPre.CostoPre)), CLng(.TextMatrix(.Row, ColPre.LDeci)))
        
        If CDbl(MTotal.Text) = 0 Then
            PartCostoGlobal = FormatNumber(0, 2)
        Else
            PartCostoGlobal = FormatNumber(CDbl(mTotalLn) * 100 / CDbl(MTotal.Text), 2)
        End If
        
        If TotalUnitario = 0 Then
            PartCostoUni = FormatNumber(0, 2)
        Else
            PartCostoUni = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.CostoPre)) * 100 / TotalUnitario, 2)
        End If
        
        mTexto = vbNewLine & _
        "** Detalles Linea **" & vbNewLine & vbNewLine & _
        "Fila: " & .Row & vbNewLine & _
        "Codigo: " & .TextMatrix(.Row, ColPre.Producto) & vbNewLine & _
        "Descripcion: " & .TextMatrix(.Row, ColPre.Descripci�n) & vbNewLine & _
        "Cantidad: " & .TextMatrix(.Row, ColPre.Canti) & vbNewLine & _
        FrmAppLink.PRDLabelCampoPresenta & ": " & .TextMatrix(.Row, ColPre.Presentaci�n) & vbNewLine & _
        "% Merma: " & .TextMatrix(.Row, ColPre.PorcMerma) & vbNewLine & _
        "Costo Merma: " & .TextMatrix(.Row, ColPre.CostoPreXMerma) & vbNewLine & _
        "Subtotal Merma: " & CDbl(.TextMatrix(.Row, ColPre.Canti)) * CDbl(.TextMatrix(.Row, ColPre.CostoPreXMerma)) & vbNewLine & _
        "Costo Uni: " & .TextMatrix(.Row, ColPre.CostoPre) & vbNewLine & _
        "Subtotal Costo: " & .TextMatrix(.Row, ColPre.CostoProd) & vbNewLine & _
        "Costo Total: " & mTotalLn & vbNewLine & _
        "Cant. Merma: " & FormatNumber(CDbl(.TextMatrix(.Row, ColPre.Canti)) * (CDbl(.TextMatrix(.Row, ColPre.PorcMerma)) / 100#), IIf(CLng(.TextMatrix(.Row, ColPre.LDeci)) > 3, CLng(.TextMatrix(.Row, ColPre.LDeci)), 3)) & vbNewLine & _
        "Part. Cant.: " & .TextMatrix(.Row, ColPre.Info) & vbNewLine & _
        "Part. Costo Global: " & PartCostoGlobal & "%" & vbNewLine & _
        "Part. Costo Uni.: " & PartCostoUni & "%" & vbNewLine & _
        vbNewLine
        
        mTexto = mTexto & _
        "** Totales **" & vbNewLine & vbNewLine & _
        "Subtotal: " & txt_monto.Text & vbNewLine & _
        "Merma: " & txt_merma.Text & vbNewLine & _
        "Costo Dir: " & txt_costosdirectos.Text & vbNewLine & _
        "Total Monto Produccion: " & MTotal.Text & vbNewLine & _
        "Total Cant: " & TotalCant & vbNewLine & _
        "Cant Merma: " & TotalCantMerma & vbNewLine & _
        "Ingredientes: " & ContItems
        
        FrmMostrarXml.TextArea.Text = mTexto
        FrmMostrarXml.Show vbModal
        
        Set FrmMostrarXml = Nothing
        
    End If
    End With
End Sub

Private Sub Preparado_DblClick()
    Call Preparado_KeyPress(vbKeyReturn)
End Sub

Private Sub Preparado_GotFocus()
    If Preparado.Col = ColPre.Producto And ModifFlag Then
        oTeclado.Key_Down
        Who_Ami = 1
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub Preparado_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
    Else
        Select Case KeyCode
            Case Is = vbKeyF2
                Producto_Imp = ""
                Tecla_Pulsada = True
                If Preparado.ColSel = ColPre.Producto And ModifFlag Then
                    Call MAKE_VIEW("ma_productos", "c_Codigo", "c_Descri", StellarMensaje(254), Me, "PRODUCTOS_GENERAL", , Campo_Txt, Campo_Lbl)
                    If txtedit <> "" Then Preparado_KeyPress (vbKeyReturn)
                    txtedit.MaxLength = 15
                    txtedit.Text = Mid(Producto_Imp, 2)
                    'Preparado.Text = producto_imp
                    'SendKeys "{BKSP}{ENTER}"
                    Call Preparado_KeyPress(vbKeyReturn)
                End If
                Tecla_Pulsada = False
            Case Is = vbKeyDelete
                    Call Preparado_KeyPress(vbKeyDelete)
        End Select
    End If
End Sub

Private Sub Preparado_KeyPress(KeyAscii As Integer)
    If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                With Preparado
                    If .ColSel <> ColPre.Presentaci�n And .ColSel <> ColPre.Descripci�n And .ColSel <> ColPre.LDeci Then
                        Select Case .ColSel
                            Case ColPre.Producto
                                txtedit.MaxLength = 15
                            Case ColPre.Canti
                                If MSGridRecover(Preparado, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Preparado, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Preparado, .Row, ColPre.Canti) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColPre.Canti)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                        End Select
                        
                        '   ACA SE COMENTA PARA DEJAR POR FUERA A LA COLUMNA DE MERMA
                        If (.ColSel = ColPre.Producto And .RowSel = .Rows - 1) Or .ColSel = ColPre.Canti Or IIf(ManejaMermaExplicita, .ColSel = ColPre.PorcMerma, False) Then
                            
                            If .Text <> "" Then
                                Valor_Temp = .Text
                                txtedit.Text = .Text
                            End If
                            txtedit.Left = .CellLeft + .Left
                            txtedit.Top = .CellTop + .Top
                            txtedit.Height = .CellHeight
                            txtedit.Width = .CellWidth
'                            txtedit.SelStart = 0
'                            txtedit.SelLength = Len(txtedit.Text)
                            txtedit.Visible = True
                            txtedit.Enabled = True
                            SafeFocus txtedit
                            Preparado.Enabled = False
                        Else
                            Preparado.Enabled = True
                            Preparado.Col = ColPre.Producto
                            Preparado.Row = Preparado.Rows - 1
                            SafeFocus Preparado
                        End If
                    End If
                
                    'If .ColSel = ColPre.PorcMerma Then Preparado.Row = Preparado.Rows - 1
                
                End With
            Case vbKeyEscape
                Preparado.Enabled = True
                With Preparado
                    'txtedit.Text = Preparado.Text
                    txtedit.Visible = False
                    If .Enabled And .Visible Then .SetFocus
                End With
            Case 48 To 57, 106 To 255, vbKeyDecimal
                With Preparado
                    If .ColSel <> ColPre.Presentaci�n And .ColSel <> ColPro.Descripci�n And .ColSel <> ColPre.LDeci Then
                        Select Case .Col
                            Case ColPre.Producto
                                txtedit.MaxLength = 15
                            Case ColPre.Canti
                                If MSGridRecover(Preparado, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Preparado, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Preparado, .Row, ColPre.Canti) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColPre.Canti)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                        End Select
                        Valor_Temp = .Text
                        txtedit.Left = .CellLeft + .Left
                        txtedit.Top = .CellTop + .Top
                        txtedit.Height = .CellHeight
                        txtedit.Width = .CellWidth
                        txtedit.Visible = True
                        txtedit.Text = Chr(KeyAscii)
                        txtedit.SelStart = Len(txtedit.Text)
                        txtedit.SelLength = 0
                        SafeFocus txtedit
                        Preparado.Enabled = False
                    End If
                End With
            Case vbKeyDelete
                If Preparado.Rows > 3 Then
                    If Preparado.Row < Preparado.Rows - 1 Then
                        txt_monto.Text = FormatNumber(CDbl(IIf(Trim(txt_monto.Text) = "", 0, txt_monto.Text)) - (CDbl(IIf(Trim(MSGridRecover(Preparado, Preparado.Row, ColPre.CostoPre)) = "", 0, MSGridRecover(Preparado, Preparado.Row, ColPre.CostoPre))) * CDbl(IIf(Trim(MSGridRecover(Preparado, Preparado.Row, ColPre.Canti)) = "", 0, MSGridRecover(Preparado, Preparado.Row, ColPre.Canti)))), Std_Decm)
                        txt_merma.Text = FormatNumber(CDbl(IIf(Trim(txt_merma.Text) = "", 0, txt_merma.Text)) - (CDbl(IIf(Trim(MSGridRecover(Preparado, Preparado.Row, ColPre.CostoPreXMerma)) = "", 0, MSGridRecover(Preparado, Preparado.Row, ColPre.CostoPreXMerma))) * CDbl(IIf(Trim(MSGridRecover(Preparado, Preparado.Row, ColPre.Canti)) = "", 0, MSGridRecover(Preparado, Preparado.Row, ColPre.Canti)))), Std_Decm)

                        MTotal.Text = FormatNumber(CDbl(txt_monto.Text) + CDbl(txt_merma.Text), Std_Decm)
                        Call DistTotal(CDbl(MTotal), Me.Producir)
                        
                        Call Cerrar_Recordset(rsProducto)
                        Call Cerrar_Recordset(rsPmoneda)
                        'Call DelFilaGrid(Preparado, Preparado.Row)

                        Preparado.RemoveItem (Preparado.Row)
'                        If Preparado.Row < Preparado.Rows - 1 Then
'                            Preparado.Rows = Preparado.Rows - 1
'                        End If
                        
                    End If
                Else
                    'Call Mensaje(True, "No puede eliminar m�s items porque una f�rmula debe tener por lo menos un ingrediente.")
                    Mensaje True, StellarMensaje(2804)
                End If
    
        End Select
    End If
End Sub

Private Sub Preparado_LostFocus()
    If Producir.Enabled Then
        Producir.Row = Producir.Rows - 1
        Producir.Col = ColPro.Producto
    End If
End Sub

Private Sub Preparado_SelChange()
    If Preparado.Col = ColPre.Producto And ModifFlag Then
        Who_Ami = 1
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub Producir_Click()
    With Producir
    If .Col = ColPro.Info And .TextMatrix(.Row, ColPro.Producto) <> Empty And .TextMatrix(.Row, ColPro.Canti) <> Empty Then
        
        Dim FrmMostrarXml: Set FrmMostrarXml = FrmAppLink.GetFrmMostrarXML
        
        FrmMostrarXml.Caption = "Detalles Adicionales."
        
        Dim mTexto, mTotalLn, TotalCant, TotalCantMerma, TotalUnitario
        Dim ContItems
        
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColPro.Producto) <> Empty And .TextMatrix(I, ColPro.Canti) <> Empty Then
                ContItems = ContItems + 1
                TotalCant = TotalCant + Round(CDbl(.TextMatrix(I, ColPro.Canti)), 8)
                TotalFactor = TotalFactor + CDbl(.TextMatrix(I, ColPro.Factor))
                TotalUnitario = TotalUnitario + CDbl(.TextMatrix(I, ColPro.CostoPre))
            End If
        Next
        
        If CDbl(MTotal.Text) = 0 Then
            PartCostoGlobal = FormatNumber(0, 2)
        Else
            PartCostoGlobal = FormatNumber(CDbl(.TextMatrix(.Row, ColPro.CostoProd)) * 100 / CDbl(MTotal.Text), 2)
        End If
        
        If TotalUnitario = 0 Then
            PartCostoUni = FormatNumber(0, 2)
        Else
            PartCostoUni = FormatNumber(CDbl(.TextMatrix(.Row, ColPro.CostoPre)) * 100 / TotalUnitario, 2)
        End If
        
        mTexto = vbNewLine & _
        "** Detalles Linea **" & vbNewLine & vbNewLine & _
        "Fila: " & .Row & vbNewLine & _
        "Codigo: " & .TextMatrix(.Row, ColPro.Producto) & vbNewLine & _
        "Descripcion: " & .TextMatrix(.Row, ColPro.Descripci�n) & vbNewLine & _
        "Cantidad: " & .TextMatrix(.Row, ColPro.Canti) & vbNewLine & _
        FrmAppLink.PRDLabelCampoPresenta & ": " & .TextMatrix(.Row, ColPro.Presentaci�n) & vbNewLine & _
        "Costo Uni: " & .TextMatrix(.Row, ColPro.CostoPre) & vbNewLine & _
        "Costo Total: " & .TextMatrix(.Row, ColPro.CostoProd) & vbNewLine & _
        "Factor Costeo: " & .TextMatrix(.Row, ColPro.Factor) & vbNewLine & _
        "Part. Cant.: " & .TextMatrix(.Row, ColPro.PorcPartCant) & vbNewLine & _
        "Part. Costo Global: " & PartCostoGlobal & "%" & vbNewLine & _
        "Part. Costo Uni.: " & PartCostoUni & "%" & vbNewLine & _
        vbNewLine & _
        "** Totales **" & vbNewLine & vbNewLine & _
        "Subtotal: " & txt_monto.Text & vbNewLine & _
        "Merma: " & txt_merma.Text & vbNewLine & _
        "Costo Dir: " & txt_costosdirectos.Text & vbNewLine & _
        "Total Monto Produccion: " & MTotal.Text & vbNewLine & _
        "Total Cant: " & TotalCant & vbNewLine & _
        "Total Factor: " & TotalFactor & vbNewLine & _
        "Producto(s): " & ContItems
        
        FrmMostrarXml.TextArea.Text = mTexto
        FrmMostrarXml.Show vbModal
        
        Set FrmMostrarXml = Nothing
        
    End If
    End With
End Sub

Private Sub Producir_DblClick()
    Call Producir_KeyPress(vbKeyReturn)
End Sub

Private Sub Producir_GotFocus()
    'Producir.Col = ColPro.Producto
    'MsgBox Tecla_Pulsada
    If Producir.Col = ColPro.Producto And ModifFlag Then
        oTeclado.Key_Down
        Who_Ami = 2
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        'Producir.Col = ColPro.Producto
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub Producir_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
    Else
        Select Case KeyCode
            Case Is = vbKeyF2
                Tecla_Pulsada = True
                Producto_Imp = ""
                If Producir.ColSel = ColPro.Producto And ModifFlag Then
                    Call MAKE_VIEW("ma_productos", "c_Codigo", "c_Descri", StellarMensaje(254), Me, "PRODUCTOS_GENERAL", , Campo_Txt, Campo_Lbl)
                    If txtedit2.Text <> "" Then Preparado_KeyPress (13)
                    txtedit2.MaxLength = 15
                    txtedit2.Text = Mid(Producto_Imp, 2)
                    'SendKeys "{BKSP}{ENTER}"
                    Call Producir_KeyPress(vbKeyReturn)
                End If
                Tecla_Pulsada = False
            Case Is = vbKeyDelete
                Call Producir_KeyPress(vbKeyDelete)
        End Select
    End If
End Sub

Private Sub Producir_KeyPress(KeyAscii As Integer)
    If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                With Producir
                    If .ColSel <> ColPro.Presentaci�n And .ColSel <> ColPro.Descripci�n And .ColSel <> ColPro.LDeci And .ColSel <> ColPro.CostoProd Then
                        Select Case .ColSel
                            Case ColPro.Producto
                                txtedit2.MaxLength = 15
                            Case ColPro.Canti
                                If MSGridRecover(Producir, .Row, ColPro.Producto) = "" Then
                                    Call SetDefMSGrid(Producir, .Row, ColPro.Producto)
                                    Exit Sub
                                End If
                                txtedit2.MaxLength = 0
                            Case ColPro.CostoPre
                                If MSGridRecover(Producir, .Row, ColPro.Producto) = "" Then
                                    Call SetDefMSGrid(Producir, .Row, ColPro.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Producir, .Row, ColPro.Canti) = "" Then
                                    Call SetDefMSGrid(Producir, .Row, ColPro.Canti)
                                    Exit Sub
                                End If
                                txtedit2.MaxLength = 20
                            Case ColPro.CostoProd
                                If MSGridRecover(Producir, .Row, ColPro.Producto) = "" Then
                                    Call SetDefMSGrid(Producir, .Row, ColPro.Producto)
                                    Exit Sub
                                End If
                                txtedit2.MaxLength = 0
                        End Select
                        
                        If (.ColSel = ColPro.Producto And .RowSel = .Rows - 1) Or (.ColSel = ColPro.Canti Or .ColSel = ColPro.CostoProd Or .ColSel = ColPro.Factor) Then
                            
                            If .Text <> "" Then
                                Valor_Temp = .Text
                                txtedit2.Text = FormatNumber(CDbl(.Text), Std_Decm)
                            End If
                            txtedit2.Left = .CellLeft + .Left
                            txtedit2.Top = .CellTop + .Top
                            txtedit2.Height = .CellHeight
                            txtedit2.Width = .CellWidth
                            'txtedit2.SelStart = 0
                            'txtedit2.SelLength = Len(txtedit2.Text)
                            txtedit2.Visible = True
                            txtedit2.Enabled = True
                            SafeFocus txtedit2
                            Producir.Enabled = False
                        Else
                            Producir.Enabled = True
                            Producir.Col = ColPro.Producto
                            Producir.Row = Producir.Rows - 1
                            SafeFocus Producir
                        End If
                    End If
                End With
            Case vbKeyEscape
                Producir.Enabled = True
                With Producir
                    txtedit2.Visible = False
                    If .Enabled And .Visible Then .SetFocus
                End With
            Case 48 To 57, 106 To 255, vbKeyDecimal
                With Producir
                    If .ColSel <> ColPro.Presentaci�n And .ColSel <> ColPro.Descripci�n And .ColSel <> ColPro.LDeci And .ColSel <> ColPro.CostoPre Then
                        Select Case .Col
                            Case ColPro.Producto
                                txtedit2.MaxLength = 15
                            Case ColPro.Canti
                                If MSGridRecover(Producir, .Row, ColPro.Producto) = "" Then
                                    Call SetDefMSGrid(Producir, .Row, ColPro.Producto)
                                    Exit Sub
                                End If
                                txtedit2.MaxLength = 0
                            Case ColPro.CostoPre
                                If MSGridRecover(Producir, .Row, ColPro.Producto) = "" Then
                                    Call SetDefMSGrid(Producir, .Row, ColPro.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Producir, .Row, ColPro.Canti) = "" Then
                                    Call SetDefMSGrid(Producir, .Row, ColPro.Canti)
                                    Exit Sub
                                End If
                                txtedit2.MaxLength = 20
                        End Select
                        Valor_Temp = .Text
                        txtedit2.Left = .CellLeft + .Left
                        txtedit2.Top = .CellTop + .Top
                        txtedit2.Height = .CellHeight
                        txtedit2.Width = .CellWidth
                        txtedit2.Visible = True
                        txtedit2.Text = Chr(KeyAscii)
                        txtedit2.SelStart = Len(txtedit2.Text)
                        txtedit2.SelLength = 0
                        SafeFocus txtedit2
                        Producir.Enabled = False
                    End If
                End With
            Case vbKeyDelete
                If Producir.Rows > 3 And Producir.ColSel = ColPro.Producto Then
                    If Producir.Row < Producir.Rows - 1 Then
                        'Call DelFilaGrid(Producir, Producir.Row)
                        
                        Producir.RemoveItem (Producir.Row)
'                        If Producir.Row < Producir.Rows - 1 Then
'                            Producir.Rows = Producir.Rows - 1
'                        End If
                        
                        'Call DistTotal(CDbl(txt_Monto.Text) + CDbl(txt_costosdirectos.Text), Producir)
                        Call DistTotal(CDbl(MTotal), Producir)
                    End If
                Else
                    'Call Mensaje(True, "No puede eliminar m�s items porque una f�rmula debe tener por lo menos un ingrediente.")
                    Mensaje True, StellarMensaje(2805)
                End If
    
        End Select
    End If
End Sub

Private Sub Producir_LostFocus()
    'resp = ""
End Sub

Private Sub Producir_SelChange()
    If Producir.Col = ColPro.Producto And ModifFlag Then
        Who_Ami = 2
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub txt_costosdirectos_Change()
    If Not CheckCad(txt_costosdirectos, Std_Decm, 20, False) Then
        'txt_costosdirectos.Text = FormatNumber(0, std_decm)
        txt_costosdirectos.SelStart = 0
        txt_costosdirectos.SelLength = Len(txt_costosdirectos.Text)
        If txt_costosdirectos.Enabled Then
            SafeFocus txt_costosdirectos
        Else
            txt_costosdirectos.Text = ""
        End If
    End If
End Sub

Private Sub txt_costosdirectos_GotFocus()
    txt_costosdirectos.SelStart = 0
    txt_costosdirectos.SelLength = Len(txt_costosdirectos.Text)
End Sub

Private Sub txt_costosdirectos_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub txt_costosdirectos_LostFocus()
    If txt_costosdirectos.Enabled Then
        If Trim(txt_costosdirectos.Text) <> "" Then
            txt_costosdirectos.Text = FormatNumber(CDbl(txt_costosdirectos.Text), Std_Decm)
            Call DistTotal(CDbl(txt_monto.Text) + CDbl(txt_costosdirectos.Text) + _
            CDbl(Me.txt_merma), Producir)
            If Preparado.Enabled Then
                Preparado.Row = Preparado.Rows - 1
                Preparado.Col = ColPre.Producto
                Producir.Row = Producir.Rows - 1
                Producir.Col = ColPro.Producto
            End If
        Else
            txt_costosdirectos.Text = FormatNumber(0, Std_Decm)
        End If
    Else
        txt_costosdirectos.Text = ""
    End If
End Sub

Private Sub txt_descripcion_GotFocus()
    txt_descripcion.SelStart = 0
    txt_descripcion.SelLength = Len(txt_descripcion.Text)
End Sub

Private Sub txt_descripcion_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub txt_descripcion_LostFocus()
    'Preparado.Row = Preparado.Rows - 1
    'Preparado.Col = ColPre.Producto
    'Producir.Row = Producir.Rows - 1
    'Producir.Col = ColPro.Producto
End Sub

Private Sub txt_sucursal_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Tecla_Pulsada = True
            Set Forma = Me
            Set Campo_Txt = txt_Sucursal
            Set Campo_Lbl = lblDescLocalidad
            Call MAKE_VIEW("ma_sucursales", "c_Codigo", "c_Descripcion", UCase(Stellar_Mensaje(1254)) _
            , Me, "GENERICO", , Campo_Txt, Campo_Lbl) '"S U C U R S A L E S"
    End Select
End Sub

Private Sub txt_sucursal_LostFocus()
    
    Dim RsSucursal As New ADODB.Recordset
    
    If Trim(txt_Sucursal.Text) <> Empty Then
        
        Call Apertura_Recordset(RsSucursal)
        
        RsSucursal.Open "Select * from ma_sucursales " & _
        "where c_Codigo = '" & Trim(txt_Sucursal.Text) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsSucursal.EOF Then
            lblDescLocalidad.Caption = RsSucursal!c_Descripcion
        Else
            Call Mensaje(True, StellarMensaje(16357))
            Me.lblDescLocalidad.Caption = ""
            txt_Sucursal.Text = ""
        End If
        
        RsSucursal.Close
        
    Else
        lblDescLocalidad.Caption = ""
        txt_Sucursal.Text = ""
    End If
    
End Sub

Private Sub txtedit2_Change()
    Dim Lcol As Integer, Lrow As Integer
    Producir.Enabled = True
    If Producir.Col = ColPro.Canti Then
        Lrow = Producir.Row
        Lcol = Producir.Col
        If txtedit2.Visible Then
            'If CheckCad(txtedit2, CDbl(MSGridRecover(Producir, Producir.Row, 0)), , True) Then
            'End If
        End If
        Call SetDefMSGrid(Producir, Lrow, Lcol)
    End If
    'Producir.Enabled = False
End Sub

Private Sub txtedit2_GotFocus()
    If Producir.Col = ColPro.Producto Then
        Who_Ami = 2
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    End If
End Sub

Private Sub txtedit2_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            'oTeclado.Key_Tab
            Call txtedit2_LostFocus
        Case vbKeyEscape
            Call Producir_KeyPress(vbKeyEscape)
    End Select
End Sub

Private Sub txtedit2_LostFocus()
    Dim Mensajes As String, RsEureka As New ADODB.Recordset, ColAct As Integer, FilAct As Integer, Merma As Double, Monto As Double, Diferencia As Double
    
    With Producir
        Select Case .Col
            Case ColPro.Canti
                
                .Enabled = True
                If Trim(txtedit2.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If InStr(1, txtedit2, "%") <> 0 Then
                        If Preparado.Rows > 3 Then
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "No puede escribir cantidades porcentuales con preparados de f�rmulas con m�s de un producto.")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            Exit Sub
                        Else
                            txtedit2.Text = FormatNumber(CDbl(MSGridRecover(Preparado, 1, ColPre.Canti)) _
                            * (CDbl(Mid(txtedit2.Text, 1, InStr(1, txtedit2.Text, "%") - 1)) / 100), _
                            CDbl(MSGridRecover(Producir, .Row, ColPro.LDeci)))
                        End If
                    End If
                    If CheckCad(txtedit2, CDbl(MSGridRecover(Producir, .Row, ColPro.LDeci)), , True) Then
                        If CDbl(txtedit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la Celda actual
                            'se usar�n dos (2) digitos decimales para los porcentajes...!
                            
                            .Text = FormatNumber(txtedit2.Text, CDbl(MSGridRecover(Producir, .Row, ColPro.LDeci)))
                            .CellAlignment = flexAlignRightCenter
                            Call Apertura_Recordset(RsRecetas)
                            
                            Call SetDefMSGrid(Producir, .Row, ColPro.CostoProd)
                            'SendKeys Chr$(13)
                            Diferencia = 0
                            
                            Call DistTotal(CDbl(MTotal), Producir)
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                oTeclado.Key_Down
                            End If
                            Call SetDefMSGrid(Producir, .Row + 1, ColPro.Producto)
                            txtedit2.Text = ""
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit2.Enabled = True
                            txtedit2.Text = ""
                            txtedit2.Visible = True
                            SafeFocus txtedit2
                            .Enabled = False
                            Exit Sub
                        End If
                    Else
                        If txtedit2.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit2.Enabled = True
                            txtedit2.Text = ""
                            txtedit2.Visible = True
                            SafeFocus txtedit2
                            Exit Sub
                        Else
                            .Col = ColPro.Canti
                            .Text = 1
                            'SendKeys Chr$(13)
                            oTeclado.Key_Return
                        End If
                    End If
                End If
            Case ColPro.CostoPre 'Costo Produccion
                If Trim(txtedit2.Text) <> "" And Tecla_Pulsada = False Then
                    If CheckCad(txtedit2, 3, , False) Then
                        If CDbl(txtedit2.Text) >= 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la celda actual
                            .Enabled = True
                            .Text = FormatNumber(CDbl(txtedit2.Text), Std_DecC)
                            .CellAlignment = flexAlignRightCenter
                            .Col = ColPro.CostoProd
                            
                            .Text = FormatNumber(CDbl(MSGridRecover(Producir, .Row, ColPro.CostoProd)), Std_Decm)
                            If FormatNumber(SumaGrid(Producir, ColPro.CostoProd), Std_Decm) <= FormatNumber(CDbl(txt_monto.Text) + CDbl(txt_costosdirectos.Text) + CDbl(txt_merma), Std_Decm) Then
                                If .Row = .Rows - 1 Then
                                    .Rows = .Rows + 1
                                End If
                                Call SetDefMSGrid(Producir, .Row + 1, ColPro.Producto)
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "El monto debe ser menor o igual que el total")
                                Mensaje True, StellarMensaje(2817)
                                Tecla_Pulsada = False
                                .Col = ColPro.CostoProd
                                .Text = 0
                            End If
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor es negativo")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit2.Enabled = True
                            txtedit2.Text = ""
                            txtedit2.Visible = True
                            SafeFocus txtedit2
                            .Enabled = False
                            Exit Sub
                        End If
                        txtedit2.Text = ""
                    Else
                        If txtedit2.Text <> "" Then
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit2.Enabled = True
                            txtedit2.Text = ""
                            txtedit2.Visible = True
                            SafeFocus txtedit2
                            .Enabled = False
                            Exit Sub
                        End If
                    End If
                End If
            
            Case ColPro.Factor 'fACTOR cOSTO
                .Enabled = True
                If Trim(txtedit2.Text) <> "" And Tecla_Pulsada = False Then
                    If InStr(1, txtedit2, "%") <> 0 Then
                        If Preparado.Rows > 3 Then
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "No puede escribir cantidades porcentuales .")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            Exit Sub
                        Else
                            txtedit2.Text = FormatNumber(1, 2)
                        End If
                    End If
                    If CheckCad(txtedit2, 2, , False) Then
                        If CDbl(txtedit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la Celda actual
                            'se usar�n dos (2) digitos decimales para los porcentajes...!
                            '
                            .Text = FormatNumber(txtedit2.Text, 2)
                            .CellAlignment = flexAlignRightCenter
                            Call Apertura_Recordset(RsRecetas)

                            Call SetDefMSGrid(Producir, .Row, ColPro.Factor)
                            Diferencia = 0
                            Call DistTotal(CDbl(MTotal), Producir)
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                oTeclado.Key_Down
                            End If
                            Call SetDefMSGrid(Producir, .Row + 1, ColPro.Producto)
                            txtedit2.Text = ""
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit2.Enabled = True
                            txtedit2.Text = ""
                            txtedit2.Visible = True
                            SafeFocus txtedit2
                            .Enabled = False
                            Exit Sub
                        End If
                    Else
                        If txtedit2.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit2.Enabled = True
                            txtedit2.Text = ""
                            txtedit2.Visible = True
                            SafeFocus txtedit2
                            Exit Sub
                        Else
                            .Col = ColPro.Factor
                            .Text = FormatNumber(1, 2)
                            oTeclado.Key_Return
                        End If
                    End If
                End If
            
            Case ColPro.Producto
                'Producto
                If Tecla_Pulsada = True Then Exit Sub
                If Trim(txtedit2.Text) <> "" Then
                    txtedit2.Text = AlternateCode(Trim(txtedit2.Text))
                    If Not Validar_Repeticiones(Producir, txtedit2) And Not InSet(txtedit2.Text, Preparado) Then
                        Set rsProducto = ScanData("SELECT * FROM MA_PRODUCTOS LEFT JOIN MA_MONEDAS ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda WHERE c_Codigo = '" & Trim(txtedit2.Text) & "' ")
                        If Not rsProducto.EOF Then
                            If rsProducto!n_Activo = 1 Then
                                .Text = txtedit2.Text
                                .Col = ColPro.LDeci
                                .Text = rsProducto!Cant_Decimales
                                .Col = ColPro.Descripci�n
                                .Text = rsProducto!c_Descri
                                .Col = ColPro.Presentaci�n
                                .Text = IIf(IsNull(rsProducto!c_Presenta), "", rsProducto!c_Presenta)
                                .Col = ColPro.Factor 'factor de costo
                                .Text = 1
                                
                                '.Col = ColPro.CostoProd
                                '.Text = FormatNumber(FaltaTotal(CDbl(txt_monto.Text) + CDbl(txt_costosdirectos.Text), Producir), std_decm)
                                .Col = ColPro.Canti
                                'SendKeys Chr$(13)
                                oTeclado.Key_Return
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "El Producto no est� activo.")
                                Mensaje True, StellarMensaje(16101)
                                Tecla_Pulsada = False
                            End If
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = ColPro.Producto
                        End If
                        rsProducto.Close
                        txtedit2.Text = ""
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista o el monto de la producci�n es 0.")
                        Mensaje True, StellarMensaje(2816)
                        Tecla_Pulsada = False
                        .Col = ColPro.Producto
                        .Text = ""
                    End If
                Else
                    .Col = ColPro.Producto
                End If
                
        End Select
    End With
    txtedit2.Visible = False
    txtedit2.Text = ""
    If Tecla_Pulsada = False Then
        Producir.Enabled = True
        SafeFocus Producir
    End If
    If Producir.ColSel = ColPro.Producto Then
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub txtedit_Change()
    Dim Lcol As Integer, Lrow As Integer
    Preparado.Enabled = True
    If Preparado.Col = ColPre.Canti Then
        Lrow = Preparado.Row
        Lcol = Preparado.Col
        If txtedit.Visible Then
            'If CheckCad(txtedit, CDbl(MSGridRecover(Preparado, Preparado.Row, 0)), , True) Then
            'End If
        End If
        Call SetDefMSGrid(Preparado, Lrow, Lcol)
    End If
    'Preparado.Enabled = False
End Sub

Private Sub TxtEdit_GotFocus()
    'MsgBox producto_imp
    If Preparado.Col = ColPre.Producto And MSGridRecover(Producir, Producir.Row, ColPro.Producto) = "" Then
        Who_Ami = 1
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    ElseIf Preparado.Col = ColPre.Producto And MSGridRecover(Producir, Producir.Row, ColPro.Producto) <> "" Then
        Who_Ami = 1
        txtedit2.Enabled = False
        txtedit2.Text = ""
    End If
End Sub

Private Sub txtEdit_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            'oTeclado.Key_Tab
            Call txtEdit_LostFocus
        Case vbKeyEscape
            Call Preparado_KeyPress(vbKeyEscape)
        Case Else
            If Preparado.Col = ColPre.PorcMerma Then ' MERMA
                Select Case KeyAscii
                    Case 48 To 57
                    Case 8
                    Case 46
                    Case Else
                        KeyAscii = 0
                End Select
            End If
    End Select
End Sub

Private Sub txtEdit_LostFocus()
    
    Dim Mensajes As String, RsEureka As New ADODB.Recordset, ColAct As Integer, FilAct As Integer, Merma As Double, Monto As Double
    
    On Error GoTo Errores
    
    With Preparado
        Select Case .Col
            Case ColPre.Canti 'Cantidad
                .Enabled = True
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    If CheckCad(txtedit, CDbl(MSGridRecover(Preparado, .Row, ColPre.LDeci)), , False) Then
                        If CDbl(txtedit.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la Celda actual
                            .Text = FormatNumber(txtedit.Text, CDbl(MSGridRecover(Preparado, .Row, ColPre.LDeci)))
                            .TextMatrix(.Row, ColPre.CostoProd) = FormatNumber(CDbl(.Text) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            .CellAlignment = flexAlignRightCenter
                            Call Apertura_Recordset(RsRecetas)
                            texto_cod = MSGridRecover(Preparado, .RowSel, ColPre.Producto)
                            Criterio = "select *, ma_productos." & CostoActivo & " as costo from ma_productos where c_Codigo = '" & texto_cod & "'"
                            RsRecetas.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            Call Apertura_Recordset(rsPmoneda)
                            rsPmoneda.Open "select * from ma_monedas where c_CodMoneda = '" & RsRecetas!c_CodMoneda & "'", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                            Monto = 0
                            For Cont = 1 To Preparado.Rows - 1
                                Monto = Monto + (CDbl(IIf(MSGridRecover(Preparado, Cont, ColPre.CostoPre) = "", _
                                0, MSGridRecover(Preparado, Cont, ColPre.CostoPre))) * _
                                CDbl(IIf(MSGridRecover(Preparado, Cont, ColPre.Canti) = "", 0, _
                                MSGridRecover(Preparado, Cont, ColPre.Canti))))
                            Next Cont
                            txt_monto.Text = FormatNumber(Monto, rsPmoneda!n_Decimales)
                            If CDbl(MSGridRecover(Preparado, .Row, ColPre.PorcMerma)) = 0 Then
                                .Col = ColPre.PorcMerma
                                .Text = FormatNumber(0, Std_Decm)
                                .Col = ColPre.CostoPreXMerma
                                .Text = FormatNumber(0, Std_Decm)
                                If Not ManejaMermaExplicita Then
                                    '   ACA SE AGREGA LA LINEA ADICIONAL QUE HACE FALTA PARA REEMPLAZAR A LA DE MERMA
                                    .Rows = .Rows + 1
                                End If
                                oTeclado.Key_Return
                            End If
                            
                            MTotal = FormatNumber(CDbl(txt_monto.Text) + CDbl(txt_costosdirectos.Text) + CDbl(txt_merma.Text), Std_Decm)
                            Call DistTotal(CDbl(MTotal.Text), Producir)
                            Call Cerrar_Recordset(rsPmoneda)
                            Call Cerrar_Recordset(RsRecetas)
                            
                            If CDbl(MSGridRecover(Preparado, .Row, ColPre.PorcMerma)) > 0 Then
                                .Col = ColPre.PorcMerma
                                Me.txtedit = .Text
                                txtEdit_LostFocus
                            Else
                                Call SetDefMSGrid(Preparado, .Row, ColPre.PorcMerma)
                            End If
                            
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                    Else
                        If txtedit.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            Exit Sub
                        Else
                            .Col = ColPre.Canti
                            .Text = 1
                            'SendKeys Chr$(13)
                            oTeclado.Key_Return
                        End If
                    End If
                End If
            Case ColPre.PorcMerma 'Merma
                
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    If CheckCad(txtedit, 3, , False) Then
                        If CDbl(txtedit.Text) >= 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la celda actual
                            .Enabled = True
                            .Text = FormatNumber(CDbl(txtedit.Text), 2)
                            .CellAlignment = flexAlignRightCenter
                            'BarraP.Buttons("Grabar").ButtonMenus("GESP").Enabled = True
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                oTeclado.Key_Down
                            End If
                            
                            Call MSGridAsign(Preparado, .Row, ColPre.CostoPreXMerma, FormatNumber((CDbl(txtedit.Text) * CDbl(MSGridRecover(Preparado, .Row, ColPre.CostoPre))) / 100, Std_Decm), , flexAlignRightCenter)
                            
                            Merma = 0
                            For Cont = 1 To Preparado.Rows - 2
                                Merma = Merma + ((CDbl(MSGridRecover(Preparado, Cont, ColPre.CostoPreXMerma)) * CDbl(MSGridRecover(Preparado, Cont, ColPre.Canti)))) ' CDbl(MSGridRecover(Preparado, Cont, ColPre.Canti))
                            Next Cont
                            txt_merma.Text = FormatNumber(Merma, Std_Decm)
                            Call SetDefMSGrid(Preparado, .Row + 1, ColPre.Producto)
                            
                            MTotal = FormatNumber(CDbl(txt_monto.Text) + CDbl(txt_costosdirectos.Text) + CDbl(txt_merma.Text), Std_Decm)
                            Call DistTotal(CDbl(MTotal), Producir)
                        
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor es negativo")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                    Else
                        If txtedit.Text <> "" Then
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                    End If
                End If
            Case ColPre.Producto
                'Producto
                If Tecla_Pulsada = True Then Exit Sub
                If Trim(txtedit.Text) <> "" Then
                    txtedit.Text = AlternateCode(Trim(txtedit.Text))
                    If Not Validar_Repeticiones(Preparado, txtedit) And Not InSet(txtedit.Text, Producir) Then
                        Set rsProducto = ScanData("SELECT * FROM MA_PRODUCTOS INNER JOIN MA_MONEDAS ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda WHERE c_Codigo = '" & Trim(txtedit.Text) & "' ")
                        If Not rsProducto.EOF Then
                            If rsProducto!n_Activo = 1 Then
                                .Text = txtedit.Text
                                .Col = ColPre.LDeci
                                .Text = rsProducto!Cant_Decimales
                                .Col = ColPre.Descripci�n
                                .Text = rsProducto!c_Descri
                                .Col = ColPre.Presentaci�n
                                .Text = IIf(IsNull(rsProducto!c_Presenta), "", rsProducto!c_Presenta)
                                .Col = ColPre.PorcMerma

                                '   ACA SE APLICA PARA COLOCAR POR DEFECTO VALOR EN 0 YA QUE OCASIONABA ERRORES Y DESCUADRES. ADEMAS LOS USUARIOS NO SON GENTE ASI QUE NO LES DA LA CABEZA PARA PENSAR
                                '.Text = FormatNumber(rsProducto!nu_pocentajemerma, Std_DecC)
                                .Text = FormatNumber(0, Std_DecC)
                                
                                .Col = ColPre.CostoPre
                                
                                    ' Autor: Angelica Rondon
                                    ' Descripcion: Se reemplaza el CostoActual por la variable CostoActivo,
                                    ' la cual busca el costo segun regla de negocio.
                                '.Text = FormatNumber((rsProducto!n_CostoAct * rsProducto!n_Factor) / CDbl(msk_factor.Text), Std_Decm)
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * (rsProducto!n_Factor / CDbl(MSK_FACTOR.Text))), Std_Decm)
                                .TextMatrix(.Row, ColPre.CostoProd) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                                .Col = ColPre.Canti
                                
                                'SendKeys Chr$(13)
                                oTeclado.Key_Return
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Producto no esta activo.")
                                Mensaje True, StellarMensaje(16101)
                                Tecla_Pulsada = False
                            End If
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = ColPre.Producto
                        End If
                        rsProducto.Close
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista.")
                        Mensaje True, StellarMensaje(2817)
                        Tecla_Pulsada = False
                        .Col = ColPre.Producto
                        .Text = ""
                    End If
                Else
                    .Col = ColPre.Producto
                End If
        End Select
    End With
    
    txtedit.Visible = False
    txtedit.Text = ""
    
    If Tecla_Pulsada = False Then
        Preparado.Enabled = True
        'Preparado.Visible = True
        SafeFocus Preparado
    End If
    
    If Preparado.ColSel = ColPre.Producto Then
        BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Sub Salir()
    Set FRM_PRODUCCION_FORMULA_1 = Nothing
    Unload Me
End Sub

Sub Cancelar()
    
    Call Apertura_RecordsetC(rsProducto)
    Call Apertura_RecordsetC(RsRecetas)
    Call Apertura_RecordsetC(rsPmoneda)
    Call Apertura_RecordsetC(RsInventario)
    Call Apertura_RecordsetC(RsCodigos)
    Call Apertura_RecordsetC(RsEureka)
    
    FlgUpd = False
    FlgAdd = False
    
    BarraP.Buttons("Agregar").Enabled = True
    BarraP.Buttons("Buscar").Enabled = True
    BarraP.Buttons("Salir").Enabled = True
    BarraP.Buttons("Opciones").Enabled = True
    BarraP.Buttons("Eliminar").Enabled = False
    
    BarraP.Buttons("Modificar").Enabled = False
    BarraP.Buttons("Cancelar").Enabled = False
    BarraP.Buttons("Grabar").Enabled = False
    
    BarraP.Buttons("Buscar").ButtonMenus("BFOR").Enabled = True
    BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    
    txt_descripcion.Enabled = False
    txt_descripcion.Text = ""
    txt_costosdirectos.Enabled = False
    txt_costosdirectos.Text = ""
    txt_Sucursal.Enabled = False
    CmdLocalidad.Enabled = False
    
    Preparado.Rows = 1
    Preparado.Rows = 2
    Producir.Rows = 1
    Producir.Rows = 2
    
    Preparado.Enabled = True
    Preparado.Col = ColPre.Producto
    Preparado.Row = 1
    
    Producir.Enabled = True
    Producir.Col = ColPro.Producto
    Producir.Row = 1
    
    lbl_fecha.Caption = Format(Now, "short date")
    
    ModifFlag = False
    
    GridLleno = False
    txt_costosdirectos.Text = FormatNumber(0, Std_Decm)
    txt_monto.Text = FormatNumber(0, Std_Decm)
    txt_merma.Text = FormatNumber(0, Std_Decm)
    txtedit.Visible = False
    txtedit.Text = ""
    txtedit2.Visible = False
    txtedit2.Text = ""
    txt_Sucursal.Text = Sucursal
    
    Call Nuevo_Consecutivo
    
End Sub

Sub Nuevo_Consecutivo()
    lbl_consecutivo.Caption = Format(No_Consecutivo("produccion_formula", False), "0000000000")
End Sub

Sub Agregar()
    
    'Desactivar Botones
    
    GridLleno = False
    ModifFlag = True
    FlgAdd = True
    FlgUpd = False
    BarraP.Buttons("Agregar").Enabled = False
    BarraP.Buttons("Modificar").Enabled = False
    BarraP.Buttons("Eliminar").Enabled = False
    BarraP.Buttons("Salir").Enabled = False
    'Activar Botones
    BarraP.Buttons("Buscar").Enabled = True
    BarraP.Buttons("Buscar").ButtonMenus("BFOR").Enabled = False
    BarraP.Buttons("Grabar").Enabled = True
    BarraP.Buttons("Cancelar").Enabled = True
    BarraP.Buttons("Opciones").Enabled = True
    'Activar Text y Grids
    txt_descripcion.Enabled = True
    SafeFocus txt_descripcion
    txt_costosdirectos.Enabled = True
    txt_Sucursal.Enabled = True
    CmdLocalidad.Enabled = True
    
    txt_descripcion.Text = ""
    txt_costosdirectos = FormatNumber(0, Std_Decm)
    txt_merma.Text = FormatNumber(0, Std_Decm)
    txt_monto.Text = FormatNumber(0, Std_Decm)
    
    Preparado.Rows = 1
    Preparado.Rows = 2
    Producir.Rows = 1
    Producir.Rows = 2
    
    Call Ini_Moneda
    
    Preparado.Enabled = True
    Producir.Enabled = True
    
End Sub

Sub Cambiar_Moneda()
    
    Dim MActual As String, MActDec As Integer, mDesc As String, MSimbolo As String, mFACTOR As Double
    Dim RsMoneda As New ADODB.Recordset
    
    MActual = dbmoneda.Text
    mDesc = lbl_moneda.Caption
    MActDec = Std_Decm
    MSimbolo = lbl_simbolo.Caption
    mFACTOR = CDbl(MSK_FACTOR.Text)
    
    Call Consulta_F2(Me, "MONEDA_GENERICA", "MONEDAS")
    CodMoneda = FrmAppLink.GetCodMonedaSel
    
    If CodMoneda <> "" Then
        If Buscar_Moneda(dbmoneda, lbl_moneda, MSK_FACTOR, False, CodMoneda, True, True) = False Then
            'Call Mensaje(True, "No existe una moneda predeterminada en el sistema.")
            Mensaje True, StellarMensaje(16289)
            Unload Me
            Exit Sub
        End If
    End If
    
    If MActual <> dbmoneda.Text Then
        'MsgBox "Cambio la moneda, recuerde colocar el cambio de moneda."
        '
        'ESCRIBIR PROCEDIMIENTOS DE RECALCULAR GRIDS
        '
        Call Apertura_RecordsetC(RsMoneda)
        RsMoneda.Open "select * from ma_monedas where c_CodMoneda = '" & dbmoneda & "' ", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsMoneda.EOF Then
            lbl_simbolo.Caption = RsMoneda!c_Simbolo
        Else
            lbl_simbolo.Caption = "Unk"
        End If
        Std_Decm = RsMoneda!n_Decimales
        RsMoneda.Close
        Call CambiarMonedaPrep(CDbl(MSK_FACTOR.Text), mFACTOR, Std_Decm)
        Call CambiarMonedaProd(CDbl(MSK_FACTOR.Text), mFACTOR, Std_Decm)
        
    End If
    
End Sub

Sub Ini_Moneda()
    Dim RsMoneda As New ADODB.Recordset
    RsMoneda.Open "select * from ma_monedas where b_Activa = 1 AND b_Preferencia = 1", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsMoneda.EOF Then
        dbmoneda.Text = RsMoneda!c_CodMoneda
        lbl_moneda.Caption = RsMoneda!c_Descripcion
        MSK_FACTOR.Text = RsMoneda!n_Factor
        Std_Decm = RsMoneda!n_Decimales
        lbl_simbolo.Caption = RsMoneda!c_Simbolo
        CodMoneda = dbmoneda.Text
    Else
        If Me.Visible Then Unload Me
    End If
End Sub

Function Validar_Repeticiones(Grid As MSFlexGrid, Txt As TextBox) As Boolean
    Dim FilAct As Integer, ColAct As Integer
    Validar_Repeticiones = False
    With Grid
        ColAct = Grid.Col
        FilAct = Grid.Row
        .Enabled = True
        For Cont = 1 To Grid.Rows - 1
            If Grid.TextMatrix(Cont, ColPro.Producto) = Txt.Text Then
                Call SetDefMSGrid(Grid, FilAct, ColAct)
                'Call mensaje(True, "No puede Colocar el mismo producto mas de una vez.")
                .Enabled = False
                Validar_Repeticiones = True
            End If
        Next Cont
        Call SetDefMSGrid(Grid, FilAct, ColAct)
    End With
End Function

Function AlternateCode(Codigo As Variant) As Variant
    Dim RsCodigos As New ADODB.Recordset
    Call Apertura_RecordsetC(RsCodigos)
    RsCodigos.Open "select * from ma_codigos where c_Codigo = '" & Codigo & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsCodigos.EOF Then
        AlternateCode = RsCodigos!c_CodNasa
    Else
        AlternateCode = Codigo
    End If
    RsCodigos.Close
End Function

Function ScanData(SQLI As String) As ADODB.Recordset
    Dim RsTemp As New ADODB.Recordset
    Call Apertura_RecordsetC(RsTemp)
    RsTemp.Open SQLI, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsTemp.EOF Then
        Set ScanData = RsTemp
    Else
        Set ScanData = RsTemp
    End If
End Function

Function InSet(Producto As String, a As MSFlexGrid) As Boolean
    Dim Cont As Integer
    Cont = 1
    InSet = False
    While Not InSet And Cont <= a.Row
        If MSGridRecover(a, Cont, ColPro.Producto) = Producto Then
            InSet = True
        End If
        Cont = Cont + 1
    Wend
End Function

Function FaltaTotal(Monto As Double, Grid As MSFlexGrid) As Double
    Dim Cont As Integer
    FaltaTotal = Monto
    Cont = 1
    While Cont < Grid.Rows
        If MSGridRecover(Grid, Cont, ColPro.CostoProd) <> "" Then
            'FaltaTotal = FormatNumber(FaltaTotal, std_decm) - FormatNumber(CDbl(MSGridRecover(GRID, Cont, ColPro.CostoProd)), std_decm)
            FaltaTotal = FormatNumber(FaltaTotal - CDbl(MSGridRecover(Grid, Cont, ColPro.CostoProd)), Std_Decm)
        End If
        Cont = Cont + 1
    Wend
    FaltaTotal = Round(FaltaTotal, 0)
End Function

Function SumaGrid(Grid As MSFlexGrid, Columna As Integer) As Double
    Dim total As Double, Cont As Integer
    total = 0
    For Cont = 1 To Grid.Rows - 1
        If MSGridRecover(Grid, Cont, Columna) <> "" Then
            total = total + CDbl(MSGridRecover(Grid, Cont, Columna))
        End If
    Next Cont
    SumaGrid = total
End Function

Sub CambiarMonedaPrep(FacNvo As Double, FacAnt As Double, DecMoneda As Integer)
    Dim Cont As Integer
    For Cont = 1 To Preparado.Rows - 1
        If MSGridRecover(Preparado, Cont, ColPre.CostoPreXMerma) <> "" And MSGridRecover(Preparado, Cont, ColPre.CostoPre) <> "" Then
            With Preparado
                .Row = Cont
                .Col = ColPre.CostoPreXMerma
                    .Text = FormatNumber((CDbl(.Text) * FacAnt) / FacNvo, DecMoneda)
                .Col = ColPre.CostoPre
                    .Text = FormatNumber((CDbl(.Text) * FacAnt) / FacNvo, DecMoneda)
            End With
        End If
    Next Cont
    txt_merma.Text = FormatNumber((CDbl(txt_merma.Text) * FacAnt) / FacNvo, DecMoneda)
    txt_monto.Text = FormatNumber((CDbl(txt_monto.Text) * FacAnt) / FacNvo, DecMoneda)
End Sub

Sub CambiarMonedaProd(FacNvo As Double, FacAnt As Double, DecMoneda As Integer)
    Dim Cont As Integer
    For Cont = 1 To Producir.Rows - 1
        If MSGridRecover(Producir, Cont, ColPro.CostoPre) <> "" Then
            With Producir
                .Row = Cont
                .Col = ColPro.CostoPre
                    .Text = FormatNumber((CDbl(.Text) * FacAnt) / FacNvo, DecMoneda)
            End With
        End If
    Next Cont
End Sub

Sub Grabar_Formula()
    
    'TODOS LAS FICHAS SON COMPLETADOS
    Dim lb_consecutivo As String, CanRecord As Boolean, RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset
    Dim RsProducir As New ADODB.Recordset, CostosDirec As Double, Cont As Integer, TTotal As Double
    On Error GoTo RecordFailure
    'VALIDAR ANTES DE COMENZAR
    CanRecord = True
    If Trim(txt_descripcion.Text) = "" Then
        CanRecord = False
        'Call Mensaje(True, "Debe Escribir la Descripci�n para el preparado de la f�rmula.")
        Mensaje True, StellarMensaje(2819)
        SafeFocus txt_descripcion
        Exit Sub
    End If
    If CDbl(txt_costosdirectos.Text) < 0 Then
        CanRecord = False
        Call Mensaje(True, "Los costos directos de producci�n no deben ser negativos.")
        SafeFocus txt_costosdirectos
        Exit Sub
    End If
    If Trim(MSGridRecover(Preparado, 1, ColPre.Producto)) = "" Then
        CanRecord = False
        'Call Mensaje(True, "Especifique el detalle de productos a utilizar.")
        Mensaje True, StellarMensaje(2821)
        SafeFocus Preparado
        Preparado.Col = ColPre.Producto
        Preparado.Row = Preparado.Rows - 1
        Exit Sub
    End If
    If Trim(MSGridRecover(Producir, 1, ColPro.Producto)) = "" Then
        CanRecord = False
        'Call Mensaje(True, "Especifique el detalle de productos a producir.")
        Mensaje True, StellarMensaje(2821)
        SafeFocus Producir
        Producir.Col = ColPro.Producto
        Producir.Row = Producir.Rows - 1
        Exit Sub
    End If
    
    TTotal = FaltaTotal(CDbl(txt_monto.Text) + CDbl(txt_costosdirectos.Text) _
    + CDbl(Me.txt_merma.Text), Producir)
    
    If TTotal > 0 Then
        CanRecord = False
        'Call Mensaje(True, "Problemas con el monto, Difieren el monto de la Formula y las proporciones de los productos...! por: " & FormatNumber(TTotal, Std_Decm) & ")")
        Mensaje True, Replace(StellarMensaje(2820), "$(Total)", FormatNumber(TTotal, Std_Decm))
        SafeFocus txt_descripcion
        Exit Sub
    End If
    'FIN DE VALIDAR
    If CanRecord Then
        Ent.BDD.BeginTrans
        If FlgAdd And Not FlgUpd Then
            lb_consecutivo = Format(No_Consecutivo("PRODUCCION_FORMULA"), "0000000000")
        ElseIf FlgUpd And Not FlgAdd Then
            lb_consecutivo = lbl_consecutivo.Caption
        ElseIf Not FlgUpd And Not FlgAdd Then
            'Call Mensaje(True, "Comun�quese con Soporte T�cnico, no es posible tomar una decisi�n.") ' ???????????
            Exit Sub
        End If
        'GRABAR CABECERO RsMaProduccion
        Call Apertura_RecordsetC(RsMaProduccion)
        RsMaProduccion.Open "SELECT * FROM MA_PRODUCCION WHERE C_FORMULA = '" & lb_consecutivo & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If RsMaProduccion.EOF Then
            RsMaProduccion.AddNew
            RsMaProduccion!add_date = Date
        End If
        RsMaProduccion!C_FORMULA = lb_consecutivo
        RsMaProduccion!c_Descripcion = txt_descripcion.Text
        RsMaProduccion!c_Status = "DCO"
        RsMaProduccion!C_CODUSUARIO = lcCodUsuario
        RsMaProduccion!update_date = Date
        CostosDirec = CDbl(txt_costosdirectos.Text)
        RsMaProduccion!n_Costodir = CostosDirec
        RsMaProduccion!c_CodMoneda = dbmoneda.Text
        RsMaProduccion!c_CodLocalidad = txt_Sucursal.Text
        'GRABAR A PRODUCIR RsProducir
        Call Apertura_RecordsetC(RsProducir)
        Ent.BDD.Execute "DELETE FROM TR_PRODUCCION WHERE C_FORMULA = '" & lb_consecutivo & "' AND B_PRODUCIR = '1'"
        RsProducir.Open "SELECT * FROM TR_PRODUCCION WHERE C_FORMULA = '" & lb_consecutivo & "' AND B_PRODUCIR = '1'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        For Cont = 1 To Producir.Rows - 1
            If MSGridRecover(Producir, Cont, ColPro.Producto) <> "" Then
                RsProducir.AddNew
                RsProducir!C_FORMULA = lb_consecutivo
                RsProducir!n_Cantidad = CDbl(MSGridRecover(Producir, Cont, ColPro.Canti))
                RsProducir!n_merma = 0
                RsProducir!c_Presenta = MSGridRecover(Producir, Cont, ColPro.Presentaci�n)
                RsProducir!c_codproducto = MSGridRecover(Producir, Cont, ColPro.Producto)
                RsProducir!b_producir = "1"
                RsProducir!n_Costo = CDbl(MSGridRecover(Producir, Cont, ColPro.CostoPre)) 'CMM1N
                ' Este costo es simplemente una copia del costo unitario del momento
                ' A la hora de cargar la formula nuevamente no importa ya que se debe
                ' cargar los costos actualizados de la ficha y los factores actualizados
                ' de las monedas involucradas (tanto de los productos como de la transaccion)
                RsProducir!nu_FactorCosto = CDbl(MSGridRecover(Producir, Cont, ColPro.Factor))
            End If
        Next Cont
        'GRABAR A UTILIZAR RsTrProduccion
        Call Apertura_RecordsetC(RsTrProduccion)
        Ent.BDD.Execute "DELETE FROM TR_PRODUCCION WHERE C_FORMULA = '" & lb_consecutivo & "' AND B_PRODUCIR = '0'"
        RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION WHERE C_FORMULA = '" & lb_consecutivo & "' AND B_PRODUCIR = '0'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        For Cont = 1 To Preparado.Rows - 1
            If MSGridRecover(Preparado, Cont, ColPre.Producto) <> "" Then
                RsTrProduccion.AddNew
                RsTrProduccion!C_FORMULA = lb_consecutivo
                RsTrProduccion!n_Cantidad = CDbl(MSGridRecover(Preparado, Cont, ColPre.Canti))
                RsTrProduccion!n_merma = CDbl(MSGridRecover(Preparado, Cont, ColPre.PorcMerma))
                RsTrProduccion!c_Presenta = MSGridRecover(Preparado, Cont, ColPre.Presentaci�n)
                RsTrProduccion!c_codproducto = MSGridRecover(Preparado, Cont, ColPre.Producto)
                RsTrProduccion!b_producir = "0"
                RsTrProduccion!n_Costo = CDbl(MSGridRecover(Preparado, Cont, ColPre.CostoPre))
            End If
        Next Cont
        RsMaProduccion.UpdateBatch
        RsTrProduccion.UpdateBatch
        RsProducir.UpdateBatch
        Ent.BDD.CommitTrans
        RsMaProduccion.Close
        RsTrProduccion.Close
        RsProducir.Close
        GridLleno = False
        Call Cancelar
    Else
        'Call Mensaje(True, "Revise la Forma, faltan algunos campos por llenar.")
        Mensaje True, StellarMensaje(16311)
    End If
    Exit Sub
RecordFailure:
    Ent.BDD.RollbackTrans
    'Call Mensaje(True, "Error de Escritura, por favor comun�quese con Soporte T�cnico")
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    Exit Sub
End Sub

Sub Buscar_Formula(Formula As String)
    'BUSCAR PRODUCCION
    Dim RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset, RsProducir As New ADODB.Recordset, rsMonedas As New ADODB.Recordset
    Dim tmonto As Double, TMerma As Double, Cont As Integer
    Call Cancelar
    Call Apertura_RecordsetC(RsMaProduccion)
    RsMaProduccion.Open "SELECT * FROM MA_PRODUCCION WHERE C_FORMULA = '" & Formula & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    GridLleno = False
    If Not RsMaProduccion.EOF Then
        'BUSCAR DETALLES Y CARGAR
        lbl_consecutivo.Caption = RsMaProduccion!C_FORMULA
        txt_descripcion.Text = RsMaProduccion!c_Descripcion
        'MONEDA
        Call Apertura_Recordset(rsMonedas)
        rsMonedas.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda = '" & RsMaProduccion!c_CodMoneda & "' ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not rsMonedas.EOF Then
            lbl_moneda.Caption = rsMonedas!c_Descripcion
            dbmoneda.Text = rsMonedas!c_CodMoneda
            Std_Decm = rsMonedas!n_Decimales
            MSK_FACTOR.Text = FormatNumber(rsMonedas!n_Factor, Std_Decm)
            lbl_simbolo.Caption = rsMonedas!c_Simbolo
        Else
            'Call Mensaje(True, "No se encontr� la moneda de la f�rmula.")
            Mensaje True, StellarMensaje(2806)
            Call Cancelar
            Exit Sub
        End If
        txt_costosdirectos.Text = FormatNumber(RsMaProduccion!n_Costodir, Std_Decm)
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = '0'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        tmonto = 0
        TMerma = 0
        If Not RsTrProduccion.EOF Then
            Preparado.Rows = RsTrProduccion.RecordCount + 2
            For Cont = 1 To Preparado.Rows - 2
                If RsTrProduccion!n_Activo = 1 Then
                    Call MSGridAsign(Preparado, Cont, ColPre.LDeci, RsTrProduccion!Cant_Decimales)
                    Call MSGridAsign(Preparado, Cont, ColPre.Producto, RsTrProduccion!c_Codigo, , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColPre.Descripci�n, RsTrProduccion!c_Descri, , flexAlignLeftCenter)
                    Call MSGridAsign(Preparado, Cont, ColPre.Canti, FormatNumber(RsTrProduccion!n_Cantidad, RsTrProduccion!Cant_Decimales), , flexAlignRightCenter)
                    If CDbl(MSK_FACTOR.Text) = 0 Then
                        Call Mensaje(True, "La moneda no permite trabajar con produccion")
                        Call Form_Load
                        Exit Sub
                    End If
                    Call MSGridAsign(Preparado, Cont, ColPre.Presentaci�n, RsTrProduccion!c_Presenta, , flexAlignLeftCenter)
                    Call MSGridAsign(Preparado, Cont, ColPre.PorcMerma, FormatNumber(RsTrProduccion!n_merma, 2), , flexAlignRightCenter)
                    
                    'Autor: Angelica Rondon / Descricpci�n: se reemplaza la referencia de CostoAct por la variable CostoActivo
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                    
                    'Call MSGridAsign(Preparado, Cont, ColPre.CostoPreXMerma, FormatNumber((RsTrProduccion!n_merma * ((RsTrProduccion!n_CostoAct * RsTrProduccion!moneda_act) / CDbl(msk_factor.Text))) / 100, Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColPre.CostoPreXMerma, FormatNumber((RsTrProduccion!n_merma * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))) / 100), Std_Decm), , flexAlignRightCenter)
                    
                    'Call MSGridAsign(Preparado, Cont, ColPre.CostoPre, FormatNumber((RsTrProduccion!n_CostoAct * RsTrProduccion!moneda_act) / CDbl(msk_factor.Text), Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColPre.CostoPre, FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                    
                    Call MSGridAsign(Preparado, Cont, ColPre.CostoProd, FormatNumber((RsTrProduccion!n_Cantidad * RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                    
                    Call MSGridAsign(Preparado, Cont, ColPre.Info, "(+)")
                    
                    'tmonto = tmonto + (RsTrProduccion!n_Cantidad * ((RsTrProduccion!n_CostoAct * RsTrProduccion!moneda_act) / CDbl(msk_factor.Text)))
                    tmonto = tmonto + (RsTrProduccion!n_Cantidad * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))))
                    
                    'TMerma = TMerma + (((RsTrProduccion!n_merma * ((RsTrProduccion!n_CostoAct * RsTrProduccion!moneda_act) / CDbl(msk_factor.Text))) / 100) * RsTrProduccion!n_Cantidad)
                    TMerma = TMerma + (((RsTrProduccion!n_merma * ((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)) / 100)))) * RsTrProduccion!n_Cantidad)
                    
                    RsTrProduccion.MoveNext
                    
                Else
                    'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    Exit Sub
                End If
            Next Cont
            
            txt_merma.Text = FormatNumber(TMerma, Std_Decm)
            txt_monto.Text = FormatNumber(tmonto, Std_Decm)
            MTotal.Text = FormatNumber(CDbl(txt_costosdirectos.Text) + tmonto + TMerma, Std_Decm)
            
        Else
            'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
        
        'A PRODUCIR
        Call Apertura_RecordsetC(RsProducir)
        RsProducir.Open "SELECT * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = '1'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsProducir.EOF Then
            
            Producir.Rows = RsProducir.RecordCount + 2
            
            For Cont = 1 To Producir.Rows - 2
                
                If RsProducir!n_Activo = 1 Then
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsProducir!c_CodMoneda
                    
                    Call MSGridAsign(Producir, Cont, ColPro.LDeci, RsProducir!Cant_Decimales)
                    Call MSGridAsign(Producir, Cont, ColPro.Producto, RsProducir!c_Codigo, , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColPro.Descripci�n, RsProducir!c_Descri, , flexAlignLeftCenter)
                    Call MSGridAsign(Producir, Cont, ColPro.Canti, FormatNumber(RsProducir!n_Cantidad, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColPro.Presentaci�n, RsProducir!c_Presenta, , flexAlignLeftCenter)
                    Call MSGridAsign(Producir, Cont, ColPro.CostoPre, FormatNumber((RsProducir!n_Costo) / RsProducir!n_Cantidad, Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColPro.CostoProd, FormatNumber((RsProducir!n_Costo), Std_Decm), , flexAlignRightCenter)
                    'If gCambioHechos Then
                        Call MSGridAsign(Producir, Cont, ColPro.Factor, FormatNumber(RsProducir!nu_FactorCosto, Std_Decm), , flexAlignRightCenter)
                    'End If
                    Call MSGridAsign(Producir, Cont, ColPro.Info, "(+)")
                    
                    RsProducir.MoveNext
                Else
                    'Call Mensaje(True, "El producto '" & RsProducir!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    Exit Sub
                End If
            Next Cont
            
            GridLleno = True
            
            Call DistTotal(CDbl(MTotal.Text), Producir)
            
            RsTrProduccion.Close
            RsProducir.Close
            rsMonedas.Close
            Producir.Row = Producir.Rows - 1
            Producir.Col = ColPro.Producto
            Preparado.Row = Preparado.Rows - 1
            Preparado.Col = ColPre.Producto
            
            txt_Sucursal.Text = RsMaProduccion!c_CodLocalidad
            txt_sucursal_LostFocus
            
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
        
        BarraP.Buttons("Eliminar").Enabled = True
        BarraP.Buttons("Modificar").Enabled = True
        
    Else
        'Call Mensaje(True, "No se encontr� la Formula N� " & Formula)
        Mensaje True, Replace(StellarMensaje(2808), "$(Code)", Formula)
    End If
    
    RsMaProduccion.Close
    
End Sub

Sub Modificar()
    If GridLleno Then
        ModifFlag = True
        FlgUpd = True
        FlgAdd = False
        BarraP.Buttons("Agregar").Enabled = False
        BarraP.Buttons("Modificar").Enabled = False
        BarraP.Buttons("Eliminar").Enabled = False
        BarraP.Buttons("Salir").Enabled = False
        BarraP.Buttons("Buscar").ButtonMenus("BFOR").Enabled = False
        'Activar Botones
        BarraP.Buttons("Grabar").Enabled = True
        BarraP.Buttons("Cancelar").Enabled = True
        BarraP.Buttons("Opciones").Enabled = True
        'BarraP.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
        'Activar Text y Grids
        txt_descripcion.Enabled = True
        SafeFocus txt_descripcion
        txt_costosdirectos.Enabled = True
        txt_Sucursal.Enabled = True
        CmdLocalidad.Enabled = True
        Preparado.Enabled = True
        Producir.Enabled = True
        GridLleno = False
    Else
        Call Cancelar
    End If
End Sub

Sub MenuAccess(EnblGRD As Boolean, EnblTXT As Boolean)
    txtedit.Visible = EnblTXT
    txtedit.Text = ""
    txtedit2.Visible = EnblTXT
    txtedit2.Text = ""
    Preparado.Enabled = EnblGRD
    Producir.Enabled = EnblGRD
End Sub

Sub eliminar(consecutivo As String)
    Dim RsFormula As New ADODB.Recordset
    Call Apertura_RecordsetC(RsFormula)
    RsFormula.Open "select * from ma_produccion where c_formula = '" & consecutivo & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsFormula.EOF Then
        RsFormula.Close
        Ent.BDD.Execute "DELETE FROM MA_PRODUCCION WHERE C_FORMULA = '" & consecutivo & "' "
        Ent.BDD.Execute "DELETE FROM TR_PRODUCCION WHERE C_FORMULA = '" & consecutivo & "' "
        Call Cancelar
    Else
        'Call Mensaje(True, "No se encontr� la F�rmula '" & Format(consecutivo, "0000000000") & "'")
        Mensaje True, Replace(StellarMensaje(2808), "$(Code)", Format(consecutivo, "0000000000"))
        RsFormula.Close
    End If
End Sub

Sub DistTotal(Monto As Double, Grid As MSFlexGrid)
    
    Dim Cont As Integer, Cantidad As Double, dTotal As Double
    
    Dim mSumaFactor As Double, SumaCant As Double
    
    dTotal = Monto
    Cont = 1
    Cantidad = 0
    Grid.FocusRect = flexFocusNone
    
    Cont = 1
    
    While Cont <= Preparado.Rows - 1
        If MSGridRecover(Preparado, Cont, ColPre.Canti) <> "" Then
            Cantidad = CDbl(FormatNumber(Cantidad, CDbl(FormatNumber(MSGridRecover(Preparado, Cont, ColPre.LDeci), Std_Decm)))) + CDbl(FormatNumber(MSGridRecover(Preparado, Cont, ColPre.Canti), CDbl(MSGridRecover(Preparado, Cont, ColPre.LDeci))))
        End If
        Cont = Cont + 1
    Wend
    
    Cont = 1
    
    While Cont <= Preparado.Rows - 1
        If MSGridRecover(Preparado, Cont, ColPre.Canti) <> "" Then
            If Cantidad <> 0 Then
                Call MSGridAsign(Preparado, Cont, ColPre.PorcPartCant, FormatNumber(CDbl(MSGridRecover(Preparado, Cont, ColPre.Canti)) / Cantidad * 100, 2) & "%")
            Else
                Call MSGridAsign(Preparado, Cont, ColPre.PorcPartCant, "N/A")
            End If
            Preparado.TextMatrix(Cont, ColPre.Info) = "(+)"
        End If
        Cont = Cont + 1
    Wend
    
    Cont = 1
    Cantidad = 0
    
    While Cont <= Grid.Rows - 1
        If MSGridRecover(Grid, Cont, ColPro.Canti) <> "" Then
            Cantidad = CDbl(FormatNumber(Cantidad, CDbl(FormatNumber(MSGridRecover(Grid, Cont, ColPro.LDeci), Std_Decm)))) + CDbl(FormatNumber(MSGridRecover(Grid, Cont, ColPro.Canti), CDbl(MSGridRecover(Grid, Cont, ColPro.LDeci))))
        End If
        Cont = Cont + 1
    Wend
    
    For Cont = 1 To Grid.Rows - 1
        mValorCosteo = MSGridRecover(Grid, Cont, ColPro.Factor)
        If IsNumeric(mValorCosteo) Then
            mSumaFactor = mSumaFactor + CDbl(mValorCosteo)
        End If
    Next
    
    For Cont = 1 To Grid.Rows - 1
        If MSGridRecover(Grid, Cont, ColPro.Canti) <> "" Then
            mValorCosteoAct = CDbl(MSGridRecover(Grid, Cont, ColPro.Factor))
            mPorcenPart = mValorCosteoAct / mSumaFactor
            Call MSGridAsign(Grid, Cont, ColPro.CostoProd, FormatNumber((((Monto)) * mPorcenPart), Std_Decm), , flexAlignRightCenter)
            Call MSGridAsign(Grid, Cont, ColPro.CostoPre, _
            FormatNumber((( _
            CDbl(MSGridRecover(Grid, Cont, ColPro.CostoProd)) / _
            CDbl(MSGridRecover(Grid, Cont, ColPro.Canti))) _
            ), Std_Decm), , flexAlignRightCenter)
            If Cantidad <> 0 Then
                Call MSGridAsign(Grid, Cont, ColPro.PorcPartCant, FormatNumber(CDbl(MSGridRecover(Grid, Cont, ColPro.Canti)) / Cantidad * 100, 2) & "%")
            Else
                Call MSGridAsign(Grid, Cont, ColPro.PorcPartCant, "N/A")
            End If
            Grid.TextMatrix(Cont, ColPro.Info) = "(+)"
        End If
    Next Cont
    
    Grid.FocusRect = flexFocusHeavy
    
End Sub

Function Validar_Porcentaje(ByRef Valor As Double) As Boolean
    
    Dim Cont As Integer, Suma As Double
    
    Cont = 1
    Validar_Porcentaje = False
    Suma = 0
    
    While MSGridRecover(Producir, Cont, ColPro.Producto) <> ""
        If MSGridRecover(Producir, Cont, ColPro.Canti) <> "" Then
            Suma = Suma + CDbl(MSGridRecover(Producir, Cont, ColPro.Canti))
        End If
        Cont = Cont + 1
    Wend
    
    Valor = 100 - Suma
    Validar_Porcentaje = (Suma <= 100)
    
End Function

Public Sub Reimprimir_Formula(Tipo As String, Formula As String, _
Optional Numero As Integer = 0, Optional TipoImpresion As String)
    
    Cancelar
    
    Produccion_Reimprimir_Formula Tipo, Formula, Numero, TipoImpresion
    
End Sub
