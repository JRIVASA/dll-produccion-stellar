VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsTeclado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Sub Key_Tab()
    keybd_event VK_TAB, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_Return()
    keybd_event VK_RETURN, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_Down()
    keybd_event VK_DOWN, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_Up()
    keybd_event VK_UP, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_Right()
    keybd_event VK_RIGHT, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_Left()
    keybd_event VK_LEFT, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F1()
    keybd_event VK_F1, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F2()
    keybd_event VK_F2, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F3()
    keybd_event VK_F3, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F4()
    keybd_event VK_F4, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F5()
    keybd_event VK_F5, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F6()
    keybd_event VK_F6, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F7()
    keybd_event VK_F7, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F8()
    keybd_event VK_F8, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F9()
    keybd_event VK_F9, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F10()
    keybd_event VK_F10, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F11()
    keybd_event VK_F11, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_F12()
    keybd_event VK_F12, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

Public Sub Key_BackSpace()
    keybd_event VK_SPACE, 0, KEYEVENTF_EXTENDEKEY, 0
End Sub

