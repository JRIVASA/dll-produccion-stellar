Attribute VB_Name = "Variables"
Public FrmAppLink As Object
Public ClsGrupos As Object

Global CodMoneda            As String
Public MonedaDoc As Object
Public MonedaProd As Object

Public pClsGrupos As Object
Global LibBsn As Object
Public LibBusLink As Object

Public Forma As Object
Public Titulo As String
Public Tabla As String
Public CampoT As Object
Global Campo_Txt As Object
Global Campo_Lbl As Object

Public Const MaxGridRowsxPag = 250

Public CancelarPromo_Salir As Boolean
Public PromocionCreada As Boolean

Public Campanna As String
Public TipoPromo As Integer
Public EstatusPromo As String
Public mCodPromo_Origen             As String
Public mCodPromo_Actual             As String
Public mCodPromo_New                As String

Public RecordsAffected              As Long ' ByRef Output

Public CombinarProductos            As Boolean
Public CantLlevar                   As Double
Public CantPagar                    As Double

Public Find_Concept                 As String
Public Find_Status                  As String
Global LcConsecu                    As String
Global lcLocalidad                  As String

Public Type DTEnv
    BDD                             As Object
    POS                             As Object
    SHAPE_ADM                       As Object
    SHAPE_POS                       As Object
    RsReglasComerciales             As Object
End Type

Public Ent                          As DTEnv

Global Std_Decm As Integer
Global Std_DecC As Integer

Global Producto_Imp         As String
Global RsEureka As New ADODB.Recordset

Global TmpValue As Variant

Global gCls                         As ClsModProduccion
