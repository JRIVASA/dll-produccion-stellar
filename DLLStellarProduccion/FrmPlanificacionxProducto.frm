VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form FrmPlanificacionxProducto 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9180
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   7560
   ControlBox      =   0   'False
   Icon            =   "FrmPlanificacionxProducto.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9180
   ScaleWidth      =   7560
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox ChkVencimientoAutomaticoPeriodo 
      BackColor       =   &H00E7E8E8&
      Caption         =   "Se vence al transcurrir el per�odo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   555
      Left            =   240
      TabIndex        =   39
      Top             =   8100
      Value           =   1  'Checked
      Width           =   2985
   End
   Begin VB.CheckBox chkProduccionParcial 
      BackColor       =   &H00E7E8E8&
      Caption         =   "La Orden de Producci�n acepta producciones parciales"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   555
      Left            =   240
      TabIndex        =   38
      Top             =   7380
      Value           =   1  'Checked
      Width           =   2985
   End
   Begin VB.CommandButton CmdSalir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   5280
      Picture         =   "FrmPlanificacionxProducto.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   7500
      Width           =   1335
   End
   Begin VB.CommandButton CmdAceptar 
      Appearance      =   0  'Flat
      Caption         =   "Ordenar Producci�n"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   3540
      MaskColor       =   &H00FFFFFF&
      Picture         =   "FrmPlanificacionxProducto.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   7500
      Width           =   1335
   End
   Begin VB.Frame FrameMateriaPrima 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   2580
      TabIndex        =   21
      Top             =   660
      Width           =   2295
      Begin VB.Label lblMateriaPrima 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Materia Prima"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   22
         Top             =   30
         Width           =   1995
      End
   End
   Begin VB.Frame FrameHistorial 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   5040
      TabIndex        =   15
      Top             =   660
      Visible         =   0   'False
      Width           =   2295
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Historial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   30
         Top             =   30
         Visible         =   0   'False
         Width           =   1995
      End
   End
   Begin VB.Frame Frame6 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   3585
      Left            =   60
      TabIndex        =   10
      Top             =   3600
      Width           =   7335
      Begin VB.TextBox txtPresentacion 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   44
         Top             =   2880
         Width           =   1215
      End
      Begin VB.TextBox txtTurno 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   42
         Top             =   1680
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker dtpFechaInicioPlanificacion 
         Height          =   375
         Left            =   5700
         TabIndex        =   40
         Top             =   600
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   114819073
         CurrentDate     =   43775
      End
      Begin VB.TextBox txtLineaProduccion 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3000
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   1680
         Width           =   2595
      End
      Begin VB.TextBox txtCapacidadTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   1680
         Width           =   2415
      End
      Begin VB.TextBox txtPeriodo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         Locked          =   -1  'True
         TabIndex        =   23
         Text            =   "Abril 2019"
         Top             =   600
         Width           =   3975
      End
      Begin VB.TextBox txtCantRequeridaTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   2880
         Width           =   2415
      End
      Begin VB.TextBox txtCantPlanificar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3000
         TabIndex        =   11
         Top             =   2880
         Width           =   2535
      End
      Begin VB.Label lblUndMed 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Unidad de Medida"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   5640
         TabIndex        =   43
         Top             =   2280
         Width           =   1635
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Turno"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   5820
         TabIndex        =   41
         Top             =   1200
         Width           =   1635
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "L�nea de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3000
         TabIndex        =   27
         Top             =   1200
         Width           =   2595
      End
      Begin VB.Label lbl_Precio2Iva 
         BackStyle       =   0  'Transparent
         Caption         =   "Capacidad de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1200
         Width           =   2595
      End
      Begin VB.Label lblEtiqPeriodo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Periodo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   24
         Top             =   600
         Width           =   1065
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Planificaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   17
         Top             =   120
         Width           =   1050
      End
      Begin VB.Label lbl_Oferta 
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad Requerida"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   2400
         Width           =   2295
      End
      Begin VB.Label lbl_Precio3 
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad a Planificar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3000
         TabIndex        =   13
         Top             =   2400
         Width           =   1935
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   1320
         X2              =   7125
         Y1              =   240
         Y2              =   240
      End
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   2175
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Width           =   7335
      Begin VB.TextBox txtCantibul 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4800
         Locked          =   -1  'True
         TabIndex        =   37
         Top             =   1680
         Width           =   2055
      End
      Begin VB.TextBox txtStockMaximo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   35
         Top             =   1680
         Width           =   2055
      End
      Begin VB.TextBox txtStockMinimo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   1680
         Width           =   2055
      End
      Begin VB.TextBox txtCodigo 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         TabIndex        =   7
         Top             =   840
         Width           =   2055
      End
      Begin VB.TextBox txtDescri 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2520
         TabIndex        =   6
         Top             =   840
         Width           =   4455
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad por Empaque"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   4800
         TabIndex        =   36
         Top             =   1320
         Width           =   2295
      End
      Begin VB.Label lbl_Precio2 
         BackStyle       =   0  'Transparent
         Caption         =   "Stock Maximo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   2520
         TabIndex        =   34
         Top             =   1320
         Width           =   1515
      End
      Begin VB.Label lbl_Precio1 
         BackStyle       =   0  'Transparent
         Caption         =   "Stock Minimo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   960
         X2              =   7200
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   16
         Top             =   120
         Width           =   750
      End
      Begin VB.Label lbl_codProd 
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label lbl_DescPro 
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   2520
         TabIndex        =   8
         Top             =   480
         Width           =   4455
      End
   End
   Begin VB.Frame FramePlan 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   660
      Width           =   2295
      Begin VB.Label lblPlanificacion 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Planificaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   4
         Top             =   30
         Width           =   1995
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7680
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5115
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   7080
         Picture         =   "FrmPlanificacionxProducto.frx":8CD6
         Top             =   -30
         Width           =   480
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   1545
      Left            =   0
      TabIndex        =   18
      Top             =   1920
      Width           =   7335
      Begin MSFlexGridLib.MSFlexGrid Grid 
         DragMode        =   1  'Automatic
         Height          =   975
         Left            =   120
         TabIndex        =   20
         Top             =   480
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   1720
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00AE5B00&
         X1              =   960
         X2              =   7200
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Historial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   19
         Top             =   120
         Width           =   690
      End
   End
End
Attribute VB_Name = "FrmPlanificacionxProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada        As Boolean

Public FormaDeTrabajo      As Integer
Public MostrarCantibul     As Boolean

Public CodProducto          As String
Public Descripcion          As String
Public Presentacion         As String
Public Periodo              As String
Public DatosPeriodo         As Dictionary
Public CapacidadPeriodo     As String
Public CapacidadEmp         As String
Public CapacidadUni         As String
Public StockMinimo          As String
Public StockMaximo          As String
Public CantDec              As Integer
Public Cantibul             As Double
Public CantidadRequerida    As Double
Public LineaProduccion      As String
Public TurnoProduccion      As String
Public CodLocalidad         As String

Private mMulti              As Double

Private FormulaProducto     As String
Private CantidadFormula     As Double
Private OPRGenerada         As String

Private mObservacionAdicional As String

Public EjecutoPlanificacion As Boolean

Private Sub CmdAgregar_Click()
    EjecutoPlanificacion = False
End Sub

Private Sub CmdAceptar_Click()
    
    If dtpFechaInicioPlanificacion.Value < dtpFechaInicioPlanificacion.MinDate _
    And dtpFechaInicioPlanificacion.Value > dtpFechaInicioPlanificacion.MaxDate Then
        Mensaje True, "Per�odo inv�lido."
        Exit Sub
    End If
    
    mObservacionAdicional = QuickInputRequest("Si desea agregar una observaci�n adicional, " & _
    "especifique, en caso contrario presione aceptar", True, "**[CANCEL]**", mObservacionAdicional, _
    "Observaci�n", , , , , , , True)
    
    If mObservacionAdicional = "**[CANCEL]**" Then
        mObservacionAdicional = Empty
        Exit Sub
    End If
    
    If Mensaje(False, "�Est� seguro de que todos los datos estan correctos? " & _
    "Presione Aceptar para generar la orden de producci�n para este per�odo, " & _
    "o Cancelar para verificar los datos nuevamente.") Then
        
        'If GenerarOPR() Then
            'MostrarDocumentoStellar "OPR", "DPE", OPRGenerada
        Set FRM_PRODUCCION_ORDEN_MANUAL_1 = Nothing
        If FRM_PRODUCCION_ORDEN_MANUAL_1.GenerarOrdenAutomatica(FormulaProducto, _
        dtpFechaInicioPlanificacion.Value, mMulti, "Orden de Producci�n autom�tica en base a Formula N� " & FormulaProducto & " desde Planificaci�n de Producci�n. " & mObservacionAdicional, _
        chkProduccionParcial.Value = vbChecked, ChkVencimientoAutomaticoPeriodo.Value = vbChecked, _
        LineaProduccion, TurnoProduccion) Then
            EjecutoPlanificacion = True
            Set FRM_PRODUCCION_ORDEN_MANUAL_1 = Nothing
            Me.Hide
            Exit Sub
        Else
            Set FRM_PRODUCCION_ORDEN_MANUAL_1 = Nothing
        End If
        
    End If
    
End Sub

Private Function GenerarOPR() As Boolean
    
    On Error GoTo Error
    
    Dim mRsFormula As ADODB.Recordset
    Dim mRsIngredientes  As ADODB.Recordset
    Dim mRsProductoFinal As ADODB.Recordset
    
    Set mRsFormula = New ADODB.Recordset
    Set mRsIngredientes = New ADODB.Recordset
    Set mRsProductoFinal = New ADODB.Recordset
    
    mRsFormula.CursorLocation = adUseClient
    mRsIngredientes.CursorLocation = adUseClient
    mRsProductoFinal.CursorLocation = adUseClient
    
    Dim mRsOrden As ADODB.Recordset
    Dim mRsDetalle  As ADODB.Recordset
    
    cSumaFactor = "ROUND(isNULL((SELECT SUM(nu_FactorCosto) FROM TR_PRODUCCION SUMAFACTOR WHERE SUMAFACTOR.b_Producir = 1 AND SUMAFACTOR.c_Formula = MA.c_Formula), 1), 8, 0)"
    
    mRsFormula.Open "SELECT " & cSumaFactor & " AS FactorProduccion, * FROM MA_PRODUCCION MA WHERE c_Formula = '" & FormulaProducto & "' ", FrmAppLink.CnADM, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRsIngredientes.Open "SELECT TR.*, MA.c_Descri, MA.n_Cantibul, MA.c_CodMoneda AS CodMon, MA.c_Presenta AS Presentacion, MA.Cant_Decimales FROM TR_PRODUCCION TR INNER JOIN MA_PRODUCTOS MA ON TR.c_CodProducto = MA.c_Codigo WHERE TR.c_Formula = '" & FormulaProducto & "' AND TR.b_Producir = 0", FrmAppLink.CnADM, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRsProductoFinal.Open "SELECT TR.*, MA.c_Descri, MA.n_Cantibul, MA.c_CodMoneda AS CodMon, MA.c_Presenta AS Presentacion, MA.Cant_Decimales FROM TR_PRODUCCION TR INNER JOIN MA_PRODUCTOS MA ON TR.c_CodProducto = MA.c_Codigo WHERE TR.c_Formula = '" & FormulaProducto & "' AND TR.b_Producir = 1 ORDER BY CASE WHEN c_CodProducto = '" & CodProducto & "' THEN 1 ELSE 0 END DESC ", FrmAppLink.CnADM, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    mRsFormula.ActiveConnection = Nothing
    mRsIngredientes.ActiveConnection = Nothing
    mRsProductoFinal.ActiveConnection = Nothing
    
    If mRsFormula.EOF Or mRsIngredientes.EOF Or mRsProductoFinal.EOF Then
        Mensaje True, "Hay datos faltantes en la formula, no se puede generar la orden de producci�n."
        Exit Function
    End If
    
    If FormaDeTrabajo = 1 Then
        mFactorCantidad = Round((CDbl(txtCantPlanificar.Text) * Cantibul) / mRsProductoFinal!n_Cantidad, 8)
    Else
        mFactorCantidad = Round(CDbl(txtCantPlanificar.Text) / mRsProductoFinal!n_Cantidad, 8)
    End If
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans: ActiveTrans = True
    
    Dim DocOPR As String, I As Long, mSubtotalOPR As Double, mMermaOPR As Double
    
    DocOPR = Format(No_Consecutivo("Orden_Produccion_Nuevo", True), "000000000")
    
    Set MonedaDoc = FrmAppLink.ClaseMonedaDocumento
    MonedaDoc.BuscarMonedas , mRsFormula!c_CodMoneda
    
    Set mRsDetalle = New ADODB.Recordset
    
    With mRsDetalle
    
    .Open "SELECT * FROM TR_ORDEN_PRODUCCION WHERE 1 = 2", _
    FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    I = 1
    
    Do While Not mRsIngredientes.EOF
        
        .AddNew
        
        !c_Documento = DocOPR
        !c_Linea = I
        !c_CodigoInput = mRsIngredientes!c_CodProducto
        !c_Descripcion = mRsIngredientes!c_Descri
        !c_Presenta = mRsIngredientes!Presentacion
        !c_CodArticulo = !c_CodigoInput
        
        !n_Cantidad = Round(mRsIngredientes!n_Cantidad * mFactorCantidad, CantDec)
        ' No estamos recogiendo el costo activo ni recalculando el monto de la
        ' producci�n debido a que cuando se jale la orden de producci�n igual
        ' se debe tomar es el costo activo del momento y factorizado de acuerdo
        ' a la moneda seleccionada el documento y a la final todos los montos de la
        ' producci�n se vuelven a recalcular en la pantalla de producci�n.
        ' Aqui practicamente lo importante es grabar las cantidades bien y ya.
        !n_Costo = Round(mRsIngredientes!n_Costo, MonedaDoc.DecMoneda)
        !n_PorcMerma = Round(mRsIngredientes!n_Merma, MonedaDoc.DecMoneda)
        !n_MontoMerma = Round((!n_Cantidad * (!n_Costo * !n_PorcMerma / 100)), MonedaDoc.DecMoneda)
        mMermaOPR = Round(mMermaOPR + !n_MontoMerma, MonedaDoc.DecMoneda)
        !n_Subtotal = Round((!n_Cantidad * !n_Costo), MonedaDoc.DecMoneda)
        !n_Total = Round(!n_Subtotal + !n_MontoMerma, MonedaDoc.DecMoneda)
        mSubtotalOPR = Round(mSubtotalOPR + !n_Subtotal, MonedaDoc.DecMoneda)
        !n_Descuento = 0
        !n_Impuesto = 0
        
        !b_Producir = 0
        
        !n_Cant_Utilizada = 0
        !n_Cant_Realizada = 0
        
        !Impuesto1 = 0
        !Impuesto2 = 0
        !Impuesto3 = 0
        
        !c_IDLote = vbNullString
        !n_CostoOriginal = Round(!n_Costo, MonedaDoc.DecMoneda)
        !c_CodLocalidad = CodLocalidad
        !c_MonedaProd = mRsIngredientes!CodMon
        !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, FrmAppLink.CnADM)
        !n_Decimales = mRsIngredientes!Cant_Decimales
        !n_Prod_Ext = vbNullString
        !ns_CantidadEmpaque = mRsIngredientes!n_Cantibul
        !nu_FactorCosto = 1
        
        mRsIngredientes.MoveNext
        I = I + 1
        
    Loop
    
    I = 1
    
    Do While Not mRsProductoFinal.EOF
        
        .AddNew
        
        !c_Documento = DocOPR
        !c_Linea = I
        !c_CodigoInput = mRsProductoFinal!c_CodProducto
        !c_Descripcion = mRsProductoFinal!c_Descri
        !c_Presenta = mRsProductoFinal!Presentacion
        !c_CodArticulo = !c_CodigoInput
        
        !n_Cantidad = Round(mRsProductoFinal!n_Cantidad * mFactorCantidad, CantDec)
        ' No estamos recogiendo el costo activo ni recalculando el monto de la
        ' producci�n debido a que cuando se jale la orden de producci�n igual
        ' se debe tomar es el costo activo del momento y factorizado de acuerdo
        ' a la moneda seleccionada el documento y a la final todos los montos de la
        ' producci�n se vuelven a recalcular en la pantalla de producci�n.
        ' Aqui practicamente lo importante es grabar las cantidades bien y ya.
        mFactorProduccion = mRsFormula!FactorProduccion
        If mFactorProduccion <= 0 Then mFactorProduccion = 1
        mPorcenPart = (mRsProductoFinal!nu_FactorCosto / mFactorProduccion)
        !n_Subtotal = Round(((mSubtotalOPR + mMermaOPR + mRsFormula!n_CostoDir) * mPorcenPart), MonedaDoc.DecMoneda)
        !n_Costo = Round(!n_Subtotal / !n_Cantidad, MonedaDoc.DecMoneda) 'mRsProductoFinal!n_Costo
        !n_PorcMerma = 0
        !n_MontoMerma = 0
        
        !n_Descuento = 0
        !n_Impuesto = 0
        !n_Total = !n_Subtotal
        
        !b_Producir = 1
        
        !n_Cant_Utilizada = 0
        !n_Cant_Realizada = 0
        
        !Impuesto1 = 0
        !Impuesto2 = 0
        !Impuesto3 = 0
        
        !c_IDLote = vbNullString
        !n_CostoOriginal = !n_Costo
        !c_CodLocalidad = CodLocalidad
        !c_MonedaProd = mRsProductoFinal!CodMon
        !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, FrmAppLink.CnADM)
        !n_Decimales = CantDec
        !n_Prod_Ext = vbNullString
        !ns_CantidadEmpaque = Cantibul
        !nu_FactorCosto = mRsProductoFinal!nu_FactorCosto
        
        mRsProductoFinal.MoveNext
        I = I + 1
        
    Loop
    
    End With
    
    Set mRsOrden = New ADODB.Recordset
    
    mRsOrden.Open "SELECT * FROM MA_ORDEN_PRODUCCION WHERE 1 = 2", _
    FrmAppLink.CnADM, adOpenDynamic, adLockOptimistic, adCmdText
    
    With mRsOrden
    
    .AddNew
    
    !c_Documento = DocOPR
    !d_Fecha = FechaBD(Now, FBD_FULL, True)
    !c_Status = "DPE"
    !c_CodLocalidad = CodLocalidad
    !c_CodMoneda = mRsFormula!c_CodMoneda 'FrmAppLink.CodMonedaPref
    !n_FactorCambio = MonedaDoc.FacMoneda 'FrmAppLink.FacMonedaPref 'CMM1N
    !n_CostoDir = mRsFormula!n_CostoDir
    !c_Observacion = "Orden de Producci�n autom�tica en base a Formula N� " & FormulaProducto & " desde Planificaci�n de Producci�n. " & mObservacionAdicional
    !b_BackOrder = CBool(chkProduccionParcial.Value)
    !c_Relacion = vbNullString
    !c_Formula = FormulaProducto
    !c_Descripcion = mRsFormula!c_Descripcion
    !c_Usuario = FrmAppLink.GetCodUsuario
    !d_Fecha_Produccion = FechaBD(dtpFechaInicioPlanificacion.Value, , True) 'FechaBD(DatosPeriodo("fPeriodoIni"), , True)
    !n_Subtotal = Round(mSubtotalOPR, MonedaDoc.DecMoneda)
    !n_MontoMerma = Round(mMermaOPR, MonedaDoc.DecMoneda)
    !n_Descuento = 0
    !n_Impuesto = 0
    !n_Total = Round(mSubtotalOPR + mMermaOPR + !n_CostoDir, MonedaDoc.DecMoneda)
    
    If CBool(ChkVencimientoAutomaticoPeriodo) Then
        !d_FechaVencimiento = FechaBD(DatosPeriodo("fPeriodoFin"), , True)
    Else
        !d_FechaVencimiento = Null
    End If
    
    !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
    !c_LineaProduccion = LineaProduccion
    !c_Turno = TurnoProduccion
    
    End With
    
    mRsDetalle.UpdateBatch
    mRsOrden.UpdateBatch
    
    FrmAppLink.CnADM.CommitTrans: ActiveTrans = False
    
    OPRGenerada = DocOPR
    GenerarOPR = True
    
    Exit Function
    
Error:
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans: ActiveTrans = False
    End If
    
    GenerarOPR = False
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Function

Private Sub CmdSalir_Click()
    Me.Hide
End Sub

Private Sub Exit_Click()
    CmdSalir_Click
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        If BuscarFormula Then
            txtCodigo.Text = CodProducto
            txtDescri.Text = Descripcion
            txtPresentacion.Text = Presentacion
            txtPeriodo.Text = Periodo
            txtCapacidadTotal.Text = CapacidadPeriodo
            txtStockMinimo.Text = StockMinimo
            txtStockMaximo.Text = StockMaximo
            txtCantibul.Text = Cantibul
            txtLineaProduccion.Text = LineaProduccion
            txtTurno.Text = TurnoProduccion
            txtCantRequeridaTotal.Text = CantidadRequerida
            txtCantPlanificar.Text = Empty
            SafeFocus txtCantPlanificar
            AjustarPantalla Me
            FramePlan.Enabled = True
            txtCantPlanificar.Enabled = True
            Me.Enabled = True
            dtpFechaInicioPlanificacion.Value = CDate(DatosPeriodo("nPeriodoIni"))
            dtpFechaInicioPlanificacion.MinDate = dtpFechaInicioPlanificacion.Value
            dtpFechaInicioPlanificacion.MaxDate = CDate(DatosPeriodo("nPeriodoFin"))
        Else
            ActivarMensajeGrande 60
            Mensaje True, "Debe crear primero la formula " & _
            "modelo del producto a producir, necesaria " & _
            "para poder crear automaticamente la orden de producci�n. " & vbNewLine & vbNewLine & _
            "En caso contrario puede planificar generando manualmente " & _
            "ordenes de producci�n asignando la fecha estimada de producci�n " & _
            "en el per�odo actual seleccionado."
            Unload Me
            Exit Sub
        End If
    End If
End Sub

Private Function BuscarFormula() As Boolean
    
    On Error GoTo Error
    
    Dim mSql As String, mRsFormula As ADODB.Recordset
    
    mSql = "SELECT TOP 1 * FROM TR_PRODUCCION TR " & _
    "INNER JOIN MA_PRODUCCION MA " & _
    "ON TR.c_Formula = MA.c_Formula " & _
    "WHERE (MA.c_CodLocalidad = '" & CodLocalidad & "' OR LTRIM(RTRIM(MA.c_CodLocalidad)) = '') " & _
    "AND b_Producir = 1 AND n_Cantidad > 0 " & _
    "AND c_CodProducto = '" & CodProducto & "' " & _
    "ORDER BY CASE WHEN MA.c_CodLocalidad = '" & CodLocalidad & "' THEN 1 ELSE 0 END DESC, n_Cantidad ASC "
    
    Set mRsFormula = FrmAppLink.CnADM.Execute(mSql)
    
    If Not mRsFormula.EOF Then
        FormulaProducto = mRsFormula!c_Formula
        CantidadFormula = mRsFormula!n_Cantidad
        BuscarFormula = True
    End If
    
    Exit Function
    
Error:
    
    BuscarFormula = False
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Function

Private Sub FrameMateriaPrima_Click()
    lblMateriaPrima_Click
End Sub

Private Sub lblMateriaPrima_Click()
    ReimprimirFormulaProduccion Empty, FormulaProducto, , "REIMPRESI�N"
End Sub

Private Sub txtCantPlanificar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtCantPlanificar_LostFocus
    End If
End Sub

Private Sub txtCantPlanificar_LostFocus()
    If Trim(txtCantPlanificar.Text) <> Empty Then
        If IsNumeric(txtCantPlanificar.Text) Then
            If CDbl(txtCantPlanificar.Text) > 0 Then
                If FormaDeTrabajo = 1 Then
                    txtCantPlanificar.Text = FormatNumber(CDbl(txtCantPlanificar.Text), 0)
                Else
                    txtCantPlanificar.Text = FormatNumber(CDbl(txtCantPlanificar.Text), CantDec)
                End If
                ValidarCantidad
                If Trim(txtCantPlanificar.Text) = Empty Then GoTo InvalidCant
                CmdAceptar.Enabled = True
                SafeFocus CmdAceptar
            Else
                GoTo InvalidCant
            End If
        ElseIf UCase(Right(txtCantPlanificar.Text, 2)) = "EM" Then
            txtCantPlanificar.Text = Left(txtCantPlanificar, Len(txtCantPlanificar) - Len("EM"))
            If IsNumeric(txtCantPlanificar.Text) Then
                If CDbl(txtCantPlanificar.Text) > 0 Then
                    If FormaDeTrabajo = 1 Then
                        txtCantPlanificar.Text = FormatNumber(CDbl(txtCantPlanificar.Text), 0)
                    Else
                        txtCantPlanificar.Text = FormatNumber(CDbl(txtCantPlanificar.Text) * Cantibul, CantDec)
                    End If
                    ValidarCantidad
                    If Trim(txtCantPlanificar.Text) = Empty Then GoTo InvalidCant
                    CmdAceptar.Enabled = True
                    SafeFocus CmdAceptar
                Else
                    GoTo InvalidCant
                End If
            Else
                GoTo InvalidCant
            End If
        End If
    Else
InvalidCant:
        txtCantPlanificar.Text = Empty
        CmdAceptar.Enabled = False
    End If
End Sub

Private Sub ValidarCantidad()
    
    Dim mCantidad As Double, mResiduo As Double
    
    mCantidad = CDbl(txtCantPlanificar.Text)
    
    If FormaDeTrabajo = 1 Then
        mMulti = Round((mCantidad * Cantibul) / CantidadFormula, 8)
    Else
        mMulti = Round(mCantidad / CantidadFormula, 8)
    End If
    
    mResiduo = Round(mMulti - Fix(mMulti), CantDec)
    
    If mResiduo > 0 Then
        
        mCantidadMax = FormatNumber((Fix(mMulti) + 1) * CantidadFormula, CantDec)
        mCantidadMin = FormatNumber(Fix(mMulti) * CantidadFormula, CantDec)
        
        ActivarMensajeGrande 75
        Retorno = Mensaje(False, "Atenci�n, la cantidad dispuesta en la formula es de " & FormatNumber(CantidadFormula, CantDec) & vbNewLine & vbNewLine & _
        "Solo puede multiplicar la formula de manera unitaria. Las cantidades m�ximas y m�nimas posibles con respecto a la indicada son: " & mCantidadMax & " o " & mCantidadMin & vbNewLine & vbNewLine & _
        "Presione Aceptar si desea redondear hacia arriba o Cancelar para redondear hacia a abajo.")
        
        If FormaDeTrabajo = 1 Then
            
            If Retorno Then
                txtCantPlanificar.Text = mCantidadMax
            Else
                txtCantPlanificar.Text = mCantidadMin
            End If
            
            If Round(CDbl(txtCantPlanificar.Text) - Fix(CDbl(txtCantPlanificar.Text)), 8) <> 0 Then
                
                ActivarMensajeGrande 75
                
                Mensaje True, "La cantidad de empaques resultante es compatible con la cantidad de la formula, " & _
                "pero la cantidad de empaques debe ser entera. Indique una cantidad de empaque que pueda satisfacer " & _
                "ambos requerimientos (ser entera y que no cause inestabilidad en la formula)."
                
                txtCantPlanificar.Text = Empty
                
            End If
            
        Else
            If Retorno Then
                txtCantPlanificar.Text = mCantidadMax
            Else
                txtCantPlanificar.Text = mCantidadMin
            End If
        End If
        
    End If
    
End Sub
