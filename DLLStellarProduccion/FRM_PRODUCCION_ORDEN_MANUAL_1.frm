VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FRM_PRODUCCION_ORDEN_MANUAL_1 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   405
   ClientWidth     =   15330
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   Tag             =   "FORMULAS"
   Begin VB.Frame FrameTabSumario 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   240
      TabIndex        =   46
      Top             =   2520
      Width           =   4095
      Begin VB.Label lblSumario 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Resumen de Orden de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   47
         Top             =   30
         Width           =   3855
      End
   End
   Begin VB.Frame FrameTabProceso 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   4800
      TabIndex        =   44
      Top             =   2520
      Width           =   3735
      Begin VB.Label lblProceso 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Detalle"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   45
         Top             =   30
         Width           =   3495
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   33
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.TextBox MSK_FACTOR 
      Alignment       =   1  'Right Justify
      CausesValidation=   0   'False
      Enabled         =   0   'False
      Height          =   315
      Left            =   5355
      TabIndex        =   29
      Text            =   "0"
      Top             =   1425
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox dbmoneda 
      Enabled         =   0   'False
      Height          =   315
      Left            =   315
      MaxLength       =   10
      TabIndex        =   28
      Top             =   1395
      Visible         =   0   'False
      Width           =   1365
   End
   Begin VB.Frame frame_datos 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ClipControls    =   0   'False
      Height          =   720
      Left            =   240
      TabIndex        =   26
      Top             =   1635
      Width           =   14775
      Begin VB.CheckBox ModifFlag 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Trabajar sin formula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   11880
         TabIndex        =   3
         Top             =   240
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.CommandButton CmdFormula 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   435
         Left            =   2880
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   120
         Width           =   555
      End
      Begin VB.TextBox txtFormula 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   0
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   190
         Width           =   1515
      End
      Begin VB.TextBox txt_descripcion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4860
         MaxLength       =   50
         TabIndex        =   2
         Top             =   190
         Width           =   5865
      End
      Begin VB.Label lblFormula 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Formula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   38
         Top             =   240
         Width           =   750
      End
      Begin VB.Label lblDescFormula 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3645
         TabIndex        =   27
         Top             =   240
         Width           =   1170
      End
   End
   Begin VB.PictureBox CoolBar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1080
      ScaleWidth      =   21270
      TabIndex        =   24
      Top             =   421
      Width           =   21270
      Begin MSComctlLib.Toolbar BarraO 
         Height          =   810
         Left            =   120
         TabIndex        =   88
         Top             =   120
         Width           =   10140
         _ExtentX        =   17886
         _ExtentY        =   1429
         ButtonWidth     =   1746
         ButtonHeight    =   1429
         ToolTips        =   0   'False
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   9
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "F2 Buscar"
               ImageIndex      =   1
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   3
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "BORD"
                     Text            =   "Orden de Producci�n"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Object.Visible         =   0   'False
                     Key             =   "BDCO"
                     Text            =   "Ordenes Completadas"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Enabled         =   0   'False
                     Key             =   "BPRO"
                     Text            =   "Buscar Producto"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "F4 Grabar"
               ImageIndex      =   2
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   1
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "GORD"
                     Text            =   "Grabar Orden de Producci�n"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Anular"
               Key             =   "Anular"
               Object.ToolTipText     =   "F6 Anular"
               ImageIndex      =   3
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   1
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "AORD"
                     Text            =   "Orden de Producci�n"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Cancelar"
               Key             =   "Cancelar"
               Object.ToolTipText     =   "F7 Cancelar"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Reimprimir"
               Key             =   "Reimprimir"
               Object.ToolTipText     =   "F8 Reimprimir"
               ImageIndex      =   5
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "RORD"
                     Text            =   "Orden de Producci�n"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Object.Visible         =   0   'False
                     Key             =   "RDCO"
                     Text            =   "Ordenes Completadas"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "F12 Salir"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Opciones"
               Key             =   "Opciones"
               Object.ToolTipText     =   "F1 Ayuda"
               ImageIndex      =   7
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Ayuda"
                     Text            =   "Ayuda"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Moneda"
                     Text            =   "Cambiar Moneda"
                  EndProperty
               EndProperty
            EndProperty
         EndProperty
      End
      Begin VB.Label lbl_fecha 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "01/01/2015"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   11040
         TabIndex        =   37
         Top             =   675
         Width           =   3495
      End
      Begin VB.Label lbl_consecutivo 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "000000000"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   12840
         TabIndex        =   36
         Top             =   195
         Width           =   1815
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   35
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lbl_concepto 
         BackStyle       =   0  'Transparent
         Caption         =   "Orden de Producci�n No:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   34
         Top             =   195
         Width           =   2415
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   1095
         Index           =   3
         Left            =   10440
         Top             =   0
         Width           =   4575
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":6A8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":881E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":A5B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":C342
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":E0D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":FE66
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":11BF8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame FrameSumario 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7695
      Left            =   240
      TabIndex        =   49
      Top             =   3000
      Visible         =   0   'False
      Width           =   14895
      Begin VB.ComboBox CboLnP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11775
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   360
         Visible         =   0   'False
         Width           =   2520
      End
      Begin VB.ComboBox CboTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11760
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   960
         Visible         =   0   'False
         Width           =   2520
      End
      Begin VB.TextBox txtPesoTotalMezcla 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10200
         Locked          =   -1  'True
         TabIndex        =   105
         Text            =   "0.00"
         Top             =   4800
         Width           =   1575
      End
      Begin VB.TextBox txtPesoTotalProductos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10200
         Locked          =   -1  'True
         TabIndex        =   101
         Text            =   "0.00"
         Top             =   4200
         Width           =   1575
      End
      Begin VB.TextBox txtCostosDirUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2880
         Locked          =   -1  'True
         TabIndex        =   99
         Text            =   "0.00"
         Top             =   3345
         Width           =   2235
      End
      Begin VB.TextBox txtCostoUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2880
         Locked          =   -1  'True
         TabIndex        =   95
         Text            =   "0.00"
         Top             =   2760
         Width           =   2235
      End
      Begin VB.Frame FrameCargarLotes 
         Appearance      =   0  'Flat
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   6800
         TabIndex        =   93
         Top             =   1680
         Width           =   1695
         Begin VB.Label lblCargarLotes 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Batches"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   94
            Top             =   30
            Width           =   1335
         End
      End
      Begin VB.Frame FrameMultiplicarFormula 
         Appearance      =   0  'Flat
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   4800
         TabIndex        =   91
         Top             =   1680
         Width           =   1695
         Begin VB.Label lblMultiplicarFormula 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Veces"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   92
            Top             =   30
            Width           =   1335
         End
      End
      Begin VB.TextBox txtMulti 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2880
         TabIndex        =   12
         Text            =   "1"
         Top             =   1680
         Width           =   1455
      End
      Begin VB.TextBox txt_Sucursal 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1440
         MaxLength       =   20
         TabIndex        =   7
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   960
         Width           =   1275
      End
      Begin VB.CommandButton CmdLocalidad 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   435
         Left            =   3000
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":1398A
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   960
         Width           =   555
      End
      Begin VB.TextBox txtDescProducto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   5220
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   6
         Top             =   360
         Width           =   4065
      End
      Begin VB.TextBox txtProducir 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1440
         Locked          =   -1  'True
         MaxLength       =   20
         TabIndex        =   4
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   360
         Width           =   1275
      End
      Begin VB.CommandButton CmdProducir 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   435
         Left            =   3000
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_1.frx":1418C
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   330
         Width           =   555
      End
      Begin VB.TextBox txtCostoTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   82
         Text            =   "0.00"
         Top             =   1635
         Width           =   2235
      End
      Begin VB.TextBox txtHorasHombreXUnidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         TabIndex        =   15
         Text            =   "0.00"
         Top             =   7200
         Width           =   1695
      End
      Begin VB.TextBox txtCostoHoraCargaFabril 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         TabIndex        =   14
         Text            =   "0.00"
         Top             =   6600
         Width           =   1695
      End
      Begin VB.TextBox txtCostoHoraHombre 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   10440
         TabIndex        =   13
         Text            =   "0.00"
         Top             =   6000
         Width           =   1695
      End
      Begin VB.TextBox txtCantiLote 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   73
         Text            =   "0"
         Top             =   7200
         Width           =   1575
      End
      Begin VB.TextBox txtCostosDir 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   11040
         Locked          =   -1  'True
         TabIndex        =   68
         Text            =   "0.00"
         Top             =   2280
         Width           =   2235
      End
      Begin VB.TextBox txtCapacidadLote 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   64
         Text            =   "0.00"
         Top             =   6600
         Width           =   1575
      End
      Begin VB.TextBox txtCapacidadMaximaLote 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         TabIndex        =   62
         Text            =   "0.00"
         Top             =   6000
         Width           =   1575
      End
      Begin VB.TextBox txtPesoEmp 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   60
         Text            =   "0.00"
         Top             =   5400
         Width           =   1575
      End
      Begin VB.TextBox txtCantibul 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   58
         Text            =   "1"
         Top             =   4800
         Width           =   1575
      End
      Begin VB.TextBox txtPesoUni 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   56
         Text            =   "0.00"
         Top             =   4200
         Width           =   1575
      End
      Begin VB.TextBox txtCantProducto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2880
         Locked          =   -1  'True
         TabIndex        =   52
         Text            =   "1.00"
         Top             =   2235
         Width           =   1515
      End
      Begin VB.Label lblLineaProduccion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "L�nea de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   9480
         TabIndex        =   108
         Top             =   390
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.Label lblTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Turno de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   9480
         TabIndex        =   107
         Top             =   960
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.Label lblNomenclaturaPesoTotalMezcla 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12240
         TabIndex        =   106
         Top             =   4800
         Width           =   345
      End
      Begin VB.Label lblPesoTotalMezcla 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Total de Mezcla: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   104
         Top             =   4800
         Width           =   2370
      End
      Begin VB.Label lblPesoTotalProductos 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso Total de Productos: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   103
         Top             =   4200
         Width           =   2730
      End
      Begin VB.Label lblNomenclaturaPesoTotalProductos 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12240
         TabIndex        =   102
         Top             =   4200
         Width           =   345
      End
      Begin VB.Label lblNomenclaturaCDirUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5280
         TabIndex        =   100
         Top             =   3390
         Width           =   1065
      End
      Begin VB.Label lblCostosDirUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costos Dir. por Und.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   98
         Top             =   3390
         Width           =   2280
      End
      Begin VB.Label lblCostoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo por Und.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   97
         Top             =   2805
         Width           =   1710
      End
      Begin VB.Label lblNomenclaturaCostoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5280
         TabIndex        =   96
         Top             =   2805
         Width           =   1065
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Aplicar Formula: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   90
         Top             =   1680
         Width           =   1845
      End
      Begin VB.Label lblDescLocalidad 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3720
         TabIndex        =   9
         Top             =   960
         Width           =   5580
      End
      Begin VB.Label lblSucursal 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Localidad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   360
         TabIndex        =   89
         Top             =   1080
         Width           =   900
      End
      Begin VB.Label lblDescProducto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3765
         TabIndex        =   87
         Top             =   420
         Width           =   1470
      End
      Begin VB.Label lblProducto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   360
         TabIndex        =   86
         Top             =   420
         Width           =   885
      End
      Begin VB.Label lblNomenclaturaCostoTotal 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13440
         TabIndex        =   83
         Top             =   1680
         Width           =   315
      End
      Begin VB.Label lblCostoTotal 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Total: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   9120
         TabIndex        =   81
         Top             =   1680
         Width           =   1380
      End
      Begin VB.Label lblNomenclaturaHorasHombreXUnidad 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Horas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   80
         Top             =   7200
         Width           =   615
      End
      Begin VB.Label lblHorasHombreXUnidad 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cant. Hora Hombre / Und.: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   79
         Top             =   7200
         Width           =   2985
      End
      Begin VB.Label lblNomenclaturaCostoHoraCargaFabril 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Hora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   78
         Top             =   6600
         Width           =   1065
      End
      Begin VB.Label lblCostoHoraCargaFabril 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Hora Carga Fabril: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   77
         Top             =   6600
         Width           =   2700
      End
      Begin VB.Label lblNomenclaturaCostoHoraHombre 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs. / Hora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   12360
         TabIndex        =   76
         Top             =   6000
         Width           =   1065
      End
      Begin VB.Label lblCostoHoraHombre 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo Hora Hombre: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7200
         TabIndex        =   75
         Top             =   6000
         Width           =   2280
      End
      Begin VB.Label lblNomenclaturaUnidadesXLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   74
         Top             =   7200
         Width           =   510
      End
      Begin VB.Label lblUnidadesXLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cant. / Batch: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   72
         Top             =   7200
         Width           =   1530
      End
      Begin VB.Label lblNomenclaturaCantibul 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   71
         Top             =   4800
         Width           =   510
      End
      Begin VB.Label lblNomenclaturaPesoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   70
         Top             =   4200
         Width           =   345
      End
      Begin VB.Label lblNomenclaturaPesoEmp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   69
         Top             =   5400
         Width           =   345
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Unidad de Medida: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   4800
         TabIndex        =   67
         Top             =   2280
         Width           =   2070
      End
      Begin VB.Label lblNomenclaturaCapMaxLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   66
         Top             =   6000
         Width           =   345
      End
      Begin VB.Label lblNomenclaturaCapLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Kg."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   5400
         TabIndex        =   65
         Top             =   6600
         Width           =   345
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Capacidad Batch: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   63
         Top             =   6600
         Width           =   1890
      End
      Begin VB.Label lblCapacidadMaximaLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cap. Max. Batch: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   61
         Top             =   6000
         Width           =   1860
      End
      Begin VB.Label lblPesoEmp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso por Empaque: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   59
         Top             =   5400
         Width           =   2160
      End
      Begin VB.Label lblCantibul 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Und. por Empaque: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   57
         Top             =   4800
         Width           =   2175
      End
      Begin VB.Label lblPesoUni 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Peso por Und: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   55
         Top             =   4200
         Width           =   1605
      End
      Begin VB.Label lblNomenclaturaCant 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Und."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   6960
         TabIndex        =   54
         Top             =   2280
         Width           =   1515
      End
      Begin VB.Label lblNomenclaturaCDir 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   13440
         TabIndex        =   53
         Top             =   2280
         Width           =   315
      End
      Begin VB.Label lblCantidadProducto 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cantidad Producto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   360
         TabIndex        =   51
         Top             =   2280
         Width           =   2040
      End
      Begin VB.Label lblTotalCostosDir 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costos Directos: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   9120
         TabIndex        =   50
         Top             =   2280
         Width           =   1800
      End
   End
   Begin VB.Frame FrameProceso 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7695
      Left            =   240
      TabIndex        =   39
      Top             =   3000
      Width           =   14895
      Begin VB.TextBox txtEdit2 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   1680
         TabIndex        =   85
         Top             =   3840
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.ComboBox CboPCD 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         Style           =   2  'Dropdown List
         TabIndex        =   84
         Top             =   6720
         Visible         =   0   'False
         Width           =   2760
      End
      Begin MSFlexGridLib.MSFlexGrid Ingredientes 
         CausesValidation=   0   'False
         Height          =   2505
         Left            =   0
         TabIndex        =   16
         Top             =   360
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   4419
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid TotalesIngredientes 
         CausesValidation=   0   'False
         Height          =   420
         Left            =   0
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   2925
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   741
         _Version        =   393216
         Rows            =   1
         Cols            =   8
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   0   'False
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid Empaques 
         CausesValidation=   0   'False
         Height          =   1305
         Left            =   0
         TabIndex        =   17
         Top             =   3430
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   2302
         _Version        =   393216
         Rows            =   1
         Cols            =   8
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid TotalesEmpaques 
         CausesValidation=   0   'False
         Height          =   420
         Left            =   0
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   4800
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   741
         _Version        =   393216
         Rows            =   1
         Cols            =   8
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   0   'False
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid CostosDirectos 
         CausesValidation=   0   'False
         Height          =   1785
         Left            =   120
         TabIndex        =   18
         Top             =   5760
         Width           =   6975
         _ExtentX        =   12303
         _ExtentY        =   3149
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid GrdTotales 
         CausesValidation=   0   'False
         Height          =   1785
         Left            =   7320
         TabIndex        =   19
         Top             =   5760
         Width           =   7335
         _ExtentX        =   12938
         _ExtentY        =   3149
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         AllowBigSelection=   0   'False
         Enabled         =   -1  'True
         FocusRect       =   2
         HighLight       =   0
         GridLinesFixed  =   0
         MergeCells      =   4
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line LnProductosUtilizar 
         BorderColor     =   &H00AE5B00&
         X1              =   2505
         X2              =   14500
         Y1              =   165
         Y2              =   165
      End
      Begin VB.Label lblTotales 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Totales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   7320
         TabIndex        =   48
         Top             =   5400
         Width           =   1005
      End
      Begin VB.Line LnTotales 
         BorderColor     =   &H00AE5B00&
         X1              =   8505
         X2              =   14500
         Y1              =   5565
         Y2              =   5565
      End
      Begin VB.Label lblCostosDir 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costos Directos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   120
         TabIndex        =   43
         Top             =   5400
         Width           =   1635
      End
      Begin VB.Line LnCostoDir 
         BorderColor     =   &H00AE5B00&
         X1              =   1905
         X2              =   7000
         Y1              =   5565
         Y2              =   5565
      End
      Begin VB.Label lblProductosUtilizar 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Productos a Utilizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Left            =   120
         TabIndex        =   40
         Top             =   0
         Width           =   2055
      End
   End
   Begin VB.TextBox txtedit 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1980
      TabIndex        =   20
      Top             =   4320
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.Label lbl_moneda 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1755
      TabIndex        =   30
      Top             =   1425
      Visible         =   0   'False
      Width           =   3525
   End
   Begin VB.Label LBL_TEMP 
      BackStyle       =   0  'Transparent
      Height          =   210
      Left            =   10950
      TabIndex        =   25
      Top             =   9840
      Visible         =   0   'False
      Width           =   840
   End
   Begin VB.Label lbl_simbolo2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   5445
      TabIndex        =   23
      Top             =   7020
      Width           =   615
   End
   Begin VB.Label lbl_simbolo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   10740
      TabIndex        =   22
      Top             =   5280
      Width           =   615
   End
   Begin VB.Label QUIEN_SOY 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "F�RMULAS DE PRODUCCI�N"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   -570
      TabIndex        =   21
      Top             =   1110
      Visible         =   0   'False
      Width           =   4170
   End
End
Attribute VB_Name = "FRM_PRODUCCION_ORDEN_MANUAL_1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Tecla_Pulsada As Boolean, Who_Ami As Integer
Dim cproducto As String, cEureka As String, cRecetas As String, Criterio As String, cinventario As String
Dim RsProducto As New ADODB.Recordset, RsRecetas As New ADODB.Recordset, rsPmoneda As New ADODB.Recordset
Dim RsInventario As New ADODB.Recordset, RsCodigos As New ADODB.Recordset
Dim Valor_Temp As Variant, Lines As Integer, Cols As Integer
Dim CuantosDel As Integer, CostoActivo As String, DecGrid(0) As Integer
Dim RsEureka As New ADODB.Recordset, FlgAdd As Boolean, FlgUpd As Boolean
Dim Cambio As Double, Cod_Moneda As String, GridLleno As Boolean, LastConsecutivo As String
Dim mCostoOrig  As Double, mDecimales As Double

Private mClsGrupos As New ClsGrupos

Private TmpDisableEvent As Boolean
Private TmpNoCalcular   As Boolean

Private mRsProducto As ADODB.Recordset

Private ManejaMermaExplicita            As Boolean
Private FormaDeTrabajoRes               As Integer
Private MultiplicadorCantibul           As Double
Private mMultiplicar                    As Double
Private CantidadBaseFormula             As Double

Private NomenclaturaFDT                 As String

Private Enum ColPre
    LDeci
    Producto
    Descripci�n
    Presentaci�n
    CantLote
    PorcLoteReal
    PorcLote
    Canti
    PorcMerma
    CostoPreXMerma
    NetCant
    CostoPre
    CostoProd
    CostoProdNeto
    PorcTipoCosto
    PorcCostoGlobal
    nCantibul
    Info
    [ColCount]
End Enum

Private Enum ColDir
    TipoCosto
    Monto
    PorcCostoGlobal
    [ColCount]
End Enum

Private Enum ColTot
    TipoCosto
    Monto
    PorcCostoGlobal
    [ColCount]
End Enum

Private FormulaOrigenOPR    As String
Private ObservacionInicial  As String
Private DocumentoRelacion   As String
Private esBackOrder         As Boolean
Private FechaEstimadaPRD    As Date
Private FechaVcto           As Double
Private EstatusOPR          As String

Private mOrdenAutomatica                             As Boolean
Private mOrdenAutomatica_Formula                     As String
Private mOrdenAutomatica_FechaProduccion             As Date
Private mOrdenAutomatica_CantidadInicial             As Double
Private mOrdenAutomatica_CantidadFinal               As Double
Private mOrdenAutomatica_Observacion                 As String
Private mOrdenAutomatica_BackOrder                   As Boolean
Private mOrdenAutomatica_Vencimiento                 As Boolean
Private mOrdenAutomatica_LineaProduccion             As String
Private mOrdenAutomatica_TurnoProduccion             As String
Private mOrdenAutomatica_LocalidadAlterna            As String
Private mOrdenAutomatica_Fallo                       As Boolean
Private mOrdenAutomatica_Grabo                       As Boolean

Public Function GenerarOrdenAutomatica(ByVal pFormula, ByVal pFechaProduccion As Date, _
ByVal pCantidad, ByVal pObservacion, ByVal pBackOrder As Boolean, ByVal pVctoAutomaticoPeriodo, _
Optional ByVal pLineaPrd As String = "", Optional ByVal pTurnoPrd As String = "", _
Optional pLocalidad As String = "") As Boolean
    
    mOrdenAutomatica = True
    mOrdenAutomatica_Formula = pFormula
    mOrdenAutomatica_FechaProduccion = pFechaProduccion
    mOrdenAutomatica_CantidadInicial = pCantidad
    mOrdenAutomatica_Observacion = pObservacion
    mOrdenAutomatica_BackOrder = pBackOrder
    mOrdenAutomatica_Vencimiento = pVctoAutomaticoPeriodo
    mOrdenAutomatica_LocalidadAlterna = pLocalidad
    
    Form_Load
    
    txtFormula = pFormula
    'txtFormula_KeyDown vbKeyF2, 0
    
    txtMulti = mOrdenAutomatica_CantidadInicial
    lblMultiplicarFormula_Click
    
    If mOrdenAutomatica_Fallo Then
        Unload Me
        Exit Function
    End If
    
    FormulaOrigenOPR = pFormula
    ObservacionInicial = pObservacion
    DocumentoRelacion = Empty
    esBackOrder = pBackOrder
    FechaEstimadaPRD = pFechaProduccion
    If mOrdenAutomatica_Vencimiento Then
        FechaVcto = CDbl(pFechaProduccion) + 1
    Else
        FechaVcto = 0
    End If
    If pLineaPrd <> Empty Then
        ListSafeItemSelection CboLnP, pLineaPrd
    End If
    If pTurnoPrd <> Empty Then
        ListSafeItemSelection CboTurno, pTurnoPrd
    End If
    
    BarraO_ButtonMenuClick BarraO.Buttons(3).ButtonMenus("GORD")
    
    If mOrdenAutomatica_Fallo Then
        Unload Me
        Exit Function
    End If
    
    If mOrdenAutomatica_Grabo Then
        GenerarOrdenAutomatica = True
        Unload Me
        Exit Function
    End If
    
End Function

Private Sub BarraO_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case "BUSCAR"
            Tecla_Pulsada = True
            'Call Form_KeyDown(vbKeyF2, 0)
            BarraO_ButtonMenuClick BarraO.Buttons(1).ButtonMenus("BORD")
            
        Case "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
            
        Case "ANULAR"
            Call Form_KeyDown(vbKeyF6, 0)
        
        Case "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        
        Case "REIMPRIMIR"
            Call Form_KeyDown(vbKeyF8, 0)
        
        Case "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        
        Case "AYUDA"
            'Ayuda
            
    End Select
    
End Sub

Private Sub BarraO_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    
    Select Case UCase(ButtonMenu.Key)
        
        Case "BPRO"
            'Call Buscar_Producto(Codigo)
            Select Case Who_Ami
                Case 1
                    If Not txtedit.Visible Then
                        Producto_Imp = Empty
                        Tecla_Pulsada = True
                        If Ingredientes.Col = ColPre.Producto Then
                            mArrProducto = BuscarInfoProducto_Basica(Ent.BDD, , , , , FrmBuscarProducto_Vista.VistaMarca, True)
                            
                            If Not IsEmpty(mArrProducto) Then
                                txtedit = mArrProducto(0)
                                Tecla_Pulsada = False
                                Call txtEdit_LostFocus
                            Else
                                txtedit = Empty
                            End If
                        End If
                        Tecla_Pulsada = False
                    End If
                Case 2
                    If Not txtEdit2.Visible Then
                        Producto_Imp = Empty
                        Tecla_Pulsada = True
                        If Empaques.Col = ColPre.Producto Then
                            mArrProducto = BuscarInfoProducto_Basica(Ent.BDD, , , , , FrmBuscarProducto_Vista.VistaMarca, True)
                            
                            If Not IsEmpty(mArrProducto) Then
                                txtedit = mArrProducto(0)
                                Tecla_Pulsada = False
                                Call txtedit2_LostFocus
                            Else
                                txtEdit2 = Empty
                            End If
                        End If
                        Tecla_Pulsada = False
                    End If
            End Select
            
        Case "GORD", "GESP"
            'GRABAR
            Call Grabar_Orden(UCase(ButtonMenu.Key))
        
        Case "BPRD", "BPRDESP"
            'BUSCAR
            Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "BPRD", "DCO", "DWT"), False, False)
            
        Case "BORD", "BESP", "BDCO"
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, _
            IIf(UCase(ButtonMenu.Key) = "BORD", "DPE", _
            IIf(UCase(ButtonMenu.Key) = "BDCO", "DCO", "DWT")), _
            False, False, , , "c_CodLocalidad")
            
        Case "AORD", "AESP"
            
            'ANULAR
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "AORD", "DPE", "DWT"), True, False, , , "c_CodLocalidad")
            
        Case "RESP", "RORD"
            
            FrmAppLink.EnableTmpDLLProduccion1 = gCls
            'REIMPRIMIR
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "RORD", "DPE", "DWT"), False, True, , , "c_CodLocalidad")
            FrmAppLink.EnableTmpDLLProduccion1 = Nothing
            
        Case "RDCO"
            
            FrmAppLink.EnableTmpDLLProduccion1 = gCls
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, "DCO", False, True, , , "c_CodLocalidad")
            FrmAppLink.EnableTmpDLLProduccion1 = Nothing
            
        Case "MONEDA"
            
            Call Cambiar_Moneda
            
    End Select
    
End Sub

Private Sub CmdFormula_Click()
    txtFormula_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdLocalidad_Click()
    Tecla_Pulsada = True
    SafeFocus txt_sucursal
    Call txt_sucursal_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub CmdProducir_Click()
    mArr = BuscarInfoProducto_Basica(Ent.BDD, , False, , , VistaMarca, , True)
    If Not IsEmpty(mArr) Then
        txtProducir.Text = mArr(0)
        txtProducir_LostFocus
    Else
        txtProducir.Text = Empty
        txt_descripcion.Text = Empty
    End If
End Sub

Private Sub CargarGrid()
    
    ' Para Ingredientes
    
    NomenclaturaFDT = IIf(FormaDeTrabajoRes = 1, "Emp.", _
    IIf(lblNomenclaturaCant <> Empty, StrConv(lblNomenclaturaCant, vbProperCase), Empty))
    
    Ingredientes.Rows = 1
    Ingredientes.Rows = 2
    
    Ingredientes.AllowUserResizing = flexResizeColumns
    Ingredientes.Cols = ColPre.ColCount
    Ingredientes.WordWrap = True
    Ingredientes.ScrollTrack = True
    Ingredientes.ScrollBars = flexScrollBarBoth
    Ingredientes.Row = 0
    Ingredientes.RowHeight(0) = 720
    
    Call MSGridAsign(Ingredientes, 0, ColPre.LDeci, "LDec", 0, flexAlignRightCenter)
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Producto, Stellar_Mensaje(5010), 1400, flexAlignCenterCenter) 'producto
    Call MSGridAsign(Ingredientes, 0, ColPre.Descripci�n, Stellar_Mensaje(143), 4500, flexAlignCenterCenter) 'descripcion
    Call MSGridAsign(Ingredientes, 0, ColPre.Presentaci�n, FrmAppLink.PRDLabelCampoPresenta, 1200, flexAlignCenterCenter) 'presentacion
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CantLote, "Cantidad Batch", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcLoteReal, "% Batch Real", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcLote, "% Batch", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Canti, Stellar_Mensaje(3001) & _
    IIf(NomenclaturaFDT <> Empty, " por " & NomenclaturaFDT, Empty), IIf(ModifFlag, 930, 0), IIf(ModifFlag, flexAlignCenterCenter, flexAlignRightCenter)) 'cantidad
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    If ManejaMermaExplicita Then
        
        Call MSGridAsign(Ingredientes, 0, ColPre.PorcMerma, Stellar_Mensaje(5013), IIf(ModifFlag, 750, 0), IIf(ModifFlag, flexAlignCenterCenter, flexAlignRightCenter)) '% merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
        Call MSGridAsign(Ingredientes, 0, ColPre.CostoPreXMerma, Stellar_Mensaje(5011), 0, flexAlignRightCenter) 'costo x %merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
        Call MSGridAsign(Ingredientes, 0, ColPre.NetCant, "Cantidad Estandar de Orden", 1200, flexAlignCenterCenter) 'cantidad
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
    Else
        
        Call MSGridAsign(Ingredientes, 0, ColPre.PorcMerma, Stellar_Mensaje(5013), 0, flexAlignRightCenter) '% merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
        Call MSGridAsign(Ingredientes, 0, ColPre.CostoPreXMerma, Stellar_Mensaje(5011), 0, flexAlignRightCenter) 'costo x %merma
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
        Call MSGridAsign(Ingredientes, 0, ColPre.NetCant, "Cantidad Estandar de Orden", 0, flexAlignRightCenter) 'cantidad
        Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
        
    End If
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CostoPre, Replace(Stellar_Mensaje(151), ":", ""), 2000, flexAlignCenterCenter)   'costo
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CostoProd, Replace(Stellar_Mensaje(151), ":", ""), 0, flexAlignRightCenter)   'costo
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.CostoProdNeto, Replace(Stellar_Mensaje(2938), ":", "") & _
    Empty, 2000, flexAlignCenterCenter)
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcTipoCosto, "% Costo / Tipo Material", 900, flexAlignCenterCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.PorcCostoGlobal, "% Costo Total", 900, flexAlignCenterCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.nCantibul, "Cant. por Emp.", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call MSGridAsign(Ingredientes, 0, ColPre.Info, "Info", 0, flexAlignRightCenter) 'presentacion
    Ingredientes.ColAlignment(Ingredientes.Col) = flexAlignRightCenter
    
    Call SetDefMSGrid(Ingredientes, 1, ColPre.Producto)
    
    '
    
    With TotalesIngredientes
        
        .Rows = 0
        .Rows = 1
        
        .AllowUserResizing = flexResizeColumns
        .Cols = ColPre.ColCount
        .WordWrap = True
        .ScrollTrack = True
        .ScrollBars = flexScrollBarBoth
        .Row = 0
        .RowHeightMin = Ingredientes.RowHeightMin
        
        For I = 0 To .Cols - 1
            .Col = I
            .ColWidth(I) = Ingredientes.ColWidth(I)
            .ColAlignment(I) = Ingredientes.ColAlignment(I)
            .CellBackColor = 11857337
        Next I
        
    End With
    
    With Empaques
        
        .Rows = 0
        .Rows = 1
        
        .AllowUserResizing = flexResizeColumns
        .Cols = ColPre.ColCount
        .WordWrap = True
        .ScrollTrack = True
        .ScrollBars = flexScrollBarBoth
        .Row = 0
        .RowHeightMin = Ingredientes.RowHeightMin
        
        For I = 0 To .Cols - 1
            .ColWidth(I) = Ingredientes.ColWidth(I)
            .ColAlignment(I) = Ingredientes.ColAlignment(I)
        Next I
        
    End With
    
    With TotalesEmpaques
        
        .Rows = 0
        .Rows = 1
        
        .AllowUserResizing = flexResizeColumns
        .Cols = ColPre.ColCount
        .WordWrap = True
        .ScrollTrack = True
        .ScrollBars = flexScrollBarBoth
        .Row = 0
        .RowHeightMin = Ingredientes.RowHeightMin
        
        For I = 0 To .Cols - 1
            .Col = I
            .ColWidth(I) = Ingredientes.ColWidth(I)
            .ColAlignment(I) = Ingredientes.ColAlignment(I)
            .CellBackColor = 11857337
        Next I
        
    End With
    
    ' Para Costos Directos
    
    CostosDirectos.Rows = 1
    CostosDirectos.Rows = 2
    
    CostosDirectos.AllowUserResizing = flexResizeColumns
    CostosDirectos.Cols = ColDir.ColCount
    CostosDirectos.WordWrap = True
    CostosDirectos.ScrollTrack = True
    CostosDirectos.ScrollBars = flexScrollBarBoth
    CostosDirectos.Row = 0
    CostosDirectos.RowHeight(0) = 360
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.TipoCosto, "Tipo Costo", 3100, flexAlignCenterCenter)
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignLeftCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.Monto, "Monto", 2100, flexAlignCenterCenter)
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    Call MSGridAsign(CostosDirectos, 0, ColDir.PorcCostoGlobal, "% Costo", 1350, flexAlignCenterCenter)
    CostosDirectos.ColAlignment(CostosDirectos.Col) = flexAlignRightCenter
    
    ' Para Totalizar
    
    GrdTotales.Rows = 1
    GrdTotales.Rows = 4
    
    GrdTotales.AllowUserResizing = flexResizeColumns
    GrdTotales.Cols = ColTot.ColCount
    GrdTotales.WordWrap = True
    GrdTotales.ScrollTrack = True
    GrdTotales.ScrollBars = flexScrollBarBoth
    GrdTotales.Row = 0
    GrdTotales.RowHeight(0) = 360
    
    Call MSGridAsign(GrdTotales, 0, ColTot.TipoCosto, "Tipo Costo", 3000, flexAlignCenterCenter)
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignLeftCenter
    
    Call MSGridAsign(GrdTotales, 0, ColTot.Monto, "Monto", 2400, flexAlignCenterCenter)
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignRightCenter
    
    Call MSGridAsign(GrdTotales, 0, ColTot.PorcCostoGlobal, "% Costo", 1350, flexAlignCenterCenter)
    GrdTotales.ColAlignment(GrdTotales.Col) = flexAlignRightCenter
    
    GrdTotales.Row = 1
    GrdTotales.TextMatrix(1, ColTot.TipoCosto) = "MATERIA PRIMA Y EMPAQUE"
    GrdTotales.TextMatrix(1, ColTot.Monto) = FormatNumber(0, Std_Decm)
    GrdTotales.TextMatrix(1, ColTot.PorcCostoGlobal) = FormatNumber(0, Std_Decm) & "%"
    CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
    
    GrdTotales.Row = 2
    GrdTotales.TextMatrix(2, ColTot.TipoCosto) = "COSTOS DIRECTOS"
    GrdTotales.TextMatrix(2, ColTot.Monto) = FormatNumber(0, Std_Decm)
    GrdTotales.TextMatrix(2, ColTot.PorcCostoGlobal) = FormatNumber(0, Std_Decm) & "%"
    CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
    
    If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelVerCostosDirectos _
    Or FrmAppLink.PRDNivelVerCostosDirectos = 0 Then
        
        GrdTotales.RowHeight(2) = 0
        
        CostosDirectos.Visible = False
        lblCostosDir.Visible = False
        LnCostoDir.Visible = False
        
        lblTotales.Left = 4080
        LnTotales.X1 = 5265
        LnTotales.X2 = 11260
        GrdTotales.Left = lblTotales.Left
        
        lblTotalCostosDir.Visible = False
        txtCostosDir.Visible = False
        lblNomenclaturaCDir.Visible = False
        
        lblCostosDirUni.Visible = False
        txtCostosDirUni.Visible = False
        lblNomenclaturaCDirUni.Visible = False
        
        lblCostoHoraHombre.Visible = False
        lblCostoHoraCargaFabril.Visible = False
        lblHorasHombreXUnidad.Visible = False
        
        txtCostoHoraHombre.Visible = False
        txtCostoHoraCargaFabril.Visible = False
        txtHorasHombreXUnidad.Visible = False
        
        lblNomenclaturaCostoHoraHombre.Visible = False
        lblNomenclaturaCostoHoraCargaFabril.Visible = False
        lblNomenclaturaHorasHombreXUnidad.Visible = False
        
    End If
    
    GrdTotales.Row = 3
    GrdTotales.TextMatrix(3, ColTot.TipoCosto) = "GENERAL"
    GrdTotales.TextMatrix(3, ColTot.Monto) = FormatNumber(0, Std_Decm)
    GrdTotales.TextMatrix(3, ColTot.PorcCostoGlobal) = FormatNumber(0, Std_Decm) & "%"
    CambiarColorFila GrdTotales, GrdTotales.Row, 16761024
    
    '
    
    lblNomenclaturaCostoTotal = lbl_simbolo
    lblNomenclaturaCDir = lbl_simbolo
    lblNomenclaturaCostoUni = lbl_simbolo & " / " & NomenclaturaFDT
    lblNomenclaturaCDirUni = lbl_simbolo & " / " & NomenclaturaFDT
    
    If FormaDeTrabajoRes = 1 Then
        lblNomenclaturaCant = "Emp."
        lblNomenclaturaUnidadesXLote = "Emp."
        lblHorasHombreXUnidad = "Cant. Hora Hombre / Emp.: "
    End If
    
    FrameSumario.Height = FrameProceso.Height
    
    CargarCostosDirectos
    
End Sub

Private Sub CostosDirectos_Click()
    With CostosDirectos
        If .Col = ColDir.TipoCosto Then
            If .TextMatrix(.Row, .Col) = Empty Then
                mClsGrupos.cTipoGrupo = "PCD"
                mClsGrupos.CargarComboGrupos Ent.BDD, CboPCD
                Tecla_Pulsada = True
                ListRemoveItems CboPCD, " ", "Ninguno"
                Tecla_Pulsada = False
                CboPCD.Move .CellLeft + .Left, .CellTop + .Top
                CboPCD.Visible = True
            End If
        ElseIf .Col = ColDir.Monto Then
            mValorAnt = .Text
            mValorNew = QuickInputRequest("Ingrese el Monto relacionado al Costo Directo.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
            If IsNumeric(mValorNew) Then
                If Not IsNumeric(mValorAnt) Then mValorAnt = 0
                If CDbl(mValorNew) <> CDbl(mValorAnt) Then
                    .Text = FormatNumber(mValorNew, Std_Decm)
                    RecalcularTransaccion
                End If
                If .Row = .Rows - 1 Then
                    .Rows = .Rows + 1
                End If
            End If
        End If
    End With
End Sub

Private Sub CostosDirectos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If CostosDirectos.Rows = 2 And CostosDirectos.Row = 1 Then
            CostosDirectos.Rows = CostosDirectos.Rows + 1
            CostosDirectos.RemoveItem CostosDirectos.Row
        Else
            CostosDirectos.RemoveItem CostosDirectos.Row
        End If
        RecalcularTransaccion
    End If
End Sub

Private Sub Form_Load()
    
    Call AjustarPantalla(Me)
    
    BarraO.Buttons(1).Caption = Stellar_Mensaje(102) 'buscar
    
    BarraO.Buttons(1).ButtonMenus(1).Text = Stellar_Mensaje(5014) 'Orden de Producci�n
    
    BarraO.Buttons(1).ButtonMenus(2).Text = Stellar_Mensaje(2955)
    BarraO.Buttons(1).ButtonMenus(2).Visible = True
    BarraO.Buttons(1).ButtonMenus(3).Text = Stellar_Mensaje(5002)
    
    BarraO.Buttons(3).Caption = Stellar_Mensaje(103) 'grabar
    BarraO.Buttons(3).ButtonMenus(1).Text = Stellar_Mensaje(5016) 'Grabar Orden de Producci�n
    
    BarraO.Buttons(4).Caption = Stellar_Mensaje(104) 'anular
    
    BarraO.Buttons(4).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(1).Text

    BarraO.Buttons(5).Caption = Stellar_Mensaje(105) 'cancelar
    BarraO.Buttons(6).Caption = Stellar_Mensaje(106) 'reimprimir
    
    BarraO.Buttons(6).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(1).Text
    BarraO.Buttons(6).ButtonMenus(2).Text = Stellar_Mensaje(2955)
    BarraO.Buttons(6).ButtonMenus(2).Visible = True
    
    BarraO.Buttons(8).Caption = Stellar_Mensaje(107) 'salir
    BarraO.Buttons(9).Caption = Stellar_Mensaje(108) 'opciones
    BarraO.Buttons(9).ButtonMenus(1).Text = Stellar_Mensaje(7) 'Ayuda
    BarraO.Buttons(9).ButtonMenus(2).Text = Stellar_Mensaje(120) 'Cambiar Moneda
    
    lbl_concepto.Caption = Stellar_Mensaje(5021) 'orden de produccion n�
    
    Label5.Caption = Stellar_Mensaje(128) 'fecha
    
    lblDescFormula.Caption = Stellar_Mensaje(143) 'descripcion
    lblTotalCostosDir.Caption = Stellar_Mensaje(5004) 'costos directos
    lblSucursal.Caption = StellarMensaje(12) 'Sucursal
    
    lblProductosUtilizar.Caption = Stellar_Mensaje(5006) 'productos a utilizar
    lblCostosDir.Caption = lblTotalCostosDir.Caption
    
    lbl_Organizacion.Caption = FrmAppLink.GetLblOrganizacion
    
    ManejaMermaExplicita = FrmAppLink.PRDManejaMermaExplicita
    FormaDeTrabajoRes = FrmAppLink.PRDAvanzadaResultadosFormaDeTrabajo
    
    lbl_fecha.Caption = SDate(Now)
    
    ObservacionInicial = StellarMensaje(5014) ' ORDEN DE PRODUCCION
    lblLineaProduccion.Visible = True
    CboLnP.Visible = True
    lblTurno.Visible = True
    CboTurno.Visible = True
    
    mClsGrupos.cTipoGrupo = "LNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
    Tecla_Pulsada = True
    ListRemoveItems CboLnP, "Ninguno"
    Tecla_Pulsada = False
    
    mClsGrupos.cTipoGrupo = "TNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
    Tecla_Pulsada = True
    ListRemoveItems CboTurno, "Ninguno"
    Tecla_Pulsada = False
    
    ' Carga de Reglas Comerciales para el c�lculo del costo de producci�n.
    
    Ent.RsReglasComerciales.Requery
    
    Select Case Ent.RsReglasComerciales!Estimacion_Inv
        Case 0
            CostoActivo = "n_CostoAct"
        Case 1
            CostoActivo = "n_CostoAnt"
        Case 2
            CostoActivo = "n_CostoPro"
        Case 3
            CostoActivo = "n_CostoRep"
    End Select
    
    If FrmAppLink.PRDCostoActivo <> Empty Then CostoActivo = FrmAppLink.PRDCostoActivo
    
    If Not FrmAppLink.GetTipoUsuario Then
        lblSucursal.Visible = False
        txt_sucursal.Visible = False
        CmdLocalidad.Visible = False
        lblDescLocalidad.Visible = False
    End If
    
    Cancelar
    
    If FrmAppLink.PRDAvanzadaOcultarCantidadFormula Then
        FrameCargarLotes.Left = FrameMultiplicarFormula.Left
        FrameMultiplicarFormula.Visible = False
    End If
    
End Sub

Private Sub Form_Activate()
    Screen.MousePointer = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbAltMask Then
        
        Select Case KeyCode
            
            Case vbKeyB
            
            Case vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
                
            Case vbKeyN
                Call Form_KeyDown(vbKeyF6, 0)
            
            Case vbKeyC
                Call Form_KeyDown(vbKeyF7, 0)
            
            Case vbKeyR
                Call Form_KeyDown(vbKeyF8, 0)
            
            Case vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
                
            Case vbKeyA
            
        End Select
        
    Else
        
        Select Case KeyCode
            Case vbKeyF1
                'AYUDA
            
            Case vbKeyF2
                'BUSCAR
                If txtFormula.Enabled Then
                    SafeFocus txtFormula
                    'Call txtFormula_KeyDown(vbKeyF2, 0)
                End If
            
            Case vbKeyF3
                'AGREGAR
            Case vbKeyF4
                'GRABAR
                BarraO_ButtonMenuClick BarraO.Buttons(3).ButtonMenus("GORD")
            Case vbKeyF5
                'MODIFICAR
            Case vbKeyF6
                'ANULAR
                BarraO_ButtonMenuClick BarraO.Buttons(4).ButtonMenus("AORD")
            Case vbKeyF7
                'CANCELAR
                Call Cancelar
            Case vbKeyF8
                'REIMPRIMIR
                BarraO_ButtonMenuClick BarraO.Buttons(6).ButtonMenus("RORD")
            Case vbKeyF9
                'RESERVADO
            
            Case vbKeyF10
                'RESERVADO
            
            Case vbKeyF12
                'SALIR
                Call Salir
                
        End Select
        
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set FRM_PRODUCCION_ORDEN_MANUAL_1 = Nothing
End Sub

Private Sub FrameCargarLotes_Click()
    lblCargarLotes_Click
End Sub

Private Sub FrameMultiplicarFormula_Click()
    lblMultiplicarFormula_Click
End Sub

Private Sub Ingredientes_DblClick()
    Call Ingredientes_KeyPress(vbKeyReturn)
End Sub

Private Sub Ingredientes_GotFocus()
    If Ingredientes.Col = ColPre.Producto And ModifFlag Then
        oTeclado.Key_Down
        Who_Ami = 1
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub Ingredientes_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
    Else
        Select Case KeyCode
            Case Is = vbKeyF2
                If FlgAdd Or FlgUpd Then
                    Who_Ami = 1
                    BarraO_ButtonMenuClick BarraO.Buttons("Buscar").ButtonMenus("BPRO")
                End If
            Case Is = vbKeyDelete
                Call Ingredientes_KeyPress(vbKeyDelete)
        End Select
    End If
End Sub

Private Sub Ingredientes_KeyPress(KeyAscii As Integer)
    If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                With Ingredientes
                    If .Col = ColPre.Producto Or .Col = ColPre.CantLote Or .Col = ColPre.Canti Or .Col = ColPre.PorcMerma Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtedit.MaxLength = 0
                                
                            Case ColPre.Canti, ColPre.CantLote
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Ingredientes, .Row, ColPre.Canti) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Canti)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                        End Select
                        
                        '   ACA SE COMENTA PARA DEJAR POR FUERA A LA COLUMNA DE MERMA
                        If (.Col = ColPre.Producto And .Row = .Rows - 1) _
                        Or .Col = ColPre.Canti _
                        Or .Col = ColPre.CantLote _
                        Or IIf(ManejaMermaExplicita, .Col = ColPre.PorcMerma, False) Then
                            If .Text <> "" Then
                                Valor_Temp = .Text
                                txtedit.Text = .Text
                            End If
                            txtedit.Left = FrameProceso.Left + .CellLeft + .Left
                            txtedit.Top = FrameProceso.Top + .CellTop + .Top
                            txtedit.Height = .CellHeight
                            txtedit.Width = .CellWidth
                            SeleccionarTexto txtedit
                            txtedit.Visible = True
                            txtedit.Enabled = True
                            SafeFocus txtedit
                            .Enabled = False
                        Else
                            .Enabled = True
                            .Col = ColPre.Producto
                            .Row = .Rows - 1
                            SafeFocus Ingredientes
                        End If
                    End If
                
                End With
                
            Case vbKeyEscape
                With Ingredientes
                    .Enabled = True
                    txtedit.Visible = False
                    If .Enabled And .Visible Then .SetFocus
                End With
                
            Case 48 To 57, 106 To 255, vbKeyDecimal
                With Ingredientes
                    If .Col = ColPre.Producto Or .Col = ColPre.CantLote Or .Col = ColPre.Canti Or .Col = ColPre.PorcMerma Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtedit.MaxLength = 0
                                
                            Case ColPre.Canti, ColPre.CantLote
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Ingredientes, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Ingredientes, .Row, ColPre.Canti) = "" Then
                                    Call SetDefMSGrid(Ingredientes, .Row, ColPre.Canti)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                        End Select
                        
                        Valor_Temp = .Text
                        txtedit.Left = FrameProceso.Left + .CellLeft + .Left
                        txtedit.Top = FrameProceso.Top + .CellTop + .Top
                        txtedit.Height = .CellHeight
                        txtedit.Width = .CellWidth
                        txtedit.Visible = True
                        txtedit.Text = Chr(KeyAscii)
                        txtedit.SelStart = Len(txtedit.Text)
                        txtedit.SelLength = 0
                        SafeFocus txtedit
                        .Enabled = False
                    End If
                    
                End With
                
            Case vbKeyDelete
                With Ingredientes
                    If .Row = 1 And .Rows = 2 Then
                        .Rows = 1
                        .Rows = 2
                        RecalcularTransaccion
                    Else
                        .RemoveItem .Row
                        RecalcularTransaccion
                    End If
                End With
        End Select
    End If
End Sub

Private Sub Ingredientes_LostFocus()
    If Empaques.Enabled Then
        Empaques.Row = Empaques.Rows - 1
        Empaques.Col = ColPre.Producto
    End If
End Sub

Private Sub Ingredientes_SelChange()
    If Ingredientes.Col = ColPre.Producto And ModifFlag Then
        Who_Ami = 1
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

'

Private Sub Empaques_DblClick()
    Call Empaques_KeyPress(vbKeyReturn)
End Sub

Private Sub Empaques_GotFocus()
    If Empaques.Col = ColPre.Producto And ModifFlag Then
        Who_Ami = 2
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

Private Sub Empaques_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
    Else
        Select Case KeyCode
            Case Is = vbKeyF2
                If FlgAdd Or FlgUpd Then
                    Who_Ami = 2
                    BarraO_ButtonMenuClick BarraO.Buttons("Buscar").ButtonMenus("BPRO")
                End If
            Case Is = vbKeyDelete
                Call Empaques_KeyPress(vbKeyDelete)
        End Select
    End If
End Sub

Private Sub Empaques_KeyPress(KeyAscii As Integer)
    If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                With Empaques
                    If .Col = ColPre.Producto Or .Col = ColPre.Canti Or .Col = ColPre.PorcMerma Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtEdit2.MaxLength = 0
                                
                            Case ColPre.Canti
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                                
                            Case ColPre.PorcMerma
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Empaques, .Row, ColPre.Canti) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Canti)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                        End Select
                        
                        '   ACA SE COMENTA PARA DEJAR POR FUERA A LA COLUMNA DE MERMA
                        If (.Col = ColPre.Producto And .Row = .Rows - 1) _
                        Or .Col = ColPre.Canti _
                        Or IIf(ManejaMermaExplicita, .Col = ColPre.PorcMerma, False) Then
                            If .Text <> "" Then
                                Valor_Temp = .Text
                                txtEdit2.Text = .Text
                            End If
                            txtEdit2.Left = .CellLeft + .Left
                            txtEdit2.Top = .CellTop + .Top
                            txtEdit2.Height = .CellHeight
                            txtEdit2.Width = .CellWidth
                            SeleccionarTexto txtEdit2
                            txtEdit2.Visible = True
                            txtEdit2.Enabled = True
                            SafeFocus txtEdit2
                            .Enabled = False
                        Else
                            .Enabled = True
                            .Col = ColPre.Producto
                            .Row = .Rows - 1
                            SafeFocus Empaques
                        End If
                    End If
                
                End With
                
            Case vbKeyEscape
                With Empaques
                    .Enabled = True
                    txtEdit2.Visible = False
                    If .Enabled And .Visible Then .SetFocus
                End With
                
            Case 48 To 57, 106 To 255, vbKeyDecimal
                With Empaques
                    If .Col = ColPre.Producto Or .Col = ColPre.Canti Or .Col = ColPre.PorcMerma Then
                        
                        Select Case .Col
                            Case ColPre.Producto
                                txtEdit2.MaxLength = 0
                                
                            Case ColPre.Canti, ColPre.CantLote
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                            Case ColPre.PorcMerma
                                If MSGridRecover(Empaques, .Row, ColPre.Producto) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Empaques, .Row, ColPre.Canti) = "" Then
                                    Call SetDefMSGrid(Empaques, .Row, ColPre.Canti)
                                    Exit Sub
                                End If
                                txtEdit2.MaxLength = 0
                        End Select
                        
                        Valor_Temp = .Text
                        txtEdit2.Left = .CellLeft + .Left
                        txtEdit2.Top = .CellTop + .Top
                        txtEdit2.Height = .CellHeight
                        txtEdit2.Width = .CellWidth
                        txtEdit2.Visible = True
                        txtEdit2.Text = Chr(KeyAscii)
                        txtEdit2.SelStart = Len(txtEdit2.Text)
                        txtEdit2.SelLength = 0
                        SafeFocus txtEdit2
                        .Enabled = False
                    End If
                    
                End With
                
            Case vbKeyDelete
                With Empaques
                    If .Row = 0 And .Rows = 1 Then
                        .Rows = 0
                        .Rows = 1
                        RecalcularTransaccion
                    Else
                        .RemoveItem .Row
                        RecalcularTransaccion
                    End If
                End With
        End Select
    End If
End Sub

Private Sub Empaques_LostFocus()
    If CostosDirectos.Enabled Then
        CostosDirectos.Row = CostosDirectos.Rows - 1
        CostosDirectos.Col = ColDir.Monto
    End If
End Sub

Private Sub Empaques_SelChange()
    If Empaques.Col = ColPre.Producto And ModifFlag Then
        Who_Ami = 2
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    Else
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
End Sub

'

Private Sub RecalcularTransaccion()
    
    On Error GoTo Error
    
    Ingredientes.Visible = False
    Empaques.Visible = False
    CostosDirectos.Visible = False
    GrdTotales.Visible = False
    
    ' Primero Calcular Detalle
    
    With Ingredientes
        
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.Canti)) Then
                If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    .TextMatrix(I, ColPre.CostoPreXMerma) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.PorcMerma) / 100) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * (1# + (.TextMatrix(I, ColPre.PorcMerma) / 100)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    .TextMatrix(I, ColPre.CostoProd) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCant)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                End If
            End If
        Next
        
    End With
    
    With Empaques
        For I = 0 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.Canti)) Then
                If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    .TextMatrix(I, ColPre.CostoPreXMerma) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.PorcMerma) / 100) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * (1# + (.TextMatrix(I, ColPre.PorcMerma) / 100)), CDbl(.TextMatrix(I, ColPre.LDeci)))
                    .TextMatrix(I, ColPre.CostoProd) = FormatNumber(CDbl(.TextMatrix(I, ColPre.Canti)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                    .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(I, ColPre.NetCant)) * CDbl(.TextMatrix(I, ColPre.CostoPre)), Std_Decm)
                End If
            End If
        Next
    End With
    
    ' Luego Sumar Totales
    
    Dim SumaCostoIngredientes As Double, SumaCostoEmpaques As Double, _
    SumaCostoMateriales As Double, SumaCostosDirectos As Double, SumaGlobal As Double
    
    SumaCostoIngredientes = SumaGrid(Ingredientes, ColPre.CostoProdNeto)
    SumaCostoEmpaques = SumaGrid(Empaques, ColPre.CostoProdNeto)
    SumaCostoMateriales = Round(SumaCostoIngredientes + SumaCostoEmpaques, 8)
    SumaCostosDirectos = SumaGrid(CostosDirectos, ColDir.Monto)
    SumaGlobal = Round(SumaCostoMateriales + SumaCostosDirectos, 8)
    
    txtCantProducto.Tag = (mMultiplicar * MultiplicadorCantibul)
    txtCantProducto.Text = (mMultiplicar)
    
    If CDbl(txtCantProducto.Tag) > 1 Then
        ' Para evitar que el monto unitario quede con mas decimales que los que maneja la moneda.
        SumaGlobal = Round(Round(SumaGlobal / CDbl(txtCantProducto.Tag), Std_Decm) * CDbl(txtCantProducto.Tag), Std_Decm)
    End If
    
    txtCostoUni = FormatNumber(SumaGlobal / CDbl(txtCantProducto.Tag), Std_Decm)
    txtCostosDirUni = FormatNumber(SumaCostosDirectos / CDbl(txtCantProducto.Tag), Std_Decm)
    
    With Ingredientes
        
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.Canti)) Then
                If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    If SumaCostoIngredientes <> 0 Then
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaCostoIngredientes * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(0, 2) & "%"
                    End If
                    If SumaGlobal <> 0 Then
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaGlobal * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
                    End If
                End If
            End If
        Next
        
    End With
    
    With TotalesIngredientes
        I = 0
        .TextMatrix(I, ColPre.Descripci�n) = "TOTAL POR INGREDIENTES"
        .TextMatrix(I, ColPre.CantLote) = FormatNumber(SumaGrid(Ingredientes, ColPre.CantLote), 3)
        .TextMatrix(I, ColPre.PorcLote) = "100%"
        .TextMatrix(I, ColPre.Canti) = FormatNumber(SumaGrid(Ingredientes, ColPre.Canti), 3)
        .TextMatrix(I, ColPre.NetCant) = FormatNumber(SumaGrid(Ingredientes, ColPre.NetCant), 3)
        txtPesoTotalMezcla.Text = .TextMatrix(I, ColPre.NetCant)
        .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(SumaCostoIngredientes, Std_Decm)
        .TextMatrix(I, ColPre.PorcTipoCosto) = "100%"
        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(SumaGrid(Ingredientes, ColPre.PorcCostoGlobal), 2) & "%"
    End With
    
    With Empaques
        For I = 0 To .Rows - 1
            If .TextMatrix(I, ColPre.Producto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColPre.Canti)) Then
                If CDbl(.TextMatrix(I, ColPre.Canti)) > 0 Then
                    If SumaCostoEmpaques <> 0 Then
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaCostoEmpaques * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcTipoCosto) = FormatNumber(0, 2) & "%"
                    End If
                    If SumaGlobal <> 0 Then
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(.TextMatrix(I, ColPre.CostoProdNeto) / SumaGlobal * 100, 2) & "%"
                    Else
                        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
                    End If
                End If
            End If
        Next
    End With
    
    With TotalesEmpaques
        I = 0
        .TextMatrix(I, ColPre.Descripci�n) = "TOTAL POR EMPAQUES"
        .TextMatrix(I, ColPre.CostoProdNeto) = FormatNumber(SumaCostoEmpaques, Std_Decm)
        .TextMatrix(I, ColPre.PorcTipoCosto) = "100%"
        .TextMatrix(I, ColPre.PorcCostoGlobal) = FormatNumber(SumaGrid(Empaques, ColPre.PorcCostoGlobal), 2) & "%"
    End With
    
    With CostosDirectos
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColDir.TipoCosto) <> Empty _
            And IsNumeric(.TextMatrix(I, ColDir.Monto)) Then
                If SumaGlobal <> 0 Then
                    .TextMatrix(I, ColDir.PorcCostoGlobal) = FormatNumber(.TextMatrix(I, ColDir.Monto) / SumaGlobal * 100, 2) & "%"
                Else
                    .TextMatrix(I, ColDir.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
                End If
            End If
        Next
    End With
    
    With GrdTotales
        
        If SumaGlobal <> 0 Then
            
            .TextMatrix(1, ColTot.Monto) = FormatNumber(SumaCostoMateriales, Std_Decm)
            .TextMatrix(1, ColTot.PorcCostoGlobal) = FormatNumber( _
            SumaCostoMateriales / SumaGlobal * 100, 2) & "%"
            
            .TextMatrix(2, ColTot.Monto) = FormatNumber(SumaCostosDirectos, Std_Decm)
            .TextMatrix(2, ColTot.PorcCostoGlobal) = FormatNumber(SumaCostosDirectos / SumaGlobal * 100, 2) & "%"
            
            .TextMatrix(3, ColTot.Monto) = FormatNumber(SumaGlobal, Std_Decm)
            .TextMatrix(3, ColTot.PorcCostoGlobal) = "100%"
            
        Else
            
            .TextMatrix(1, ColTot.Monto) = FormatNumber(SumaCostoMateriales, Std_Decm)
            .TextMatrix(1, ColTot.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
            
            .TextMatrix(2, ColTot.Monto) = FormatNumber(SumaCostosDirectos, Std_Decm)
            .TextMatrix(2, ColTot.PorcCostoGlobal) = FormatNumber(0, 2) & "%"
            
            .TextMatrix(3, ColTot.Monto) = FormatNumber(SumaGlobal, Std_Decm)
            .TextMatrix(3, ColTot.PorcCostoGlobal) = "100%"
            
        End If
        
    End With
    
Finally:
    
    Ingredientes.Visible = True
    Empaques.Visible = True
    CostosDirectos.Visible = FrmAppLink.GetNivelUsuario >= FrmAppLink.PRDNivelVerCostosDirectos And FrmAppLink.PRDNivelVerCostosDirectos > 0
    GrdTotales.Visible = True
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    GoTo Finally
    
End Sub

Private Sub RecalcularSumario()
    txtCostosDir = GrdTotales.TextMatrix(2, ColTot.Monto)
    txtCostoTotal = GrdTotales.TextMatrix(3, ColTot.Monto)
End Sub

Private Sub FrameTabProceso_Click()
    lblProceso_Click
End Sub

Private Sub FrameTabSumario_Click()
    lblSumario_Click
End Sub

Private Sub lblCargarLotes_Click()
    
    If Not IsNumeric(txtMulti) Then
        Mensaje True, "Cantidad inv�lida."
        Exit Sub
    End If
    
    mMultiplicarAnt = CDbl(txtCantProducto)
    mMultiplicar = Fix(Fix(CDbl(txtMulti)) * CDbl(txtCantiLote))
    
    If CDbl(mMultiplicar) > 0 Then
        txtProducir.Tag = Empty
        Buscar_Formula txtFormula.Text
    Else
        mMultiplicar = mMultiplicarAnt
    End If
    
End Sub

Private Sub lblMultiplicarFormula_Click()
    
    If Not IsNumeric(txtMulti) Then
        Mensaje True, "Cantidad inv�lida."
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        Exit Sub
    End If
    
    If CDbl(txtCantiLote) > 0 And CDbl(txtCapacidadMaximaLote) > 0 Then
        mLotes = Round(CDbl(txtMulti) / CDbl(txtCantiLote), 8)
        mRes = Round(mLotes - Fix(mLotes), 8)
        If mRes >= 0.001 Then
            mSugMinLotes = Fix(mLotes)
            mSugMaxLotes = mSugMinLotes + 1
            mSugMin = Fix(mSugMinLotes * CDbl(txtCantiLote))
            mSugMax = Fix(mSugMaxLotes * CDbl(txtCantiLote))
            ActivarMensajeGrande 80
            If Mensaje(False, "Atenci�n, la cantidad que est� asignando es menor o mayor " & _
            "a una cantidad de Batches Completos. Se sugiere colocar " & mSugMinLotes & _
            " o " & mSugMaxLotes & " Batches (" & mSugMin & " o " & mSugMax & ") . Presione Aceptar para continuar si desea omitir " & _
            "esta alerta o Cancelar para aplicar la cantidad sugerida.") Then
                txtMulti = Fix(CDbl(txtMulti))
            Else
                If Mensaje(False, "Presione Aceptar para cargar " & mSugMinLotes & " Batches (" & mSugMin & ")" & _
                " o Cancelar para cargar " & mSugMaxLotes & " Batches (" & mSugMax & ")") Then
                    txtMulti = mSugMin
                Else
                    txtMulti = mSugMax
                End If
            End If
        End If
    Else
        txtMulti = Fix(CDbl(txtMulti))
    End If
    
    If CDbl(txtMulti) > 0 Then
        txtProducir.Tag = Empty
        mMultiplicar = CDbl(txtMulti)
        Buscar_Formula txtFormula.Text
    Else
        txtMulti = 1
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
    End If
    
End Sub

Private Sub lblProceso_Click()
    FrameProceso.Visible = True
    FrameSumario.Visible = False
End Sub

Private Sub lblSumario_Click()
    FrameProceso.Visible = False
    FrameSumario.Visible = True
    RecalcularSumario
End Sub

Private Sub ModifFlag_Click()
    
    If TmpDisableEvent Then
        TmpDisableEvent = False
        Exit Sub
    End If
    
    If ModifFlag Then
        
        'PRDNivelTrabajarSinFormula = 11 ' Debug
        
        If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelTrabajarSinFormula Then
            
            'PRDNivelTrabajarSinFormula = 6 ' Debug
            
            Dim FrmAutorizacion: Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
            
            FrmAutorizacion.mNivel = FrmAppLink.PRDNivelTrabajarSinFormula
            FrmAutorizacion.Titulo = StellarMensaje(2967) '"Introduzca las credenciales " & _
            "de un usuario que autorice a trabajar sin " & _
            "base a una formula."
            
            Set FrmAutorizacion.mConexion = Ent.BDD
            
            FrmAutorizacion.Show vbModal
            
            If FrmAutorizacion.mAceptada Then
                
                Autorizante = FrmAutorizacion.mUsuario
                Set FrmAutorizacion = Nothing
                ' Continuar
                
            Else
                
                Set FrmAutorizacion = Nothing
                TmpDisableEvent = True
                ModifFlag.Value = vbUnchecked
                Exit Sub
                
            End If
            
        End If
        
    End If
    
    If ModifFlag Then
        txtFormula.Enabled = False
        CmdFormula.Enabled = False
        txtFormula.Text = Empty
        CantidadBaseFormula = 0
        txt_descripcion.Enabled = False
        txtProducir.Enabled = True
        txt_sucursal.Enabled = True
        CmdLocalidad.Enabled = True
        Ingredientes.Row = 1
        txtMulti.Visible = False
        FrameMultiplicarFormula.Visible = False
        FrameCargarLotes.Visible = False
        MenuAccess True, False
    Else
        Cancelar
    End If
    
End Sub

Private Sub txt_descripcion_GotFocus()
    txt_descripcion.SelStart = 0
    txt_descripcion.SelLength = Len(txt_descripcion.Text)
End Sub

Private Sub txt_descripcion_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub txt_sucursal_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Tecla_Pulsada = True
            Set Forma = Me
            Set Campo_Txt = txt_sucursal
            Set Campo_Lbl = lblDescLocalidad
            Call MAKE_VIEW("ma_sucursales", "c_Codigo", "c_Descripcion", UCase(Stellar_Mensaje(1254)) _
            , Me, "GENERICO", , Campo_Txt, Campo_Lbl) '"S U C U R S A L E S"
    End Select
End Sub

Private Sub txt_sucursal_LostFocus()
    
    Dim RsSucursal As New ADODB.Recordset
    
    If Trim(txt_sucursal.Text) <> Empty Then
        
        Call Apertura_Recordset(RsSucursal)
        
        RsSucursal.Open "Select * from ma_sucursales " & _
        "where c_Codigo = '" & Trim(txt_sucursal.Text) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsSucursal.EOF Then
            lblDescLocalidad.Caption = RsSucursal!c_Descripcion
        Else
            If txt_sucursal.Visible Then
                Call Mensaje(True, StellarMensaje(16357))
            End If
            Me.lblDescLocalidad.Caption = ""
            txt_sucursal.Text = ""
        End If
        
        RsSucursal.Close
        
    Else
        lblDescLocalidad.Caption = ""
        txt_sucursal.Text = ""
    End If
    
End Sub

Private Sub txtCapacidadMaximaLote_Click()
    mValorAnt = txtCapacidadMaximaLote
    mValorNew = QuickInputRequest("Ingrese el Peso Maximo / Capacidad del Batch.", True, , mValorAnt, "Escriba la Cantidad", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtCapacidadMaximaLote = FormatNumber(Abs(mValorNew), Std_Decm)
        End If
    End If
End Sub

Private Sub txtCostoHoraCargaFabril_Click()
    mValorAnt = txtCostoHoraCargaFabril
    mValorNew = QuickInputRequest("Ingrese el Costo por Hora de Carga Fabril.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtCostoHoraCargaFabril = FormatNumber(Abs(mValorNew), Std_Decm)
        End If
    End If
End Sub

Private Sub txtCostoHoraHombre_Click()
    mValorAnt = txtCostoHoraHombre
    mValorNew = QuickInputRequest("Ingrese el Costo por Hora Hombre.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtCostoHoraHombre = FormatNumber(Abs(mValorNew), Std_Decm)
        End If
    End If
End Sub

Private Sub TxtEdit_GotFocus()
    If Ingredientes.Col = ColPre.Producto And Empaques.TextMatrix(Empaques.Row, ColPre.Producto) = Empty Then
        Who_Ami = 1
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    ElseIf Ingredientes.Col = ColPre.Producto And Empaques.TextMatrix(Empaques.Row, ColPre.Producto) <> Empty Then
        Who_Ami = 1
        txtEdit2.Enabled = False
        txtEdit2.Text = Empty
    End If
End Sub

Private Sub txtEdit_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            'oTeclado.Key_Tab
            Call txtEdit_LostFocus
        Case vbKeyEscape
            Call Ingredientes_KeyPress(vbKeyEscape)
        Case Else
            If Ingredientes.Col = ColPre.PorcMerma Then ' MERMA
                Select Case KeyAscii
                    Case 48 To 57
                    Case 8
                    Case 46
                    Case Else
                        KeyAscii = 0
                End Select
            End If
    End Select
End Sub

Private Sub txtEdit_LostFocus()
    
    Dim Mensajes As String, RsEureka As New ADODB.Recordset, ColAct As Integer, FilAct As Integer, Merma As Double, Monto As Double
    
    On Error GoTo Errores
    
    With Ingredientes
        
        Select Case .Col
            
            Case ColPre.Producto
                
                'Producto
                
                If Tecla_Pulsada = True Then Exit Sub
                
                If Trim(txtedit.Text) <> "" Then
                    
                    txtedit.Text = AlternateCode(Trim(txtedit.Text))
                    
                    If Not (Validar_Repeticiones(Ingredientes, txtedit) _
                    Or Validar_Repeticiones2(Empaques, txtedit)) _
                    And txtedit.Text <> txtProducir.Text Then
                        
                        Set RsProducto = ScanData("SELECT * FROM MA_PRODUCTOS INNER JOIN MA_MONEDAS " & _
                        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
                        "WHERE c_Codigo = '" & Trim(txtedit.Text) & "' ")
                        
                        If Not RsProducto.EOF Then
                            
                            If RsProducto!n_Activo = 1 Then
                                
                                .Text = txtedit.Text
                                
                                .TextMatrix(.Row, ColPre.LDeci) = RsProducto!Cant_Decimales
                                .TextMatrix(.Row, ColPre.Descripci�n) = RsProducto!c_Descri
                                .TextMatrix(.Row, ColPre.Presentaci�n) = isDBNull(RsProducto!c_Presenta, Empty)
                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, 2)
                                .TextMatrix(.Row, ColPre.CostoPre) = FormatNumber((RsProducto.Fields(CostoActivo).Value * (RsProducto!n_Factor / CDbl(MSK_FACTOR.Text))), Std_Decm)
                                .TextMatrix(.Row, ColPre.CostoProd) = .TextMatrix(.Row, ColPre.CostoPre)
                                
                                'If CDbl(txtCantiLote.Text) > 0 Then
                                    '.Col = ColPre.CantLote
                                'Else
                                    .Col = ColPre.Canti
                                'End If
                                
                                oTeclado.Key_Return
                                
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Producto no esta activo.")
                                Mensaje True, StellarMensaje(16101)
                                Tecla_Pulsada = False
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = ColPre.Producto
                        End If
                        
                        RsProducto.Close
                        
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista.")
                        Mensaje True, StellarMensaje(2816)
                        Tecla_Pulsada = False
                        .Col = ColPre.Producto
                        .Text = ""
                    End If
                    
                Else
                    .Col = ColPre.Producto
                End If
                
            Case ColPre.CantLote ' CANT LOTE
                
                .Enabled = True
                
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If CheckCad(txtedit, CDbl(.TextMatrix(.Row, ColPre.LDeci)), , False) Then
                        
                        If Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            ValorAnt = .Text
                            ValorAnt2 = txtCapacidadLote
                            
                            .Text = FormatNumber(txtedit.Text, CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            txtCapacidadLote.Text = FormatNumber(SumaGrid(Ingredientes, .Col), 2)
                            
                            If CDbl(txtCapacidadMaximaLote) <> 0 And CDbl(txtCapacidadLote) > CDbl(txtCapacidadMaximaLote) Then
                                If Not Mensaje(False, "�Al ingresar esta cantidad esta excediendo la capacidad del lote. Est� seguro?") Then
                                    .Text = ValorAnt
                                    txtCapacidadLote = ValorAnt2
                                    Tecla_Pulsada = False
                                    txtedit.Enabled = True
                                    txtedit.Visible = True
                                    SafeFocus txtedit
                                    SeleccionarTexto txtedit
                                    .Enabled = False
                                    Exit Sub
                                End If
                            End If
                            
                            If CDbl(txtPesoEmp.Tag) > 0 Then
                                txtCantiLote.Tag = Round(CDbl(txtCapacidadLote) / CDbl(txtPesoEmp), 8)
                                txtCantiLote.Text = FormatNumber(txtCantiLote.Tag, 0)
                            Else
                                txtCantiLote.Text = FormatNumber(0, 0)
                                txtCantiLote.Tag = 0
                            End If
                            
                            If CDbl(txtCapacidadLote) > 0 And CDbl(txtCantiLote) > 0 Then
                                For I = 1 To .Rows - 1
                                    If .TextMatrix(I, ColPre.Producto) <> Empty _
                                    And IsNumeric(.TextMatrix(I, ColPre.CantLote)) Then
                                        .TextMatrix(I, ColPre.PorcLoteReal) = (.TextMatrix(I, ColPre.CantLote) / CDbl(txtCapacidadLote))
                                        .TextMatrix(I, ColPre.PorcLote) = FormatNumber(.TextMatrix(I, ColPre.CantLote) / CDbl(txtCapacidadLote) * 100, 2) & "%"
                                        If CDbl(txtPesoEmp.Tag) <> 0 Then
                                            .TextMatrix(I, ColPre.Canti) = FormatNumber(.TextMatrix(I, ColPre.PorcLoteReal) * CDbl(txtPesoEmp.Tag), .TextMatrix(I, ColPre.LDeci))
                                        Else
                                            .TextMatrix(I, ColPre.Canti) = FormatNumber(0, .TextMatrix(I, ColPre.LDeci))
                                        End If
                                    End If
                                Next
                                RecalcularTransaccion
                                .Col = ColPre.Canti
                                'oTeclado.Key_Return
                            Else
                                .Col = ColPre.Canti
                            End If
                            
'                        Else
'                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
'                            Tecla_Pulsada = True
'                            'Call Mensaje(True, "Valor no puede ser cero")
'                            Mensaje True, StellarMensaje(2815)
'                            Tecla_Pulsada = False
'                            txtedit.Enabled = True
'                            txtedit.Text = ""
'                            txtedit.Visible = True
'                            SafeFocus txtedit
'                            .Enabled = False
'                            Exit Sub
                            
                        End If
                        
                    Else
                    
                        If txtedit.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            Exit Sub
                        Else
                            .Col = ColPre.Canti
                            .Text = 1
                            oTeclado.Key_Return
                        End If
                        
                    End If
                    
                End If
                
            Case ColPre.Canti 'Cantidad
                
                .Enabled = True
                
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If CheckCad(txtedit, CDbl(.TextMatrix(.Row, ColPre.LDeci)), , False) Then
                        
                        If CDbl(txtedit.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Text = FormatNumber(txtedit.Text, CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProd) = FormatNumber(CDbl(.Text) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            .TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.Text) * (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            Call Apertura_Recordset(RsRecetas)
                            texto_cod = .TextMatrix(.Row, ColPre.Producto)
                            
                            Criterio = "select *, ma_productos." & CostoActivo & " AS Costo from ma_productos " & _
                            "where c_Codigo = '" & texto_cod & "'"
                            
                            RsRecetas.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            Call Apertura_Recordset(rsPmoneda)
                            
                            rsPmoneda.Open "select * from ma_monedas where c_CodMoneda = '" & RsRecetas!c_CodMoneda & "'", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                            
                            If CDbl(.TextMatrix(.Row, ColPre.PorcMerma)) = 0 Then
                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, 2)
                                .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber(0, Std_Decm)
                                If Not ManejaMermaExplicita Then
                                    .Rows = .Rows + 1
                                Else
                                    .Col = ColPre.PorcMerma
                                End If
                                oTeclado.Key_Return
                            Else
                                .Col = ColPre.PorcMerma
                                txtedit = .TextMatrix(.Row, .Col)
                                TmpNoCalcular = True
                                txtEdit_LostFocus
                                TmpNoCalcular = False
                            End If
                            
                            RecalcularTransaccion
                            
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        
                        If txtedit.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            Exit Sub
                        Else
                            .Col = ColPre.Canti
                            .Text = 1
                            oTeclado.Key_Return
                        End If
                        
                    End If
                    
                End If
                
            Case ColPre.PorcMerma 'Merma
                
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If CheckCad(txtedit, 3, , False) Then
                        
                        If CDbl(txtedit.Text) >= 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Enabled = True
                            .Text = FormatNumber(CDbl(txtedit.Text), 2)
                            
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                oTeclado.Key_Down
                            End If
                            
                            .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber((CDbl(txtedit.Text) * CDbl(.TextMatrix(.Row, ColPre.CostoPre))) / 100, Std_Decm)
                            
                            .TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.Canti)) * (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            If Not TmpNoCalcular Then
                                RecalcularTransaccion
                                Call SetDefMSGrid(Ingredientes, .Row + 1, ColPre.Producto)
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor es negativo")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        If txtedit.Text <> "" Then
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                    End If
                    
                End If
            
        End Select
        
    End With
    
    txtedit.Visible = False
    txtedit.Text = ""
    
    If Tecla_Pulsada = False Then
        Ingredientes.Enabled = True
        SafeFocus Ingredientes
    End If
    
    If Ingredientes.Col = ColPre.Producto Then
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Private Sub txtedit2_GotFocus()
    If Empaques.Col = ColPre.Producto And Ingredientes.TextMatrix(Ingredientes.Row, ColPre.Producto) = Empty Then
        Who_Ami = 2
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = True
    ElseIf Empaques.Col = ColPre.Producto And Ingredientes.TextMatrix(Ingredientes.Row, ColPre.Producto) <> Empty Then
        Who_Ami = 2
        txtEdit2.Enabled = False
        txtEdit2.Text = Empty
    End If
End Sub

Private Sub txtedit2_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            Call txtedit2_LostFocus
        Case vbKeyEscape
            Call Empaques_KeyPress(vbKeyEscape)
        Case Else
            If Empaques.Col = ColPre.PorcMerma Then ' MERMA
                Select Case KeyAscii
                    Case 48 To 57
                    Case 8
                    Case 46
                    Case Else
                        KeyAscii = 0
                End Select
            End If
    End Select
End Sub

Private Sub txtedit2_LostFocus()
    
    Dim Mensajes As String, RsEureka As New ADODB.Recordset, ColAct As Integer, FilAct As Integer, Merma As Double, Monto As Double
    
    On Error GoTo Errores
    
    With Empaques
        
        Select Case .Col
            
            Case ColPre.Producto
                
                'Producto
                
                If Tecla_Pulsada = True Then Exit Sub
                
                If Trim(txtEdit2.Text) <> "" Then
                    
                    txtEdit2.Text = AlternateCode(Trim(txtEdit2.Text))
                    
                    If Not (Validar_Repeticiones(Ingredientes, txtEdit2) _
                    Or Validar_Repeticiones2(Empaques, txtEdit2)) _
                    And txtEdit2.Text <> txtProducir.Text Then
                        
                        Set RsProducto = ScanData("SELECT * FROM MA_PRODUCTOS INNER JOIN MA_MONEDAS " & _
                        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
                        "WHERE c_Codigo = '" & Trim(txtEdit2.Text) & "' ")
                        
                        If Not RsProducto.EOF Then
                            
                            If RsProducto!n_Activo = 1 Then
                                
                                .Text = txtEdit2.Text
                                
                                .TextMatrix(.Row, ColPre.LDeci) = RsProducto!Cant_Decimales
                                .TextMatrix(.Row, ColPre.Descripci�n) = RsProducto!c_Descri
                                .TextMatrix(.Row, ColPre.Presentaci�n) = isDBNull(RsProducto!c_Presenta, Empty)
                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, 2)
                                .TextMatrix(.Row, ColPre.CostoPre) = FormatNumber((RsProducto.Fields(CostoActivo).Value * (RsProducto!n_Factor / CDbl(MSK_FACTOR.Text))), Std_Decm)
                                .TextMatrix(.Row, ColPre.CostoProd) = .TextMatrix(.Row, ColPre.CostoPre)
                                
                                .Col = ColPre.Canti
                                
                                oTeclado.Key_Return
                                
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Producto no esta activo.")
                                Mensaje True, StellarMensaje(16101)
                                Tecla_Pulsada = False
                            End If
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = ColPre.Producto
                        End If
                        RsProducto.Close
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista.")
                        Mensaje True, StellarMensaje(2816)
                        Tecla_Pulsada = False
                        .Col = ColPre.Producto
                        .Text = ""
                    End If
                Else
                    .Col = ColPre.Producto
                End If
                
            Case ColPre.Canti 'Cantidad
                
                .Enabled = True
                
                If Trim(txtEdit2.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If CheckCad(txtEdit2, CDbl(.TextMatrix(.Row, ColPre.LDeci)), , False) Then
                        
                        If CDbl(txtEdit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Text = FormatNumber(txtEdit2.Text, CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProd) = FormatNumber(CDbl(.Text) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            .TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.Text) * (1# + (.TextMatrix(.Row, ColPre.PorcMerma) / 100)), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            Call Apertura_Recordset(RsRecetas)
                            texto_cod = .TextMatrix(.Row, ColPre.Producto)
                            
                            Criterio = "select *, ma_productos." & CostoActivo & " AS Costo from ma_productos " & _
                            "where c_Codigo = '" & texto_cod & "'"
                            
                            RsRecetas.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            Call Apertura_Recordset(rsPmoneda)
                            
                            rsPmoneda.Open "select * from ma_monedas where c_CodMoneda = '" & RsRecetas!c_CodMoneda & "'", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                            
                            If CDbl(.TextMatrix(.Row, ColPre.PorcMerma)) = 0 Then
                                .TextMatrix(.Row, ColPre.PorcMerma) = FormatNumber(0, 2)
                                .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber(0, Std_Decm)
                                If Not ManejaMermaExplicita Then
                                    .Rows = .Rows + 1
                                Else
                                    .Col = ColPre.PorcMerma
                                End If
                                oTeclado.Key_Return
                            Else
                                .Col = ColPre.PorcMerma
                                txtEdit2 = .TextMatrix(.Row, .Col)
                                TmpNoCalcular = True
                                txtedit2_LostFocus
                                TmpNoCalcular = False
                            End If
                            
                            RecalcularTransaccion
                            
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                    Else
                        If txtEdit2.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            Exit Sub
                        Else
                            .Col = ColPre.Canti
                            .Text = 1
                            oTeclado.Key_Return
                        End If
                    End If
                    
                End If
                
            Case ColPre.PorcMerma 'Merma
                
                If Trim(txtEdit2.Text) <> "" And Tecla_Pulsada = False Then
                    
                    If CheckCad(txtEdit2, 3, , False) Then
                        
                        If CDbl(txtEdit2.Text) >= 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            .Enabled = True
                            .Text = FormatNumber(CDbl(txtEdit2.Text), 2)
                            
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                'oTeclado.Key_Down
                            End If
                            
                            .TextMatrix(.Row, ColPre.CostoPreXMerma) = FormatNumber((CDbl(txtEdit2.Text) * CDbl(.TextMatrix(.Row, ColPre.CostoPre))) / 100, Std_Decm)
                            
                            .TextMatrix(.Row, ColPre.NetCant) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.Canti)) * (1# + .TextMatrix(.Row, ColPre.PorcMerma) / 100), CDbl(.TextMatrix(.Row, ColPre.LDeci)))
                            
                            .TextMatrix(.Row, ColPre.CostoProdNeto) = FormatNumber(CDbl(.TextMatrix(.Row, ColPre.NetCant)) * CDbl(.TextMatrix(.Row, ColPre.CostoPre)), Std_Decm)
                            
                            If Not TmpNoCalcular Then
                                RecalcularTransaccion
                                Call SetDefMSGrid(Empaques, .Rows - 1, ColPre.Producto)
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor es negativo")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        If txtEdit2.Text <> "" Then
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                    End If
                    
                End If

        End Select
        
    End With
    
    txtEdit2.Visible = False
    txtEdit2.Text = ""
    
    If Tecla_Pulsada = False Then
        Empaques.Enabled = True
        SafeFocus Empaques
    End If
    
    If Empaques.Col = ColPre.Producto Then
        BarraO.Buttons("Buscar").ButtonMenus("BPRO").Enabled = False
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Sub Salir()
    Set FRM_PRODUCCION_ORDEN_MANUAL_1 = Nothing
    Unload Me
End Sub

Sub Cancelar( _
Optional ByVal MantenerFormula As Boolean = False _
)
    
    Call Apertura_RecordsetC(RsProducto)
    Call Apertura_RecordsetC(RsRecetas)
    Call Apertura_RecordsetC(rsPmoneda)
    Call Apertura_RecordsetC(RsInventario)
    Call Apertura_RecordsetC(RsCodigos)
    Call Apertura_RecordsetC(RsEureka)
    
    FlgUpd = False
    FlgAdd = False
    ModifFlag.Value = vbUnchecked
    
    txtFormula.Enabled = True
    CmdFormula.Enabled = True
    txtFormula.Text = Empty
    lblFormula = Empty
    
    txtProducir.Enabled = False
    txtProducir.Text = Empty
    txtProducir.Tag = Empty
    txt_descripcion.Enabled = False
    txt_descripcion.Text = Empty
    txt_sucursal.Enabled = True
    CmdLocalidad.Enabled = True
    
    txtMulti.Visible = True
    FrameMultiplicarFormula.Visible = True
    FrameCargarLotes.Visible = True
    
    CargarGrid
    
    lbl_fecha.Caption = Format(Now, "short date")
    
    ModifFlag.Value = vbUnchecked
    
    GridLleno = False
    
    txtedit.Visible = False
    txtedit.Text = Empty
    txtEdit2.Visible = False
    txtEdit2.Text = Empty
    
    txt_sucursal.Text = FrmAppLink.GetCodLocalidadSistema
    txt_sucursal_LostFocus
    
    txtCantProducto.Text = FormatNumber(1, 2)
    lblNomenclaturaCant.Caption = Empty
    txtPesoUni.Text = FormatNumber(0, 2)
    txtCantibul.Text = FormatNumber(1, 2)
    txtPesoEmp.Text = FormatNumber(0, 2)
    txtPesoEmp.Tag = 0
    txtCapacidadMaximaLote.Text = FormatNumber(0, 2)
    txtCapacidadLote.Text = FormatNumber(0, 2)
    txtCantiLote.Text = FormatNumber(0, 0)
    txtCantiLote.Tag = txtCantiLote.Text
    
    txtCostoTotal.Text = FormatNumber(0, Std_Decm)
    txtCostosDir.Text = FormatNumber(0, Std_Decm)
    txtCostoHoraHombre.Text = FormatNumber(0, Std_Decm)
    txtCostoHoraCargaFabril.Text = FormatNumber(0, Std_Decm)
    txtHorasHombreXUnidad.Text = FormatNumber(0, 2)
    
    Call Nuevo_Consecutivo
    Call Ini_Moneda
    
End Sub

Sub Nuevo_Consecutivo()
    lbl_consecutivo.Caption = Format(No_Consecutivo("Orden_Produccion_Nuevo", False), "000000000")
End Sub

Sub CargarCostosDirectos()
    
    If FrmAppLink.PRDAvanzadaPrecargarCostosDirectos Then
        
        With CostosDirectos
            
            Dim mRsCD As ADODB.Recordset
            
            Set mRsCD = Ent.BDD.Execute("SELECT DISTINCT cs_Grupo FROM MA_AUX_GRUPO WHERE cs_Tipo = 'PCD'")
            
            .Rows = 1
            .Rows = 2
            .Row = 1
            
            I = 0
            
            While Not mRsCD.EOF
                I = I + 1
                .TextMatrix(I, ColDir.TipoCosto) = mRsCD!cs_Grupo
                .TextMatrix(I, ColDir.Monto) = FormatNumber(0, Std_Decm)
                .Rows = .Rows + 1
                mRsCD.MoveNext
            Wend
            
            .Row = .Rows - 1
            .Col = ColDir.Monto
            
            mRsCD.Close
            
        End With
        
    End If
    
End Sub

Sub Cambiar_Moneda()
    
    Dim MActual As String, MActDec As Integer, mDesc As String, MSimbolo As String, mFACTOR As Double
    Dim RsMoneda As New ADODB.Recordset
    
    MActual = dbmoneda.Text
    mDesc = lbl_moneda.Caption
    MActDec = Std_Decm
    MSimbolo = lbl_simbolo.Caption
    mFACTOR = CDbl(MSK_FACTOR.Text)
    
    Call Consulta_F2(Me, "MONEDA_GENERICA", "MONEDAS")
    CodMoneda = FrmAppLink.GetCodMonedaSel
    
    If CodMoneda <> "" Then
        If Buscar_Moneda(dbmoneda, lbl_moneda, MSK_FACTOR, False, CodMoneda, True, True) = False Then
            'Call Mensaje(True, "No existe una moneda predeterminada en el sistema.")
            Mensaje True, StellarMensaje(16289)
            Unload Me
            Exit Sub
        End If
    End If
    
    If MActual <> dbmoneda.Text Then
        'MsgBox "Cambio la moneda, recuerde colocar el cambio de moneda."
        '
        'ESCRIBIR PROCEDIMIENTOS DE RECALCULAR GRIDS
        '
        Call Apertura_RecordsetC(RsMoneda)
        RsMoneda.Open "select * from ma_monedas where c_CodMoneda = '" & dbmoneda & "' ", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsMoneda.EOF Then
            lbl_simbolo.Caption = RsMoneda!c_Simbolo
        Else
            lbl_simbolo.Caption = "Unk"
        End If
        Std_Decm = RsMoneda!n_Decimales
        RsMoneda.Close
        
    End If
    
End Sub

Sub Ini_Moneda()
    Dim RsMoneda As New ADODB.Recordset
    RsMoneda.Open "select * from ma_monedas where b_Activa = 1 AND b_Preferencia = 1", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsMoneda.EOF Then
        dbmoneda.Text = RsMoneda!c_CodMoneda
        lbl_moneda.Caption = RsMoneda!c_Descripcion
        MSK_FACTOR.Text = RsMoneda!n_Factor
        Std_Decm = RsMoneda!n_Decimales
        lbl_simbolo.Caption = RsMoneda!c_Simbolo
        CodMoneda = dbmoneda.Text
    Else
        If Me.Visible Then Unload Me
    End If
End Sub

Function Validar_Repeticiones(Grid As MSFlexGrid, Txt As TextBox) As Boolean
    'Dim FilAct As Integer, ColAct As Integer
    Validar_Repeticiones = False
    With Grid
        'ColAct = Grid.Col
        'FilAct = Grid.Row
        .Enabled = True
        For Cont = 1 To Grid.Rows - 1
            If Grid.TextMatrix(Cont, ColPre.Producto) = Txt.Text Then
                .Enabled = False
                Validar_Repeticiones = True
                Exit Function
            End If
        Next Cont
    End With
End Function

Function Validar_Repeticiones2(Grid As MSFlexGrid, Txt As TextBox) As Boolean
    'Dim FilAct As Integer, ColAct As Integer
    Validar_Repeticiones2 = False
    With Grid
        'ColAct = Grid.Col
        'FilAct = Grid.Row
        .Enabled = True
        For Cont = 0 To Grid.Rows - 1
            If Grid.TextMatrix(Cont, ColPre.Producto) = Txt.Text Then
                .Enabled = False
                Validar_Repeticiones2 = True
                Exit Function
            End If
        Next Cont
    End With
End Function

Function AlternateCode(Codigo As Variant) As Variant
    Dim RsCodigos As New ADODB.Recordset
    Call Apertura_RecordsetC(RsCodigos)
    RsCodigos.Open "select * from ma_codigos where c_Codigo = '" & Codigo & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsCodigos.EOF Then
        AlternateCode = RsCodigos!c_codnasa
    Else
        AlternateCode = Codigo
    End If
    RsCodigos.Close
End Function

Function ScanData(SQLI As String) As ADODB.Recordset
    Dim RsTemp As New ADODB.Recordset
    Call Apertura_RecordsetC(RsTemp)
    RsTemp.Open SQLI, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsTemp.EOF Then
        Set ScanData = RsTemp
    Else
        Set ScanData = RsTemp
    End If
End Function

Function SumaGrid(Grid As MSFlexGrid, Columna As Integer) As Double
    Dim total As Double, Cont As Integer
    total = 0
    For Cont = Grid.FixedRows To Grid.Rows - 1
        If IsNumeric(Replace(Grid.TextMatrix(Cont, Columna), "%", Empty)) Then
            total = total + CDbl(Replace(Grid.TextMatrix(Cont, Columna), "%", Empty))
        End If
    Next Cont
    SumaGrid = Round(total, 8)
End Function

Sub Buscar_Formula(Formula As String)
    
    'BUSCAR PRODUCCION
    Dim RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset, _
    RsCostoDir As New ADODB.Recordset, RsProducir As New ADODB.Recordset, rsMonedas As New ADODB.Recordset
    
    Call Cancelar
    Call Apertura_RecordsetC(RsMaProduccion)
    
    RsMaProduccion.Open "SELECT * FROM MA_PRODUCCION WHERE C_FORMULA = '" & Formula & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    GridLleno = False
    If mMultiplicar <= 0 Then mMultiplicar = 1
    
    If Not RsMaProduccion.EOF Then
        
        lbl_consecutivo.Caption = RsMaProduccion!c_Formula
        
        If RsMaProduccion!TipoFormula <> 1 Then
            Mensaje True, "El tipo de f�rmula es incompatible con el modulo de producci�n avanzada."
            Cancelar
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            Exit Sub
        End If
        
        'BUSCAR DETALLES Y CARGAR
        
        'A PRODUCIR
        Call Apertura_RecordsetC(RsProducir)
        RsProducir.Open "SELECT TOP 1 * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = '1'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsProducir.EOF Then
            
            'For Cont = 1 To Producir.Rows - 2
                
                If RsProducir!n_Activo = 1 Then
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsProducir!c_CodMoneda
                    
                    txtProducir.Text = RsProducir!c_Codigo
                    txtProducir_LostFocus
                    
                Else
                    'Call Mensaje(True, "El producto '" & RsProducir!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
                    Exit Sub
                End If
                
            'Next Cont
        
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            Exit Sub
        End If
        
        txtFormula.Text = Formula
        txt_descripcion.Text = RsMaProduccion!c_Descripcion
        
        'MONEDA
        Call Apertura_Recordset(rsMonedas)
        rsMonedas.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda = '" & RsMaProduccion!c_CodMoneda & "' ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not rsMonedas.EOF Then
            lbl_moneda.Caption = rsMonedas!c_Descripcion
            dbmoneda.Text = rsMonedas!c_CodMoneda
            Std_Decm = rsMonedas!n_Decimales
            MSK_FACTOR.Text = FormatNumber(rsMonedas!n_Factor, Std_Decm)
            lbl_simbolo.Caption = rsMonedas!c_Simbolo
        Else
            'Call Mensaje(True, "No se encontr� la moneda de la f�rmula.")
            Mensaje True, StellarMensaje(2806)
            Call Cancelar
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            Exit Sub
        End If
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = 0 AND TipoItem = 1", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With Ingredientes
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 2
                
                For Cont = 1 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(RsTrProduccion!CantidadEnLote, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = RsTrProduccion!PorcVsLote
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(RsTrProduccion!PorcVsLote * 100, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(RsTrProduccion!n_Cantidad * mMultiplicar, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_Merma, 2)
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
                        Exit Sub
                    End If
                Next Cont
                
            Else
                'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                Mensaje True, StellarMensaje(359)
                Call Cancelar
                If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
                Exit Sub
            End If
        End With
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION LEFT JOIN MA_PRODUCTOS ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = 0 AND TipoItem = 2", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With Empaques
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 1
                
                For Cont = 0 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = 0
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(0, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(RsTrProduccion!n_Cantidad * mMultiplicar, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_Merma, 2)
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
                        Exit Sub
                    End If
                Next Cont
                
            Else
                'Estos no deben ser obligatorios. Es opcional manejar empaques.
                ''Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                'Mensaje True, StellarMensaje(359)
                'Call Cancelar
                'If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
                'Exit Sub
            End If
            
        End With
        
        CargarCostosDirectos
        
        ' COSTOS DIRECTOS
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM MA_PRODUCCION_COSTOS_DIRECTOS WHERE CodFormula = '" & Formula & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With CostosDirectos
            
            I = 0
            
            Do While Not RsTrProduccion.EOF
                
                If PRDAvanzadaPrecargarCostosDirectos Then
                    
                    For I = 0 To .Rows - 1
                        If UCase(RsTrProduccion!TipoCosto) = UCase(.TextMatrix(I, ColDir.TipoCosto)) Then
                            .TextMatrix(I, ColDir.Monto) = FormatNumber(RsTrProduccion!Monto * mMultiplicar, Std_Decm)
                            Exit For
                        End If
                    Next
                    
                Else
                    
                    I = I + 1
                    .TextMatrix(I, ColDir.TipoCosto) = RsTrProduccion!TipoCosto
                    .TextMatrix(I, ColDir.Monto) = FormatNumber(RsTrProduccion!Monto * mMultiplicar, Std_Decm)
                    .Rows = .Rows + 1
                    
                End If
                
                RsTrProduccion.MoveNext
                
            Loop
            
        End With
        
        GridLleno = True
        
        RecalcularTransaccion ' Primero calcular las columnas faltantes
        RecalcularTransaccion ' Luego calcular totales
        lblSumario_Click
        
        txtCapacidadMaximaLote = FormatNumber(RsMaProduccion!CapacidadMaxLote, 3)
        txtCapacidadLote.Text = FormatNumber(SumaGrid(Ingredientes, ColPre.CantLote), 2)
        
        If CDbl(txtPesoEmp.Tag) > 0 Then
            txtCantiLote.Tag = Round(CDbl(txtCapacidadLote) / CDbl(txtPesoEmp), 8)
            txtCantiLote.Text = FormatNumber(txtCantiLote.Tag, 0)
        Else
            txtCantiLote.Text = FormatNumber(0, 0)
            txtCantiLote.Tag = 0
        End If
        
        CantidadBaseFormula = RsMaProduccion!CantidadBaseFormula
        txtCantProducto = FormatNumber(mMultiplicar, 0)
        
        txtCostoHoraHombre = FormatNumber(RsMaProduccion!CostoHoraHombre, Std_Decm)
        txtCostoHoraCargaFabril = FormatNumber(RsMaProduccion!CostoHoraCargaFabril, Std_Decm)
        txtHorasHombreXUnidad = FormatNumber(RsMaProduccion!HorasHombrePorUnidad, Std_Decm)
        
        RsTrProduccion.Close
        RsProducir.Close
        rsMonedas.Close
        
        Ingredientes.Row = Ingredientes.Rows - 1
        Ingredientes.Col = ColPre.Producto
        Empaques.Row = Empaques.Rows - 1
        Empaques.Col = ColPre.Producto
        CostosDirectos.Row = CostosDirectos.Rows - 1
        CostosDirectos.Col = ColDir.TipoCosto
        MenuAccess False, False
        
        If mOrdenAutomatica Then
            If mOrdenAutomatica_LocalidadAlterna <> Empty Then
                txt_sucursal.Text = RsMaProduccion!c_CodLocalidad
            Else
                txt_sucursal.Text = RsMaProduccion!c_CodLocalidad
            End If
        Else
            txt_sucursal.Text = RsMaProduccion!c_CodLocalidad
        End If
        
        txt_sucursal_LostFocus
        
        If Not mOrdenAutomatica Then
            SafeFocus txtMulti
            'txtMulti_Click
        End If
        
    Else
        'Call Mensaje(True, "No se encontr� la Formula N� " & Formula)
        Mensaje True, Replace(StellarMensaje(2808), "$(Code)", Formula)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
    End If
    
    RsMaProduccion.Close
    
End Sub

Sub Buscar_Orden_Produccion(ByVal tipo As String, _
ByVal orden As String, ByVal CodLocalidad As String)
    
    'BUSCAR PRODUCCION
    Dim RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset, _
    RsCostoDir As New ADODB.Recordset, RsProducir As New ADODB.Recordset, rsMonedas As New ADODB.Recordset
    
    Call Cancelar
    Call Apertura_RecordsetC(RsMaProduccion)
    
    RsMaProduccion.Open "SELECT * FROM MA_ORDEN_PRODUCCION " & _
    "WHERE c_Documento = '" & orden & "' " & _
    "AND c_CodLocalidad = '" & CodLocalidad & "' " & _
    "AND c_Status = '" & FrmAppLink.GetFindStatus & "' ", _
    Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    GridLleno = False
    mMultiplicar = 1
    
    If Not RsMaProduccion.EOF Then
        
        If RsMaProduccion!TipoOrdenProduccion <> 1 Then
            Mensaje True, "El tipo de orden de producci�n es incompatible con el modulo de producci�n avanzada."
            Cancelar
            Exit Sub
        End If
        
        'BUSCAR DETALLES Y CARGAR
        
        'A PRODUCIR
        Call Apertura_RecordsetC(RsProducir)
        RsProducir.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
        "LEFT JOIN MA_MONEDAS " & _
        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
        "WHERE c_Documento = '" & orden & "' AND b_Producir = 1 ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsProducir.EOF Then
            
            'For Cont = 1 To Producir.Rows - 2
                
                If RsProducir!n_Activo = 1 Then
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsProducir!c_CodMoneda
                    
                    txtProducir.Text = RsProducir!c_Codigo
                    txtProducir_LostFocus
                    
                    txtCantProducto.Tag = RsProducir!n_Cantidad
                    txtCantProducto.Text = FormatNumber(RsProducir!n_Cantidad, 0)
                    mMultiplicar = CDbl(txtCantProducto.Text)
                    
                Else
                    'Call Mensaje(True, "El producto '" & RsProducir!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    Exit Sub
                End If
                
            'Next Cont
        
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
        
        txtFormula.Text = RsMaProduccion!c_Formula
        txt_descripcion.Text = RsMaProduccion!c_Descripcion
        
        'MONEDA
        Call Apertura_Recordset(rsMonedas)
        rsMonedas.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda = '" & RsMaProduccion!c_CodMoneda & "' ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not rsMonedas.EOF Then
            lbl_moneda.Caption = rsMonedas!c_Descripcion
            dbmoneda.Text = rsMonedas!c_CodMoneda
            Std_Decm = rsMonedas!n_Decimales
            MSK_FACTOR.Text = FormatNumber(rsMonedas!n_Factor, Std_Decm)
            lbl_simbolo.Caption = rsMonedas!c_Simbolo
        Else
            'Call Mensaje(True, "No se encontr� la moneda de la f�rmula.")
            Mensaje True, StellarMensaje(2806)
            Call Cancelar
            Exit Sub
        End If
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open _
        "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
        "WHERE c_Documento = '" & orden & "' AND b_Producir = 0 AND TipoItem = 1", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With Ingredientes
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 2
                
                For Cont = 1 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(RsTrProduccion!CantidadEnLote, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = RsTrProduccion!PorcVsLote
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(RsTrProduccion!PorcVsLote * 100, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(RsTrProduccion!n_Cantidad, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_PorcMerma, 2)
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Exit Sub
                    End If
                Next Cont
                
            Else
                'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                Mensaje True, StellarMensaje(359)
                Call Cancelar
                Exit Sub
            End If
        End With
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open _
        "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
        "WHERE c_Documento = '" & orden & "' AND b_Producir = 0 AND TipoItem = 2", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With Empaques
            
            If Not RsTrProduccion.EOF Then
                
                .Rows = RsTrProduccion.RecordCount + 1
                
                For Cont = 0 To .Rows - 2
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        .TextMatrix(Cont, ColPre.LDeci) = RsTrProduccion!Cant_Decimales
                        .TextMatrix(Cont, ColPre.Producto) = RsTrProduccion!c_Codigo
                        .TextMatrix(Cont, ColPre.Descripci�n) = RsTrProduccion!c_Descri
                        .TextMatrix(Cont, ColPre.CantLote) = FormatNumber(0, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcLoteReal) = 0
                        .TextMatrix(Cont, ColPre.PorcLote) = FormatNumber(0, 2) & "%"
                        .TextMatrix(Cont, ColPre.Presentaci�n) = RsTrProduccion!c_Presenta
                        .TextMatrix(Cont, ColPre.Canti) = FormatNumber(RsTrProduccion!n_Cantidad, RsTrProduccion!Cant_Decimales)
                        .TextMatrix(Cont, ColPre.PorcMerma) = FormatNumber(RsTrProduccion!n_PorcMerma, 2)
                        .TextMatrix(Cont, ColPre.CostoPre) = FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm)
                        .TextMatrix(Cont, ColPre.nCantibul) = RsTrProduccion!n_Cantibul
                        
                        RsTrProduccion.MoveNext
                        
                    Else
                        'Call Mensaje(True, "El producto '" & RsTrProduccion!c_Codigo & "' al que hace referencia esta f�rmula est� inactivo, la f�rmula '" & Formula & "' no se puede actualizar.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Exit Sub
                    End If
                Next Cont
                
            Else
                
                'Estos no deben ser obligatorios. Es opcional manejar empaques.
                ''Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                'Mensaje True, StellarMensaje(359)
                'Call Cancelar
                'Exit Sub
                
            End If
            
        End With
        
        CargarCostosDirectos
        
        ' COSTOS DIRECTOS
        Call Apertura_RecordsetC(RsTrProduccion)
        RsTrProduccion.Open "SELECT * FROM MA_ORDEN_PRODUCCION_COSTOS_DIRECTOS WHERE CodOrden = '" & orden & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        With CostosDirectos
            
            I = 0
            
            Do While Not RsTrProduccion.EOF
                
                If FrmAppLink.PRDAvanzadaPrecargarCostosDirectos Then
                    
                    For I = 0 To .Rows - 1
                        If UCase(RsTrProduccion!TipoCosto) = UCase(.TextMatrix(I, ColDir.TipoCosto)) Then
                            .TextMatrix(I, ColDir.Monto) = FormatNumber(RsTrProduccion!Monto, Std_Decm)
                            Exit For
                        End If
                    Next
                        
                Else
                    
                    I = I + 1
                    .TextMatrix(I, ColDir.TipoCosto) = RsTrProduccion!TipoCosto
                    .TextMatrix(I, ColDir.Monto) = FormatNumber(RsTrProduccion!Monto, Std_Decm)
                    .Rows = .Rows + 1
                    
                End If
                
                RsTrProduccion.MoveNext
                
            Loop
            
        End With
        
        GridLleno = True
        
        RecalcularTransaccion ' Primero calcular las columnas faltantes
        RecalcularTransaccion ' Luego calcular totales
        lblSumario_Click
        
        txtCapacidadMaximaLote = FormatNumber(RsMaProduccion!CapacidadMaxLote, 3)
        txtCapacidadLote.Text = FormatNumber(SumaGrid(Ingredientes, ColPre.CantLote), 2)
        
        If CDbl(txtPesoEmp.Tag) > 0 Then
            txtCantiLote.Tag = Round(CDbl(txtCapacidadLote) / CDbl(txtPesoEmp), 8)
            txtCantiLote.Text = FormatNumber(txtCantiLote.Tag, 0)
        Else
            txtCantiLote.Text = FormatNumber(0, 0)
            txtCantiLote.Tag = 0
        End If
        
        txtCostoHoraHombre = FormatNumber(RsMaProduccion!CostoHoraHombre, Std_Decm)
        txtCostoHoraCargaFabril = FormatNumber(RsMaProduccion!CostoHoraCargaFabril, Std_Decm)
        txtHorasHombreXUnidad = FormatNumber(RsMaProduccion!HorasHombrePorUnidad, Std_Decm)
        
        txtPesoTotalProductos.Text = FormatNumber(RsMaProduccion!PesoTotal, 3)
        txtPesoTotalMezcla.Text = FormatNumber(RsMaProduccion!PesoTotalNeto, 3)
        
        RsTrProduccion.Close
        RsProducir.Close
        rsMonedas.Close
        
        Ingredientes.Row = Ingredientes.Rows - 1
        Ingredientes.Col = ColPre.Producto
        Empaques.Row = Empaques.Rows - 1
        Empaques.Col = ColPre.Producto
        CostosDirectos.Row = CostosDirectos.Rows - 1
        CostosDirectos.Col = ColDir.Monto
        MenuAccess False, False
        
        txtMulti.Visible = False
        FrameCargarLotes.Visible = False
        FrameMultiplicarFormula.Visible = False
        
        txt_sucursal.Text = RsMaProduccion!c_CodLocalidad
        txt_sucursal_LostFocus
        
    Else
        'Call Mensaje(True, "No se encontr� la Formula N� " & Formula)
        Mensaje True, Replace(StellarMensaje(2808), "$(Code)", Formula)
    End If
    
    RsMaProduccion.Close
    
End Sub

Sub MenuAccess(EnblGRD As Boolean, EnblTXT As Boolean)
    txtedit.Visible = EnblTXT
    txtedit.Text = Empty
    txtEdit2.Visible = EnblTXT
    txtEdit2.Text = Empty
    Ingredientes.Enabled = EnblGRD
    Empaques.Enabled = EnblGRD
    CostosDirectos.Enabled = EnblGRD
End Sub

Private Sub txtFormula_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            If Trim(txtFormula.Text) <> Empty Then
                Buscar_Formula txtFormula.Text
                
                Who_Ami = -1
                
                DocumentoRelacion = txtFormula.Text
                ObservacionInicial = StellarMensaje(2948)
                esBackOrder = False
                FechaEstimadaPRD = Date
                FechaVcto = 0
                Call ListSafeIndexSelection(CboLnP, 0)
                Call ListSafeIndexSelection(CboTurno, 0)
                    
            Else
                txt_descripcion.Text = Empty
            End If
            oTeclado.Key_Tab
        Case vbKeyF2
            Tecla_Pulsada = True
            TmpDisableEvent = True
            Set Campo_Txt = txtFormula
            SafeFocus Campo_Txt
            Set Campo_Lbl = txt_descripcion
            Call MAKE_VIEW("MA_PRODUCCION", "c_Formula", "c_Descripcion", StellarMensaje(2803), Me, "GENERICO", , Campo_Txt, Campo_Lbl)
            TS = Now
            TS1 = DateAdd("s", 1, TS)
            While TS < TS1
                TS = Now
                DoEvents
            Wend
            oTeclado.Key_Return
            Tecla_Pulsada = False
            TmpDisableEvent = False
    End Select
End Sub

Private Sub txtHorasHombreXUnidad_Click()
    mValorAnt = txtHorasHombreXUnidad
    mValorNew = QuickInputRequest("Ingrese el Numero de Horas hombre por unidad.", True, , mValorAnt, "Escriba el Monto", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 0
        If CDbl(mValorNew) <> CDbl(mValorAnt) Then
            txtHorasHombreXUnidad = FormatNumber(Abs(mValorNew), Std_Decm)
        End If
    End If
End Sub

Private Sub txtMulti_Click()
    mValorAnt = txtMulti
    mValorNew = QuickInputRequest("Ingrese la Cantidad entera que desea aplicar para la Orden.", True, , mValorAnt, "Escriba la Cantidad", , , , , , , True)
    If IsNumeric(mValorNew) Then
        If Not IsNumeric(mValorAnt) Then mValorAnt = 1
        If CDbl(mValorNew) <> CDbl(mValorAnt) And CDbl(mValorNew) >= 1 Then
            txtMulti = FormatNumber(Abs(mValorNew), 0)
        End If
    End If
End Sub

Private Sub txtProducir_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtProducir_LostFocus
    ElseIf KeyCode = vbKeyF2 Then
        CmdProducir_Click
    End If
End Sub

Private Sub txtProducir_LostFocus()
    
    Set mRsProducto = New ADODB.Recordset
    mRsProducto.CursorLocation = adUseClient
    
    If txtProducir.Text <> Empty Then
        
        mRsProducto.Open "SELECT P.* FROM MA_PRODUCTOS P INNER JOIN MA_CODIGOS C " & _
        "ON P.c_Codigo = C.c_CodNasa " & _
        "WHERE C.c_Codigo = '" & txtProducir.Text & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not mRsProducto.EOF Then
            txtProducir.Text = mRsProducto!c_Codigo
            If txtProducir.Text <> txtProducir.Tag Then
                txtProducir.Tag = mRsProducto!c_Codigo
                txtDescProducto.Text = mRsProducto!c_Descri
                lblNomenclaturaCant = mRsProducto!c_Presenta
                If FrmAppLink.FichaProd_CamposPesoNeto Then
                    txtPesoUni = FormatNumber(mRsProducto!n_PesoNetoUni, 3)
                Else
                    txtPesoUni = FormatNumber(mRsProducto!n_Peso, 3)
                End If
                txtCantibul = FormatNumber(mRsProducto!n_Cantibul, 0)
                txtPesoEmp.Tag = Round(CDbl(txtPesoUni) * CDbl(txtCantibul), 8)
                txtPesoEmp.Text = FormatNumber(txtPesoEmp.Tag, 3)
                mCostoOrig = mRsProducto.Fields(CostoActivo).Value
                mDecimales = mRsProducto!Cant_Decimales
                If txtCantProducto.Tag = Empty Then txtCantProducto.Tag = txtMulti
                If FormaDeTrabajoRes = 1 Then
                    MultiplicadorCantibul = Abs(txtCantibul)
                    txtPesoTotalProductos.Text = FormatNumber(CDbl(txtPesoEmp.Tag) * CDbl(txtCantProducto.Tag), 3)
                Else
                    MultiplicadorCantibul = 1
                    txtPesoTotalProductos.Text = FormatNumber(CDbl(txtPesoUni.Text) * CDbl(txtCantProducto.Tag), 3)
                End If
                CargarGrid
                RecalcularTransaccion
            End If
        Else
            GoTo NotFound
        End If
        
        mRsProducto.ActiveConnection = Nothing
        
    Else
NotFound:
        txtProducir.Text = Empty
        txtProducir.Tag = Empty
        txt_descripcion.Text = Empty
    End If
    
End Sub

Private Sub CboPCD_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntL As Long
    
    mClsGrupos.cTipoGrupo = "PCD"
    
    If PosAntL <> CboPCD.ListIndex Or CboPCD.ListCount = 1 Then
        PosAntL = CboPCD.ListIndex
        If CboPCD.ListIndex = CboPCD.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboPCD
            mClsGrupos.CargarComboGrupos Ent.BDD, CboPCD
            Tecla_Pulsada = True
            ListRemoveItems CboPCD, " ", "Ninguno"
            Tecla_Pulsada = False
        Else
            If Trim(CboPCD.Text) <> Empty Then
                Found = False
                For I = 1 To CostosDirectos.Rows - 1
                    If UCase(CostosDirectos.TextMatrix(I, ColDir.TipoCosto)) = UCase(CboPCD.Text) Then
                        CostosDirectos.Row = I
                        CostosDirectos.Col = ColDir.Monto
                        Found = True
                        Exit For
                    End If
                Next
                CboPCD.Visible = False
                If Not Found Then
                    CostosDirectos.Row = CostosDirectos.Rows - 1
                    CostosDirectos.TextMatrix(CostosDirectos.Row, ColDir.TipoCosto) = CboPCD.Text
                    CostosDirectos.TextMatrix(CostosDirectos.Row, ColDir.Monto) = FormatNumber(0, Std_Decm)
                    CostosDirectos.Rows = CostosDirectos.Rows + 1
                    RecalcularTransaccion
                End If
            Else
                CboPCD.Visible = False
            End If
        End If
    End If
    
End Sub

Sub Grabar_Orden(tipo As String)
    
    Dim RsEureka As New ADODB.Recordset, RsInventarioCargar As New ADODB.Recordset, _
    RsInventarioDescargar As New ADODB.Recordset, RsCostoDir As New ADODB.Recordset
    
    Dim RsDepositoCargo As New ADODB.Recordset, RsDepositoDescargo As New ADODB.Recordset, _
    RsMerma As New ADODB.Recordset, ConsecutivoAjuste As String
    
    Dim ActiveTrans As Boolean
    
    'On Error GoTo RecordFail
    'VALIDAR DATOS
    
    RecalcularSumario
    
    If CDbl(txtCostoTotal) < 0 Then
        'Call Mensaje(True, "El monto de producci�n no puede ser negativo.")
        Mensaje True, StellarMensaje(2809)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        Exit Sub
    ElseIf CDbl(txtCostoTotal) = 0 Then
        'Call Mensaje(True, "No hay datos suficientes para grabar." & vbNewLine & "Verifique que existen productos para la realizar la producci�n.")
        Mensaje True, Replace(StellarMensaje(2810), "$(Line)", vbNewLine)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        Exit Sub
    End If
    
    If Trim(txt_descripcion.Text) = Empty Then
        CanRecord = False
        'Call Mensaje(True, "Debe Escribir la Descripci�n para el preparado de la f�rmula.")
        Mensaje True, StellarMensaje(2819)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        SafeFocus txt_descripcion
        Exit Sub
    End If
    
    If Trim(MSGridRecover(Ingredientes, 1, ColPre.Producto)) = Empty Then
        CanRecord = False
        'Call Mensaje(True, "Especifique el detalle de productos a utilizar.")
        Mensaje True, StellarMensaje(2821)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        SafeFocus Ingredientes
        Ingredientes.Col = ColPre.Producto
        Ingredientes.Row = Ingredientes.Rows - 1
        Exit Sub
    End If
    
    If txtProducir.Tag = Empty Then
        CanRecord = False
        'Call Mensaje(True, "Especifique el detalle de productos a producir.")
        Mensaje True, StellarMensaje(2821)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        SafeFocus txtProducir
        Exit Sub
    End If
      
    If Trim(txt_sucursal.Text) = Empty Then
        'Call Mensaje(True, "Debe colocar el dep�sito Origen, de donde se tomar�n los productos a utilizar.")
        Mensaje True, StellarMensaje(16134)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        Exit Sub
    End If
    
    If FrmAppLink.PRDRequiereLineaDeProduccion Then
        If Trim(CboLnP.Text) = Empty _
        Or UCase(Trim(CboLnP.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2964)
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            SafeFocus CboLnP
            Exit Sub
        End If
    End If
    
    If FrmAppLink.PRDRequiereTurno Then
        If Trim(CboTurno.Text) = Empty _
        Or UCase(Trim(CboTurno.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2965)
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            SafeFocus CboTurno
            Exit Sub
        End If
    End If
    
    Dim Cont As Integer
    
    For Cont = 1 To Ingredientes.Rows - 1
        If Ingredientes.TextMatrix(Cont, ColPre.Producto) = Empty _
        And Ingredientes.TextMatrix(Cont, ColPre.Canti) <> Empty Then
            Call Mensaje(True, Stellar_Mensaje(16165)) '"El C�digo del Producto no Existe.")
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            Exit Sub
        End If
    Next
    
    If Trim(txt_sucursal.Text) <> Empty Then
        
        Call Apertura_Recordset(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_SUCURSALES " & _
        "WHERE c_Codigo = '" & txt_sucursal & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsEureka.EOF Then
            lblDescLocalidad.Caption = RsEureka!c_Descripcion
        Else
            'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
            Mensaje True, StellarMensaje(16189)
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            lcLocalidad = Empty
            lblDescLocalidad.Caption = Empty
            txt_sucursal.Text = Empty
            Exit Sub
        End If
        
        Call Cerrar_Recordset(RsEureka)
        
    Else
        'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
        Mensaje True, StellarMensaje(16189)
        If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
        lbl_origen.Caption = ""
        Exit Sub
    End If
    
    lcLocalidad = txt_sucursal.Text
    
    If Not mOrdenAutomatica Then
        
        Dim Frm_Totalizar: Set Frm_Totalizar = FrmAppLink.GetFrmTotalizar
        
        With Frm_Totalizar
            
            .lbl_impuesto.Caption = FormatNumber(0)
            .lbl_subtotal.Caption = FormatNumber(txtCostoTotal, Std_Decm)
            .msk_bsdesc.Text = FormatNumber(0)
            .lbl_total.Caption = txtCostoTotal
            .txtImpuesto = FormatNumber(0)
            
            .ManejaObservacion = True
            .Obsevacion_SoloLectura = False
            .ObservacionInicial = ObservacionInicial
            
            If FrmAppLink.GetNivelUsuario >= FrmAppLink.PRDNivelVerCostosDirectos _
            And FrmAppLink.PRDNivelVerCostosDirectos > 0 Then
                .ManejaOtrosCargos = True
                .RecalcularOtrosCargos = False
                .MostrarPorcentajeOtrosCargos = False
                .txtOtrosCargos.Text = _
                FormatNumber(CDbl(txtCostosDir), Std_Decm)
            End If
            
            .fFechaEstimadaOrigen = CDbl(FechaEstimadaPRD)
            .fFechaVencimientoOrigen = FechaVcto
            .fBackOrderOrigen = esBackOrder
            
            .Show vbModal
            
        End With
        
        If FrmAppLink.GetLcGrabar = False Then
            Set Frm_Totalizar = Nothing
            Exit Sub
        End If
        
        FechaEstimadaPRD = Frm_Totalizar.fecha_recepcion
        
        If Frm_Totalizar.chk_vencimiento = vbChecked Then
            FechaVcto = CDbl(Frm_Totalizar.dtp_odc_vence)
        Else
            mFechaVcto = 0
        End If
        
        ObservacionInicial = Frm_Totalizar.txtObs.Text
        
        Set Frm_Totalizar = Nothing
        
    End If
    
    Ent.BDD.BeginTrans: ActiveTrans = True
    
    Dim RowIsClean As Boolean
    
    For I = 1 To Ingredientes.Rows - 1
        RowIsClean = True
        For K = 0 To Ingredientes.Cols - 1
            If Ingredientes.TextMatrix(I, K) <> Empty Then
                RowIsClean = False
                Exit For
            End If
        Next K
        If (RowIsClean And I > 1) Then Ingredientes.RemoveItem I
    Next I
    
    For I = 0 To Empaques.Rows - 1
        RowIsClean = True
        For K = 0 To Empaques.Cols - 1
            If Empaques.TextMatrix(I, K) <> Empty Then
                RowIsClean = False
                Exit For
            End If
        Next K
        If (RowIsClean And I > 1) Then Empaques.RemoveItem I
    Next I
    
    If tipo = "GORD" Then
        
        'GRABAR ORDEN
        
        lbl_consecutivo.Caption = Format(No_Consecutivo("Orden_Produccion_Nuevo"), "000000000")
        
        Call Apertura_RecordsetC(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_ORDEN_PRODUCCION " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Status = 'DPE' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsEureka.EOF Then
            
            RsEureka.AddNew
            
            Call Header_Orden_Produccion(RsEureka, "DPE", _
            FechaEstimadaPRD, FechaVcto)
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A UTILIZAR (DESCARGO)
            Call Apertura_RecordsetC(RsInventarioDescargar)
            
            RsInventarioDescargar.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND b_Producir = 0", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioDescargar.EOF Then
                Call Update_Utilizar_Orden(RsInventarioDescargar, tipo)
            Else
                Call Mensaje(True, "Existe un detalle con el documento N� '" & lbl_consecutivo.Caption & "' ")
                If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A PRODUCIR (CARGO)
            Call Apertura_RecordsetC(RsInventarioCargar)
            
            RsInventarioCargar.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND b_Producir = 1", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioCargar.EOF Then
                Call Update_Producir_Orden(RsInventarioCargar, tipo)
            Else
                Call Mensaje(True, "Existe un detalle con el documento N� '" & lbl_consecutivo.Caption & "' ")
                If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
            Call Apertura_RecordsetC(RsCostoDir)
            Ent.BDD.Execute "DELETE FROM MA_ORDEN_PRODUCCION_COSTOS_DIRECTOS WHERE CodOrden = '" & lbl_consecutivo & "' AND CodLocalidad = '" & txt_sucursal & "' "
            RsCostoDir.Open "SELECT * FROM MA_ORDEN_PRODUCCION_COSTOS_DIRECTOS WHERE CodOrden = '" & lbl_consecutivo & "' AND CodLocalidad = '" & txt_sucursal & "' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            With CostosDirectos
                For Cont = 1 To .Rows - 1
                    If .TextMatrix(Cont, ColDir.TipoCosto) <> Empty Then
                        If IsNumeric(.TextMatrix(Cont, ColDir.Monto)) Then
                            If CDbl(.TextMatrix(Cont, ColDir.Monto)) > 0 Then
                                RsCostoDir.AddNew
                                RsCostoDir!CodOrden = lbl_consecutivo
                                RsCostoDir!CodLocalidad = txt_sucursal.Text
                                RsCostoDir!IDGrupo = 0
                                RsCostoDir!TipoCosto = .TextMatrix(Cont, ColDir.TipoCosto)
                                RsCostoDir!Monto = CDbl(.TextMatrix(Cont, ColDir.Monto))
                                RsCostoDir!CodMoneda = dbmoneda.Text
                                RsCostoDir!nFactorTemp = CDbl(MSK_FACTOR.Text)
                                RsCostoDir.UpdateBatch
                            End If
                        End If
                    End If
                Next Cont
            End With
            
        Else
            Call Mensaje(True, "La orden N� '" & lbl_consecutivo.Caption & "' existe en el Sistema.")
            If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
    End If
    
    Ent.BDD.CommitTrans: ActiveTrans = False
    
    LcConsecu = lbl_consecutivo.Caption
    Find_Concept = "OPR"
    
    MostrarDocumentoStellar Find_Concept, FrmAppLink.GetFindStatus, LcConsecu, gCls
    
    If mOrdenAutomatica Then
        mOrdenAutomatica_Grabo = True
        Unload Me
        Exit Sub
    End If
    
    Call Nuevo_Consecutivo
    Call Cancelar
    
    Exit Sub
    
RecordFail:
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans: ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If mOrdenAutomatica Then mOrdenAutomatica_Fallo = True
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Sub Header_Orden_Produccion(ByRef RsTemp As ADODB.Recordset, _
Status As String, ByVal pFechaProducir As Date, _
ByVal pFechaVcto As Double)
    
    With RsTemp
        
        Find_Status = Status
        !c_Documento = lbl_consecutivo.Caption
        !d_Fecha = FechaBD(Now, FBD_FULL, True)
        !c_Status = Find_Status
        !c_CodLocalidad = txt_sucursal.Text
        !c_CodMoneda = dbmoneda.Text
        !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
        !n_CostoDir = CDbl(txtCostosDir.Text)
        !c_Observacion = ObservacionInicial
        If mOrdenAutomatica Then
            !b_BackOrder = mOrdenAutomatica_BackOrder
        Else
            !b_BackOrder = CBool(FrmAppLink.GetAceptaBackOrder)
        End If
        !c_Relacion = DocumentoRelacion
        !c_Formula = txtFormula.Text
        !c_Descripcion = txt_descripcion.Text
        !c_Usuario = FrmAppLink.GetCodUsuario
        !d_Fecha_Produccion = FechaBD(pFechaProducir, , True)
        !n_Subtotal = Round(SumaGrid(Ingredientes, ColPre.CostoProd) + _
        SumaGrid(Empaques, ColPre.CostoProd), 8)
        !n_MontoMerma = Round(SumaGrid(Ingredientes, ColPre.CostoPreXMerma) + _
        SumaGrid(Empaques, ColPre.CostoPreXMerma), 8)
        !n_Descuento = 0
        !n_Impuesto = 0
        !n_Total = CDbl(txtCostoTotal)
        
        If pFechaVcto = 0 Then
            !d_FechaVencimiento = Null
        Else
            !d_FechaVencimiento = FechaBD(CDate(pFechaVcto), , True)
        End If
        
        !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
        !c_LineaProduccion = CboLnP.Text
        !c_Turno = CboTurno.Text
        
        !TipoOrdenProduccion = 1
        !PesoUnitario = CDbl(txtPesoUni)
        !Cantibul = CDbl(txtCantibul)
        !PesoEmpaque = CDbl(txtPesoEmp.Tag)
        !CapacidadMaxLote = CDbl(txtCapacidadMaximaLote)
        !CapacidadLote = CDbl(txtCapacidadLote)
        !UnidadesPorLote = CDbl(txtCantiLote.Tag)
        !CantidadBaseFormula = CantidadBaseFormula
        
        If Not IsNumeric(txtCostoHoraHombre) Then txtCostoHoraHombre = 0
        If Not IsNumeric(txtCostoHoraCargaFabril) Then txtCostoHoraCargaFabril = 0
        If Not IsNumeric(txtHorasHombreXUnidad) Then txtHorasHombreXUnidad = 0
        
        !CostoHoraHombre = CDbl(txtCostoHoraHombre)
        !CostoHoraCargaFabril = CDbl(txtCostoHoraCargaFabril)
        !HorasHombrePorUnidad = CDbl(txtHorasHombreXUnidad)
        
        !PesoTotal = CDbl(txtPesoTotalProductos)
        !PesoTotalNeto = CDbl(txtPesoTotalMezcla)
        
    End With
    
    RsTemp.UpdateBatch
    
End Sub

Sub Update_Utilizar_Orden(ByRef RsTemp As ADODB.Recordset, _
tipo As String)
    
    Dim Cont As Integer, NewCont As Long
    
    For Cont = 1 To Ingredientes.Rows - 1
        
        mCant = Ingredientes.TextMatrix(Cont, ColPre.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Documento = lbl_consecutivo.Caption
            !c_Linea = NewCont ' Cont
            !c_Descripcion = Ingredientes.TextMatrix(Cont, ColPre.Descripci�n)
            !c_Presenta = Ingredientes.TextMatrix(Cont, ColPre.Presentaci�n)
            !c_CodArticulo = Ingredientes.TextMatrix(Cont, ColPre.Producto)
            !c_CodigoInput = !c_CodArticulo
            
            !n_Cantidad = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.Canti)), _
            CLng(Ingredientes.TextMatrix(Cont, ColPre.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.CostoPre)), Std_Decm))
            !n_PorcMerma = CDbl(Ingredientes.TextMatrix(Cont, ColPre.PorcMerma))
            !n_MontoMerma = CDbl(Ingredientes.TextMatrix(Cont, ColPre.CostoPreXMerma) * !n_Cantidad)
            !n_Subtotal = CDbl(FormatNumber(CDbl(Ingredientes.TextMatrix(Cont, ColPre.CostoPre) * !n_Cantidad), Std_Decm))
            !n_Descuento = 0
            !n_Impuesto = 0
            !n_Total = Round(!n_Subtotal + !n_MontoMerma, 8)
            
            !b_Producir = 0
            
            !n_Cant_Utilizada = 0
            !n_Cant_Realizada = 0
            
            !Impuesto1 = 0
            !Impuesto2 = 0
            !Impuesto3 = 0
            
            !c_IDLote = vbNullString
            !n_CostoOriginal = 0
            !c_CodLocalidad = txt_sucursal.Text
            !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_Decimales = CDbl(Ingredientes.TextMatrix(Cont, ColPre.LDeci))
            !n_Prod_Ext = vbNullString
            !ns_CantidadEmpaque = CDbl(Ingredientes.TextMatrix(Cont, ColPre.nCantibul))
            !nu_FactorCosto = 1
            
            !TipoItem = 1
            !CantidadEnLote = CDbl(Ingredientes.TextMatrix(Cont, ColPre.CantLote))
            !PorcVsLote = CDbl(Ingredientes.TextMatrix(Cont, ColPre.PorcLoteReal))
            !TotalCantidadPorTipoItem = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.Canti))
            !TotalCantidadNetaPorTipoItem = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.NetCant))
            !TotalCostoPorTipoItem = CDbl(TotalesIngredientes.TextMatrix(0, ColPre.CostoProdNeto))
            !TotalCostoEnItems = CDbl(GrdTotales.TextMatrix(1, ColTot.Monto))
            
            If txtFormula <> Empty Then
                !CantidadBaseFormula = Round(!n_Cantidad / CDbl(txtCantProducto.Tag), !n_Decimales)
            Else
                !CantidadBaseFormula = 0
            End If
            
        End With
        
        RsTemp.UpdateBatch
        
        End If
        End If
        
    Next Cont
    
    For Cont = 0 To Empaques.Rows - 1
        
        mCant = Empaques.TextMatrix(Cont, ColPre.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Documento = lbl_consecutivo.Caption
            !c_Linea = NewCont ' Cont
            !c_Descripcion = Empaques.TextMatrix(Cont, ColPre.Descripci�n)
            !c_Presenta = Empaques.TextMatrix(Cont, ColPre.Presentaci�n)
            !c_CodArticulo = Empaques.TextMatrix(Cont, ColPre.Producto)
            !c_CodigoInput = !c_CodArticulo
            
            !n_Cantidad = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.Canti)), _
            CLng(Empaques.TextMatrix(Cont, ColPre.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.CostoPre)), Std_Decm))
            !n_PorcMerma = CDbl(Empaques.TextMatrix(Cont, ColPre.PorcMerma))
            !n_MontoMerma = CDbl(Empaques.TextMatrix(Cont, ColPre.CostoPreXMerma) * !n_Cantidad)
            !n_Subtotal = CDbl(FormatNumber(CDbl(Empaques.TextMatrix(Cont, ColPre.CostoPre) * !n_Cantidad), Std_Decm))
            !n_Descuento = 0
            !n_Impuesto = 0
            !n_Total = Round(!n_Subtotal + !n_MontoMerma, 8)
            
            !b_Producir = 0
            
            !n_Cant_Utilizada = 0
            !n_Cant_Realizada = 0
            
            !Impuesto1 = 0
            !Impuesto2 = 0
            !Impuesto3 = 0
            
            !c_IDLote = vbNullString
            !n_CostoOriginal = 0
            !c_CodLocalidad = txt_sucursal.Text
            !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_Decimales = CDbl(Empaques.TextMatrix(Cont, ColPre.LDeci))
            !n_Prod_Ext = vbNullString
            !ns_CantidadEmpaque = CDbl(Empaques.TextMatrix(Cont, ColPre.nCantibul))
            !nu_FactorCosto = 1
            
            !TipoItem = 2
            !CantidadEnLote = CDbl(Empaques.TextMatrix(Cont, ColPre.CantLote))
            !PorcVsLote = 0
            !TotalCantidadPorTipoItem = 0
            !TotalCantidadNetaPorTipoItem = 0
            !TotalCostoPorTipoItem = CDbl(TotalesEmpaques.TextMatrix(0, ColPre.CostoProdNeto))
            !TotalCostoEnItems = CDbl(GrdTotales.TextMatrix(1, ColTot.Monto))
            
            If txtFormula <> Empty Then
                !CantidadBaseFormula = Round(!n_Cantidad / CDbl(txtCantProducto.Tag), !n_Decimales)
            Else
                !CantidadBaseFormula = 0
            End If
            
        End With
        
        RsTemp.UpdateBatch
        
        End If
        End If
        
    Next Cont
    
End Sub

Sub Update_Producir_Orden(ByRef RsTemp As ADODB.Recordset, _
tipo As String)
    
    Dim Cont As Integer, NewCont As Long
    
    'For Cont = 1 To Producir.Rows - 1
        
        mCant = CDbl(txtCantProducto.Tag)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Documento = lbl_consecutivo.Caption
            !c_Linea = NewCont ' Cont
            
            !c_CodArticulo = txtProducir
            !c_CodigoInput = !c_CodArticulo
            !c_Descripcion = txtDescProducto
            !c_Presenta = lblNomenclaturaCant
            
            !n_Cantidad = Round(mCant, 8)
            !n_Costo = Round(CDbl(txtCostoUni), 8)
            !n_PorcMerma = 0
            !n_MontoMerma = 0
            !n_Subtotal = Round(CDbl(txtCostoTotal), 8)
            !n_Descuento = 0
            !n_Impuesto = 0
            !n_Total = !n_Subtotal
            
            !b_Producir = 1
            
            !n_Cant_Utilizada = 0
            !n_Cant_Realizada = 0
            
            !Impuesto1 = 0
            !Impuesto2 = 0
            !Impuesto3 = 0
            
            !c_IDLote = vbNullString
            !c_CodLocalidad = txt_sucursal.Text
            !n_CostoOriginal = mCostoOrig
            !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_Decimales = mDecimales
            !n_Prod_Ext = vbNullString
            !ns_CantidadEmpaque = CDbl(txtCantibul)
            !nu_FactorCosto = 1
            
            !CantidadBaseFormula = CantidadBaseFormula
            
        End With
        
        RsTemp.UpdateBatch
        
        End If
        End If
        
    'Next Cont
    
End Sub

Private Sub CboLnP_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntL As Long
    
    mClsGrupos.cTipoGrupo = "LNP"
    
    If PosAntL <> CboLnP.ListIndex Or CboLnP.ListCount = 1 Then
        PosAntL = CboLnP.ListIndex
        If CboLnP.ListIndex = CboLnP.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboLnP
            mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
            Tecla_Pulsada = True
            ListRemoveItems CboLnP, "Ninguno"
            Tecla_Pulsada = False
        End If
    End If
    
End Sub

Private Sub CboTurno_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntT As Long
    
    mClsGrupos.cTipoGrupo = "TNP"
    
    If PosAntT <> CboTurno.ListIndex Or CboTurno.ListCount = 1 Then
        PosAntT = CboTurno.ListIndex
        If CboTurno.ListIndex = CboTurno.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboTurno
            mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
            Tecla_Pulsada = True
            ListRemoveItems CboTurno, "Ninguno"
            Tecla_Pulsada = False
        End If
    End If
    
End Sub
