VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMask32.ocx"
Begin VB.Form FRM_PRODUCCION_ORDEN_MANUAL_2 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   165
   ClientTop       =   -30
   ClientWidth     =   15330
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   Begin MSMask.MaskEdBox MascaraLote 
      Height          =   255
      Left            =   12120
      TabIndex        =   49
      Top             =   3720
      Visible         =   0   'False
      Width           =   915
      _ExtentX        =   1614
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
      PromptChar      =   "_"
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   40
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   41
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.TextBox txtedit2 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1440
      TabIndex        =   13
      Top             =   8830
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.TextBox txtedit 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   1440
      TabIndex        =   11
      Top             =   4670
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.TextBox MTotal 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   390
      Left            =   11400
      Locked          =   -1  'True
      TabIndex        =   36
      Top             =   7200
      Width           =   2445
   End
   Begin VB.TextBox dbmoneda 
      Enabled         =   0   'False
      Height          =   315
      Left            =   5535
      MaxLength       =   10
      TabIndex        =   34
      Top             =   1395
      Visible         =   0   'False
      Width           =   1365
   End
   Begin VB.TextBox MSK_FACTOR 
      Alignment       =   1  'Right Justify
      CausesValidation=   0   'False
      Enabled         =   0   'False
      Height          =   315
      Left            =   7155
      TabIndex        =   33
      Text            =   "0"
      Top             =   1425
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txt_merma 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   390
      Left            =   6360
      Locked          =   -1  'True
      TabIndex        =   31
      Top             =   7215
      Width           =   2445
   End
   Begin VB.TextBox txt_monto 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   390
      Left            =   2115
      Locked          =   -1  'True
      TabIndex        =   28
      Top             =   7215
      Width           =   2445
   End
   Begin VB.Frame frame_datos 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   240
      TabIndex        =   17
      Top             =   1695
      Width           =   14775
      Begin VB.ComboBox CboTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10440
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   960
         Visible         =   0   'False
         Width           =   3120
      End
      Begin VB.ComboBox CboLnP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10455
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   480
         Visible         =   0   'False
         Width           =   3120
      End
      Begin VB.TextBox txt_cantidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10590
         MaxLength       =   10
         TabIndex        =   9
         Top             =   1440
         Width           =   1125
      End
      Begin VB.CommandButton Aplicar_CostoDirecto 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   7920
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Recalcula la Produccion en base al nuevo costo."
         Top             =   1365
         Width           =   600
      End
      Begin VB.TextBox txt_costosdirec 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   5940
         TabIndex        =   5
         Text            =   "0"
         Top             =   1500
         Width           =   1860
      End
      Begin VB.CheckBox ModifFlag 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Trabajar sin formula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   285
         TabIndex        =   4
         Top             =   1560
         Width           =   2655
      End
      Begin VB.CommandButton cmd_destino 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2670
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":6614
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1080
         Width           =   360
      End
      Begin VB.CommandButton cmd_origen 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2670
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":6E16
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   630
         Width           =   360
      End
      Begin VB.TextBox txt_origen 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3075
         MaxLength       =   10
         TabIndex        =   2
         Top             =   630
         Width           =   1500
      End
      Begin VB.TextBox txt_destino 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3075
         MaxLength       =   10
         TabIndex        =   3
         Top             =   1080
         Width           =   1500
      End
      Begin VB.CommandButton cmd_preparado 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2670
         Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":7618
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   165
         Width           =   360
      End
      Begin VB.TextBox txt_descripcion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4785
         TabIndex        =   1
         Top             =   165
         Width           =   7700
      End
      Begin VB.TextBox txt_preparado 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3075
         MaxLength       =   10
         TabIndex        =   0
         Top             =   120
         Width           =   1500
      End
      Begin VB.Label lblTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Turno de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   8400
         TabIndex        =   51
         Top             =   960
         Visible         =   0   'False
         Width           =   1875
      End
      Begin VB.Label lblLineaProduccion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "L�nea de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   8400
         TabIndex        =   50
         Top             =   510
         Visible         =   0   'False
         Width           =   1875
      End
      Begin VB.Label lblMulti 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Aplicar F�rmula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8880
         TabIndex        =   48
         Top             =   1500
         Width           =   1665
      End
      Begin VB.Label lblVeces 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "veces."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   11985
         TabIndex        =   47
         Top             =   1500
         Width           =   630
      End
      Begin VB.Label lblCostosDir 
         BackStyle       =   0  'Transparent
         Caption         =   "Costos Directos de Produccion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   660
         Left            =   4020
         TabIndex        =   39
         Top             =   1395
         Width           =   1815
         WordWrap        =   -1  'True
      End
      Begin VB.Label lbl_destino 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   4785
         TabIndex        =   23
         Top             =   1080
         Width           =   3500
      End
      Begin VB.Label lbl_origen 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   4785
         TabIndex        =   22
         Top             =   630
         Width           =   3500
      End
      Begin VB.Label lblOrigen 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito Origen"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   285
         TabIndex        =   21
         Top             =   690
         Width           =   1545
      End
      Begin VB.Label lblDestino 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito Destino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   285
         TabIndex        =   20
         Top             =   1140
         Width           =   1650
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo de F�rmula"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   285
         TabIndex        =   18
         Top             =   225
         Width           =   1755
      End
   End
   Begin MSFlexGridLib.MSFlexGrid Producir 
      CausesValidation=   0   'False
      Height          =   2250
      Left            =   240
      TabIndex        =   12
      Top             =   8430
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   3969
      _Version        =   393216
      Cols            =   6
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      Enabled         =   -1  'True
      FocusRect       =   2
      HighLight       =   0
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid Preparado 
      CausesValidation=   0   'False
      Height          =   2850
      Left            =   240
      TabIndex        =   10
      Top             =   4260
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   5027
      _Version        =   393216
      Cols            =   8
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      Enabled         =   -1  'True
      FocusRect       =   2
      HighLight       =   0
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   10410
      Top             =   3450
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":7E1A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":9BAC
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":B93E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":D6D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":F462
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":111F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FRM_PRODUCCION_ORDEN_MANUAL_2.frx":12F86
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1080
      ScaleWidth      =   15330
      TabIndex        =   26
      Top             =   421
      Width           =   15330
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   -1920
         TabIndex        =   27
         Top             =   0
         Width           =   12270
         Begin MSComctlLib.Toolbar BarraO 
            Height          =   810
            Left            =   2040
            TabIndex        =   38
            Top             =   120
            Width           =   10140
            _ExtentX        =   17886
            _ExtentY        =   1429
            ButtonWidth     =   1746
            ButtonHeight    =   1429
            ToolTips        =   0   'False
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   9
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "F2 Buscar"
                  ImageIndex      =   1
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   5
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "BORD"
                        Text            =   "Orden de Producci�n"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "BESP"
                        Text            =   "Orden de Producci�n en Espera"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "BPRD"
                        Text            =   "Produccion"
                     EndProperty
                     BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "BPRDESP"
                        Text            =   "Produccion en Espera"
                     EndProperty
                     BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Visible         =   0   'False
                        Key             =   "BDCO"
                        Text            =   "Ordenes Completadas"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   2
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "GORD"
                        Text            =   "Grabar Orden de Producci�n"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "GESP"
                        Text            =   "Colocar en Espera"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Anular"
                  Key             =   "Anular"
                  Object.ToolTipText     =   "F6 Anular"
                  ImageIndex      =   3
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "AORD"
                        Text            =   "Orden de Producci�n"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "AESP"
                        Text            =   "Orden en Espera"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Reimprimir"
                  Key             =   "Reimprimir"
                  Object.ToolTipText     =   "F8 Reimprimir"
                  ImageIndex      =   5
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "RORD"
                        Text            =   "Orden de Producci�n"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "RESP"
                        Text            =   "Orden en Espera"
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Visible         =   0   'False
                        Key             =   "RDCO"
                        Text            =   "Ordenes Completadas"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "F12 Salir"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Opciones"
                  Key             =   "Opciones"
                  Object.ToolTipText     =   "F1 Ayuda"
                  ImageIndex      =   7
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   2
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Ayuda"
                        Text            =   "Ayuda"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "Moneda"
                        Text            =   "Cambiar Moneda"
                     EndProperty
                  EndProperty
               EndProperty
            EndProperty
         End
      End
      Begin VB.Label lbl_fecha 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "01/01/2015"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   11040
         TabIndex        =   46
         Top             =   675
         Width           =   3495
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   45
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lbl_consecutivo 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0000000"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   12840
         TabIndex        =   44
         Top             =   195
         Width           =   1755
      End
      Begin VB.Label lbl_concepto 
         BackStyle       =   0  'Transparent
         Caption         =   "Orden de Producci�n No:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10560
         TabIndex        =   43
         Top             =   195
         Width           =   2505
      End
      Begin VB.Shape Shape1 
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   1080
         Index           =   0
         Left            =   10440
         Top             =   0
         Width           =   4575
      End
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00AE5B00&
      X1              =   2745
      X2              =   14675
      Y1              =   8250
      Y2              =   8250
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00AE5B00&
      X1              =   2745
      X2              =   14675
      Y1              =   4100
      Y2              =   4100
   End
   Begin VB.Label QUIEN_SOY 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "ORDEN DE PRODUCCI�N MANUAL"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   720
      TabIndex        =   19
      Top             =   1140
      Visible         =   0   'False
      Width           =   4950
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Monto Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   9855
      TabIndex        =   37
      Top             =   7275
      Width           =   1200
   End
   Begin VB.Label lbl_moneda 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   10755
      TabIndex        =   35
      Top             =   1485
      Visible         =   0   'False
      Width           =   3525
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Merma"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   5385
      TabIndex        =   32
      Top             =   7275
      Width           =   690
   End
   Begin VB.Label lbl_simbolo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   345
      Left            =   10965
      TabIndex        =   30
      Top             =   5400
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Subtotal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   1065
      TabIndex        =   29
      Top             =   7275
      Width           =   780
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00E7E8E8&
      Caption         =   "Productos a Utilizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Left            =   240
      TabIndex        =   25
      Top             =   3960
      Width           =   2055
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00E7E8E8&
      Caption         =   "Productos a Producir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Left            =   240
      TabIndex        =   24
      Top             =   8100
      Width           =   2205
   End
   Begin VB.Shape Shape1 
      Height          =   2010
      Index           =   3
      Left            =   240
      Top             =   8430
      Width           =   14775
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   2970
      Index           =   2
      Left            =   240
      Top             =   4785
      Width           =   14775
   End
End
Attribute VB_Name = "FRM_PRODUCCION_ORDEN_MANUAL_2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rsMonedas As New ADODB.Recordset
Dim RsDeposito As New ADODB.Recordset, rsPmoneda As New ADODB.Recordset, RsRecetas As New ADODB.Recordset, rsProducto As New ADODB.Recordset
Dim cdeposito As String, tmonto As Double, TMerma As Double, pMonto As Double
Dim CostoActivo As String, UpdateData As Boolean
Dim Monto_Ord As Double, Mmoneda As String, Who_Ami As Integer, FlgIncoherente As Boolean
Dim RsMaProduccion As New ADODB.Recordset, RsTrProduccion As New ADODB.Recordset, RsProducir As New ADODB.Recordset
Const ConceptoAjuste As Integer = 93

Dim mClsGrupos As New ClsGrupos
Dim AnchoColDesc_Prepaparado    As Long
Dim AnchoColDesc_Producir       As Long

Private TmpDisableEvent As Boolean
Private mRecalculandoIngredientes As Boolean

Private Const ColorUp As Long = &HFFC0C0
Private Const ColorNormal As Long = &HFAFAFA
'Private ProcesandoCambio As Boolean
Private Const MaskWidth = 2500

Private FormulaOrigenOPR    As String
Private ObservacionInicial  As String
Private DocumentoRelacion   As String
Private esBackOrder         As Boolean
Private FechaEstimadaPRD    As Date
Private FechaVcto           As Double
Private EstatusOPR          As String

Private Enum ColumnaPreparado
    LDeci               ' 0
    Producto            ' 1
    Descripci�n         ' 2
    CantOrden           ' 3
    CantTeorica         ' 4
    Canti               ' 5
    Presentaci�n        ' 6
    Lote                ' 7
    PorcMerma           ' 8
    Merma               ' 9
    CostoPreXMerma      ' 10
    CostoPre            ' 11
    Cantibul            ' 12
    CostoProd           ' 13
    CostoOriginal       ' 14
    CodigoInput         ' 15
    DocumentoOrigen     ' 16
    Info                ' 17
    [ColCount]
End Enum

Private Enum ColumnaProducir
    LDeci               ' 0
    Producto            ' 1
    Descripci�n         ' 2
    CantOrden           ' 3
    CantTeorica         ' 4
    Canti               ' 5
    Presentaci�n        ' 6
    Lote                ' 7
    CostoPre            ' 8
    CostoProd           ' 9
    Factor              ' 10
    Cantibul            ' 11
    CostoOriginal       ' 12
    CodigoInput         ' 13
    DocumentoOrigen     ' 14
    Info                ' 15
    [ColCount]
End Enum

Private ManejaLote                              As Boolean
Private ManejaMermaExplicita                    As Boolean
Private ManejaFactor                            As Boolean
Private CambioPrecioAutomatico                  As Integer
Private MascaraParaLotes                        As String

Private Sub Aplicar_CostoDirecto_Click()
    
    If IsNumeric(txt_costosdirec.Text) Then
        
        If CDbl(txt_costosdirec.Text) >= 0 Then
            
            txt_costosdirec.Text = FormatNumber(txt_costosdirec, Std_Decm)
            
            MTotal = FormatNumber(CDbl(txt_monto.Text) + CDbl(txt_costosdirec.Text), Std_Decm)
            
            Call DistTotal(MTotal, Producir)
            
        Else
            
            'Call Mensaje(True, "El costo directo de produccion no puede ser negativo.")
            Mensaje True, StellarMensaje(2800)
            
        End If
    
    Else
        
        'Call Mensaje(True, "Por favor, asegurese de que el Costo Directo sea un numero valido.")
        Mensaje True, StellarMensaje(2801)
        
    End If

End Sub

Private Sub BarraO_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case "BUSCAR"
            Tecla_Pulsada = True
            'Call Form_KeyDown(vbKeyF2, 0)
            BarraO_ButtonMenuClick BarraO.Buttons(1).ButtonMenus("BORD")
            
        Case "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
            
        Case "ANULAR"
            Call Form_KeyDown(vbKeyF6, 0)
        
        Case "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        
        Case "REIMPRIMIR"
            Call Form_KeyDown(vbKeyF8, 0)
        
        Case "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        
        Case "AYUDA"
            'Ayuda
            
    End Select
    
End Sub

Private Sub BarraO_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    
    Select Case UCase(ButtonMenu.Key)
        
        Case "GORD", "GESP"
            'GRABAR
            If Find_Concept = "PRD" Then
                Call Grabar_Produccion(UCase(ButtonMenu.Key))
            ElseIf Find_Concept = "OPR" Then
                Call Grabar_Orden(UCase(ButtonMenu.Key))
            End If
        
        'BUSCAR
        Case "BPRD", "BPRDESP"
            Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "BPRD", "DCO", "DWT"), False, False)
            
        Case "BORD", "BESP", "BDCO"
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, _
            IIf(UCase(ButtonMenu.Key) = "BORD", "DPE", _
            IIf(UCase(ButtonMenu.Key) = "BDCO", "DCO", "DWT")), _
            False, False, , , "c_CodLocalidad")
            
        Case "AORD", "AESP"
            
            'ANULAR
            If Find_Concept = "PRD" Then
                Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "AORD", "DCO", "DWT"), True, False)
            ElseIf Find_Concept = "OPR" Then
                Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "AORD", "DPE", "DWT"), True, False, , , "c_CodLocalidad")
            End If
            
        Case "RESP", "RORD"
            
            'REIMPRIMIR
            
            If Find_Concept = "PRD" Then
                Call MAKE_DOC("ma_inventario", "c_Documento", "c_Dep_Orig", "d_Fecha", StellarMensaje(2803), Me, "PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "RORD", "DCO", "DWT"), False, True)
            ElseIf Find_Concept = "OPR" Then
                Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, IIf(UCase(ButtonMenu.Key) = "RORD", "DPE", "DWT"), False, True, , , "c_CodLocalidad")
            End If
            
        Case "RDCO"
            Call MAKE_DOC("MA_ORDEN_PRODUCCION", "c_Documento", "MA_SUCURSALES.c_Descripcion", "d_Fecha", StellarMensaje(2802), Me, "ORDEN_PRODUCCION", Find_Concept, "DCO", False, True, , , "c_CodLocalidad")
        
        Case "MONEDA"
            
            Call Cambiar_Moneda
            Call ReEscribir(CDbl(txt_cantidad.Text))
            
    End Select
    
End Sub

Private Sub cmd_destino_Click()
    Tecla_Pulsada = True
    Call txt_destino_KeyDown(vbKeyF2, 0)
End Sub

Private Sub cmd_origen_Click()
    Tecla_Pulsada = True
    Call txt_ORIGEN_KeyDown(vbKeyF2, 0)
End Sub

Private Sub cmd_preparado_Click()
    'Set Campo_Txt = txt_preparado
    'Set Campo_Lbl = txt_Descripcion
    'Call MAKE_VIEW("MA_PRODUCCION", "C_FORMULA", "c_Descripcion", StellarMensaje(2803), Me, "GENERICO")
    'txt_preparado.SetFocus
    ''SendKeys Chr$(13)
    'oTeclado.Key_Return
    txt_preparado_KeyDown vbKeyF2, 0
End Sub

Private Sub Form_Load()
    
    'Grid de Formula Base
    'Preparado.Cols = Preparado.Cols + 1
    
    BarraO.Buttons(1).Caption = Stellar_Mensaje(102) 'buscar
    
    BarraO.Buttons(1).ButtonMenus(1).Text = Stellar_Mensaje(5014) 'Orden de Producci�n
    BarraO.Buttons(1).ButtonMenus(2).Text = Stellar_Mensaje(5015) 'Orden de Producci�n en Espera
    
    If Find_Concept = "PRD" Then
        BarraO.Buttons(1).ButtonMenus(3).Text = Stellar_Mensaje(1010) 'Producci�n
        BarraO.Buttons(1).ButtonMenus(4).Text = Stellar_Mensaje(2951) 'Producci�n en Espera
        BarraO.Buttons(1).ButtonMenus(2).Visible = False
    ElseIf Find_Concept = "OPR" Then
        BarraO.Buttons(1).ButtonMenus(3).Visible = False
        BarraO.Buttons(1).ButtonMenus(4).Visible = False
    End If
    
    BarraO.Buttons(1).ButtonMenus(5).Text = Stellar_Mensaje(2955)
    BarraO.Buttons(1).ButtonMenus(5).Visible = True
    
    BarraO.Buttons(3).Caption = Stellar_Mensaje(103) 'grabar
    
    If Find_Concept = "PRD" Then
        BarraO.Buttons(3).ButtonMenus(1).Text = Stellar_Mensaje(2952) 'Grabar Producci�n
    ElseIf Find_Concept = "OPR" Then
        BarraO.Buttons(3).ButtonMenus(1).Text = Stellar_Mensaje(5016) 'Grabar Orden de Producci�n
    End If
    
    BarraO.Buttons(3).ButtonMenus(2).Text = Stellar_Mensaje(116) 'Colocar en Espera
    BarraO.Buttons(4).Caption = Stellar_Mensaje(104) 'anular
    
    If Find_Concept = "PRD" Then
        BarraO.Buttons(4).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(3).Text
        BarraO.Buttons(4).ButtonMenus(2).Text = BarraO.Buttons(1).ButtonMenus(4).Text
    ElseIf Find_Concept = "OPR" Then
        BarraO.Buttons(4).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(1).Text
        BarraO.Buttons(4).ButtonMenus(2).Text = BarraO.Buttons(1).ButtonMenus(2).Text
    End If

    BarraO.Buttons(5).Caption = Stellar_Mensaje(105) 'cancelar
    BarraO.Buttons(6).Caption = Stellar_Mensaje(106) 'reimprimir
    
    If Find_Concept = "PRD" Then
        BarraO.Buttons(6).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(3).Text
        BarraO.Buttons(6).ButtonMenus(2).Text = BarraO.Buttons(1).ButtonMenus(4).Text
    ElseIf Find_Concept = "OPR" Then
        BarraO.Buttons(6).ButtonMenus(1).Text = BarraO.Buttons(1).ButtonMenus(1).Text
        BarraO.Buttons(6).ButtonMenus(2).Text = BarraO.Buttons(1).ButtonMenus(2).Text
        BarraO.Buttons(6).ButtonMenus(3).Text = Stellar_Mensaje(2955)
        BarraO.Buttons(6).ButtonMenus(3).Visible = True
    End If
    
    BarraO.Buttons(8).Caption = Stellar_Mensaje(107) 'salir
    BarraO.Buttons(9).Caption = Stellar_Mensaje(108) 'opciones
    BarraO.Buttons(9).ButtonMenus(1).Text = Stellar_Mensaje(7) 'Ayuda
    BarraO.Buttons(9).ButtonMenus(2).Text = Stellar_Mensaje(120) 'Cambiar Moneda
    
    If Find_Concept = "PRD" Then
        lbl_concepto.Caption = Stellar_Mensaje(2953) 'produccion n�
    Else
        lbl_concepto.Caption = Stellar_Mensaje(5021) 'orden de produccion n�
    End If
    
    Label5.Caption = Stellar_Mensaje(128) 'fecha
    
    Label2.Caption = Stellar_Mensaje(5017) 'codigo de formula
    
    If Find_Concept = "PRD" Then
        lblDestino.Caption = Stellar_Mensaje(4033) 'deposito destino
        lblOrigen.Caption = Stellar_Mensaje(4032) 'deposito origen
    ElseIf Find_Concept = "OPR" Then
        lblDestino.Caption = StellarMensaje(12) 'LOCALIDAD
        lblOrigen.Visible = False
        cmd_origen.Visible = False
        lbl_origen.Visible = False
        txt_origen.Visible = False
        lblDestino.Top = lblOrigen.Top
        txt_destino.Top = txt_origen.Top
        cmd_destino.Top = cmd_origen.Top
        lbl_destino.Top = lbl_origen.Top
    End If
    
    ModifFlag.Caption = Stellar_Mensaje(5022) 'trabajar sin formula
    lblCostosDir.Caption = Stellar_Mensaje(5018) 'costos directos de produccion
    
    Label6.Caption = Stellar_Mensaje(5006) 'productos a utilizar
    Label12.Caption = Stellar_Mensaje(139) 'subtotal
    Label1.Caption = Stellar_Mensaje(3036) 'merma
    Label11.Caption = Stellar_Mensaje(5007) 'monto total
    
    Label7.Caption = Stellar_Mensaje(5008) 'productos a producir
    
    lblLineaProduccion.Caption = StellarMensaje(2947) ' LineaProduccion
    lblTurno.Caption = StellarMensaje(2966) ' Turno de Produccion
    
    If Find_Concept = "PRD" Then
        ObservacionInicial = StellarMensaje(2948) ' EJECUCION DE FORMULA DE PRODUCCI�N
        If FrmAppLink.InventarioCampoLineaProduccion Then
            lblLineaProduccion.Visible = True
            CboLnP.Visible = True
        End If
        If FrmAppLink.InventarioCampoTurnoProduccion Then
            lblTurno.Visible = True
            CboTurno.Visible = True
        End If
    ElseIf Find_Concept = "OPR" Then
        ObservacionInicial = StellarMensaje(5014) ' ORDEN DE PRODUCCION
        lblLineaProduccion.Visible = True
        CboLnP.Visible = True
        lblTurno.Visible = True
        CboTurno.Visible = True
    End If
    
    mClsGrupos.cTipoGrupo = "LNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
    
    mClsGrupos.cTipoGrupo = "TNP"
    mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
    
    ManejaLote = FrmAppLink.PRDManejaLote
    
    ManejaMermaExplicita = FrmAppLink.PRDManejaMermaExplicita
    
    Preparado.Cols = ColumnaPreparado.ColCount
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.LDeci, "LDec", 0, flexAlignCenterCenter)
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.Producto, Stellar_Mensaje(5010), 1700, flexAlignCenterCenter) 'producto
    
    If ManejaLote Then
        Call MSGridAsign(Preparado, 0, ColumnaPreparado.Descripci�n, Stellar_Mensaje(143), 4100 + IIf(Not ManejaMermaExplicita, 700, 0), flexAlignCenterCenter) 'descripcion
        Call MSGridAsign(Preparado, 0, ColumnaPreparado.Lote, Stellar_Mensaje(2936), 1200, flexAlignCenterCenter) ' Lote
    Else
        Call MSGridAsign(Preparado, 0, ColumnaPreparado.Descripci�n, Stellar_Mensaje(143), 5000 + IIf(Not ManejaMermaExplicita, 300, 0), flexAlignCenterCenter) 'descripcion
        Call MSGridAsign(Preparado, 0, ColumnaPreparado.Lote, Stellar_Mensaje(5023), 0, flexAlignRightCenter)
    End If
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.Presentaci�n, PRDLabelCampoPresenta, 1400, flexAlignCenterCenter) 'presentacion
    
    If ManejaMermaExplicita Then
        Call MSGridAsign(Preparado, 0, ColumnaPreparado.PorcMerma, StellarMensaje(5013), 1000, flexAlignRightCenter)
    Else
        Call MSGridAsign(Preparado, 0, ColumnaPreparado.PorcMerma, "", 0, flexAlignRightCenter)
    End If
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.Merma, "", 0, flexAlignRightCenter)
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.CostoPreXMerma, "", 0, flexAlignRightCenter)
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.CantOrden, Stellar_Mensaje(2957), 0, flexAlignRightCenter) ' Cant. OPR
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.CantTeorica, Empty, 0, flexAlignRightCenter) ' Cant. Formula
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.Canti, Stellar_Mensaje(3001), 1100, flexAlignCenterCenter) 'cantidad
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.CostoPre, Replace(Stellar_Mensaje(151), ":", ""), 1775, flexAlignCenterCenter) ' Costo Uni.
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.CostoProd, Stellar_Mensaje(2938), 1775, flexAlignCenterCenter) ' Costo Prod.
    
    'para el manejo cuando el ajuste cantibul
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.Cantibul, "cantibul", 0, flexAlignRightCenter)
    
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.CostoOriginal, "CostoOriginal", 0, flexAlignRightCenter)
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.CodigoInput, "CodigoInput", 0, flexAlignRightCenter)
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.DocumentoOrigen, "DocumentoOrigen", 0, flexAlignRightCenter)
    Call MSGridAsign(Preparado, 0, ColumnaPreparado.Info, "Info", _
    IIf(StellarMensaje(555) = "es-VE", 400, 0), IIf(StellarMensaje(555) = "es-VE", flexAlignCenterCenter, flexAlignRightCenter))
    
    Call SetDefMSGrid(Preparado, 1, ColumnaPreparado.Producto)
    
    'Grid de C�lculo de Producci�n
    Producir.Cols = ColumnaProducir.ColCount

    Call MSGridAsign(Producir, 0, ColumnaProducir.LDeci, "LDec", 0, flexAlignRightCenter)
    Call MSGridAsign(Producir, 0, ColumnaProducir.Producto, Stellar_Mensaje(5010), 1700, flexAlignCenterCenter) 'producto
    
    Dim LenColLote As Long
    
    If ManejaLote Then LenColLote = 1200
    
    Call MSGridAsign(Producir, 0, ColumnaProducir.Descripci�n, Stellar_Mensaje(143), 5300 - LenColLote, flexAlignCenterCenter) 'descripcion
    Call MSGridAsign(Producir, 0, ColumnaProducir.CantOrden, Stellar_Mensaje(2957), 0, flexAlignRightCenter) ' Cant. Orden
    Call MSGridAsign(Producir, 0, ColumnaProducir.CantTeorica, Empty, 0, flexAlignRightCenter) ' Cant. Formula
    Call MSGridAsign(Producir, 0, ColumnaProducir.Canti, Stellar_Mensaje(3001), 1100, flexAlignCenterCenter) 'cantidad
    
    ManejaFactor = FrmAppLink.PRDManejaFactor
    
    If ManejaFactor Then
        Call MSGridAsign(Producir, 0, ColumnaProducir.Presentaci�n, PRDLabelCampoPresenta, 1500, flexAlignCenterCenter) 'presentacion
        Call MSGridAsign(Producir, 0, ColumnaProducir.CostoPre, Stellar_Mensaje(151), 1550, flexAlignCenterCenter) ' Costo Uni.
        Call MSGridAsign(Producir, 0, ColumnaProducir.CostoProd, Stellar_Mensaje(2938), 1550, flexAlignCenterCenter) ' Costo Prod.
        Call MSGridAsign(Producir, 0, ColumnaProducir.Factor, Replace(Stellar_Mensaje(221), ":", ""), 880, flexAlignCenterCenter) 'factor
        Call MSGridAsign(Producir, 0, ColumnaProducir.Lote, Stellar_Mensaje(2936), LenColLote, IIf(LenColLote, flexAlignCenterCenter, flexAlignRightCenter)) ' Lote
    Else
        Call MSGridAsign(Producir, 0, ColumnaProducir.Presentaci�n, PRDLabelCampoPresenta, 1780, flexAlignCenterCenter) 'presentacion
        Call MSGridAsign(Producir, 0, ColumnaProducir.CostoPre, Stellar_Mensaje(151), 1850, flexAlignCenterCenter) ' Costo Uni.
        Call MSGridAsign(Producir, 0, ColumnaProducir.CostoProd, Stellar_Mensaje(2938), 1850, flexAlignCenterCenter) ' Costo Prod.
        Call MSGridAsign(Producir, 0, ColumnaProducir.Factor, "", 0, flexAlignRightCenter) 'factor
        Call MSGridAsign(Producir, 0, ColumnaProducir.Lote, Stellar_Mensaje(2936), LenColLote, IIf(LenColLote, flexAlignCenterCenter, flexAlignRightCenter)) 'Lote
    End If
    
    Call MSGridAsign(Producir, 0, ColumnaProducir.Cantibul, "cantibul", 0, flexAlignRightCenter)
    
    Call MSGridAsign(Producir, 0, ColumnaProducir.CostoOriginal, "CostoOriginal", 0, flexAlignRightCenter)
    Call MSGridAsign(Producir, 0, ColumnaProducir.CodigoInput, "CodigoInput", 0, flexAlignRightCenter)
    Call MSGridAsign(Producir, 0, ColumnaProducir.DocumentoOrigen, "DocumentoOrigen", 0, flexAlignRightCenter)
    Call MSGridAsign(Producir, 0, ColumnaProducir.Info, "Info", _
    IIf(StellarMensaje(555) = "es-VE", 400, 0), IIf(StellarMensaje(555) = "es-VE", flexAlignCenterCenter, flexAlignRightCenter))
    
    Call SetDefMSGrid(Producir, 1, ColumnaPreparado.Producto)
    lbl_fecha.Caption = Date 'Format(Now, "short date")
    
    'Muestra de Consecutivo
    Call Nuevo_Consecutivo
    Call Ini_Moneda
    Call Activar_Data(False)
        
    txt_merma.Text = FormatNumber(0, Std_Decm)
    txt_monto.Text = FormatNumber(0, Std_Decm)
    
    ' Carga de Reglas Comerciales para el c�lculo del costo de producci�n.
    
    Ent.RsReglasComerciales.Requery
    
    Select Case Ent.RsReglasComerciales!Estimacion_Inv
        Case 0
            CostoActivo = "n_CostoAct"
        Case 1
            CostoActivo = "n_CostoAnt"
        Case 2
            CostoActivo = "n_CostoPro"
        Case 3
            CostoActivo = "n_CostoRep"
    End Select
    
    If FrmAppLink.PRDCostoActivo <> Empty Then CostoActivo = FrmAppLink.PRDCostoActivo
    
    'Calculo.AddItem "Por Cantidad a Utilizar"
    'Calculo.AddItem "Por Cantidad a Producir"
    'Calculo.ListIndex = 0
    
    CambioPrecioAutomatico = FrmAppLink.PRDCambioPrecioAutomatico
    
    MascaraParaLotes = FrmAppLink.PRDMascaraParaLotes
    
    AnchoColDesc_Prepaparado = Preparado.ColWidth(ColumnaPreparado.Descripci�n)
    AnchoColDesc_Producir = Producir.ColWidth(ColumnaProducir.Descripci�n)
    
    Call AjustarPantalla(Me)
    
    lbl_Organizacion.Caption = FrmAppLink.GetLblOrganizacion
    
    ExecuteSafeSQL "UPDATE MA_ORDEN_PRODUCCION SET Estatus = 'DCO' " & _
    "WHERE Estatus = 'DPE' " & _
    "AND d_FechaVencimiento IS NOT NULL " & _
    "AND CAST(GetDate() AS DATE) > CAST(d_FechaVencimiento AS DATE) ", _
    Ent.BDD
    
    If Find_Concept = "OPR" Then
        txt_destino.Text = Sucursal
        txt_destino_LostFocus
    End If
    
End Sub

Private Sub Form_Activate()
    Screen.MousePointer = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbAltMask Then
        
        Select Case KeyCode
            
            Case vbKeyB
            
            Case vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
                
            Case vbKeyN
                Call Form_KeyDown(vbKeyF6, 0)
            
            Case vbKeyC
                Call Form_KeyDown(vbKeyF7, 0)
            
            Case vbKeyR
                Call Form_KeyDown(vbKeyF8, 0)
            
            Case vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
                
            Case vbKeyA
            
        End Select
        
    Else
        
        Select Case KeyCode
            Case vbKeyF1
                'AYUDA
            
            Case vbKeyF2
                'BUSCAR
                Select Case Who_Ami
                    Case 0
                        If Find_Concept = "PRD" Then
                            BarraO_ButtonMenuClick BarraO.Buttons(1).ButtonMenus("BORD")
                        ElseIf Find_Concept = "OPR" Then
                            BarraO_ButtonMenuClick BarraO.Buttons(1).ButtonMenus("BPRD")
                        End If
                        'CAMPO DONDE ESCRIBIR [LBL_CONSECUTIVO.CAPTION]
                    Case 1
                        If Tecla_Pulsada Then
                            If txt_preparado.Enabled Then
                                txt_preparado.SetFocus
                                Call txt_preparado_KeyDown(vbKeyF2, 0)
                            End If
                        End If
                End Select
            
            Case vbKeyF3
                'AGREGAR
            Case vbKeyF4
                'GRABAR
                BarraO_ButtonMenuClick BarraO.Buttons(3).ButtonMenus("GORD")
            Case vbKeyF5
                'MODIFICAR
            Case vbKeyF6
                'ANULAR
                BarraO_ButtonMenuClick BarraO.Buttons(4).ButtonMenus("AORD")
            Case vbKeyF7
                'CANCELAR
                Call Cancelar
            Case vbKeyF8
                'REIMPRIMIR
                BarraO_ButtonMenuClick BarraO.Buttons(6).ButtonMenus("RORD")
            Case vbKeyF9
                'RESERVADO
            
            Case vbKeyF10
                'RESERVADO
            
            Case vbKeyF12
                'SALIR
                Call Salir
                
        End Select
        
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set FRM_PRODUCCION_ORDEN_MANUAL_2 = Nothing
End Sub

Private Sub txt_cantidad_GotFocus()
    txt_cantidad.SelStart = 0
    txt_cantidad.SelLength = Len(txt_cantidad.Text)
End Sub

Private Sub txt_cantidad_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub txt_cantidad_LostFocus()
    If CheckCad(txt_cantidad, 4, 10, False) And txt_cantidad.Enabled Then
        If CDbl(txt_cantidad.Text) < 0 Then
            'Call Mensaje(True, "Coloque un valor mayor que 0.")
            Mensaje True, StellarMensaje(2815)
            txt_cantidad.Text = "1"
        Else
            Call ReEscribir(CDbl(txt_cantidad.Text))
        End If
    End If
End Sub

Private Sub ModifFlag_Click()
    
    If TmpDisableEvent Then
        TmpDisableEvent = False
        Exit Sub
    End If
    
    If Not (Left(DocumentoRelacion, 4) = "OPR " _
    And Find_Concept = "PRD" _
    And FrmAppLink.GetFindStatus = "DPE") _
    And ModifFlag Then
        
        'PRDNivelTrabajarSinFormula = 11 ' Debug
        
        If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelTrabajarSinFormula Then
            
            'PRDNivelTrabajarSinFormula = 6 ' Debug
            
            Dim FrmAutorizacion: Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
            
            FrmAutorizacion.mNivel = FrmAppLink.PRDNivelTrabajarSinFormula
            FrmAutorizacion.Titulo = StellarMensaje(2967) '"Introduzca las credenciales " & _
            "de un usuario que autorice a trabajar sin " & _
            "base a una formula."
            
            Set FrmAutorizacion.mConexion = Ent.BDD
            
            FrmAutorizacion.Show vbModal
            
            If FrmAutorizacion.mAceptada Then
                
                Autorizante = FrmAutorizacion.mUsuario
                Set FrmAutorizacion = Nothing
                ' Continuar
                
            Else
                
                Set FrmAutorizacion = Nothing
                TmpDisableEvent = True
                ModifFlag.Value = vbUnchecked
                Exit Sub
                
            End If
            
        End If
        
    End If
    
    If ModifFlag Then
        Cancelar True
        'lblCostosDir.Visible = True
        'txt_costosdirec.Visible = True
        'Aplicar_CostoDirecto.Visible = True
        txt_preparado.Enabled = False
        txt_origen.Enabled = True
        txt_destino.Enabled = True
        cmd_origen.Enabled = True
        cmd_destino.Enabled = True
        cmd_preparado.Enabled = False
        lblMulti.Visible = False
        lblVeces.Visible = False
        txt_cantidad.Visible = False
        txt_costosdirec.Enabled = True
        Preparado.Row = 1
    Else
        'lblCostosDir.Visible = False
        'txt_costosdirec.Visible = False
        'Aplicar_CostoDirecto.Visible = False
        txt_preparado.Enabled = True
        txt_origen.Enabled = False
        txt_destino.Enabled = False
        cmd_origen.Enabled = False
        cmd_preparado.Enabled = True
        cmd_destino.Enabled = False
        Cancelar
    End If
    
End Sub

Private Sub VerDetalleUtilizar()
    With Preparado
    If .Col = ColumnaPreparado.Info And .TextMatrix(.Row, ColumnaPreparado.Producto) <> Empty And .TextMatrix(.Row, ColumnaPreparado.Canti) <> Empty Then
        
        Dim FrmMostrarXml: Set FrmMostrarXml = FrmAppLink.GetFrmMostrarXML
        
        FrmMostrarXml.Caption = "Detalles Adicionales."
        
        Dim mTexto, mTotalLn, TotalCant, TotalCantMerma, TotalUnitario
        Dim ContItems
        
        For I = 1 To .Rows - 1
            If .TextMatrix(I, ColumnaPreparado.Producto) <> Empty And .TextMatrix(I, ColumnaPreparado.Canti) <> Empty Then
                ContItems = ContItems + 1
                TotalCant = TotalCant + Round(CDbl(.TextMatrix(I, ColumnaPreparado.Canti)), 8)
                TotalCantMerma = TotalCantMerma + Round((CDbl(.TextMatrix(I, ColumnaPreparado.Canti)) * (CDbl(.TextMatrix(I, ColumnaPreparado.PorcMerma)) / 100#)), IIf(CLng(.TextMatrix(I, ColumnaPreparado.LDeci)) > 3, CLng(.TextMatrix(I, ColumnaPreparado.LDeci)), 3))
                TotalUnitario = TotalUnitario + CDbl(.TextMatrix(I, ColumnaPreparado.CostoPre))
            End If
        Next
        
        mTotalLn = FormatNumber(((CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti))) * CDbl(.TextMatrix(.Row, ColumnaPreparado.CostoPreXMerma))) + CDbl(.TextMatrix(.Row, ColumnaPreparado.CostoProd)), CLng(.TextMatrix(.Row, ColumnaPreparado.LDeci)))
        
        mDifCant = FormatNumber(CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti)) - CDbl(.TextMatrix(.Row, ColumnaPreparado.CantTeorica)), CDbl(.TextMatrix(.Row, ColumnaPreparado.LDeci)))
        If CDbl(.TextMatrix(.Row, ColumnaPreparado.CantTeorica)) = 0 Or CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti)) = 0 Then
            mDifPorc = "100%"
        'ElseIf CDbl(.TextMatrix(.Row, ColumnaPreparado.CantTeorica)) = 0 Or CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti)) = 0 Then
        Else
            mDifPorc = FormatNumber(CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti)) / CDbl(.TextMatrix(.Row, ColumnaPreparado.CantTeorica)) * 100, 2) & "%"
        End If
        
        mTexto = vbNewLine & _
        "** Detalles Linea **" & vbNewLine & vbNewLine & _
        "Fila: " & .Row & vbNewLine & _
        "Codigo: " & .TextMatrix(.Row, ColumnaPreparado.Producto) & vbNewLine & _
        "Descripcion: " & .TextMatrix(.Row, ColumnaPreparado.Descripci�n) & vbNewLine & _
        "Cant. Teorica: " & .TextMatrix(.Row, ColumnaPreparado.CantTeorica) & vbNewLine & _
        "Cantidad: " & .TextMatrix(.Row, ColumnaPreparado.Canti) & vbNewLine & _
        "Variacion Cant.: " & mDifCant & " (" & mDifPorc & ") " & vbNewLine & _
        PRDLabelCampoPresenta & ": " & .TextMatrix(.Row, ColumnaPreparado.Presentaci�n) & vbNewLine & _
        "% Merma: " & .TextMatrix(.Row, ColumnaPreparado.PorcMerma) & vbNewLine & _
        "Costo Merma: " & .TextMatrix(.Row, ColumnaPreparado.CostoPreXMerma) & vbNewLine & _
        "Subtotal Merma: " & CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti)) * CDbl(.TextMatrix(.Row, ColumnaPreparado.CostoPreXMerma)) & vbNewLine & _
        "Costo Uni: " & .TextMatrix(.Row, ColumnaPreparado.CostoPre) & vbNewLine & _
        "Subtotal Costo: " & .TextMatrix(.Row, ColumnaPreparado.CostoProd) & vbNewLine & _
        "Costo Total: " & mTotalLn & vbNewLine & _
        "Cant. Merma: " & FormatNumber(CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti)) * (CDbl(.TextMatrix(.Row, ColumnaPreparado.PorcMerma)) / 100#), IIf(CLng(.TextMatrix(.Row, ColumnaPreparado.LDeci)) > 3, CLng(.TextMatrix(.Row, ColumnaPreparado.LDeci)), 3)) & vbNewLine & _
        "Part. Cant.: " & FormatNumber(CDbl(CDbl(.TextMatrix(.Row, ColumnaPreparado.Canti))) * 100 / TotalCant, 2) & vbNewLine & _
        "Part. Costo Global: " & FormatNumber(CDbl(mTotalLn) * 100 / CDbl(MTotal.Text), 2) & "%" & vbNewLine & _
        "Part. Costo Uni.: " & FormatNumber(CDbl(.TextMatrix(.Row, ColumnaPreparado.CostoPre)) * 100 / TotalUnitario, 2) & "%" & vbNewLine & _
        vbNewLine
        
        mTexto = mTexto & _
        "** Totales **" & vbNewLine & vbNewLine & _
        "Subtotal: " & txt_monto.Text & vbNewLine & _
        "Merma: " & txt_merma.Text & vbNewLine & _
        "Costo Dir: " & txt_costosdirec.Text & vbNewLine & _
        "Total Monto Produccion: " & MTotal.Text & vbNewLine & _
        "Total Cant: " & TotalCant & vbNewLine & _
        "Cant Merma: " & TotalCantMerma & vbNewLine & _
        "Ingredientes: " & ContItems
        
        FrmMostrarXml.TextArea.Text = mTexto
        FrmMostrarXml.Show vbModal
        
        Set FrmMostrarXml = Nothing
        
    End If
    End With
End Sub

Private Sub Preparado_Click()
    If TmpDisableEvent Then Exit Sub
    If Preparado.Col = ColumnaPreparado.Info Then
        VerDetalleUtilizar
        Exit Sub
    End If
    If ModifFlag Then
    Else
        If Preparado.ColSel = ColumnaPreparado.Lote Then
            Preparado_KeyPress vbKeyReturn
        End If
        ' No permitir alterar la formula a menos que se marque el check.
        'If Preparado.ColSel = ColumnaPreparado.Canti _
        'Or Preparado.ColSel = ColumnaPreparado.PorcMerma Then 'MONTAMOS EL TXTEDIT
            'txtedit.Visible = True
            ''txtedit.Left = 5000
            'With Me.Preparado
                'txtedit.Left = .CellLeft + .Left
                'txtedit.Top = .CellTop + .Top
                'txtedit.Height = .CellHeight
                'txtedit.Width = .CellWidth
                'txtedit.Text = Preparado.TextMatrix(.RowSel, .ColSel)
                'SeleccionarTexto txtedit
                'If PuedeObtenerFoco(txtedit) Then txtedit.SetFocus
            'End With
        'End If
    End If
End Sub

Private Sub Preparado_DblClick()
    If ModifFlag Then
        Call Preparado_KeyPress(vbKeyReturn)
    End If
End Sub

Private Sub Preparado_GotFocus()
    If Preparado.Col = 1 And ModifFlag Then
        oTeclado.Key_Down
        Who_Ami = 1
        'BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = True
    Else
        'BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = False
    End If
End Sub

Private Sub MostrarTxt1(Columna As Integer, Fila As Integer)
    With Me.Preparado
        .Col = Columna
        .Row = Fila
        txtedit.Left = .CellLeft + .Left
        txtedit.Top = .CellTop + .Top
        txtedit.Height = .CellHeight
        txtedit.Width = .CellWidth
        txtedit.Text = Preparado.TextMatrix(.RowSel, ColSel)
        txtedit.Text = .TextMatrix(Fila, Columna)
        txtedit.SelStart = 0
        txtedit.SelLength = Len(txtedit.Text)
    End With
    txtedit.Visible = True
    txtedit.SetFocus
End Sub

Private Sub Preparado_KeyDown(KeyCode As Integer, Shift As Integer)
    If ModifFlag Then
        If Shift = vbAltMask Then
        Else
            Select Case KeyCode
                Case Is = vbKeyF2
                    Producto_Imp = ""
                    Tecla_Pulsada = True
                    If Preparado.ColSel = ColumnaPreparado.Producto And ModifFlag Then
                        Call MAKE_VIEW("ma_productos", "c_Codigo", "c_Descri", StellarMensaje(254), Me, "PRODUCTOS_GENERAL", , Campo_Txt, Campo_Lbl)
                        If txtedit <> "" Then Preparado_KeyPress (vbKeyReturn)
                        txtedit.MaxLength = 15
                        txtedit.Text = Mid(Producto_Imp, 2)
                        Call Preparado_KeyPress(vbKeyReturn)
                    End If
                    Tecla_Pulsada = False
                Case Is = vbKeyDelete
                    Call Preparado_KeyPress(vbKeyDelete)
            End Select
        End If
    Else
        ' No permitir alterar la formula a menos que se marque el check.
        'Select Case KeyCode
            'Case Is = vbKeyReturn
                'If Preparado.ColSel = ColumnaPreparado.Canti Then 'MONTAMOS EL TXTEDIT
                    'Call MostrarTxt1(Preparado.Col, Preparado.Row)
                'ElseIf Preparado.ColSel = ColumnaPreparado.PorcMerma Then
                    'Call MostrarTxt1(Preparado.Col, Preparado.Row)
                'End If
        'End Select
    End If
End Sub

Private Sub MostrarTxt2(Columna As Integer, Fila As Integer)
    If ModifFlag Then
        
    Else
        txtEdit2.Visible = True
        txtEdit2.Text = ""
        With Me.Producir
            .Row = Fila
            .Col = Columna
            txtEdit2.Left = .CellLeft + .Left
            txtEdit2.Top = .CellTop + .Top
            txtEdit2.Height = .CellHeight
            txtEdit2.Width = .CellWidth
            txtEdit2.Text = .TextMatrix(Fila, Columna)
            txtEdit2.SelStart = 0
            txtEdit2.SelLength = Len(txtEdit2.Text)
        End With
        txtEdit2.Visible = True
        SafeFocus txtEdit2
    End If
End Sub

Private Sub Preparado_KeyPress(KeyAscii As Integer)
    
    With Preparado
    
    If ModifFlag Then
        
        If Find_Concept = "PRD" And Not FrmAppLink.OPRSinBackOrder_ModificaCantidad And .ColSel = ColumnaPreparado.Canti _
        And Left(.TextMatrix(.Row, ColumnaPreparado.DocumentoOrigen), 4) = "OPR " _
        And EstatusOPR = "DPE" And Not esBackOrder Then
            Mensaje True, StellarMensaje(2958) '"Atenci�n, la Orden de Producci�n no acepta producci�n parcial."
            KeyAscii = 0
            Exit Sub
        End If
        
        Select Case KeyAscii
            
            Case vbKeyReturn
                
                If .ColSel = ColumnaPreparado.Producto _
                Or .ColSel = ColumnaPreparado.Canti _
                Or .ColSel = ColumnaPreparado.PorcMerma _
                Or .ColSel = ColumnaPreparado.Lote Then
                    Select Case .ColSel
                        Case ColumnaPreparado.Producto
                            txtedit.MaxLength = 15
                        Case ColumnaPreparado.Canti
                            If MSGridRecover(Preparado, .Row, ColumnaPreparado.Producto) = "" Then
                                Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Producto)
                                Exit Sub
                            End If
                            txtedit.MaxLength = 0
                        Case ColumnaPreparado.PorcMerma, ColumnaPreparado.Lote
                            If MSGridRecover(Preparado, .Row, ColumnaPreparado.Producto) = "" Then
                                Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Producto)
                                Exit Sub
                            End If
                            If MSGridRecover(Preparado, .Row, ColumnaPreparado.Canti) = "" Then
                                Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Canti)
                                Exit Sub
                            End If
                            txtedit.MaxLength = 0
                    End Select
                    
                    If (.ColSel = ColumnaPreparado.Producto And .RowSel = .Rows - 1) _
                    Or .ColSel = ColumnaPreparado.Canti _
                    Or .ColSel = ColumnaPreparado.PorcMerma _
                    Or .ColSel = ColumnaPreparado.Lote Then
                        
Escribir:
                        
                        If .Text <> "" Then
                            Valor_Temp = .Text
                            txtedit.Text = .Text
                        End If
                        
                        txtedit.Left = .CellLeft + .Left
                        txtedit.Top = .CellTop + .Top
                        txtedit.Height = .CellHeight
                        txtedit.Width = .CellWidth
                        'txtedit.SelStart = 0
                        'txtedit.SelLength = Len(txtedit.Text)
                        txtedit.Visible = True
                        txtedit.Enabled = True
                        SafeFocus txtedit
                        .Enabled = False
                        
                        If .ColSel = ColumnaPreparado.Lote Then
                            ShowTooltip MascaraParaLotes, MaskWidth, 30000, txtedit, 3, _
                            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                        End If
                        
                    Else
                        .Enabled = True
                        .Col = ColumnaPreparado.Producto
                        .Row = .Rows - 1
                        .SetFocus
                    End If
                    
                End If
                
            Case vbKeyEscape
                
                .Enabled = True
                
                'txtedit.Text = Preparado.Text
                txtedit.Visible = False
                If .Enabled And .Visible Then .SetFocus
                
            Case 48 To 57, 106 To 255, vbKeyDecimal
                
                    If .ColSel = ColumnaPreparado.Producto _
                    Or .ColSel = ColumnaPreparado.Canti _
                    Or .ColSel = ColumnaPreparado.PorcMerma _
                    Or .ColSel = ColumnaPreparado.Lote Then
                        
                        Select Case .Col
                            Case ColumnaPreparado.Producto
                                txtedit.MaxLength = 15
                            Case ColumnaPreparado.Canti
                                If MSGridRecover(Preparado, .Row, ColumnaPreparado.Producto) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Producto)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                            Case ColumnaPreparado.PorcMerma, ColumnaPreparado.Lote
                                If MSGridRecover(Preparado, .Row, ColumnaPreparado.Producto) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Producto)
                                    Exit Sub
                                End If
                                If MSGridRecover(Preparado, .Row, ColumnaPreparado.Canti) = "" Then
                                    Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Canti)
                                    Exit Sub
                                End If
                                txtedit.MaxLength = 0
                        End Select
                        
                        Valor_Temp = .Text
                        txtedit.Left = .CellLeft + .Left
                        txtedit.Top = .CellTop + .Top
                        txtedit.Height = .CellHeight
                        txtedit.Width = .CellWidth
                        txtedit.Visible = True
                        txtedit.Text = Chr(KeyAscii)
                        txtedit.SelStart = Len(txtedit.Text)
                        txtedit.SelLength = 0
                        SafeFocus txtedit
                        .Enabled = False
                        
                        If .ColSel = ColumnaPreparado.Lote Then
                            ShowTooltip MascaraParaLotes, MaskWidth, 30000, txtedit, 3, _
                            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                        End If
                        
                    End If
                    
            Case vbKeyDelete
                
                If Find_Concept = "PRD" And Not FrmAppLink.OPRSinBackOrder_ModificaCantidad _
                And Left(.TextMatrix(.Row, ColumnaPreparado.DocumentoOrigen), 4) = "OPR " _
                And EstatusOPR = "DPE" And Not esBackOrder Then
                    Mensaje True, StellarMensaje(2958) '"Atenci�n, la Orden de Producci�n no acepta producci�n parcial."
                    KeyAscii = 0
                    Exit Sub
                End If
                
                If .Rows > 3 Then
                    If .Row < .Rows - 1 Then
                        
                        txt_monto.Text = FormatNumber(CDbl(IIf(Trim(txt_monto.Text) = "", 0, txt_monto.Text)) _
                        - (CDbl(IIf(Trim(MSGridRecover(Preparado, .Row, ColumnaPreparado.CostoPre)) = "", 0, _
                        MSGridRecover(Preparado, .Row, ColumnaPreparado.CostoPre))) * _
                        CDbl(IIf(Trim(MSGridRecover(Preparado, .Row, ColumnaPreparado.Canti)) = "", 0, _
                        MSGridRecover(Preparado, .Row, ColumnaPreparado.Canti)))), Std_Decm)
                        
                        txt_merma.Text = FormatNumber(CDbl(IIf(Trim(txt_merma.Text) = "", 0, txt_merma.Text)) _
                        - (CDbl(IIf(Trim(MSGridRecover(Preparado, .Row, ColumnaPreparado.Merma)) = "", 0, _
                        MSGridRecover(Preparado, .Row, ColumnaPreparado.Merma))) * _
                        CDbl(IIf(Trim(MSGridRecover(Preparado, .Row, ColumnaPreparado.Canti)) = "", 0, _
                        MSGridRecover(Preparado, .Row, ColumnaPreparado.Canti)))), Std_Decm)

                        MTotal.Text = FormatNumber(CDbl(txt_monto.Text) + _
                        CDbl(txt_merma.Text) + CDbl(txt_costosdirec.Text), Std_Decm)
                        
                        Call Cerrar_Recordset(rsProducto)
                        Call Cerrar_Recordset(rsPmoneda)
                        'Call DelFilaGrid(Preparado, Preparado.Row)

                        .RemoveItem .Row
'                        If Preparado.Row < Preparado.Rows - 1 Then
'                            Preparado.Rows = Preparado.Rows - 1
'                        End If
                        
                        Call DistTotal(CDbl(MTotal), Me.Producir)
                        
                    End If
                    
                Else
                    'Call Mensaje(True, "No puede eliminar m�s items porque una f�rmula debe tener por lo menos un ingrediente.")
                    Mensaje True, StellarMensaje(2804)
                End If
                
        End Select
        
    Else
        
        Select Case KeyAscii
            
            Case vbKeyReturn, 48 To 57, 106 To 255, vbKeyDecimal
                
                If .ColSel = ColumnaPreparado.Lote Then
                    
                    If MSGridRecover(Preparado, .Row, ColumnaPreparado.Producto) = "" Then
                        Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Producto)
                        Exit Sub
                    End If
                    
                    If MSGridRecover(Preparado, .Row, ColumnaPreparado.Canti) = "" Then
                        Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Canti)
                        Exit Sub
                    End If
                    
                    txtEdit2.MaxLength = 0
                    
                    GoTo Escribir
                    
                End If
                
        End Select
        
    End If
    
    End With
    
End Sub

Private Sub Preparado_LostFocus()
    If Producir.Enabled And ModifFlag Then
        Producir.Row = Producir.Rows - 1
        Producir.Col = ColumnaProducir.Producto
    End If
End Sub

Private Sub Producir_Click()
    If TmpDisableEvent Then Exit Sub
    If Producir.Col = ColumnaProducir.Info Then
        Exit Sub
    End If
    If ModifFlag Then
    Else
        If Producir.ColSel = ColumnaProducir.Lote Then
            Producir_KeyPress vbKeyReturn
        End If
        ' No permitir alterar la formula a menos que se marque el check.
        'If Producir.ColSel = ColumnaProducir.Factor _
        'Or Producir.ColSel = ColumnaProducir.Canti _
        'Or ColumnaProducir.Lote Then 'MONTAMOS EL TXTEDIT
            'Call MostrarTxt2(Producir.Col, Producir.Row)
        'End If
    End If
End Sub

Private Sub Producir_DblClick()
    If ModifFlag Then
        Call Producir_KeyPress(vbKeyReturn)
    End If
End Sub

Private Sub Producir_KeyPress(KeyAscii As Integer)
    
    With Producir
    
    If ModifFlag Then
        
        If Find_Concept = "PRD" And Not FrmAppLink.OPRSinBackOrder_ModificaCantidad And .ColSel = ColumnaProducir.Canti _
        And Left(.TextMatrix(.Row, ColumnaProducir.DocumentoOrigen), 4) = "OPR " _
        And EstatusOPR = "DPE" And Not esBackOrder Then
            Mensaje True, StellarMensaje(2958) '"Atenci�n, la Orden de Producci�n no acepta producci�n parcial."
            KeyAscii = 0
            Exit Sub
        End If
        
        Select Case KeyAscii
            
            Case vbKeyReturn
                
                If .ColSel = ColumnaProducir.Producto _
                Or .ColSel = ColumnaProducir.Canti _
                Or .ColSel = ColumnaProducir.Factor _
                Or .ColSel = ColumnaProducir.Lote Then
                    
                    Select Case .ColSel
                        
                        Case ColumnaProducir.Producto
                            txtEdit2.MaxLength = 15
                            
                        Case ColumnaProducir.Canti
                            
                            If MSGridRecover(Producir, .Row, ColumnaProducir.Producto) = "" Then
                                Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Producto)
                                Exit Sub
                            End If
                            
                            txtEdit2.MaxLength = 0
                            
                        Case ColumnaProducir.Lote, ColumnaProducir.Factor
                            
                            If MSGridRecover(Producir, .Row, ColumnaProducir.Producto) = "" Then
                                Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Producto)
                                Exit Sub
                            End If
                            
                            If MSGridRecover(Producir, .Row, ColumnaProducir.Canti) = "" Then
                                Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Canti)
                                Exit Sub
                            End If
                            
                            txtEdit2.MaxLength = 0
                            
                    End Select
                    
                    If (.ColSel = ColumnaProducir.Producto And .RowSel = .Rows - 1) _
                    Or .ColSel = ColumnaProducir.Canti _
                    Or .ColSel = ColumnaProducir.Lote _
                    Or .ColSel = ColumnaProducir.Factor Then
                        
Escribir:
                        
                        Valor_Temp = .Text
                        txtEdit2.Text = .Text
                        
                        txtEdit2.Left = .CellLeft + .Left
                        txtEdit2.Top = .CellTop + .Top
                        txtEdit2.Height = .CellHeight
                        txtEdit2.Width = .CellWidth
                        'txtedit2.SelStart = 0
                        'txtedit2.SelLength = Len(txtedit2.Text)
                        txtEdit2.Visible = True
                        txtEdit2.Enabled = True
                        SafeFocus txtEdit2
                        .Enabled = False
                        
                        If .ColSel = ColumnaProducir.Lote Then
                            ShowTooltip MascaraParaLotes, MaskWidth, 30000, txtEdit2, 3, _
                            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                        End If
                        
                    Else
                        .Enabled = True
                        .Col = ColumnaProducir.Producto
                        .Row = Producir.Rows - 1
                        SafeFocus Producir
                    End If
                    
                End If
                
            Case vbKeyEscape
                
                .Enabled = True
                txtEdit2.Visible = False
                If .Enabled And .Visible Then .SetFocus
                
            Case 48 To 57, 106 To 255, vbKeyDecimal
                
                If .ColSel = ColumnaProducir.Producto _
                Or .ColSel = ColumnaProducir.Canti _
                Or .ColSel = ColumnaProducir.Factor _
                Or .ColSel = ColumnaProducir.Lote Then
                    
                    Select Case .Col
                        
                        Case ColumnaProducir.Producto
                            
                            txtEdit2.MaxLength = 15
                            
                        Case ColumnaProducir.Canti
                            
                            If MSGridRecover(Producir, .Row, ColumnaProducir.Producto) = "" Then
                                Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Producto)
                                Exit Sub
                            End If
                            
                            txtEdit2.MaxLength = 0
                            
                        Case ColumnaProducir.Lote, ColumnaProducir.Factor
                            
                            If MSGridRecover(Producir, .Row, ColumnaProducir.Producto) = "" Then
                                Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Producto)
                                Exit Sub
                            End If
                            
                            If MSGridRecover(Producir, .Row, ColumnaProducir.Canti) = "" Then
                                Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Canti)
                                Exit Sub
                            End If
                            
                            txtEdit2.MaxLength = 0
                            
                    End Select
                    
                    Valor_Temp = .Text
                    txtEdit2.Left = .CellLeft + .Left
                    txtEdit2.Top = .CellTop + .Top
                    txtEdit2.Height = .CellHeight
                    txtEdit2.Width = .CellWidth
                    txtEdit2.Visible = True
                    txtEdit2.Text = Chr(KeyAscii)
                    txtEdit2.SelStart = Len(txtEdit2.Text)
                    txtEdit2.SelLength = 0
                    SafeFocus txtEdit2
                    .Enabled = False
                    
                    If .ColSel = ColumnaProducir.Lote Then
                        ShowTooltip MascaraParaLotes, MaskWidth, 30000, txtEdit2, 3, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                    End If
                    
                End If
                
            Case vbKeyDelete
                
                If Find_Concept = "PRD" And Not FrmAppLink.OPRSinBackOrder_ModificaCantidad _
                And Left(.TextMatrix(.Row, ColumnaProducir.DocumentoOrigen), 4) = "OPR " _
                And EstatusOPR = "DPE" And Not esBackOrder Then
                    Mensaje True, StellarMensaje(2958) '"Atenci�n, la Orden de Producci�n no acepta producci�n parcial."
                    KeyAscii = 0
                    Exit Sub
                End If
                
                If .Rows > 3 And .ColSel = ColumnaProducir.Producto Then
                    
                    If .Row < Producir.Rows - 1 Then
                        
                        .RemoveItem .Row
                        
                        Call DistTotal(CDbl(MTotal), Producir)
                        
                    End If
                    
                Else
                    'Call Mensaje(True, "No puede eliminar m�s items porque una f�rmula debe tener por lo menos un ingrediente.")
                    Mensaje True, StellarMensaje(2805)
                End If
                
        End Select
        
    Else
        
        Select Case KeyAscii
            
            Case vbKeyReturn, 48 To 57, 106 To 255, vbKeyDecimal
                
                If .ColSel = ColumnaProducir.Lote Then
                    
                    If MSGridRecover(Producir, .Row, ColumnaProducir.Producto) = "" Then
                        Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Producto)
                        Exit Sub
                    End If
                    
                    If MSGridRecover(Producir, .Row, ColumnaProducir.Canti) = "" Then
                        Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Canti)
                        Exit Sub
                    End If
                    
                    txtEdit2.MaxLength = 0
                    
                    GoTo Escribir
                    
                End If
                
        End Select
        
    End If
    
    End With
    
End Sub

Private Sub Producir_KeyDown(KeyCode As Integer, Shift As Integer)
    If ModifFlag Then
        If Shift = vbAltMask Then
        Else
            Select Case KeyCode
                Case Is = vbKeyF2
                    Tecla_Pulsada = True
                    Producto_Imp = ""
                    If Producir.ColSel = ColumnaProducir.Producto And ModifFlag Then
                        Call MAKE_VIEW("ma_productos", "c_Codigo", "c_Descri", StellarMensaje(254), Me, "PRODUCTOS_GENERAL", , Campo_Txt, Campo_Lbl)
                        If txtEdit2.Text <> "" Then Preparado_KeyPress (13)
                        txtEdit2.MaxLength = 15
                        txtEdit2.Text = Mid(Producto_Imp, 2)
                        Call Producir_KeyPress(vbKeyReturn)
                    End If
                    Tecla_Pulsada = False
                Case Is = vbKeyDelete
                    Call Producir_KeyPress(vbKeyDelete)
            End Select
        End If
    Else
    End If
End Sub

Private Sub Producir_GotFocus()
    If ModifFlag Then
        If Producir.Col = ColumnaProducir.Producto Then
            oTeclado.Key_Down
            Who_Ami = 2
            'BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = True
        Else
            'BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = False
        End If
    End If
End Sub

Private Sub txt_costosdirec_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Aplicar_CostoDirecto_Click
    End If
End Sub

Private Sub txt_destino_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            lastdestino = txt_destino.Text
            SafeFocus txt_destino
            TmpDisableEvent = True
            
            If Find_Concept = "PRD" Then
            
                Set Campo_Txt = txt_destino
                Set Campo_Lbl = lbl_destino
                
                Call Consulta_F2(Me, "GENERICO", Stellar_Mensaje(65), Campo_Txt, Campo_Lbl) 'DEPOSITOS
                
                If lastdestino <> txt_destino.Text Then
                    oTeclado.Key_Tab
                End If
                
            ElseIf Find_Concept = "OPR" Then
            
                Set Forma = Me
                
                Set Campo_Txt = txt_destino
                Set Campo_Lbl = lbl_destino
                
                Call MAKE_VIEW("ma_sucursales", "c_Codigo", "c_Descripcion", _
                UCase(Stellar_Mensaje(1254)), Me, "GENERICO")  '"S U C U R S A L E S"
                
                If lastdestino <> txt_destino.Text Then
                    oTeclado.Key_Tab
                End If
                            
            End If
            
            TS = Now
            TS1 = DateAdd("s", 1, TS)
            
            While TS < TS1
                TS = Now
                DoEvents
            Wend
            
            TmpDisableEvent = False
            
        Case Is = vbKeyReturn
            
            oTeclado.Key_Tab
            
    End Select
    
End Sub

Private Sub txt_destino_LostFocus()
    If Find_Concept = "PRD" Then
        If txt_destino.Text <> "" And Tecla_Pulsada = False Then
            Call Apertura_Recordset(RsEureka)
            RsEureka.Open "select * from ma_deposito " & _
            "where c_CodDeposito = '" & txt_destino & "'", _
            Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
            If Not RsEureka.EOF Then
                lbl_destino.Caption = RsEureka!c_Descripcion
                SafeFocus Preparado
                Preparado.Row = Preparado.Rows - 1
                Preparado.Col = ColumnaPreparado.Canti
            Else
                'Call Mensaje(True, "El c�digo no esta asignado a ning�n dep�sito.")
                Mensaje True, StellarMensaje(16314)
                lcDeposito = ""
                lbl_destino.Caption = ""
                txt_destino.Text = ""
            End If
            Call Cerrar_Recordset(RsEureka)
        Else
            lbl_destino.Caption = ""
        End If
    ElseIf Find_Concept = "OPR" Then
        If txt_destino.Text <> "" And Tecla_Pulsada = False Then
            Call Apertura_Recordset(RsEureka)
            RsEureka.Open "select * from MA_SUCURSALES " & _
            "WHERE c_Codigo = '" & txt_destino & "'", _
            Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
            If Not RsEureka.EOF Then
                lbl_destino.Caption = RsEureka!c_Descripcion
                SafeFocus Preparado
                Preparado.Row = Preparado.Rows - 1
                Preparado.Col = ColumnaPreparado.Canti
            Else
                'Call Mensaje(True, "El c�digo no esta asignado a ning�n dep�sito.")
                Mensaje True, StellarMensaje(16145)
                lcDeposito = ""
                lbl_destino.Caption = ""
                txt_destino.Text = ""
            End If
            Call Cerrar_Recordset(RsEureka)
        Else
            lbl_destino.Caption = ""
        End If
    End If
End Sub

Private Sub txt_ORIGEN_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            lastdeposito = txt_origen.Text
            SafeFocus txt_origen
            TmpDisableEvent = True
            Set Campo_Txt = txt_origen
            Set Campo_Lbl = lbl_origen
            Call Consulta_F2(Me, "GENERICO", Stellar_Mensaje(65), Campo_Txt, Campo_Lbl) 'DEPOSITOS
            If lastdeposito <> txt_origen.Text Then
                oTeclado.Key_Tab
            End If
            TS = Now
            TS1 = DateAdd("s", 1, TS)
            While TS < TS1
                TS = Now
                DoEvents
            Wend
            TmpDisableEvent = False
        Case Is = vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub txt_origen_LostFocus()
    If txt_origen.Text <> "" And Tecla_Pulsada = False Then
        Call Apertura_Recordset(RsEureka)
        RsEureka.Open "select * from ma_deposito " & _
        "where c_CodDeposito = '" & txt_origen & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsEureka.EOF Then
            lbl_origen.Caption = RsEureka!c_Descripcion
        Else
            'Call Mensaje(True, "El c�digo no esta asignado a ning�n dep�sito.")
            Mensaje True, StellarMensaje(16314)
            lcDeposito = ""
            lbl_origen.Caption = ""
            txt_origen.Text = ""
        End If
        Call Cerrar_Recordset(RsEureka)
    Else
        lbl_origen.Caption = ""
    End If
End Sub

Private Sub txt_preparado_GotFocus()
    Who_Ami = 1
    Tecla_Pulsada = False
End Sub

Private Sub txt_preparado_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            If Trim(txt_preparado.Text) <> "" Then
                If Buscar_Formula(txt_preparado.Text) Then
                    
                    Who_Ami = -1
                    Call Activar_Data(True)
                    
                    DocumentoRelacion = txt_preparado.Text
                    ObservacionInicial = StellarMensaje(2948)
                    esBackOrder = False
                    FechaEstimadaPRD = Date
                    FechaVcto = 0
                    Call ListSafeIndexSelection(CboLnP, 0)
                    Call ListSafeIndexSelection(CboTurno, 0)
                    
                Else
                    txt_preparado.Text = ""
                End If
            Else
                txt_descripcion.Text = ""
            End If
            oTeclado.Key_Tab
        Case vbKeyF2
            Tecla_Pulsada = True
            TmpDisableEvent = True
            Set Campo_Txt = txt_preparado
            SafeFocus Campo_Txt
            Set Campo_Lbl = txt_descripcion
            Call MAKE_VIEW("MA_PRODUCCION", "c_Formula", "c_Descripcion", StellarMensaje(2803), Me, "GENERICO", , Campo_Txt, Campo_Lbl)
            If Not Screen.ActiveControl Is Nothing Then
                If Screen.ActiveControl <> Preparado.Name Then
                    oTeclado.Key_Return
                End If
            End If
            TS = Now
            TS1 = DateAdd("s", 1, TS)
            While TS < TS1
                TS = Now
                DoEvents
            Wend
            Tecla_Pulsada = False
            TmpDisableEvent = False
    End Select
End Sub

Sub Nuevo_Consecutivo()
    If Find_Concept = "PRD" Then
        lbl_consecutivo.Caption = Format(No_Consecutivo("produccion_orden", False), "000000000")
    ElseIf Find_Concept = "OPR" Then
        lbl_consecutivo.Caption = Format(No_Consecutivo("Orden_Produccion_Nuevo", False), "000000000")
    End If
End Sub

Sub Ini_Moneda()
    
    Dim RsMoneda As New ADODB.Recordset
    
    RsMoneda.Open "select * from ma_monedas " & _
    "where b_Preferencia = 1", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsMoneda.EOF Then
        dbmoneda.Text = RsMoneda!c_CodMoneda
        lbl_moneda.Caption = RsMoneda!c_Descripcion
        MSK_FACTOR.Text = RsMoneda!n_Factor
        Std_Decm = RsMoneda!n_Decimales
        lbl_simbolo.Caption = RsMoneda!c_Simbolo
    Else
        If Me.Visible Then Unload Me
    End If
    
End Sub

Sub Salir()
    Set FRM_PRODUCCION_ORDEN_MANUAL_2 = Nothing
    Unload Me
End Sub

Sub Activar_Data(FlgBool As Boolean)
    
    'txt_preparado.Enabled = Not FlgBool
    'cmd_preparado.Enabled = Not FlgBool
    
    'Calculo.Enabled = FlgBool
    txt_cantidad.Enabled = FlgBool
    txt_preparado.Enabled = True
    cmd_preparado.Enabled = True
    
    If Find_Concept = "PRD" Then
        txt_destino.Enabled = FlgBool
        cmd_destino.Enabled = FlgBool
        txt_origen.Enabled = FlgBool
        cmd_origen.Enabled = FlgBool
    ElseIf Find_Concept = "OPR" Then
        txt_destino.Enabled = FlgBool
        cmd_destino.Enabled = FlgBool
    End If
    
    'ModifFlag.value = False
    
End Sub

Sub Cancelar( _
Optional ByVal MantenerFormula As Boolean = False _
)
    If MantenerFormula Then
        
        FlgIncoherente = False
        txt_preparado.Text = Empty
        If Not Left(DocumentoRelacion, 4) = "OPR " Then
            txt_descripcion.Text = StellarMensaje(1176)
        End If
        txt_descripcion.Enabled = True
        txt_preparado.Enabled = False
        cmd_preparado.Enabled = False
        
        If Find_Concept = "PRD" Then
            txt_destino.Text = ""
            lbl_destino.Caption = ""
            txt_origen.Text = ""
            lbl_origen.Caption = ""
        End If
        
        Preparado.Col = ColumnaPreparado.Canti
        Producir.Col = ColumnaProducir.Canti
        Preparado.ColSel = Preparado.Col
        Producir.ColSel = Producir.ColSel
        SafeFocus Preparado
        
    Else
        
        ModifFlag.Visible = True
        If ModifFlag Then ModifFlag.Value = vbUnchecked
        Call Ini_Moneda
        FlgIncoherente = False
        txt_preparado.Text = ""
        txt_descripcion.Text = ""
        Call Activar_Data(False)
        
        Preparado.Rows = 1
        Preparado.Rows = 2
        Producir.Rows = 1
        Producir.Rows = 2
        
        txt_cantidad = FormatNumber(1, Std_DecC)
        txt_destino.Text = ""
        lbl_destino.Caption = ""
        txt_origen.Text = ""
        txt_costosdirec = ""
        lbl_origen.Caption = ""
        
        If Find_Concept = "OPR" Then
            txt_destino.Text = Sucursal
            txt_destino_LostFocus
        End If
        
        txt_cantidad.Text = FormatNumber(1, Std_Decm)
        txt_costosdirec.Text = FormatNumber(0, Std_Decm)
        txt_merma.Text = FormatNumber(0, Std_Decm)
        txt_monto.Text = FormatNumber(0, Std_Decm)
        
        MTotal = FormatNumber(0, Std_Decm)
        
        If txt_preparado.Enabled Then
            SafeFocus txt_preparado
        End If
        
        lblMulti.Visible = True
        lblVeces.Visible = True
        txt_cantidad.Visible = True
        
        DocumentoRelacion = Empty
        ObservacionInicial = StellarMensaje(2948)
        esBackOrder = False
        FechaEstimadaPRD = Date
        FechaVcto = 0
        Call ListSafeIndexSelection(CboLnP, 0)
        Call ListSafeIndexSelection(CboTurno, 0)
        EstatusOPR = Empty
        
    End If
    
    Tecla_Pulsada = False
    TmpDisableEvent = False
    
End Sub

Function Buscar_Formula(Formula As String) As Boolean
    
    'BUSCAR PRODUCCION
    Dim Cont As Integer
    Call Cancelar
    Buscar_Formula = False
    Call Apertura_RecordsetC(RsMaProduccion)
    
    RsMaProduccion.Open "SELECT * FROM MA_PRODUCCION " & _
    "WHERE c_Formula = '" & Formula & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    GridLleno = False
    
    If Not RsMaProduccion.EOF Then
        
        'BUSCAR DETALLES Y CARGAR
        txt_preparado.Text = RsMaProduccion!c_Formula
        txt_descripcion.Text = RsMaProduccion!c_Descripcion
        txt_costosdirec.Text = FormatNumber(RsMaProduccion!n_Costodir, Std_Decm)
        
        'MONEDA
        Call Apertura_Recordset(rsMonedas)
        rsMonedas.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda = '" & RsMaProduccion!c_CodMoneda & "' ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not rsMonedas.EOF Then
            lbl_moneda.Caption = rsMonedas!c_Descripcion
            dbmoneda.Text = rsMonedas!c_CodMoneda
            Std_Decm = rsMonedas!n_Decimales
            MSK_FACTOR.Text = FormatNumber(rsMonedas!n_Factor, Std_Decm)
            lbl_simbolo.Caption = rsMonedas!c_Simbolo
        Else
            'Call Mensaje(True, "No se encontr� la moneda de la f�rmula.")
            Mensaje True, StellarMensaje(2806)
            Call Cancelar
            Exit Function
        End If
        
        'A UTILIZAR
        Call Apertura_RecordsetC(RsTrProduccion)
        
        RsTrProduccion.Open "SELECT * FROM TR_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO " & _
        "WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = '0'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        tmonto = 0
        TMerma = 0
        
        If Not RsTrProduccion.EOF Then
            
            Preparado.Rows = RsTrProduccion.RecordCount + 1
            
            For Cont = 1 To Preparado.Rows - 1
                
                If RsTrProduccion!n_Activo = 1 Then
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                    
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.LDeci, RsTrProduccion!Cant_Decimales)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Producto, RsTrProduccion!c_Codigo, , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Descripci�n, RsTrProduccion!c_Descri, , flexAlignLeftCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CantOrden, FormatNumber(0, RsTrProduccion!Cant_Decimales), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CantTeorica, FormatNumber(0, RsTrProduccion!Cant_Decimales), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Canti, FormatNumber(RsTrProduccion!n_Cantidad, RsTrProduccion!Cant_Decimales), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Presentaci�n, RsTrProduccion!c_Presenta, , flexAlignLeftCenter)
                    
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Lote, Empty, , flexAlignRightCenter)
                    
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(0, Std_Decm), , flexAlignRightCenter)
                    
                    If ManejaMermaExplicita Then
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(RsTrProduccion!n_Merma, Std_Decm), , flexAlignRightCenter)
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber((RsTrProduccion!n_Merma * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100, Std_Decm), , flexAlignRightCenter)
                    Else
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(0, Std_Decm), , flexAlignRightCenter)
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber(0, Std_Decm), , flexAlignRightCenter)
                    End If
                    
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.PorcMerma, MSGridRecover(Preparado, Cont, ColumnaPreparado.Merma))
                    
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPre, FormatNumber(RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)), Std_Decm), , flexAlignRightCenter)
                    
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber((RsTrProduccion!n_Cantidad * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))), Std_Decm), , flexAlignRightCenter)
                    
                    ' 29/09/2004 para el nmanejo de cantibul
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Cantibul, RsTrProduccion!n_Cantibul, , flexAlignRightCenter)
                    
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoOriginal, MSGridRecover(Preparado, Cont, ColumnaPreparado.CostoPre), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CodigoInput, MSGridRecover(Preparado, Cont, ColumnaPreparado.Producto), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.DocumentoOrigen, Empty, , flexAlignRightCenter)
                    
                    Preparado.TextMatrix(Cont, ColumnaPreparado.Info) = "(+)"
                    
                    'tmonto = tmonto + (RsTrProduccion!n_Cantidad * ((RsTrProduccion!n_CostoAct * RsTrProduccion!moneda_act) / CDbl(MSK_FACTOR.Text)))
                    tmonto = tmonto + (RsTrProduccion!n_Cantidad * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))))
                    
                    If ManejaMermaExplicita Then
                        TMerma = TMerma + (((RsTrProduccion!n_Merma * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100) * RsTrProduccion!n_Cantidad)
                    Else
                        TMerma = TMerma + (((0 * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100) * RsTrProduccion!n_Cantidad)
                    End If
                    
                    RsTrProduccion.MoveNext
                    txt_merma.Text = FormatNumber(TMerma, Std_Decm)
                    txt_monto.Text = FormatNumber(tmonto, Std_Decm)
                    MTotal.Text = FormatNumber(CDbl(txt_costosdirec.Text) + TMerma + tmonto, Std_Decm)
                    
                Else
                    'Call Mensaje(True, "La f�rmula que desea utilizar '" & Formula & "' posee un producto inactivo.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    Buscar_Formula = False
                    FlgIncoherente = False
                    Exit Function
                End If
            Next Cont
            
        Else
            'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Function
        End If
        
        'A PRODUCIR
        Call Apertura_RecordsetC(RsProducir)
        
        RsProducir.Open "SELECT * FROM TR_PRODUCCION " & _
        "LEFT JOIN MA_PRODUCTOS " & _
        "ON MA_PRODUCTOS.c_Codigo = TR_PRODUCCION.C_CODPRODUCTO " & _
        "LEFT JOIN MA_MONEDAS " & _
        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
        "WHERE C_FORMULA = '" & Formula & "' AND B_PRODUCIR = '1'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        pMonto = 0
        
        If Not RsProducir.EOF Then
            
            Producir.Rows = RsProducir.RecordCount + 1
            
            For Cont = 1 To Producir.Rows - 1
                
                If RsProducir!n_Activo = 1 Then
                    
                    FrmAppLink.MonedaProd.BuscarMonedas , RsProducir!c_CodMoneda
                    
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.LDeci, RsProducir!Cant_Decimales)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Producto, RsProducir!c_Codigo, , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Descripci�n, RsProducir!c_Descri, , flexAlignLeftCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CantOrden, FormatNumber(0, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CantTeorica, FormatNumber(0, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Canti, FormatNumber(RsProducir!n_Cantidad, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Presentaci�n, RsProducir!c_Presenta, , flexAlignLeftCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Lote, Empty, , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoPre, FormatNumber((RsProducir!n_Costo) / RsProducir!n_Cantidad, Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoProd, FormatNumber((RsProducir!n_Costo), Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Factor, FormatNumber(RsProducir!nu_FactorCosto, 2), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Cantibul, CStr(RsProducir!n_Cantibul), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoOriginal, FormatNumber(RsProducir.Fields(CostoActivo) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)), Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CodigoInput, RsProducir!c_Codigo, , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.DocumentoOrigen, Empty, , flexAlignRightCenter)
                    Producir.TextMatrix(Cont, ColumnaProducir.Info) = "(+)"
                    
                    pMonto = pMonto + ((RsProducir!n_Costo * RsProducir!nu_FactorCosto))
                    
                    RsProducir.MoveNext
                    
                Else
                    'Call Mensaje(True, "La f�rmula que desea utilizar '" & Formula & "' posee un producto inactivo.")
                    Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                    Call Cancelar
                    Buscar_Formula = False
                    FlgIncoherente = False
                    Exit Function
                End If
            Next Cont
            
            GridLleno = True
            Buscar_Formula = True
            
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Function
        End If
        
        Call DistTotal(CDbl(MTotal.Text), Producir)
        
        txt_costosdirec.Enabled = False
        Preparado.Enabled = True
        Producir.Enabled = True
        
    Else
        'Call Mensaje(True, "No se encontr� la Formula N� " & Formula)
        Mensaje True, Replace(StellarMensaje(2808), "$(Code)", Formula)
    End If
    
End Function

Sub ReEscribir(Multiplex As Double)
    
    Dim Cont As Integer
    
    On Error GoTo ReWriteFail
    
    tmonto = 0
    TMerma = 0
    
    If Left(DocumentoRelacion, 4) = "OPR " Then
    
    If RsMaProduccion.State = adStateOpen _
    And RsTrProduccion.State = adStateOpen _
    And RsProducir.State = adStateOpen Then
        
        RsMaProduccion.MoveFirst
        RsTrProduccion.MoveFirst
        RsProducir.MoveFirst
        
        If Not RsTrProduccion.EOF Then
            
            Preparado.Rows = RsTrProduccion.RecordCount + 1
            
            For Cont = 1 To Preparado.Rows - 1
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.LDeci, RsTrProduccion!Cant_Decimales)
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Producto, RsTrProduccion!c_Codigo, , flexAlignRightCenter)
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Descripci�n, RsTrProduccion!c_Descri, , flexAlignLeftCenter)
                
                FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Canti, FormatNumber(RsTrProduccion!n_Cantidad * Multiplex, CDbl(MSGridRecover(Preparado, Cont, ColumnaPreparado.LDeci))), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Canti, FormatNumber(RsTrProduccion!n_Cantidad, CDbl(MSGridRecover(Preparado, Cont, ColumnaPreparado.LDeci))), , flexAlignRightCenter)
                End If
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Presentaci�n, RsTrProduccion!c_Presenta, , flexAlignLeftCenter)
                
                If ManejaMermaExplicita Then
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(RsTrProduccion!n_PorcMerma, Std_DecC), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber(((RsTrProduccion!n_PorcMerma * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100), Std_Decm), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(0, Std_DecC), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber(0, Std_Decm), , flexAlignRightCenter)
                End If
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPre, FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber(((RsTrProduccion!n_Cantidad * RsTrProduccion.Fields(CostoActivo).Value) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber(((RsTrProduccion!n_Cantidad * Multiplex * RsTrProduccion.Fields(CostoActivo).Value) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                    tmonto = tmonto + ((RsTrProduccion!n_Cantidad * Multiplex) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))))
                Else
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber(((RsTrProduccion!n_Cantidad * RsTrProduccion.Fields(CostoActivo).Value) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                    tmonto = tmonto + ((RsTrProduccion!n_Cantidad) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))))
                End If
                
                If Multiplex > 0 Then
                    
                    If ManejaMermaExplicita Then
                        TMerma = TMerma + ((((RsTrProduccion!n_PorcMerma * Multiplex) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100) * (RsTrProduccion!n_Cantidad))
                    End If
                    
                Else
                    
                    If ManejaMermaExplicita Then
                        TMerma = TMerma + ((((RsTrProduccion!n_PorcMerma) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100) * (RsTrProduccion!n_Cantidad))
                    End If
                    
                End If
                
                RsTrProduccion.MoveNext
                
            Next Cont
            
            txt_merma.Text = FormatNumber(TMerma, Std_Decm)
            txt_monto.Text = FormatNumber(tmonto, Std_Decm)
            
            If Multiplex > 0 Then
                MTotal.Text = FormatNumber((CDbl(txt_costosdirec.Text) * Multiplex) + tmonto + CDbl(txt_merma), Std_Decm)
            Else
                MTotal.Text = FormatNumber(tmonto + CDbl(txt_merma), Std_Decm)
            End If
            
        Else
            'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
        
        'A PRODUCIR
        If Not RsProducir.EOF Then
            
            Producir.Rows = RsProducir.RecordCount + 1
            
            For Cont = 1 To Producir.Rows - 1
                
                Call MSGridAsign(Producir, Cont, ColumnaProducir.LDeci, RsProducir!Cant_Decimales)
                Call MSGridAsign(Producir, Cont, ColumnaProducir.Producto, RsProducir!c_Codigo, , flexAlignLeftCenter)
                Call MSGridAsign(Producir, Cont, ColumnaProducir.Descripci�n, RsProducir!c_Descri, , flexAlignLeftCenter)
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Canti, FormatNumber(RsProducir!n_Cantidad * Multiplex, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Canti, FormatNumber(RsProducir!n_Cantidad, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                End If
                
                Call MSGridAsign(Producir, Cont, ColumnaProducir.Presentaci�n, RsProducir!c_Presenta, , flexAlignLeftCenter)
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoPre, FormatNumber(((RsProducir!n_Costo * Multiplex / RsProducir!n_Cantidad) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoProd, FormatNumber(((RsProducir!n_Costo * Multiplex) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoPre, FormatNumber(((RsProducir!n_Costo / RsProducir!n_Cantidad) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoProd, FormatNumber(((RsProducir!n_Costo) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                End If
                
                RsProducir.MoveNext
                
            Next Cont
            
            Call DistTotal(CDbl(MTotal.Text), Producir)
            GridLleno = True
            
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
    End If
    
    Else
    
    If RsMaProduccion.State = adStateOpen _
    And RsTrProduccion.State = adStateOpen _
    And RsProducir.State = adStateOpen Then
        
        RsMaProduccion.MoveFirst
        RsTrProduccion.MoveFirst
        RsProducir.MoveFirst
        
        If Not RsTrProduccion.EOF Then
            
            Preparado.Rows = RsTrProduccion.RecordCount + 1
            
            For Cont = 1 To Preparado.Rows - 1
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.LDeci, RsTrProduccion!Cant_Decimales)
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Producto, RsTrProduccion!c_Codigo, , flexAlignRightCenter)
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Descripci�n, RsTrProduccion!c_Descri, , flexAlignLeftCenter)
                
                FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Canti, FormatNumber(RsTrProduccion!n_Cantidad * Multiplex, CDbl(MSGridRecover(Preparado, Cont, ColumnaPreparado.LDeci))), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Canti, FormatNumber(RsTrProduccion!n_Cantidad, CDbl(MSGridRecover(Preparado, Cont, ColumnaPreparado.LDeci))), , flexAlignRightCenter)
                End If
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Presentaci�n, RsTrProduccion!c_Presenta, , flexAlignLeftCenter)
                
                If ManejaMermaExplicita Then
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(RsTrProduccion!n_Merma, Std_DecC), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber(((RsTrProduccion!n_Merma * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100), Std_Decm), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(0, Std_DecC), , flexAlignRightCenter)
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber(0, Std_Decm), , flexAlignRightCenter)
                End If
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPre, FormatNumber((RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                
                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber(((RsTrProduccion!n_Cantidad * RsTrProduccion.Fields(CostoActivo).Value) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber(((RsTrProduccion!n_Cantidad * Multiplex * RsTrProduccion.Fields(CostoActivo).Value) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                    tmonto = tmonto + ((RsTrProduccion!n_Cantidad * Multiplex) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))))
                Else
                    Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber(((RsTrProduccion!n_Cantidad * RsTrProduccion.Fields(CostoActivo).Value) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))), Std_Decm), , flexAlignRightCenter)
                    tmonto = tmonto + ((RsTrProduccion!n_Cantidad) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))))
                End If
                
                If Multiplex > 0 Then
                    
                    If ManejaMermaExplicita Then
                        TMerma = TMerma + ((((RsTrProduccion!n_Merma * Multiplex) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100) * (RsTrProduccion!n_Cantidad))
                    End If
                    
                Else
                    
                    If ManejaMermaExplicita Then
                        TMerma = TMerma + ((((RsTrProduccion!n_Merma) * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100) * (RsTrProduccion!n_Cantidad))
                    End If
                    
                End If
                
                RsTrProduccion.MoveNext
                
            Next Cont
            
            txt_merma.Text = FormatNumber(TMerma, Std_Decm)
            txt_monto.Text = FormatNumber(tmonto, Std_Decm)
            
            If Multiplex > 0 Then
                MTotal.Text = FormatNumber((CDbl(txt_costosdirec.Text) * Multiplex) + tmonto + CDbl(txt_merma), Std_Decm)
            Else
                MTotal.Text = FormatNumber(tmonto + CDbl(txt_merma), Std_Decm)
            End If
            
        Else
            'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
        
        'A PRODUCIR
        If Not RsProducir.EOF Then
            
            Producir.Rows = RsProducir.RecordCount + 1
            
            For Cont = 1 To Producir.Rows - 1
                
                Call MSGridAsign(Producir, Cont, ColumnaProducir.LDeci, RsProducir!Cant_Decimales)
                Call MSGridAsign(Producir, Cont, ColumnaProducir.Producto, RsProducir!c_Codigo, , flexAlignLeftCenter)
                Call MSGridAsign(Producir, Cont, ColumnaProducir.Descripci�n, RsProducir!c_Descri, , flexAlignLeftCenter)
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Canti, FormatNumber(RsProducir!n_Cantidad * Multiplex, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.Canti, FormatNumber(RsProducir!n_Cantidad, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                End If
                
                Call MSGridAsign(Producir, Cont, ColumnaProducir.Presentaci�n, RsProducir!c_Presenta, , flexAlignLeftCenter)
                
                If Multiplex > 0 Then
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoPre, FormatNumber(((RsProducir!n_Costo * Multiplex / RsProducir!n_Cantidad) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoProd, FormatNumber(((RsProducir!n_Costo * Multiplex) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                Else
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoPre, FormatNumber(((RsProducir!n_Costo / RsProducir!n_Cantidad) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                    Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoProd, FormatNumber(((RsProducir!n_Costo) * RsProducir!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm), , flexAlignRightCenter)
                End If
                
                RsProducir.MoveNext
                
            Next Cont
            
            If Multiplex > 0 Then
                txt_costosdirec.Text = FormatNumber(CDbl(txt_costosdirec.Text) * Multiplex, Std_Decm)
            End If
            
            Call DistTotal(CDbl(MTotal.Text), Producir)
            GridLleno = True
            
        Else
            'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
            Mensaje True, StellarMensaje(359)
            Call Cancelar
            Exit Sub
        End If
    End If
    
    End If
    
    Exit Sub
    
ReWriteFail:
    
    'Call Mensaje(True, "Error Fatal, Recordset Fallaron al leer y Reescribir.")
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    Call Salir
    
End Sub

Sub Cambiar_Moneda()
    
    Dim MActual As String, MActDec As Integer, mDesc As String, MSimbolo As String, mFACTOR As Double
    Dim RsMoneda As New ADODB.Recordset
    
    MActual = dbmoneda.Text
    mDesc = lbl_moneda.Caption
    MActDec = Std_Decm
    MSimbolo = lbl_simbolo.Caption
    mFACTOR = CDbl(MSK_FACTOR.Text)
    
    Call Consulta_F2(Me, "MONEDA_GENERICA", "MONEDAS")
    CodMoneda = FrmAppLink.GetCodMonedaSel
    
    If CodMoneda <> "" Then
        If Buscar_Moneda(dbmoneda, lbl_moneda, MSK_FACTOR, False, CodMoneda, True, True) = False Then
            'Call Mensaje(True, "No existe una moneda predeterminada en el sistema.")
            Mensaje True, StellarMensaje(16289)
            Unload Me
            Exit Sub
        End If
    End If
    
    If MActual <> dbmoneda.Text Then
        'MsgBox "Cambio la moneda, recuerde colocar el cambio de moneda."
        '
        'ESCRIBIR PROCEDIMIENTOS DE RECALCULAR GRIDS
        '
        Call Apertura_RecordsetC(RsMoneda)
        RsMoneda.Open "select * from ma_monedas where c_CodMoneda = '" & dbmoneda & "' ", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsMoneda.EOF Then
            lbl_simbolo.Caption = RsMoneda!c_Simbolo
        Else
            lbl_simbolo.Caption = "Unk"
        End If
        Std_Decm = RsMoneda!n_Decimales
    End If
    
End Sub

Sub Grabar_Produccion(Tipo As String)
    
    Dim RsEureka As New ADODB.Recordset, RsInventarioCargar As New ADODB.Recordset, _
    RsInventarioDescargar As New ADODB.Recordset
    
    Dim RsDepositoCargo As New ADODB.Recordset, RsDepositoDescargo As New ADODB.Recordset, _
    RsMerma As New ADODB.Recordset, ConsecutivoAjuste As String
    
    Dim ActiveTrans As Boolean
    
    'On Error GoTo RecordFail
    'VALIDAR DATOS
    If CDbl(txt_monto.Text) < 0 Then
        'Call Mensaje(True, "El monto de producci�n no puede ser negativo.")
        Mensaje True, StellarMensaje(2809)
        Exit Sub
    ElseIf CDbl(txt_monto.Text) = 0 Then
        'Call Mensaje(True, "No hay datos suficientes para grabar." & vbNewLine & "Verifique que existen productos para la realizar la producci�n.")
        Mensaje True, Replace(StellarMensaje(2810), "$(Line)", vbNewLine)
        Exit Sub
    End If
    
    If Producir.TextMatrix(1, ColumnaProducir.Producto) = Empty _
    Or Len(Producir.TextMatrix(1, ColumnaProducir.Producto)) <= 0 Then
        'Call Mensaje(True, "Faltan datos de los Productos a producir.")
        Mensaje True, StellarMensaje(359)
        Exit Sub
    End If
      
    If Trim(txt_origen.Text) = "" Then
        'Call Mensaje(True, "Debe colocar el dep�sito Origen, de donde se tomar�n los productos a utilizar.")
        Mensaje True, StellarMensaje(11009)
        Exit Sub
    End If
    
    If FrmAppLink.PRDRequiereLineaDeProduccion Then
        If Trim(CboLnP.Text) = Empty _
        Or UCase(Trim(CboLnP.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2964)
            SafeFocus CboLnP
            Exit Sub
        End If
    End If
    
    If FrmAppLink.PRDRequiereTurno Then
        If Trim(CboTurno.Text) = Empty _
        Or UCase(Trim(CboTurno.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2965)
            SafeFocus CboTurno
            Exit Sub
        End If
    End If
    
    Dim Cont As Integer
    
    For Cont = 1 To Preparado.Rows - 1
        If Preparado.TextMatrix(Cont, ColumnaPreparado.Producto) = Empty _
        And Preparado.TextMatrix(Cont, ColumnaPreparado.Canti) <> Empty Then
            Call Mensaje(True, Stellar_Mensaje(16165)) '"El C�digo del Producto no Existe.")
            Exit Sub
        End If
    Next
    
    For Cont = 1 To Producir.Rows - 1
        If Producir.TextMatrix(Cont, ColumnaProducir.Producto) = Empty _
        And Producir.TextMatrix(Cont, ColumnaProducir.Canti) <> Empty Then
            Call Mensaje(True, Stellar_Mensaje(16165)) '"El C�digo del Producto no Existe.")
            Exit Sub
        End If
    Next
    
    If ManejaLote _
    And Trim(MascaraParaLotes) <> Empty Then
        
        ' Si tiene una mascara o es "*" no tiene formato especifico _
        pero igual es requerido.
        
        For Cont = 1 To Preparado.Rows - 1
            If Preparado.TextMatrix(Cont, ColumnaPreparado.Producto) <> Empty _
            And Trim(Preparado.TextMatrix(Cont, ColumnaPreparado.Lote)) = Empty Then
                Mensaje True, Replace(StellarMensaje(2640), "$(RowNum)", Cont)
                Preparado.Row = Cont
                Preparado.Col = ColumnaPreparado.Lote
                SafeFocus Preparado
                Exit Sub
            End If
        Next
        
        For Cont = 1 To Producir.Rows - 1
            If Producir.TextMatrix(Cont, ColumnaProducir.Producto) <> Empty _
            And Trim(Producir.TextMatrix(Cont, ColumnaProducir.Lote)) = Empty Then
                Mensaje True, Replace(StellarMensaje(2640), "$(RowNum)", Cont)
                Producir.Row = Cont
                Producir.Col = ColumnaProducir.Lote
                SafeFocus Producir
                Exit Sub
            End If
        Next
        
    End If
    
    If txt_origen.Text <> "" Then
        
        Call Apertura_Recordset(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_DEPOSITO " & _
        "WHERE c_CodDeposito = '" & txt_origen & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsEureka.EOF Then
            lbl_origen.Caption = RsEureka!c_Descripcion
            'txtEdit.SetFocus
        Else
            'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
            Mensaje True, StellarMensaje(16189)
            lcDeposito = ""
            lbl_origen.Caption = ""
            txt_origen.Text = ""
            Exit Sub
        End If
        
        Call Cerrar_Recordset(RsEureka)
        
    Else
        'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
        Mensaje True, StellarMensaje(16189)
        lbl_origen.Caption = ""
        Exit Sub
    End If
    
    If Trim(txt_destino.Text) = "" Then
        'Call Mensaje(True, "Debe colocar el dep�sito Destino, donde se almacenar�n los productos a producir.")
        Mensaje True, StellarMensaje(16554)
        Exit Sub
    End If
    
    If txt_destino.Text <> "" Then
        
        Call Apertura_Recordset(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_DEPOSITO " & _
        "WHERE c_CodDeposito = '" & txt_destino & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsEureka.EOF Then
            lbl_destino.Caption = RsEureka!c_Descripcion
        Else
            'Call Mensaje(True, "El c�digo del dep�sito Destino no es Valido.")
            Mensaje True, StellarMensaje(16189)
            lcDeposito = ""
            lbl_destino.Caption = ""
            txt_destino.Text = ""
            Exit Sub
        End If
        
        Call Cerrar_Recordset(RsEureka)
        
    Else
        'Call Mensaje(True, "El c�digo del dep�sito Destino no es Valido.")
        Mensaje True, StellarMensaje(16189)
        lbl_destino.Caption = ""
        Exit Sub
    End If
    
    If Tipo = "GESP" Then
        If ModifFlag.Value = vbChecked Then
            'Mensaje True, "Para guardar en Espera debe trabajar en base a una formula."
            Mensaje True, StellarMensaje(2799)
            Exit Sub
        End If
    End If
    
    If Not (Left(DocumentoRelacion, 4) = "OPR " _
    And Find_Concept = "PRD" _
    And FrmAppLink.GetFindStatus = "DPE") _
    Then
        
        'PRDNivelProduccionSinOPR = 11 ' Debug
        
        If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelProduccionSinOPR Then
            
            Dim FrmAutorizacion: Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
            
            'PRDNivelProduccionSinOPR = 6 ' Debug
            
            FrmAutorizacion.mNivel = FrmAppLink.PRDNivelProduccionSinOPR
            FrmAutorizacion.Titulo = StellarMensaje(2968) ' "Introduzca las credenciales " & _
            "de un usuario que autorice la producci�n " & _
            "sin orden de producci�n asociada."
            
            Set FrmAutorizacion.mConexion = Ent.BDD
            
            FrmAutorizacion.Show vbModal
            
            If FrmAutorizacion.mAceptada Then
                
                Autorizante = FrmAutorizacion.mUsuario
                Set FrmAutorizacion = Nothing
                ' Continuar
                
            Else
                
                Set FrmAutorizacion = Nothing
                Mensaje True, StellarMensaje(16050)
                Exit Sub
                
            End If
            
        End If
        
    End If
    
    If Tipo = "GORD" Then
        
        If CambioPrecioAutomatico = 0 _
        Or CambioPrecioAutomatico = 2 Then
            
            For Cont = 1 To Producir.Rows - 1
                
                Dim mCostoNew As Double, mCostoOrig As Double, _
                mVerPrecios As Boolean, Visualizar As Integer
                
                mCostoNew = Round((( _
                CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoProd)) / _
                CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Canti))) _
                ), Std_Decm)
                
                mCostoOrig = CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoOriginal))
                
                If mCostoOrig <> mCostoNew Then
                    
                    Visualizar = 0
                    
                    '"Existe una variaci�n en el costo del Producto [$(Code)][$(Desc)]" & _
                    "�Desea validar los precios de venta?"
                    Mensaje False, Replace(Replace(StellarMensaje(2939), "$(Code)", Producir.TextMatrix(Cont, ColumnaProducir.Producto)), _
                    "$(Desc)", Producir.TextMatrix(Cont, ColumnaProducir.Descripci�n))
                    
                    If Retorno Then
                        Visualizar = 1
                    Else
                        Visualizar = -1
                    End If
                    
                    If Visualizar = 1 Then
                        
                        Dim Newfrm_Orden_Reposicion: Set Newfrm_Orden_Reposicion = FrmAppLink.GetNewFrmOrdenReposicion
                        
                        FrmAppLink.MonedaDoc.BuscarMonedas , dbmoneda.Text
                        Newfrm_Orden_Reposicion.ReadOnly = True
                        Newfrm_Orden_Reposicion.CampoCostoComparar = CostoActivo
                        Newfrm_Orden_Reposicion.Codigo = Producir.TextMatrix(Cont, ColumnaProducir.Producto)
                        
                        unitario = mCostoNew
                        
                        If CambioPrecioAutomatico = 2 Then
                            Newfrm_Orden_Reposicion.SoloAceptar = True
                        End If
                        
                        Newfrm_Orden_Reposicion.Reload
                        Newfrm_Orden_Reposicion.Show vbModal
                        
                        If CambioPrecioAutomatico = 0 Then
                            If Not Ingresar Then Exit Sub
                        End If
                        
                    End If
                    
                End If
                
            Next
        
        End If
        
        Dim Frm_Totalizar: Set Frm_Totalizar = FrmAppLink.GetFrmTotalizar
        
        Frm_Totalizar.lbl_impuesto.Caption = FormatNumber(0)
        Frm_Totalizar.lbl_subtotal.Caption = txt_monto.Text
        Frm_Totalizar.msk_bsdesc.Text = FormatNumber(0)
        Frm_Totalizar.lbl_total.Caption = MTotal.Text
        Frm_Totalizar.txtImpuesto = FormatNumber(0)
        
        Frm_Totalizar.ManejaObservacion = True
        Frm_Totalizar.Obsevacion_SoloLectura = False
        Frm_Totalizar.ObservacionInicial = ObservacionInicial
        Frm_Totalizar.ManejaOtrosCargos = True
        Frm_Totalizar.RecalcularOtrosCargos = False
        Frm_Totalizar.MostrarPorcentajeOtrosCargos = False
        Frm_Totalizar.txtOtrosCargos.Text = FormatNumber(CDbl(txt_costosdirec.Text), Std_Decm)
        Frm_Totalizar.Show vbModal
        
        If FrmAppLink.GetLcGrabar = False Then
            Set Frm_Totalizar = Nothing
            Exit Sub
        End If
        
        mFechaProduccion = Frm_Totalizar.fecha_recepcion
        
        If Frm_Totalizar.chk_vencimiento = vbChecked Then
            mFechaVcto = CDbl(Frm_Totalizar.dtp_odc_vence)
        Else
            mFechaVcto = 0
        End If
        
        ObservacionInicial = Frm_Totalizar.txtObs.Text
        
        Set Frm_Totalizar = Nothing
        
    End If
    
    Ent.BDD.BeginTrans: ActiveTrans = True
    
    ConsecutivoAjuste = ""
    
    Dim RowIsClean As Boolean
    
    For I = 1 To Preparado.Rows - 1
        RowIsClean = True
        For k = 0 To Preparado.Cols - 1
            If Preparado.TextMatrix(I, k) <> "" Then
                RowIsClean = False
                Exit For
            End If
        Next k
        If (RowIsClean And I > 1) Then Preparado.RemoveItem I
    Next I
    
    For I = 1 To Producir.Rows - 1
        RowIsClean = True
        For k = 0 To Producir.Cols - 1
            If Producir.TextMatrix(I, k) <> "" Then
                RowIsClean = False
                Exit For
            End If
        Next k
        If (RowIsClean And I > 1) Then Producir.RemoveItem I
    Next I
    
    If Tipo = "GORD" Then
        
        'GRABAR ORDEN
        
        lbl_consecutivo.Caption = Format(No_Consecutivo("PRODUCCION_ORDEN"), "000000000")
        
        If CDbl(txt_merma.Text) > 0 Then
            ConsecutivoAjuste = Format(No_Consecutivo("AJUSTES"), "000000000")
        End If
        
        Call Apertura_RecordsetC(RsEureka)
        RsEureka.Open "SELECT * FROM MA_INVENTARIO " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Concepto = 'PRD' AND c_Status = 'DCO' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsEureka.EOF Then
            
            RsEureka.AddNew
            
            'ACTUALIZAR EL CABECERO DE INVENTARIO CON LOS DATOS DE LA FORMA
            Call UpdateHeader(RsEureka, "DCO", ConsecutivoAjuste)
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A UTILIZAR (DESCARGO)
            Call Apertura_RecordsetC(RsInventarioDescargar)
            
            RsInventarioDescargar.Open "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND c_Concepto = 'PRD' AND c_TipoMov = 'Descargo'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioDescargar.EOF Then
                Call UpdateUtilizar(RsInventarioDescargar, "Descargo", Tipo)
            Else
                Call Mensaje(True, "Existe un detalle de Descargo con el documento N� '" & lbl_consecutivo.Caption & "' ")
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
            'ACTUALIZAR EL DEPOSITO CON LOS PRODUCTOS A UTILIZAR
            Call UpDateDepositoUtilizar(RsDepositoDescargo)
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A PRODUCIR (CARGO)
            Call Apertura_RecordsetC(RsInventarioCargar)
            
            RsInventarioCargar.Open "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND c_Concepto = 'PRD' AND c_TipoMov = 'Cargo'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioCargar.EOF Then
                Call UpdateProducir(RsInventarioCargar, "Cargo", Tipo)
            Else
                Call Mensaje(True, "Existe un detalle de Cargo con el documento N� '" & lbl_consecutivo.Caption & "' ")
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
            'ACTUALIZAR EL DEPOSITO CON LOS PRODUCTOS A PRODUCIR
            Call UpDateDepositoProducir(RsDepositoCargo)
            
            'ACTUALIZAR MERMA
            If CDbl(Me.txt_merma) > 0 Then
                If Not UpdateMerma("DCO", ConsecutivoAjuste, RsEureka!c_Documento) Then
                    Call Mensaje(True, "No se pudo procesar un ajuste por merma.")
                    Ent.BDD.RollbackTrans: ActiveTrans = False
                    Exit Sub
                End If
            End If
            
            If Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
                
                If esBackOrder Then
                    
                    ' Cerrar Documento cuando se haya producido la cantidad total de cada producto de la orden.
                    
                    TmpValue = ";WITH Cantidades_OPR AS ( " & vbNewLine & _
                    "SELECT c_Documento AS Documento, ID AS RowID, CASE WHEN MAX(n_Decimales) > 3 THEN MAX(n_Decimales) ELSE 3 END AS Decimales, SUM(CASE WHEN n_Cant_Realizada > n_Cantidad THEN n_Cantidad ELSE n_Cant_Realizada END) AS CantDPE, " & vbNewLine & _
                    "SUM(n_Cantidad) As CantDCO " & vbNewLine & _
                    "FROM TR_ORDEN_PRODUCCION " & vbNewLine & _
                    "WHERE b_Producir = 1 " & vbNewLine & _
                    "AND c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & vbNewLine & _
                    "GROUP BY c_Documento, ID " & vbNewLine & _
                    "), TMP_CANT AS ( " & vbNewLine & _
                    "SELECT ROUND(TBL.CantDPE - TBL.CantDCO, TBL.Decimales) AS CantRestante, ((-1.0) * (1.0 / POWER(10, TBL.Decimales))) AS DiferenciaMinimaParaCerrarDocumento, * FROM Cantidades_OPR TBL " & vbNewLine & _
                    ") UPDATE MA_ORDEN_PRODUCCION " & vbNewLine & _
                    "SET c_Status = 'DCO' " & vbNewLine & _
                    "FROM MA_ORDEN_PRODUCCION OPR INNER JOIN TMP_CANT " & vbNewLine & _
                    "ON OPR.c_Documento = TMP_CANT.Documento " & vbNewLine & _
                    "WHERE OPR.c_Documento = TMP_CANT.Documento " & vbNewLine & _
                    "AND TMP_CANT.CantRestante >= TMP_CANT.DiferenciaMinimaParaCerrarDocumento; " & vbNewLine
                    
                Else
                    
                    ' Cerrar Orden independientemente de las cantidades utilizadas / realizadas.
                    
                    TmpValue = "UPDATE MA_ORDEN_PRODUCCION " & vbNewLine & _
                    "SET c_Status = 'DCO' " & vbNewLine & _
                    "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & vbNewLine
                    
                End If
                
                Ent.BDD.Execute TmpValue
                
            End If
            
        Else
            Call Mensaje(True, "La orden N� '" & lbl_consecutivo.Caption & "' existe en el Sistema.")
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
    ElseIf Tipo = "GESP" Then
        
        'GRABAR EN Espera
        
        If Mid(lbl_consecutivo.Caption, 1, 1) <> "E" Then
            lbl_consecutivo.Caption = "E" & Format(No_Consecutivo("PRODUCCION_ORDEN_ESP"), "00000000")
        End If
        
        Call Apertura_RecordsetC(RsEureka)
        
        Ent.BDD.Execute "DELETE FROM MA_INVENTARIO WHERE c_Documento = '" & lbl_consecutivo.Caption & "' AND c_Concepto = 'PRD' AND c_Status = 'DWT' "
        Ent.BDD.Execute "DELETE FROM TR_INVENTARIO WHERE c_Documento = '" & lbl_consecutivo.Caption & "' AND c_Concepto = 'PRD' "
        
        RsEureka.Open "SELECT * FROM MA_INVENTARIO " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Concepto = 'PRD' AND c_Status = 'DWT' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsEureka.EOF Then
            RsEureka.AddNew
        End If
        
        'ACTUALIZAR EL CABECERO DE INVENTARIO CON LOS DATOS DE LA FORMA
        Call UpdateHeader(RsEureka, "DWT")
        
        'ACTUALIZAR EL DETALLE DE PRODUCTOS A UTILIZAR (DESCARGO)
        Call Apertura_RecordsetC(RsInventarioDescargar)
        
        RsInventarioDescargar.Open "SELECT * FROM TR_INVENTARIO " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Concepto = 'PRD' AND c_TipoMov = 'Descargo'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsInventarioDescargar.EOF Then
            Call UpdateUtilizar(RsInventarioDescargar, "Descargo", Tipo)
        Else
            Call Mensaje(True, "Existe un detalle de Descargo con el documento N� '" & lbl_consecutivo.Caption & "' ")
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
        'ACTUALIZAR EL DETALLE DE PRODUCTOS A PRODUCIR (CARGO)
        
        Call Apertura_RecordsetC(RsInventarioCargar)
        
        RsInventarioCargar.Open "SELECT * FROM TR_INVENTARIO " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Concepto = 'PRD' AND c_TipoMov = 'Cargo'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsInventarioCargar.EOF Then
            Call UpdateProducir(RsInventarioCargar, "Cargo", Tipo)
        Else
            Call Mensaje(True, "Existe un detalle de Cargo con el documento N� '" & lbl_consecutivo.Caption & "' ")
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
    End If
    
    Ent.BDD.CommitTrans: ActiveTrans = False
    
    LcConsecu = lbl_consecutivo.Caption
    Find_Concept = "PRD"
    
    FrmAppLink.MostrarDocumentoStellar Find_Concept, FrmAppLink.GetFindStatus, LcConsecu
    
    Call Nuevo_Consecutivo
    Call Cancelar
    
    Exit Sub
    
RecordFail:
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans: ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Sub UpdateHeader(ByRef RsTemp As ADODB.Recordset, Status As String, Optional Ajuste As String = "")
    With RsTemp
        !c_Documento = lbl_consecutivo.Caption
        !d_Fecha = Date
        !c_Descripcion = txt_descripcion.Text
        !c_Status = Status
        Find_Status = Status
        !c_Concepto = "PRD"
        !c_CodProveedOR = Ajuste
        !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
        !c_CodMoneda = dbmoneda.Text
        !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
        !n_Descuento = CDbl(txt_cantidad.Text)
        !c_Observacion = ObservacionInicial '"EJECUCION DE FORMULA DE PRODUCCI�N"
        If Left(DocumentoRelacion, 4) = "OPR " Then
            !c_Relacion = DocumentoRelacion
        Else
            !c_Relacion = txt_preparado.Text
        End If
        !c_CodComprador = lcCodUsuario
        !c_Dep_Orig = txt_origen.Text
        !c_Dep_Dest = txt_destino.Text
        !c_Motivo = "PRODUCCION"
        !c_CodTransporte = ""
        !c_Ejecutor = lcDescr_User
        If FrmAppLink.InventarioCampoLineaProduccion Then
            !c_LineaProduccion = CboLnP.Text
        End If
        If FrmAppLink.InventarioCampoTurnoProduccion Then
            !c_Turno = CboTurno.Text
        End If
        !c_Factura = ""
        !c_Transportista = ""
        !n_Cantidad_compra = CDbl(txt_costosdirec.Text)
    End With
    RsTemp.UpdateBatch
End Sub

Sub UpdateUtilizar(ByRef RsTemp As ADODB.Recordset, TipoMov As String, Tipo As String)
    
    Dim Cont As Integer
    
    For Cont = 1 To Preparado.Rows - 1
        
        RsTemp.AddNew
        
        With RsTemp
            
            !c_Linea = Cont
            !c_Concepto = "PRD"
            !c_Documento = lbl_consecutivo.Caption
            !c_Deposito = txt_origen.Text
            !c_CodArticulo = Preparado.TextMatrix(Cont, ColumnaPreparado.Producto)
            !n_Cantidad = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Canti)), _
            CLng(Preparado.TextMatrix(Cont, ColumnaPreparado.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre)), Std_Decm))
            !n_Subtotal = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre) * !n_Cantidad), Std_Decm))
            !n_Impuesto = 0
            !n_Total = !n_Subtotal
            !c_TipoMov = TipoMov
            !n_Cant_Teorica = 0
            !n_Cant_Diferencia = 0
            !n_Precio = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre)), Std_Decm))
            !n_Precio_Original = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre)), Std_Decm))
            !f_Fecha = Date
            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
            
            'If ExisteCampoTabla("n_FactorMonedaProd", RsTemp) Then 'CMM1N
                ''!n_FactorMonedaProd = mFactorLn
                '!n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            'End If
            
            !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
            !ns_CantidadEmpaque = CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Cantibul))
            
            '' Nuevo. Lote. Si algun dia hay que agregar algo mas en este campo, lo separamos por ,
            '!c_Descripcion = StellarMensaje(2937) & " |" & Preparado.TextMatrix(Cont, ColumnaPreparado.Lote) & "|"
            If FrmAppLink.InventarioCampoIDLote Then
                !c_IDLote = Preparado.TextMatrix(Cont, ColumnaPreparado.Lote)
            End If
            
        End With
        
        RsTemp.UpdateBatch
        
        If (Tipo = "GORD") And RsTemp!n_Cantidad > 0 And Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
            Ent.BDD.Execute "UPDATE TR_ORDEN_PRODUCCION " & _
            "SET n_Cant_Utilizada = ROUND(n_Cant_Utilizada + (" & RsTemp!n_Cantidad & "), 8) " & _
            "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & _
            "AND c_CodArticulo = '" & RsTemp!c_CodArticulo & "' " & _
            "AND b_Producir = 0 ", TmpValue
        End If
        
    Next Cont
    
End Sub

Sub UpdateProducir(ByRef RsTemp As ADODB.Recordset, TipoMov As String, Tipo As String)
    
    Dim Cont As Integer, RsProductos As New ADODB.Recordset, Moneda_Prod As Integer
    Dim RsMonedaProd As New ADODB.Recordset
    Dim FactorMonedaProd As Double
    
    Dim Actualizar As Boolean
    Dim ActualizarCosto As Boolean
    
    If Not (Tipo = "GESP") Then
        
        ''Call Mensaje(False, "�Desea actualizar el costo de los productos?" & vbNewLine & "Si es as�, presione aceptar para continuar.")
        'Mensaje False, Replace(StellarMensaje(2812), "$(Line)", vbNewLine)
        
        'If Retorno Then
            ActualizarCosto = True ' Siempre actualizar.
        'End If
        
        If CambioPrecioAutomatico > 0 Then
            Actualizar = True
        Else
            
            'Call Mensaje(False, "�Desea actualizar el precio de los productos?" & vbNewLine & "Si es as�, presione aceptar para continuar.")
            Mensaje False, Replace(StellarMensaje(2811), "$(Line)", vbNewLine)
            
            If Retorno Then
                Actualizar = True
            End If
            
        End If
        
    End If
    
    For Cont = 1 To Producir.Rows - 1
        
        If Producir.TextMatrix(Cont, ColumnaProducir.Producto) <> "" Then
            
            RsTemp.AddNew
            
            With RsTemp
                
                !c_Linea = Cont
                !c_Concepto = "PRD"
                !c_Documento = lbl_consecutivo.Caption
                !c_Deposito = txt_destino.Text
                !c_CodArticulo = Producir.TextMatrix(Cont, ColumnaProducir.Producto)
                
                !n_Cantidad = CDbl(FormatNumber(CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Canti)), _
                CLng(Producir.TextMatrix(Cont, ColumnaProducir.LDeci))))
                
                !n_Costo = CDbl(FormatNumber(CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoProd)), Std_Decm)) / !n_Cantidad
                !n_Subtotal = CDbl(FormatNumber(CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoProd)), Std_Decm))
                
                !n_Total = !n_Subtotal
                
                !n_Impuesto = 0
                !c_TipoMov = TipoMov
                !n_Cant_Teorica = 0
                !n_Cant_Diferencia = 0
                
                !ns_CantidadEmpaque = CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Cantibul))
                
                !n_Precio = !n_Costo
                !n_Precio_Original = !n_Costo
                
                !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                
                !f_Fecha = Date
                !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
                
                'If ExisteCampoTabla("n_FactorMonedaProd", RsTemp) Then 'CMM1N
                    ''!n_FactorMonedaProd = mFactorLn
                    '!n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
                'End If
                
                '' Nuevo. Lote. Si algun dia hay que agregar algo mas en este campo, lo separamos por ,
                '!c_Descripcion = StellarMensaje(2937) & " |" & Producir.TextMatrix(Cont, ColumnaProducir.Lote) & "|"
                If FrmAppLink.InventarioCampoIDLote Then
                    !c_IDLote = Producir.TextMatrix(Cont, ColumnaProducir.Lote)
                End If
                
                ' Actualizar Precios!
                Call Apertura_RecordsetC(RsProductos)
                
                RsProductos.Open "SELECT * FROM MA_PRODUCTOS " & _
                "WHERE c_Codigo = '" & Producir.TextMatrix(Cont, ColumnaProducir.Producto) & "'", _
                Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                If Not RsProductos.EOF Then
                    
                    'ACA AGREGO LAS VARIABLES NECESARIAS PARA EL AGREGADO EN HISTORICO
                    
                    Dim CostoAnterior As Double
                    Dim CostoRepAnterior As Double
                    Dim Precio1Anterior As Double
                    Dim Precio2Anterior As Double
                    Dim Precio3Anterior As Double
                    CostoAnterior = RsProductos!n_CostoAct
                    CostoRepAnterior = RsProductos!n_Costorep
                            
                    Precio1Anterior = RsProductos!n_Precio1
                    Precio2Anterior = RsProductos!n_Precio2
                    Precio3Anterior = RsProductos!n_Precio3
                    
                    Call Apertura_RecordsetC(RsMonedaProd)
                    
                    RsMonedaProd.Open "SELECT * FROM MA_MONEDAS " & _
                    "WHERE c_CodMoneda = '" & RsProductos!c_CodMoneda & "'", _
                    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    If Not RsMonedaProd.EOF Then
                        Moneda_Prod = RsMonedaProd!n_Decimales
                        FactorMonedaProd = RsMonedaProd!n_Factor
                    Else
                        Moneda_Prod = 2
                        FactorMonedaProd = 1
                    End If
                    
                    'ACA SE ACTUALIZAN LOS COSTOS
                    
                    If ActualizarCosto Then
                        RsProductos!n_CostoAnt = RsProductos!n_CostoAct
                        RsProductos!n_CostoAct = CDbl(FormatNumber(( _
                        ((CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoProd)) _
                        * CDbl(MSK_FACTOR.Text)) / FactorMonedaProd)) _
                        / CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Canti)), Moneda_Prod))
                        RsProductos!n_CostoPro = CDbl(FormatNumber((RsProductos!n_CostoAnt + RsProductos!n_CostoAct) / 2, Moneda_Prod))
                        RsProductos!n_Costorep = RsProductos!n_CostoAct
                    End If
                    
                    'ACA SE AGREGAN PARA DEFINIRLOS Y GUARDAN LOS NUEVOS PRECIOS
                    Precio1 = 0
                    Precio2 = 0
                    Precio3 = 0
                    
                    If Actualizar Then
                        
                        Dim Porcentaje As Double
                        
                        If RsProductos!n_CostoAnt <> 0 Then
                            Porcentaje = (RsProductos!n_CostoAct / RsProductos!n_CostoAnt)
                        Else ' Evitar error. No Modificar precios.
                            'Mensaje True, "El producto [" & RsProductos!c_Descri & "] tiene el Costo Anterior en 0. No se pueden recalcular los precios para este producto. Debe verificar los precios manualmente."
                            Mensaje True, Replace(StellarMensaje(2813), "$(Desc)", RsProductos!c_Descri)
                            Porcentaje = 1
                        End If
                        
                        RsProductos!n_Precio1 = FormatNumber(RsProductos!n_Precio1 * Porcentaje, Moneda_Prod)
                        RsProductos!n_Precio2 = FormatNumber(RsProductos!n_Precio2 * Porcentaje, Moneda_Prod)
                        RsProductos!n_Precio3 = FormatNumber(RsProductos!n_Precio3 * Porcentaje, Moneda_Prod)
                        
                        'ACA SE GUARDAN LOS NUEVOS PRECIOS
                        Precio1 = CDbl(FormatNumber(RsProductos!n_Precio1 * Porcentaje, Moneda_Prod))
                        Precio2 = CDbl(FormatNumber(RsProductos!n_Precio2 * Porcentaje, Moneda_Prod))
                        Precio3 = CDbl(FormatNumber(RsProductos!n_Precio3 * Porcentaje, Moneda_Prod))
                        
                    End If
                    
                    RsProductos.UpdateBatch
                    
                    'ACA SE GUARDA LOS CAMBIOS EN LA TABLA MA_HISTORICO_COSTO_PRECIO
                    
                    If (CostoRepAnterior <> CDbl(RsProductos!n_Costorep)) Or (Precio1Anterior <> CDbl(Precio1)) Or (Precio2Anterior <> CDbl(Precio2)) Or (Precio3Anterior <> CDbl(Precio3)) Then
                        
                        If ExisteTablaV3("MA_HISTORICO_COSTO_PRECIO", Ent.BDD) Then
                            Dim Rs As New ADODB.Recordset
                            CorrelativoHistorico = No_Consecutivo("HISTORICO_COSTO_PRECIO", True)
                            Cadena = "insert into MA_HISTORICO_COSTO_PRECIO (c_Historico,d_FechaCambio,c_ProcesoOrigen,c_Documento,c_Usuario,c_CodArticulo,Precio1_Ant,Precio1_Nuevo,Precio2_Ant,Precio2_Nuevo,Precio3_Ant,Precio3_Nuevo,CostoAct_Ant,CostoAct_Nuevo,CostoRep_Ant,CostoRep_Nuevo)" _
                            & " values('" & CorrelativoHistorico & "', GETDATE(), 'PRODUCCION MANUAL', '" & LcConsecu & "','" & FrmAppLink.GetCodUsuario & "','" & RsProductos!c_Codigo & "', " & Precio1Anterior & "," & RsProductos!n_Precio1 & "," & Precio2Anterior & "," & RsProductos!n_Precio2 & "," & Precio3Anterior & "," & RsProductos!n_Precio3 & "," & CostoAnterior & "," & RsProductos!n_CostoAct & "," & CostoRepAnterior & "," & RsProductos!n_Costorep & ")"
                            Debug.Print Cadena
                            Rs.Open Cadena, Ent.BDD, adOpenDynamic, adLockBatchOptimistic
                            'Debug.Print cadena
                        End If
                        
                        If Not (Tipo = "GESP") Then
                            PasarTrPend "TR_PENDIENTE_PROD", RsProductos, Ent.BDD, Ent.BDD, Array("id")
                        End If
                        
                    End If
                    
                    'HASTA AQUI EL AGREGADO
                    
                End If
                
            End With
            
            If (Tipo = "GORD") And RsTemp!n_Cantidad > 0 And Left(DocumentoRelacion, 4) = "OPR " And EstatusOPR = "DPE" Then
                Ent.BDD.Execute "UPDATE TR_ORDEN_PRODUCCION " & _
                "SET n_Cant_Realizada = ROUND(n_Cant_Realizada + (" & RsTemp!n_Cantidad & "), 8) " & _
                "WHERE c_Documento = '" & Mid(DocumentoRelacion, 5) & "' " & _
                "AND c_CodArticulo = '" & RsTemp!c_CodArticulo & "' " & _
                "AND b_Producir = 1 ", TmpValue
            End If
            
            RsTemp.UpdateBatch
            
        End If
        
    Next Cont
    
End Sub

Function NumeroFilas(gridUsado) As Integer
    If gridUsado = 1 Then
        If ModifFlag Then
            NumeroFilas = Preparado.Rows - 2
        Else
            NumeroFilas = Preparado.Rows - 1
        End If
    Else
        If ModifFlag Then
            NumeroFilas = Producir.Rows - 2
        Else
            NumeroFilas = Producir.Rows - 1
        End If
    End If
End Function

Sub UpDateDepositoUtilizar(ByRef RsTemp As ADODB.Recordset)
    
    Dim Cont As Integer
    
    For Cont = 1 To Preparado.Rows - 1
        
        If Preparado.TextMatrix(Cont, ColumnaPreparado.Producto) <> "" Then
            
            Call Apertura_RecordsetC(RsTemp)
            
            RsTemp.Open "SELECT * FROM MA_DEPOPROD " & _
            "WHERE c_CodDeposito = '" & Trim(txt_origen.Text) & "' " & _
            "AND c_CodArticulo = '" & Preparado.TextMatrix(Cont, ColumnaPreparado.Producto) & "' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsTemp.EOF Then
                RsTemp.AddNew
                RsTemp!n_Cantidad = 0
                RsTemp!n_Cant_Comprometida = 0
                RsTemp!n_Cant_Ordenada = 0
            End If
            
            With RsTemp
                !c_CodDeposito = Trim(txt_origen.Text)
                !c_CodArticulo = Preparado.TextMatrix(Cont, ColumnaPreparado.Producto)
                !c_Descripcion = Preparado.TextMatrix(Cont, ColumnaPreparado.Descripci�n)
                !n_Cantidad = CDbl(FormatNumber(!n_Cantidad - _
                CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Canti)), _
                CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.LDeci))))
            End With
            
            RsTemp.UpdateBatch
            
        End If
        
    Next Cont
    
End Sub

Sub UpDateDepositoProducir(ByRef RsTemp As ADODB.Recordset)
    
    Dim Cont As Integer
    
    For Cont = 1 To Producir.Rows - 1
        
        If Producir.TextMatrix(Cont, ColumnaProducir.Producto) <> "" Then
            
            Call Apertura_RecordsetC(RsTemp)
            
            RsTemp.Open "SELECT * FROM MA_DEPOPROD " & _
            "WHERE c_CodDeposito = '" & Trim(txt_destino.Text) & "' " & _
            "AND c_CodArticulo = '" & Producir.TextMatrix(Cont, ColumnaProducir.Producto) & "' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsTemp.EOF Then
                RsTemp.AddNew
                RsTemp!n_Cantidad = 0
                RsTemp!n_Cant_Comprometida = 0
                RsTemp!n_Cant_Ordenada = 0
            End If
            
            With RsTemp
                !c_CodDeposito = Trim(txt_destino.Text)
                !c_CodArticulo = Producir.TextMatrix(Cont, ColumnaProducir.Producto)
                !c_Descripcion = Producir.TextMatrix(Cont, ColumnaProducir.Descripci�n)
                !n_Cantidad = CDbl(FormatNumber(!n_Cantidad + _
                CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Canti)), _
                CDbl(Producir.TextMatrix(Cont, ColumnaProducir.LDeci))))
            End With
            
            RsTemp.UpdateBatch
            
        End If
        
    Next Cont
    
End Sub

Function UpdateMerma(Status As String, ConsecutivoAjuste As String, _
Optional Orden As String) As Boolean
    
    Dim Cont As Integer, RsAjuste As New ADODB.Recordset, RsTrAjuste As New ADODB.Recordset
    UpdateMerma = False
    
    Call Apertura_RecordsetC(RsAjuste)
    
    If ConsecutivoAjuste = "" Then
        UpdateMerma = True
    Else
        RsAjuste.Open "SELECT * FROM MA_INVENTARIO " & _
        "WHERE c_Documento = '" & ConsecutivoAjuste & "' " & _
        "AND c_Concepto = 'AJU' AND c_Status = 'DCO' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If RsAjuste.EOF Then
            RsAjuste.AddNew
            With RsAjuste
                !c_Documento = ConsecutivoAjuste
                !c_Concepto = "AJU"
                !d_Fecha = Date
                !c_Descripcion = "Ajuste por O.P. N� '" & Orden & "',merma de prod."
                !c_Status = Status
                !c_CodProveedOR = ""
                !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                !c_CodMoneda = dbmoneda.Text
                !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
                'If ExisteCampoTabla("n_FactorMonedaProd", RsAjuste) Then 'CMM1N
                    ''!n_FactorMonedaProd = mFactorLn
                    '!n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
                'End If
                !n_Descuento = CDbl(FormatNumber(CDbl(txt_merma.Text), Std_Decm))
                !c_Observacion = "Ajuste Autom�tico por Produccion N� '" & Orden & "', por merma de productos"
                !c_Relacion = Orden
                !c_CodComprador = FrmAppLink.GetCodUsuario
                !c_Dep_Orig = txt_origen.Text
                !c_Dep_Dest = ""
                !c_Motivo = "Producci�n"
                !c_CodTransporte = ""
                !c_Ejecutor = FrmAppLink.GetCodUsuario
                !c_Factura = ""
                !c_Transportista = ""
                !n_Cantidad_compra = 0
                !CodConcepto = ConceptoAjuste
            End With
            
            RsAjuste.UpdateBatch
            Call Apertura_RecordsetC(RsTrAjuste)
            RsTrAjuste.Open "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Documento = '" & ConsecutivoAjuste & "' " & _
            "AND c_Concepto = 'AJU' AND c_TipoMov = 'Descargo' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsTrAjuste.EOF Then
                With RsTrAjuste
                    For Cont = 1 To NumeroFilas(1)
                        If CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.PorcMerma)) > 0 Then
                            .AddNew
                            !c_Linea = Cont
                            !c_Concepto = "AJU"
                            !c_Documento = ConsecutivoAjuste
                            !c_Deposito = txt_origen.Text
                            !c_CodArticulo = Trim(Preparado.TextMatrix(Cont, ColumnaPreparado.Producto))
                            
                            !n_Cantidad = CDbl(FormatNumber(Preparado.TextMatrix(Cont, ColumnaPreparado.Canti), _
                            CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.LDeci)))) _
                            * Preparado.TextMatrix(Cont, ColumnaPreparado.Merma) / 100
                            
                            !n_Costo = FormatNumber(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre), Std_Decm)
                            !n_Subtotal = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Canti)) _
                            * CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPreXMerma)), Std_Decm))
                            
                            !n_Impuesto = 0
                            !n_Total = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Canti)) _
                            * CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPreXMerma)), Std_Decm))
                            !c_TipoMov = "Descargo"
                            !n_Cant_Teorica = 0
                            !n_Cant_Diferencia = 0
                            !n_Precio = CDbl(FormatNumber(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre), Std_Decm))
                            !n_Precio_Original = CDbl(FormatNumber(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPreXMerma), Std_Decm))
                            !f_Fecha = Date
                            !c_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                            !n_FactorCambio = CDbl(FormatNumber(CDbl(MSK_FACTOR.Text), Std_Decm))
                            !CodConcepto = ConceptoAjuste
                            !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
                            !ns_CantidadEmpaque = CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Cantibul))
                            RsTrAjuste.UpdateBatch
                        End If
                        
                    Next Cont
                    
                End With
                
                UpdateMerma = True
            Else
                UpdateMerma = False
            End If
            
        Else
            UpdateMerma = False
        End If
        
    End If
    
End Function

Sub Buscar_Produccion(Tipo As String, Orden As String)
    
    Dim RsOrden As New ADODB.Recordset
    Call Apertura_RecordsetC(RsOrden)
    RsOrden.Open "SELECT * FROM MA_INVENTARIO " & _
    "WHERE c_Documento = '" & Orden & "' " & _
    "AND c_Concepto = 'PRD' " & _
    "AND c_Status = '" & IIf(Tipo = "BORD", "DCO", "DWT") & "' ", _
    Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not RsOrden.EOF Then
        Call Cancelar
        If Buscar_Formula(RsOrden!c_Relacion) Then
            txt_preparado.Text = RsOrden!c_Relacion
            'Call txt_preparado_KeyDown(vbKeyReturn, 0)
            txt_origen.Text = RsOrden!c_Dep_Orig
            txt_destino.Text = RsOrden!c_Dep_Dest
            If Tipo = "BORD" Then
                Nuevo_Consecutivo
            Else
                lbl_consecutivo.Caption = RsOrden!c_Documento
            End If
            'If txt_preparado.Enabled Then
                'txt_preparado.SetFocus
            'End If
            'txt_cantidad.Text = RsOrden!n_Descuento
            Call txt_destino_LostFocus
            Call txt_origen_LostFocus
            Call txt_cantidad_LostFocus
            Call ReEscribir(CDbl(RsOrden!n_Descuento))
            DocumentoRelacion = txt_preparado.Text
            ObservacionInicial = StellarMensaje(2948)
            esBackOrder = False
            FechaEstimadaPRD = Date
            FechaVcto = 0
            Call ListSafeIndexSelection(CboLnP, 0)
            Call ListSafeIndexSelection(CboTurno, 0)
        Else
            'Call Mensaje(True, "No se encontr� la f�rmula '" & RsOrden!c_Relacion & "', relacionada con la producci�n, es posible que haya sido eliminada, o la f�rmula es incoherente, actualicela.")
            Mensaje True, Replace(StellarMensaje(2808), "$(Code)", RsOrden!c_Relacion)
            If FlgIncoherente Then
                RsOrden!c_Relacion = "INC"
            End If
            Call Cancelar
        End If
    Else
        'Call Mensaje(True, "No se encontr� la orden '" & orden & "' ")
        Mensaje True, Replace(StellarMensaje(2814), "$(Code)", Orden)
        Exit Sub
    End If
    
End Sub

Sub Anular_Produccion(Tipo As String, Orden As String, Optional pGrid As Object = Nothing)
    
    Dim RsMaProduccionA As New ADODB.Recordset, RsTrProduccionACargo As New ADODB.Recordset, RsTrProduccionADescargo As New ADODB.Recordset, RsProducirA As New ADODB.Recordset
    Dim RsMaProduccionD As New ADODB.Recordset, RsTrProduccionDCargo As New ADODB.Recordset, RsTrProduccionDDescargo As New ADODB.Recordset, RsProducirD As New ADODB.Recordset
    Dim RsDepoProd As New ADODB.Recordset
    Dim Del_consecutivo As String
    
    Call Apertura_RecordsetC(RsMaProduccionA)
    Call Apertura_RecordsetC(RsTrProduccionACargo)
    Call Apertura_RecordsetC(RsTrProduccionADescargo)
    Call Apertura_RecordsetC(RsProducirA)
    Call Apertura_RecordsetC(RsMaProduccionD)
    Call Apertura_RecordsetC(RsTrProduccionDCargo)
    Call Apertura_RecordsetC(RsTrProduccionDDescargo)
    Call Apertura_RecordsetC(RsProducirD)
    'On Error GoTo ErrorUpdate
    
    If Tipo = "AORD" Then
        
        'PRDNivelAnularProduccion = 11 ' Debug
        
        If FrmAppLink.GetNivelUsuario < FrmAppLink.PRDNivelAnularProduccion Then
            
            Dim FrmAutorizacion: Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
            
            'PRDNivelAnularProduccion = 6 ' Debug
            
            FrmAutorizacion.mNivel = FrmAppLink.PRDNivelAnularProduccion
            FrmAutorizacion.Titulo = StellarMensaje(2970) ' "Introduzca las credenciales " & _
            "de un usuario que anula el movimiento de produccion."
            
            Set FrmAutorizacion.mConexion = Ent.BDD
            
            FrmAutorizacion.Show vbModal
            
            If FrmAutorizacion.mAceptada Then
                
                Autorizante = FrmAutorizacion.mUsuario
                Set FrmAutorizacion = Nothing
                ' Continuar
                
            Else
                
                Set FrmAutorizacion = Nothing
                Mensaje True, StellarMensaje(16050)
                Exit Sub
                
            End If
            
        End If
        
        'ANULAR ORDEN
        Ent.BDD.BeginTrans
        Del_consecutivo = Format(No_Consecutivo("PRODUCCION_ORDEN"), "A00000000")
        RsMaProduccionA.Open "SELECT * FROM MA_INVENTARIO WHERE c_Documento = '" & Orden & "' AND c_Concepto = 'PRD' AND c_Status = 'DCO' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If Not RsMaProduccionA.EOF Then
            RsMaProduccionD.Open "SELECT * FROM MA_INVENTARIO WHERE c_Documento = '" & Del_consecutivo & "' AND c_Concepto = 'PRD' AND c_Status = 'ANU'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If RsMaProduccionD.EOF Then
                With RsMaProduccionD
                    While Not RsMaProduccionA.EOF
                        .AddNew
                        !c_Concepto = Trim(RsMaProduccionA!c_Concepto)
                        !c_Documento = Del_consecutivo
                        !d_Fecha = Date
                        !c_Descripcion = Trim(RsMaProduccionA!c_Descripcion)
                        !c_Status = "ANU"
                        !c_CodProveedOR = Trim(RsMaProduccionA!c_CodProveedOR)
                        !c_CodLocalidad = Trim(RsMaProduccionA!c_CodLocalidad)
                        !c_CodMoneda = Trim(RsMaProduccionA!c_CodMoneda)
                        !n_FactorCambio = RsMaProduccionA!n_FactorCambio
                        !n_Descuento = RsMaProduccionA!n_Descuento
                        !c_Observacion = "Proceso de Anulaci�n del Documento de Produccion " & RsMaProduccionA!c_Documento
                        !c_Relacion = Trim(RsMaProduccionA!c_Documento)
                        !c_CodComprador = Trim(RsMaProduccionA!c_CodComprador)
                        !c_Dep_Orig = Trim(RsMaProduccionA!c_Dep_Orig)
                        !c_Dep_Dest = Trim(RsMaProduccionA!c_Dep_Dest)
                        !c_Motivo = "Anular"
                        !c_CodTransporte = Trim(RsMaProduccionA!c_CodTransporte)
                        !c_Ejecutor = Trim(RsMaProduccionA!c_Ejecutor)
                        !c_Factura = Trim(RsMaProduccionA!c_Factura)
                        !c_Transportista = Trim(RsMaProduccionA!c_Transportista)
                        !n_Cantidad_compra = RsMaProduccionA!n_Cantidad_compra
                        .UpdateBatch
                        RsMaProduccionA!c_Status = "ANU"
                        RsMaProduccionA.UpdateBatch
                        'ANULAR LA MERMA
                        If RsMaProduccionA!c_CodProveedOR <> "" Then
                            Call Anular_AjustexMerma(RsMaProduccionA!c_CodProveedOR)
                        End If
                        RsMaProduccionA.MoveNext
                    Wend
                End With
            End If
        Else
            'Call Mensaje(True, "No se encontraron datos.")
            Ent.BDD.RollbackTrans
            Mensaje True, StellarMensaje(335)
            Exit Sub
        End If
        
        'DESCARGAR
        RsTrProduccionACargo.Open "SELECT * FROM TR_INVENTARIO WHERE c_Documento = '" & Orden & "' AND c_Concepto = 'PRD' AND c_TipoMov = 'Cargo'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If Not RsTrProduccionACargo.EOF Then
            RsTrProduccionDDescargo.Open "SELECT * FROM TR_INVENTARIO WHERE c_Documento = '" & Orden & "' AND c_Concepto = 'PRD' AND c_TipoMov = 'Descargo'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            With RsTrProduccionDDescargo
                While Not RsTrProduccionACargo.EOF
                    .AddNew
                    !c_Linea = RsTrProduccionACargo!c_Linea
                    !c_Concepto = Trim(RsTrProduccionACargo!c_Concepto)
                    !c_Documento = Del_consecutivo
                    !c_Deposito = Trim(RsTrProduccionACargo!c_Deposito)
                    !c_CodArticulo = Trim(RsTrProduccionACargo!c_CodArticulo)
                    !n_Cantidad = RsTrProduccionACargo!n_Cantidad
                    !n_Costo = RsTrProduccionACargo!n_Costo
                    !n_Subtotal = RsTrProduccionACargo!n_Subtotal
                    !n_Impuesto = RsTrProduccionACargo!n_Impuesto
                    !n_Total = RsTrProduccionACargo!n_Total
                    !c_TipoMov = "Descargo"
                    !n_Cant_Teorica = RsTrProduccionACargo!n_Cant_Teorica
                    !n_Cant_Diferencia = RsTrProduccionACargo!n_Cant_Diferencia
                    !n_Precio = RsTrProduccionACargo!n_Precio
                    !n_Precio_Original = RsTrProduccionACargo!n_Precio_Original
                    !f_Fecha = Date
                    !c_CodLocalidad = Trim(RsTrProduccionACargo!c_CodLocalidad)
                    !n_FactorCambio = RsTrProduccionACargo!n_FactorCambio
                    Call Apertura_RecordsetC(RsDepoProd)
                    RsDepoProd.Open "SELECT * FROM MA_DEPOPROD WHERE c_CodDeposito = '" & RsTrProduccionACargo!c_Deposito & "' AND c_CodArticulo = '" & RsTrProduccionACargo!c_CodArticulo & "' ", _
                    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                    'AUMENTA EL INVENTARIO
                    If Not RsDepoProd.EOF Then
                        RsDepoProd!n_Cantidad = RsDepoProd!n_Cantidad - RsTrProduccionACargo!n_Cantidad
                        RsDepoProd.UpdateBatch
                    Else
                        RsDepoProd.AddNew
                        RsDepoProd!c_CodDeposito = Trim(RsTrProduccionACargo!c_Deposito)
                        RsDepoProd!c_CodArticulo = Trim(RsTrProduccionACargo!c_CodArticulo)
                        RsDepoProd!c_CodDeposito = Trim(RsTrProduccionACargo!c_Deposito)
                        RsDepoProd!n_Cantidad = RsTrProduccionACargo!n_Cantidad
                        RsDepoProd!n_Cant_Comprometida = 0
                        RsDepoProd!n_Cant_Ordenada = 0
                        RsDepoProd.UpdateBatch
                    End If
                    .UpdateBatch
                    RsTrProduccionACargo.MoveNext
                Wend
            End With
        Else
            'Call Mensaje(True, "No se encontraron datos.")
            Ent.BDD.RollbackTrans
            Mensaje True, StellarMensaje(335)
            Exit Sub
        End If
        
        'CARGAR
        RsTrProduccionADescargo.Open "SELECT * FROM TR_INVENTARIO WHERE c_Documento = '" & Orden & "' AND c_Concepto = 'PRD' AND c_TipoMov = 'Descargo'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If Not RsTrProduccionADescargo.EOF Then
            RsTrProduccionDCargo.Open "SELECT * FROM TR_INVENTARIO WHERE c_Documento = '" & Orden & "' AND c_Concepto = 'PRD' AND c_TipoMov = 'Cargo'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            With RsTrProduccionDCargo
                While Not RsTrProduccionADescargo.EOF
                    .AddNew
                    !c_Linea = RsTrProduccionADescargo!c_Linea
                    !c_Concepto = Trim(RsTrProduccionADescargo!c_Concepto)
                    !c_Documento = Del_consecutivo
                    !c_Deposito = Trim(RsTrProduccionADescargo!c_Deposito)
                    !c_CodArticulo = Trim(RsTrProduccionADescargo!c_CodArticulo)
                    !n_Cantidad = RsTrProduccionADescargo!n_Cantidad
                    !n_Costo = RsTrProduccionADescargo!n_Costo
                    !n_Subtotal = RsTrProduccionADescargo!n_Subtotal
                    !n_Impuesto = RsTrProduccionADescargo!n_Impuesto
                    !n_Total = RsTrProduccionADescargo!n_Total
                    !c_TipoMov = "Cargo"
                    !n_Cant_Teorica = RsTrProduccionADescargo!n_Cant_Teorica
                    !n_Cant_Diferencia = RsTrProduccionADescargo!n_Cant_Diferencia
                    !n_Precio = RsTrProduccionADescargo!n_Precio
                    !n_Precio_Original = RsTrProduccionADescargo!n_Precio_Original
                    !f_Fecha = Date
                    !c_CodLocalidad = Trim(RsTrProduccionADescargo!c_CodLocalidad)
                    !n_FactorCambio = RsTrProduccionADescargo!n_FactorCambio
                    Call Apertura_RecordsetC(RsDepoProd)
                    RsDepoProd.Open "SELECT * FROM MA_DEPOPROD WHERE c_CodDeposito = '" & RsTrProduccionADescargo!c_Deposito & "' AND c_CodArticulo = '" & RsTrProduccionADescargo!c_CodArticulo & "' ", _
                    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                    'AUMENTA EL INVENTARIO
                    If Not RsDepoProd.EOF Then
                        RsDepoProd!n_Cantidad = RsDepoProd!n_Cantidad + RsTrProduccionADescargo!n_Cantidad
                        RsDepoProd.UpdateBatch
                    Else
                        RsDepoProd.AddNew
                        RsDepoProd!c_CodDeposito = Trim(RsTrProduccionADescargo!c_Deposito)
                        RsDepoProd!c_CodArticulo = Trim(RsTrProduccionADescargo!c_CodArticulo)
                        RsDepoProd!c_CodDeposito = Trim(RsTrProduccionADescargo!c_Deposito)
                        RsDepoProd!n_Cantidad = RsTrProduccionADescargo!n_Cantidad
                        RsDepoProd!n_Cant_Comprometida = 0
                        RsDepoProd!n_Cant_Ordenada = 0
                        RsDepoProd.UpdateBatch
                    End If
                    .UpdateBatch
                    RsTrProduccionADescargo.MoveNext
                Wend
            End With
        Else
            'Call Mensaje(True, "No se encontraron datos.")
            Ent.BDD.RollbackTrans
            Mensaje True, StellarMensaje(335)
            Exit Sub
        End If
        
        Ent.BDD.CommitTrans
        
        If Not pGrid Is Nothing Then
            pGrid.ListItems.Remove pGrid.SelectedItem.Index
        End If
        
        Exit Sub
            
ErrorUpdate:
        
        Ent.BDD.RollbackTrans
        'Call Mensaje(True, "Error de Actualizaci�n, Intente mas tarde.")
        mErrorNumber = Err.Number
        mErrorDesc = Err.Description
        mErrorSource = Err.Source
        
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
        Exit Sub
        
    ElseIf Tipo = "AESP" Then
        'ANULAR ORDEN EN Espera
        Ent.BDD.BeginTrans
        Ent.BDD.Execute "DELETE FROM MA_INVENTARIO WHERE c_Documento = '" & Orden & "' AND c_Status = 'DWT' AND c_Concepto = 'PRD' "
        Ent.BDD.Execute "DELETE FROM TR_INVENTARIO WHERE c_Documento = '" & Orden & "' AND c_Concepto = 'PRD' "
        Ent.BDD.CommitTrans
    End If
    
End Sub

Sub DistTotal(Monto As Double, Grid As MSFlexGrid)
    
    Grid.Visible = True
    
    Dim Cont As Integer, Cantidad As Double, dTotal As Double, mValorCosteoAct As Double
    
    dTotal = Monto
    
    Cont = 1
    Cantidad = 0
    Grid.FocusRect = flexFocusNone
    'Debug.Print GRID.Name
    
    For Cont = 1 To Grid.Rows - 1
        mValorCosteo = MSGridRecover(Grid, Cont, ColumnaProducir.Factor)
        If IsNumeric(mValorCosteo) Then
            mSumaFactor = mSumaFactor + CDbl(mValorCosteo)
        End If
    Next
    
    While Cont <= Grid.Rows - 1
        If MSGridRecover(Grid, Cont, ColumnaProducir.Canti) <> "" Then
            Cantidad = CDbl(FormatNumber(Cantidad, CDbl(FormatNumber(MSGridRecover(Grid, Cont, ColumnaPreparado.LDeci), Std_Decm)))) _
            + CDbl(FormatNumber(MSGridRecover(Grid, Cont, ColumnaPreparado.Canti), CDbl(MSGridRecover(Grid, Cont, ColumnaPreparado.LDeci))))
        End If
        Cont = Cont + 1
    Wend
    
    For Cont = 1 To Grid.Rows - 1
        
        If MSGridRecover(Grid, Cont, ColumnaProducir.Canti) <> "" Then
            
            mValorCosteoAct = CDbl(MSGridRecover(Grid, Cont, ColumnaProducir.Factor))
            mPorcenPart = (mValorCosteoAct / mSumaFactor)
            
            Dim mCostoNew As Double
            
            Call MSGridAsign(Grid, Cont, ColumnaProducir.CostoProd, FormatNumber((((Monto)) * mPorcenPart), Std_Decm), , flexAlignRightCenter)
            
            mCostoNew = Round((( _
            CDbl(MSGridRecover(Grid, Cont, ColumnaProducir.CostoProd)) / _
            CDbl(MSGridRecover(Grid, Cont, ColumnaProducir.Canti))) _
            ), Std_Decm)
            
            Call MSGridAsign(Grid, Cont, ColumnaProducir.CostoPre, FormatNumber(mCostoNew, Std_Decm), , flexAlignRightCenter)
            
            If MSGridRecover(Grid, Cont, ColumnaProducir.CostoPre) <> "" Then
                
                Dim mCostoOrig As Double, _
                mVerPrecios As Boolean, Visualizar As Integer
                
                mCostoOrig = CDbl(MSGridRecover(Grid, Cont, ColumnaProducir.CostoOriginal))
                
                If mCostoOrig <> mCostoNew Then
                    
                    Call CambiarColorFila(Grid, CLng(Cont), ColorUp)
                    
                    'If Visualizar = 0 Then
                        'ProcesandoCambio = True
                        'If vbYes = MsgBox("Existe una variaci�n en el costo de reposici�n, " & _
                        "�Desea previsualizar los precios de venta?", vbQuestion + vbYesNo, "Mensaje Stellar") Then
                            'Visualizar = 1
                        'Else
                            'Visualizar = -1
                        'End If
                    'End If
                    
                    'If Visualizar = 1 Then
                        'MonedaDoc.BuscarMonedas , dbmoneda.Text
                        'newfrm_orden_reposicion.Codigo = MSGridRecover(Grid, Cont, ColumnaProducir.Producto)
                        'newfrm_orden_reposicion.Show vbModal
                    'End If
                    
                    ' Mejor solo cambiar color de la fila... hacer esto por cada cambio en el grid es muy
                    ' molesto para el usuario. Mejor le damos la opcion de verlos todos antes de grabar.
                    
                Else
                    Call CambiarColorFila(Grid, CLng(Cont), ColorNormal)
                End If
                
            End If
            
        End If
        
    Next Cont
    
    Grid.FocusRect = flexFocusLight
    
    Grid.Visible = True
    
    'ProcesandoCambio = False
    
End Sub

Sub Anular_AjustexMerma(Ajuste As String)
    
    Dim RsAjusteMA As New ADODB.Recordset, RsAjusteTR As New ADODB.Recordset, NConsecutivo As String
    Dim RsAjusteMA_New As New ADODB.Recordset, RsAjusteTR_New As New ADODB.Recordset
    
    Call Apertura_RecordsetC(RsAjusteMA)
    
    RsAjusteMA.Open "SELECT * FROM MA_INVENTARIO " & _
    "WHERE c_Documento = '" & Ajuste & "' " & _
    "AND c_Status = 'DCO' AND c_Concepto = 'AJU'", _
    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not RsAjusteMA.EOF Then
        
        NConsecutivo = Format(No_Consecutivo("ajustes"), "000000000")
        
        Call Apertura_RecordsetC(RsAjusteTR)
        
        RsAjusteTR.Open "SELECT * FROM TR_INVENTARIO " & _
        "WHERE c_Documento = '" & Ajuste & "' AND c_Concepto = 'AJU'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsAjusteTR.EOF Then
            
            Call Apertura_RecordsetC(RsAjusteMA_New)
            Call Apertura_RecordsetC(RsAjusteTR_New)
            
            RsAjusteMA_New.Open "SELECT * FROM MA_INVENTARIO " & _
            "WHERE c_Documento = '" & NConsecutivo & "' AND c_Status = 'DCO' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            RsAjusteTR_New.Open "SELECT * FROM TR_INVENTARIO " & _
            "WHERE c_Documento = '" & NConsecutivo & "' ", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            RsAjusteMA_New.AddNew
            
            With RsAjusteMA_New
                !c_Concepto = "AJU"
                !c_Documento = NConsecutivo
                !d_Fecha = Date
                !c_Descripcion = "PRODUCCION GENERO UN AJUSTE,DEVOLUCION POR AJUSTE"
                !c_Status = "ANU"
                !c_CodProveedOR = Orden
                !c_CodLocalidad = Trim(RsAjusteMA!c_CodLocalidad)
                !c_CodMoneda = Trim(RsAjusteMA!c_CodMoneda)
                !n_FactorCambio = RsAjusteMA!n_FactorCambio
                !n_Descuento = RsAjusteMA!n_Descuento
                !c_Observacion = "ANULACI�N DE AJUSTE POR ORDEN DE PRODUCCION # " & Orden & "'"
                !c_Relacion = Ajuste
                !c_CodComprador = Trim(RsAjusteMA!c_CodComprador)
                !c_Dep_Orig = Trim(RsAjusteMA!c_Dep_Dest)
                !c_Dep_Dest = Trim(RsAjusteMA!c_Dep_Orig)
                !c_Motivo = "PRODUCCION"
                !c_CodTransporte = ""
                !c_Ejecutor = Trim(FrmAppLink.GetCodUsuario)
                !c_Factura = Trim(RsAjusteMA!c_Factura)
                !c_Transportista = Trim(RsAjusteMA!c_Transportista)
                !n_Cantidad_compra = RsAjusteMA!n_Cantidad_compra
                .UpdateBatch
            End With
            
            'RsAjusteTR_New.AddNew
            With RsAjusteTR_New
                While Not RsAjusteTR.EOF
                    .AddNew
                    !c_Linea = RsAjusteTR!c_Linea
                    !c_Concepto = Trim(RsAjusteTR!c_Concepto)
                    !c_Documento = NConsecutivo
                    !c_Deposito = Trim(RsAjusteTR!c_Deposito)
                    !c_CodArticulo = Trim(RsAjusteTR!c_CodArticulo)
                    !n_Cantidad = RsAjusteTR!n_Cantidad
                    !n_Costo = RsAjusteTR!n_Costo
                    !n_Subtotal = RsAjusteTR!n_Subtotal
                    !n_Impuesto = RsAjusteTR!n_Impuesto
                    !n_Total = RsAjusteTR!n_Total
                    !c_TipoMov = "Cargo"
                    !n_Cant_Teorica = RsAjusteTR!n_Cant_Teorica
                    !n_Cant_Diferencia = RsAjusteTR!n_Cant_Diferencia
                    !n_Precio = RsAjusteTR!n_Precio
                    !n_Precio_Original = RsAjusteTR!n_Precio_Original
                    !f_Fecha = Date
                    !c_CodLocalidad = Trim(RsAjusteTR!c_CodLocalidad)
                    !n_FactorCambio = RsAjusteTR!n_FactorCambio
                    RsAjusteTR.MoveNext
                Wend
                .UpdateBatch
            End With
        Else
            Exit Sub
        End If
    Else
        Exit Sub
    End If
    
End Sub

Private Sub txtedit_Change()
    
    On Error GoTo Errores
    
    If ModifFlag Then
        Dim Lcol As Integer, Lrow As Integer
        Preparado.Enabled = True
        If Preparado.Col = ColumnaPreparado.Canti Then
            Lrow = Preparado.Row
            Lcol = Preparado.Col
            If txtedit.Visible Then
                'If CheckCad(txtedit, CDbl(MSGridRecover(Preparado, Preparado.Row, 0)), , True) Then
                'End If
            End If
            Call SetDefMSGrid(Preparado, Lrow, Lcol)
        End If
    End If
    
    Exit Sub
    
Errores:
    
    txtedit.Visible = False
    txtedit.Text = ""
    txtEdit_LostFocus
    
End Sub

Private Sub TxtEdit_GotFocus()
    If ModifFlag Then
        If Preparado.Col = 1 And MSGridRecover(Producir, Producir.Row, ColumnaProducir.Producto) = "" Then
            Who_Ami = 1
            BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = True
        ElseIf Preparado.Col = 1 And MSGridRecover(Producir, Producir.Row, ColumnaProducir.Producto) <> "" Then
            Who_Ami = 1
            txtEdit2.Enabled = False
            txtEdit2.Text = ""
        End If
    End If
End Sub

Private Sub txtEdit_KeyPress(KeyAscii As Integer)
    If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                'oTeclado.Key_Tab
                Call txtEdit_LostFocus
            Case vbKeyEscape
                Call Preparado_KeyPress(vbKeyEscape)
            Case Else
                'If Preparado.Col = ColumnaPreparado.PorcMerma Then
                    'Select Case KeyAscii ' Aqui validar Mascara
                        'Case 48 To 57
                        'Case 8
                        'Case 46
                        'Case Else
                            'KeyAscii = 0
                    'End Select
                'End If
        End Select
    End If
End Sub

Private Sub txtEdit_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo Errores
    
    If ModifFlag Then
    Else
    Select Case KeyCode
        Case Is = vbKeyReturn
            
        Dim RsRecetas As New ADODB.Recordset
        txtedit.Visible = False
        With Preparado
            Select Case .Col
                Case ColumnaPreparado.Canti 'Cantidad
                    .Enabled = True
                    If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                        If CheckCad(txtedit, CDbl(MSGridRecover(Preparado, .Row, ColumnaPreparado.LDeci)), , False) Then
                            If CDbl(txtedit.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                                'Cambia la Celda actual
                                .Text = FormatNumber(txtedit.Text, CDbl(MSGridRecover(Preparado, .Row, ColumnaPreparado.LDeci)))
                                .TextMatrix(.Row, ColumnaPreparado.CostoProd) = FormatNumber(CDbl(.Text) * CDbl(.TextMatrix(.Row, ColumnaPreparado.CostoPre)), Std_Decm)
                                .CellAlignment = flexAlignRightCenter
                                Call Apertura_Recordset(RsRecetas)
                                texto_cod = MSGridRecover(Preparado, .RowSel, ColumnaPreparado.Producto)
                                Criterio = "select *, ma_productos." & CostoActivo & " as costo " & _
                                "from ma_productos where c_Codigo ='" & texto_cod & "'"
                                RsRecetas.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                                
                                Call Apertura_Recordset(rsPmoneda)
                                rsPmoneda.Open "select * from ma_monedas " & _
                                "where c_CodMoneda = '" & RsRecetas!c_CodMoneda & "'", _
                                Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                                Monto = 0
                                For Cont = 1 To Preparado.Rows - 1
                                    Monto = Monto + (CDbl(IIf(MSGridRecover(Preparado, Cont, ColumnaPreparado.CostoPre) = "", 0, _
                                    MSGridRecover(Preparado, Cont, ColumnaPreparado.CostoPre))) _
                                    * CDbl(IIf(MSGridRecover(Preparado, Cont, ColumnaPreparado.Canti) = "", 0, _
                                    MSGridRecover(Preparado, Cont, ColumnaPreparado.Canti))))
                                Next Cont
                                txt_monto.Text = FormatNumber(Monto, rsPmoneda!n_Decimales)
                                If CDbl(MSGridRecover(Preparado, .Row, ColumnaPreparado.Merma)) = 0 Then
                                    .Col = ColumnaPreparado.Merma
                                    .Text = FormatNumber(0, Std_DecC)
                                    .Col = ColumnaPreparado.CostoPreXMerma
                                    .Text = FormatNumber(0, Std_Decm)
                                    'oTeclado.Key_Return
                                End If
                                
                                MTotal = FormatNumber(CDbl(txt_monto.Text) + CDbl(Me.txt_merma), 2)
                                
                                Call DistTotal(CDbl(txt_monto.Text) + CDbl(Me.txt_merma.Text), Producir)
                                Call Cerrar_Recordset(rsPmoneda)
                                Call Cerrar_Recordset(RsRecetas)
                                
                                If CDbl(MSGridRecover(Preparado, .Row, ColumnaPreparado.Merma)) > 0 Then
                                    .Col = ColumnaPreparado.Merma
                                    Me.txtedit = .Text
                                Else
                                    Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.Merma)
                                End If
                                
                                If ManejaMermaExplicita And ModifFlag = 0 Then
                                    Call MostrarTxt1(ColumnaPreparado.PorcMerma, Preparado.Row)
                                    DoEvents
                                    txtedit.Text = 0
                                    DoEvents
                                    txtEdit_KeyDown vbKeyReturn, 0
                                End If
                                
                            Else
                                ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Valor no puede ser cero")
                                Mensaje True, StellarMensaje(2815)
                                Tecla_Pulsada = False
                                txtedit.Enabled = True
                                txtedit.Text = ""
                                txtedit.Visible = True
                                SafeFocus txtedit
                                .Enabled = False
                                Exit Sub
                            End If
                            
                        Else
                            If txtedit.Text <> "" Then
                                .Enabled = False
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Dato de entrada incorrecto")
                                Mensaje True, StellarMensaje(2815)
                                Tecla_Pulsada = False
                                txtedit.Enabled = True
                                txtedit.Text = ""
                                txtedit.Visible = True
                                SafeFocus txtedit
                                Exit Sub
                            Else
                                .Col = ColumnaPreparado.Canti
                                .Text = 1
                                'SendKeys Chr$(13)
                                oTeclado.Key_Return
                                
                            End If
                        End If
                    End If
                    
                Case ColumnaPreparado.PorcMerma
                    .Enabled = True
                    'If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                        'If CheckCad(txtedit, CDbl(MSGridRecover(Preparado, .Row, ColumnaPreparado.LDeci)), , False) Then
                            'If CDbl(txtedit.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                                'Cambia la Celda actual
                                '.Text = FormatNumber(txtedit.Text, CDbl(MSGridRecover(Preparado, .Row, ColumnaPreparado.LDeci)))
                                .Text = Empty
                                .CellAlignment = flexAlignRightCenter
                                If (.Row >= .Rows - 1) Then
                                    Call MostrarTxt2(ColumnaProducir.Canti, 1)
                                Else
                                    Call MostrarTxt1(ColumnaPreparado.Canti, .Row + 1)
                                End If
                            'End If
                        'End If
                    'End If
                    
            End Select
            
        End With
        
    End Select
    
    End If
    
    Exit Sub
    
Errores:
    
    txtedit.Text = ""
    txtedit.Visible = False
    
End Sub

Private Sub txtEdit_LostFocus()
    
    If TmpDisableEvent Then TmpDisableEvent = False: Exit Sub
    
    With Preparado
    
     If ModifFlag Then
        
        Dim Mensajes As String, RsEureka As New ADODB.Recordset, _
        ColAct As Integer, FilAct As Integer, _
        Merma As Double, Monto As Double
        
        On Error GoTo Errores
        
        Select Case .Col
            Case ColumnaPreparado.Canti 'Cantidad
                .Enabled = True
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    If CheckCad(txtedit, CDbl(.TextMatrix(.Row, ColumnaPreparado.LDeci)), , False) Then
                        If CDbl(txtedit.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            
                            'Cambia la Celda actual
                            .Text = FormatNumber(txtedit.Text, CDbl(.TextMatrix(.Row, ColumnaPreparado.LDeci)))
                            .TextMatrix(.Row, ColumnaPreparado.CostoProd) = FormatNumber(CDbl(.Text) _
                            * CDbl(.TextMatrix(.Row, ColumnaPreparado.CostoPre)), Std_Decm)
                            
                            If mRecalculandoIngredientes Then
                                .TextMatrix(.Row, ColumnaPreparado.CantTeorica) = .Text
                            End If
                            
                            .CellAlignment = flexAlignRightCenter
                            
                            Call Apertura_Recordset(RsRecetas)
                            
                            texto_cod = .TextMatrix(.RowSel, ColumnaPreparado.Producto)
                            Criterio = "select *, ma_productos." & CostoActivo & " as costo " & _
                            "from ma_productos where c_Codigo ='" & texto_cod & "'"
                            
                            RsRecetas.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            Call Apertura_Recordset(rsPmoneda)
                            
                            rsPmoneda.Open "select * from ma_monedas " & _
                            "where c_CodMoneda = '" & RsRecetas!c_CodMoneda & "'", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                            
                            Monto = 0
                            
                            For Cont = 1 To .Rows - 1
                                If .TextMatrix(Cont, ColumnaPreparado.Producto) <> Empty Then
                                    Monto = Monto + (CDbl(IIf(.TextMatrix(Cont, ColumnaPreparado.CostoPre) = "", 0, _
                                    .TextMatrix(Cont, ColumnaPreparado.CostoPre))) _
                                    * CDbl(IIf(.TextMatrix(Cont, ColumnaPreparado.Canti) = "", 0, _
                                    .TextMatrix(Cont, ColumnaPreparado.Canti))))
                                End If
                            Next Cont
                            
                            txt_monto.Text = FormatNumber(Monto, rsPmoneda!n_Decimales)
                            
                            Merma = 0
                            
                            For Cont = 1 To .Rows - 1
                                If .TextMatrix(Cont, ColumnaPreparado.Producto) <> Empty Then
                                    Merma = Merma + ((CDbl(.TextMatrix(Cont, ColumnaPreparado.CostoPreXMerma)) _
                                    * CDbl(.TextMatrix(Cont, ColumnaPreparado.Canti))))
                                End If
                            Next Cont
                            
                            txt_merma.Text = FormatNumber(Merma, Std_Decm)
                            
                            If Not mRecalculandoIngredientes And Not ManejaMermaExplicita And .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                oTeclado.Key_Down
                            End If
                            
                            MTotal = FormatNumber(CDbl(txt_monto.Text) + _
                            IIf(CDbl(txt_costosdirec.Text) >= 0, CDbl(txt_costosdirec.Text), 0) _
                            + CDbl(txt_merma.Text), Std_Decm)
                            
                            If Not mRecalculandoIngredientes Then
                                Call DistTotal(MTotal, Producir)
                            End If
                            
                            Call Cerrar_Recordset(rsPmoneda)
                            Call Cerrar_Recordset(RsRecetas)
                            
                            If Not mRecalculandoIngredientes Then
                                Call SetDefMSGrid(Preparado, .Row, ColumnaPreparado.PorcMerma)
                                txtedit.Text = 0
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        
                        If txtedit.Text <> Empty Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            Exit Sub
                        Else
                            .Col = ColumnaPreparado.Canti
                            .Text = 1
                            'SendKeys Chr$(13)
                            oTeclado.Key_Return
                        End If
                        
                    End If
                    
                End If
                
            Case ColumnaPreparado.PorcMerma 'Merma
                
                If Trim(txtedit.Text) <> "" And Tecla_Pulsada = False Then
                    If CheckCad(txtedit, 3, , False) Then
                        If CDbl(txtedit.Text) >= 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la celda actual
                            .Enabled = True
                            'mValorOld = CDbl(.Text)
                            .Text = FormatNumber(CDbl(txtedit.Text), 2)
                            'mCambio = (mValorOld <> CDbl(.Text))
                            .CellAlignment = flexAlignRightCenter
                            'BarraO.Buttons("Grabar").ButtonMenus("GESP").Enabled = True
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                '.Row = .Rows - 1
                                oTeclado.Key_Down
                            End If
                            
                            Call MSGridAsign(Preparado, .Row, ColumnaPreparado.Merma, FormatNumber(CDbl(txtedit.Text), Std_Decm), , flexAlignRightCenter)
                            Call MSGridAsign(Preparado, .Row, ColumnaPreparado.CostoPreXMerma, FormatNumber((CDbl(txtedit.Text) * CDbl(MSGridRecover(Preparado, .Row, ColumnaPreparado.CostoPre))) / 100, Std_Decm), , flexAlignRightCenter)
                            
                            Merma = 0
                            
                            For Cont = 1 To Preparado.Rows - 2
                                Merma = Merma + ((CDbl(MSGridRecover(Preparado, Cont, ColumnaPreparado.CostoPreXMerma)) _
                                * CDbl(MSGridRecover(Preparado, Cont, ColumnaPreparado.Canti))) * 1)
                            Next Cont
                            
                            txt_merma.Text = FormatNumber(Merma, Std_Decm)
                            
                            Call SetDefMSGrid(Preparado, .Row + 1, ColumnaPreparado.Producto)
                            
                            MTotal = FormatNumber(CDbl(txt_monto.Text) + _
                            IIf(CDbl(txt_costosdirec.Text) >= 0, CDbl(txt_costosdirec.Text), 0) _
                            + CDbl(txt_merma.Text), Std_Decm)
                            
                            'If mCambio Then
                                Call DistTotal(CDbl(MTotal), Producir)
                            'End If
                        
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor es negativo")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                        
                    Else
                        If txtedit.Text <> "" Then
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            txtedit.Enabled = True
                            txtedit.Text = ""
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            Exit Sub
                        End If
                    End If
                End If
                
            Case ColumnaPreparado.Producto
                
                If Tecla_Pulsada = True Then Exit Sub
                
                If Trim(txtedit.Text) <> "" Then
                    
                    mCodigoInput = Trim(txtedit.Text)
                    
                    txtedit.Text = AlternateCode(mCodigoInput)
                    
                    If Not Validar_Repeticiones(Preparado, txtedit.Text) _
                    And Not InSet(txtedit.Text, Producir) Then
                        
                        Set rsProducto = ScanData("SELECT * FROM MA_PRODUCTOS " & _
                        "LEFT JOIN MA_MONEDAS " & _
                        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
                        "WHERE c_Codigo = '" & Trim(txtedit.Text) & "' ")
                        
                        If Not rsProducto.EOF Then
                            
                            If rsProducto!n_Activo = 1 Then
                                
                                .Text = txtedit.Text
                                .Col = ColumnaPreparado.LDeci
                                .Text = rsProducto!Cant_Decimales
                                .Col = ColumnaPreparado.Descripci�n
                                .Text = rsProducto!c_Descri
                                .Col = ColumnaPreparado.CantOrden
                                .Text = FormatNumber(0, rsProducto!Cant_Decimales)
                                .Col = ColumnaProducir.CantTeorica
                                .Text = FormatNumber(0, rsProducto!Cant_Decimales)
                                .Col = ColumnaPreparado.Presentaci�n
                                .Text = IIf(IsNull(rsProducto!c_Presenta), "", rsProducto!c_Presenta)
                                .Col = ColumnaPreparado.PorcMerma
                                .Text = FormatNumber(rsProducto!nu_pocentajemerma, Std_DecC)
                                .Col = ColumnaPreparado.CostoPreXMerma
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * rsProducto!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm)
                                .Col = ColumnaPreparado.CostoPre
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * rsProducto!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm)
                                .Col = ColumnaPreparado.CostoProd
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * rsProducto!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm)
                                .Col = ColumnaPreparado.Cantibul
                                .Text = rsProducto!n_Cantibul
                                .Col = ColumnaPreparado.CostoOriginal
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * rsProducto!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm)
                                .Col = ColumnaPreparado.CodigoInput
                                .Text = mCodigoInput
                                .Col = ColumnaPreparado.DocumentoOrigen
                                .Text = Empty
                                .Col = ColumnaPreparado.Info
                                .Text = "(+)"
                                
                                .Col = ColumnaPreparado.Canti
                                'SendKeys Chr$(13)
                                oTeclado.Key_Return
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Producto no esta activo.")
                                Mensaje True, StellarMensaje(16101)
                                Tecla_Pulsada = False
                            End If
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = 1
                        End If
                        
                        rsProducto.Close
                        
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista.")
                        Mensaje True, StellarMensaje(2816)
                        Tecla_Pulsada = False
                        .Col = ColumnaPreparado.Producto
                        If .TextMatrix(.Row, .Col) <> Empty _
                        And .TextMatrix(.Row, ColumnaPreparado.Descripci�n) = Empty _
                        And .TextMatrix(.Row, ColumnaPreparado.Canti) = Empty Then
                            .Text = ""
                        End If
                    End If
                    
                Else
                    .Col = ColumnaPreparado.Producto
                End If
                
            Case ColumnaPreparado.Lote
            
ValidarLote:
                
                If MascaraParaLotes <> "*" Then
                    MascaraLote.Mask = MascaraParaLotes
                End If
                
                If MascaraParaLotes <> Empty Then
                    
                    On Error GoTo MskError
                    
                    If MascaraParaLotes <> "*" And Find_Concept = "PRD" Then
                        MascaraLote.Text = txtedit.Text
                    ElseIf MascaraParaLotes <> "*" And Find_Concept = "OPR" Then
                        If Trim(txtedit.Text) <> Empty Then
                            MascaraLote.Text = txtedit.Text
                        End If
                    End If
MskError:
                    If Err.Number = 0 Then
                        ' Aprobado
                        .TextMatrix(.Row, .Col) = txtedit.Text
                    Else
                        If Err.Number = 380 Then
                            Err.Clear
                            TmpDisableEvent = True
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            TmpDisableEvent = False
                            Tecla_Pulsada = False
                            txtedit.Enabled = True
                            txtedit.Visible = True
                            SafeFocus txtedit
                            .Enabled = False
                            ShowTooltip MascaraParaLotes, MaskWidth, 30000, txtedit, 3, _
                            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                            Exit Sub
                        End If
                    End If
                Else
                    .TextMatrix(.Row, .Col) = txtedit.Text
                End If
                
                ShowTooltip MascaraParaLotes, MaskWidth, 1, txtedit, 3, _
                &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                
        End Select
        
        txtedit.Visible = False
        txtedit.Text = ""
        
        If Tecla_Pulsada = False Then
            If .Col = ColumnaPreparado.Lote And ModifFlag Then TmpDisableEvent = True
            .Enabled = True
            '.Visible = True
            .SetFocus
        End If
        
        If .Col = ColumnaPreparado.Lote Then
            If .Row < .Rows - 1 Then
                .Row = .Row + 1
            Else
                Producir.Row = 1
                Producir.Col = ColumnaPreparado.Lote
                SafeFocus Producir
            End If
        End If
        
        'If .ColSel = ColumnaPreparado.Producto Then
            'BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = False
        'End If
        
    Else
        
        If .Col = ColumnaPreparado.Lote Then
            GoTo ValidarLote
        End If
                    
    End If
    
    End With
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Private Sub txtedit2_Change()
    If ModifFlag Then
        Dim Lcol As Integer, Lrow As Integer
        Producir.Enabled = True
        If Producir.Col = ColumnaPreparado.Canti Then
            Lrow = Producir.Row
            Lcol = Producir.Col
            If txtEdit2.Visible Then
                'If CheckCad(txtedit2, CDbl(MSGridRecover(Producir, Producir.Row, 0)), , True) Then
                'End If
            End If
            Call SetDefMSGrid(Producir, Lrow, Lcol)
        End If
        'Producir.Enabled = False
    End If
End Sub

Private Sub txtedit2_GotFocus()
    If ModifFlag Then
        If Producir.Col = ColumnaProducir.Producto Then
            Who_Ami = 2
            BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = True
        End If
    End If
End Sub

Private Sub txtedit2_KeyPress(KeyAscii As Integer)
    If ModifFlag Then
        Select Case KeyAscii
            Case vbKeyReturn
                'oTeclado.Key_Tab
                Call txtedit2_LostFocus
            Case vbKeyEscape
                Call Producir_KeyPress(vbKeyEscape)
        End Select
    End If
End Sub

Private Sub txtEdit2_KeyDown(KeyCode As Integer, Shift As Integer)
    If ModifFlag Then
    Else
        Select Case KeyCode
            Case Is = vbKeyReturn
                txtEdit2.Visible = False
                With Producir
                    Select Case .Col
                        Case ColumnaProducir.Factor 'fACTOR cOSTO
                        .Enabled = True
                        If Trim(txtEdit2.Text) <> "" And Tecla_Pulsada = False Then
                            If InStr(1, txtEdit2, "%") <> 0 Then
                                If Preparado.Rows > 3 Then
                                    Tecla_Pulsada = True
                                    'Call Mensaje(True, "No puede escribir cantidades porcentuales .")
                                    Mensaje True, StellarMensaje(2815)
                                    Tecla_Pulsada = False
                                    Exit Sub
                                Else
                                    txtEdit2.Text = FormatNumber(1, 2)
                                End If
                            End If
                            If CheckCad(txtEdit2, 2, , False) Then
                                If CDbl(txtEdit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                                    'Cambia la Celda actual
                                    'se usar�n dos (2) digitos decimales para los porcentajes...!
                                    '
                                    .Text = FormatNumber(txtEdit2.Text, 2)
                                    .CellAlignment = flexAlignRightCenter
                                    
                                    Call SetDefMSGrid(Producir, .Row, ColumnaProducir.Factor)
                                    Diferencia = 0
                                    Call DistTotal(CDbl(MTotal), Producir)
                                    If .Row = .Rows - 1 Then
                                        .Rows = .Rows + 1
                                        oTeclado.Key_Down
                                    End If
                                    Call SetDefMSGrid(Producir, .Row + 1, ColumnaProducir.Producto)
                                    txtEdit2.Text = ""
                                Else
                                    ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                                    Tecla_Pulsada = True
                                    'Call Mensaje(True, "Valor no puede ser cero")
                                    Mensaje True, StellarMensaje(2815)
                                    Tecla_Pulsada = False
                                    txtEdit2.Enabled = True
                                    txtEdit2.Text = ""
                                    txtEdit2.Visible = True
                                    SafeFocus txtEdit2
                                    .Enabled = False
                                    Exit Sub
                                End If
                            Else
                                If txtEdit2.Text <> "" Then
                                    .Enabled = False
                                    Tecla_Pulsada = True
                                    'Call Mensaje(True, "Dato de entrada incorrecto")
                                    Mensaje True, StellarMensaje(2815)
                                    Tecla_Pulsada = False
                                    txtEdit2.Enabled = True
                                    txtEdit2.Text = ""
                                    txtEdit2.Visible = True
                                    SafeFocus txtEdit2
                                    Exit Sub
                                Else
                                    .Col = ColumnaProducir.Factor
                                    .Text = FormatNumber(1, 2)
                                    oTeclado.Key_Return
                                End If
                            End If
                        End If
                        
'                    Case ColumnaProducir.Lote
'
'                        .Enabled = True
'
'                        If Trim(txtedit2.Text) <> "" And Tecla_Pulsada = False Then
'                            If InStr(1, txtedit2, "%") <> 0 Then
'                                If Preparado.Rows > 3 Then
'                                    Tecla_Pulsada = True
'                                    'Call Mensaje(True, "No puede escribir cantidades porcentuales .")
'                                    Mensaje True, StellarMensaje(2815)
'                                    Tecla_Pulsada = False
'                                    Exit Sub
'                                Else
'                                    txtedit2.Text = FormatNumber(1, 2)
'                                End If
'                            End If
'                            If CheckCad(txtedit2, 2, , False) Then
'                                If CDbl(txtedit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
'                                    'Cambia la Celda actual
'                                    'se usar�n dos (2) digitos decimales para los porcentajes...!
'                                    '
'                                    .Text = FormatNumber(txtedit2.Text, 2)
'                                    .CellAlignment = flexAlignRightCenter
'                                    If (.Row <= .Rows - 1) Then 'FINAL TABLA
'                                        If .TextMatrix(.Row + 1, 0) = "" Then ' TAMBIEN SE CONSIDERA FINAL
'                                            'Call MostrarTxt2(columnaProducir.canti, 1)
'                                            'Debug.Print "sql"
'                                            Me.txt_destino.SetFocus
'                                        Else
'                                            Call MostrarTxt2(ColumnaProducir.Canti, .Row + 1)
'                                        End If
'                                    Else
'                                        Call MostrarTxt2(ColumnaProducir.Canti, .Row + 1)
'                                    End If
'                                Else
'                                    Call MostrarTxt2(ColumnaProducir.Canti, .Row + 1)
'                                End If
'                            End If
'                        End If
                        
                    Case ColumnaProducir.Canti
                    
                    .Enabled = True
                    If Trim(txtEdit2.Text) <> "" And Tecla_Pulsada = False Then
                        If InStr(1, txtEdit2, "%") <> 0 Then
                            If Producir.Rows > 3 Then
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "No puede escribir cantidades porcentuales con preparados de f�rmulas con m�s de un producto.")
                                Mensaje True, StellarMensaje(2815)
                                Tecla_Pulsada = False
                                Exit Sub
                            Else
                                txtEdit2.Text = FormatNumber(CDbl(MSGridRecover(Producir, 1, ColumnaPreparado.Producto)) _
                                * (CDbl(Mid(txtEdit2.Text, 1, InStr(1, txtEdit2.Text, "%") - 1)) / 100), _
                                CDbl(MSGridRecover(Producir, .Row, ColumnaProducir.LDeci)))
                            End If
                        End If
                        If CheckCad(txtEdit2, CDbl(MSGridRecover(Producir, .Row, ColumnaProducir.LDeci)), , True) Then
                            If CDbl(txtEdit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                                'Cambia la Celda actual
                                'se usar�n dos (2) digitos decimales para los porcentajes...!
                                '
                                .Text = FormatNumber(txtEdit2.Text, CDbl(MSGridRecover(Producir, .Row, ColumnaProducir.LDeci)))
                                .CellAlignment = flexAlignRightCenter
                                Call SetDefMSGrid(Producir, .Row, ColumnaProducir.CostoProd)
                                Diferencia = 0
                                filaUsada = .Row
                                Call DistTotal(CDbl(MTotal), Producir)
                                .Row = filaUsada
                                If .Row = .Rows - 1 Then
                                    .Rows = .Rows + 1
                                    oTeclado.Key_Down
                                End If
                                Call SetDefMSGrid(Producir, .Row + 1, ColumnaProducir.Producto)
                                txtEdit2.Text = ""
                                
                                'Call MostrarTxt2(ColumnaProducir.CantiAlterna, CInt(filaUsada))
                            Else
                                ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Valor no puede ser cero")
                                Mensaje True, StellarMensaje(2815)
                                Tecla_Pulsada = False
                                txtEdit2.Enabled = True
                                txtEdit2.Text = ""
                                txtEdit2.Visible = True
                                SafeFocus txtEdit2
                                .Enabled = False
                                Exit Sub
                            End If
                        Else
                            If txtEdit2.Text <> "" Then
                                .Enabled = False
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "Dato de entrada incorrecto")
                                Mensaje True, StellarMensaje(2815)
                                Tecla_Pulsada = False
                                txtEdit2.Enabled = True
                                txtEdit2.Text = ""
                                txtEdit2.Visible = True
                                SafeFocus txtEdit2
                                Exit Sub
                            Else
                                .Col = ColumnaProducir.Canti
                                .Text = 1
                                'SendKeys Chr$(13)
                                oTeclado.Key_Return
                            End If
                        End If
                    End If
                    End Select
                End With
        End Select
    End If
    Exit Sub
Errores:
    txtEdit2.Text = ""
    txtEdit2.Visible = False
End Sub

Private Sub txtedit2_LostFocus()
    
    If TmpDisableEvent Then TmpDisableEvent = False: Exit Sub
        
    On Error GoTo Errores
    
    'If ProcesandoCambio Then
        'ProcesandoCambio = False
        'Exit Sub
    'End If
    
    With Producir
    
    If ModifFlag Then
        
        Dim Mensajes As String, RsEureka As New ADODB.Recordset, _
        ColAct As Integer, FilAct As Integer, Merma As Double, Monto As Double, Diferencia As Double
        
        Select Case .Col
            
            Case ColumnaProducir.Canti
                
                .Enabled = True
                
                If Trim(txtEdit2.Text) <> "" And Tecla_Pulsada = False Then
                    If InStr(1, txtEdit2, "%") <> 0 Then
                        If .Rows > 3 Then
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "No puede escribir cantidades porcentuales con preparados de f�rmulas con m�s de un producto.")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            Exit Sub
                        Else
                            txtEdit2.Text = FormatNumber(CDbl(.TextMatrix(1, ColumnaProducir.Canti)) _
                            * (CDbl(Mid(txtEdit2.Text, 1, InStr(1, txtEdit2.Text, "%") - 1)) / 100), _
                            CDbl(.TextMatrix(.Row, ColumnaProducir.LDeci)))
                        End If
                    End If
                    
                    If CheckCad(txtEdit2, CDbl(.TextMatrix(.Row, ColumnaProducir.LDeci)), , True) Then
                        If CDbl(txtEdit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la Celda actual
                            'se usar�n dos (2) digitos decimales para los porcentajes...!
                            '
                            .Text = FormatNumber(txtEdit2.Text, _
                            CDbl(.TextMatrix(.Row, ColumnaProducir.LDeci)))
                            .CellAlignment = flexAlignRightCenter
                            
                            If FrmAppLink.OPRConFormula_Recalcular And FormulaOrigenOPR <> Empty Then
                                .TextMatrix(.Row, ColumnaPreparado.CantTeorica) = .Text
                            End If
                            
                            Call Apertura_Recordset(RsRecetas)
                            'Call SetDefMSGrid(Producir, .Row, 5)
                            Diferencia = 0
                            
                            If Not mRecalculandoIngredientes And FrmAppLink.OPRConFormula_Recalcular And FormulaOrigenOPR <> Empty Then
                                
                                Dim NewCantOrigen, FactorMulti, mCantFormula, NewCantDestino
                                
                                mFilaActual = .Row: mColActual = .Col
                                
                                mCantFormula = BuscarValorBD("n_Cantidad", _
                                "SELECT n_Cantidad FROM TR_PRODUCCION " & _
                                "WHERE c_Formula = '" & FormulaOrigenOPR & "' " & _
                                "AND b_Producir = 1 " & _
                                "AND c_CodProducto = '" & .TextMatrix(mFilaActual, ColumnaProducir.Producto) & "' ", _
                                -1, Ent.BDD)
                                
                                If mCantFormula >= 0 Then
                                    
                                    mRecalculandoIngredientes = True
                                    
                                    NewCantOrigen = CDbl(.Text)
                                    FactorMulti = (NewCantOrigen / mCantFormula)
                                    
                                    For I = 1 To Preparado.Rows - 1
                                        If Preparado.TextMatrix(I, ColumnaPreparado.Producto) <> Empty _
                                        And Preparado.TextMatrix(I, ColumnaPreparado.Canti) <> Empty Then
                                            mCantFormula = BuscarValorBD("n_Cantidad", _
                                            "SELECT n_Cantidad FROM TR_PRODUCCION " & _
                                            "WHERE c_Formula = '" & FormulaOrigenOPR & "' " & _
                                            "AND b_Producir = 0 " & _
                                            "AND c_CodProducto = '" & Preparado.TextMatrix(I, ColumnaPreparado.Producto) & "' ", _
                                            -1, Ent.BDD)
                                            If mCantFormula >= 0 Then
                                                NewCantDestino = (mCantFormula * FactorMulti)
                                                DoEvents
                                                Preparado.Row = I
                                                Preparado.Col = ColumnaPreparado.Canti
                                                txtedit.Text = Round(NewCantDestino, Val(Preparado.TextMatrix(Preparado.Row, ColumnaPreparado.LDeci)))
                                                txtEdit_LostFocus
                                            End If
                                        End If
                                    Next
                                    
                                    For I = 1 To Producir.Rows - 1
                                        If I <> mFilaActual Then
                                            If .TextMatrix(I, ColumnaProducir.Producto) <> Empty _
                                            And .TextMatrix(I, ColumnaProducir.Canti) <> Empty Then
                                                mCantFormula = BuscarValorBD("n_Cantidad", _
                                                "SELECT n_Cantidad FROM TR_PRODUCCION " & _
                                                "WHERE c_Formula = '" & FormulaOrigenOPR & "' " & _
                                                "AND b_Producir = 1 " & _
                                                "AND c_CodProducto = '" & .TextMatrix(I, ColumnaProducir.Producto) & "' ", _
                                                -1, Ent.BDD)
                                                If mCantFormula >= 0 Then
                                                    NewCantDestino = (mCantFormula * FactorMulti)
                                                    DoEvents
                                                    .Row = I
                                                    txtEdit2.Text = Round(NewCantDestino, Val(.TextMatrix(.Row, ColumnaProducir.LDeci)))
                                                    txtedit2_LostFocus
                                                End If
                                            End If
                                        End If
                                    Next
                                    
                                    mRecalculandoIngredientes = False
                                    
                                    .Row = mFilaActual
                                    .Col = mColActual
                                    
                                End If
                                
                            End If
                            
                            If Not mRecalculandoIngredientes Then
                                
                                Call DistTotal(CDbl(MTotal.Text), Producir)
                                
                                If .Row = .Rows - 1 Then
                                    .Rows = .Rows + 1
                                    oTeclado.Key_Down
                                End If
                                
                                Call SetDefMSGrid(Producir, .Row + 1, ColumnaProducir.Producto)
                                txtEdit2.Text = Empty
                                
                            End If
                            
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                    Else
                        If txtEdit2.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            Exit Sub
                        Else
                            .Col = ColumnaProducir.Canti
                            .Text = 1
                            'SendKeys Chr$(13)
                            oTeclado.Key_Return
                        End If
                    End If
                    
                End If
                
            Case ColumnaProducir.Factor ' fACTOR cOSTO
                
                .Enabled = True
                
                If Trim(txtEdit2.Text) <> "" And Tecla_Pulsada = False Then
                    If InStr(1, txtEdit2, "%") <> 0 Then
                        If .Rows > 3 Then
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "No puede escribir cantidades porcentuales .")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            Exit Sub
                        Else
                            txtEdit2.Text = FormatNumber(1, 2)
                        End If
                    End If
                    
                    If CheckCad(txtEdit2, 2, , False) Then
                        If CDbl(txtEdit2.Text) > 0 And Trim(lbl_consecutivo.Caption) <> "" Then
                            'Cambia la Celda actual
                            'se usar�n dos (2) digitos decimales para los porcentajes...!
                            '
                            .Text = FormatNumber(txtEdit2.Text, 2)
                            .CellAlignment = flexAlignRightCenter
                            Call Apertura_Recordset(RsRecetas)

                            Call SetDefMSGrid(Producir, .Row, ColumnaProducir.CostoProd)
                            Diferencia = 0
                            Call DistTotal(CDbl(MTotal), Producir)
                            If .Row = .Rows - 1 Then
                                .Rows = .Rows + 1
                                oTeclado.Key_Down
                            End If
                            Call SetDefMSGrid(Producir, .Row + 1, ColumnaProducir.Producto)
                            txtEdit2.Text = ""
                        Else
                            ' MsgBox "Valor esta nulo", vbCritical, "Valor Nulo"
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Valor no puede ser cero")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            Exit Sub
                        End If
                    Else
                        If txtEdit2.Text <> "" Then
                            .Enabled = False
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Text = ""
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            Exit Sub
                        Else
                            .Col = ColumnaProducir.CostoProd
                            .Text = FormatNumber(1, 2)
                            oTeclado.Key_Return
                        End If
                    End If
                End If
            
            Case ColumnaProducir.Producto
                
                If Tecla_Pulsada = True Then Exit Sub
                
                If Trim(txtEdit2.Text) <> "" Then
                    
                    mCodigoInput = Trim(txtEdit2.Text)
                    
                    txtEdit2.Text = AlternateCode(mCodigoInput)
                    
                    If Not Validar_Repeticiones(Producir, txtEdit2.Text) _
                    And Not InSet(txtEdit2.Text, Preparado) _
                    And CDbl(txt_monto.Text) > 0 Then
                        
                        Set rsProducto = ScanData( _
                        "SELECT * FROM MA_PRODUCTOS " & _
                        "LEFT JOIN MA_MONEDAS " & _
                        "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
                        "WHERE c_Codigo = '" & Trim(txtEdit2.Text) & "' ")
                        
                        If Not rsProducto.EOF Then
                            If rsProducto!n_Activo = 1 Then
                                .Text = txtEdit2.Text
                                .Col = ColumnaProducir.LDeci
                                .Text = rsProducto!Cant_Decimales
                                .Col = ColumnaProducir.Descripci�n
                                .Text = rsProducto!c_Descri
                                .Col = ColumnaProducir.CantOrden
                                .Text = FormatNumber(0, rsProducto!Cant_Decimales)
                                .Col = ColumnaProducir.CantTeorica
                                .Text = FormatNumber(0, rsProducto!Cant_Decimales)
                                .Col = ColumnaProducir.Presentaci�n
                                .Text = IIf(IsNull(rsProducto!c_Presenta), _
                                "", rsProducto!c_Presenta)
                                .Col = ColumnaProducir.Lote
                                .Text = Empty
                                .Col = ColumnaProducir.CostoPre
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * rsProducto!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm)
                                .Col = ColumnaProducir.CostoProd
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * rsProducto!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm)
                                .Col = ColumnaProducir.CostoOriginal
                                .Text = FormatNumber((rsProducto.Fields(CostoActivo).Value * rsProducto!n_Factor) / CDbl(MSK_FACTOR.Text), Std_Decm)
                                .Col = ColumnaProducir.Factor 'factor de costo
                                .Text = FormatNumber(1, Std_Decm)
                                .Col = ColumnaProducir.Cantibul
                                .Text = rsProducto!n_Cantibul
                                .Col = ColumnaProducir.CodigoInput
                                .Text = mCodigoInput
                                .Col = ColumnaProducir.DocumentoOrigen
                                .Text = Empty
                                .Col = ColumnaPreparado.Info
                                .Text = "(+)"
                                
                                .Col = ColumnaProducir.Canti
                                oTeclado.Key_Return
                            Else
                                Tecla_Pulsada = True
                                'Call Mensaje(True, "El Producto no est� activo.")
                                Mensaje True, StellarMensaje(16161)
                                Tecla_Pulsada = False
                            End If
                            
                        Else
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Producto no Existe en la Base de Datos.")
                            Mensaje True, StellarMensaje(16165)
                            Tecla_Pulsada = False
                            .Col = ColumnaProducir.Producto
                        End If
                        
                        rsProducto.Close
                        txtEdit2.Text = ""
                        
                    Else
                        Tecla_Pulsada = True
                        'Call Mensaje(True, "El Producto ya se encuentra en la lista o el monto de la producci�n es 0.")
                        Mensaje True, StellarMensaje(2816)
                        Tecla_Pulsada = False
                        .Col = ColumnaProducir.Producto
                        If .TextMatrix(.Row, .Col) <> Empty _
                        And .TextMatrix(.Row, ColumnaProducir.Descripci�n) = Empty _
                        And .TextMatrix(.Row, ColumnaProducir.Canti) = Empty Then
                            .Text = ""
                        End If
                    End If
                Else
                    .Col = ColumnaProducir.Producto
                End If
                
            Case ColumnaProducir.Lote
                
ValidarLote:
                
                If MascaraParaLotes <> "*" Then
                    MascaraLote.Mask = MascaraParaLotes
                End If
                
                If MascaraParaLotes <> Empty Then
                    On Error GoTo MskError
                    If MascaraParaLotes <> "*" And Find_Concept = "PRD" Then
                        MascaraLote.Text = txtEdit2.Text
                    ElseIf MascaraParaLotes <> "*" And Find_Concept = "OPR" Then
                        If Trim(txtedit.Text) <> Empty Then
                            MascaraLote.Text = txtEdit2.Text
                        End If
                    End If
MskError:
                    If Err.Number = 0 Then
                        ' Aprobado
                        .TextMatrix(.Row, .Col) = txtEdit2.Text
                    Else
                        If Err.Number = 380 Then
                            Err.Clear
                            TmpDisableEvent = True
                            Tecla_Pulsada = True
                            'Call Mensaje(True, "Dato de entrada incorrecto")
                            Mensaje True, StellarMensaje(2815)
                            TmpDisableEvent = False
                            Tecla_Pulsada = False
                            txtEdit2.Enabled = True
                            txtEdit2.Visible = True
                            SafeFocus txtEdit2
                            .Enabled = False
                            ShowTooltip MascaraParaLotes, MaskWidth, 30000, txtEdit2, 3, _
                            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                            Exit Sub
                        End If
                    End If
                Else
                    .TextMatrix(.Row, .Col) = txtEdit2.Text
                End If
                
                Tecla_Pulsada = False
                
                ShowTooltip MascaraParaLotes, MaskWidth, 1, txtEdit2, 3, _
                &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "10", True)
                
        End Select
        
        If Not mRecalculandoIngredientes Then
            
            txtEdit2.Visible = False
            txtEdit2.Text = ""
            
            If Tecla_Pulsada = False Then
                If .Col = ColumnaProducir.Lote And ModifFlag Then TmpDisableEvent = True
                .Enabled = True
                SafeFocus Producir
            End If
            
            If .Col = ColumnaProducir.Lote Then
                If .Row < .Rows - 1 Then
                    .Row = .Row + 1
                Else
                    Preparado.Row = 1
                    Preparado.Col = ColumnaPreparado.Lote
                    SafeFocus Preparado
                End If
            End If
            
        End If
        
        'If .ColSel = ColumnaProducir.Producto Then
            'BarraO.Buttons("Buscar").ButtonMenus("BORD").Enabled = False
        'End If
        
    Else
        
        If .Col = ColumnaProducir.Lote Then
            GoTo ValidarLote
        End If
        
    End If
    
    End With
    
    Exit Sub
    
Errores:
    
    mRecalculandoIngredientes = False
    
    If FrmAppLink.DebugMode Then
        
        'Resume ' Debugging
        
        mErrorNumber = Err.Number
        mErrorDesc = Err.Description
        mErrorSource = Err.Source
        
        Resume SafeErrHandler
        
SafeErrHandler:
        
        On Error Resume Next
        
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
        
    End If
    
    txtEdit2.Text = Empty
    txtEdit2.Visible = False
    
End Sub

Function AlternateCode(ByVal Codigo As Variant) As Variant
    Dim RsCodigos As New ADODB.Recordset
    Call Apertura_RecordsetC(RsCodigos)
    RsCodigos.Open "select * from ma_codigos " & _
    "where c_Codigo = '" & Codigo & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsCodigos.EOF Then
        AlternateCode = RsCodigos!c_CodNasa
    Else
        AlternateCode = Codigo
    End If
    RsCodigos.Close
End Function

Function Validar_Repeticiones(Grid As MSFlexGrid, Txt As String) As Boolean
    Dim FilAct As Integer, ColAct As Integer
    Validar_Repeticiones = False
    With Grid
        ColAct = Grid.Col
        FilAct = Grid.Row
        .Enabled = True
        For Cont = 1 To Grid.Rows - 1
            If MSGridRecover(Grid, Cont, ColumnaProducir.Producto) = Txt Then
                Call SetDefMSGrid(Grid, FilAct, ColAct)
                'Call mensaje(True, "No puede Colocar el mismo producto mas de una vez.")
                .Enabled = False
                Validar_Repeticiones = True
            End If
        Next Cont
        Call SetDefMSGrid(Grid, FilAct, ColAct)
    End With
End Function

Function InSet(Producto As String, a As MSFlexGrid) As Boolean
    Dim Cont As Integer
    Cont = 1
    InSet = False
    While Not InSet And Cont <= a.Row
        If MSGridRecover(a, Cont, ColumnaProducir.Producto) = Producto Then
            InSet = True
        End If
        Cont = Cont + 1
    Wend
End Function

Function ScanData(SQLI As String) As ADODB.Recordset
    Dim RsTemp As New ADODB.Recordset
    Call Apertura_RecordsetC(RsTemp)
    RsTemp.Open SQLI, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsTemp.EOF Then
        Set ScanData = RsTemp
    Else
        Set ScanData = RsTemp
    End If
End Function

Function SumaGrid(Grid As MSFlexGrid, Columna As Integer) As Double
    Dim total As Double, Cont As Integer
    total = 0
    For Cont = 1 To Grid.Rows - 1
        If MSGridRecover(Grid, Cont, Columna) <> "" Then
            total = total + CDbl(MSGridRecover(Grid, Cont, Columna))
        End If
    Next Cont
    SumaGrid = total
End Function

Private Sub CboLnP_Click()
    
    Static PosAntL As Long
    
    mClsGrupos.cTipoGrupo = "LNP"
    
    If PosAntL <> CboLnP.ListIndex Or CboLnP.ListCount = 1 Then
        PosAntL = CboLnP.ListIndex
        If CboLnP.ListIndex = CboLnP.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboLnP
            mClsGrupos.CargarComboGrupos Ent.BDD, CboLnP
        End If
    End If
    
End Sub

Private Sub CboTurno_Click()
    
    Static PosAntT As Long
    
    mClsGrupos.cTipoGrupo = "TNP"
    
    If PosAntT <> CboTurno.ListIndex Or CboTurno.ListCount = 1 Then
        PosAntT = CboTurno.ListIndex
        If CboTurno.ListIndex = CboTurno.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CboTurno
            mClsGrupos.CargarComboGrupos Ent.BDD, CboTurno
        End If
    End If
    
End Sub

Sub Grabar_Orden(Tipo As String)
    
    Dim RsEureka As New ADODB.Recordset, RsInventarioCargar As New ADODB.Recordset, _
    RsInventarioDescargar As New ADODB.Recordset
    
    Dim RsDepositoCargo As New ADODB.Recordset, RsDepositoDescargo As New ADODB.Recordset, _
    RsMerma As New ADODB.Recordset, ConsecutivoAjuste As String
    
    Dim ActiveTrans As Boolean
    
    'On Error GoTo RecordFail
    'VALIDAR DATOS
    If CDbl(txt_monto.Text) < 0 Then
        'Call Mensaje(True, "El monto de producci�n no puede ser negativo.")
        Mensaje True, StellarMensaje(2809)
        Exit Sub
    ElseIf CDbl(txt_monto.Text) = 0 Then
        'Call Mensaje(True, "No hay datos suficientes para grabar." & vbNewLine & "Verifique que existen productos para la realizar la producci�n.")
        Mensaje True, Replace(StellarMensaje(2810), "$(Line)", vbNewLine)
        Exit Sub
    End If
    
    If Producir.TextMatrix(1, ColumnaProducir.Producto) = "" _
    Or Len(Producir.TextMatrix(1, ColumnaProducir.Producto)) <= 0 Then
        'Call Mensaje(True, "Faltan datos de los Productos a producir.")
        Mensaje True, StellarMensaje(359)
        Exit Sub
    End If
      
    If Trim(txt_destino.Text) = "" Then
        'Call Mensaje(True, "Debe colocar el dep�sito Origen, de donde se tomar�n los productos a utilizar.")
        Mensaje True, StellarMensaje(16134)
        Exit Sub
    End If
    
    If FrmAppLink.PRDRequiereLineaDeProduccion Then
        If Trim(CboLnP.Text) = Empty _
        Or UCase(Trim(CboLnP.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2964)
            SafeFocus CboLnP
            Exit Sub
        End If
    End If
    
    If FrmAppLink.PRDRequiereTurno Then
        If Trim(CboTurno.Text) = Empty _
        Or UCase(Trim(CboTurno.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2965)
            SafeFocus CboTurno
            Exit Sub
        End If
    End If
    
    Dim Cont As Integer
    
    For Cont = 1 To Preparado.Rows - 1
        If Preparado.TextMatrix(Cont, ColumnaPreparado.Producto) = Empty _
        And Preparado.TextMatrix(Cont, ColumnaPreparado.Canti) <> Empty Then
            Call Mensaje(True, Stellar_Mensaje(16165)) '"El C�digo del Producto no Existe.")
            Exit Sub
        End If
    Next
    
    Dim SumCantProducir As Double
    
    For Cont = 1 To Producir.Rows - 1
        If IsNumeric(Producir.TextMatrix(Cont, ColumnaProducir.Canti)) Then
            SumCantProducir = SumCantProducir + CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Canti))
        End If
        If SumCantProducir <= 0 Then
            Mensaje True, StellarMensaje(16103)
            Exit Sub
        End If
        If Producir.TextMatrix(Cont, ColumnaProducir.Producto) = Empty _
        And Producir.TextMatrix(Cont, ColumnaProducir.Canti) <> Empty Then
            Call Mensaje(True, Stellar_Mensaje(16165)) '"El C�digo del Producto no Existe.")
            Exit Sub
        End If
    Next
    
    'If ManejaLote Then ' En este caso no podemos ser tan restrictivos.
        
        'For Cont = 1 To Preparado.Rows - 1
            'If Preparado.TextMatrix(Cont, ColumnaPreparado.Producto) <> Empty _
            'And Trim(Preparado.TextMatrix(Cont, ColumnaPreparado.Lote)) = Empty Then
                'Mensaje True, Replace(StellarMensaje(2640), "$(RowNum)", Cont)
                'Exit Sub
            'End If
        'Next
        
        'For Cont = 1 To Producir.Rows - 1
            'If Producir.TextMatrix(Cont, ColumnaProducir.Producto) <> Empty _
            'And Trim(Producir.TextMatrix(Cont, ColumnaProducir.Lote)) = Empty Then
                'Mensaje True, Replace(StellarMensaje(2640), "$(RowNum)", Cont)
                'Exit Sub
            'End If
        'Next
        
    'End If
    
    If Trim(txt_destino.Text) <> Empty Then
        
        Call Apertura_Recordset(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_SUCURSALES " & _
        "WHERE c_Codigo = '" & txt_destino & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsEureka.EOF Then
            lbl_destino.Caption = RsEureka!c_Descripcion
            'txtEdit.SetFocus
        Else
            'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
            Mensaje True, StellarMensaje(16189)
            lcDeposito = ""
            lbl_destino.Caption = ""
            txt_destino.Text = ""
            Exit Sub
        End If
        
        Call Cerrar_Recordset(RsEureka)
        
    Else
        'Call Mensaje(True, "El c�digo del dep�sito Origen no es Valido.")
        Mensaje True, StellarMensaje(16189)
        lbl_origen.Caption = ""
        Exit Sub
    End If
    
    lcLocalidad = txt_destino.Text
    
    ' En esta etapa no aplican estas validaciones.
'    If tipo = "GORD" Then
'
'        If CambioPrecioAutomatico = 0 _
'        Or CambioPrecioAutomatico = 2 Then
'
'            For Cont = 1 To Producir.Rows - 1
'
'                Dim mCostoNew As Double, mCostoOrig As Double, _
'                mVerPrecios As Boolean, Visualizar As Integer
'
'                mCostoNew = Round((( _
'                CDbl(MSGridRecover(Producir, Cont, ColumnaProducir.CostoProd)) / _
'                CDbl(MSGridRecover(Producir, Cont, ColumnaProducir.Canti))) _
'                ), Std_Decm)
'
'                mCostoOrig = CDbl(MSGridRecover(Producir, Cont, ColumnaProducir.CostoOriginal))
'
'                If mCostoOrig <> mCostoNew Then
'
'                    Visualizar = 0
'
'                    '"Existe una variaci�n en el costo del Producto [$(Code)][$(Desc)]" & _
'                    "�Desea validar los precios de venta?"
'                    Mensaje False, Replace(Replace(StellarMensaje(2939), "$(Code)", MSGridRecover(Producir, Cont, ColumnaProducir.Producto)), _
'                    "$(Desc)", MSGridRecover(Producir, Cont, ColumnaProducir.Descripci�n))
'
'                    If Retorno Then
'                        Visualizar = 1
'                    Else
'                        Visualizar = -1
'                    End If
'
'                    If Visualizar = 1 Then
'
'                        MonedaDoc.BuscarMonedas , dbmoneda.Text
'                        newfrm_orden_reposicion.ReadOnly = True
'                        newfrm_orden_reposicion.CampoCostoComparar = CostoActivo
'                        newfrm_orden_reposicion.Codigo = MSGridRecover(Producir, Cont, ColumnaProducir.Producto)
'
'                        unitario = mCostoNew
'
'                        If CambioPrecioAutomatico = 2 Then
'                            newfrm_orden_reposicion.SoloAceptar = True
'                        End If
'
'                        newfrm_orden_reposicion.Show vbModal
'
'                        If CambioPrecioAutomatico = 0 Then
'                            If Not Ingresar Then Exit Sub
'                        End If
'
'                    End If
'
'                End If
'
'            Next
'
'        End If
        
        Dim Frm_Totalizar: Set Frm_Totalizar = FrmAppLink.GetFrmTotalizar
        
        With Frm_Totalizar
            
            .lbl_impuesto.Caption = FormatNumber(0)
            .lbl_subtotal.Caption = FormatNumber(CDbl(txt_monto.Text) + CDbl(txt_merma.Text), Std_Decm)
            .msk_bsdesc.Text = FormatNumber(0)
            .lbl_total.Caption = MTotal.Text
            .txtImpuesto = FormatNumber(0)
            
            .ManejaObservacion = True
            .Obsevacion_SoloLectura = False
            .ObservacionInicial = ObservacionInicial
            .ManejaOtrosCargos = True
            .RecalcularOtrosCargos = False
            .MostrarPorcentajeOtrosCargos = False
            .txtOtrosCargos.Text = _
            FormatNumber(CDbl(txt_costosdirec.Text), Std_Decm)
            
            .fFechaEstimadaOrigen = CDbl(FechaEstimadaPRD)
            .fFechaVencimientoOrigen = FechaVcto
            .fBackOrderOrigen = esBackOrder
            
            Frm_Totalizar.Show vbModal
            
        End With
        
        If FrmAppLink.GetLcGrabar = False Then
            Set Frm_Totalizar = Nothing
            Exit Sub
        End If
        
        FechaEstimadaPRD = Frm_Totalizar.fecha_recepcion
        
        If Frm_Totalizar.chk_vencimiento = vbChecked Then
            FechaVcto = CDbl(Frm_Totalizar.dtp_odc_vence)
        Else
            mFechaVcto = 0
        End If
        
        ObservacionInicial = Frm_Totalizar.txtObs.Text
        
        Set Frm_Totalizar = Nothing
        
    'End If
    
    Ent.BDD.BeginTrans: ActiveTrans = True
    
    Dim RowIsClean As Boolean
    
    For I = 1 To Preparado.Rows - 1
        RowIsClean = True
        For k = 0 To Preparado.Cols - 1
            If Preparado.TextMatrix(I, k) <> "" Then
                RowIsClean = False
                Exit For
            End If
        Next k
        If (RowIsClean And I > 1) Then Preparado.RemoveItem I
    Next I
    
    For I = 1 To Producir.Rows - 1
        RowIsClean = True
        For k = 0 To Producir.Cols - 1
            If Producir.TextMatrix(I, k) <> "" Then
                RowIsClean = False
                Exit For
            End If
        Next k
        If (RowIsClean And I > 1) Then Producir.RemoveItem I
    Next I
    
    If Tipo = "GORD" Then
        
        'GRABAR ORDEN
        
        lbl_consecutivo.Caption = Format(No_Consecutivo("Orden_Produccion_Nuevo"), "000000000")
        
        Call Apertura_RecordsetC(RsEureka)
        
        RsEureka.Open "SELECT * FROM MA_ORDEN_PRODUCCION " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Status = 'DPE' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsEureka.EOF Then
            
            RsEureka.AddNew
            
            Call Header_Orden_Produccion(RsEureka, "DPE", _
            FechaEstimadaPRD, FechaVcto)
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A UTILIZAR (DESCARGO)
            Call Apertura_RecordsetC(RsInventarioDescargar)
            
            RsInventarioDescargar.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND b_Producir = 0", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioDescargar.EOF Then
                Call Update_Utilizar_Orden(RsInventarioDescargar, Tipo)
            Else
                Call Mensaje(True, "Existe un detalle con el documento N� '" & lbl_consecutivo.Caption & "' ")
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
            'ACTUALIZAR EL DETALLE DE PRODUCTOS A PRODUCIR (CARGO)
            Call Apertura_RecordsetC(RsInventarioCargar)
            
            RsInventarioCargar.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
            "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
            "AND b_Producir = 1", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If RsInventarioCargar.EOF Then
                Call Update_Producir_Orden(RsInventarioCargar, Tipo)
            Else
                Call Mensaje(True, "Existe un detalle con el documento N� '" & lbl_consecutivo.Caption & "' ")
                Ent.BDD.RollbackTrans: ActiveTrans = False
                Exit Sub
            End If
            
        Else
            Call Mensaje(True, "La orden N� '" & lbl_consecutivo.Caption & "' existe en el Sistema.")
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
    ElseIf Tipo = "GESP" Then
        
        'GRABAR EN Espera
        
        If Mid(lbl_consecutivo.Caption, 1, 1) <> "E" Then
            lbl_consecutivo.Caption = "E" & Format(No_Consecutivo("Orden_Produccion_Nuevo_Espera"), "000000000")
        End If
        
        Call Apertura_RecordsetC(RsEureka)
        
        Ent.BDD.Execute "DELETE FROM MA_ORDEN_PRODUCCION WHERE c_Documento = '" & lbl_consecutivo.Caption & "' AND c_Status = 'DWT' "
        Ent.BDD.Execute "DELETE FROM TR_ORDEN_PRODUCCION WHERE c_Documento = '" & lbl_consecutivo.Caption & "' "
        
        RsEureka.Open "SELECT * FROM MA_ORDEN_PRODUCCION " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND c_Status = 'DWT' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsEureka.EOF Then
            RsEureka.AddNew
        End If
        
        Call Header_Orden_Produccion(RsEureka, "DWT", _
        FechaEstimadaPRD, FechaVcto)
        
        'ACTUALIZAR EL DETALLE DE PRODUCTOS A UTILIZAR (DESCARGO)
        Call Apertura_RecordsetC(RsInventarioDescargar)
        
        RsInventarioDescargar.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND b_Producir = 0", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsInventarioDescargar.EOF Then
            Call Update_Utilizar_Orden(RsInventarioDescargar, Tipo)
        Else
            Call Mensaje(True, "Existe un detalle con el documento N� '" & lbl_consecutivo.Caption & "' ")
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
        'ACTUALIZAR EL DETALLE DE PRODUCTOS A PRODUCIR (CARGO)
        Call Apertura_RecordsetC(RsInventarioCargar)
        
        RsInventarioCargar.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
        "WHERE c_Documento = '" & lbl_consecutivo.Caption & "' " & _
        "AND b_Producir = 1", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsInventarioCargar.EOF Then
            Call Update_Producir_Orden(RsInventarioCargar, Tipo)
        Else
            Call Mensaje(True, "Existe un detalle con el documento N� '" & lbl_consecutivo.Caption & "' ")
            Ent.BDD.RollbackTrans: ActiveTrans = False
            Exit Sub
        End If
        
    End If
    
    Ent.BDD.CommitTrans: ActiveTrans = False
    
    LcConsecu = lbl_consecutivo.Caption
    Find_Concept = "OPR"
    
    FrmAppLink.MostrarDocumentoStellar Find_Concept, FrmAppLink.GetFindStatus, LcConsecu
    
    Call Nuevo_Consecutivo
    Call Cancelar
    
    Exit Sub
    
RecordFail:
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans: ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Sub Header_Orden_Produccion(ByRef RsTemp As ADODB.Recordset, _
Status As String, ByVal pFechaProducir As Date, _
ByVal pFechaVcto As Double)
    
    With RsTemp
        
        Find_Status = Status
        !c_Documento = lbl_consecutivo.Caption
        !d_Fecha = FechaBD(Now, FBD_FULL, True)
        !c_Status = Find_Status
        !c_CodLocalidad = txt_destino.Text
        !c_CodMoneda = dbmoneda.Text
        !n_FactorCambio = CDbl(MSK_FACTOR.Text) 'CMM1N
        !n_Costodir = CDbl(txt_costosdirec.Text)
        !c_Observacion = ObservacionInicial
        !b_BackOrder = CBool(FrmAppLink.GetAceptaBackOrder)
        !c_Relacion = DocumentoRelacion
        !c_Formula = txt_preparado.Text
        !c_Descripcion = txt_descripcion.Text
        !c_Usuario = FrmAppLink.GetCodUsuario
        !d_Fecha_Produccion = FechaBD(pFechaProducir, , True)
        !n_Subtotal = CDbl(txt_monto.Text)
        !n_MontoMerma = CDbl(txt_merma.Text)
        !n_Descuento = 0
        !n_Impuesto = 0
        !n_Total = CDbl(MTotal)
        
        If pFechaVcto = 0 Then
            !d_FechaVencimiento = Null
        Else
            !d_FechaVencimiento = FechaBD(CDate(pFechaVcto), , True)
        End If
        
        !cs_CodLocalidad = FrmAppLink.GetCodLocalidadSistema
        !c_LineaProduccion = CboLnP.Text
        !c_Turno = CboTurno.Text
        
    End With
    
    RsTemp.UpdateBatch
    
End Sub

Sub Update_Utilizar_Orden(ByRef RsTemp As ADODB.Recordset, _
Tipo As String)
    
    Dim Cont As Integer, NewCont As Long
    
    For Cont = 1 To Preparado.Rows - 1
        
        mCant = Preparado.TextMatrix(Cont, ColumnaPreparado.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Documento = lbl_consecutivo.Caption
            !c_Linea = NewCont ' Cont
            !c_CodigoInput = Preparado.TextMatrix(Cont, ColumnaPreparado.CodigoInput)
            !c_Descripcion = Preparado.TextMatrix(Cont, ColumnaPreparado.Descripci�n)
            !c_Presenta = Preparado.TextMatrix(Cont, ColumnaPreparado.Presentaci�n)
            !c_CodArticulo = Preparado.TextMatrix(Cont, ColumnaPreparado.Producto)
            
            !n_Cantidad = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Canti)), _
            CLng(Preparado.TextMatrix(Cont, ColumnaPreparado.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre)), Std_Decm))
            !n_PorcMerma = CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.PorcMerma))
            !n_MontoMerma = CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPreXMerma) * !n_Cantidad)
            !n_Subtotal = CDbl(FormatNumber(CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoPre) * !n_Cantidad), Std_Decm))
            !n_Descuento = 0
            !n_Impuesto = 0
            !n_Total = Round(!n_Subtotal + !n_MontoMerma, 8)
            
            !b_Producir = 0
            
            !n_Cant_Utilizada = 0
            !n_Cant_Realizada = 0
            
            !Impuesto1 = 0
            !Impuesto2 = 0
            !Impuesto3 = 0
            
            !c_IDLote = Preparado.TextMatrix(Cont, ColumnaPreparado.Lote)
            !n_CostoOriginal = CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.CostoOriginal))
            !c_CodLocalidad = txt_destino.Text
            !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_Decimales = CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.LDeci))
            !n_Prod_Ext = vbNullString
            !ns_CantidadEmpaque = CDbl(Preparado.TextMatrix(Cont, ColumnaPreparado.Cantibul))
            !nu_FactorCosto = 1
            
        End With
        
        RsTemp.UpdateBatch
        
        End If
        End If
        
    Next Cont
    
End Sub

Sub Update_Producir_Orden(ByRef RsTemp As ADODB.Recordset, _
Tipo As String)
    
    Dim Cont As Integer, NewCont As Long
    
    For Cont = 1 To Producir.Rows - 1
        
        mCant = Producir.TextMatrix(Cont, ColumnaProducir.Canti)
        
        If IsNumeric(mCant) Then
        If mCant > 0 Then
        
        RsTemp.AddNew
        
        With RsTemp
            
            NewCont = NewCont + 1
            
            !c_Documento = lbl_consecutivo.Caption
            !c_Linea = NewCont ' Cont
            
            !c_CodigoInput = Producir.TextMatrix(Cont, ColumnaProducir.CodigoInput)
            !c_Descripcion = Producir.TextMatrix(Cont, ColumnaProducir.Descripci�n)
            !c_Presenta = Producir.TextMatrix(Cont, ColumnaProducir.Presentaci�n)
            !c_CodArticulo = Producir.TextMatrix(Cont, ColumnaProducir.Producto)
            
            !n_Cantidad = CDbl(FormatNumber(CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Canti)), _
            CLng(Producir.TextMatrix(Cont, ColumnaProducir.LDeci))))
            !n_Costo = CDbl(FormatNumber(CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoPre)), Std_Decm))
            !n_PorcMerma = 0
            !n_MontoMerma = 0
            !n_Subtotal = CDbl(FormatNumber(CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoPre) * !n_Cantidad), Std_Decm))
            !n_Descuento = 0
            !n_Impuesto = 0
            !n_Total = !n_Subtotal
            
            !b_Producir = 1
            
            !n_Cant_Utilizada = 0
            !n_Cant_Realizada = 0
            
            !Impuesto1 = 0
            !Impuesto2 = 0
            !Impuesto3 = 0
            
            !c_IDLote = Producir.TextMatrix(Cont, ColumnaProducir.Lote)
            !c_CodLocalidad = txt_destino.Text
            !n_CostoOriginal = CDbl(Producir.TextMatrix(Cont, ColumnaProducir.CostoOriginal))
            !c_MonedaProd = MonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_FactorMonedaProd = FactorMonedaProducto(!c_CodArticulo, Ent.BDD)
            !n_Decimales = CDbl(Producir.TextMatrix(Cont, ColumnaProducir.LDeci))
            !n_Prod_Ext = vbNullString
            !ns_CantidadEmpaque = CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Cantibul))
            !nu_FactorCosto = CDbl(Producir.TextMatrix(Cont, ColumnaProducir.Factor))
            
        End With
        
        RsTemp.UpdateBatch
        
        End If
        End If
        
    Next Cont
    
End Sub

Sub Reimprimir_Produccion(Tipo As String, Orden As String, _
Optional Numero As Integer = 0, _
Optional TipoImpresion As String)
    FrmAppLink.ReimprimirProduccion Tipo, Orden, Numero, TipoImpresion
End Sub

Function Center_Text(Texto As String, Ancho As Integer) As String
    Dim Lng As Integer
    Lng = Round((Ancho - Len(Texto)) / 2)
    If Lng <= 0 Then
        Center_Text = Texto
    Else
        Center_Text = Space(Lng) & Texto & Space(Lng)
        If Len(Center_Text) < Ancho Then
            Center_Text = Space(Ancho - Len(Center_Text)) & Center_Text
        End If
    End If
End Function

Function LeftJust(Texto As String, Ancho As Integer)
    Dim Lng As Integer
    Lng = Ancho - Len(Texto)
    If Lng <= 0 Then
        LeftJust = Texto
    Else
        LeftJust = Texto & Space(Lng)
    End If
End Function

Function RightJust(Texto As String, Ancho As Integer)
    Dim Lng As Integer
    Lng = Ancho - Len(Texto)
    If Lng <= 0 Then
        RightJust = Texto
    Else
        RightJust = Space(Lng) & Texto
    End If
End Function

Sub Escribir(Cadena As String, Reporte As Object, _
Seccion As String, Objeto As String, _
Optional RC As Boolean = True)
    Reporte.Sections(Seccion).Controls(Objeto).Caption = _
    Reporte.Sections(Seccion).Controls(Objeto).Caption & _
    Cadena & IIf(RC, vbNewLine, "")
End Sub

Sub Reimprimir_Orden_Produccion(ByVal Tipo As String, _
ByVal Orden As String, ByVal CodLocalidad As String, _
Optional ByVal Numero As Integer = 0, _
Optional ByVal TipoImpresion)
    FrmAppLink.ReimprimirOrdenProduccion Tipo, Orden, CodLocalidad, Numero, TipoImpresion
End Sub

Sub Anular_Orden_Produccion(Tipo As String, Orden As String, _
ByVal CodLocalidad As String, Optional pGrid As Object = Nothing)
    FrmAppLink.AnularOrdenProduccion Tipo, Orden, CodLocalidad, pGrid
End Sub

Sub Buscar_Orden_Produccion(ByVal Tipo As String, _
ByVal Orden As String, ByVal CodLocalidad As String)
    
    Call Apertura_RecordsetC(RsMaProduccion)
    
    RsMaProduccion.Open "SELECT * FROM MA_ORDEN_PRODUCCION " & _
    "WHERE c_Documento = '" & Orden & "' " & _
    "AND c_CodLocalidad = '" & CodLocalidad & "' " & _
    "AND c_Status = '" & FrmAppLink.GetFindStatus & "' ", _
    Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not RsMaProduccion.EOF Then
        
        Dim Cargar As Boolean
        
        'If Trim(RsMaProduccion!c_Formula) <> Empty Then
            'Cargar = Buscar_Formula(RsMaProduccion!c_Formula)
        'Else
            
            ' BUSCAR ORDEN DE PRODUCCION.
            
            Dim Cont As Integer
            Call Cancelar
            Cargar = False
            
            EstatusOPR = FrmAppLink.GetFindStatus
            
            'BUSCAR DETALLES Y CARGAR
            FormulaOrigenOPR = RsMaProduccion!c_Formula
            txt_preparado.Text = FormulaOrigenOPR
            txt_descripcion.Text = RsMaProduccion!c_Descripcion
            txt_costosdirec.Text = FormatNumber(RsMaProduccion!n_Costodir, Std_Decm)
            
            'MONEDA
            Call Apertura_Recordset(rsMonedas)
            rsMonedas.Open "SELECT * FROM MA_MONEDAS " & _
            "WHERE c_CodMoneda = '" & RsMaProduccion!c_CodMoneda & "' ", _
            Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsMonedas.EOF Then
                lbl_moneda.Caption = rsMonedas!c_Descripcion
                dbmoneda.Text = rsMonedas!c_CodMoneda
                Std_Decm = rsMonedas!n_Decimales
                MSK_FACTOR.Text = FormatNumber(rsMonedas!n_Factor, Std_Decm)
                lbl_simbolo.Caption = rsMonedas!c_Simbolo
            Else
                'Call Mensaje(True, "No se encontr� la moneda de la f�rmula.")
                Mensaje True, StellarMensaje(2806)
                Call Cancelar
                Exit Sub
            End If
            
            'A UTILIZAR
            Call Apertura_RecordsetC(RsTrProduccion)
            
            RsTrProduccion.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
            "LEFT JOIN MA_PRODUCTOS " & _
            "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
            "WHERE c_Documento = '" & Orden & "' AND b_Producir = 0 ", _
            Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            tmonto = 0
            TMerma = 0
            
            Dim mCantidadDisponible  As Double
            
            If Not RsTrProduccion.EOF Then
                
                Preparado.Rows = RsTrProduccion.RecordCount + 1
                
                For Cont = 1 To Preparado.Rows - 1
                    
                    If RsTrProduccion!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsTrProduccion!c_CodMoneda
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.LDeci, RsTrProduccion!Cant_Decimales)
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Producto, RsTrProduccion!c_Codigo, , flexAlignRightCenter)
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Descripci�n, RsTrProduccion!c_Descri, , flexAlignLeftCenter)
                        
                        If Find_Concept = "PRD" And EstatusOPR = "DPE" Then
                            Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CantOrden, FormatNumber(RsTrProduccion!n_Cantidad - RsTrProduccion!n_Cant_Utilizada, RsTrProduccion!Cant_Decimales), 1200, flexAlignRightCenter)
                            Preparado.ColWidth(ColumnaPreparado.Descripci�n) = (AnchoColDesc_Prepaparado - Preparado.ColWidth(ColumnaPreparado.CantOrden))
                            mCantidadDisponible = CDbl(MSGridRecover(Preparado, Cont, ColumnaPreparado.CantOrden))
                            If mCantidadDisponible < 0 Then
                                mCantidadDisponible = 0
                                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CantOrden, FormatNumber(0, RsTrProduccion!Cant_Decimales))
                            End If
                        Else
                            Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CantOrden, FormatNumber(0, RsTrProduccion!Cant_Decimales), 0, flexAlignRightCenter)
                            Preparado.ColWidth(ColumnaPreparado.Descripci�n) = AnchoColDesc_Prepaparado
                            mCantidadDisponible = RsTrProduccion!n_Cantidad
                            If mCantidadDisponible < 0 Then
                                mCantidadDisponible = 0
                                Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CantOrden, FormatNumber(0, RsTrProduccion!Cant_Decimales))
                            End If
                        End If
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Canti, FormatNumber(mCantidadDisponible, RsTrProduccion!Cant_Decimales), , flexAlignRightCenter)
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CantTeorica, FormatNumber(0, RsTrProduccion!Cant_Decimales), , flexAlignRightCenter)
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Presentaci�n, RsTrProduccion!c_Presenta, , flexAlignLeftCenter)
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Lote, RsTrProduccion!c_IDLote, , flexAlignRightCenter)
                        
                        If ManejaMermaExplicita Then
                            Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(RsTrProduccion!n_PorcMerma, Std_Decm), , flexAlignRightCenter)
                            Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber((RsTrProduccion!n_PorcMerma * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100, Std_Decm), , flexAlignRightCenter)
                        Else
                            Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Merma, FormatNumber(0, Std_Decm), , flexAlignRightCenter)
                            Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPreXMerma, FormatNumber(0, Std_Decm), , flexAlignRightCenter)
                        End If
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.PorcMerma, MSGridRecover(Preparado, Cont, ColumnaPreparado.Merma))
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoPre, FormatNumber(RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)), Std_Decm), , flexAlignRightCenter)
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoProd, FormatNumber((mCantidadDisponible * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))), Std_Decm), , flexAlignRightCenter)
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.Cantibul, RsTrProduccion!n_Cantibul, , flexAlignRightCenter)
                        
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CostoOriginal, MSGridRecover(Preparado, Cont, ColumnaPreparado.CostoPre), , flexAlignRightCenter)
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.CodigoInput, RsTrProduccion!c_CodigoInput, , flexAlignRightCenter)
                        Call MSGridAsign(Preparado, Cont, ColumnaPreparado.DocumentoOrigen, "OPR " & RsMaProduccion!c_Documento, , flexAlignRightCenter)
                        Preparado.TextMatrix(Cont, ColumnaPreparado.Info) = "(+)"
                        
                        tmonto = tmonto + (mCantidadDisponible * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text))))
                        
                        If ManejaMermaExplicita Then
                            TMerma = TMerma + (((RsTrProduccion!n_PorcMerma * (RsTrProduccion.Fields(CostoActivo).Value * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)))) / 100) * mCantidadDisponible)
                        End If
                        
                        RsTrProduccion.MoveNext
                        
                        txt_merma.Text = FormatNumber(TMerma, Std_Decm)
                        txt_monto.Text = FormatNumber(tmonto, Std_Decm)
                        MTotal.Text = FormatNumber(CDbl(txt_costosdirec.Text) + TMerma + tmonto, Std_Decm)
                        
                    Else
                        'Call Mensaje(True, "La f�rmula que desea utilizar '" & Formula & "' posee un producto inactivo.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Cargar = False
                        FlgIncoherente = False
                        Exit Sub
                    End If
                    
                Next Cont
                
            Else
                'Call Mensaje(True, "No se encontr� productos a utilizar de esta formula.")
                Mensaje True, StellarMensaje(359)
                Call Cancelar
                Exit Sub
            End If
            
            'A PRODUCIR
            Call Apertura_RecordsetC(RsProducir)
            
            RsProducir.Open "SELECT * FROM TR_ORDEN_PRODUCCION " & _
            "LEFT JOIN MA_PRODUCTOS " & _
            "ON MA_PRODUCTOS.c_Codigo = TR_ORDEN_PRODUCCION.c_CodArticulo " & _
            "LEFT JOIN MA_MONEDAS " & _
            "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
            "WHERE c_Documento = '" & Orden & "' AND b_Producir = 1 ", _
            Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            pMonto = 0
            
            If Not RsProducir.EOF Then
                
                Producir.Rows = RsProducir.RecordCount + 1
                
                For Cont = 1 To Producir.Rows - 1
                    
                    If RsProducir!n_Activo = 1 Then
                        
                        FrmAppLink.MonedaProd.BuscarMonedas , RsProducir!c_CodMoneda
                        
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.LDeci, RsProducir!Cant_Decimales)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.Producto, RsProducir!c_Codigo, , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.Descripci�n, RsProducir!c_Descri, , flexAlignLeftCenter)
                        
                        If Find_Concept = "PRD" And EstatusOPR = "DPE" Then
                            Call MSGridAsign(Producir, Cont, ColumnaProducir.CantOrden, FormatNumber(RsProducir!n_Cantidad - RsProducir!n_Cant_Realizada, RsProducir!Cant_Decimales), 1200, flexAlignRightCenter)
                            Producir.ColWidth(ColumnaProducir.Descripci�n) = (AnchoColDesc_Producir - Producir.ColWidth(ColumnaProducir.CantOrden))
                            mCantidadDisponible = CDbl(MSGridRecover(Producir, Cont, ColumnaProducir.CantOrden))
                            If mCantidadDisponible < 0 Then
                                mCantidadDisponible = 0
                                Call MSGridAsign(Producir, Cont, ColumnaProducir.CantOrden, FormatNumber(0, RsProducir!Cant_Decimales))
                            End If
                        Else
                            Call MSGridAsign(Producir, Cont, ColumnaProducir.CantOrden, FormatNumber(0, RsProducir!Cant_Decimales), 0, flexAlignRightCenter)
                            Producir.ColWidth(ColumnaProducir.Descripci�n) = AnchoColDesc_Producir
                            mCantidadDisponible = RsProducir!n_Cantidad
                            If mCantidadDisponible < 0 Then
                                mCantidadDisponible = 0
                                Call MSGridAsign(Producir, Cont, ColumnaProducir.CantOrden, FormatNumber(0, RsProducir!Cant_Decimales))
                            End If
                        End If
                        
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.CantTeorica, FormatNumber(0, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.Canti, FormatNumber(mCantidadDisponible, RsProducir!Cant_Decimales), , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.Presentaci�n, RsProducir!c_Presenta, , flexAlignLeftCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.Lote, RsProducir!c_IDLote, , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoPre, FormatNumber((RsProducir!n_Costo) / mCantidadDisponible, Std_Decm), , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoProd, FormatNumber((RsProducir!n_Costo), Std_Decm), , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.Factor, FormatNumber(RsProducir!nu_FactorCosto, 2), , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.Cantibul, CStr(RsProducir!n_Cantibul), , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.CostoOriginal, FormatNumber(RsProducir.Fields(CostoActivo) * (FrmAppLink.MonedaProd.FacMoneda / CDbl(MSK_FACTOR.Text)), Std_Decm), , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.CodigoInput, RsProducir!c_CodigoInput, , flexAlignRightCenter)
                        Call MSGridAsign(Producir, Cont, ColumnaProducir.DocumentoOrigen, "OPR " & RsMaProduccion!c_Documento, , flexAlignRightCenter)
                        Producir.TextMatrix(Cont, ColumnaProducir.Info) = "(+)"
                        
                        pMonto = pMonto + ((RsProducir!n_Costo * RsProducir!nu_FactorCosto))
                        
                        RsProducir.MoveNext
                        
                    Else
                        'Call Mensaje(True, "La f�rmula que desea utilizar '" & Formula & "' posee un producto inactivo.")
                        Mensaje True, Replace(StellarMensaje(2807), "$(Code)", Formula)
                        Call Cancelar
                        Cargar = False
                        FlgIncoherente = False
                        Exit Sub
                    End If
                Next Cont
                
                GridLleno = True
                Cargar = True
                
            Else
                'Call Mensaje(True, "No se encontr� productos a producir de esta formula.")
                Mensaje True, StellarMensaje(359)
                Call Cancelar
                Exit Sub
            End If
            
            Call DistTotal(CDbl(MTotal.Text), Producir)
            
            txt_costosdirec.Enabled = False
            
        'End If
        
        Preparado.Rows = Preparado.Rows + 1
        Producir.Rows = Producir.Rows + 1
        
        DocumentoRelacion = "OPR " & RsMaProduccion!c_Documento
        ObservacionInicial = "OPR N� " & RsMaProduccion!c_Documento & _
        vbNewLine & RsMaProduccion!c_Observacion
        esBackOrder = RsMaProduccion!b_BackOrder
        If Not isDBNull(RsMaProduccion!d_FechaVencimiento) Then
            FechaVcto = RsMaProduccion!d_FechaVencimiento
            DiasRestantes = DateDiff("d", Date, FechaVcto)
            If EstatusOPR = "DPE" And DiasRestantes >= 0 And DiasRestantes <= 1 Then
                Mensaje True, Replace(StellarMensaje(2956), "$(FV)", SDate(FechaVcto))
            End If
        Else
            FechaVcto = 0
        End If
        Call ListSafeItemSelection(CboLnP, RsMaProduccion!c_LineaProduccion)
        Call ListSafeItemSelection(CboTurno, RsMaProduccion!c_Turno)
        
        Nuevo_Consecutivo
        
        Call Activar_Data(True)
        txt_preparado.Enabled = False
        
        If EstatusOPR = "DPE" Then
            ModifFlag.Visible = False
            ModifFlag.Value = vbChecked
        End If
        
        If Find_Concept = "PRD" Then
            'Intentar ubicar deposito por defecto
            txt_destino.Text = BuscarValorBD("c_CodDeposito", _
            "SELECT TOP 1 c_CodDeposito FROM MA_DEPOSITO WHERE c_CodLocalidad = '" & RsMaProduccion!c_CodLocalidad & "' ORDER BY c_CodDeposito", Empty)
        Else
            txt_destino.Text = RsMaProduccion!c_CodLocalidad
        End If
        
        Call txt_destino_LostFocus
        
        If FrmAppLink.OPRConFormula_Recalcular Then
            Producir.Row = 1
            Producir.Col = ColumnaProducir.Canti
            mValor = Round(CDbl(Producir.Text), CDbl(Producir.TextMatrix(Producir.Row, ColumnaProducir.LDeci)))
            DoEvents
            txtEdit2.Text = mValor
            txtedit2_LostFocus
        End If
        
    Else
        'Call Mensaje(True, "No se encontr� la orden '" & orden & "' ")
        Mensaje True, Replace(StellarMensaje(2814), "$(Code)", Orden)
        Exit Sub
    End If
    
End Sub
