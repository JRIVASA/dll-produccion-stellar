VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsModProduccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Property Let Entrada(ByVal pValor As Object)
    Set FrmAppLink = pValor
    Set Ent.BDD = FrmAppLink.CnADM
    Set Ent.POS = FrmAppLink.CnPOS
    Set Ent.SHAPE_ADM = FrmAppLink.CnADMShape
    Set Ent.SHAPE_POS = FrmAppLink.CnPOSShape
    Set Ent.RsReglasComerciales = FrmAppLink.RsReglasComerciales
    Set RsEureka = FrmAppLink.GetRsEureka
    Set gCls = Me
End Property

Public Sub InterfazPlanificacion()
    FrmPlanificacionProduccion.Show vbModal
    Set FrmPlanificacionProduccion = Nothing
End Sub

Public Sub InterfazPlanificacionVentas()
    FrmPlanificacionProdVen.Show vbModal
    Set FrmPlanificacionProdVen = Nothing
End Sub

Public Sub Produccion_Manufactura_Formulas_Alimentos()
    FRM_PRODUCCION_FORMULA_1.Show vbModal
End Sub

Public Sub Produccion_Manufactura_OrdenProduccion_Alimentos()
    Find_Concept = FrmAppLink.GetFindConcept
    FRM_PRODUCCION_ORDEN_MANUAL_1.Show vbModal
End Sub

Public Sub Produccion_Manufactura_Produccion_Alimentos()
    Find_Concept = FrmAppLink.GetFindConcept
    FRM_PRODUCCION_ORDEN_MANUAL_3.Show vbModal
End Sub

Public Sub Reporte_Listado_Produccion_Alimentos()
    REP_ORDENES.Show vbModal
End Sub

Public Sub Reporte_Listado_Formulas_Alimentos()
    REP_ORDENES.ListadoDeFormulas = True
    REP_ORDENES.Show vbModal
End Sub

Public Sub Reporte_Listado_OrdenesProduccion_Alimentos()
    REP_ORDENES.ListadoDeOrdenes = True
    REP_ORDENES.Show vbModal
End Sub

Public Sub Reimprimir_Orden_Produccion(ByVal Tipo As String, _
ByVal Orden As String, ByVal CodLocalidad As String, _
Optional ByVal Numero As Integer = 0, _
Optional ByVal TipoImpresion)
    
    REP_ORDENES.ListadoDeOrdenes = True
    REP_ORDENES.ReimpresionDocumento = Orden
    REP_ORDENES.ReimpresionLocalidad = CodLocalidad
    REP_ORDENES.OpcionNumero = Numero
    
    If REP_ORDENES.Visible Then
        REP_ORDENES.PreviewDoc
    Else
        REP_ORDENES.Show vbModal
    End If
    
    Set REP_ORDENES = Nothing
    
End Sub

Public Sub Reimprimir_Produccion(Tipo As String, Orden As String, _
Optional Numero As Integer = 0, _
Optional TipoImpresion As String)
    
    REP_ORDENES.ReimpresionDocumento = Orden
    REP_ORDENES.OpcionNumero = Numero
    
    If REP_ORDENES.Visible Then
        REP_ORDENES.PreviewDoc
    Else
        REP_ORDENES.Show vbModal
    End If
    
    Set REP_ORDENES = Nothing
    
End Sub

