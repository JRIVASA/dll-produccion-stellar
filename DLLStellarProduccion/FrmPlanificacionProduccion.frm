VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmPlanificacionProduccion 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10710
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10710
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   3015
      Left            =   9480
      TabIndex        =   23
      Top             =   600
      Width           =   5655
      Begin VB.Frame FrameDiaInicioSemana 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Height          =   375
         Left            =   0
         TabIndex        =   35
         Top             =   2580
         Visible         =   0   'False
         Width           =   5415
         Begin VB.OptionButton OptDiaInicioSemana 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Lun"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   1
            Left            =   3840
            MaskColor       =   &H00585A58&
            TabIndex        =   37
            Top             =   60
            Value           =   -1  'True
            Width           =   800
         End
         Begin VB.OptionButton OptDiaInicioSemana 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Dom"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   0
            Left            =   2760
            MaskColor       =   &H00585A58&
            TabIndex        =   36
            Top             =   60
            Width           =   800
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "D�a Inicio de Semana"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   180
            TabIndex        =   38
            Top             =   105
            Width           =   2295
         End
      End
      Begin VB.OptionButton opt_Intervalo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Anual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   3
         Left            =   3060
         MaskColor       =   &H00585A58&
         TabIndex        =   33
         Top             =   2160
         Width           =   1455
      End
      Begin VB.OptionButton opt_Intervalo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Mensual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   2
         Left            =   3060
         MaskColor       =   &H00585A58&
         TabIndex        =   32
         Top             =   1680
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton opt_Intervalo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Semanal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   1
         Left            =   840
         MaskColor       =   &H00585A58&
         TabIndex        =   31
         Top             =   2160
         Width           =   1455
      End
      Begin VB.OptionButton opt_Intervalo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Diario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   0
         Left            =   840
         MaskColor       =   &H00585A58&
         TabIndex        =   30
         Top             =   1680
         Width           =   1215
      End
      Begin VB.TextBox txtFechaHasta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2820
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   840
         Width           =   2500
      End
      Begin VB.TextBox txtFechaDesde 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   25
         Top             =   840
         Width           =   2500
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   960
         X2              =   5000
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Intervalo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   29
         Top             =   1320
         Width           =   750
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   2820
         TabIndex        =   28
         Top             =   480
         Width           =   570
      End
      Begin VB.Label lblFechaInicio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   120
         TabIndex        =   26
         Top             =   480
         Width           =   615
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   840
         X2              =   5000
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Periodo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   24
         Top             =   120
         Width           =   645
      End
   End
   Begin VB.Frame FrameCargaMasiva 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   3015
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   9195
      Begin VB.CheckBox ChkItemsLineaProduccion 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Items de la L�nea"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   555
         Left            =   5520
         TabIndex        =   52
         Top             =   1800
         Width           =   1785
      End
      Begin VB.ComboBox CmbInsumos 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         ItemData        =   "FrmPlanificacionProduccion.frx":0000
         Left            =   1800
         List            =   "FrmPlanificacionProduccion.frx":0007
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1920
         Width           =   3645
      End
      Begin VB.ComboBox CmbActivos 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         ItemData        =   "FrmPlanificacionProduccion.frx":0012
         Left            =   1800
         List            =   "FrmPlanificacionProduccion.frx":0019
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   2400
         Width           =   3645
      End
      Begin VB.TextBox subgrupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   11
         Top             =   1440
         Width           =   1440
      End
      Begin VB.CommandButton bSubgrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "FrmPlanificacionProduccion.frx":0026
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1440
         Width           =   435
      End
      Begin VB.TextBox grupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   9
         Top             =   960
         Width           =   1440
      End
      Begin VB.CommandButton bGrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "FrmPlanificacionProduccion.frx":0828
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   960
         Width           =   435
      End
      Begin VB.TextBox departamento 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   7
         Top             =   480
         Width           =   1440
      End
      Begin VB.CommandButton bDpto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "FrmPlanificacionProduccion.frx":102A
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   480
         Width           =   435
      End
      Begin VB.Label lblDescBtnExcluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   8400
         TabIndex        =   45
         Top             =   2640
         Width           =   510
      End
      Begin VB.Label lblDescBtnIncluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Incluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   7620
         TabIndex        =   44
         Top             =   2640
         Width           =   495
      End
      Begin VB.Image CmdExcluir 
         Height          =   600
         Left            =   8400
         Picture         =   "FrmPlanificacionProduccion.frx":182C
         Stretch         =   -1  'True
         Top             =   1920
         Width           =   600
      End
      Begin VB.Image CmdIncluir 
         Height          =   600
         Left            =   7560
         Picture         =   "FrmPlanificacionProduccion.frx":5419
         Stretch         =   -1  'True
         Top             =   1920
         Width           =   600
      End
      Begin VB.Line LnCriterios 
         BorderColor     =   &H00AE5B00&
         X1              =   960
         X2              =   9000
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label lblCriterios 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   22
         Top             =   120
         Width           =   960
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Estatus"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   21
         Top             =   2400
         Width           =   720
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   19
         Top             =   1920
         Width           =   420
      End
      Begin VB.Label lblSubgrupo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Subgrupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   17
         Top             =   1440
         Width           =   915
      End
      Begin VB.Label LBL_SUBGRUPO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         TabIndex        =   16
         Top             =   1440
         UseMnemonic     =   0   'False
         Width           =   5250
      End
      Begin VB.Label LBL_GRUPO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         TabIndex        =   15
         Top             =   960
         UseMnemonic     =   0   'False
         Width           =   5250
      End
      Begin VB.Label LBL_DEPARTAMENTO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         TabIndex        =   14
         Top             =   480
         UseMnemonic     =   0   'False
         Width           =   5250
      End
      Begin VB.Label lblGrupo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   13
         Top             =   960
         Width           =   585
      End
      Begin VB.Label lblDpto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   12
         Top             =   480
         Width           =   1410
      End
   End
   Begin VB.Frame FrameGrid 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Excluir"
      Height          =   6855
      Left            =   120
      TabIndex        =   3
      Top             =   3720
      Width           =   15045
      Begin VB.CommandButton CmdResumenFecha 
         Caption         =   "Resumen de Fecha"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1200
         Left            =   12000
         Picture         =   "FrmPlanificacionProduccion.frx":60E3
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   120
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton CmdFichaLineaPrd 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   5520
         MaskColor       =   &H80000005&
         Picture         =   "FrmPlanificacionProduccion.frx":972D
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   720
         Width           =   675
      End
      Begin VB.ComboBox CboTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   8460
         Style           =   2  'Dropdown List
         TabIndex        =   49
         Top             =   900
         Width           =   2760
      End
      Begin VB.TextBox txtedit 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   180
         TabIndex        =   48
         Top             =   2040
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.ComboBox CboLnP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2175
         Style           =   2  'Dropdown List
         TabIndex        =   46
         Top             =   900
         Width           =   3180
      End
      Begin VB.CommandButton cmd_Cambiar 
         Caption         =   "Recargar Intervalo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1200
         Left            =   13500
         Picture         =   "FrmPlanificacionProduccion.frx":A3F7
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton cmd_sucursal 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   2760
         Picture         =   "FrmPlanificacionProduccion.frx":C179
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   300
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.TextBox txt_sucursal 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   1110
         TabIndex        =   39
         Top             =   300
         Visible         =   0   'False
         Width           =   1560
      End
      Begin MSFlexGridLib.MSFlexGrid Grid_Productos 
         Height          =   4875
         Left            =   120
         TabIndex        =   4
         Top             =   1740
         Width           =   14655
         _ExtentX        =   25850
         _ExtentY        =   8599
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Turno de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   6420
         TabIndex        =   50
         Top             =   900
         Width           =   1875
      End
      Begin VB.Label lblLineaProduccion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "L�nea de Producci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   120
         TabIndex        =   47
         Top             =   930
         Width           =   1875
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Sucursal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   42
         Top             =   420
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lbl_sucursal 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   3255
         TabIndex        =   41
         Top             =   300
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   2070
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00AE5B00&
         X1              =   1080
         X2              =   14880
         Y1              =   1500
         Y2              =   1500
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Productos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   34
         Top             =   1320
         Width           =   840
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   16920
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   7515
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12555
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmPlanificacionProduccion.frx":C97B
         Top             =   -30
         Width           =   480
      End
   End
End
Attribute VB_Name = "FrmPlanificacionProduccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private CodigoIngresado As String
Private FormaDeTrabajo As Integer
Private MostrarCantibul As Boolean
Private RequerirLineadeProduccion As Boolean
Private RequerirTurno As Boolean

Public DiasHabilesW  As Integer
Public DiasHabilesM  As Integer
Public DiasHabilesY  As Integer

Private TmpDisableEvent As Boolean
Private FormaCargada As Boolean
Private mClsGrupos As New ClsGrupos

Private pCargandoMasiva As Boolean

Private Enum GridCols
    ColLn
    ColCodigoInput
    ColCodProducto
    ColDescri
    ColPresentacion
    ColCantibul
    ColCapacidadEmpBase
    ColCapacidadUniBase
    ColCapacidadEmpIntervalo
    ColCapacidadUniIntervalo
    ColCantDec
    ColStockMinimo
    ColStockMaximo
    [FixedColCount]
End Enum

Private Const ColorUp As Long = &HFFC0C0
Private Const ColorNormal As Long = &HFAFAFA

Private CodLocalidadSel     As String
Private CboLnPSel           As String
Private CboTurnoSel         As String
Private CantidadIntervalos  As Long
Private ColsxInt            As Integer
Private DatosPeriodo        As Dictionary
Private DPTemp              As Dictionary
Private DPItem              As Dictionary
Private TipoIntervalo       As String

Dim nDesde As Double, nHasta As Double
Dim fDesde As Double, fHasta As Double

Private Sub CboLnP_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntL As Long
    
    mClsGrupos.cTipoGrupo = "LNP"
    
    If PosAntL <> CboLnP.ListIndex Or CboLnP.ListCount = 1 Then
        PosAntL = CboLnP.ListIndex
        If CboLnP.ListIndex = CboLnP.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo FrmAppLink.CnADM, CboLnP
            mClsGrupos.CargarComboGrupos FrmAppLink.CnADM, CboLnP
            Tecla_Pulsada = True
            ListRemoveItems CboLnP, "Ninguno"
            Tecla_Pulsada = False
        End If
    End If
    
End Sub

Private Sub CboTurno_Click()
    
    If Tecla_Pulsada Then Exit Sub
    
    Static PosAntT As Long
    
    mClsGrupos.cTipoGrupo = "TNP"
    
    If PosAntT <> CboTurno.ListIndex Or CboTurno.ListCount = 1 Then
        PosAntT = CboTurno.ListIndex
        If CboTurno.ListIndex = CboTurno.ListCount - 1 Then
            mClsGrupos.AgregarModificarGrupo FrmAppLink.CnADM, CboTurno
            mClsGrupos.CargarComboGrupos FrmAppLink.CnADM, CboTurno
            Tecla_Pulsada = True
            ListRemoveItems CboTurno, "Ninguno"
            Tecla_Pulsada = False
        End If
    End If
    
End Sub

Private Sub cmd_Cambiar_Click()
    
    On Error GoTo Error
    
    If nDesde <= 0 Or nHasta <= 0 Then
        Mensaje True, "Atenci�n, el rango del per�odo a mostrar es inv�lido."
        SafeFocus txtFechaDesde
        Exit Sub
    End If
    
    If Trim(txt_sucursal.Text) = Empty Then
        Mensaje True, StellarMensaje(16134)
        SafeFocus txt_sucursal
        Exit Sub
    End If
    
    If RequerirLineadeProduccion Then
        If Trim(CboLnP.Text) = Empty _
        Or UCase(Trim(CboLnP.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2964)
            SafeFocus CboLnP
            Exit Sub
        End If
    End If
    
    If RequerirTurno Then
        If Trim(CboTurno.Text) = Empty _
        Or UCase(Trim(CboTurno.Text)) = UCase("Ninguno") Then
            Mensaje True, StellarMensaje(2965)
            SafeFocus CboTurno
            Exit Sub
        End If
    End If
    
    Set DPTemp = New Dictionary
    
    Select Case True
        
        Case opt_Intervalo(0)
            TipoIntervalo = "D"
            CantidadIntervalos = DateDiff("d", fDesde, fHasta) + 1
        Case opt_Intervalo(1)
            TipoIntervalo = "W"
            CantidadIntervalos = DateDiff("d", fDesde, CDate(nHasta + 6)) + 1
            CantidadIntervalos = (CantidadIntervalos / 7)
        Case opt_Intervalo(2)
            TipoIntervalo = "M"
            CantidadIntervalos = DateDiff("m", fDesde, fHasta) + 1
        Case opt_Intervalo(3)
            TipoIntervalo = "Y"
            CantidadIntervalos = DateDiff("yyyy", fDesde, fHasta) + 1
            
    End Select
    
    ColsxInt = 4
    
    Select Case True
        Case opt_Intervalo(0)
            For I = 0 To CantidadIntervalos - 1
                Set DPItem = New Dictionary
                DPItem("nPeriodoIni") = CDbl(nDesde + I)
                DPItem("nPeriodoFin") = CDbl(nDesde + I)
                DPItem("fPeriodoIni") = CDate(DPItem("nPeriodoIni"))
                DPItem("fPeriodoFin") = CDate(DPItem("nPeriodoFin"))
                DPTemp.add I + 1, DPItem
            Next
        Case opt_Intervalo(1)
            For I = 0 To CantidadIntervalos - 1
                Set DPItem = New Dictionary
                DPItem("nPeriodoIni") = CDbl(nDesde + (I * 7))
                DPItem("nPeriodoFin") = CDbl(nDesde + (I * 7) + 6)
                DPItem("fPeriodoIni") = CDate(DPItem("nPeriodoIni"))
                DPItem("fPeriodoFin") = CDate(DPItem("nPeriodoFin"))
                DPTemp.add I + 1, DPItem
            Next
        Case opt_Intervalo(2)
            For I = 0 To CantidadIntervalos - 1
                Set DPItem = New Dictionary
                DPItem("nPeriodoIni") = CDbl(DateAdd("M", (I), fDesde))
                DPItem("nPeriodoFin") = CDbl(DateAdd("d", -1, DateAdd("M", 1, CDate(DPItem("nPeriodoIni")))))
                DPItem("fPeriodoIni") = CDate(DPItem("nPeriodoIni"))
                DPItem("fPeriodoFin") = CDate(DPItem("nPeriodoFin"))
                DPTemp.add I + 1, DPItem
            Next
        Case opt_Intervalo(3)
            For I = 0 To CantidadIntervalos - 1
                Set DPItem = New Dictionary
                DPItem("nPeriodoIni") = CDbl(DateAdd("YYYY", (I), fDesde))
                DPItem("nPeriodoFin") = CDbl(DateAdd("d", -1, DateAdd("YYYY", 1, CDate(DPItem("nPeriodoIni")))))
                DPItem("fPeriodoIni") = CDate(DPItem("nPeriodoIni"))
                DPItem("fPeriodoFin") = CDate(DPItem("nPeriodoFin"))
                DPTemp.add I + 1, DPItem
            Next
    End Select
    
    With Grid_Productos
        
        .Visible = False
        
        .Cols = FixedColCount + (ColsxInt * CantidadIntervalos)
        
        Dim CurrentCol As Long
        
        CurrentCol = (FixedColCount - 1)
        
        For I = 1 To CantidadIntervalos
            
            Set DPItem = DPTemp(I)
            .Row = 1
            
            .Col = CurrentCol + 1
            .ColWidth(.Col) = IIf(FormaDeTrabajo > 0, 1500, 0)
            .ColAlignment(.Col) = flexAlignRightCenter
            .CellAlignment = flexAlignCenterCenter
            
            Select Case TipoIntervalo
                Case "D"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Req.", "Requerido")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni")
                Case "W"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Req.", "Requerido")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni") & " a " & DPItem("fPeriodoFin")
                Case "M"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Req.", "Requerido")
                    .TextMatrix(0, .Col) = MonthName(Month(DPItem("fPeriodoIni")), False) & " " & Year(DPItem("fPeriodoIni"))
                Case "Y"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Req.", "Requerido")
                    .TextMatrix(0, .Col) = Year(DPItem("fPeriodoIni"))
            End Select
            
            .Row = 0
            .CellAlignment = flexAlignCenterCenter
            .Row = 1
            
            .Col = CurrentCol + 2
            .ColWidth(.Col) = IIf(FormaDeTrabajo <> 1, 1500, 0)
            .ColAlignment(.Col) = flexAlignRightCenter
            .CellAlignment = flexAlignCenterCenter
            
            Select Case TipoIntervalo
                Case "D"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Req.", "Requerido")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni")
                Case "W"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Req.", "Requerido")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni") & " a " & DPItem("fPeriodoFin")
                Case "M"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Req.", "Requerido")
                    .TextMatrix(0, .Col) = MonthName(Month(DPItem("fPeriodoIni")), False) & " " & Year(DPItem("fPeriodoIni"))
                Case "Y"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Req.", "Requerido")
                    .TextMatrix(0, .Col) = Year(DPItem("fPeriodoIni"))
            End Select
            
            DPItem("fPeriodo") = .TextMatrix(0, .Col)
            
            .Row = 0
            .CellAlignment = flexAlignCenterCenter
            .Row = 1
            
            .Col = CurrentCol + 3
            .ColWidth(.Col) = IIf(FormaDeTrabajo > 0, 1500, 0)
            .ColAlignment(.Col) = flexAlignRightCenter
            .CellAlignment = flexAlignCenterCenter
            
            Select Case TipoIntervalo
                Case "D"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni")
                Case "W"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni") & " a " & DPItem("fPeriodoFin")
                Case "M"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = MonthName(Month(DPItem("fPeriodoIni")), False) & " " & Year(DPItem("fPeriodoIni"))
                Case "Y"
                    .Text = IIf(FormaDeTrabajo = 2, "Emp. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = Year(DPItem("fPeriodoIni"))
            End Select
            
            DPItem("fNombrePeriodo") = .TextMatrix(0, .Col)
            
            .Row = 0
            .CellAlignment = flexAlignCenterCenter
            .Row = 1
            
            .Col = CurrentCol + 4
            .ColWidth(.Col) = IIf(FormaDeTrabajo <> 1, 1500, 0)
            .ColAlignment(.Col) = flexAlignRightCenter
            .CellAlignment = flexAlignCenterCenter
            
            Select Case TipoIntervalo
                Case "D"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni")
                Case "W"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = DPItem("fPeriodoIni") & " a " & DPItem("fPeriodoFin")
                Case "M"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = MonthName(Month(DPItem("fPeriodoIni")), False) & " " & Year(DPItem("fPeriodoIni"))
                Case "Y"
                    .Text = IIf(FormaDeTrabajo = 2, "Uni. Plan.", "Planificado")
                    .TextMatrix(0, .Col) = Year(DPItem("fPeriodoIni"))
            End Select
            
            .Row = 0
            .CellAlignment = flexAlignCenterCenter
            .Row = 1
            
            CurrentCol = CurrentCol + ColsxInt
            
        Next
        
        For I = 2 To .Rows - 1
            
            .Row = I
            
            If .TextMatrix(I, ColLn) <> Empty And .TextMatrix(I, ColCodProducto) <> Empty Then
                
                Dim mRs As ADODB.Recordset
                Set mRs = New Recordset
                
                Dim mSql As String, mCampos As String
                
                mCampos = IIf(Trim(CboLnP.Text) <> Empty _
                Or Trim(CboTurno.Text) <> Empty, _
                "*", "isNULL(SUM(nCapacidadEmpaques), 0) AS nCapacidadEmpaques, " & _
                "isNULL(SUM(nCapacidadUnidades), 0) AS nCapacidadUnidades")
                
                mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_CAPACIDAD_MAXIMA MA " & _
                "WHERE c_CodArticulo = '" & .TextMatrix(I, ColCodProducto) & "' " & _
                IIf(Trim(txt_sucursal.Text) <> Empty, _
                "AND c_CodLocalidad = '" & txt_sucursal.Text & "' ", Empty) & _
                IIf(Trim(CboLnP.Text) <> Empty, _
                "AND (c_LineaProduccion = '" & CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                IIf(Trim(CboTurno.Text) <> Empty, _
                "AND (c_Turno = '" & CboTurno.Text & "' OR c_Turno = '') ", Empty)
                
                Set mRs = FrmAppLink.CnADM.Execute(mSql)
                
                If Not mRs.EOF Then
                    
                    mEmpaques = mRs!nCapacidadEmpaques
                    mUnidades = mRs!nCapacidadUnidades
                    mTotal = (mEmpaques * CDbl(.TextMatrix(I, ColCantibul))) + mUnidades
                    mEmpaques = Fix(mTotal / CDbl(.TextMatrix(I, ColCantibul)))
                    mUnidades = Round(Round((mTotal / CDbl(.TextMatrix(I, ColCantibul))) - mEmpaques, 8) * CDbl(.TextMatrix(I, ColCantibul)), CantDec)
                    
                    If FormaDeTrabajo > 0 Then
                        .TextMatrix(I, ColCapacidadEmpBase) = mEmpaques
                    Else
                        .TextMatrix(I, ColCapacidadEmpBase) = 0
                    End If
                    
                    If FormaDeTrabajo <> 1 Then
                        .TextMatrix(I, ColCapacidadUniBase) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnidades), Val(.TextMatrix(I, ColCantDec)))
                    Else
                        .TextMatrix(I, ColCapacidadUniBase) = FormatNumber(0, Val(.TextMatrix(I, ColCantDec)))
                    End If
                    
                Else
                    .TextMatrix(I, ColCapacidadEmpBase) = 0
                    .TextMatrix(I, ColCapacidadUniBase) = FormatNumber(0, Val(.TextMatrix(I, ColCantDec)))
                End If
                
                mTotal = Round(CDbl(.TextMatrix(I, ColCapacidadUniBase)) + (CDbl(.TextMatrix(I, ColCapacidadEmpBase)) * CDbl(.TextMatrix(I, ColCantibul))), Val(.TextMatrix(I, ColCantDec)))
                
                Select Case TipoIntervalo
                    Case "D"
                        mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
                        mEmp = Fix(mEmpRaw)
                        mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
                        .Col = ColCapacidadEmpIntervalo
                        .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                        .Col = ColCapacidadUniIntervalo
                        .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                    Case "W"
                        mTotal = Round(mTotal * DiasHabilesW, Val(.TextMatrix(I, ColCantDec)))
                        mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
                        mEmp = Fix(mEmpRaw)
                        mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
                        .Col = ColCapacidadEmpIntervalo
                        .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                        .Col = ColCapacidadUniIntervalo
                        .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                    Case "M"
                        mTotal = Round(mTotal * DiasHabilesM, Val(.TextMatrix(I, ColCantDec)))
                        mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
                        mEmp = Fix(mEmpRaw)
                        mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
                        .Col = ColCapacidadEmpIntervalo
                        .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                        .Col = ColCapacidadUniIntervalo
                        .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                    Case "Y"
                        mTotal = Round(mTotal * DiasHabilesY, Val(.TextMatrix(I, ColCantDec)))
                        mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
                        mEmp = Fix(mEmpRaw)
                        mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
                        .Col = ColCapacidadEmpIntervalo
                        .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                        .Col = ColCapacidadUniIntervalo
                        .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                End Select
                
                mRs.Close
                
                CurrentCol = (FixedColCount - 1)
                
                For K = 1 To CantidadIntervalos
                    
                    mCampos = IIf(Trim(CboLnP.Text) <> Empty _
                    Or Trim(CboTurno.Text) <> Empty, _
                    "*", "isNULL(SUM(nEmpaquesRequerido), 0) AS nEmpaquesRequerido, " & _
                    "isNULL(SUM(nUnidadesRequerido), 0) AS nUnidadesRequerido")
                    
                    mSql = "SELECT " & mCampos & " FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS MA " & _
                    "WHERE c_CodArticulo = '" & .TextMatrix(I, ColCodProducto) & "' " & _
                    IIf(Trim(txt_sucursal.Text) <> Empty, "AND c_CodLocalidad = '" & txt_sucursal.Text & "' ", Empty) & _
                    IIf(Trim(CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & CboLnP.Text & "' OR c_LineaProduccion = '') ", Empty) & _
                    IIf(Trim(CboTurno.Text) <> Empty, "AND (c_Turno = '" & CboTurno.Text & "' OR c_Turno = '') ", Empty)
                    
                    Set DPItem = DPTemp(K)
                    
                    'Select Case TipoIntervalo
                        'Case "D"
                            mSql = mSql & " AND dFechaInicio BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND '" & FechaBD(EndOfDay(DPItem("fPeriodoFin")), FBD_FULL) & "' AND dFechaFin BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND '" & FechaBD(DPItem("fPeriodoFin"), FBD_FULL) & "' "
                        'Case "W"
                            'mSQL = mSQL & " AND dFechaInicio BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND dFechaFin = '" & FechaBD(DPItem("fPeriodoFin")) & "' "
                        'Case "M"
                            'mSQL = mSQL & " AND dFechaInicio BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND dFechaFin = '" & FechaBD(DPItem("fPeriodoFin")) & "' "
                        'Case "Y"
                            'mSQL = mSQL & " AND dFechaInicio BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND dFechaFin = '" & FechaBD(DPItem("fPeriodoFin")) & "' "
                    'End Select
                    
                    mSql = mSql & " AND cTipoPeriodo = '" & TipoIntervalo & "' "
                    
                    Set mRs = FrmAppLink.CnADM.Execute(mSql)
                    
                    If Not mRs.EOF Then
                        
                        mEmpaques = mRs!nEmpaquesRequerido
                        mUnidades = mRs!nUnidadesRequerido
                        mTotal = (mEmpaques * CDbl(.TextMatrix(I, ColCantibul))) + mUnidades
                        mEmpaques = Fix(mTotal / CDbl(.TextMatrix(I, ColCantibul)))
                        mUnidades = Round(Round((mTotal / CDbl(.TextMatrix(I, ColCantibul))) - mEmpaques, 8) * CDbl(.TextMatrix(I, ColCantibul)), CantDec)
                        
                        .Col = (CurrentCol + 1)
                        
                        If FormaDeTrabajo > 0 Then
                            .TextMatrix(I, .Col) = mEmpaques
                        Else
                            .TextMatrix(I, .Col) = 0
                        End If
                        
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                        
                        .Col = (CurrentCol + 2)
                        
                        If FormaDeTrabajo <> 1 Then
                            .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnidades), .TextMatrix(I, ColCantDec))
                        Else
                            .TextMatrix(I, .Col) = FormatNumber(0, Val(.TextMatrix(I, ColCantDec)))
                        End If
                        
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                        
                    Else
                        .Col = (CurrentCol + 1)
                        .TextMatrix(I, .Col) = 0
                        .CellBackColor = ColorNormal
                        .Col = (CurrentCol + 2)
                        .TextMatrix(I, .Col) = FormatNumber(0, .TextMatrix(I, ColCantDec))
                        .CellBackColor = ColorNormal
                    End If
                    
                    mRs.Close
                    
                    mSql = "SELECT isNULL(SUM(n_Cantidad), 0) AS CantPeriodo " & _
                    "FROM TR_ORDEN_PRODUCCION TR " & _
                    "INNER JOIN MA_ORDEN_PRODUCCION MA " & _
                    "ON TR.c_CodLocalidad = MA.c_CodLocalidad " & _
                    "AND TR.c_Documento = MA.c_Documento " & _
                    "WHERE b_Producir = 1 " & _
                    "AND c_CodArticulo = '" & .TextMatrix(I, ColCodProducto) & "' " & _
                    "AND MA.c_Status IN ('DPE', 'DCO') " & _
                    "AND MA.d_Fecha_Produccion BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' " & _
                    "AND '" & FechaBD(DPItem("fPeriodoFin"), FBD_FULL) & "' " & _
                    IIf(Trim(txt_sucursal.Text) <> Empty, _
                    "AND MA.c_CodLocalidad = '" & txt_sucursal.Text & "' ", Empty) & _
                    IIf(Trim(CboLnP.Text) <> Empty, "AND (c_LineaProduccion = '" & CboLnP.Text & "' or c_LineaProduccion = '') ", Empty) & _
                    IIf(Trim(CboTurno.Text) <> Empty, "AND (c_Turno = '" & CboTurno.Text & "' OR c_Turno = '') ", Empty)
                    
                    Set mRs = FrmAppLink.CnADM.Execute(mSql)
                    
                    If Not mRs.EOF Then
                        mTotal = Round(mRs!CantPeriodo, Val(.TextMatrix(I, ColCantDec)))
                        mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
                        mEmp = Fix(mEmpRaw)
                        mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
                        .Col = (CurrentCol + 3)
                        .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                        .Col = (CurrentCol + 4)
                        .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), .TextMatrix(I, ColCantDec))
                        .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
                    Else
                        .Col = (CurrentCol + 3)
                        .TextMatrix(I, .Col) = 0
                        .CellBackColor = ColorNormal
                        .Col = (CurrentCol + 4)
                        .TextMatrix(I, .Col) = FormatNumber(0, .TextMatrix(I, ColCantDec))
                        .CellBackColor = ColorNormal
                    End If
                    
                    mRs.Close
                    
                    CurrentCol = CurrentCol + ColsxInt
                    
                Next
                
            End If
            
        Next
        
        If TipoIntervalo <> "Y" Then
            CmdResumenFecha.Visible = True
        Else
            CmdResumenFecha.Visible = False
        End If
        
        CboLnPSel = CboLnP.Text
        CboTurnoSel = CboTurno.Text
        CodLocalidadSel = txt_sucursal.Text
        
        .Row = .Rows - 1
        .Col = ColCodProducto
        .Visible = True
        .Enabled = True
        SafeFocus Grid_Productos
        
    End With
    
    Exit Sub
    
Error:
    
    Grid_Productos.Visible = True
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub cmd_sucursal_Click()
    Call txt_sucursal_KeyDown(vbKeyF2, 0)
End Sub

Private Sub CmdEdit_Click()
    
End Sub

Private Sub CmdExcluir_Click()
    CargaMasiva True
End Sub

Private Sub CmdFichaLineaPrd_Click()
    If Trim(CboLnP) <> Empty Then
        FrmFichaLineaProduccion.Descripcion_PK = CboLnP.Text
        FrmFichaLineaProduccion.Show vbModal
        Set FrmFichaLineaProduccion = Nothing
    End If
End Sub

Private Sub CmdIncluir_Click()
    CargaMasiva
End Sub

Private Sub CargaMasiva(Optional ByVal pExcluir As Boolean = False)
    
    On Error GoTo Error
    
    Dim mRsDatos As ADODB.Recordset
    Set mRsDatos = New ADODB.Recordset
    Apertura_RecordsetC mRsDatos
    
    Dim mCriterio As String
    
    'mCriterio = mCriterio & _
    IIf(Trim(PROVEEDOR.Text) <> Empty, _
    "AND PRO.c_Codigo IN (SELECT c_Codigo FROM MA_PRODxPROV WHERE c_CodProvee = " & _
    "'" & QuitarComillasSimples(PROVEEDOR.Text) & "') ", Empty)
    
    mCriterio = mCriterio & _
    IIf(Trim(departamento.Text) <> Empty, _
    "AND PRO.c_Departamento = '" & QuitarComillasSimples(departamento.Text) & "' ", Empty)
    
    mCriterio = mCriterio & _
    IIf(Trim(grupo.Text) <> Empty, _
    "AND PRO.c_Grupo = '" & QuitarComillasSimples(grupo.Text) & "' ", Empty)
    
    mCriterio = mCriterio & _
    IIf(Trim(subgrupo.Text) <> Empty, _
    "AND PRO.c_Subgrupo = '" & QuitarComillasSimples(subgrupo.Text) & "' ", Empty)
    
    'mCriterio = mCriterio & _
    IIf(Trim(TxtMarca.Text) <> Empty, _
    IIf(TxtMarca.Text = "[]", "AND PRO.c_Marca = ''", _
    "AND PRO.c_Marca LIKE '" & QuitarComillasSimples(TxtMarca.Text) & "' "), Empty)
    
    'mCriterio = mCriterio & _
    IIf(Trim(TxtModelo.Text) <> Empty, _
    IIf(TxtModelo.Text = "[]", "AND PRO.c_Modelo = ''", _
    "AND PRO.c_Modelo LIKE '" & QuitarComillasSimples(TxtModelo.Text) & "' "), Empty)
    
    mCriterio = mCriterio & _
    IIf(CmbInsumos.ListIndex <> 0, _
    IIf(CmbInsumos.ListIndex = 1, "AND PRO.nu_InsumoInterno = 1 ", "AND PRO.nu_InsumoInterno = 0 "), Empty)
    
    mCriterio = mCriterio & _
    IIf(CmbActivos.ListIndex <> 0, _
    IIf(CmbActivos.ListIndex = 1, "AND PRO.n_Activo = 0 ", Empty), "AND PRO.n_Activo = 1 ")
    
    mCriterio = mCriterio & _
    IIf(ChkItemsLineaProduccion.Value = vbChecked, "AND c_Codigo IN (" & _
    "SELECT c_CodArticulo FROM MA_PRODUCCION_CAPACIDAD_MAXIMA MAX " & _
    "WHERE 1 = 1 AND c_LineaProduccion = '" & QuitarComillasSimples(CboLnP.Text) & "' " & _
    IIf(txt_sucursal <> Empty, _
    "AND c_CodLocalidad = '" & QuitarComillasSimples(txt_sucursal.Text) & "' ", Empty) & _
    "GROUP BY c_CodArticulo HAVING SUM(nCapacidadEmpaques + nCapacidadUnidades) > 0)", Empty)
    
    SQL = "SELECT * FROM MA_PRODUCTOS PRO " & _
    "WHERE 1 = 1 " & mCriterio
    
    mRsDatos.Open SQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
    
    If mRsDatos.RecordCount <= 0 Then
        Exit Sub
    End If
    
    pCargandoMasiva = True
    
    Grid_Productos.Row = Grid_Productos.Rows - 1
    
    'SafeFocus Grid_Productos
    Grid_Productos.Visible = False
    
    While Not mRsDatos.EOF
        
        'DoEvents
        
Recorrer:
        
        CodigoIngresado = mRsDatos!c_Codigo
        
        For I = 2 To Grid_Productos.Rows - 1
            If Grid_Productos.TextMatrix(I, ColCodProducto) = CodigoIngresado Then
                If pExcluir Then
                    Grid_Productos.Row = I
                    Grid_Productos_KeyDown vbKeyDelete, 0
                    Exit For
                Else
                    GoTo Continue
                End If
            End If
        Next
        
        If Not pExcluir Then
            With Grid_Productos
                
                .TextMatrix(.Row, ColLn) = .Rows - 1
                .TextMatrix(.Row, ColCodigoInput) = CodigoIngresado
                .TextMatrix(.Row, ColCodProducto) = CodigoIngresado
                .TextMatrix(.Row, ColDescri) = mRsDatos!c_Descri
                .TextMatrix(.Row, ColPresentacion) = mRsDatos!c_Presenta
                .TextMatrix(.Row, ColCantibul) = FormatNumber(mRsDatos!n_Cantibul, mRsDatos!Cant_Decimales)
                .TextMatrix(.Row, ColCantDec) = mRsDatos!Cant_Decimales
                .TextMatrix(.Row, ColStockMinimo) = mRsDatos!nu_StockMin
                .TextMatrix(.Row, ColStockMaximo) = mRsDatos!nu_StockMax
                
                .Rows = .Rows + 1
                .Row = .Rows - 1
                
            End With
        End If
        
Continue:
        
        mRsDatos.MoveNext
        
    Wend
    
    mRsDatos.Close
    
    If pExcluir Then
        For I = 2 To Grid_Productos.Rows - 1
            If Grid_Productos.TextMatrix(I, ColCodProducto) <> Empty Then
                Grid_Productos.TextMatrix(I, ColLn) = I - 1
            End If
        Next I
        If Grid_Productos.Rows = 2 And Grid_Productos.TextMatrix(2, ColCodProducto) <> Empty Then
            Grid_Productos.Rows = Grid_Productos.Rows + 1
            Grid_Productos.Row = Grid_Productos.Rows - 1
        End If
    End If
    
    pCargandoMasiva = False
    Grid_Productos.Visible = True
    SafeFocus Grid_Productos
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    pCargandoMasiva = False
    Grid_Productos.Visible = True
    SafeFocus Grid_Productos
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub CmdResumenFecha_Click()
    
    'On Error GoTo Error
    
    With Grid_Productos
                
            If .Col > (FixedColCount - 1) Then
                
                Intervalo = CDec(1 + (Round(CDbl(.Col) - CDbl(FixedColCount - 1), 8) / CDbl(ColsxInt)) - 0.000001)
                Intervalo = Fix(Intervalo)
                Set DPItem = DPTemp(Intervalo)
                IntervaloColIni = (FixedColCount - 1) + ((CLng(Intervalo) - 1) * ColsxInt)
                IntervaloColFin = IntervaloColIni + (ColsxInt)
                
                Dim Ver As Boolean
                Ver = True
                
'                Select Case .Col
'
'                    Case IntervaloColIni + 1 ' Emp. Req
'
'                    Case IntervaloColIni + 2 ' Uni. Req
'
'                    Case IntervaloColIni + 3 ' Emp. Plan
'
'                    Case IntervaloColIni + 4 ' Uni. Plan
'
'                End Select
                
            End If
            
        'End If
        
    End With
    
    If Ver Then
        
        With Grid_Productos
            
            If Not .TextMatrix(.Row, ColCapacidadEmpBase) = Empty And Not .TextMatrix(.Row, ColCapacidadUniBase) = Empty Then
                
                FrmVerPlanificacionIntervalosSuperiores.FormaDeTrabajo = FormaDeTrabajo
                FrmVerPlanificacionIntervalosSuperiores.MostrarCantibul = MostrarCantibul
                
                FrmVerPlanificacionIntervalosSuperiores.CodProducto = .TextMatrix(.Row, ColCodProducto)
                FrmVerPlanificacionIntervalosSuperiores.Descripcion = .TextMatrix(.Row, ColDescri)
                FrmVerPlanificacionIntervalosSuperiores.Presentacion = .TextMatrix(.Row, ColPresentacion)
                FrmVerPlanificacionIntervalosSuperiores.Periodo = DPItem("fPeriodo")
                FrmVerPlanificacionIntervalosSuperiores.TipoPeriodo = TipoIntervalo
                
                Set FrmVerPlanificacionIntervalosSuperiores.DatosPeriodo = DPItem
                
                FrmVerPlanificacionIntervalosSuperiores.CantDec = Val(.TextMatrix(.Row, ColCantDec))
                FrmVerPlanificacionIntervalosSuperiores.Cantibul = CDbl(.TextMatrix(.Row, ColCantibul))
                
                FrmVerPlanificacionIntervalosSuperiores.CapacidadEmp = .TextMatrix(.Row, ColCapacidadEmpIntervalo)
                FrmVerPlanificacionIntervalosSuperiores.CapacidadUni = .TextMatrix(.Row, ColCapacidadUniIntervalo)
                FrmVerPlanificacionIntervalosSuperiores.CapacidadEmpBase = CDbl(.TextMatrix(.Row, ColCapacidadEmpBase))
                FrmVerPlanificacionIntervalosSuperiores.CapacidadUniBase = CDbl(.TextMatrix(.Row, ColCapacidadUniBase))
                
                If FormaDeTrabajo = 1 Then
                    mTotal = CDbl(.TextMatrix(.Row, ColCapacidadEmpIntervalo))
                Else
                    mTotal = FormatNumber(CDbl(.TextMatrix(.Row, ColCapacidadUniIntervalo)) + _
                    (CDbl(.TextMatrix(.Row, ColCapacidadEmpIntervalo)) * FrmVerPlanificacionIntervalosSuperiores.Cantibul), _
                    FrmVerPlanificacionIntervalosSuperiores.CantDec)
                End If
                
                FrmVerPlanificacionIntervalosSuperiores.CapacidadPeriodo = mTotal
                
                Cantibul = FrmVerPlanificacionIntervalosSuperiores.Cantibul
                
                mEmpaques = CDbl(.TextMatrix(.Row, IntervaloColIni + 1))
                mUnidades = CDbl(.TextMatrix(.Row, IntervaloColIni + 2))
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                FrmVerPlanificacionIntervalosSuperiores.RequeridosPeriodo = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                
                mEmpaques = CDbl(.TextMatrix(.Row, IntervaloColIni + 3))
                mUnidades = CDbl(.TextMatrix(.Row, IntervaloColIni + 4))
                mTotal = (mEmpaques * Cantibul) + mUnidades
                mEmpaques = Fix(mTotal / Cantibul)
                mUnidades = Round(Round((mTotal / Cantibul) - mEmpaques, 8) * Cantibul, CantDec)
                
                FrmVerPlanificacionIntervalosSuperiores.PlanificadosPeriodo = IIf(FormaDeTrabajo = 2, mEmpaques & " " & "Empaques, " & mUnidades & " Unidades", IIf(FormaDeTrabajo = 1, mEmpaques & " " & "Empaques", mTotal & " " & "Unidades"))
                
                FrmVerPlanificacionIntervalosSuperiores.Show vbModal
                
                Set FrmVerPlanificacionIntervalosSuperiores = Nothing
                
            End If
            
        End With
        
    End If
    
End Sub

Private Sub Grid_Productos_KeyPress(KeyAscii As Integer)
    txtedit.Text = Chr(KeyAscii)
    txtedit.SelStart = Len(txtedit.Text)
    txtedit.SelLength = 0
    Grid_Productos_Click
End Sub

Private Sub txtedit_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtEdit_LostFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtedit.Text = Empty
        txtEdit_LostFocus
    ElseIf KeyCode = vbKeyF2 Then
        txtedit.Text = Empty
        txtEdit_LostFocus
        Grid_Productos_KeyDown vbKeyF2, 0
    End If
End Sub

Public Sub txtEdit_LostFocus()
    
    If TmpDisableEvent Then
        TmpDisableEvent = False
        Exit Sub
    End If
    
    If Trim(txtedit.Text = Empty) Then
        txtedit.Visible = False
        Grid_Productos.Enabled = True
        SafeFocus Grid_Productos
        Grid_Productos.Row = Grid_Productos.Rows - 1
        Grid_Productos.Col = GridCols.ColCodProducto
        Exit Sub
    End If
    
    CodigoIngresado = txtedit.Text
    
    Dim RsCodProd As ADODB.Recordset
    Set RsCodProd = New ADODB.Recordset
    Dim RsProducto As ADODB.Recordset
    Set RsProducto = New ADODB.Recordset
    Dim SQL As String
    Dim CodigoProd As String
    
    Apertura_Recordset RsProducto
    
    SQL = "SELECT * FROM MA_CODIGOS WHERE c_Codigo = '" & Trim(CodigoIngresado) & "' "
    
    RsCodProd.Open SQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not RsCodProd.EOF Then
        
        CodigoProd = RsCodProd!c_codnasa
        Cerrar_Recordset RsCodProd
        
        Apertura_Recordset RsProducto
        
        For I = 2 To Grid_Productos.Rows - 1
            If Grid_Productos.TextMatrix(I, ColCodProducto) = CodigoProd Then
                Call Mensaje(True, StellarMensaje(2816))
                CodigoProd = Empty
                txtedit.Text = CodigoProd
                Cerrar_Recordset RsProducto
                Grid_Productos.Enabled = True
                SafeFocus Grid_Productos
                Grid_Productos.Row = Grid_Productos.Rows - 1
                Grid_Productos.Col = GridCols.ColCodProducto
                Exit Sub
            End If
        Next
        
        SQL = "SELECT * FROM MA_PRODUCTOS WHERE c_Codigo = '" & CodigoProd & "' "
        RsProducto.Open SQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
        
        If RsProducto.EOF And RsProducto.BOF Then
            Call Mensaje(True, StellarMensaje(16100))
            CodigoProd = Empty
            txtedit.Text = CodigoProd
            Cerrar_Recordset RsProducto
            Grid_Productos.Enabled = True
            SafeFocus Grid_Productos
            Grid_Productos.Row = Grid_Productos.Rows - 1
            Grid_Productos.Col = GridCols.ColCodProducto
            Exit Sub
        End If
        
        With Grid_Productos
            
            .TextMatrix(.Row, ColLn) = .Rows - 1
            .TextMatrix(.Row, ColCodigoInput) = CodigoIngresado
            .TextMatrix(.Row, ColCodProducto) = CodigoProd
            .TextMatrix(.Row, ColDescri) = RsProducto!c_Descri
            .TextMatrix(.Row, ColPresentacion) = RsProducto!c_Presenta
            .TextMatrix(.Row, ColCantibul) = FormatNumber(RsProducto!n_Cantibul, RsProducto!Cant_Decimales)
            .TextMatrix(.Row, ColCantDec) = RsProducto!Cant_Decimales
            .TextMatrix(.Row, ColStockMinimo) = RsProducto!nu_StockMin
            .TextMatrix(.Row, ColStockMaximo) = RsProducto!nu_StockMax
            
            .Rows = .Rows + 1
            .Row = .Rows - 1
            
        End With
        
        TmpDisableEvent = True
        txtedit.Text = Empty
        txtedit.Visible = False
        Grid_Productos.Enabled = True
        SafeFocus Grid_Productos
        Grid_Productos.Row = Grid_Productos.Rows - 1
        Grid_Productos.Col = GridCols.ColCodProducto
        
    Else
        Call Mensaje(True, StellarMensaje(16100))
        CodigoProd = Empty
        txtedit.Text = CodigoProd
        Cerrar_Recordset RsProducto
        Grid_Productos.Enabled = True
        SafeFocus Grid_Productos
        Grid_Productos.Row = Grid_Productos.Rows - 1
        Grid_Productos.Col = GridCols.ColCodProducto
        Exit Sub
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        txt_sucursal.Text = FrmAppLink.GetCodLocalidadSistema()
        txt_sucursal_LostFocus
    End If
End Sub

Private Sub Form_Load()
    
    lbl_Organizacion.Caption = StellarMensaje(2963) ' Planificacion de Produccion
    lblDpto.Caption = StellarMensaje(3028) '"Departamento"
    lblGrupo.Caption = StellarMensaje(161) '"Grupo"
    lblSubgrupo.Caption = StellarMensaje(3029) '"SubGrupo"
    lblLineaProduccion.Caption = StellarMensaje(2947) ' LineaProduccion
    lblTurno.Caption = StellarMensaje(2966) ' Turno de Produccion
    
    FormaDeTrabajo = FrmAppLink.PRD_Plan_FormaDeTrabajo
    MostrarCantibul = FrmAppLink.PRD_Plan_MostrarCantibul
    RequerirLineadeProduccion = FrmAppLink.PRDRequiereLineaDeProduccion
    RequerirTurno = FrmAppLink.PRDRequiereTurno
    DiasHabilesW = FrmAppLink.PRD_Plan_DiasHabilesW
    DiasHabilesM = FrmAppLink.PRD_Plan_DiasHabilesM
    DiasHabilesY = FrmAppLink.PRD_Plan_DiasHabilesY
    
    mClsGrupos.cTipoGrupo = "LNP"
    mClsGrupos.CargarComboGrupos FrmAppLink.CnADM, CboLnP
    
    Tecla_Pulsada = True
    ListRemoveItems CboLnP, "Ninguno"
    Tecla_Pulsada = False
    
    mClsGrupos.cTipoGrupo = "TNP"
    mClsGrupos.CargarComboGrupos FrmAppLink.CnADM, CboTurno
    
    Tecla_Pulsada = True
    ListRemoveItems CboTurno, "Ninguno"
    Tecla_Pulsada = False
    
    CmbInsumos.Clear
    CmbInsumos.AddItem StellarMensaje(2545)
    CmbInsumos.AddItem StellarMensaje(3024)
    CmbInsumos.AddItem StellarMensaje(2961)
    CmbInsumos.ListIndex = 0
    
    CmbActivos.Clear
    CmbActivos.AddItem StellarMensaje(16475)
    CmbActivos.AddItem StellarMensaje(16227)
    CmbActivos.AddItem StellarMensaje(2545)
    CmbActivos.ListIndex = 0
    
    Call ListSafeIndexSelection(CboLnP, 0)
    Call ListSafeIndexSelection(CboTurno, 0)
    
    CargarGrid
    FrmAppLink.CargarMonedaPref
    
    AjustarPantalla Me
    
End Sub

Private Sub Frame11_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub CargarGrid()
                
    With Grid_Productos
        
        .Clear
        
        .Rows = 3 ' 2 Fixed
        .FixedRows = 2
        
        .RowHeight(0) = 320
        .RowHeight(1) = .RowHeight(0)
        .RowHeightMin = 640
        
        .SelectionMode = flexSelectionFree
        .MergeCells = flexMergeRestrictRows
        .Cols = FixedColCount
        
        .Row = 0
        .Col = GridCols.ColLn
        .Text = "Ln"
        .ColWidth(.Col) = 500
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCodigoInput
        .Text = "CodigoInput"
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCodProducto
        .Text = "C�digo"
        .ColWidth(.Col) = 1500
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColDescri
        .Text = "Descripci�n"
        .ColWidth(.Col) = 3500
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColPresentacion
        .Text = FrmAppLink.PRDLabelCampoPresenta
        .ColWidth(.Col) = 1500
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCantibul
        .Text = "CantXEmp"
        .ColWidth(.Col) = IIf(MostrarCantibul, 1200, 0)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCapacidadEmpBase
        .Text = "Capacidad (Emp)"
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCapacidadUniBase
        .Text = "Capacidad (Uni)"
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCapacidadEmpIntervalo
        .Text = "Capacidad (Emp)"
        .ColWidth(.Col) = IIf(FormaDeTrabajo > 0, 1500, 0)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCapacidadUniIntervalo
        .Text = "Capacidad (Uni)"
        .ColWidth(.Col) = IIf(FormaDeTrabajo <> 1, 1500, 0)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColCantDec
        .Text = Empty
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColStockMinimo
        .Text = Empty
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .Row = 0
        .Col = GridCols.ColStockMaximo
        .Text = Empty
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .Row = 1
        .CellAlignment = flexAlignCenterCenter
        .TextMatrix(1, .Col) = .TextMatrix(0, .Col)
        .MergeCol(.Col) = True
        
        .ScrollBars = flexScrollBarBoth
        .ScrollTrack = True
        .WordWrap = True
        .AllowUserResizing = flexResizeNone
        
        .MergeRow(0) = True
        .MergeRow(1) = True
        
        .Row = 2
        .RowSel = .Row
        .Col = GridCols.ColCodProducto
        .ColSel = .Col
        
        .Enabled = True
        
    End With
    
End Sub

Private Sub Grid_DblClick()
    FrmPlanificacionxProducto.Show vbModal
End Sub

Private Sub Grid_Productos_Click()
    
    With Grid_Productos
        
        If .Col = ColCodProducto And .TextMatrix(.Row, ColCodProducto) = Empty Then
            
            txtedit.Left = .CellLeft + .Left
            txtedit.Top = .CellTop + .Top
            txtedit.Height = .CellHeight
            txtedit.Width = .CellWidth
            'txtedit.SelStart = 0
            'txtedit.SelLength = Len(txtedit.Text)
            txtedit.Visible = True
            txtedit.Enabled = True
            txtedit.SetFocus
            .Enabled = False
            
        ElseIf .TextMatrix(.Row, ColCodProducto) <> Empty Then
            
            'Grid_Productos_DblClick
            
        End If
        
    End With
    
End Sub

Private Sub Grid_Productos_DblClick()
    
    On Error GoTo Error
    
    Dim TransActive As Boolean
    
    With Grid_Productos
        
        If .Col = ColCodProducto _
        And .TextMatrix(.Row, ColCodProducto) = Empty Then
            
            txtedit.Left = .CellLeft + .Left
            txtedit.Top = .CellTop + .Top
            txtedit.Height = .CellHeight
            txtedit.Width = .CellWidth
            'txtedit.SelStart = 0
            'txtedit.SelLength = Len(txtedit.Text)
            txtedit.Visible = True
            txtedit.Enabled = True
            txtedit.SetFocus
            .Enabled = False
            
        ElseIf .TextMatrix(.Row, ColLn) <> Empty _
        And .TextMatrix(.Row, ColCodProducto) <> Empty Then
            
            Dim ValorColStr, ValorCol, NewValorCol
            
            ValorColStr = .TextMatrix(.Row, .Col)
            
            If .Col = ColCapacidadEmpIntervalo Then
                
                If Not IsNumeric(ValorColStr) Then
                    Mensaje True, "Debe cargar la informaci�n del intervalo antes de modificar este dato."
                    Exit Sub
                End If
                
                ValorCol = CDbl(ValorColStr)
                
                If (Trim(CboLnPSel) = Empty) Then
                    NewValorCol = QuickInputRequest("" & "Indique la Capacidad Maxima por D�a en Empaques sin especificar l�nea de producci�n." & "", _
                    True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
                Else
                    NewValorCol = QuickInputRequest("" & "Indique la Capacidad Maxima por D�a en Empaques para esta l�nea de producci�n." & "", _
                    True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
                End If
                
                If NewValorCol = "[**Cancel**]" Then Exit Sub
                
                If IsNumeric(NewValorCol) Then
                    
                    If CDbl(NewValorCol) >= 0 Then
                        
                        FrmAppLink.CnADM.BeginTrans: TransActive = True
                        
                        FrmAppLink.CnADM.Execute "DELETE FROM MA_PRODUCCION_CAPACIDAD_MAXIMA " & _
                        "WHERE c_CodArticulo = '" & .TextMatrix(.Row, ColCodProducto) & "' " & _
                        IIf(Trim(CodLocalidadSel) <> Empty, _
                        "AND c_CodLocalidad = '" & CodLocalidadSel & "' ", Empty) & _
                        IIf(Trim(CboLnPSel) <> Empty, _
                        "AND (c_LineaProduccion = '" & CboLnPSel & "') ", Empty)
                        
                        FrmAppLink.CnADM.Execute "INSERT INTO MA_PRODUCCION_CAPACIDAD_MAXIMA " & _
                        "(c_CodArticulo, c_CodLocalidad, c_LineaProduccion, " & _
                        "nCapacidadEmpaques, nCapacidadUnidades, cTipoFrecuencia, " & _
                        "nCantidadFrecuencia) VALUES ('" & .TextMatrix(.Row, ColCodProducto) & "', " & _
                        "'" & CodLocalidadSel & "', '" & CboLnPSel & "', (" & Round(NewValorCol, 0) & "), " & _
                        "(" & Round(CDbl(.TextMatrix(.Row, ColCapacidadUniBase)), _
                        Val(.TextMatrix(.Row, ColCantDec))) & "), 'D', 1)"
                        
                        FrmAppLink.CnADM.CommitTrans: TransActive = False
                        
                        .TextMatrix(.Row, ColCapacidadEmpBase) = Round(NewValorCol, 0)
                        
                        RecalcularCapacidadIntervalo .TextMatrix(.Row, ColCapacidadEmpBase), .TextMatrix(.Row, ColCapacidadUniBase)
                        
                    Else
                        Mensaje True, "La cantidad no puede ser negativa."
                    End If
                    
                Else
                    Mensaje True, StellarMensaje(16136)
                End If
                
            ElseIf .Col = ColCapacidadUniIntervalo Then
                
                If Not IsNumeric(ValorColStr) Then
                    Mensaje True, "Debe cargar la informaci�n del intervalo antes de modificar este dato."
                    Exit Sub
                End If
                
                ValorCol = CDbl(ValorColStr)
                
                If (Trim(CboLnPSel) = Empty) Then
                    NewValorCol = QuickInputRequest("" & "Indique la Capacidad Maxima por D�a en Unidades sin especificar l�nea de producci�n." & "", _
                    True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
                Else
                    NewValorCol = QuickInputRequest("" & "Indique la Capacidad Maxima por D�a en Unidades para esta l�nea de producci�n." & "", _
                    True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
                End If
                
                If NewValorCol = "[**Cancel**]" Then Exit Sub
                
                If IsNumeric(NewValorCol) Then
                    
                    If CDbl(NewValorCol) >= 0 Then
                        
                        FrmAppLink.CnADM.BeginTrans: TransActive = True
                        
                        FrmAppLink.CnADM.Execute "DELETE FROM MA_PRODUCCION_CAPACIDAD_MAXIMA " & _
                        "WHERE c_CodArticulo = '" & .TextMatrix(.Row, ColCodProducto) & "' " & _
                        IIf(Trim(CodLocalidadSel) <> Empty, _
                        "AND c_CodLocalidad = '" & CodLocalidadSel & "' ", Empty) & _
                        IIf(Trim(CboLnPSel) <> Empty, _
                        "AND (c_LineaProduccion = '" & CboLnPSel & "') ", Empty)
                        
                        FrmAppLink.CnADM.Execute "INSERT INTO MA_PRODUCCION_CAPACIDAD_MAXIMA " & _
                        "(c_CodArticulo, c_CodLocalidad, c_LineaProduccion, " & _
                        "nCapacidadEmpaques, nCapacidadUnidades, cTipoFrecuencia, " & _
                        "nCantidadFrecuencia) VALUES ('" & .TextMatrix(.Row, ColCodProducto) & "', " & _
                        "'" & CodLocalidadSel & "', '" & CboLnPSel & "', " & _
                        "(" & CDbl(.TextMatrix(.Row, ColCapacidadEmpBase)) & "), " & _
                        "(" & Round(NewValorCol, Val(.TextMatrix(.Row, ColCantDec))) & "), 'D', 1)"
                        
                        FrmAppLink.CnADM.CommitTrans: TransActive = False
                        
                        .TextMatrix(.Row, ColCapacidadUniBase) = FormatNumber(NewValorCol, Val(.TextMatrix(.Row, ColCantDec)))
                        
                        RecalcularCapacidadIntervalo .TextMatrix(.Row, ColCapacidadEmpBase), .TextMatrix(.Row, ColCapacidadUniBase)
                        
                    Else
                        Mensaje True, "La cantidad no puede ser negativa."
                    End If
                    
                Else
                    Mensaje True, StellarMensaje(16136)
                End If
                
            ElseIf .Col > (FixedColCount - 1) Then
                
                Intervalo = CDec(1 + (Round(CDbl(.Col) - CDbl(FixedColCount - 1), 8) / CDbl(ColsxInt)) - 0.000001)
                Intervalo = Fix(Intervalo)
                Set DPItem = DPTemp(Intervalo)
                IntervaloColIni = (FixedColCount - 1) + ((CLng(Intervalo) - 1) * ColsxInt)
                IntervaloColFin = IntervaloColIni + (ColsxInt)
                
                If Date > DPItem("fPeriodoFin") Then
                    Mensaje True, "Este per�odo es de solo lectura, debido a que ya esta causado. No es posible realizar modificaciones."
                    Exit Sub
                End If
                
                Select Case .Col
                    
                    Case IntervaloColIni + 1 ' Emp. Req
                        
'                        If Not IsNumeric(ValorColStr) Then
'                            Mensaje True, "Debe cargar la informaci�n del intervalo antes de modificar este dato."
'                            Exit Sub
'                        End If
'
'                        ValorCol = CDbl(ValorColStr)
'
'                        If (Trim(CboLnPSel) = Empty) Then
'                            NewValorCol = QuickInputRequest("" & "Indique la Cantidad de Empaques requeridos para el per�odo, sin especificar l�nea de producci�n." & "", _
'                            True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
'                        Else
'                            NewValorCol = QuickInputRequest("" & "Indique la Cantidad de Empaques requeridos para el per�odo para esta l�nea de producci�n." & "", _
'                            True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
'                        End If
'
'                        If NewValorCol = "[**Cancel**]" Then Exit Sub
'
'                        If IsNumeric(NewValorCol) Then
'
'                            If CDbl(NewValorCol) >= 0 Then
'
'                                FrmAppLink.CnADM.BeginTrans: TransActive = True
'
'                                FrmAppLink.CnADM.Execute "DELETE FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS " & _
'                                "WHERE c_CodArticulo = '" & .TextMatrix(.Row, ColCodProducto) & "' " & _
'                                IIf(Trim(CodLocalidadSel) <> Empty, _
'                                "AND c_CodLocalidad = '" & CodLocalidadSel & "' ", Empty) & _
'                                IIf(Trim(CboLnPSel) <> Empty, _
'                                "AND (c_LineaProduccion = '" & CboLnPSel & "') ", Empty) & _
'                                "AND( " & _
'                                "(dFechaInicio BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND '" & FechaBD(DPItem("fPeriodoFin"), FBD_FULL) & "' " & _
'                                "AND dFechaFin BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND '" & FechaBD(DPItem("fPeriodoFin"), FBD_FULL) & "') " & _
'                                ")" '"OR ('" & FechaBD(DPItem("fPeriodoIni")) & "' BETWEEN dFechaInicio AND dFechaFin " & _
'                                "AND '" & FechaBD(DPItem("fPeriodoFin")) & "' BETWEEN dFechaInicio AND dFechaFin)) " Permitir coexistencia de planificaciones superiores.
'
'                                FrmAppLink.CnADM.Execute "INSERT INTO MA_PRODUCCION_PLANIFICACION_REQUERIDOS " & _
'                                "(c_CodArticulo, c_CodLocalidad, c_LineaProduccion, " & _
'                                "nEmpaquesRequerido, nUnidadesRequerido, cTipoPeriodo, " & _
'                                "dFechaInicio, dFechaFin) VALUES ('" & .TextMatrix(.Row, ColCodProducto) & "', " & _
'                                "'" & CodLocalidadSel & "', '" & CboLnPSel & "', (" & Round(NewValorCol, 0) & "), " & _
'                                "(" & Round(CDbl(.TextMatrix(.Row, IntervaloColIni + 2)), _
'                                Val(.TextMatrix(I, ColCantDec))) & "), '" & TipoIntervalo & "', " & _
'                                "'" & FechaBD(DPItem("fPeriodoIni")) & "', '" & FechaBD(DPItem("fPeriodoFin")) & "') "
'
'                                FrmAppLink.CnADM.CommitTrans: TransActive = False
'
'                                .TextMatrix(.Row, .Col) = Round(NewValorCol, 0)
'                                .CellBackColor = IIf(CDbl(.TextMatrix(.Row, .Col)) > 0, ColorUp, ColorNormal)
'
'                            Else
'                                Mensaje True, "La cantidad no puede ser negativa."
'                            End If
'
'                        Else
'                            Mensaje True, StellarMensaje(16136)
'                        End If
                        
                    Case IntervaloColIni + 2 ' Uni. Req
                        
'                        If Date > DPItem("fPeriodoFin") Then
'                            Mensaje True, "Este per�odo es de solo lectura, debido a que ya esta causado. No es posible realizar modificaciones."
'                            Exit Sub
'                        End If
'
'                        If Not IsNumeric(ValorColStr) Then
'                            Mensaje True, "Debe cargar la informaci�n del intervalo antes de modificar este dato."
'                            Exit Sub
'                        End If
'
'                        ValorCol = CDbl(ValorColStr)
'
'                        If (Trim(CboLnPSel) = Empty) Then
'                            NewValorCol = QuickInputRequest("" & "Indique la Cantidad de Unidades requeridas para el per�odo, sin especificar l�nea de producci�n." & "", _
'                            True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
'                        Else
'                            NewValorCol = QuickInputRequest("" & "Indique la Cantidad de Unidades requeridas para el per�odo para esta l�nea de producci�n." & "", _
'                            True, "[**Cancel**]", ValorColStr, "" & Replace(StellarMensaje(16602), "$(Text)", "la cantidad") & "", , , , , , , True)
'                        End If
'
'                        If NewValorCol = "[**Cancel**]" Then Exit Sub
'
'                        If IsNumeric(NewValorCol) Then
'
'                            If CDbl(NewValorCol) >= 0 Then
'
'                                FrmAppLink.CnADM.BeginTrans: TransActive = True
'
'                                FrmAppLink.CnADM.Execute "DELETE FROM MA_PRODUCCION_PLANIFICACION_REQUERIDOS " & _
'                                "WHERE c_CodArticulo = '" & .TextMatrix(.Row, ColCodProducto) & "' " & _
'                                IIf(Trim(CodLocalidadSel) <> Empty, _
'                                "AND c_CodLocalidad = '" & CodLocalidadSel & "' ", Empty) & _
'                                IIf(Trim(CboLnPSel) <> Empty, _
'                                "AND (c_LineaProduccion = '" & CboLnPSel & "') ", Empty) & _
'                                "AND( " & _
'                                "(dFechaInicio BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND '" & FechaBD(DPItem("fPeriodoFin"), FBD_FULL) & "' " & _
'                                "AND dFechaFin BETWEEN '" & FechaBD(DPItem("fPeriodoIni")) & "' AND '" & FechaBD(DPItem("fPeriodoFin"), FBD_FULL) & "') " & _
'                                ")" '"OR ('" & FechaBD(DPItem("fPeriodoIni")) & "' BETWEEN dFechaInicio AND dFechaFin " & _
'                                "AND '" & FechaBD(DPItem("fPeriodoFin")) & "' BETWEEN dFechaInicio AND dFechaFin)) "
'
'                                FrmAppLink.CnADM.Execute "INSERT INTO MA_PRODUCCION_PLANIFICACION_REQUERIDOS " & _
'                                "(c_CodArticulo, c_CodLocalidad, c_LineaProduccion, " & _
'                                "nEmpaquesRequerido, nUnidadesRequerido, cTipoPeriodo, " & _
'                                "dFechaInicio, dFechaFin) VALUES ('" & .TextMatrix(.Row, ColCodProducto) & "', " & _
'                                "'" & CodLocalidadSel & "', '" & CboLnPSel & "', (" & Round(CDbl(.TextMatrix(.Row, IntervaloColIni + 1)), 0) & "), " & _
'                                "(" & Round(NewValorCol, Val(.TextMatrix(.Row, ColCantDec))) & "), '" & TipoIntervalo & "', " & _
'                                "'" & FechaBD(DPItem("fPeriodoIni")) & "', '" & FechaBD(DPItem("fPeriodoFin")) & "') "
'
'                                FrmAppLink.CnADM.CommitTrans: TransActive = False
'
'                                .TextMatrix(.Row, .Col) = FormatNumber(NewValorCol, Val(.TextMatrix(.Row, ColCantDec)))
'                                .CellBackColor = IIf(CDbl(.TextMatrix(.Row, .Col)) > 0, ColorUp, ColorNormal)
'
'                            Else
'                                Mensaje True, "La cantidad no puede ser negativa."
'                            End If
'
'                        Else
'                            Mensaje True, StellarMensaje(16136)
'                        End If
                        
                    Case IntervaloColIni + 3 ' Emp. Plan
                        
                        Planificar (IntervaloColIni + 3), (IntervaloColIni + 4)
                        Exit Sub
                        
                    Case IntervaloColIni + 4 ' Uni. Plan
                        
                        Planificar (IntervaloColIni + 3), (IntervaloColIni + 4)
                        Exit Sub
                        
                End Select
                
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    If TransActive Then
        FrmAppLink.CnADM.RollbackTrans
        TransActive = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub Grid_Productos_KeyDown(KeyCode As Integer, Shift As Integer)
    
    With Grid_Productos
    
        Select Case KeyCode
        
            Case Is = vbKeyF2
                                                        
                If .Col = ColCodProducto Or .Col = ColDescri Then     'Columna: CodigoProducto
                    
                    If .TextMatrix(.Row, ColLn) <> Empty And .TextMatrix(.Row, ColCodProducto) <> Empty Then
                        .Row = .Rows - 1
                        Exit Sub
                    End If
                    
                    mArrProducto = BuscarInfoProducto_Basica(FrmAppLink.CnADM, , , , , VistaMarca)
                                                    
                    If Not IsEmpty(mArrProducto) Then
                        txtedit.Text = mArrProducto(0)
                        Call txtEdit_LostFocus
                    Else
                        txtedit.Text = Empty
                    End If
                              
                End If
            
            Case Is = vbKeyDelete
            
                If .Row >= 2 Then
                    
                    If .Row = 2 And .Rows = 3 Then
                        .Rows = .Rows + 1
                        .RemoveItem .Row
                    Else
                        .RemoveItem .Row
                    End If
                    
                    If Not pCargandoMasiva Then
                        For I = 2 To .Rows - 1
                            .TextMatrix(I, ColLn) = I - 1
                        Next I
                    End If
                    
                End If
                
        End Select
    
    End With
            
End Sub

Private Sub Grid_Productos_SelChange()
    txtedit = Grid_Productos.Text
End Sub

Private Sub opt_Intervalo_Click(Index As Integer)
    
    txtFechaDesde.Text = Empty
    txtFechaDesde.Tag = Empty
    txtFechaHasta.Text = Empty
    txtFechaHasta.Tag = Empty
    
    ValidarFechas
    
    Select Case Index
        Case 1
            FrameDiaInicioSemana.Visible = True
        Case Else
            FrameDiaInicioSemana.Visible = False
    End Select
    
End Sub

Private Sub txt_sucursal_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyF2 Then
        
        Set Forma = Me
        
        Call MAKE_VIEW("ma_sucursales", "c_Codigo", "c_Descripcion", _
        UCase(Stellar_Mensaje(1254)), Me, "GENERICO", , txt_sucursal, lbl_sucursal) '"S U C U R S A L E S"
        
    ElseIf KeyCode = vbKeyReturn Then
        
        txt_sucursal_LostFocus
    
    ElseIf KeyCode = vbKeyDelete Then
        
        txt_sucursal.Text = Empty
    
    End If
    
End Sub

Private Sub txt_sucursal_LostFocus()
    Dim RsEureka As New Recordset
    If txt_sucursal.Text <> Empty Then
        Call Apertura_Recordset(RsEureka)
        RsEureka.Open "SELECT * FROM MA_SUCURSALES " & _
        "WHERE c_Codigo = '" & txt_sucursal.Text & "'", _
        FrmAppLink.CnADM, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not RsEureka.EOF Then
            lbl_sucursal.Caption = RsEureka!c_Descripcion
        Else
            'Call Mensaje(True, "El c�digo no esta asignado a ning�n dep�sito.")
            Mensaje True, StellarMensaje(16145)
            lbl_sucursal.Caption = ""
            txt_sucursal.Text = ""
        End If
        Call Cerrar_Recordset(RsEureka)
    Else
        lbl_sucursal.Caption = ""
    End If
End Sub

Private Sub txtFechaDesde_Click()
    
    Dim mTitulo As String
    
    Select Case True
        Case opt_Intervalo(0)
            mTitulo = "Haga clic para seleccionar el d�a de inicio"
        Case opt_Intervalo(1)
            mTitulo = "Seleccione cualquier d�a de la semana de inicio"
        Case opt_Intervalo(2)
            mTitulo = "Seleccione cualquier d�a del mes de inicio"
        Case opt_Intervalo(3)
            mTitulo = "Seleccione cualquier d�a del a�o de inicio"
    End Select
    
    DatePicker_Fecha txtFechaDesde, mTitulo, _
    IIf(OptDiaInicioSemana(1).Value, _
    MSComCtl2.DayConstants.mvwMonday, MSComCtl2.DayConstants.mvwSunday)
    
    ValidarFechas
    
End Sub

Private Sub TxtFechaHasta_Click()
    
    Dim mTitulo As String
    
    Select Case True
        Case opt_Intervalo(0)
            mTitulo = "Haga clic para seleccionar el d�a de fin"
        Case opt_Intervalo(1)
            mTitulo = "Seleccione cualquier d�a de la semana de fin"
        Case opt_Intervalo(2)
            mTitulo = "Seleccione cualquier d�a del mes de fin"
        Case opt_Intervalo(3)
            mTitulo = "Seleccione cualquier d�a del a�o de fin"
    End Select
    
    DatePicker_Fecha txtFechaHasta, mTitulo, _
    IIf(OptDiaInicioSemana(1).Value, _
    MSComCtl2.DayConstants.mvwMonday, MSComCtl2.DayConstants.mvwSunday)
    
    ValidarFechas
    
End Sub

Private Sub ValidarFechas()
    
    If IsNumeric(txtFechaDesde.Tag) Then
        nDesde = CDbl(txtFechaDesde.Tag)
    End If
    
    If IsNumeric(txtFechaHasta.Tag) Then
        nHasta = CDbl(txtFechaHasta.Tag)
    End If
    
    If nDesde > nHasta Then nHasta = 0
    
    If nDesde = 0 Then txtFechaDesde.Text = Empty
    If nHasta = 0 Then txtFechaHasta.Text = Empty
    
    Dim DiaSemanaVB, DiaSemanaSQL
    
    Select Case True
        Case opt_Intervalo(0)
            
            If nDesde > 0 Then
                fDesde = CDate(nDesde)
                txtFechaDesde.Text = SDate(fDesde)
            End If
            
            If nHasta > 0 Then
                fHasta = CDate(nHasta)
                txtFechaHasta.Tag = SDate(fHasta)
            End If
            
        Case opt_Intervalo(1)
            
            If OptDiaInicioSemana(0).Value Then
                DiaSemanaVB = vbSunday
                DiaSemanaSQL = 7
            Else
                DiaSemanaVB = vbMonday
                DiaSemanaSQL = 1
            End If
            
            If nDesde > 0 Then
                fDesde = CDate(nDesde)
                NumWDay = Weekday(fDesde, DiaSemanaVB)
                If NumWDay > 1 Then nDesde = CDbl(DateAdd("d", -1 * (NumWDay - 1), fDesde))
                fDesde = CDate(nDesde)
                txtFechaDesde.Tag = nDesde
                txtFechaDesde.Text = Replace(SDate(fDesde), Year(fDesde), Format(fDesde, "yy")) & _
                " a " & _
                Replace(SDate(CDate(nDesde + 6)), Year(CDate(nDesde + 6)), Format(CDate(nDesde + 6), "yy")) & _
                "  (" & DatePart("ww", fDesde, DiaSemanaVB, vbFirstJan1) & ")"
            End If
            
            If nHasta > 0 Then
                fHasta = CDate(nHasta)
                NumWDay = Weekday(fHasta, DiaSemanaVB)
                If NumWDay > 1 Then nHasta = CDbl(DateAdd("d", -1 * (NumWDay - 1), fHasta))
                txtFechaHasta.Tag = nHasta
                txtFechaHasta.Text = Replace(SDate(fHasta), Year(fHasta), Format(fHasta, "yy")) & _
                " a " & _
                Replace(SDate(CDate(nHasta + 6)), Year(CDate(nHasta + 6)), Format(CDate(nHasta + 6), "yy")) & _
                "  (" & DatePart("ww", fHasta, DiaSemanaVB, vbFirstJan1) & ")"
            End If
            
        Case opt_Intervalo(2)
            
            If nDesde > 0 Then
                fDesde = CDate(nDesde)
                nDesde = CDbl(DateSerial(Year(fDesde), Month(fDesde), 1))
                fDesde = CDate(nDesde)
                txtFechaDesde.Tag = nDesde
                txtFechaDesde.Text = MonthName(Month(fDesde)) & " " & Year(fDesde)
            End If
            
            If nHasta > 0 Then
                fHasta = CDate(nHasta)
                nHasta = CDbl(DateAdd("d", -1, DateAdd("M", 1, DateSerial(Year(fHasta), Month(fHasta), 1))))
                fHasta = CDate(nHasta)
                txtFechaHasta.Tag = nHasta
                txtFechaHasta.Text = MonthName(Month(fHasta)) & " " & Year(fHasta)
            End If
            
        Case opt_Intervalo(3)
            
            If nDesde > 0 Then
                fDesde = CDate(nDesde)
                nDesde = CDbl(DateSerial(Year(fDesde), 1, 1))
                fDesde = CDate(nDesde)
                txtFechaDesde.Tag = nDesde
                txtFechaDesde.Text = Year(fDesde)
            End If
            
            If nHasta > 0 Then
                fHasta = CDate(nHasta)
                nHasta = CDbl(DateAdd("d", -1, DateAdd("yyyy", 1, DateSerial(Year(fHasta), 1, 1))))
                fHasta = CDate(nHasta)
                txtFechaHasta.Tag = nHasta
                txtFechaHasta.Text = Year(fHasta)
            End If
            
    End Select
    
End Sub

Private Sub RecalcularCapacidadIntervalo( _
ByVal pEmpaques As String, ByVal pUnidades As String)
    
    With Grid_Productos
    
    Dim I: I = .Row
    
    mTotal = Round(CDbl(pUnidades) + (CDbl(pEmpaques) * CDbl(.TextMatrix(I, ColCantibul))), Val(.TextMatrix(I, ColCantDec)))
    
    Select Case TipoIntervalo
        Case "D"
            mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
            mEmp = Fix(mEmpRaw)
            mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
            .Col = ColCapacidadEmpIntervalo
            .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
            .Col = ColCapacidadUniIntervalo
            .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
        Case "W"
            mTotal = Round(mTotal * DiasHabilesW, Val(.TextMatrix(I, ColCantDec)))
            mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
            mEmp = Fix(mEmpRaw)
            mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
            .Col = ColCapacidadEmpIntervalo
            .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
            .Col = ColCapacidadUniIntervalo
            .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
        Case "M"
            mTotal = Round(mTotal * DiasHabilesM, Val(.TextMatrix(I, ColCantDec)))
            mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
            mEmp = Fix(mEmpRaw)
            mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
            .Col = ColCapacidadEmpIntervalo
            .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
            .Col = ColCapacidadUniIntervalo
            .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
        Case "Y"
            mTotal = Round(mTotal * DiasHabilesY, Val(.TextMatrix(I, ColCantDec)))
            mEmpRaw = (mTotal / CDbl(.TextMatrix(I, ColCantibul)))
            mEmp = Fix(mEmpRaw)
            mUnd = Round(mEmpRaw - mEmp, 8) * CDbl(.TextMatrix(I, ColCantibul))
            .Col = ColCapacidadEmpIntervalo
            .TextMatrix(I, .Col) = IIf(FormaDeTrabajo > 0, mEmp, 0)
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
            .Col = ColCapacidadUniIntervalo
            .TextMatrix(I, .Col) = FormatNumber(IIf(FormaDeTrabajo = 0, mTotal, mUnd), Val(.TextMatrix(I, ColCantDec)))
            .CellBackColor = IIf(CDbl(.TextMatrix(I, .Col)) > 0, ColorUp, ColorNormal)
    End Select
    
    End With
    
End Sub

Private Sub Planificar(ByVal ColEmpReq, ByVal ColEmpUni)
    
    If Date > DPItem("fPeriodoFin") Then
        Mensaje True, "Este per�odo es de solo lectura, debido a que ya esta causado. No es posible realizar modificaciones."
        Exit Sub
    End If
    
    With Grid_Productos
        
        FrmPlanificacionxProducto.FormaDeTrabajo = FormaDeTrabajo
        FrmPlanificacionxProducto.MostrarCantibul = MostrarCantibul
        
        FrmPlanificacionxProducto.CodProducto = .TextMatrix(.Row, ColCodProducto)
        FrmPlanificacionxProducto.Descripcion = .TextMatrix(.Row, ColDescri)
        FrmPlanificacionxProducto.Presentacion = .TextMatrix(.Row, ColPresentacion)
        FrmPlanificacionxProducto.Periodo = DPItem("fPeriodo")
        Set FrmPlanificacionxProducto.DatosPeriodo = DPItem
        
        FrmPlanificacionxProducto.CapacidadEmp = .TextMatrix(.Row, ColCapacidadEmpIntervalo)
        FrmPlanificacionxProducto.CapacidadUni = .TextMatrix(.Row, ColCapacidadUniIntervalo)
        FrmPlanificacionxProducto.CantDec = Val(.TextMatrix(.Row, ColCantDec))
        FrmPlanificacionxProducto.Cantibul = CDbl(.TextMatrix(.Row, ColCantibul))
        
        If FormaDeTrabajo = 1 Then
            mTotal = CDbl(.TextMatrix(.Row, ColCapacidadEmpIntervalo))
        Else
            mTotal = FormatNumber(CDbl(.TextMatrix(.Row, ColCapacidadUniIntervalo)) + _
            (CDbl(.TextMatrix(.Row, ColCapacidadEmpIntervalo)) * FrmPlanificacionxProducto.Cantibul), _
            FrmPlanificacionxProducto.CantDec)
        End If
        
        FrmPlanificacionxProducto.CapacidadPeriodo = mTotal
        
        FrmPlanificacionxProducto.StockMinimo = FormatNumber(.TextMatrix(.Row, ColStockMinimo), FrmPlanificacionxProducto.CantDec)
        FrmPlanificacionxProducto.StockMaximo = FormatNumber(.TextMatrix(.Row, ColStockMaximo), FrmPlanificacionxProducto.CantDec)
        
        If FormaDeTrabajo = 1 Then
            mTotal = CDbl(.TextMatrix(.Row, ColEmpReq - 2))
        Else
            mTotal = FormatNumber(CDbl(.TextMatrix(.Row, ColEmpUni - 2)) + _
            (CDbl(.TextMatrix(.Row, ColEmpReq - 2)) * FrmPlanificacionxProducto.Cantibul), _
            FrmPlanificacionxProducto.CantDec)
        End If
        
        FrmPlanificacionxProducto.CantidadRequerida = mTotal
        FrmPlanificacionxProducto.LineaProduccion = CboLnPSel
        FrmPlanificacionxProducto.TurnoProduccion = CboTurnoSel
        FrmPlanificacionxProducto.CodLocalidad = CodLocalidadSel
        
        FrmPlanificacionxProducto.Show vbModal
        
        If FrmPlanificacionxProducto.EjecutoPlanificacion Then
            Set FrmPlanificacionxProducto = Nothing
            cmd_Cambiar_Click
            Exit Sub
        End If
        
        Set FrmPlanificacionxProducto = Nothing
        
    End With
    
End Sub

Private Sub departamento_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub departamento_GotFocus()
    Set CampoT = departamento
End Sub

Private Sub bdpto_Click()
    departamento.SetFocus
    Call departamento_KeyDown(vbKeyF2, 0)
End Sub

Private Sub departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_DEPARTAMENTOS"
            Titulo = UCase(Stellar_Mensaje(361)) '"D E P A R T A M E N T O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            departamento.Tag = Empty
            departamento.Text = Empty
            LBL_DEPARTAMENTO.Caption = Empty
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub departamento_LostFocus()
    If departamento.Text <> "" Then
        Set Forma = Me
        Call Carga_DGS(departamento, "MA_DEPARTAMENTOS", 0)
    Else
        Call departamento_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub grupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub grupo_GotFocus()
    Set CampoT = grupo
End Sub

Private Sub bgrupo_Click()
    grupo.SetFocus
    Call grupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub grupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_GRUPOS"
            Titulo = UCase(Stellar_Mensaje(362)) 'G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub grupo_LostFocus()
    If grupo.Text <> "" Then
        Set Forma = Me
        Call Carga_DGS(grupo, "MA_GRUPOS", 1)
    Else
        Call grupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub bsubgrupo_Click()
    subgrupo.SetFocus
    Call subgrupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub subgrupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub subgrupo_GotFocus()
    Set CampoT = subgrupo
End Sub

Private Sub subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_SUBGRUPOS"
            Titulo = UCase(Stellar_Mensaje(363)) 'S U B - G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub subgrupo_LostFocus()
    If subgrupo.Text <> "" Then
        Set Forma = Me
        Call Carga_DGS(subgrupo, "MA_SUBGRUPOS", 2)
    Else
        Call subgrupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub
